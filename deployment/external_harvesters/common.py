import json
import logging
import os
import sys


logging.basicConfig(level=os.getenv("PYTHON_LOG"))


def read_config():
    return json.loads(sys.stdin.readline())["Config"]


def write_dataset(dataset):
    print(json.dumps({"Dataset": dataset}), flush=True)


def log_error(error):
    print(json.dumps({"Error": error}), flush=True)


def fetch_text(key, url, isolated=False):
    print(
        json.dumps(
            {
                "FetchText": {
                    "key": key,
                    "url": url,
                    "isolated": isolated,
                }
            }
        ),
        flush=True,
    )
    msg = json.loads(sys.stdin.readline())
    if "Error" in msg:
        raise RuntimeError(msg["Error"])
    return msg["Text"]
