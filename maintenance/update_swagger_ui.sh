#!/bin/bash -xe
path_to_swagger="https://github.com/swagger-api/swagger-ui/archive/refs/tags/"
file_name="v$1.tar.gz"
dir_name="swagger-ui-$1"

if [ $# != 1 ]
then
	echo "usage: $0 <version>"
	exit 255
fi

cd $(dirname "$0")

curl --output "$file_name" --location "$path_to_swagger$file_name"
tar -xf "$file_name"

gunzip "../server/assets/swagger-ui/swagger-initializer.js.gz"

git diff --no-index "./$dir_name/dist/swagger-initializer.js" "../server/assets/swagger-ui/swagger-initializer.js" || true

rm -f ./"$dir_name"/dist/*.map ./"$dir_name"/dist/*.png ./"$dir_name"/dist/*.map ./"$dir_name"/dist/*standalone* ./"$dir_name"/dist/*initializer*

gzip --best ./"$dir_name"/dist/* "../server/assets/swagger-ui/swagger-initializer.js"
mv --force ./"$dir_name"/dist/* "../server/assets/swagger-ui/"

rm -rf "$dir_name" "$file_name"
