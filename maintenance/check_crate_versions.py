#!/usr/bin/env python3

import requests

from concurrent.futures import ThreadPoolExecutor
from packaging.version import Version
from tomllib import loads
from pathlib import Path


def all_manifests(path):
    for entry in path.iterdir():
        if entry.is_file() and entry.name == "Cargo.toml":
            yield entry
        elif entry.is_dir() and not entry.name.startswith("."):
            for manifest in all_manifests(entry):
                yield manifest


def collect_crate_versions(crate_versions, manifest_path, dependencies):
    global first_seen

    for crate, dependency in dependencies.items():
        if isinstance(dependency, str):
            version = dependency
        else:
            version = dependency.get("version")

        if version:
            prev_crate_version = crate_versions.get(crate)
            if prev_crate_version:
                raise Exception(
                    f"{crate}: redundant dependency in {prev_crate_version[1]} and {manifest_path}"
                )

            crate_versions[crate] = (Version(version), manifest_path)


session = requests.Session()


def check_crate_version(arg):
    global session

    crate, (version, manifest_path) = arg

    metadata = session.get(f"https://crates.io/api/v1/crates/{crate}").json()

    for current_version in metadata["versions"]:
        current_version = Version(current_version["num"])

        if not current_version.is_prerelease:
            break
    else:
        raise Exception(f"{crate}: only pre-releases available")

    update = None

    if version.major == 0:
        if current_version.major != 0:
            update = "stable"
        elif version.minor != current_version.minor:
            update = "unstable"
    elif version.major != current_version.major:
        update = "major"

    if update:
        print(f"{crate}: {update} {current_version} > {version} ({manifest_path})")


workspace_path = None

path = Path(".").resolve()

for path in [path, *path.parents]:
    if (path / "Cargo.toml").is_file():
        workspace_path = path

if workspace_path is None:
    raise Exception("failed to determine workspace path")

crate_versions = {}

for manifest_path in all_manifests(workspace_path):
    manifest = loads(manifest_path.read_text())

    manifest_path = manifest_path.relative_to(workspace_path)

    dependencies = manifest.get("dependencies")
    if dependencies:
        collect_crate_versions(crate_versions, manifest_path, dependencies)

    workspace = manifest.get("workspace")
    if workspace:
        dependencies = workspace.get("dependencies")
        if dependencies:
            collect_crate_versions(crate_versions, manifest_path, dependencies)


with ThreadPoolExecutor() as executor:
    executor.map(check_crate_version, crate_versions.items())
