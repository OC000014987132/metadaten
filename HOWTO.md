# How to integrate data sources into the metadata index

We need to integrate a variety of data sources using different protocols and metadata schemas into a single index. In addition to standardized interfaces like the [CKAN API](https://docs.ckan.org/en/latest/api/) or [CSW API](https://en.wikipedia.org/wiki/Catalogue_Service_for_the_Web), we therefore often write modules which are specific to a single data source.

Generally speaking, we differentiate three kinds of integrations: "harvesters", "crawlers" and "scrapers". A harvester is understood to access machine-readable catalogued data. A crawler works with data that is still machine-readable, but is not available as cohesive catalogue and rather needs to be collected follow some sort of linkage structure like a directory hierarchy. Finally, a scraper targets human-readable information and tries to extract machine-readable structured data.

Colloquially, we often refer to all types of integrations as "harvesters" when the specific type is not relevant. Examples for proper harvesters in our code base are the [`csw` module](harvester/src/csw.rs) and the [`ckan` module](harvester/src/ckan.rs). Examples for scrapers in our code base are the [`doris_bfs` module](harvester/src/doris_bfs.rs) and the [`website_bfn` module](harvester/src/website_bfn.rs).

While these require different techniques to implement, they share multiple common goals:

* We always need to provide a useful hyperlink back to the data source which the user can follow as we want to improve the visibility of the sources instead of our own.
For example, even if we harvest a catalogue with JSON-based API, we need to provide hyperlinks into an affiliated website 
* All collected data should improve the findability of the dataset in the index. Mostly this implies that we do not need datasets which are in some sense complete, but we only need those attributes which help the users answer their questions.
For example, we want to support searching for datasets based on their spatial extent (with the help of a bounding box). However, the coordinate reference system (CRS) is only required for a spatial dataset when a dataset is actually processed and we therefore do not include that information in the index.
* A corollary to the previous goal, we need to harmonize the collected metadata so that the dimensions in our index are comparable. This often implies that we need more coarse attributes compared to capturing the full information contained in the source.
For example, it is not so important whether a date we attach to a dataset represents when it was created, published or updated. Rather, we should aim to provide a rough point in time for as many datasets as possible so that filtering by time is as useful as possible.

To register a new harvester, for example named `my_fabulous_harvester`, one would create a new Rust module at `harvester/src/my_fabulous_harvester.rs` by adding `mod my_fabulous_harvester;` in `harvester/src/main.rs`. This module needs to export a single public asynchronous function

```rust
pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> { .. }
```

Finally, the new harvester needs to be added to the invocation of the `dispatch_harvester!` macro in `harvester/src/main.rs`. It can then be used by specifying `type = "my_fabulous_harvester"` in the `harvester.toml` configuration file.

## Techniques

It is easy to accidentally overwhelm data sources by making many requests concurrently or with a high rate. We therefore apply different standard techniques to manage the load we place on the data sources.

### Caching

While metadata extraction and indexing run completely each day to enable the resulting simplicity of our data and schema management, all server responses we receive from the data sources are cached on disk to avoid retrieving this data every day if that is unnecessary from a functional perspective. Additionally, this helps during iterative development where intermediate responses are retrieved repeatedly until the harvester is complete.

This depends on the developer providing a _key_ for each request to functions like `Client::make_request` or `fetch_text` under which the compressed response is stored on disk. For example, if the source consists of multiple pages of results, the page number might be a reasonable key. For scrapers of websites, using the path part of the URL with slashes replaced by dashes is often sufficient.

A special case are keys which end with `.isol` for "isolated". By default, all stored responses of a given source are deleted together after a specified maximum age to ensure their internal consistency, e.g. links between the pages. However, for some of the sources, some of the responses can be expired individually as they are self-contained. In this case the usual procedure is to collect a table of contents using non-isolated responses and fetch the individual items as isolated responses. Examples of this particular case are the [`wisia_bfn` module](harvester/src/wisia_bfn.rs) and the [`rote-list-zentrum-bfn.rs` module](harvester/src/rote_liste_zentrum_bfn.rs). If the maximum age is combined with an age smear, it will result in gradual updates of the isolated responses over a certain duration combined with rare updates of the non-isolated responses. In general, it is preferable to isolate responses. However, this process is often not possible, if there are dependencies between the pages.

Note that human-readable keys are helpful because it enables us to manually inspect the responses stored on disk in `$DATA_PATH/responses/<source>/<key>`. For example, we sometimes use Jupyter notebooks reading those responses to analyze the data before deciding on the details of a given harvester.

### Networking

The reusable functions for making network requests based on the `Client::make_request` interface also handle further concerns. First, they automatically retry failed requests in the sense that the closure passed to the method returned an `Result::Err` variant after waiting for a geometrically increasing duration. Second, they limit the amount of requests which are made concurrently. This is configured using the `source.concurrency` field of the harvester configuration and defaults to one. Finally, they can be configured to limit rate of requests if enable via the `source.rate_limit` field of the harvester configuration.

For data sources which require a large amount of requests, it is often helpful to use a concurrency larger than one to have multiple requests pending at the same time. To do so, the `fetch_many` helper function can be used (including in a nesting manner) to iterate over a list of objects each of which produces a separate request. Note that using this function can be beneficial even if the source is not configured for concurrent requests, because other requests made for each dataset, for example towards SNS or the file system, have independent concurrency limits.  

### Accounting and pagination

For our internal accounting, most importantly logs and metrics, we compute three numbers for each harvest: The number datasets available at the sources, usually called `count` in the code. The number of datasets which we were able to transmit from the source, usually called `results` or `transmitted` in the code. And finally the number of errors encountered while fetching the dataset, usually called `errors` or `failed` in the code. Note that `count == results + errors` does not always hold as some errors affect more than one dataset when for example a paginated API is used.

Such paginated API are relatively common with catalogues and search engines and the above interacts with that insofar our helper function often expect harvesters to return a triple `Result<(usize, usize, usize)>` of the form `Ok((count, results, errors))` even if only some of these numbers are used. A typical approach is to have one function to fetch a page of results and then apply `fetch_many` to the set of all pages, e.g.

```rust
let (count, results, errors) = fetch_page(client, dir, source, 0).await?;

let pages = pages(count, source.batch_size);

let (results, errors) = fetch_many(results, errors, 1..pages, |page| {
    fetch_page(dir, client, source, page)
})
.await;
```

### Parsing and deserialization

When data sources provide API which yield structured data, e.g. JSON or XML, we currently use two basic approaches: Parse the result into an untyped representation and programmatically extract the desired information.[^1] Or create a typed representation of the relevant part of the response and deserialize it using `serde`. 

[^1]: Building scrapers by parsing HTML and selecting elements using CSS selectors also falls into this category.

To contrast the two example, consider the following XML document:

```xml
<?xml version="1.0" ?>

<results>
    <dataset>
        <title>The title...</title>
        <description>...and its description.</description>
        <year>2023</year>
    </dataset>
</results>
```

Applying the first approach to extract title, description and year fields using the [`roxmltree` crate](https://github.com/RazrFalcon/roxmltree) would entail the following:

```rust
let document = Document::parse(&text)?;

for result in document.root_element().children() {
    let title = result
        .children()
        .find(|element| element.has_tag_name("title"))
        .ok_or_else(|| anyhow!("Missing title"))?;

    let description = result
        .children()
        .find(|element| element.has_tag_name("description"));
    
    let year = result
        .children()
        .find(|element| element.has_tag_name("year"))
        .map(|year| year.parse::<i32>())
        .transpose()?;

    write_dataset(title, description, year).await?;
}
```

The second approach using the [`serde-roxmltree` crate](https://github.com/adamreichold/serde-roxmltree) instead would look like:

```rust
#[derive(Deserialize)]
struct Results {
    result: Vec<Result>,
}

#[derive(Deserialize)]
struct Result {
    title: String,
    description: Option<String>,
    year: Option<i32>,
}

let results = from_str::<Results>(&text)?;

for result in results.result {
    write_dataset(
        result.title,
        result.description,
        result.year,
    )
    .await?;
}
```

Generally speaking, the first approach is more effective when a small subset of the overall data structure is to be extracted in a rather targeted manner. The second works better when most of the data is relevant and the one time effort of modelling its structure as types is amortized over many extractions. Using a domain-specific language like XPath or CSS selectors to simplify the programmatic extraction can sometimes shift this balance though.

## Writing external harvesters

For sources which do not employ a reuseable protocol, e.g. one-off scrapers, and which do not have stringent performance requirements, there exists the option of integrating external programs as harvesters, usually free-standing Python scripts.

Such an external program must be placed in [`deployment/external_harvesters`](deployment/external_harvesters) as an executable file, usually including a shebang like `#!/usr/bin/env python3`. These programs are expected to communicate with the parent process via the standard input and output streams, direct their log message to the standard error stream and follow a line-oriented protocol:

* They should set the log level according to `PYTHON_LOG` environment variable, e.g. `logging.basicConfig(level=os.getenv("PYTHON_LOG"))`.
* They should read the configuration as the first line of standard input in JSON, e.g. `config = json.loads(sys.stdin.readline())["Config"]`.
* They should write one dataset per line to standard output as JSON, following the schema available at [`assets/openapi.yaml`](assets/openapi.yml), e.g. `print(json.dumps({"Dataset": dataset}), flush=True)`.
* To fetch text from an URL using the HTTP connection pool managed by the parent process, they should write a request to standard output, e.g. `print({"FetchText": {"key": key, "url": url}}, flush=True)`, and expect a response on standard input, e.g. `text = json.loads(sys.stdin.readline())["Text"]`.

## Writing scrapers

A lot of the systems we integrate into the metadata index are basically websites intended for human consumption or at least systems that provide no readily machine-readable form of access. Hence we often need to write scrapers which try to produce as much structured metadata as possible from human-readable text and most importantly from the underlying structure of the HTML code.

The main downside of this approach is that it is rather brittle as the HTML code can change often and spuriously even if the underlying information was not modified at all. Hence each scraper commits us to significant amount of ongoing maintenance effort to keep up with changes to the design of the source pages.

Another issue is websites which are based on client-side interactive content, e.g. pages which dynamically constructed using JavaScript or WebAssembly. We currently try to avoid this as much as possible as this would required us to drive a full browser engine from within the harvester, for example via the [WebDriver](https://www.selenium.dev/documentation/webdriver/) API. We therefore currently limit ourselves to writing so-called passive scrapers which just fetch the HTML source, parse it and then try to extract the relevant information from that.

We also generally try to avoid classic crawlers, i.e. harvesters which indiscriminately follow all links emanating from a given page. Instead, we explicitly target pages which we consider good candidates for a dataset, for example we often scrape based on a website's navigation or reuse existing search indexes instead of crawling a website ourselves.

Nevertheless, the URL of the pages we fetch from a given site are often not known statically and this usually means that we have to automatically derive a request key based on the URL, or at least its [path component](https://docs.rs/url/latest/url/struct.Url.html#method.path). Often, it is sufficient to replace slashes by dashes via `url.path().replace('/', "-")` to be able to use the path as a file name and thereby as a request key.

The main workflow for writing scrapers which extract structured information from websites starts with the development tools provided by all the common browsers ([Firefox](https://firefox-source-docs.mozilla.org/devtools-user/), [Chrome](https://developer.chrome.com/docs/devtools/), etc.) to inspect the structure of a page and derive the corresponding CSS selectors ([Tutorial](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Selectors), [Reference](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_selectors)) which extract certain elements from the given HTML structure. These selectors can also be tested interactively in the browser console using JavaScript, e.g.

```javascript
document.querySelectorAll("table.results > tbody > tr > td > a.main_link[href]")
```

In Rust we use the [`scraper`](https://github.com/causal-agent/scraper) crate which provides the scaffolding required to integrate the [`html5ever`](https://github.com/servo/html5ever), [`cssparser`](https://github.com/servo/rust-cssparser) and [`selectors`](https://github.com/servo/servo/tree/master/components/selectors) crates from the [Servo project](https://servo.org/) outside of that particular browser engine. So the above JavaScript code would look something like

```rust
let selector = Selector::parse("table.results > tbody > tr > td > a.main_link[href]").unwrap();
let elements  = document.select(&selector);

for element in elements {
    dbg!(element.attr("href").unwrap());
}
```

when using Rust and the `scraper` crate.

It is often helpful to avoid selecting all relevant elements from the top level and instead resume the selection process from intermediate nodes which were themselves selected earlier. For example, when extracting information from a table, it might help to first select each table row and only then select the data presented in the columns, i.e. start with

```rust
let row_selector = Selector::parse("table.results > tbody > tr").unwrap();
let rows = document.select(&row_selector);
```

and then extract the relevant columns

```rust
for row in rows {
    let main_link_selector = Selector::parse("td > a.main_link[href]").unwrap();
    let main_link = row.select(&main_link_selector).next().unwrap();

    let description_selector = Selector::parse("td > p.description").unwrap();
    let description = row.select(&description_selector).next().unwrap();
}
```

Parsing CSS selectors is expensive and so to improve efficiency, we aim to do this only once per harvester invocation by collecting all CSS selectors ([`scraper::Selector`](https://docs.rs/scraper/latest/scraper/selector/struct.Selector.html)) and regular expressions ([`regex::Regex`](https://docs.rs/regex/latest/regex/struct.Regex.html)) which are used multiple times into a single `struct Selectors`:

```rust
struct Selectors {
    row: Selector,
    main_link: Selector,
    description: Selector,
}

impl Default for Selectors {
    fn default() -> Self {
        Self {
            row: Selector::parse("table.results > tbody > tr").unwrap(),
            main_link: Selector::parse("td > a.main_link[href]").unwrap(),
            description: Selector::parse("td > p.description").unwrap(),
        }
    }
}

fn parse_table(document: &Html, selectors: &Selectors) {
    let rows = document.select(&selectors.row);

    for row in rows {
        let main_link = row.select(&selectors.main_link).next().unwrap();
        let description = row.select(&selectors.description).next().unwrap();
    }
}
```

To reduce the amount of boiler plate required to manage `struct Selectors`, the `selectors!` macro can be used to replace the above code by:

```rust
selectors! {
    row: "table.results > tbody > tr",
    main_link: "td > a.main_link[href]",
    description: "td > p.description",
}
```

The text nodes inside HTML documents will often contain superfluous whitespace, for example due to indentation, the HTML source 

```html
<p>
    <b>This text is bold.</b>
    This text is plain.
</p>
```

would yield the text nodes `\t`, `This text is bold.` and `\n\tThis text is plain.\n` which needs to be collected into `This text is bold. This text is plain.`. To do this efficiently, we have a `collect_text` helper function which will trim whitespace from the text nodes and drop those which are empty after trimming, e.g.

```rust
assert_eq!(
    collect_text(fragment.root_element().text()),
    "This text is bold. This text is plain."
);
```

### Future cannot be sent between threads safely

When using the [`scraper` crate](https://github.com/causal-agent/scraper) to build scrapers, one usually encounters the type `scraper::Html`, e.g. when parsing the fetched text:

```rust
let document = Html::parse_document(&text);

let description = document.select(&selectors.description).next().map(|element| collect_text(element.text()));
```

This will often lead to seemingly unrelated compiler errors on a unfulfilled `Send` bound involving the `tendril` crate, e.g.

```console
error: future cannot be sent between threads safely
   --> src/bin/harvester.rs:54:23
    |
54  |                   spawn(async move {
    |  _______________________^
55  | |                     let result = harvest(&dir, &client, &source)
56  | |                         .await
57  | |                         .with_context(|| format!("Failed to harvest source {}", source.name));
58  | |
59  | |                     (source, result)
60  | |                 })
    | |_________________^ future created by async block is not `Send`
    |
    = help: within `tendril::tendril::NonAtomic`, the trait `std::marker::Sync` is not implemented for `std::cell::Cell<usize>`
    = note: if you want to do aliasing and mutation between multiple threads, use `std::sync::RwLock` or `std::sync::atomic::AtomicUsize` instead
note: required by a bound in `tokio::spawn`
   --> /home/ubuntu/.cargo/registry/src/index.crates.io-6f17d22bba15001f/tokio-1.28.2/src/task/spawn.rs:163:21
    |
163 |         T: Future + Send + 'static,
    |                     ^^^^ required by this bound in `spawn`
```

This happens because `scraper::Html` or rather `tendril::tendril::NonAtomic` contained therein is not thread-safe as this increases the efficiency of the implementation. Most importantly, this implies that it cannot be held across calling `.await` due to the [`tokio` runtime](https://tokio.rs/) using multiple threads to distribute the workload across multiple processors.

The solution is to put a scope around the usage of the document to ensure that it is is not held across a yield point, i.e.

```rust
let description;

{
    let document = Html::parse_document(&text);

    description = document.select(&selectors.description).next().map(|element| collect_text(element.text()));
}
```

The details of asynchronous I/O, multi-threaded runtimes, non-sendable futures and yield points underpinning this particular error message are covered by the official learning materials available at <https://www.rust-lang.org/learn>.

### Further reading

A nice self-contained introduction to writing scrapers using Rust is <https://github.com/kxzk/scraping-with-rust> which notably provides a complete ready-to-use code base. And while it targets Python code, the article at <https://datascience.blog.wzb.eu/2020/12/04/robust-data-collection-via-web-scraping-and-web-apis/> discusses some of the more advanced topics on scraping. 

There a several organizations committed to make government data more accessible, which includes writing scrapers when no API-based access is possible:
* _bundDEV_ which mostly documents existing API, but also provides API which are internally based on scrapers.
    - <https://bund.dev/>
    - <https://github.com/bundesAPI>
* _Open States_ which heavily rely on scrapers to accompany in the legislative process in the United States.
    - <https://openstates.org/>
    - <https://github.com/openstates>
    - <https://docs.openstates.org/contributing/scrapers/>

Several commercial entities provide infrastructure like IP address pools for scraping web resources and provide some general introductions, but one should keep in mind that these companies have something to sell:
* <https://www.scrapingbee.com/blog/web-scraping-rust/>
* <https://oxylabs.io/blog/rust-web-scraping>
* <https://datagrab.io/blog/web-scraping-best-practices/>
