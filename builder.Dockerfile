FROM ubuntu:latest

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH

RUN apt-get update && \
    apt-get install --no-install-recommends --yes \
        curl ca-certificates unzip \
        build-essential git-lfs cmake clang \
        pkg-config libssl-dev libsqlite3-dev libpoppler-glib-dev libqpdf-dev \
        pipx && \
    rm -rf /var/lib/apt/lists/*

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | \
    sh -s -- -y --no-modify-path --profile minimal --component rustfmt clippy

RUN export CARGO_HOME=/tmp/cargo && \
    export CARGO_BUILD_JOBS=2 && \
    export CARGO_PROFILE_RELEASE_LTO=thin && \
    cargo install cargo-audit cargo-deb cargo-cyclonedx && \
    cp -v /tmp/cargo/bin/* /usr/local/bin && \
    rm -rf /tmp/cargo*

RUN export PIPX_HOME=/usr/local/pipx && \
    export PIPX_BIN_DIR=/usr/local/bin && \
    pipx install ruff && \
    rm -rf /root/.cache/pip
