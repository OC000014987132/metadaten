use std::env::args;

use anyhow::{anyhow, bail, ensure, Result};
use cap_std::{ambient_authority, fs::Dir};

use metadaten::{data_path_from_env, stats::Stats};

fn main() -> Result<()> {
    let args = args().collect::<Vec<_>>();
    ensure!(args.len() > 1, "Usage: {} dataset|term|tag ..", args[0]);
    let kind = &*args[1];

    let dir = Dir::open_ambient_dir(data_path_from_env(), ambient_authority())?;

    let mut stats = Stats::read(&dir)?;

    match kind {
        "dataset" => {
            ensure!(args.len() == 4, "Usage: {} dataset <source> <id>", args[0]);
            let source = &args[2];
            let id = &args[3];

            let _accesses = stats
                .accesses
                .get_mut(source)
                .ok_or_else(|| anyhow!("No such source: {source}"))?
                .remove(id)
                .ok_or_else(|| anyhow!("No such dataset: {id}"))?;
        }
        "tag" => {
            ensure!(args.len() == 3, "Usage: {} tag <tag>", args[0]);
            let tag = args[2].as_str();

            let old_len = stats.tags.len();
            stats.tags.retain(|tag1, _| tag1.to_string() != tag);
            let new_len = stats.tags.len();

            ensure!(old_len > new_len, "No such tag: {tag}");
        }
        kind => bail!("Unexpected kind: {kind}"),
    }

    stats.overwrite(&dir)?;

    Ok(())
}
