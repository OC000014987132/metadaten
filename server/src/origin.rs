use anyhow::anyhow;
use askama::Template;
use axum::{
    extract::{Path, State},
    response::Response,
};
use hashbrown::HashMap;
use serde::Serialize;
use utoipa::ToSchema;

use crate::{Accept, ServerError};
use metadaten::dataset::origin::Origin;

#[utoipa::path(
    get,
    path = "/origin/{id}",
    params(
        ("id", description = "Path to requested origin", example = "Bund/UBA"),
    ),
    responses(
        (status = 200, description = "Metadata of best-matching origin", content((OriginPage = "application/json"), (OriginPage = "text/html")))
    ),
)]
/// Retrieve backgound information on data providers
pub async fn origin(
    Path(id): Path<String>,
    accept: Accept,
    State(origins): State<&'static HashMap<String, Origin>>,
) -> Result<Response, ServerError> {
    let orig_id = &id;
    let mut id = id.as_str();

    let (id, origin) = loop {
        if let Some(origin) = origins.get(id) {
            break (id, origin);
        }

        if let Some(origin) = origins.values().find(|origin| {
            origin
                .pattern
                .as_ref()
                .is_some_and(|pattern| pattern.matches(orig_id))
        }) {
            break (origin.pattern.as_ref().unwrap().as_str(), origin);
        }

        id = match id.rsplit_once('/') {
            Some((prefix, _suffix)) => prefix,
            None => return Err(anyhow!("Origin {orig_id} not found").into()),
        };
    };

    let page = OriginPage { id, origin };

    Ok(accept.into_response(page))
}

#[derive(Template, Serialize, ToSchema)]
#[template(path = "origin.html")]
pub(super) struct OriginPage<'a> {
    /// Path to best-matching origin
    id: &'a str,
    origin: &'a Origin,
}
