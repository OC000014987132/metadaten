use std::cmp::Ordering;

use axum::{extract::State, Json};
use axum_extra::extract::Query;
use itertools::Itertools;
use ordered_float::NotNan;
use serde::{ser::Serializer, Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};

use crate::{Cache, ServerError};
use metadaten::{
    dataset::{LabelledTag, Tag, UniquelyIdentifiedDataset},
    index::Searcher,
};

#[derive(Deserialize, IntoParams)]
pub struct TopNParams {
    #[serde(default = "default_n")]
    n: usize,
}

fn default_n() -> usize {
    5
}

#[utoipa::path(
    get,
    path = "/top-n/datasets",
    params(TopNParams),
    responses(
        (status = 200, description = "The top n datasets by accesses", body = Vec<TopNDataset>),
    ),
)]
pub async fn datasets(
    State(cache): State<&'static Cache>,
    State(searcher): State<&'static Searcher>,
    Query(params): Query<TopNParams>,
) -> Result<Json<Vec<TopNDataset>>, ServerError> {
    if params.n > 100 {
        return Err(ServerError::BadRequest(
            "Number of top datasets must not be larger than 100",
        ));
    }

    let stats = cache.stats.load();

    let top_n = stats
        .accesses
        .iter()
        .flat_map(|(source, accesses)| {
            accesses
                .iter()
                .map(|(id, accesses)| (*accesses, (source.as_str(), id.as_str())))
        })
        .top_n(2 * params.n)
        .filter_map(|(accesses, (source, id))| {
            searcher
                .read(source, id)
                .map(|value| value.map(|value| TopNDataset { value, accesses }))
                .transpose()
        })
        .take(params.n)
        .collect::<Result<Vec<_>, _>>()?;

    Ok(Json(top_n))
}

#[derive(Serialize, ToSchema)]
pub struct TopNDataset {
    #[serde(flatten)]
    value: UniquelyIdentifiedDataset,
    accesses: u64,
}

#[utoipa::path(
    get,
    path = "/top-n/tags",
    params(TopNParams),
    responses(
        (status = 200, description = "The top n tags by queries", body = TopNTag),
    ),
)]
pub async fn tags(
    State(cache): State<&'static Cache>,
    Query(params): Query<TopNParams>,
) -> Result<Json<Vec<TopNTag>>, ServerError> {
    if params.n > 100 {
        return Err(ServerError::BadRequest(
            "Number of top tags must not be larger than 100",
        ));
    }

    let stats = cache.stats.load();
    let metrics = cache.metrics.load();

    let top_n = stats
        .tags
        .iter()
        .filter_map(|(tag, &queries)| {
            if tag.is_other() {
                return None;
            }

            let datasets = metrics.tags.get(tag).copied().unwrap_or(0);

            if datasets < 100 {
                return None;
            }

            let score = NotNan::new(queries as f32 / datasets as f32).expect("Invalid tag score");

            Some((score, (tag, queries, datasets)))
        })
        .top_n(params.n)
        .map(|(score, (tag, queries, datasets))| TopNTag {
            tag: tag.clone(),
            queries,
            datasets,
            score: score.into_inner(),
        })
        .collect();

    Ok(Json(top_n))
}

#[derive(Serialize, ToSchema)]
pub struct TopNTag {
    #[serde(serialize_with = "tag_with_label")]
    #[schema(value_type = [LabelledTag])]
    tag: Tag,
    queries: u64,
    datasets: usize,
    score: f32,
}

fn tag_with_label<S>(tag: &Tag, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    tag.with_label(|tag| tag.serialize(serializer))
}

pub trait TopNExt {
    type Item;

    fn top_n(self, n: usize) -> impl Iterator<Item = Self::Item>;
}

impl<I, K, V> TopNExt for I
where
    I: IntoIterator<Item = (K, V)>,
    K: Ord,
{
    type Item = (K, V);

    fn top_n(self, n: usize) -> impl Iterator<Item = Self::Item> {
        struct Item<K, V>(K, V);

        impl<K, V> PartialEq for Item<K, V>
        where
            K: Eq,
        {
            fn eq(&self, other: &Self) -> bool {
                self.0 == other.0
            }
        }

        impl<K, V> Eq for Item<K, V> where K: Eq {}

        impl<K, V> PartialOrd for Item<K, V>
        where
            K: Ord,
        {
            fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
                Some(self.cmp(other))
            }
        }

        impl<K, V> Ord for Item<K, V>
        where
            K: Ord,
        {
            fn cmp(&self, other: &Self) -> Ordering {
                // Note that `other` and `self` are swapped,
                // i.e. we sort by key, but in descending order.
                other.0.cmp(&self.0)
            }
        }

        self.into_iter()
            .map(|(key, val)| Item(key, val))
            .k_smallest(n)
            .map(|Item(key, val)| (key, val))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn top_n_works() {
        assert_eq!(
            vec![
                (10, 1),
                (1, 2),
                (9, 3),
                (2, 4),
                (8, 5),
                (3, 6),
                (7, 7),
                (4, 8),
                (6, 9),
                (5, 10)
            ]
            .top_n(5)
            .collect::<Vec<_>>(),
            vec![(10, 1), (9, 3), (8, 5), (7, 7), (6, 9)]
        );
    }
}
