use std::fmt::Display;
use std::sync::Arc;

use askama::Template;
use axum::{
    extract::State,
    response::{IntoResponse, Response},
    Json,
};
use axum_extra::extract::Query;
use compact_str::CompactString;
use geo::{Coord, Rect};
use parking_lot::Mutex;
use reqwest::Client;
use serde::{Deserialize, Serialize};
use tantivy::schema::Facet;
use time::Date;
use tokio::task::spawn_blocking;
use utoipa::{IntoParams, ToSchema};

use crate::{filters, into_ndjson, prometheus::Prometheus, Accept, Cache, ServerError};
use metadaten::{
    dataset::{ScoredDataset, TimeRange, UniquelyIdentifiedDataset},
    index::{QueryRepr, Searcher, SpatialCluster, StructQuery, TextQuery},
    stats::Stats,
    umthes::similar_terms::fetch_similar_terms,
    FacetWeight,
};

#[utoipa::path(
    get,
    path = "/search",
    params(SearchParams),
    responses(
        (status = 200, description = "Matching datasets and facets", content((SearchPage<TextQuery> = "application/json"), (SearchPage<TextQuery> = "text/html"))),
    ),
)]
/// Search for multiple datasets
pub async fn text_search(
    State(searcher): State<&'static Searcher>,
    State(client): State<&'static Client>,
    State(cache): State<&'static Cache>,
    State(stats): State<&'static Mutex<Stats>>,
    State(prometheus): State<&'static Prometheus>,
    accept: Accept,
    Query(params): Query<SearchParams<TextQuery>>,
) -> Result<Response, ServerError> {
    search_impl(searcher, client, cache, stats, prometheus, accept, params).await
}

#[utoipa::path(
    post,
    path = "/search",
    request_body = SearchParams<StructQuery>,
    responses(
        (status = 200, description = "Matching datasets and facets", content((SearchPage<StructQuery> = "application/json"), (SearchPage<StructQuery> = "text/html"))),
    ),
)]
/// Search for multiple datasets
pub async fn struct_search(
    State(searcher): State<&'static Searcher>,
    State(client): State<&'static Client>,
    State(cache): State<&'static Cache>,
    State(stats): State<&'static Mutex<Stats>>,
    State(prometheus): State<&'static Prometheus>,
    accept: Accept,
    Json(params): Json<SearchParams<StructQuery>>,
) -> Result<Response, ServerError> {
    search_impl(searcher, client, cache, stats, prometheus, accept, params).await
}

async fn search_impl<Q>(
    searcher: &'static Searcher,
    client: &'static Client,
    cache: &'static Cache,
    stats: &'static Mutex<Stats>,
    prometheus: &'static Prometheus,
    accept: Accept,
    params: SearchParams<Q>,
) -> Result<Response, ServerError>
where
    Q: QueryRepr + Serialize + Display + Send + 'static,
{
    fn inner<Q>(
        mut params: SearchParams<Q>,
        searcher: &Searcher,
        cache: &Cache,
        stats: &Mutex<Stats>,
    ) -> Result<SearchPage<Q>, ServerError>
    where
        Q: QueryRepr + Serialize + Display,
    {
        if params.page == 0 || params.results_per_page == 0 {
            return Err(ServerError::BadRequest(
                "Page and results per page must not be zero",
            ));
        }

        if params.results_per_page > 100 {
            return Err(ServerError::BadRequest(
                "Results per page must not be larger than 100",
            ));
        }

        let cached_stats = cache.stats.load();

        let bounding_box = params.bounding_box();
        let time_range = params.time_range();

        let mut results = searcher.search(
            &cached_stats,
            &params.query,
            &params.types_root,
            &params.topics_root,
            &params.origins_root,
            &params.licenses_root,
            &params.languages_root,
            &params.resource_types_root,
            bounding_box,
            params.bounding_box_contains,
            params.bounding_box_spatial_clusters,
            time_range,
            &params.origin_weights,
            params.results_per_page,
            (params.page - 1) * params.results_per_page,
        )?;

        let pages = results.count.div_ceil(params.results_per_page);

        if params.page > pages && pages > 0 {
            tracing::debug!(
                "Page {} is larger than {} and will be clamped",
                params.page,
                pages
            );

            params.page = pages;

            results = searcher.search(
                &cached_stats,
                &params.query,
                &params.types_root,
                &params.topics_root,
                &params.origins_root,
                &params.licenses_root,
                &params.languages_root,
                &params.resource_types_root,
                bounding_box,
                params.bounding_box_contains,
                params.bounding_box_spatial_clusters,
                time_range,
                &params.origin_weights,
                params.results_per_page,
                (params.page - 1) * params.results_per_page,
            )?;
        }

        tracing::debug!("Found {} documents", results.count);

        stats
            .lock()
            .record_query(&results.terms, &params.topics_root, &results.results);

        let page = SearchPage {
            params,
            count: results.count,
            pages,
            results: results.results,
            types: results.types,
            topics: results.topics,
            origins: results.origins,
            licenses: results.licenses,
            languages: results.languages,
            resource_types: results.resource_types,
            spatial_clusters: results.spatial_clusters,
            terms: results.terms,
            similar_terms: results.similar_terms,
            related_terms: Vec::new(),
        };

        Ok(page)
    }

    let mut page = spawn_blocking(move || inner(params, searcher, cache, stats)).await??;

    page.related_terms = fetch_similar_terms(
        client,
        &prometheus.similar_terms_request_duration,
        &page.terms,
    )
    .await;

    Ok(accept.into_response(page))
}

#[derive(Deserialize, Serialize, ToSchema, IntoParams)]
pub struct SearchParams<Q> {
    #[serde(default)]
    #[param(value_type = String)]
    /// Query following the syntax described in <https://docs.rs/tantivy/latest/tantivy/query/struct.QueryParser.html>
    query: Q,
    #[serde(default = "default_root")]
    #[schema(value_type = String)]
    #[param(value_type = String)]
    /// Entry point for faceted search of types
    types_root: Facet,
    #[serde(default = "default_root")]
    #[schema(value_type = String)]
    #[param(value_type = String)]
    /// Entry point for faceted search of tags
    topics_root: Facet,
    #[serde(default = "default_root")]
    #[schema(value_type = String)]
    #[param(value_type = String)]
    /// Entry point for faceted search of origins
    origins_root: Facet,
    #[serde(default = "default_root")]
    #[schema(value_type = String)]
    #[param(value_type = String)]
    /// Entry point for faceted search of licenses
    licenses_root: Facet,
    #[serde(default = "default_root")]
    #[schema(value_type = String)]
    #[param(value_type = String)]
    /// Entry point for faceted search of languages
    languages_root: Facet,
    #[serde(default = "default_root")]
    #[schema(value_type = String)]
    #[param(value_type = String)]
    /// Entry point for faceted search of resources types
    resource_types_root: Facet,
    #[serde(default = "default_page")]
    #[schema(minimum = 0)]
    #[param(minimum = 0)]
    /// Return results from the given page
    page: usize,
    #[serde(default = "default_results_per_page")]
    #[schema(minimum = 1, maximum = 100)]
    #[param(minimum = 1, maximum = 100)]
    /// Number of results per page
    results_per_page: usize,
    #[schema(minimum = -180.0, maximum = 180.0)]
    #[param(minimum = -180.0, maximum = 180.0)]
    /// Minimum longitude of results using WGS84
    bounding_box_west: Option<f64>,
    #[schema(minimum = -90.0, maximum = 90.0)]
    #[param(minimum = -90.0, maximum = 90.0)]
    /// Minimum latitude of results using WGS84
    bounding_box_south: Option<f64>,
    #[schema(minimum = -180.0, maximum = 180.0)]
    #[param(minimum = -180.0, maximum = 180.0)]
    /// Maximum longitude of results using WGS84
    bounding_box_east: Option<f64>,
    #[schema(minimum = -90.0, maximum = 90.0)]
    #[param(minimum = -90.0, maximum = 90.0)]
    /// Maximum latitude of results using WGS84
    bounding_box_north: Option<f64>,
    #[serde(default)]
    /// Whether the bounding box must fully contain instead of just intersect the bounding box of the dataset
    bounding_box_contains: bool,
    #[serde(default)]
    /// Whether to aggregate the results within the given bounding box into spatial clusters
    bounding_box_spatial_clusters: bool,
    /// Lower bound of time range
    #[serde(default)]
    time_range_from: Option<Date>,
    /// Upper bound of time range
    #[serde(default)]
    time_range_until: Option<Date>,
    /// Enables selectively weighing the given origins
    ///
    /// This can be given multiple times and takes the form `<facet>^<weight>`
    /// where `<facet>` can be a prefix, so for example
    ///
    /// ```text
    /// &origin_weights=/Bund^0.5&origin_weights=/Land^2.0
    /// ```
    ///
    /// will half the score of all results with origins starting with `/Bund`
    /// and double the score of all results with origins starting with `/Land`.
    #[serde(default)]
    #[schema(value_type = [String])]
    #[param(value_type = [String])]
    origin_weights: Vec<FacetWeight>,
}

fn default_root() -> Facet {
    Facet::root()
}

fn default_page() -> usize {
    1
}

fn default_results_per_page() -> usize {
    10
}

impl<Q> SearchParams<Q> {
    fn bounding_box(&self) -> Option<Rect> {
        if self.bounding_box_west.is_none()
            && self.bounding_box_south.is_none()
            && self.bounding_box_east.is_none()
            && self.bounding_box_north.is_none()
        {
            return None;
        }

        Some(Rect::new(
            Coord {
                x: self.bounding_box_west.unwrap_or(-180.0),
                y: self.bounding_box_south.unwrap_or(-90.0),
            },
            Coord {
                x: self.bounding_box_east.unwrap_or(180.0),
                y: self.bounding_box_north.unwrap_or(90.0),
            },
        ))
    }

    fn time_range(&self) -> Option<TimeRange> {
        if self.time_range_from.is_none() && self.time_range_until.is_none() {
            return None;
        }

        let from = self.time_range_from.unwrap_or(Date::MIN);
        let until = self.time_range_until.unwrap_or(Date::MAX);

        Some(TimeRange { from, until })
    }
}

#[derive(Template, Serialize, ToSchema)]
#[template(path = "search.html")]
pub(super) struct SearchPage<Q>
where
    Q: Display,
{
    #[serde(skip_serializing)]
    params: SearchParams<Q>,
    #[schema(minimum = 0)]
    /// Total number of results found
    count: usize,
    #[schema(minimum = 0)]
    /// Total number of pages
    pages: usize,
    results: Vec<ScoredDataset>,
    /// Hierarchical grouping of types
    #[schema(value_type = [(String, u64)])]
    types: Vec<(Facet, u64)>,
    /// Hierarchical grouping of tags
    #[schema(value_type = [(String, u64)])]
    topics: Vec<(Facet, u64)>,
    #[schema(value_type = [(String, u64)])]
    /// Hierarchical grouping of origin
    origins: Vec<(Facet, u64)>,
    #[schema(value_type = [(String, u64)])]
    /// Hierarchical grouping of licenses
    licenses: Vec<(Facet, u64)>,
    #[schema(value_type = [(String, u64)])]
    /// Hierarchical grouping of languages
    languages: Vec<(Facet, u64)>,
    #[schema(value_type = [(String, u64)])]
    /// Hierarchical grouping of resource types
    resource_types: Vec<(Facet, u64)>,
    spatial_clusters: Vec<SpatialCluster>,
    #[schema(value_type = [String])]
    terms: Vec<CompactString>,
    #[schema(value_type = [(String, String)])]
    similar_terms: Vec<(CompactString, CompactString)>,
    #[schema(value_type = [(String, [String])])]
    related_terms: Vec<(CompactString, Arc<[String]>)>,
}

impl<Q> SearchPage<Q>
where
    Q: Display,
{
    fn pages(&self) -> Vec<usize> {
        let mut pages = Vec::new();

        pages.extend(1..=self.pages.min(5));

        let mut extend = |new_pages| {
            for new_page in new_pages {
                let last_page = *pages.last().unwrap();

                if last_page < new_page {
                    if last_page + 1 != new_page {
                        pages.push(0);
                    }

                    pages.push(new_page);
                }
            }
        };

        if self.params.page > 2 {
            extend(self.params.page - 2..=self.pages.min(self.params.page + 2))
        }

        if self.pages > 2 {
            extend(self.pages - 2..=self.pages);
        }

        pages
    }
}

#[utoipa::path(
    get,
    path = "/complete",
    params(CompleteParams),
    responses(
        (status = 200, description = "Available completions", body = CompletePage),
    ),
)]
/// Autocompletions for a given query
///
/// This will yield completions for last term in the given query,
/// but only if has at least three characters.
pub async fn text_complete(
    State(searcher): State<&'static Searcher>,
    State(cache): State<&'static Cache>,
    Query(params): Query<CompleteParams<TextQuery>>,
) -> Result<Json<CompletePage>, ServerError> {
    complete_impl(searcher, cache, params)
}

#[utoipa::path(
    post,
    path = "/complete",
    request_body = CompleteParams<StructQuery>,
    responses(
        (status = 200, description = "Available completions", body = CompletePage),
    ),
)]
/// Autocompletions for a given query
///
/// This will yield completions for last term in the given query,
/// but only if has at least three characters.
pub async fn struct_complete(
    State(searcher): State<&'static Searcher>,
    State(cache): State<&'static Cache>,
    Json(params): Json<CompleteParams<StructQuery>>,
) -> Result<Json<CompletePage>, ServerError> {
    complete_impl(searcher, cache, params)
}

fn complete_impl<Q>(
    searcher: &'static Searcher,
    cache: &'static Cache,
    mut params: CompleteParams<Q>,
) -> Result<Json<CompletePage>, ServerError>
where
    Q: QueryRepr,
{
    let results = searcher.complete(&cache.stats.load(), &mut params.query)?;

    let page = CompletePage {
        completions: results.completions,
        similar_terms: results.similar_terms,
        types: results.types,
        topics: results.topics,
        origins: results.origins,
        licenses: results.licenses,
        languages: results.languages,
        resource_types: results.resource_types,
    };

    Ok(Json(page))
}

#[derive(Deserialize, ToSchema, IntoParams)]
pub struct CompleteParams<Q> {
    #[param(value_type = String)]
    query: Q,
}

#[derive(Serialize, ToSchema)]
pub struct CompletePage {
    /// Suffixes which complete to matching terms, for the given or default fields
    ///
    /// Attached to each term is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    completions: Vec<(CompactString, usize)>,
    /// Terms similar to the given terms, even if they are not suffixes
    ///
    /// Attached to each term is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    similar_terms: Vec<(CompactString, u64)>,
    /// Types which contain the given term
    ///
    /// Attached to each value is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    types: Vec<(Facet, usize)>,
    /// Topics which contain the given term
    ///
    /// Attached to each value is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    topics: Vec<(Facet, usize)>,
    /// Origins which contain the given term
    ///
    /// Attached to each value is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    origins: Vec<(Facet, usize)>,
    /// Licenses which contain the given term
    ///
    /// Attached to each value is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    licenses: Vec<(Facet, usize)>,
    /// Languages which contain the given term
    ///
    /// Attached to each value is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    languages: Vec<(Facet, usize)>,
    /// Resource types which contain the given term
    ///
    /// Attached to each value is the number of matching datasets.
    #[schema(value_type = [(String, usize)])]
    resource_types: Vec<(Facet, usize)>,
}

#[utoipa::path(
    get,
    path = "/search/all",
    params(SearchAllParams),
    responses(
        (status = 200, description = "Stream of matching datasets", body = UniquelyIdentifiedDataset, content_type = "application/x-ndjson")
    ),
)]
/// Efficiently stream all datasets matching a given query in a single request
pub async fn text_search_all(
    State(searcher): State<&'static Searcher>,
    Query(params): Query<SearchAllParams<TextQuery>>,
) -> Result<impl IntoResponse, ServerError> {
    search_all_impl(searcher, params)
}

#[utoipa::path(
    post,
    path = "/search/all",
    request_body = SearchAllParams<StructQuery>,
    responses(
        (status = 200, description = "Stream of matching datasets", body = UniquelyIdentifiedDataset, content_type = "application/x-ndjson")
    ),
)]
/// Efficiently stream all datasets matching a given query in a single request
pub async fn struct_search_all(
    State(searcher): State<&'static Searcher>,
    Json(params): Json<SearchAllParams<StructQuery>>,
) -> impl IntoResponse {
    search_all_impl(searcher, params)
}

fn search_all_impl<Q>(
    searcher: &'static Searcher,
    params: SearchAllParams<Q>,
) -> Result<impl IntoResponse, ServerError>
where
    Q: QueryRepr,
{
    searcher
        .search_all(
            &params.query,
            params.bounding_box(),
            params.bounding_box_contains,
            params.time_range(),
            params.sampling_fraction,
        )
        .map(into_ndjson)
        .map_err(Into::into)
}

#[derive(Deserialize, ToSchema, IntoParams)]
pub struct SearchAllParams<Q> {
    #[param(value_type = String)]
    query: Q,
    #[schema(minimum = -180.0, maximum = 180.0)]
    #[param(minimum = -180.0, maximum = 180.0)]
    /// Minimum longitude of results using WGS84
    bounding_box_west: Option<f64>,
    #[schema(minimum = -90.0, maximum = 90.0)]
    #[param(minimum = -90.0, maximum = 90.0)]
    /// Minimum latitude of results using WGS84
    bounding_box_south: Option<f64>,
    #[schema(minimum = -180.0, maximum = 180.0)]
    #[param(minimum = -180.0, maximum = 180.0)]
    /// Maximum longitude of results using WGS84
    bounding_box_east: Option<f64>,
    #[schema(minimum = -90.0, maximum = 90.0)]
    #[param(minimum = -90.0, maximum = 90.0)]
    /// Maximum latitude of results using WGS84
    bounding_box_north: Option<f64>,
    #[serde(default)]
    /// Whether the bounding box must fully contain instead of just intersect the bounding box of the dataset
    bounding_box_contains: bool,
    /// Lower bound of time range
    #[serde(default)]
    time_range_from: Option<Date>,
    /// Upper bound of time range
    #[serde(default)]
    time_range_until: Option<Date>,
    /// If set, randomly yields only the given fraction of results.
    sampling_fraction: Option<f32>,
}

impl<Q> SearchAllParams<Q> {
    fn bounding_box(&self) -> Option<Rect> {
        if self.bounding_box_west.is_none()
            && self.bounding_box_south.is_none()
            && self.bounding_box_east.is_none()
            && self.bounding_box_north.is_none()
        {
            return None;
        }

        Some(Rect::new(
            Coord {
                x: self.bounding_box_west.unwrap_or(-180.0),
                y: self.bounding_box_south.unwrap_or(-90.0),
            },
            Coord {
                x: self.bounding_box_east.unwrap_or(180.0),
                y: self.bounding_box_north.unwrap_or(90.0),
            },
        ))
    }

    fn time_range(&self) -> Option<TimeRange> {
        if self.time_range_from.is_none() && self.time_range_until.is_none() {
            return None;
        }

        let from = self.time_range_from.unwrap_or(Date::MIN);
        let until = self.time_range_until.unwrap_or(Date::MAX);

        Some(TimeRange { from, until })
    }
}
