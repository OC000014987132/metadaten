use std::env::{var, var_os};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::sync::Arc;

use anyhow::Error;
use axum::{
    middleware::from_fn_with_state as middleware_from_fn,
    response::Redirect,
    routing::{get, get_service},
    serve, Router,
};
use cap_std::{ambient_authority, fs::Dir};
use parking_lot::Mutex;
use reqwest::Client;
use tokio::{
    net::TcpListener,
    task::{block_in_place, spawn, spawn_blocking},
    time::{interval_at, Duration, Instant, MissedTickBehavior},
};
use tower_http::{compression::CompressionLayer, services::ServeDir};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use metadaten::{
    data_path_from_env, dataset::origin::load_origins, index::Searcher, metrics::Metrics,
    stats::Stats,
};
use server::{
    api_doc,
    ckan::ckan_routes,
    counts, dataset,
    metrics::metrics_routes,
    origin::origin,
    prometheus::{measure_routes, render_measurements},
    search::{
        struct_complete, struct_search, struct_search_all, text_complete, text_search,
        text_search_all,
    },
    top_n, State,
};

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::from_default_env())
        .with(tracing_subscriber::fmt::layer().without_time())
        .init();

    let data_path = data_path_from_env();

    let assets_path = var_os("ASSETS_PATH").expect("Environment variable ASSETS_PATH not set");

    let bind_addr = var("BIND_ADDR")
        .expect("Environment variable BIND_ADDR not set")
        .parse::<SocketAddr>()
        .expect("Environment variable BIND_ADDR invalid");

    let searcher = Searcher::open(&data_path)?;

    let client = Client::builder()
        .user_agent("umwelt.info server")
        .timeout(Duration::from_secs(1))
        .build()?;

    let dir = Dir::open_ambient_dir(&data_path, ambient_authority())?;

    let stats = Mutex::new(Stats::read(&dir)?);

    let cache = Default::default();

    let prometheus = Default::default();

    let origins = load_origins().unwrap_or_else(|err| {
        tracing::error!("Failed to load origins: {:#}", err);

        Default::default()
    });

    let state = &*Box::leak(Box::new(State {
        searcher,
        client,
        dir,
        stats,
        cache,
        prometheus,
        origins,
    }));

    spawn(write_stats(state));

    spawn(update_cache(state));

    spawn(refresh_tokenizers(state, data_path));

    let router = Router::new()
        .route("/search", get(text_search).post(struct_search))
        .route("/complete", get(text_complete).post(struct_complete))
        .route("/search/all", get(text_search_all).post(struct_search_all))
        .route("/dataset/{source}/{id}", get(dataset::single))
        .route("/dataset/{source}/{id}/redirect", get(dataset::redirect))
        .route("/dataset/all", get(dataset::all))
        .route("/dataset/random", get(dataset::random))
        .route("/origin/{*id}", get(origin))
        .route("/top-n/datasets", get(top_n::datasets))
        .route("/top-n/tags", get(top_n::tags))
        .route("/counts/now", get(counts::now))
        .route("/counts/all", get(counts::all))
        .nest("/metrics", metrics_routes())
        .nest("/ckan/api/3/action", ckan_routes())
        .nest("/ckan/api/action", ckan_routes())
        .route_layer(middleware_from_fn(&state.prometheus, measure_routes))
        .route("/prometheus", get(render_measurements))
        .with_state(state)
        .route("/", get(|| async { Redirect::permanent("/search") }))
        .route("/openapi.json", get(api_doc))
        .fallback_service(get_service(ServeDir::new(assets_path).precompressed_gzip()))
        .layer(CompressionLayer::new());

    tracing::info!("Listening on {}", bind_addr);

    let listener = TcpListener::bind(bind_addr).await?;
    serve(listener, router).await?;

    Ok(())
}

async fn write_stats(state: &'static State) {
    let mut interval = interval_at(
        Instant::now() + Duration::from_secs(30),
        Duration::from_secs(60),
    );
    interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

    loop {
        interval.tick().await;

        spawn_blocking(|| {
            let metrics = state.cache.metrics.load();

            if let Err(err) = Stats::write(&state.stats, &state.dir, &metrics) {
                tracing::warn!("Failed to write stats: {:#}", err);
            }
        })
        .await
        .unwrap();
    }
}

async fn update_cache(state: &'static State) {
    let mut interval = interval_at(Instant::now(), Duration::from_secs(60));
    interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

    loop {
        interval.tick().await;

        spawn_blocking(|| {
            match Stats::read(&state.dir) {
                Ok(stats) => state.cache.stats.store(Arc::new(stats)),
                Err(err) => tracing::warn!("Failed to read stats: {:#}", err),
            }

            match Metrics::read(&state.dir) {
                Ok(metrics) => state.cache.metrics.store(Arc::new(metrics)),
                Err(err) => tracing::warn!("Failed to read metrics: {:#}", err),
            }
        })
        .await
        .unwrap();
    }
}

async fn refresh_tokenizers(state: &'static State, data_path: PathBuf) {
    let mut interval = interval_at(
        Instant::now() + Duration::from_secs(24 * 60 * 60),
        Duration::from_secs(24 * 60 * 60),
    );
    interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

    loop {
        interval.tick().await;

        if let Err(err) = block_in_place(|| state.searcher.register_tokenizers(&data_path)) {
            tracing::error!("Failed to refresh tokenizers: {err:#}");
        }
    }
}
