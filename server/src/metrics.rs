use std::cmp::Reverse;
use std::time::{Duration, SystemTime};

use anyhow::{ensure, Result};
use askama::Template;
use axum::{extract::State, response::Html, routing::get, Router};
use hashbrown::{HashMap, HashSet};
use sketches_ddsketch::DDSketch;
use string_cache::DefaultAtom;

use crate::{filters, top_n::TopNExt, Cache, ServerError, State as ServerState};
use metadaten::{
    dataset::{License, OrganisationKey, Region, ResourceType, Tag},
    metrics::Harvest as HarvestMetrics,
};

pub fn metrics_routes() -> Router<&'static ServerState> {
    Router::new()
        .route("/accesses", get(accesses))
        .route("/queried_tags", get(queried_tags))
        .route("/harvests", get(harvests))
        .route("/licenses", get(licenses))
        .route("/resource-types", get(resource_types))
        .route("/harvested_tags", get(harvested_tags))
        .route("/regions", get(regions))
        .route("/organisations", get(organisations))
        .route("/quality", get(quality))
        .route("/alternatives", get(alternatives))
}

async fn accesses(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let stats = cache.stats.load();

    let top_10 = stats
        .accesses
        .iter()
        .flat_map(|(source_name, accesses)| {
            accesses.iter().map(|(dataset_id, accesses)| {
                (*accesses, (source_name.as_str(), dataset_id.as_str()))
            })
        })
        .top_n(10)
        .map(|(accesses, (source_name, dataset_id))| (source_name, dataset_id, accesses))
        .collect();

    let mut accesses = stats
        .accesses
        .iter()
        .map(|(source_name, accesses)| (source_name.as_str(), accesses.values().sum()))
        .collect::<Vec<_>>();

    accesses.sort_unstable_by_key(|(_, accesses)| Reverse(*accesses));

    let sum = accesses.iter().map(|(_, accesses)| accesses).sum();

    let page = AccessesPage {
        accesses,
        top_10,
        sum,
    };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/accesses.html")]
struct AccessesPage<'a> {
    accesses: Vec<(&'a str, u64)>,
    top_10: Vec<(&'a str, &'a str, u64)>,
    sum: u64,
}

async fn queried_tags(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let stats = cache.stats.load();

    let mut tags = stats
        .tags
        .iter()
        .map(|(tag, queries)| (tag, *queries))
        .collect::<Vec<_>>();

    tags.sort_unstable_by_key(|(_, queries)| Reverse(*queries));

    let sum = tags.iter().map(|(_, queries)| queries).sum();

    let median = tags.get(tags.len() / 2).map_or(0, |(_, queries)| *queries);

    let page = QueriedTagsPage { tags, sum, median };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/queried_tags.html")]
struct QueriedTagsPage<'a> {
    tags: Vec<(&'a Tag, u64)>,
    sum: u64,
    median: u64,
}

async fn harvests(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let mut harvests = metrics
        .harvests
        .iter()
        .map(|(source_name, harvest)| (source_name.as_ref(), harvest))
        .collect::<Vec<_>>();

    harvests
        .sort_unstable_by_key(|(_, harvest)| (Reverse(harvest.failed), Reverse(harvest.duration)));

    let (sum_deleted, sum_count, sum_transmitted, sum_failed) = harvests.iter().fold(
        (0, 0, 0, 0),
        |(sum_deleted, sum_count, sum_transmitted, sum_failed), (_, harvest)| {
            (
                sum_deleted + harvest.deleted,
                sum_count + harvest.count,
                sum_transmitted + harvest.transmitted,
                sum_failed + harvest.failed,
            )
        },
    );

    let page = HarvestsPage {
        last_harvest: metrics.last_harvest,
        harvests,
        sum_deleted,
        sum_count,
        sum_transmitted,
        sum_failed,
        failed_harvests: &metrics.failed_harvests,
        datasets: metrics.datasets,
        sources: metrics.sources.len(),
        providers: metrics.providers.len(),
    };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/harvests.html")]
struct HarvestsPage<'a> {
    last_harvest: Option<(SystemTime, Duration)>,
    harvests: Vec<(&'a str, &'a HarvestMetrics)>,
    sum_deleted: usize,
    sum_count: usize,
    sum_transmitted: usize,
    sum_failed: usize,
    failed_harvests: &'a HashSet<DefaultAtom>,
    datasets: usize,
    sources: usize,
    providers: usize,
}

async fn licenses(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let mut licenses_by_source = metrics
        .licenses
        .iter()
        .map(|(source, licenses)| {
            let (count, unknown) =
                licenses
                    .iter()
                    .fold((0, 0), |(mut count, mut unknown), (license, count1)| {
                        count += count1;

                        if license == &License::Unknown {
                            unknown += count1;
                        }

                        (count, unknown)
                    });

            (source.as_ref(), unknown as f64 / count as f64)
        })
        .collect::<Vec<_>>();

    licenses_by_source
        .sort_unstable_by(|(_, lhs_unknown), (_, rhs_unknown)| rhs_unknown.total_cmp(lhs_unknown));

    let mut licenses = metrics
        .licenses
        .iter()
        .fold(
            HashMap::<&License, usize>::new(),
            |mut licenses, (_, licenses1)| {
                for (license, count) in licenses1 {
                    *licenses.entry(license).or_default() += count;
                }

                licenses
            },
        )
        .into_iter()
        .collect::<Vec<_>>();

    licenses.sort_unstable_by_key(|(_, count)| Reverse(*count));

    let page = LicensesPage {
        licenses,
        licenses_by_source,
    };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/licenses.html")]
struct LicensesPage<'a> {
    licenses: Vec<(&'a License, usize)>,
    licenses_by_source: Vec<(&'a str, f64)>,
}

async fn resource_types(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let mut resource_types_by_source = metrics
        .resource_types
        .iter()
        .map(|(source, resource_types)| {
            let (count, unknown) = resource_types.iter().fold(
                (0, 0),
                |(mut count, mut unknown), (resource_type, count1)| {
                    count += count1;

                    if resource_type == &ResourceType::Unknown {
                        unknown += count1
                    }

                    (count, unknown)
                },
            );

            (source.as_ref(), unknown as f64 / count as f64)
        })
        .collect::<Vec<_>>();

    resource_types_by_source
        .sort_unstable_by(|(_, lhs_unknown), (_, rhs_unknown)| rhs_unknown.total_cmp(lhs_unknown));

    let mut resource_types = metrics
        .resource_types
        .iter()
        .fold(
            HashMap::<&ResourceType, usize>::new(),
            |mut resource_types, (_, resource_types1)| {
                for (resource_type, count) in resource_types1 {
                    *resource_types.entry(resource_type).or_default() += count;
                }

                resource_types
            },
        )
        .into_iter()
        .collect::<Vec<_>>();

    resource_types.sort_unstable_by_key(|(_, count)| Reverse(*count));

    let page = ResourceTypesPage {
        resource_types,
        resource_types_by_source,
    };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/resource-types.html")]
struct ResourceTypesPage<'a> {
    resource_types: Vec<(&'a ResourceType, usize)>,
    resource_types_by_source: Vec<(&'a str, f64)>,
}

async fn harvested_tags(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let mut tags = metrics
        .tags
        .iter()
        .map(|(tag, count)| (tag, *count))
        .collect::<Vec<_>>();

    tags.sort_unstable_by_key(|(_, count)| Reverse(*count));

    let (sum_other, cnt_other) = tags
        .iter()
        .filter(|(tag, _)| matches!(tag, Tag::Other(_)))
        .fold((0, 0), |(mut sum_other, mut cnt_other), (_, count)| {
            sum_other += count;
            cnt_other += 1;

            (sum_other, cnt_other)
        });

    let page = HarvestedTagsPage {
        tags,
        sum_other,
        cnt_other,
    };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/harvested_tags.html")]
struct HarvestedTagsPage<'a> {
    tags: Vec<(&'a Tag, usize)>,
    sum_other: usize,
    cnt_other: usize,
}

async fn regions(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let mut regions = metrics
        .regions
        .iter()
        .map(|(region, count)| (region, *count))
        .collect::<Vec<_>>();

    regions.sort_unstable_by_key(|(_, count)| Reverse(*count));

    let (sum_other, cnt_other) = regions
        .iter()
        .filter(|(region, _)| matches!(region, Region::Other(_)))
        .fold((0, 0), |(mut sum_other, mut cnt_other), (_, count)| {
            sum_other += count;
            cnt_other += 1;

            (sum_other, cnt_other)
        });

    let page = RegionsPage {
        regions,
        sum_other,
        cnt_other,
    };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/regions.html")]
struct RegionsPage<'a> {
    regions: Vec<(&'a Region, usize)>,
    sum_other: usize,
    cnt_other: usize,
}

async fn organisations(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let mut organisations = metrics
        .organisations
        .iter()
        .map(|(organisation, count)| (organisation, *count))
        .collect::<Vec<_>>();

    organisations.sort_unstable_by_key(|(_, count)| Reverse(*count));

    let (sum_other, cnt_other) = organisations
        .iter()
        .filter(|(region, _)| matches!(region, OrganisationKey::Other(_)))
        .fold((0, 0), |(mut sum_other, mut cnt_other), (_, count)| {
            sum_other += count;
            cnt_other += 1;

            (sum_other, cnt_other)
        });

    let page = OrganisationsPage {
        organisations,
        sum_other,
        cnt_other,
    };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/organisations.html")]
struct OrganisationsPage<'a> {
    organisations: Vec<(&'a OrganisationKey, usize)>,
    sum_other: usize,
    cnt_other: usize,
}

async fn quality(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let compute_summary = |name, sketch: &DDSketch| -> Result<_> {
        ensure!(sketch.count() != 0, "Sketch {name} is empty");

        let min = sketch.min().unwrap();
        let med = sketch.quantile(0.5)?.unwrap();
        let avg = sketch.sum().unwrap() / sketch.count() as f64;
        let max = sketch.max().unwrap();

        Ok((name, min, med, avg, max))
    };

    let scores = [
        compute_summary("Overall", &metrics.quality.overall)?,
        compute_summary("Accessibility", &metrics.quality.accessibility)?,
        compute_summary("Findability", &metrics.quality.findability)?,
        compute_summary("Interoperabiltiy", &metrics.quality.interoperabiltiy)?,
        compute_summary("Reusability", &metrics.quality.reusability)?,
    ];

    let page = QualityPage { scores };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/quality.html")]
struct QualityPage {
    scores: [(&'static str, f64, f64, f64, f64); 5],
}

async fn alternatives(State(cache): State<&'static Cache>) -> Result<Html<String>, ServerError> {
    let metrics = cache.metrics.load();

    let mut frequencies = metrics
        .alternatives
        .iter()
        .map(|(count, freq)| (*count, *freq))
        .collect::<Vec<_>>();

    frequencies.sort_unstable_by_key(|(count, _)| *count);

    let page = AlternativesPage { frequencies };

    let page = Html(page.render().unwrap());

    Ok(page)
}

#[derive(Template)]
#[template(path = "metrics/alternatives.html")]
struct AlternativesPage {
    frequencies: Vec<(usize, usize)>,
}
