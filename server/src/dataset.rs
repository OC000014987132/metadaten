use std::borrow::Borrow;

use anyhow::Result;
use askama::Template;
use axum::{
    extract::{Path, Query, State},
    response::{IntoResponse, Redirect, Response},
    Json,
};
use parking_lot::Mutex;
use serde::{Deserialize, Serialize};
use tokio::task::spawn_blocking;
use utoipa::{IntoParams, ToSchema};

use crate::{into_ndjson, Accept, ServerError};
use metadaten::{
    dataset::{Alternative, Dataset, Organisation, TimeRange, UniquelyIdentifiedDataset},
    index::Searcher,
    stats::Stats,
};

#[utoipa::path(
    get,
    path = "/dataset/{source}/{id}",
    params(
        ("source", description = "Name of the source"),
        ("id", description = "Identifier of the dataset within the source"),
    ),
    responses(
        (status = 200, description = "Content of queried data set", content((DatasetPage = "application/json"), (DatasetPage = "text/html"))),
    ),
)]
/// Access a single dataset
pub async fn single(
    Path((source, id)): Path<(String, String)>,
    accept: Accept,
    State(searcher): State<&'static Searcher>,
    State(stats): State<&'static Mutex<Stats>>,
) -> Result<Response, ServerError> {
    fn inner(
        source: String,
        id: String,
        searcher: &Searcher,
        stats: &Mutex<Stats>,
    ) -> Result<DatasetPage, ServerError> {
        let Some(dataset) = searcher.read(&source, &id)? else {
            return Err(ServerError::DatasetNotFound(source, id));
        };

        let accesses = stats.lock().record_access(&source, &id);

        let page = DatasetPage {
            source,
            id,
            dataset: dataset.value,
            accesses,
        };

        Ok(page)
    }

    let page = spawn_blocking(|| inner(source, id, searcher, stats)).await??;

    Ok(accept.into_response(page))
}

#[derive(Template, Serialize, ToSchema)]
#[template(path = "dataset.html")]
pub(super) struct DatasetPage {
    #[serde(skip_serializing)]
    source: String,
    #[serde(skip_serializing)]
    id: String,
    #[serde(flatten)]
    dataset: Dataset,
    /// Number of accesses to this dataset
    accesses: u64,
}

#[utoipa::path(
    get,
    path = "/dataset/{source}/{id}/redirect",
    params(
        ("source", description = "Name of the source"),
        ("id", description = "Identifier of the dataset within the source"),
    ),
    responses(
        (status = 303, description = "Redirect to the source URL of queried data set"),
    ),
)]
/// Redirect to the source URL of a dataset
pub async fn redirect(
    Path((source, id)): Path<(String, String)>,
    State(searcher): State<&'static Searcher>,
    State(stats): State<&'static Mutex<Stats>>,
) -> Result<Redirect, ServerError> {
    fn inner(
        source: String,
        id: String,
        searcher: &Searcher,
        stats: &Mutex<Stats>,
    ) -> Result<String, ServerError> {
        let Some(dataset) = searcher.read(&source, &id)? else {
            return Err(ServerError::DatasetNotFound(source, id));
        };

        stats.lock().record_access(&source, &id);

        Ok(dataset.value.source_url)
    }

    let source_url = spawn_blocking(|| inner(source, id, searcher, stats)).await??;

    Ok(Redirect::to(&source_url))
}

#[utoipa::path(
    get,
    path = "/dataset/all",
    params(DatasetAllParams),
    responses(
        (status = 200, description = "Stream of all datasets", body = UniquelyIdentifiedDataset, content_type = "application/x-ndjson")
    ),
)]
/// Efficiently stream all datasets in a single request
pub async fn all(
    State(searcher): State<&'static Searcher>,
    Query(params): Query<DatasetAllParams>,
) -> impl IntoResponse {
    into_ndjson(searcher.read_all(params.sampling_fraction))
}

#[derive(Deserialize, IntoParams)]
pub struct DatasetAllParams {
    /// If set, randomly yields only the given fraction of results.
    sampling_fraction: Option<f32>,
}

#[utoipa::path(
    get,
    path = "/dataset/random",
    responses(
        (status = 200, description = "Random dataset", body = UniquelyIdentifiedDataset)
    ),
)]
/// Access a randomly chosen data set
pub async fn random(
    State(searcher): State<&'static Searcher>,
) -> Result<Json<UniquelyIdentifiedDataset>, ServerError> {
    let dataset = searcher.read_random()?;

    Ok(Json(dataset))
}
