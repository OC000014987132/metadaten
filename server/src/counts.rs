use axum::{extract::State, Json};
use serde::Serialize;
use time::{serde::rfc3339, OffsetDateTime};
use utoipa::ToSchema;

use crate::Cache;
use metadaten::stats::Counts;

#[utoipa::path(
    get,
    path = "/counts/now",
    responses(
        (status = 200, description = "Counts aggregated over the whole index, current values", body = Counts),
    ),
)]
pub async fn now(State(cache): State<&'static Cache>) -> Json<Counts> {
    let metrics = cache.metrics.load();

    let counts = Counts::from(metrics.as_ref());

    Json(counts)
}

#[utoipa::path(
    get,
    path = "/counts/all",
    responses(
        (status = 200, description = "Counts aggregated over the whole index, past values", body = [TimestampedCounts]),
    ),
)]
pub async fn all(State(cache): State<&'static Cache>) -> Json<Vec<TimestampedCounts>> {
    let stats = cache.stats.load();

    let counts = stats
        .counts
        .iter()
        .rev()
        .map(|(&timestamp, counts)| TimestampedCounts {
            timestamp: timestamp.into(),
            counts: counts.clone(),
        })
        .collect();

    Json(counts)
}

#[derive(Serialize, ToSchema)]
pub struct TimestampedCounts {
    #[serde(with = "rfc3339")]
    timestamp: OffsetDateTime,
    #[serde(flatten)]
    counts: Counts,
}
