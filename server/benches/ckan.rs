use geo::{Coord, Rect};
use serde_json::to_string;
use smallvec::smallvec;
use time::macros::date;
use tiny_bench::{bench_labeled, black_box};

use metadaten::dataset::{
    Dataset, GlobalIdentifier, Organisation, OrganisationRole, Person, Region, Resource,
    ResourceType, Tag, TimeRange, UniquelyIdentifiedDataset,
};
use server::ckan::CkanDataset;

fn main() {
    let dataset = UniquelyIdentifiedDataset {
        source: "umweltportal-sh".into(),
        id: "metadata-npash78574".to_owned(),
        value: Dataset {
            title: "Rechtliche Gliederung des Nationalparks mit den angrenzenden Gebieten 2001 (GK)".to_owned(),
            description: Some("Rechtliche Gliederung (Zonierung) im Bereich des Nationalparks Schleswig-Holsteinsches Wattenmeer und benachbarter Gebiete.".to_owned()),
            origins: smallvec!["/Land/Schleswig-Holstein/Umweltportal".into()],
            license: metadaten::dataset::License::DlDeBy20,
            mandatory_registration: false,
            organisations: smallvec![Organisation::WikiData {
                identifier: 1785301,
                role: OrganisationRole::Unknown
            }],
            persons: vec![Person {
                name: "Jörn Kohlus".to_owned(),
                role: metadaten::dataset::PersonRole::Contact,
                emails: smallvec!["joern.kohlus@lkn.landsh.de".to_owned()],
                ..Default::default()
            }],
            tags: vec![Tag::Umthes(0x160802), Tag::Other("Borders".into())],
            regions: smallvec![Region::MV],
            modified: Some(date!(2023 - 05 - 23)),
            source_url: "https://umweltportal.schleswig-holstein.de/trefferanzeige?docuuid=metadata-npash78574".to_owned(),
            machine_readable_source: true,
            resources: smallvec![Resource {
                r#type: ResourceType::Zip,
                url: "https://s-h.nokis.org/Geodaten/gk/mod100/s_jura/ubgkzo01fl/ubgkzo01fl.zip".to_owned(),
                direct_link: true,
                primary_content: true,
                description: Some("A long link".to_owned())
            }],
            language: metadaten::dataset::Language::German,
            bounding_boxes: smallvec![Rect::new(
                Coord { x: 6.25, y: 53.22 },
                Coord { x: 9.52, y: 55.2 }
            )],
            time_ranges: smallvec![TimeRange {
                from: date!(2001 - 01 - 01),
                until: date!(2001 - 12 - 31)
            }],
            global_identifier: Some(GlobalIdentifier::Url(
                "http://portalu.de/igc_2/40288a8115457aa50115457c6e072bb2".parse().unwrap(),
            )),
            ..Default::default()
        },
    };

    let dataset_with_organizations = {
        let organisations = dataset
            .value
            .organisations
            .iter()
            .cycle()
            .take(500)
            .cloned()
            .collect();

        let mut dataset = dataset.clone();
        dataset.value.organisations = organisations;
        dataset
    };

    let dataset_with_tags = {
        let tags = dataset
            .value
            .tags
            .iter()
            .cycle()
            .take(500)
            .cloned()
            .collect();

        let mut dataset = dataset.clone();
        dataset.value.tags = tags;
        dataset
    };

    let convert = |dataset: UniquelyIdentifiedDataset| CkanDataset::from(black_box(dataset));
    let serialize = |dataset: UniquelyIdentifiedDataset| to_string(&convert(dataset)).unwrap();

    bench_labeled("convert", || convert(dataset.clone()));
    bench_labeled("serialize", || serialize(dataset.clone()));

    bench_labeled("convert_many_organizations", || {
        convert(dataset_with_organizations.clone())
    });
    bench_labeled("serialize_many_organizations", || {
        serialize(dataset_with_organizations.clone())
    });

    bench_labeled("convert_many_tags", || convert(dataset_with_tags.clone()));
    bench_labeled("serialize_many_tags", || {
        serialize(dataset_with_tags.clone())
    });
}
