"use strict";

let bounding_boxes = document.getElementById('bounding_boxes');

if (bounding_boxes) {
    const map = L.map(bounding_boxes);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png',
        {
            maxZoom: 19.0,
            crossOrigin: true,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        })
        .addTo(map);

    bounding_boxes = JSON.parse(bounding_boxes.querySelector("script").textContent);

    let max_x = -180
    let min_x = 180
    let max_y = -90
    let min_y = 90

    for (const bounding_box of bounding_boxes) {
        const min = bounding_box["min"];
        const max = bounding_box["max"];

        if (min.x === max.x && min.y === max.y) {
            L.marker([min.y, min.x]).addTo(map)
        } else {
            L.rectangle([[min.y, min.x], [max.y, max.x]]).addTo(map)
        }

        max_x = Math.max(max_x, max.x);
        min_x = Math.min(min_x, min.x);
        max_y = Math.max(max_y, max.y);
        min_y = Math.min(min_y, min.y);
    }

    const center_x = (max_x + min_x) / 2;
    const center_y = (max_y + min_y) / 2;

    const zoom = Math.min(
        10,
        Math.max(1, Math.floor(Math.log2(360 / (max_x - min_x)))),
    );

    map.setView([center_y, center_x], zoom);
}