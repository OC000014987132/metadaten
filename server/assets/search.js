"use strict";

const query = document.getElementsByName("query")[0];
const completions = document.getElementById("completions");
const types_root = document.getElementsByName("types_root")[0];
const topics_root = document.getElementsByName("topics_root")[0];
const origins_root = document.getElementsByName("origins_root")[0];
const licenses_root = document.getElementsByName("licenses_root")[0];
const languages_root = document.getElementsByName("languages_root")[0];
const resource_types_root = document.getElementsByName("resource_types_root")[0];
const page = document.getElementsByName("page")[0];
const results_per_page = document.getElementsByName("results_per_page")[0];
const bounding_box_west = document.getElementsByName("bounding_box_west")[0];
const bounding_box_south = document.getElementsByName("bounding_box_south")[0];
const bounding_box_east = document.getElementsByName("bounding_box_east")[0];
const bounding_box_north = document.getElementsByName("bounding_box_north")[0];
const bounding_box_contains = document.getElementsByName("bounding_box_contains")[0];
const bounding_box_spatial_clusters = document.getElementsByName("bounding_box_spatial_clusters")[0];
const time_range_from = document.getElementsByName("time_range_from")[0];
const time_range_until = document.getElementsByName("time_range_until")[0];
const form = document.getElementById("form");

function disable_if_default(element) {
    element.disabled = element.value === element.dataset.default;
}

form.addEventListener("submit", (event) => {
    disable_if_default(query);

    disable_if_default(types_root);
    disable_if_default(topics_root);
    disable_if_default(origins_root);
    disable_if_default(licenses_root);
    disable_if_default(languages_root);
    disable_if_default(resource_types_root);

    disable_if_default(page);
    disable_if_default(results_per_page);

    time_range_from.disabled = time_range_from.value.length === 0;
    time_range_until.disabled = time_range_until.value.length === 0;
});

function add_click_listener(handler) {
    const prefix = `#${handler.name}:`;

    for (const element of document.querySelectorAll(`a[href^='${prefix}']`)) {
        const argument = element.getAttribute("href").slice(prefix.length);

        element.addEventListener("click", (event) => {
            event.preventDefault();
            handler(argument);
        });
    }
}

add_click_listener(go_to_page);
add_click_listener(replace_term);
add_click_listener(add_term);
add_click_listener(set_types_root);
add_click_listener(reset_types_root);
add_click_listener(set_topics_root);
add_click_listener(reset_topics_root);
add_click_listener(set_origins_root);
add_click_listener(reset_origins_root);
add_click_listener(set_licenses_root);
add_click_listener(reset_licenses_root);
add_click_listener(set_languages_root);
add_click_listener(reset_languages_root);
add_click_listener(set_resource_types_root);
add_click_listener(reset_resource_types_root);

bounding_box_contains.addEventListener("change", () => form.requestSubmit());
bounding_box_spatial_clusters.addEventListener("change", () => form.requestSubmit());

query.addEventListener("input", () => update_completions());

document.addEventListener("keyup", (event) => {
    if (event.key === "s" && document.activeElement !== query) {
        query.focus();
        query.select();
    }
});

function go_to_page(value) {
    page.value = value;
    form.requestSubmit();
}

const spaces = /\s/;

function replace_term(value) {
    const index = value.indexOf("/");
    const term = value.slice(0, index);
    const replacement = value.slice(index + 1);

    query.value = query.value.replace(new RegExp(term, "gi"), replacement);

    form.requestSubmit();
}

function add_term(value) {
    const occur = value.at(0);
    const term = value.slice(1);

    const has_spaces = spaces.test(term);
    query.value += has_spaces ? ` ${occur}"${term}"` : ` ${occur}${term}`;

    form.requestSubmit();
}

function remove_last_component(value) {
    const index = value.lastIndexOf("/");
    return index == 0 ? "/" : value.slice(0, index);
}

function set_types_root(value) {
    types_root.value = value;
    form.requestSubmit();
}

function reset_types_root() {
    types_root.value = remove_last_component(types_root.value);
    form.requestSubmit();
}

function set_topics_root(value) {
    topics_root.value = value;
    form.requestSubmit();
}

function reset_topics_root() {
    topics_root.value = remove_last_component(topics_root.value);
    form.requestSubmit();
}

function set_origins_root(value) {
    origins_root.value = value;
    form.requestSubmit();
}

function reset_origins_root() {
    origins_root.value = remove_last_component(origins_root.value);
    form.requestSubmit();
}

function set_licenses_root(value) {
    licenses_root.value = value;
    form.requestSubmit();
}

function reset_licenses_root() {
    licenses_root.value = remove_last_component(licenses_root.value);
    form.requestSubmit();
}

function set_languages_root(value) {
    languages_root.value = value;
    form.requestSubmit();
}

function reset_languages_root() {
    languages_root.value = remove_last_component(languages_root.value);
    form.requestSubmit();
}

function set_resource_types_root(value) {
    resource_types_root.value = value;
    form.requestSubmit();
}

function reset_resource_types_root() {
    resource_types_root.value = remove_last_component(resource_types_root.value);
    form.requestSubmit();
}

function quote_term(field, term) {
    const has_spaces = spaces.test(term);
    return has_spaces ? ` +${field}:"${term}"` : ` +${field}:${term}`;
}

let completions_pending = null;

async function update_completions() {
    if (query.value.length < 3 || query.value.length !== query.selectionStart) {
        return;
    }

    if (completions_pending !== null) {
        return;
    }

    completions_pending = query.value;

    try {
        const params = new URLSearchParams({ "query": completions_pending });
        const response = await fetch(`/complete?${params}`);
        const data = await response.json();

        const desc_by_count = (lhs, rhs) => rhs[1] - lhs[1];

        data["completions"].sort(desc_by_count);
        data["topics"].sort(desc_by_count);
        data["origins"].sort(desc_by_count);
        data["licenses"].sort(desc_by_count);
        data["languages"].sort(desc_by_count);
        data["resource_types"].sort(desc_by_count);

        const options = data["completions"].map((completion) => new Option(completions_pending + completion[0]))
            .concat(
                data["topics"].map((topic) => new Option(completions_pending + quote_term("topic", topic[0]))),
                data["origins"].map((origin) => new Option(completions_pending + quote_term("origin", origin[0]))),
                data["licenses"].map((license) => new Option(completions_pending + quote_term("license", license[0]))),
                data["languages"].map((license) => new Option(completions_pending + quote_term("language", license[0]))),
                data["resource_types"].map((resource_type) => new Option(completions_pending + quote_term("resource_type", resource_type[0]))),
            );
        completions.replaceChildren(...options);
    } finally {
        completions_pending = null;
    }
}

function handle_draw_event(event) {
    const coordinates = event.layer.getBounds();

    bounding_box_west.disabled = false;
    bounding_box_west.value = coordinates.getSouthWest().lng;

    bounding_box_south.disabled = false;
    bounding_box_south.value = coordinates.getSouthWest().lat;

    bounding_box_east.disabled = false;
    bounding_box_east.value = coordinates.getNorthEast().lng;

    bounding_box_north.disabled = false;
    bounding_box_north.value = coordinates.getNorthEast().lat;

    form.requestSubmit();
}

function handle_clear_event() {
    bounding_box_south.disabled = true;
    bounding_box_west.disabled = true;
    bounding_box_north.disabled = true;
    bounding_box_east.disabled = true;

    form.requestSubmit();
}

const map = L.map('bounding_box').setView([51.5, 10.25], 5.0);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png',
    {
        maxZoom: 19.0,
        crossOrigin: true,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    })
    .addTo(map);

map.addControl(new L.Control.Draw({
    edit: false,
    position: 'topright',
    draw: {
        polygon: false,
        polyline: false,
        rectangle: true,
        circle: false,
        marker: false,
        circlemarker: false
    }
}));

const clear_control = L.Control.extend({
    options: {
        position: 'topright'
    },
    onAdd: function (map) {
        let container = L.DomUtil.create('a', 'leaflet-bar leaflet-button');

        container.innerHTML = '<span class="leaflet-bar-part leaflet-bar-part-single">&cross;</span>';
        container.title = 'Clear bounding box';
        container.onclick = handle_clear_event;

        return container;
    }
});
map.addControl(new clear_control());

map.on(L.Draw.Event.CREATED, handle_draw_event);

if (!(bounding_box_south.disabled && bounding_box_west.disabled && bounding_box_north.disabled && bounding_box_east.disabled)) {
    let south = -90.0;
    let west = -180.0;
    let north = 90.0;
    let east = 180.0;

    if (!bounding_box_south.disabled) {
        south = parseFloat(bounding_box_south.value);
    }
    if (!bounding_box_west.disabled) {
        west = parseFloat(bounding_box_west.value);
    }
    if (!bounding_box_north.disabled) {
        north = parseFloat(bounding_box_north.value);
    }
    if (!bounding_box_east.disabled) {
        east = parseFloat(bounding_box_east.value);
    }

    const bounding_box = new L.FeatureGroup();
    map.addLayer(bounding_box);

    const bounding_box_feature = L.rectangle([[south, west], [north, east]], {});
    bounding_box_feature.addTo(map);

    map.fitBounds(bounding_box_feature.getBounds(), { animate: false });
}

const spatial_clusters = JSON.parse(document.getElementById("spatial_clusters").textContent);

function filter_spatial_cluster(spatial_cluster) {
    bounding_box_west.disabled = false;
    bounding_box_west.value = spatial_cluster.bounding_box.min.x;

    bounding_box_south.disabled = false;
    bounding_box_south.value = spatial_cluster.bounding_box.min.y;

    bounding_box_east.disabled = false;
    bounding_box_east.value = spatial_cluster.bounding_box.max.x;

    bounding_box_north.disabled = false;
    bounding_box_north.value = spatial_cluster.bounding_box.max.y;

    form.requestSubmit();
}

for (const spatial_cluster of spatial_clusters) {
    L.marker(
        [spatial_cluster.position.y, spatial_cluster.position.x],
        {
            icon: L.divIcon({
                className: "spatial-cluster",
                html: spatial_cluster.count.toLocaleString(),
            })
        }
    )
    .on("click", () => filter_spatial_cluster(spatial_cluster))
    .addTo(map);
}
