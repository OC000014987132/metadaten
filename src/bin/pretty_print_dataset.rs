use std::io::{stdin, stdout};

use anyhow::Result;
use serde_json::to_writer_pretty;

use metadaten::dataset::Dataset;

fn main() -> Result<()> {
    let val = Dataset::read_with(stdin().lock(), &mut Vec::new())?;
    to_writer_pretty(stdout().lock(), &val)?;
    println!();
    Ok(())
}
