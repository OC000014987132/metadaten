use std::env::args;

use anyhow::{ensure, Result};

use metadaten::{
    data_path_from_env,
    index::{Searcher, TextQuery},
};

fn main() -> Result<()> {
    let mut args = args().skip(1).collect::<Vec<_>>();
    ensure!(args.len() == 3, "usage: explain <query> <source> <id>");
    let id = args.pop().unwrap();
    let source = args.pop().unwrap();
    let query = args.pop().unwrap();

    let searcher = Searcher::open(&data_path_from_env())?;

    let explanation = searcher.explain(&TextQuery(query), &source, &id)?;

    println!("{}", explanation);

    Ok(())
}
