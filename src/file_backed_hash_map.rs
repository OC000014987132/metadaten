use std::borrow::Borrow;
use std::cmp::max;
use std::fs::File;
use std::hash::BuildHasher;
use std::io::Write;
use std::marker::PhantomData;
use std::mem::{size_of, transmute};
use std::ops::Deref;
use std::path::Path;
use std::str::from_utf8;

use anyhow::{ensure, Result};
use bincode::{deserialize, serialize};
use cap_std::fs::{Dir, OpenOptions as FsOpenOptions};
use hashbrown::{hash_table::Entry, DefaultHashBuilder, HashTable};
use memmap2::{Advice, Mmap, MmapMut};
use rustix::fs::{flock, FlockOperation};
use serde::{Deserialize, Serialize};
use tempfile::tempfile;
use zstd::bulk::{decompress, Compressor};

use crate::COMPRESSION_LEVEL;

pub struct FileBackedHashMap<K, V, M = MmapMut>
where
    K: ?Sized,
    V: ?Sized,
{
    file: File,
    mmap: M,
    dir: HashTable<usize>,
    hash_builder: DefaultHashBuilder,
    _keys: PhantomData<K>,
    _values: PhantomData<V>,
}

impl<K, V, M> Drop for FileBackedHashMap<K, V, M>
where
    K: ?Sized,
    V: ?Sized,
{
    fn drop(&mut self) {
        let _ = flock(&self.file, FlockOperation::Unlock);
    }
}

impl<K, V> FileBackedHashMap<K, V, Mmap>
where
    K: ?Sized,
    V: ?Sized,
{
    #[allow(unsafe_code)]
    pub fn open_shared(file: File) -> Result<Self> {
        flock(&file, FlockOperation::NonBlockingLockShared)?;

        let mmap = unsafe { Mmap::map(&file)? };

        ensure!(mmap.len() >= SIZE_USED_LEN);

        mmap.advise(Advice::Sequential)?;

        let (dir, hash_builder) = read_dir(&mmap);

        mmap.advise(Advice::Random)?;

        Ok(Self {
            file,
            mmap,
            dir,
            hash_builder,
            _keys: PhantomData,
            _values: PhantomData,
        })
    }

    pub fn open_path_shared<P>(dir: &Dir, path: P) -> Result<Self>
    where
        P: AsRef<Path>,
    {
        Self::open_shared(dir.open(path)?.into_std())
    }

    pub fn open_temp_shared() -> Result<Self> {
        let mut file = tempfile()?;

        file.write_all(&0_usize.to_ne_bytes())?;

        Self::open_shared(file)
    }
}

impl<K, V> FileBackedHashMap<K, V, MmapMut>
where
    K: ?Sized,
    V: ?Sized,
{
    #[allow(unsafe_code)]
    pub fn open(file: File) -> Result<Self> {
        flock(&file, FlockOperation::NonBlockingLockExclusive)?;

        let mut mmap = unsafe { MmapMut::map_mut(&file)? };

        if mmap.is_empty() {
            file.set_len(SIZE_USED_LEN.try_into().unwrap())?;
            mmap = unsafe { MmapMut::map_mut(&file)? };

            write_used_len(&mut mmap, SIZE_USED_LEN);
        }

        assert!(mmap.len() >= SIZE_USED_LEN);

        mmap.advise(Advice::Sequential)?;

        let (dir, hash_builder) = read_dir(&mmap);

        mmap.advise(Advice::Random)?;

        Ok(Self {
            file,
            mmap,
            dir,
            hash_builder,
            _keys: PhantomData,
            _values: PhantomData,
        })
    }

    pub fn open_path<P>(dir: &Dir, path: P) -> Result<Self>
    where
        P: AsRef<Path>,
    {
        Self::open(
            dir.open_with(
                path,
                FsOpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .truncate(false),
            )?
            .into_std(),
        )
    }

    pub fn open_temp() -> Result<Self> {
        Self::open(tempfile()?)
    }
}

impl<K, V, M> FileBackedHashMap<K, V, M>
where
    K: ?Sized,
    V: ?Sized,
    M: Deref<Target = [u8]>,
{
    #[allow(unsafe_code)]
    pub fn reinterpret<P, Q>(&mut self) -> &mut FileBackedHashMap<P, Q, M> {
        unsafe { transmute(self) }
    }

    pub fn get<'k, Q>(&self, key: Q) -> Result<Option<V::Typed<'_>>>
    where
        Q: Borrow<K::Typed<'k>>,
        K: ToBytes,
        V: FromBytes,
    {
        let key_bytes = K::to_bytes(key.borrow())?;
        let key_bytes = key_bytes.as_ref();

        let hash = self.hash_builder.hash_one(key_bytes);
        let eq = |offset: &usize| cmp_offset(&self.mmap, key_bytes, offset);

        let Some(offset) = self.dir.find(hash, eq) else {
            return Ok(None);
        };

        let entry = &self.mmap[*offset..];

        let (key_len, entry) = read_len(entry);
        debug_assert!(key_len & DELETED == 0);
        let (_, entry) = read_bytes(entry, key_len);

        let (value_len, entry) = read_len(entry);
        let (value_bytes, _) = read_bytes(entry, value_len);

        let value = V::from_bytes(value_bytes)?;

        Ok(Some(value))
    }

    pub fn iter(&self) -> impl Iterator<Item = Result<(K::Typed<'_>, V::Typed<'_>)>> + '_
    where
        K: FromBytes,
        V: FromBytes,
    {
        self.dir.iter().map(|offset| {
            let entry = &self.mmap[*offset..];

            let (key_len, entry) = read_len(entry);
            debug_assert!(key_len & DELETED == 0);
            let (key_bytes, entry) = read_bytes(entry, key_len);

            let (value_len, entry) = read_len(entry);
            let (value_bytes, _) = read_bytes(entry, value_len);

            let key = K::from_bytes(key_bytes)?;
            let value = V::from_bytes(value_bytes)?;

            Ok((key, value))
        })
    }
}

impl<K, V> FileBackedHashMap<K, V, MmapMut>
where
    K: ?Sized,
    V: ?Sized,
{
    pub fn put<'k, 'v, P, Q>(&mut self, key: P, value: Q) -> Result<()>
    where
        P: Borrow<K::Typed<'k>>,
        Q: Borrow<V::Typed<'v>>,
        K: ToBytes,
        V: ToBytes,
    {
        let key_bytes = K::to_bytes(key.borrow())?;
        let key_bytes = key_bytes.as_ref();

        let value_bytes = V::to_bytes(value.borrow())?;
        let value_bytes = value_bytes.as_ref();

        let key_len = key_bytes.len().try_into().expect("Key too large");
        assert!(key_len & DELETED == 0);
        let value_len = value_bytes.len().try_into().expect("Value too large");

        let (used_len, _) = read_used_len(&self.mmap);

        let hash = self.hash_builder.hash_one(key_bytes);
        let eq = |offset: &usize| cmp_offset(&self.mmap, key_bytes, offset);
        let hasher = |offset: &usize| hash_offset(&self.mmap, &self.hash_builder, offset);

        match self.dir.entry(hash, eq, hasher) {
            Entry::Vacant(entry) => {
                entry.insert(used_len);
            }
            Entry::Occupied(entry) => {
                let offset = entry.into_mut();

                let old_entry = &self.mmap[*offset..];

                let (old_key_len, old_entry) = read_len(old_entry);
                debug_assert!(old_key_len & DELETED == 0);
                let (old_key_bytes, old_entry) = read_bytes(old_entry, old_key_len);
                debug_assert_eq!(old_key_bytes, key_bytes);

                let (old_value_len, _) = read_len(old_entry);

                if old_value_len == value_len {
                    write_bytes(
                        &mut self.mmap[*offset + 2 * SIZE_LEN + old_key_len as usize..],
                        value_bytes,
                    );

                    return Ok(());
                }

                write_len(&mut self.mmap[*offset..], old_key_len | DELETED);

                *offset = used_len;
            }
        }

        let entry_len = entry_len(key_len, value_len);

        if used_len + entry_len > self.mmap.len() {
            self.expand(used_len, entry_len)?;
        }

        let entry = &mut self.mmap[used_len..];

        let entry = write_len(entry, key_len);
        let entry = write_bytes(entry, key_bytes);

        let entry = write_len(entry, value_len);
        write_bytes(entry, value_bytes);

        write_used_len(&mut self.mmap, used_len + entry_len);

        Ok(())
    }

    #[cold]
    #[inline(never)]
    #[allow(unsafe_code)]
    fn expand(&mut self, used_len: usize, entry_len: usize) -> Result<()> {
        let file_len = max(used_len + entry_len, 2 * self.mmap.len());

        self.file.set_len(file_len.try_into().unwrap())?;
        self.mmap = unsafe { MmapMut::map_mut(&self.file)? };

        debug_assert!(used_len + entry_len <= self.mmap.len());

        self.mmap.advise(Advice::Random)?;

        Ok(())
    }

    pub fn del<'k, Q>(&mut self, key: Q) -> Result<()>
    where
        Q: Borrow<K::Typed<'k>>,
        K: ToBytes,
    {
        let key_bytes = K::to_bytes(key.borrow())?;
        let key_bytes = key_bytes.as_ref();

        let hash = self.hash_builder.hash_one(key_bytes);
        let eq = |offset: &usize| cmp_offset(&self.mmap, key_bytes, offset);

        let Ok(entry) = self.dir.find_entry(hash, eq) else {
            return Ok(());
        };

        let (offset, _) = entry.remove();

        let entry = &mut self.mmap[offset..];

        let (key_len, _) = read_len(entry);
        debug_assert!(key_len & DELETED == 0);
        write_len(entry, key_len | DELETED);

        Ok(())
    }

    pub fn retain<F>(&mut self, mut f: F) -> Result<usize>
    where
        K: FromBytes,
        V: FromBytes,
        F: FnMut(K::Typed<'_>, V::Typed<'_>) -> Result<bool>,
    {
        let mut err = None;

        self.dir.retain(|offset| {
            let mut keep = true;

            let entry = &mut self.mmap[*offset..];

            if err.is_none() {
                let (key_len, entry) = read_len(entry);
                debug_assert!(key_len & DELETED == 0);
                let (key_bytes, entry) = read_bytes(entry, key_len);

                let (value_len, entry) = read_len(entry);
                let (value_bytes, _) = read_bytes(entry, value_len);

                match K::from_bytes(key_bytes) {
                    Ok(key) => match V::from_bytes(value_bytes) {
                        Ok(value) => match f(key, value) {
                            Ok(keep1) => keep = keep1,
                            Err(err1) => err = Some(err1),
                        },
                        Err(err1) => err = Some(err1),
                    },
                    Err(err1) => err = Some(err1),
                }
            }

            if !keep {
                let (key_len, _) = read_len(entry);
                debug_assert!(key_len & DELETED == 0);
                write_len(entry, key_len | DELETED);
            }

            keep
        });

        match err {
            None => Ok(self.dir.len()),
            Some(err) => Err(err),
        }
    }

    #[allow(unsafe_code)]
    pub fn compact(&mut self) -> Result<()> {
        self.mmap.advise(Advice::Sequential)?;

        let (used_len, _) = read_used_len(&self.mmap);

        let mut offset = SIZE_USED_LEN;
        let mut skipped = 0;

        while offset + skipped < used_len {
            let entry = &self.mmap[offset + skipped..];

            let (key_len, entry) = read_len(entry);
            let (key_bytes, entry) = read_bytes(entry, key_len & !DELETED);

            let (value_len, _) = read_len(entry);

            let entry_len = entry_len(key_len, value_len);

            if key_len & DELETED == 0 {
                let hash = self.hash_builder.hash_one(key_bytes);
                let eq = |offset: &usize| cmp_offset(&self.mmap, key_bytes, offset);

                *self.dir.find_mut(hash, eq).unwrap() = offset;

                if skipped != 0 {
                    self.mmap[offset..].copy_within(skipped..skipped + entry_len, 0);
                }

                offset += entry_len;
            } else {
                skipped += entry_len;
            }
        }

        debug_assert!(offset + skipped == used_len);

        write_used_len(&mut self.mmap, offset);

        self.file.set_len(offset.try_into().unwrap())?;
        self.mmap = unsafe { MmapMut::map_mut(&self.file)? };

        debug_assert!(offset == self.mmap.len());

        self.mmap.advise(Advice::Random)?;

        Ok(())
    }
}

fn cmp_offset(mmap: &[u8], other_key_bytes: &[u8], offset: &usize) -> bool {
    let entry = &mmap[*offset..];

    let (key_len, entry) = read_len(entry);
    let (key_bytes, _) = read_bytes(entry, key_len & !DELETED);

    other_key_bytes == key_bytes
}

fn hash_offset(mmap: &[u8], hash_builder: &DefaultHashBuilder, offset: &usize) -> u64 {
    let entry = &mmap[*offset..];

    let (key_len, entry) = read_len(entry);
    let (key_bytes, _) = read_bytes(entry, key_len & !DELETED);

    hash_builder.hash_one(key_bytes)
}

fn read_dir(mmap: &[u8]) -> (HashTable<usize>, DefaultHashBuilder) {
    let mut dir = HashTable::new();
    let hash_builder = DefaultHashBuilder::default();

    let (used_len, _) = read_used_len(mmap);

    let mut offset = SIZE_USED_LEN;

    while offset < used_len {
        let entry = &mmap[offset..];

        let (key_len, entry) = read_len(entry);
        let (key_bytes, entry) = read_bytes(entry, key_len & !DELETED);

        let (value_len, _) = read_len(entry);

        if key_len & DELETED == 0 {
            let hash = hash_builder.hash_one(key_bytes);
            let hasher = |offset: &usize| hash_offset(mmap, &hash_builder, offset);

            dir.insert_unique(hash, offset, hasher);
        }

        offset += entry_len(key_len, value_len);
    }

    (dir, hash_builder)
}

const DELETED: u32 = 1 << (u32::BITS - 1);

fn entry_len(key_len: u32, value_len: u32) -> usize {
    2 * SIZE_LEN
        + usize::try_from(key_len & !DELETED).unwrap()
        + usize::try_from(value_len).unwrap()
}

const SIZE_USED_LEN: usize = size_of::<usize>();

fn read_used_len(entry: &[u8]) -> (usize, &[u8]) {
    let (entry, rest) = entry.split_at(SIZE_USED_LEN);

    let used_len = usize::from_ne_bytes(entry.try_into().unwrap());

    (used_len, rest)
}

fn write_used_len(entry: &mut [u8], used_len: usize) -> &mut [u8] {
    let (entry, rest) = entry.split_at_mut(SIZE_USED_LEN);

    entry.copy_from_slice(&used_len.to_ne_bytes());

    rest
}

const SIZE_LEN: usize = size_of::<u32>();

fn read_len(entry: &[u8]) -> (u32, &[u8]) {
    let (entry, rest) = entry.split_at(SIZE_LEN);

    let len = u32::from_ne_bytes(entry.try_into().unwrap());

    (len, rest)
}

fn write_len(entry: &mut [u8], len: u32) -> &mut [u8] {
    let (entry, rest) = entry.split_at_mut(SIZE_LEN);

    entry.copy_from_slice(&len.to_ne_bytes());

    rest
}

fn read_bytes(entry: &[u8], len: u32) -> (&[u8], &[u8]) {
    entry.split_at(len.try_into().unwrap())
}

fn write_bytes<'a>(entry: &'a mut [u8], bytes: &[u8]) -> &'a mut [u8] {
    let (entry, rest) = entry.split_at_mut(bytes.len());

    entry.copy_from_slice(bytes);

    rest
}

pub trait ToBytes {
    type Typed<'a>;
    type Bytes<'a>: AsRef<[u8]>;

    fn to_bytes<'a>(val: &'a Self::Typed<'_>) -> Result<Self::Bytes<'a>>;
}

pub trait FromBytes: ToBytes {
    fn from_bytes(bytes: &[u8]) -> Result<Self::Typed<'_>>;
}

impl ToBytes for [u8] {
    type Typed<'a> = &'a [u8];
    type Bytes<'a> = &'a [u8];

    fn to_bytes<'a>(val: &'a Self::Typed<'_>) -> Result<Self::Bytes<'a>> {
        Ok(val)
    }
}

impl FromBytes for [u8] {
    fn from_bytes(bytes: &[u8]) -> Result<Self::Typed<'_>> {
        Ok(bytes)
    }
}

impl ToBytes for str {
    type Typed<'a> = &'a str;
    type Bytes<'a> = &'a [u8];

    fn to_bytes<'a>(val: &'a Self::Typed<'_>) -> Result<Self::Bytes<'a>> {
        Ok(val.as_bytes())
    }
}

impl FromBytes for str {
    fn from_bytes(bytes: &[u8]) -> Result<Self::Typed<'_>> {
        let val = from_utf8(bytes)?;

        Ok(val)
    }
}

impl ToBytes for u64 {
    type Typed<'a> = u64;
    type Bytes<'a> = [u8; 8];

    fn to_bytes<'a>(val: &'a Self::Typed<'_>) -> Result<Self::Bytes<'a>> {
        Ok(val.to_ne_bytes())
    }
}

impl FromBytes for u64 {
    fn from_bytes(bytes: &[u8]) -> Result<Self::Typed<'_>> {
        let val = u64::from_ne_bytes(bytes.try_into()?);

        Ok(val)
    }
}

pub struct Bincode<T, const COMPRESS: bool = false>(PhantomData<T>);

impl<T, const COMPRESS: bool> ToBytes for Bincode<T, COMPRESS>
where
    T: Serialize,
{
    type Typed<'a> = T;
    type Bytes<'a> = Vec<u8>;

    fn to_bytes<'a>(val: &'a Self::Typed<'_>) -> Result<Self::Bytes<'a>> {
        let bytes = serialize(val)?;

        let bytes = if COMPRESS {
            let mut compressor = Compressor::new(COMPRESSION_LEVEL)?;
            compressor.include_checksum(false)?;
            compressor.include_dictid(false)?;

            compressor.compress(&bytes)?
        } else {
            bytes
        };

        Ok(bytes)
    }
}

impl<T, const COMPRESS: bool> FromBytes for Bincode<T, COMPRESS>
where
    T: Serialize + for<'a> Deserialize<'a>,
{
    fn from_bytes(bytes: &[u8]) -> Result<Self::Typed<'_>> {
        let bytes = if COMPRESS {
            &decompress(bytes, usize::MAX)?
        } else {
            bytes
        };

        let val = deserialize(bytes)?;

        Ok(val)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use tempfile::tempfile;

    #[test]
    fn put_get_iter_work() {
        let file = tempfile().unwrap();
        let mut map = FileBackedHashMap::<str, str>::open(file.try_clone().unwrap()).unwrap();

        map.put("foo", "bar").unwrap();
        map.put("baz", "qux").unwrap();

        let (used_len, _) = read_used_len(&map.mmap);
        assert_eq!(used_len, SIZE_USED_LEN + 2 * entry_len(3, 3));
        assert_eq!(map.mmap.len(), (SIZE_USED_LEN + entry_len(3, 3)) * 2);

        map = FileBackedHashMap::open(file).unwrap();

        assert_eq!(map.get("foo").unwrap(), Some("bar"));
        assert_eq!(map.get("baz").unwrap(), Some("qux"));

        let mut entries = map.iter().map(|res| res.unwrap()).collect::<Vec<_>>();
        entries.sort_unstable();
        assert_eq!(entries, [("baz", "qux"), ("foo", "bar")]);
    }

    #[test]
    fn del_compact_work() {
        let file = tempfile().unwrap();
        let mut map = FileBackedHashMap::<str, str>::open(file.try_clone().unwrap()).unwrap();

        let keys = (0..10).map(|idx| format!("key_{idx}")).collect::<Vec<_>>();

        let mut values = (0..10)
            .map(|idx| format!("value_{idx}"))
            .collect::<Vec<_>>();

        for idx in 0..10 {
            map.put(keys[idx].as_str(), values[idx].as_str()).unwrap();
        }

        let (used_len, _) = read_used_len(&map.mmap);
        assert_eq!(used_len, SIZE_USED_LEN + 10 * entry_len(5, 7));
        assert_eq!(
            map.mmap.len(),
            (SIZE_USED_LEN + entry_len(5, 7)) * 2 * 2 * 2
        );

        map.del(keys[0].as_str()).unwrap();
        map.del(keys[3].as_str()).unwrap();
        map.del(keys[6].as_str()).unwrap();
        map.del(keys[7].as_str()).unwrap();
        map.del(keys[9].as_str()).unwrap();

        values[4] = "VALUE_4".to_owned();
        values[8] = "VALUE_8".to_owned();

        map.put(keys[4].as_str(), values[4].as_str()).unwrap();
        map.put(keys[8].as_str(), values[8].as_str()).unwrap();

        map = FileBackedHashMap::open(file.try_clone().unwrap()).unwrap();

        assert_eq!(map.get(keys[0].as_str()).unwrap(), None);
        assert_eq!(map.get(keys[1].as_str()).unwrap(), Some(values[1].as_str()));
        assert_eq!(map.get(keys[2].as_str()).unwrap(), Some(values[2].as_str()));
        assert_eq!(map.get(keys[3].as_str()).unwrap(), None);
        assert_eq!(map.get(keys[4].as_str()).unwrap(), Some(values[4].as_str()));
        assert_eq!(map.get(keys[5].as_str()).unwrap(), Some(values[5].as_str()));
        assert_eq!(map.get(keys[6].as_str()).unwrap(), None);
        assert_eq!(map.get(keys[7].as_str()).unwrap(), None);
        assert_eq!(map.get(keys[8].as_str()).unwrap(), Some(values[8].as_str()));
        assert_eq!(map.get(keys[9].as_str()).unwrap(), None);

        let (used_len, _) = read_used_len(&map.mmap);
        assert_eq!(used_len, SIZE_USED_LEN + 10 * entry_len(5, 7));
        assert_eq!(
            map.mmap.len(),
            (SIZE_USED_LEN + entry_len(5, 7)) * 2 * 2 * 2
        );

        map.compact().unwrap();

        let (used_len, _) = read_used_len(&map.mmap);
        assert_eq!(used_len, SIZE_USED_LEN + 5 * entry_len(5, 7));
        assert_eq!(map.mmap.len(), SIZE_USED_LEN + 5 * entry_len(5, 7));

        map = FileBackedHashMap::open(file).unwrap();

        let mut entries = map.iter().map(|res| res.unwrap()).collect::<Vec<_>>();
        entries.sort_unstable();
        assert_eq!(
            entries,
            [
                (keys[1].as_str(), values[1].as_str()),
                (keys[2].as_str(), values[2].as_str()),
                (keys[4].as_str(), values[4].as_str()),
                (keys[5].as_str(), values[5].as_str()),
                (keys[8].as_str(), values[8].as_str())
            ]
        );
    }
}
