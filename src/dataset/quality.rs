use hashbrown::HashSet;
use hypher::{hyphenate, Lang};
use once_cell::sync::Lazy;
use openssl::sha::Sha256;
use regex::Regex;
use serde::{Deserialize, Serialize};
use strum::{Display, EnumCount};
use utoipa::ToSchema;

use crate::dataset::Dataset;

/// These indicators should help the user to judge the relevance of a dataset.
///
/// It follows the categories spelled out in the FAIR principles.
/// It is not a quality assessment of the data themselves, but rather
/// indicates the degree to which it is findable and usable.
///
/// Many of the fields are based on the ones used by [data.europa.eu](https://data.europa.eu/mqa/methodology)
#[derive(Debug, Clone, Default, Serialize, Deserialize, ToSchema)]
pub struct Quality {
    pub findability: Findability,
    pub accessibility: Accessibility,
    pub interoperability: Interoperability,
    pub reusability: Reusability,
    /// Total score for quality in the range of [0, 1] derived from respective sub-scores.
    pub score: f32,
}

/// This quality dimension indicates how well this dataset can be found.
#[derive(Debug, Clone, Default, Serialize, Deserialize, ToSchema)]
pub struct Findability {
    /// This score indicates the readability of the title in the range 0 (hard to understand) to 1 (easy to understand).
    /// It is equivalent to the Flesch - Reading - Ease index but scaled from 0 to 1 instead of the commonly used 0 - 100 scale.
    pub title: f32,
    /// This score indicates the readability of the description in the range 0 (no description) to 1 (easy to understand).
    /// It is equivalent to the Flesch - Reading - Ease index but scaled from 0 to 1 instead of the commonly used 0 - 100 scale.
    pub description: f32,
    pub spatial: QualitySpatial,
    /// Numeric representation of `spatial` field.
    pub spatial_score: f32,
    /// This score indicates if temporal information is provided,
    /// either by the fields `issued` or `modified`
    /// or by giving detailed information in `time_ranges`.
    pub temporal: bool,
    /// This score indicates how well the description is matched to keywords.
    /// It is given as the ratio of the keywords attached to this dataset
    /// belonging to a controlled vocabulary, therefore ranging from 0 (none of
    /// the keywords belong to a controlled vocabulary) to 1 (all of the keywords
    /// belong to a controlled vocabulary).
    pub keywords: f32,
    /// This score indicates if a `global_identifier` exists.
    pub identifier: bool,
    /// overall score of this quality dimension.
    pub score: f32,
}

/// This quality dimension indicates how well this dataset can be accessed.
#[derive(Debug, Clone, Default, Serialize, Deserialize, ToSchema)]
pub struct Accessibility {
    pub landing_page: QualityLandingPage,
    /// Numeric represenation of `landing_page` field.
    pub landing_page_score: f32,
    /// This score indicates if a direct link to the primary content exists.
    ///
    /// The following two criteria must be met:
    ///  - Direct links must be machine-navigable, i.e. without any impediments like accepting cookies / terms of use or requiring logins / sessions.
    ///  - They must link the primary content referenced in the dataset description (e.g. press releases, data tables), i.e. no user manuals, background information or similar.
    pub direct_access: bool,
    /// Indicates that no additional barriers like fees or registrations are required to access the data.
    pub publicly_accessible: bool,
    /// overall score of this quality dimension
    pub score: f32,
}

/// This quality dimension indicates how well this dataset can be integrated with other data.
#[derive(Debug, Clone, Default, Serialize, Deserialize, ToSchema)]
pub struct Interoperability {
    /// This score indicates whether the data can be retrieved in an open data format.
    pub open_file_format: bool,
    /// This score indicates whether the type of the dataset is stated.
    pub media_type: bool,
    /// This score indicates whether the dataset is given in a machine readable format.
    pub machine_readable_data: bool,
    /// This score indicates whether the metadata of the dataset is given in a machine readable format.
    pub machine_readable_metadata: bool,
    /// overall score of this quality dimension
    pub score: f32,
}

/// This quality dimension indicates how well this dataset can be reused.
#[derive(Debug, Clone, Default, Serialize, Deserialize, ToSchema)]
pub struct Reusability {
    pub license: QualityLicense,
    /// Numeric representation of `license` field.
    pub license_score: f32,
    /// This score indicates whether a contact address and/or person is given.
    pub contact_info: bool,
    /// This score indicates whether the publisher is stated.
    pub publisher_info: bool,
    /// overall score of this quality dimension
    pub score: f32,
}

impl Quality {
    pub fn of(dataset: &Dataset, duplicate_landing_pages: &HashSet<[u8; 32]>) -> Self {
        let accessibility = Accessibility::of(dataset, duplicate_landing_pages);
        let findability = Findability::of(dataset);
        let interoperability = Interoperability::of(dataset);
        let reusability = Reusability::of(dataset);

        let score = (
            accessibility.score,
            findability.score,
            interoperability.score,
            reusability.score,
        )
            .to_score();

        Self {
            accessibility,
            findability,
            interoperability,
            reusability,
            score,
        }
    }
}

impl Findability {
    pub fn of(dataset: &Dataset) -> Self {
        let title = flesch_reading_index(&dataset.title).unwrap_or(0.0);

        let description: f32 = dataset
            .description
            .as_deref()
            .and_then(flesch_reading_index)
            .unwrap_or(0.0);

        let spatial = {
            if let Some(bounding_box) = dataset.bounding_boxes.first() {
                if bounding_box.min() == bounding_box.max() {
                    match dataset.regions.first() {
                        Some(region) if !region.is_other() => QualitySpatial::RegionIdentified,
                        _ => QualitySpatial::Point,
                    }
                } else {
                    QualitySpatial::BoundingBox
                }
            } else if !dataset.regions.is_empty() {
                QualitySpatial::RegionOther
            } else {
                QualitySpatial::NoRegion
            }
        };

        let spatial_score = spatial.to_score();

        let temporal = {
            dataset.issued.is_some()
                || dataset.modified.is_some()
                || !dataset.time_ranges.is_empty()
        };

        // FIXME: The UMTHES tag should expose the `rank` value
        // given by SNS when calling AutoClassify
        let keywords =
            { ScoredIterator(dataset.tags.iter().map(|tag| !tag.is_other())).to_score() };

        let identifier = { dataset.global_identifier.is_some() };

        let score = (
            title,
            description,
            spatial_score,
            temporal,
            keywords,
            identifier,
        )
            .to_score();

        Self {
            title,
            description,
            spatial,
            spatial_score,
            temporal,
            keywords,
            identifier,
            score,
        }
    }
}

impl Accessibility {
    pub fn of(dataset: &Dataset, duplicate_landing_pages: &HashSet<[u8; 32]>) -> Self {
        let landing_page = {
            let hash = {
                let mut ctx = Sha256::new();
                ctx.update(dataset.source_url.as_bytes());
                ctx.finish()
            };

            if duplicate_landing_pages.contains(&hash) {
                QualityLandingPage::Generic
            } else {
                QualityLandingPage::Specific
            }
        };

        let landing_page_score = landing_page.to_score();

        let direct_access = {
            dataset
                .resources
                .iter()
                .any(|resource| resource.direct_link && resource.primary_content)
        };

        let publicly_accessible = { !dataset.mandatory_registration };

        let score = (landing_page_score, publicly_accessible, direct_access).to_score();

        Self {
            landing_page,
            landing_page_score,
            direct_access,
            publicly_accessible,
            score,
        }
    }
}

impl Interoperability {
    pub fn of(dataset: &Dataset) -> Self {
        let mut open_file_format = false;
        let mut media_type = false;
        let mut machine_readable_data = false;
        {
            for resource in &dataset.resources {
                if !resource.direct_link || !resource.primary_content {
                    continue;
                }

                if resource.r#type.is_open() {
                    open_file_format = true;
                }

                if !resource.r#type.is_unknown() {
                    media_type = true;
                }

                if resource.r#type.is_machine_readable() {
                    machine_readable_data = true;
                }
            }
        }

        let machine_readable_metadata = { dataset.machine_readable_source };

        let score = (
            open_file_format,
            media_type,
            machine_readable_data,
            machine_readable_metadata,
        )
            .to_score();

        Self {
            open_file_format,
            media_type,
            machine_readable_data,
            machine_readable_metadata,
            score,
        }
    }
}

impl Reusability {
    pub fn of(dataset: &Dataset) -> Self {
        let license = {
            match dataset.license.facet() {
                ["unbekannt"] => QualityLicense::NoInformation,
                ["geschlossen", "andere"]
                | ["geschlossen", "all-rights-reserved"]
                | ["geschlossen", "inspire", ..]
                | ["offen", "andere"]
                | ["offen", "geolizenz", ..] => QualityLicense::UnclearInformation,
                ["geschlossen", ..] => QualityLicense::ClearlySpecified,
                ["offen", ..] => QualityLicense::ClearlySpecifiedAndFree,
                _ => QualityLicense::NoInformation,
            }
        };

        let license_score = license.to_score();

        let contact_info = { !dataset.persons.is_empty() };
        let publisher_info = { !dataset.organisations.is_empty() };

        let score = (license_score, contact_info, publisher_info).to_score();

        Self {
            license,
            license_score,
            contact_info,
            publisher_info,
            score,
        }
    }
}

/// This categorical score indicates how precise the regional information is provided.
#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize, ToSchema, EnumCount, Display)]
pub enum QualitySpatial {
    #[default]
    NoRegion,
    RegionOther,
    BoundingBox,
    Point,
    RegionIdentified,
}

/// The Flesch Reading Ease (FRE) score is a value indicating readability for an average reader.
///
/// The implementation is taking the constants given for a German text by <https://de.wikipedia.org/wiki/Lesbarkeitsindex>
///
/// The interpretation is given by the following table:
///
/// | Flesch-Reading-Ease-Score Von … bis unter … | Lesbarkeit   | Verständlich für      |
/// |---------------------------------------------|--------------|-----------------------|
/// | 0–30                                        | Sehr schwer  | Akademiker            |
/// | 30–50                                       | Schwer       |                       |
/// | 50–60                                       | Mittelschwer |                       |
/// | 60–70                                       | Mittel       | 13–15-jährige Schüler |
/// | 70–80                                       | Mittelleicht |                       |
/// | 80–90                                       | Leicht       |                       |
/// | 90–100                                      | Sehr leicht  | 11-jährige Schüler    |
fn flesch_reading_index(text: &str) -> Option<f32> {
    struct Patterns {
        sentences: Regex,
        words: Regex,
    }

    static PATTERNS: Lazy<Patterns> = Lazy::new(|| Patterns {
        sentences: Regex::new(r"[.!?](\s+|$)|\n+").unwrap(),
        words: Regex::new(r"[\W&&[^\-_.]]+").unwrap(),
    });

    let mut num_sentences = 0;
    let mut num_words = 0;
    let mut num_syllables = 0;

    let patterns = &*PATTERNS;

    for sentence in patterns.sentences.split(text) {
        if sentence.is_empty() {
            continue;
        }

        num_sentences += 1;

        for word in patterns.words.split(sentence) {
            if word.is_empty() {
                continue;
            }

            num_words += 1;
            num_syllables += hyphenate(word, Lang::German).count();
        }
    }

    if num_words == 0 {
        return None;
    }

    num_sentences = num_sentences.max(1);

    let fre = 1.8
        - 0.01 * num_words as f32 / num_sentences as f32
        - 0.585 * num_syllables as f32 / num_words as f32;

    Some(fre.clamp(0.0, 1.0))
}

/// This categorical score indicates how precise the landing page leads to the dataset.
#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize, ToSchema, EnumCount, Display)]
pub enum QualityLandingPage {
    #[default]
    Generic,
    Specific,
}

impl QualityLandingPage {
    pub fn inherent_score(&self) -> f32 {
        match self {
            Self::Generic => 0.71, // this value was determined using ranking_optimization_landing_page.ipynb and is the optimum of two methods particle swarm optimization and differential evolution
            Self::Specific => 1.0,
        }
    }
}

/// This categorical score assesses the reusability according to the license type.
#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize, ToSchema, EnumCount, Display)]
pub enum QualityLicense {
    #[default]
    NoInformation,
    UnclearInformation,
    ClearlySpecified,
    ClearlySpecifiedAndFree,
}

trait ToScore {
    fn to_score(self) -> f32;
}

impl ToScore for f32 {
    fn to_score(self) -> f32 {
        self
    }
}

impl ToScore for bool {
    fn to_score(self) -> f32 {
        self.into()
    }
}

macro_rules! impl_to_score {
    ( @categorical $type:ident ) => {
        impl ToScore for $type {
            fn to_score(self) -> f32 {
                (self as usize as f32) / (($type::COUNT - 1) as f32)
            }
        }
    };

    ( @tuples $head:ident $(, $tail:ident )+ ) => {
        impl_to_score!( @impl_tuple $head $(, $tail )+ );

        impl_to_score!( @tuples $( $tail ),+ );
    };

    ( @tuples $head:ident ) => {};

    ( @impl_tuple $( $types:ident ),+ ) => {
        impl< $( $types ),+ > ToScore for ( $( $types ),+ )
        where
            $( $types: ToScore ),+
        {
            #[allow(non_snake_case)]
            fn to_score(self) -> f32 {
                const COUNT: usize = impl_to_score!( @count $( $types ),+ );

                let ( $( $types ),+ ) = self;

                ( 0.0 $( + $types.to_score() )+ ) / ( COUNT as f32 )
            }
        }
    };

    ( @count $head:ident $(, $tail:ident )+ ) => {
        1 + impl_to_score!( @count $( $tail ),+ )
    };

    ( @count $head:ident ) => {
        1
    };
}

impl_to_score!( @categorical QualitySpatial );

impl_to_score!( @categorical QualityLandingPage );

impl_to_score!( @categorical QualityLicense );

impl_to_score!( @tuples A, B, C, D, E, F );

struct ScoredIterator<I>(I);

impl<I, T> ToScore for ScoredIterator<I>
where
    I: Iterator<Item = T>,
    T: ToScore,
{
    fn to_score(self) -> f32 {
        let mut sum = 0.0;
        let mut cnt = 0;

        for val in self.0 {
            sum += val.to_score();
            cnt += 1;
        }

        if cnt != 0 {
            sum / (cnt as f32)
        } else {
            0.0
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn categorical_scores_use_full_range() {
        assert_eq!(QualityLicense::NoInformation.to_score(), 0.0);
        assert_eq!(QualityLicense::ClearlySpecifiedAndFree.to_score(), 1.0);
    }

    #[test]
    fn flesch_reading_index_works() {
        assert_eq!(
            flesch_reading_index("Luftschadstoff Emissionskataster"),
            Some(0.0)
        );

        assert_eq!(
            flesch_reading_index("Das ist das Haus vom Nikolaus. Und nebenan vom Weinachtsmann."),
            Some(0.87250006)
        );
    }

    #[test]
    fn flesch_reading_index_ignores_hyphens() {
        assert_eq!(
            flesch_reading_index("BfS-13-07-R-RoeV-E1-und-5-weitere32ec.pdf"),
            Some(0.0)
        );
    }
}
