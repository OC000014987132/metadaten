use std::fmt;
use std::mem::{replace, take};

use cow_utils::CowUtils;
use once_cell::sync::Lazy;
use phf::{phf_map, Map as PhfMap};
use regex::Regex;
use serde::{
    ser::{SerializeStruct, Serializer},
    Deserialize, Serialize,
};
use tantivy::schema::Facet;
use url::Url;
use utoipa::ToSchema;

use crate::TrimExt;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Deserialize, ToSchema)]
#[serde(deny_unknown_fields)]
/// A locatable resource attached to the dataset
pub struct Resource {
    #[schema(value_type = LabelledResourceType)]
    pub r#type: ResourceType,
    #[schema(format = Uri)]
    pub url: String,
    pub description: Option<String>,
    pub direct_link: bool,
    pub primary_content: bool,
}

impl Default for Resource {
    fn default() -> Self {
        Self {
            r#type: Default::default(),
            url: Default::default(),
            description: Default::default(),
            direct_link: true,
            primary_content: false,
        }
    }
}

impl_minified_serialize!(
    Resource,
    r#type as LabelledResourceType,
    url,
    description if is_none,
    direct_link,
    primary_content
);

impl Resource {
    pub fn unknown(url: String) -> Self {
        Self {
            url,
            ..Default::default()
        }
    }

    pub fn guess_or_keep_type(mut self) -> Self {
        let r#type = replace(&mut self.r#type, ResourceType::Unknown);

        self.align();

        if self.r#type == ResourceType::Unknown {
            self.r#type = r#type;
        }

        self
    }

    pub fn align(&mut self) {
        self.url = take(&mut self.url).trim();

        if self.r#type.is_unknown() {
            let guess_by_extension = |path: &str, delim: char| match path.rsplit_once(delim) {
                Some((_, "png")) => ResourceType::Png,
                Some((_, "jpeg")) => ResourceType::Jpeg,
                Some((_, "jpg")) => ResourceType::Jpeg,
                Some((_, "asc")) => ResourceType::PlainText,
                Some((_, "tif")) => ResourceType::Tiff,
                Some((_, "pdf")) => ResourceType::Pdf,
                Some((_, "txt")) => ResourceType::PlainText,
                Some((_, "doc" | "docx")) => ResourceType::MicrosoftWord,
                Some((_, "odt")) => ResourceType::OpenDocumentText,
                Some((_, "csv" | "ffcsv")) => ResourceType::Csv,
                Some((_, "xml")) => ResourceType::Xml,
                Some((_, "html" | "xhtml")) => ResourceType::WebPage,
                Some((_, "zip")) => ResourceType::Zip,
                Some((_, "kml")) => ResourceType::Kml,
                Some((_, "gml")) => ResourceType::Gml,
                Some((_, "mp3")) => ResourceType::Mp3,
                Some((_, "mp4")) => ResourceType::Mp4,
                Some((_, "shp")) => ResourceType::Shapefile,
                Some((_, "xls" | "xlsx")) => ResourceType::MicrosoftExcelSpreadsheet,
                Some((_, "ods")) => ResourceType::OpenDocumentSpeadsheet,
                Some((_, "rdf")) => ResourceType::RdfXml,
                Some((_, "gif")) => ResourceType::Gif,
                Some((_, "svg")) => ResourceType::Svg,
                _ => ResourceType::Unknown,
            };

            self.r#type = guess_by_extension(&self.url, '.');
            if self.r#type.is_unknown() {
                self.r#type = guess_by_extension(&self.url, '/');
            }

            if self.r#type.is_unknown() {
                if let Ok(url) = Url::parse(&self.url) {
                    for (key, value) in url.query_pairs() {
                        if key.eq_ignore_ascii_case("service") || key.eq_ignore_ascii_case("format")
                        {
                            self.r#type = value.cow_to_ascii_lowercase().as_ref().into();
                            break;
                        }
                    }

                    if self.r#type.is_unknown() {
                        for (key, value) in url.query_pairs() {
                            if key.eq_ignore_ascii_case("request")
                                && value.eq_ignore_ascii_case("GetCapabilities")
                            {
                                self.r#type = ResourceType::WebService;
                                break;
                            }
                        }
                    }

                    if self.r#type.is_unknown() {
                        self.r#type = guess_by_extension(url.path(), '.');
                        if self.r#type.is_unknown() {
                            self.r#type = guess_by_extension(url.path(), '/');
                        }
                    }
                }
            }
        }
    }
}

#[derive(
    Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize,
)]
pub enum ResourceType {
    #[default]
    Unknown,
    WebPage,
    XhtmlXml,
    NewsFeed,
    Document,
    PlainText,
    Rtf,
    Pdf,
    OpenDocumentSpeadsheet,
    MicrosoftExcelSpreadsheet,
    OpenDocumentText,
    MicrosoftWord,
    MicrosoftPowerpoint,
    MicrosoftAccessDatabase,
    GeoPackage,
    Gml,
    File,
    Csv,
    CsvExtended,
    GeoJson,
    Json,
    Shapefile,
    QGisProject,
    Step,
    DwgDxf,
    LasLaz,
    Kml,
    JsonLd,
    Xml,
    Image,
    Png,
    Jpeg,
    Tiff,
    WebService,
    Wms,
    Wfs,
    Wmts,
    Wcs,
    Video,
    Avi,
    Mp4,
    Audio,
    Mp3,
    Archive,
    Rar,
    Zip,
    InteractivePlot,
    RdfXml,
    Gif,
    WebApp,
    Svg,
    Css,
    ArcGisCatalogServer,
    GeoTiff,
    Notation3,
}

impl ResourceType {
    pub fn is_unknown(&self) -> bool {
        matches!(self, Self::Unknown)
    }

    /// <https://en.wikipedia.org/wiki/List_of_open_file_formats>
    pub fn is_open(&self) -> bool {
        match self {
            Self::Unknown => false,
            Self::WebPage => true,
            Self::XhtmlXml => true,
            Self::NewsFeed => true,
            Self::Document => false,
            Self::PlainText => true,
            Self::Rtf => false,
            Self::Pdf => true,
            Self::OpenDocumentSpeadsheet => true,
            Self::MicrosoftExcelSpreadsheet => false,
            Self::OpenDocumentText => true,
            Self::MicrosoftWord => false,
            Self::MicrosoftPowerpoint => false,
            Self::MicrosoftAccessDatabase => false,
            Self::GeoPackage => true,
            Self::Gml => true,
            Self::File => false,
            Self::Csv => true,
            Self::CsvExtended => true,
            Self::GeoJson => true,
            Self::Json => true,
            Self::Shapefile => false,
            Self::QGisProject => false,
            Self::Step => false,
            Self::DwgDxf => false,
            Self::LasLaz => true,
            Self::Kml => true,
            Self::JsonLd => true,
            Self::Xml => true,
            Self::Image => false,
            Self::Png => true,
            Self::Jpeg => true,
            Self::Tiff => false,
            Self::WebService => false,
            Self::Wms => true,
            Self::Wfs => true,
            Self::Wmts => true,
            Self::Wcs => true,
            Self::ArcGisCatalogServer => false,
            Self::Video => false,
            Self::Avi => false,
            Self::Mp4 => false,
            Self::Audio => false,
            Self::Mp3 => true,
            Self::Archive => false,
            Self::Rar => false,
            Self::Zip => true,
            Self::InteractivePlot => false,
            Self::RdfXml => true,
            Self::Gif => true,
            Self::WebApp => false,
            Self::Svg => true,
            Self::Css => true,
            Self::GeoTiff => true,
            Self::Notation3 => true,
        }
    }

    /// <https://en.wikipedia.org/wiki/Machine-readable_medium_and_data#Data>
    ///
    /// Generally speaking, this can be considered context sensitive,
    /// e.g. a press release conveyed as a text document might be considered machine-readable,
    /// but a time series available only as an image (e.g. a plot) is definitely not.
    ///
    // FIXME: We could model this explicitly by passing the dataset type as a parameter to this method.
    pub fn is_machine_readable(&self) -> bool {
        match self {
            Self::Unknown => false,
            Self::WebPage => false,
            Self::XhtmlXml => false,
            Self::NewsFeed => false,
            Self::Document => false,
            Self::PlainText => false,
            Self::Rtf => false,
            Self::Pdf => false,
            Self::OpenDocumentSpeadsheet => true,
            Self::MicrosoftExcelSpreadsheet => true,
            Self::OpenDocumentText => false,
            Self::MicrosoftWord => false,
            Self::MicrosoftPowerpoint => false,
            Self::MicrosoftAccessDatabase => true,
            Self::GeoPackage => true,
            Self::Gml => true,
            Self::File => false,
            Self::Csv => true,
            Self::CsvExtended => true,
            Self::GeoJson => true,
            Self::Json => true,
            Self::Shapefile => true,
            Self::QGisProject => true,
            Self::Step => true,
            Self::DwgDxf => true,
            Self::LasLaz => true,
            Self::Kml => true,
            Self::JsonLd => true,
            Self::Xml => true,
            Self::Image => false,
            Self::Png => false,
            Self::Jpeg => false,
            Self::Tiff => false,
            Self::WebService => true,
            Self::Wms => true,
            Self::Wfs => true,
            Self::Wmts => true,
            Self::Wcs => true,
            Self::ArcGisCatalogServer => true,
            Self::Video => false,
            Self::Avi => false,
            Self::Mp4 => false,
            Self::Audio => false,
            Self::Mp3 => false,
            Self::Archive => false,
            Self::Rar => false,
            Self::Zip => false,
            Self::InteractivePlot => false,
            Self::RdfXml => true,
            Self::Gif => false,
            Self::WebApp => false,
            Self::Svg => false,
            Self::Css => false,
            Self::GeoTiff => true,
            Self::Notation3 => true,
        }
    }

    pub fn default_facet() -> &'static [&'static str] {
        &["Keine"]
    }

    /// A facet to sort resources based on their supossed usablity for certain tasks.
    ///
    /// In general, the following facets are resources designed for human consumption:
    ///     - Dokument
    ///     - Webseite
    ///     - Bild
    ///     - Multimedia
    /// while the facets below contain resources to be interpreted by machines:
    ///     - Webdienst
    ///     - Datei
    pub fn facet(&self) -> &'static [&'static str] {
        match self {
            Self::Unknown => &["Unbekannt"],

            Self::WebPage => &["Webseite"],
            Self::XhtmlXml => &["Webseite", "XHTML"],
            Self::NewsFeed => &["Webseite", "Newsfeed"],
            Self::WebApp => &["Webseite", "Web-Anwendung"],
            Self::Css => &["Webseite", "CSS"],

            Self::Document => &["Dokument"],
            Self::PlainText => &["Dokument", "Textdatei"],
            Self::MicrosoftWord => &["Dokument", "Microsoft Word"],
            Self::OpenDocumentText => &["Dokument", "OpenDocument"],
            Self::Rtf => &["Dokument", "RTF"],
            Self::Pdf => &["Dokument", "PDF"],
            Self::MicrosoftPowerpoint => &["Dokument", "Powerpoint"],

            Self::File => &["Datei"],
            Self::Csv => &["Datei", "Tabelle", "CSV"],
            Self::CsvExtended => &["Datei", "Tabelle", "CSV", "Extended CSV"],
            Self::Json => &["Datei", "JSON"],
            Self::JsonLd => &["Datei", "JSON", "JSON-LD"],
            Self::Xml => &["Datei", "XML"],
            Self::RdfXml => &["Datei", "XML", "RDF (XML)"],
            Self::MicrosoftExcelSpreadsheet => &["Datei", "Tabelle", "Excel Spreadsheet"],
            Self::MicrosoftAccessDatabase => &["Datei", "Access Database"],
            Self::OpenDocumentSpeadsheet => &["Datei", "Tabelle", "Open Document Spreadsheet"],
            Self::QGisProject => &["Datei", "Geodaten", "QGIS-Projekt"],
            Self::GeoPackage => &["Datei", "Geodaten", "GeoPackage"],
            Self::GeoJson => &["Datei", "Geodaten", "GeoJSON"],
            Self::Gml => &["Datei", "Geodaten", "GML"],
            Self::Kml => &["Datei", "Geodaten", "KML"],
            Self::Shapefile => &["Datei", "Geodaten", "Shapefile"],
            Self::Step => &["Datei", "Geodaten", "STEP"],
            Self::DwgDxf => &["Datei", "Geodaten", "DWG DXF"],
            Self::LasLaz => &["Datei", "Geodaten", "LAS LAZ"],
            Self::GeoTiff => &["Datei", "Geodaten", "GeoTIFF"],
            Self::Notation3 => &["Datei", "Notation3"],

            Self::Audio => &["Multimedia", "Audio"],
            Self::Mp3 => &["Multimedia", "Audio", "MP3"],

            Self::Video => &["Multimedia", "Video"],
            Self::Avi => &["Multimedia", "Video", "AVI"],
            Self::Mp4 => &["Multimedia", "Video", "MP4"],

            Self::Image => &["Bild"],
            Self::Png => &["Bild", "PNG"],
            Self::Jpeg => &["Bild", "JPEG"],
            Self::Tiff => &["Bild", "TIFF"],
            Self::Gif => &["Bild", "GIF"],
            Self::InteractivePlot => &["Bild", "Interaktives Diagramm"],
            Self::Svg => &["Bild", "SVG"],

            Self::WebService => &["Webdienst"],
            Self::Wms => &["Webdienst", "WMS"],
            Self::Wfs => &["Webdienst", "WFS"],
            Self::Wmts => &["Webdienst", "WMTS"],
            Self::Wcs => &["Webdienst", "WCS"],
            Self::ArcGisCatalogServer => &["Webdienst", "ArcGIS Catalog Server"],

            Self::Archive => &["Archiv"],
            Self::Rar => &["Archiv", "RAR"],
            Self::Zip => &["Archiv", "ZIP"],
        }
    }

    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Unknown => "Unbekannt",
            Self::WebPage => "Webseite",
            Self::NewsFeed => "News Feed",
            Self::WebApp => "Web-Anwendung",
            Self::Document => "Dokument",
            Self::PlainText => "Textdokument",
            Self::Rtf => "Rich Text Format (RTF)",
            Self::Pdf => "PDF",
            Self::OpenDocumentText => "Open Document Text",
            Self::MicrosoftWord => "Microsoft Word Document",
            Self::MicrosoftPowerpoint => "Microsoft PowerPoint Presentation",
            Self::MicrosoftAccessDatabase => "Microsoft Access Database",
            Self::GeoPackage => "GeoPackage",
            Self::Gml => "GML (Geography Markup Language)",
            Self::File => "Datei",
            Self::Csv => "CSV",
            Self::Css => "CSS",
            Self::CsvExtended => "Extended CSV",
            Self::MicrosoftExcelSpreadsheet => "Microsoft Excel Spreadsheet",
            Self::OpenDocumentSpeadsheet => "Open Document Spreadsheet",
            Self::GeoJson => "GeoJSON",
            Self::Json => "JSON",
            Self::Shapefile => "Shapefile",
            Self::QGisProject => "QGIS-Projekt",
            Self::Step => "STEP",
            Self::DwgDxf => "DWG/DXF",
            Self::LasLaz => "LAS/LAZ",
            Self::Kml => "KML (Keyhole Markup Language)",
            Self::JsonLd => "JSON-LD",
            Self::Xml => "XML",
            Self::XhtmlXml => "XHTML/XML",
            Self::Image => "Bild",
            Self::Png => "PNG",
            Self::Jpeg => "JPEG",
            Self::Tiff => "TIFF",
            Self::Gif => "GIF",
            Self::Svg => "SVG",
            Self::WebService => "Web Service",
            Self::Wms => "WMS (Web Map Service)",
            Self::Wfs => "WFS (Web Feature Service)",
            Self::Wmts => "WMTS (Web Map Tile Service)",
            Self::Wcs => "WCS (Web Coverage Service)",
            Self::ArcGisCatalogServer => "ArcGIS Catalog Server",
            Self::Video => "Video",
            Self::Avi => "AVI",
            Self::Mp4 => "MP4",
            Self::Audio => "Audio",
            Self::Mp3 => "MP3",
            Self::Archive => "Archiv",
            Self::Rar => "RAR",
            Self::Zip => "ZIP",
            Self::InteractivePlot => "Interaktives Diagramm",
            Self::RdfXml => "RDF/XML",
            Self::GeoTiff => "GeoTIFF",
            Self::Notation3 => "Notation3",
        }
    }

    pub fn mimetype(&self) -> Option<&'static str> {
        let mimetype = match self {
            Self::WebPage => "text/html",
            Self::NewsFeed => "application/rss+xml",
            Self::WebApp => "application/x-web-app-manifest+json",
            Self::PlainText => "text/plain",
            Self::Rtf => "application/rtf",
            Self::Pdf => "application/pdf",
            Self::OpenDocumentText => "application/vnd.oasis.opendocument.text",
            Self::MicrosoftWord => {
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            }
            Self::MicrosoftPowerpoint => {
                "application/vnd.openxmlformats-officedocument.presentationml.presentation"
            }
            Self::MicrosoftAccessDatabase => "application/msaccess",
            Self::GeoPackage => "application/geopackage+sqlite3",
            Self::Gml => "application/gml+xml",
            Self::Csv => "text/csv",
            Self::CsvExtended => "text/csv",
            Self::Css => "text/css",
            Self::MicrosoftExcelSpreadsheet => {
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            }
            Self::OpenDocumentSpeadsheet => "application/vnd.oasis.opendocument.spreadsheet",
            Self::GeoJson => "application/geo+json",
            Self::Json => "application/json",
            Self::QGisProject => "application/x-qgis-project",
            Self::Step => "model/step", // According to https://reference.wolfram.com/language/ref/format/STEP.html.en
            Self::DwgDxf => "application/vnd.dwg",
            Self::LasLaz => "application/vnd.las",
            Self::Kml => "application/vnd.google-earth.kml+xml",
            Self::JsonLd => "application/ld+json",
            Self::Xml => "application/xml",
            Self::XhtmlXml => "application/xhtml+xml",
            Self::Png => "image/png",
            Self::Jpeg => "image/jpeg",
            Self::Tiff => "image/tiff",
            Self::Gif => "image/gif",
            Self::Svg => "image/svg+xml",
            Self::Wms => "application/vnd.ogc.wms_xml",
            Self::Wfs => "application/vnd.ogc.gml",
            Self::Wmts => "application/vnd.ogc.wmts_xml",
            Self::Wcs => "application/vnd.ogc.wcs",
            Self::Avi => "video/x-msvideo",
            Self::Mp4 => "video/mp4",
            Self::Mp3 => "audio/mpeg",
            Self::Rar => "application/vnd.rar",
            Self::Zip => "application/zip",
            Self::RdfXml => "application/rdf+xml",
            Self::GeoTiff => "image/tiff", // following https://docs.ogc.org/is/19-008r4/19-008r4.html
            Self::Notation3 => "text/n3",
            Self::Unknown
            | Self::Document
            | Self::Archive
            | Self::InteractivePlot
            | Self::WebService
            | Self::Image
            | Self::Shapefile
            | Self::File
            | Self::Video
            | Self::ArcGisCatalogServer
            | Self::Audio => {
                return None;
            }
        };
        Some(mimetype)
    }
}

impl From<&'_ ResourceType> for ResourceType {
    fn from(val: &ResourceType) -> Self {
        *val
    }
}

impl From<&'_ str> for ResourceType {
    fn from(val: &str) -> Self {
        static IANA: Lazy<Regex> = Lazy::new(|| {
            Regex::new(r"^https?\://www\.iana\.org/assignments/media-types/(\S+)").unwrap()
        });

        let val = IANA
            .captures(val)
            .map_or(val, |captures| captures.get(1).unwrap().as_str());

        static TYPES: PhfMap<&str, ResourceType> = phf_map! {
            "application/zip" => ResourceType::Zip,
            "textfile" => ResourceType::PlainText,
            "text/plain" => ResourceType::PlainText,
            "txt" => ResourceType::PlainText,
            "csv" => ResourceType::Csv,
            "CSV" => ResourceType::Csv,
            "ffcsv" => ResourceType::Csv,
            "text/csv" => ResourceType::Csv,
            "text/tab-separated-values" => ResourceType::Csv,
            "text/csv+extended" => ResourceType::CsvExtended,
            "pdf" => ResourceType::Pdf,
            "PDF" => ResourceType::Pdf,
            "application/pdf" => ResourceType::Pdf,
            "application/vnd.oasis.opendocument.spreadsheet" => ResourceType::OpenDocumentSpeadsheet,
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => ResourceType::MicrosoftExcelSpreadsheet,
            "application/vnd.ms-excel" => ResourceType::MicrosoftExcelSpreadsheet,
            "application/excel" => ResourceType::MicrosoftExcelSpreadsheet,
            "XLS" => ResourceType::MicrosoftExcelSpreadsheet,
            "XLSX" => ResourceType::MicrosoftExcelSpreadsheet,
            "application/vnd.ms-powerpoint" => ResourceType::MicrosoftPowerpoint,
            "application/vnd.ms-access_mde" => ResourceType::MicrosoftAccessDatabase,
            "application/msword" => ResourceType::MicrosoftWord,
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => ResourceType::MicrosoftWord,
            "xml" => ResourceType::Xml,
            "XML" => ResourceType::Xml,
            "application/xml" => ResourceType::Xml,
            "text/xml" => ResourceType::Xml,
            "application/xhtml+xml" => ResourceType::XhtmlXml,
            "text/css" => ResourceType::Css,
            "application/json" => ResourceType::Json,
            "JSON" => ResourceType::Json,
            "text/rtf" => ResourceType::Rtf,
            "application/rtf" => ResourceType::Rtf,
            "image/png" => ResourceType::Png,
            "image/jpeg" => ResourceType::Jpeg,
            "image/tiff" => ResourceType::Tiff,
            "LAS/LAZ, Tiff" => ResourceType::Tiff,
            "image/gif" => ResourceType::Gif,
            "image/svg" => ResourceType::Svg,
            "image/svg+xml" => ResourceType::Svg,
            "video/avi" => ResourceType::Avi,
            "video/mp4" => ResourceType::Mp4,
            "text/html" => ResourceType::WebPage,
            "HTML" => ResourceType::WebPage,
            "wms" => ResourceType::Wms,
            "wms_xml" => ResourceType::Wms,
            "wms_srvc" => ResourceType::Wms,
            "WMS_SRVC" => ResourceType::Wms,
            "application/vnd.ogc.wms_xml" => ResourceType::Wms,
            "wfs_srvc" => ResourceType::Wfs,
            "WFS_SRVC" => ResourceType::Wfs,
            "wfs" => ResourceType::Wfs,
            "application/vnd.ogc.wfs_xml" => ResourceType::Wfs,
            "application/vnd.ogc.wmts_xml" => ResourceType::Wmts,
            "application/vnd.ogc.wcs_xml" => ResourceType::Wcs,
            "application/vnd.rar" => ResourceType::Rar,"application/rar" => ResourceType::Rar,
            "application/x-zip-compressed" => ResourceType::Zip,
            "audio/mp3" => ResourceType::Mp3,
            "application/ld+json" => ResourceType::JsonLd,
            "JSON_LD" => ResourceType::JsonLd,
            "application/geo+json" => ResourceType::GeoJson,
            "application/vnd.geo+json" => ResourceType::GeoJson,
            "GEOJSON" => ResourceType::GeoJson,
            "application/geopackage+sqlite3" => ResourceType::GeoPackage,
            "GPKG" => ResourceType::GeoPackage,
            "application/gpkg" => ResourceType::GeoPackage,
            "application/vnd.google-earth.kml+xml" => ResourceType::Kml,
            "KML" => ResourceType::Kml,
            "application/gml+xml" => ResourceType::Gml,
            "application/openlayers" => ResourceType::Wms,
            "application/vnd.shp" => ResourceType::Shapefile,
            "SHP" => ResourceType::Shapefile,
            "dwg/dxf" => ResourceType::DwgDxf,
            "LAS/LAZ" => ResourceType::LasLaz,
            "GEOTIFF" => ResourceType::GeoTiff,
            "application/atom+xml" => ResourceType::NewsFeed,
            "ATOM" => ResourceType::NewsFeed,
            "application/x-qgis" => ResourceType::QGisProject,
            "application/p21" => ResourceType::Step,
            "application/rdf+xml" => ResourceType::RdfXml,
            "N3" => ResourceType::Notation3,
        };

        match TYPES.get(val).copied() {
            Some(r#type) => r#type,
            None => {
                tracing::trace!("Failed to parse resource type: '{}'", val);

                Self::Unknown
            }
        }
    }
}

impl From<Option<&'_ str>> for ResourceType {
    fn from(val: Option<&str>) -> Self {
        match val {
            Some(val) => val.into(),
            None => Self::Unknown,
        }
    }
}

impl fmt::Display for ResourceType {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(self.as_str())
    }
}

#[derive(Debug, Serialize, ToSchema)]
pub struct LabelledResourceType<'a> {
    #[schema(value_type = String)]
    path: Facet,
    label: &'a str,
}

impl<'a> From<&'a ResourceType> for LabelledResourceType<'a> {
    fn from(val: &'a ResourceType) -> Self {
        Self {
            path: Facet::from_path(val.facet()),
            label: val.as_str(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_json::to_string;

    #[test]
    fn serialization_of_raw_identifiers_works() {
        assert_eq!(
            to_string(&Resource::unknown("http://foo/bar".to_owned())).unwrap(),
            r#"{"type":{"path":"/Unbekannt","label":"Unbekannt"},"url":"http://foo/bar","direct_link":true,"primary_content":false}"#
        );
    }

    #[test]
    fn guess_by_extension_works() {
        let mut resource = Resource::unknown("https://open-data.dortmund.de/api/v2/catalog/datasets/wetterdaten-brueckstrasse-45-2020/exports/kml".to_owned());
        resource.align();
        assert_eq!(resource.r#type, ResourceType::Kml);

        let mut resource = Resource::unknown("https://open-data.dortmund.de/api/v2/catalog/datasets/wetterdaten-brueckstrasse-45-2020/export.kml".to_owned());
        resource.align();
        assert_eq!(resource.r#type, ResourceType::Kml);
    }

    #[test]
    fn guess_from_service_url_works() {
        let mut resource = Resource::unknown("https://geoportal.bafg.de/arcgis1/rest/services/INSPIRE/ManMadeObject/MapServer/exts/InspireView/service?VERSION=1.3.0&SERVICE=WMS&REQUEST=Getcapabilities".to_owned());
        resource.align();
        assert_eq!(resource.r#type, ResourceType::Wms);
    }
}
