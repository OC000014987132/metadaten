use std::fmt;

use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

/// Status of data described in dataset.
///  Corresponds to https://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ProgressCode used in CSW
#[derive(
    Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize, Default, ToSchema, Clone,
)]
pub enum Status {
    #[default]
    Active,
    Obsolete,
    Planned,
    UnderDevelopment,
}

impl Status {
    pub fn as_str(&self) -> &str {
        match self {
            Self::Active => "Aktiv",
            Self::Obsolete => "Obsolet",
            Self::Planned => "Geplant",
            Self::UnderDevelopment => "In Entwicklung",
        }
    }

    pub fn inherent_score(&self) -> f32 {
        match self {
            Self::Active => 1.0,
            Self::Obsolete => 0.6,
            Self::Planned => 0.95,
            Self::UnderDevelopment => 0.95,
        }
    }
}

impl fmt::Display for Status {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(self.as_str())
    }
}
