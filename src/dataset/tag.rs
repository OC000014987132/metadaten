use std::fmt;

use compact_str::CompactString;
use memchr::memrchr_iter;
use phf::Map as PhfMap;
use serde::{
    ser::{SerializeSeq, Serializer},
    Deserialize, Serialize,
};
use tantivy::schema::Facet;
use utoipa::ToSchema;

use crate::{umthes::TAGS as UMTHES_TAGS, TrimExt};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub enum Tag {
    Other(CompactString),
    Umthes(u64),
}

impl From<&'_ Tag> for Tag {
    fn from(val: &Tag) -> Self {
        val.clone()
    }
}

impl From<CompactString> for Tag {
    fn from(val: CompactString) -> Self {
        Self::Other(val)
    }
}

impl From<&'_ str> for Tag {
    fn from(val: &str) -> Self {
        Self::Other(val.into())
    }
}

impl fmt::Display for Tag {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            Self::Other(val) => val,
            Self::Umthes(id) => return fmt.write_str(&UMTHES_TAGS.resolve(*id).label),
        };

        fmt.write_str(val)
    }
}

impl Tag {
    pub fn is_other(&self) -> bool {
        matches!(self, Self::Other(_))
    }

    pub fn url(&self) -> Option<String> {
        match self {
            Self::Other(_) => None,
            Self::Umthes(id) => Some(format!("https://sns.uba.de/umthes/_{id:08x}")),
        }
    }

    pub fn with_token<F>(&self, mut f: F)
    where
        F: FnMut(&str),
    {
        match self {
            Self::Other(val) => f(val),
            Self::Umthes(id) => {
                let tag_def = UMTHES_TAGS.resolve(*id);

                for label in tag_def.labels() {
                    f(label);
                }
            }
        }
    }

    pub fn default_facet() -> Facet {
        Facet::from_path(["Weitere"])
    }

    pub fn with_facet<F>(&self, mut f: F)
    where
        F: FnMut(Facet),
    {
        match self {
            Self::Other(_) => f(Self::default_facet()),
            Self::Umthes(id) => {
                let tag_def = &UMTHES_TAGS.resolve(*id);

                for facet in &tag_def.facets {
                    let (top_level_facets, encoded) = resolve_top_level_facets(facet.encoded_str());

                    for top_level_facet in top_level_facets {
                        let mut buf = Vec::with_capacity(top_level_facet.len() + encoded.len());
                        buf.extend(top_level_facet.as_bytes());
                        buf.extend(encoded.as_bytes());

                        f(Facet::from_encoded(buf).unwrap());
                    }
                }
            }
        }
    }

    pub fn align(self) -> Option<Self> {
        match self {
            Self::Other(val) => {
                let val = val.trim();

                if val.is_empty() {
                    return None;
                }

                Some(Self::Other(val))
            }
            val => Some(val),
        }
    }

    pub fn depth(&self) -> u8 {
        match self {
            Self::Other(_val) => 0,
            Self::Umthes(id) => {
                let tag_def = &UMTHES_TAGS.resolve(*id);

                1 + tag_def.depth
            }
        }
    }

    /// Luft
    pub const AIR: Self = Self::Umthes(0x00040282);

    /// Boden
    pub const SOIL: Self = Self::Umthes(0x00029014);

    /// Fluß
    pub const RIVER: Self = Self::Umthes(0x00028841);

    /// Meer
    pub const SEA: Self = Self::Umthes(0x00028866);

    /// Grundwasser
    pub const GROUNDWATER: Self = Self::Umthes(0x00028869);

    /// Oberflächenwasser
    pub const SURFACEWATER: Self = Self::Umthes(0x00028792);

    /// Erdbeben
    pub const GEOPHYSICS: Self = Self::Umthes(0x00010958);

    /// Radioaktivität
    pub const RADIOACTIVITY: Self = Self::Umthes(0x00020155);

    /// Biologie
    pub const BIOLOGY: Self = Self::Umthes(0x00005053);

    /// Chemie
    pub const CHEMISTRY: Self = Self::Umthes(0x00005981);

    /// Chemie
    pub const NONIONISINGRADIATION: Self = Self::Umthes(0x00018059);

    /// Wasserrahmenrichtlinie
    pub const WRRL: Self = Self::Umthes(0x00048488);

    /// Übereinkommen Oslo/Paris zur Überwachung der Nordsee
    pub const OSPAR: Self = Self::Umthes(0x00018937);

    /// Helsinki-Konvention zur Überwachung der Ostsee
    pub const HELCOM: Self = Self::Umthes(0x00046210);

    /// Umweltberichterstattung Meer
    pub const SOER: Self = Self::Umthes(0x00025139);

    /// Richtlinie 91/676/EWG EU-Nitratrichtlinie
    pub const NITRATRICHTLINIE: Self = Self::Umthes(0x00046552);

    /// Hochwasserrisikomanagement-Richtlinie
    pub const HWRM_RL: Self = Self::Umthes(0x00657869);

    /// Meeresstrategie-Rahmenrichtlinie
    pub const MSR_RL: Self = Self::Umthes(0x00652832);

    /// Badegewässerrichtlinie
    pub const BGRL: Self = Self::Umthes(0x00606581);

    /// Trinkwasserrichtlinie
    pub const TWRL: Self = Self::Umthes(0x00605121);

    /// FFH-Richtlinie
    pub const FFHRL: Self = Self::Umthes(0x00046113);

    /// FFH-Lebensraumtyp
    pub const FFHLRT: Self = Self::Umthes(0x00600911);

    /// Spezies
    pub const SPECIES: Self = Self::Umthes(0x00030221);

    /// Vogel
    pub const BIRD: Self = Self::Umthes(0x00026619);

    /// Ortsdosisleistung
    pub const ODL: Self = Self::Umthes(0x00500351);

    /// Pollutant Release and Transfer Register
    pub const PRTR: Self = Self::Umthes(0x00600572);

    /// Umweltverträglichkeitsprüfung
    pub const UVP: Self = Self::Umthes(0x00025362);

    /// Citizen Science
    pub const CITIZEN_SCIENCE: Self = Self::Umthes(0x00667953);

    /// Water
    pub const WATER: Self = Self::Umthes(0x00028954);

    /// Industrieemissionsrichtlinie
    pub const IED: Self = Self::Umthes(0x00656573);

    /// IEP-Verordnung
    pub const IEP: Self = Self::Umthes(0xb78c808f);

    /// Wirtschaftszweig
    pub const BRANCHEN: Self = Self::Umthes(0x00027921);

    /// Rasterdaten
    pub const RASTERDATEN: Self = Self::Umthes(0x00049342);
}

const ATMOSPHÄRE: &str = "Luft\0Atmosphäre\0";
const EMISSIONEN_TREIBHAUSGASE: &str = "Luft\0Emissionen & Treibhausgase\0";
const WETTER: &str = "Luft\0Wetter\0";
const LUFTVERSCHMUTZUNG_QUALITAET: &str = "Luft\0Luftverschmutzung & -qualität\0";
const KLIMASCHUTZ_WANDEL: &str = "Luft\0Klimaschutz & -wandel\0";
const WINDKRAFT: &str = "Luft\0Windkraft\0";
const WETTEREXTREMEREIGNISSE: &str = "Luft\0Wetterextremereignisse\0";

const FLUESSE_SEEN: &str = "Wasser\0Flüsse & Seen\0";
const MEERE_OZEANE: &str = "Wasser\0Meere & Ozeane\0";
const WASSERVERSCHMUTZUNG_QUALITAET: &str = "Wasser\0Wasserverschmutzung & -qualität\0";
const TRINKWASSER_GRUNDWASSER: &str = "Wasser\0Trinkwasser & Grundwasser\0";
const GEWAESSERSCHUTZ_ZUSTAND: &str = "Wasser\0Gewässerschutz & -zustand\0";
const WASSEREXTREMEREIGNISSE: &str = "Wasser\0Wasserextremereignisse\0";

const BOEDEN_GESTEINE_MINERALE: &str = "Boden\0Böden, Gesteine & Minerale\0";
const BODENVERSCHMUTZUNG_QUALITAET: &str = "Boden\0Bodenqualität & -verschmutzung\0";
const BODEN_RESSOURCENNUTZUNG: &str = "Boden\0Boden- & Ressourcennutzung\0";
const BODENSCHUTZ_DEGRADIERUNG: &str = "Boden\0Bodenschutz & -degradierung\0";
const DUERRE: &str = "Boden\0Dürre\0";

const LEBEWESEN_LEBENSGEMEINSCHAFTEN: &str =
    "Lebewesen & Lebensräume\0Lebewesen & Lebensgemeinschaften\0";
const LANDSCHAFTEN_LEBENSRAEUME_BIOTOPE: &str =
    "Lebewesen & Lebensräume\0Landschaften, Lebensräume & Biotope\0";
const BIODIVERSITAET: &str = "Lebewesen & Lebensräume\0Biodiversität\0";
const ARTENSCHUTZ_SCHUTZGEBIETE: &str = "Lebewesen & Lebensräume\0Artenschutz & Schutzgebiete\0";
const INVASIVE_ARTEN: &str = "Lebewesen & Lebensräume\0Invasive Arten\0";
const NUTZTIERE_PFLANZEN_KULTURLANDSCHAFTEN: &str =
    "Lebewesen & Lebensräume\0Nutztiere & -pflanzen, Kulturlandschaften\0";
const ARTENSTERBEN_BIODIVERSITAETSVERLUST_LEBENSRAUMVERLUST: &str =
    "Lebewesen & Lebensräume\0Artensterben, Biodiversitätsverlust, Lebensraumverlust\0";

const RECHT_POLITIK_VERWALTUNG_PLANUNG: &str =
    "Mensch & Umwelt\0Recht, Politik, Verwaltung, Planung\0";
const UMWELTBILDUNG_WISSENSCHAFT: &str = "Mensch & Umwelt\0Umweltbildung & -wissenschaft\0";
const GESELLSCHAFT_SOZIALES: &str = "Mensch & Umwelt\0Gesellschaft & Soziales\0";
const GESUNDHEIT_ERNAEHRUNG: &str = "Mensch & Umwelt\0Gesundheit & Ernährung\0";
const RISIKO_SICHERHEIT: &str = "Mensch & Umwelt\0Risiko & Sicherheit\0";
const ENERGIE_RESSOURCEN_WIRTSCHAFT: &str = "Mensch & Umwelt\0Energie, Ressourcen, Wirtschaft\0";
const ERHOLUNG_TOURISMUS: &str = "Mensch & Umwelt\0Erholung & Tourismus\0";
const INFRASTRUKTUR_URBANER_RAUM: &str = "Mensch & Umwelt\0Infrastruktur & urbaner Raum\0";

include!(concat!(env!("OUT_DIR"), "/top_level_facets.rs"));

fn resolve_top_level_facets(encoded: &str) -> (&[&str], &str) {
    let mut components = memrchr_iter(0, encoded.as_bytes());

    if let Some(facets) = TOP_LEVEL_FACETS.get(encoded) {
        let matched = match components.next() {
            Some(position) => &encoded[position + 1..],
            None => encoded,
        };

        return (facets, matched);
    }

    while let Some(position) = components.next() {
        let key = &encoded[..position];

        if let Some(facets) = TOP_LEVEL_FACETS.get(key) {
            let matched = match components.next() {
                Some(position) => &encoded[position + 1..],
                None => encoded,
            };

            return (facets, matched);
        }
    }

    (&["Weitere\0"], encoded)
}

#[derive(Serialize, ToSchema)]
pub enum LabelledTag<'a> {
    Other(&'a str),
    /// Concept from Umwelthesaurus
    ///
    /// Resolves via <https://sns.uba.de/umthes/de/concepts/_[ID].html>
    Umthes {
        id: u64,
        label: &'a str,
    },
}

impl Tag {
    pub fn with_label<F, T>(&self, f: F) -> T
    where
        F: FnOnce(LabelledTag<'_>) -> T,
    {
        let tag_def;

        let tag = match self {
            Tag::Other(val) => LabelledTag::Other(val),
            Tag::Umthes(id) => {
                tag_def = UMTHES_TAGS.resolve(*id);

                LabelledTag::Umthes {
                    id: *id,
                    label: &tag_def.label,
                }
            }
        };

        f(tag)
    }
}

pub struct TagResolver<'a>(&'a [Tag]);

impl Serialize for TagResolver<'_> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_seq(Some(self.0.len()))?;

        for tag in self.0 {
            tag.with_label(|tag| state.serialize_element(&tag))?;
        }

        state.end()
    }
}

impl<'a> From<&'a Vec<Tag>> for TagResolver<'a> {
    fn from(val: &'a Vec<Tag>) -> Self {
        Self(val)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn resolve_top_level_facets_works() {
        let (facets, encoded) = resolve_top_level_facets("Ökonomie und Finanzen");
        assert_eq!(
            facets,
            &[GESELLSCHAFT_SOZIALES, ENERGIE_RESSOURCEN_WIRTSCHAFT]
        );
        assert_eq!(encoded, "Ökonomie und Finanzen");

        let (facets, encoded) =
            resolve_top_level_facets("Ökonomie und Finanzen\0Öffentlicher Haushalt");
        assert_eq!(
            facets,
            &[GESELLSCHAFT_SOZIALES, ENERGIE_RESSOURCEN_WIRTSCHAFT]
        );
        assert_eq!(encoded, "Ökonomie und Finanzen\0Öffentlicher Haushalt");

        let (facets, encoded) =
            resolve_top_level_facets("Physikalische Erscheinungen und Prozesse\0Explosion");
        assert_eq!(facets, &[RISIKO_SICHERHEIT]);
        assert_eq!(encoded, "Explosion");

        let (facets, encoded) = resolve_top_level_facets(
            "Physikalische Erscheinungen und Prozesse\0Explosion\0Feuerwerk",
        );
        assert_eq!(facets, &[RISIKO_SICHERHEIT]);
        assert_eq!(encoded, "Explosion\0Feuerwerk");

        let (facets, encoded) = resolve_top_level_facets(
            "Allgemeine und übergreifende Begriffe - Umwelt, Umweltmedien\0Umweltmedien\0Boden",
        );
        assert_eq!(facets, &[BOEDEN_GESTEINE_MINERALE]);
        assert_eq!(encoded, "Boden");
    }
}
