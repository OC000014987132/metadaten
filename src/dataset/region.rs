use std::fmt;

use compact_str::CompactString;
use geo::{
    algorithm::{BoundingRect, Contains},
    MultiPolygon, Rect,
};
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{
    ser::{SerializeSeq, Serializer},
    Deserialize, Serialize,
};
use smallvec::SmallVec;
use utoipa::ToSchema;

use crate::{ars_ags::ARS_AGS, atkis::ATKIS, geonames::GEO_NAMES, wise::WISE, TrimExt};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub enum Region {
    Other(CompactString),
    GeoName(u64),
    RegionalKey(u64),
    Atkis(u64),
    Watershed(u64),
}

impl From<&'_ Region> for Region {
    fn from(val: &Region) -> Self {
        val.clone()
    }
}

impl Region {
    pub fn is_other(&self) -> bool {
        matches!(self, Self::Other(_))
    }

    pub fn url(&self) -> Option<String> {
        let val = match self {
            Self::Other(_) | Self::Atkis(_) => return None,
            Self::GeoName(id) => format!("https://www.geonames.org/{id}/"),
            Self::RegionalKey(key) => {
                format!("https://www.dcat-ap.de/def/politicalGeocoding/regionalKey/#{key:012}")
            }
            Self::Watershed(id) => {
                let entry = WISE.resolve(*id);

                format!(
                    "http://dd.eionet.europa.eu/vocabulary/wise/SpatialUnit/euSubUnitCode.{}",
                    entry.id
                )
            }
        };

        Some(val)
    }

    pub fn bounding_box(&self) -> Option<Rect> {
        match self {
            Self::Other(_) => None,
            Self::GeoName(id) => {
                let place_name = GEO_NAMES.resolve(*id);

                place_name.coord.map(|coord| Rect::new(coord, coord))
            }
            Self::RegionalKey(key) => {
                let entry = ARS_AGS.resolve(*key);

                entry.bounding_box
            }
            Self::Atkis(key) => {
                let entry = ATKIS.resolve(*key);

                entry.bounding_box
            }
            Self::Watershed(id) => {
                let entry = WISE.resolve(*id);

                entry.shape.bounding_rect()
            }
        }
    }

    pub fn shape(&self) -> Option<MultiPolygon> {
        match self {
            Self::Other(_) => None,
            Self::GeoName(_id) => None,
            Self::RegionalKey(key) => {
                let entry = ARS_AGS.resolve(*key);

                entry.shape.clone()
            }
            Self::Atkis(_key) => None,
            Self::Watershed(id) => {
                let entry = WISE.resolve(*id);

                Some(MultiPolygon::new(vec![entry.shape.clone()]))
            }
        }
    }

    pub fn with_token<F>(self, mut f: F)
    where
        F: FnMut(String),
    {
        match self {
            Self::Other(val) => f(val.into()),
            Self::GeoName(id) => {
                let place_name = GEO_NAMES.resolve(id);

                f(place_name.name);
            }
            Self::RegionalKey(key) => {
                let entry = ARS_AGS.resolve(key);

                f((*entry.name).to_owned());
            }
            Self::Atkis(key) => {
                let entry = ATKIS.resolve(key);

                f((*entry.name).to_owned());
            }
            Self::Watershed(id) => {
                let entry = WISE.resolve(id);

                f(entry.name.clone());
            }
        }
    }

    pub fn align(self) -> Option<Self> {
        self.align_in(None)
    }

    pub fn align_in(self, region: Option<&Region>) -> Option<Self> {
        match self {
            Self::Other(val) => {
                let val = val.trim();

                if val.is_empty() || val == "Raumbezug des Datensatzes" {
                    return None;
                }

                if val.len() == 2 {
                    if let Some(key) = FEDERAL_STATES.get(&*val) {
                        return Some(key.clone());
                    }
                }

                static FEDERAL_STATE_WITH_KEY: Lazy<Regex> =
                    Lazy::new(|| Regex::new(r".+\s+\((\d\d)\)$").unwrap());

                if let Some(captures) = FEDERAL_STATE_WITH_KEY.captures(&val) {
                    if let Ok(key) = captures[1].parse::<u64>() {
                        if (1..=16).contains(&key) {
                            return Some(Region::RegionalKey(key * 10 * 100 * 10000 * 1000));
                        }
                    }
                }

                let bounding_box = region.as_ref().and_then(|region| region.bounding_box());
                let shape = region.as_ref().and_then(|region| region.shape());

                if let Some(key) = ARS_AGS.extract(&val) {
                    let region = Self::RegionalKey(key);

                    if contains(&bounding_box, &region) {
                        return Some(region);
                    }
                }

                if let Some(id) = GEO_NAMES.r#match(&val, bounding_box.as_ref(), shape.as_ref()) {
                    return Some(Self::GeoName(id));
                }

                Some(Self::Other(val))
            }
            val => Some(val),
        }
    }

    /// Schleswig-Holstein
    pub const SH: Self = Region::RegionalKey(10 * 100 * 10000 * 1000);

    /// Hamburg
    pub const HH: Self = Region::RegionalKey(2 * 10 * 100 * 10000 * 1000);

    /// Niedersachsen
    pub const NI: Self = Region::RegionalKey(3 * 10 * 100 * 10000 * 1000);

    /// Bremen
    pub const HB: Self = Region::RegionalKey(4 * 10 * 100 * 10000 * 1000);

    /// Nordrhein-Westfalen
    pub const NRW: Self = Region::RegionalKey(5 * 10 * 100 * 10000 * 1000);

    /// Hessen
    pub const HE: Self = Region::RegionalKey(6 * 10 * 100 * 10000 * 1000);

    /// Rheinland-Pfalz
    pub const RLP: Self = Region::RegionalKey(7 * 10 * 100 * 10000 * 1000);

    /// Baden-Württemberg
    pub const BW: Self = Region::RegionalKey(8 * 10 * 100 * 10000 * 1000);

    /// Bayern
    pub const BY: Self = Region::RegionalKey(9 * 10 * 100 * 10000 * 1000);

    /// Saarland
    pub const SL: Self = Region::RegionalKey(10 * 10 * 100 * 10000 * 1000);

    /// Berlin
    pub const BE: Self = Region::RegionalKey(11 * 10 * 100 * 10000 * 1000);

    /// Brandenburg
    pub const BB: Self = Region::RegionalKey(12 * 10 * 100 * 10000 * 1000);

    /// Mecklendburg-Vorpommern
    pub const MV: Self = Region::RegionalKey(13 * 10 * 100 * 10000 * 1000);

    /// Sachsen
    pub const SN: Self = Region::RegionalKey(14 * 10 * 100 * 10000 * 1000);

    /// Sachsen-Anhalt
    pub const ST: Self = Region::RegionalKey(15 * 10 * 100 * 10000 * 1000);

    /// Thüringen
    pub const TH: Self = Region::RegionalKey(16 * 10 * 100 * 10000 * 1000);
}

impl From<CompactString> for Region {
    fn from(val: CompactString) -> Self {
        Self::Other(val)
    }
}

impl From<&'_ str> for Region {
    fn from(val: &str) -> Self {
        Self::Other(val.into())
    }
}

impl fmt::Display for Region {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Other(val) => fmt.write_str(val),
            Self::GeoName(id) => {
                let place_name = GEO_NAMES.resolve(*id);

                fmt.write_str(&place_name.name)
            }
            Self::RegionalKey(key) => {
                let item = ARS_AGS.resolve(*key);

                fmt.write_str(&item.name)
            }
            Self::Atkis(key) => {
                let item = ATKIS.resolve(*key);

                fmt.write_str(&item.name)
            }
            Self::Watershed(id) => {
                let entry = WISE.resolve(*id);

                fmt.write_str(&entry.name)
            }
        }
    }
}

fn contains(bounding_box: &Option<Rect>, region: &Region) -> bool {
    match (bounding_box, region.bounding_box()) {
        (Some(bounding_box), Some(region_bounding_box)) => {
            bounding_box.contains(&region_bounding_box)
        }
        _ => true,
    }
}

static FEDERAL_STATES: Lazy<HashMap<&'static str, Region>> = Lazy::new(|| {
    [
        ("DE", 0),
        ("de", 0),
        ("SH", 1),
        ("sh", 1),
        ("HH", 2),
        ("hh", 2),
        ("NI", 3),
        ("ni", 3),
        ("HB", 4),
        ("hb", 4),
        ("NW", 5),
        ("nw", 5),
        ("HE", 6),
        ("he", 6),
        ("RP", 7),
        ("rp", 7),
        ("BW", 8),
        ("bw", 8),
        ("BY", 9),
        ("by", 9),
        ("SL", 10),
        ("sl", 10),
        ("BE", 11),
        ("be", 11),
        ("BB", 12),
        ("bb", 12),
        ("MV", 13),
        ("mv", 13),
        ("SN", 14),
        ("sn", 14),
        ("ST", 15),
        ("st", 15),
        ("TH", 16),
        ("th", 16),
    ]
    .into_iter()
    .map(|(key, val)| (key, Region::RegionalKey(val * 10 * 100 * 10000 * 1000)))
    .collect()
});

#[derive(Serialize, ToSchema)]
/// A place name affiliated with the dataset
pub enum LabelledRegion<'a> {
    Other(&'a str),
    /// GeoNames geographical database, c.f. <https://www.geonames.org>
    ///
    /// Resolves via <https://www.geonames.org/[ID]>
    GeoName {
        id: u64,
        label: &'a str,
    },
    /// Amtlicher Regionalschlüssel, c.f. <https://www.dcat-ap.de/def/politicalGeocoding/regionalKey/>
    ///
    /// Resolves via <https://www.dcat-ap.de/def/politicalGeocoding/regionalKey/[KEY]>
    RegionalKey {
        id: u64,
        label: &'a str,
    },
    /// ATKIS Blattschnitteinteilung der Deutschlandkarte, c.f. <https://gdz.bkg.bund.de/index.php/default/blattschnitt-der-topographischen-karte-1-25-000-tk25-b25.html>
    Atkis {
        id: u64,
        label: &'a str,
    },
    /// Watersheds as defined by WISE WFD, c.f. <https://sdi.eea.europa.eu/catalogue/srv/eng/catalog.search#/metadata/bce2c4e0-0dad-4c42-9ea8-a0b82607d451>
    Watershed {
        id: &'a str,
        label: &'a str,
    },
}

impl Region {
    pub fn with_label<F, T>(&self, f: F) -> T
    where
        F: FnOnce(LabelledRegion<'_>) -> T,
    {
        let place_name;
        let ars_ags;
        let atkis;
        let wise;

        let region = match self {
            Region::Other(val) => LabelledRegion::Other(val),
            Region::GeoName(id) => {
                place_name = GEO_NAMES.resolve(*id);

                LabelledRegion::GeoName {
                    id: *id,
                    label: &place_name.name,
                }
            }
            Region::RegionalKey(key) => {
                ars_ags = ARS_AGS.resolve(*key);

                LabelledRegion::RegionalKey {
                    id: *key,
                    label: &ars_ags.name,
                }
            }
            Region::Atkis(key) => {
                atkis = ATKIS.resolve(*key);

                LabelledRegion::Atkis {
                    id: *key,
                    label: &atkis.name,
                }
            }
            Region::Watershed(id) => {
                wise = WISE.resolve(*id);

                LabelledRegion::Watershed {
                    id: &wise.id,
                    label: &wise.name,
                }
            }
        };

        f(region)
    }
}

pub struct RegionResolver<'a>(&'a [Region]);

impl<'a> From<&'a SmallVec<[Region; 1]>> for RegionResolver<'a> {
    fn from(val: &'a SmallVec<[Region; 1]>) -> Self {
        Self(val)
    }
}

impl Serialize for RegionResolver<'_> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_seq(Some(self.0.len()))?;

        for region in self.0 {
            region.with_label(|region| state.serialize_element(&region))?;
        }

        state.end()
    }
}
