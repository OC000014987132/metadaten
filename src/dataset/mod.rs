macro_rules! impl_minified_serialize {
    ( $struct:ident, $( $field:ident $( if $method:ident )? $( as $ty:ty )? ),+ $(,)? ) => {
        impl Serialize for $struct {
            fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
                let Self { $( $field ),+ } = self;

                let human_readable = serializer.is_human_readable();

                let mut count = 0;

                $( impl_minified_serialize!( @count human_readable count $field $( if $method )? ); )+

                let mut state = serializer.serialize_struct(stringify!($struct), count)?;

                $( impl_minified_serialize!( @field human_readable state $field $( if $method )? $( as $ty )? ); )+

                state.end()
            }
        }
    };

    ( @count $human_readable:ident $count:ident $field:ident ) => {
        $count += 1;
    };

    ( @count $human_readable:ident $count:ident $field:ident if $method:ident ) => {
        if !($human_readable && $field.$method()) {
            $count += 1;
        }
    };

    ( @key $field:ident ) => {
        const KEY: &str = {
            let key = match stringify!($field).as_bytes() {
                [b'r', b'#', key @ ..] => key,
                key => key,
            };
            match ::std::str::from_utf8(key) {
                Ok(key) => key,
                Err(_err) => unreachable!(),
            }
        };
    };

    ( @field $human_readable:ident $state:ident $field:ident ) => {{
        impl_minified_serialize!( @key $field );

        $state.serialize_field(KEY, $field)?;
    }};

    ( @field $human_readable:ident $state:ident $field:ident as $ty:ty ) => {{
        impl_minified_serialize!( @key $field );

        if $human_readable {
            let $field = <$ty>::from($field);

            $state.serialize_field(KEY, &$field)?;
        } else {
            $state.serialize_field(KEY, $field)?;
        }
    }};

    ( @field $human_readable:ident $state:ident $field:ident if $method:ident $( as $ty:ty )? ) => {
        if !($human_readable && $field.$method()) {
            impl_minified_serialize!( @field $human_readable $state $field $( as $ty )? );
        }
    };
}

mod global_identifier;
mod language;
mod license;
mod organisation;
pub mod origin;
mod person;
pub mod quality;
mod region;
mod resource;
mod status;
mod tag;
mod time_range;
pub mod r#type;

use std::io::{Read, Write};

use anyhow::{bail, Context, Result};
use bincode::{deserialize, serialize_into};
use cap_std::fs::File;
use geo::Rect;
use serde::{
    ser::{SerializeStruct, Serializer},
    Deserialize, Serialize,
};
use smallvec::SmallVec;
use string_cache::DefaultAtom;
use time::Date;
use tokio::task::spawn_blocking;
use utoipa::ToSchema;

use quality::Quality;
use r#type::Type;
use region::RegionResolver;
use tag::TagResolver;

pub use global_identifier::{GlobalIdentifier, Provider};
pub use language::{LabelledLanguage, Language, LanguageDetector};
pub use license::{License, LinkedLicense};
pub use organisation::{Organisation, OrganisationKey, OrganisationRole};
pub use origin::Origin;
pub use person::{Person, PersonRole};
pub use region::{LabelledRegion, Region};
pub use resource::{LabelledResourceType, Resource, ResourceType};
pub use status::Status;
pub use tag::{LabelledTag, Tag};
pub use time_range::TimeRange;

#[derive(Default, Clone, Debug, Deserialize, ToSchema)]
#[serde(deny_unknown_fields)]
pub struct Dataset {
    /// The mandatory title of the dataset
    pub title: String,
    /// The optional description of the dataset
    pub description: Option<String>,
    #[serde(default)]
    pub types: SmallVec<[Type; 1]>,
    /// An optional comment adding auxiliary information on the dataset
    pub comment: Option<String>,
    /// Hierachical short-hand of origin, e.g. the instition from which the dataset was harvested.
    ///
    /// Here, we describe what pattern we use to define the origin(s) of different data sources.
    ///
    /// ## Decisions
    /// - In general, we are guided by [DCAT-AP.de](https://www.dcat-ap.de/def/dcatde/2.0/spec/).
    /// - We aim to structure the hierachry as follows: `<dct:type>/<dct:publisher>/<dcat:catalog>`.
    /// - If multiple organisations are involved, we do attach multiple origins to single datasets to improve their findability.
    /// - We call this structure **origin**, a word currently not used in DCAT-AP.de to avoid confusion.
    ///
    /// ## Definitions
    /// ### dct:type
    /// > The nature or genre of the resource.
    ///
    /// We broadly catagorise the publisher type, like an NGO being part of society (_Zivilgesellschaft_), Regional Authorities are usually embedded into federal states (_Land_), etc.
    ///
    /// ### dct:publisher
    /// > The entity responsible for making the item available.
    ///
    /// We use **organisations** who own the data/information, either by creating and owning the propierties themselves, or through contracts awarded to other organisations (_Im Auftrag von_). Examples are government departments, like Bundesministerium für Strahlenschutz (BfS), or state agencies, like Umweltbundesamt (UBA).
    ///
    /// ### dcat:catalog
    /// > Represents a catalog, which is a dataset in which each individual item is a metadata record describing some resource; the scope of dcat:Catalog is collections of metadata about datasets or data services.
    ///
    /// To simplify our structure, this catalog can be a website, web application, or any other openly accessible data/information offer.
    #[serde(default)]
    #[schema(value_type = [String])]
    pub origins: SmallVec<[DefaultAtom; 1]>,
    #[serde(default)]
    #[schema(value_type = LinkedLicense)]
    pub license: License,
    #[serde(default)]
    pub mandatory_registration: bool,
    #[serde(default)]
    pub organisations: SmallVec<[Organisation; 2]>,
    #[serde(default)]
    pub persons: Vec<Person>,
    #[serde(default)]
    #[schema(value_type = [LabelledTag])]
    pub tags: Vec<Tag>,
    #[serde(default)]
    #[schema(value_type = [LabelledRegion])]
    pub regions: SmallVec<[Region; 1]>,
    /// The date at which the dataset was originally issued
    pub issued: Option<Date>,
    /// The date at which the dataset was last modified
    pub modified: Option<Date>,
    #[schema(format = Uri)]
    /// URL of the resource from which the dataset was harvested
    pub source_url: String,
    /// Additional information when manual action is necessary to find the requested data at `source_url`
    pub source_url_explainer: Option<String>,
    /// Indicates whether the source provided machine-readable metadata
    ///
    /// If not set, the dataset had to be scraped or otherwise inferred from unstructured data.
    #[serde(default)]
    pub machine_readable_source: bool,
    /// URL to alternative versions of this dataset, e.g. other sources or languages
    #[serde(default)]
    pub alternatives: Vec<Alternative>,
    #[serde(default)]
    pub resources: SmallVec<[Resource; 4]>,
    #[serde(default)]
    #[schema(value_type = LabelledLanguage)]
    pub language: Language,
    #[serde(default)]
    #[schema(value_type = [BoundingBox])]
    /// The extents of the dataset in the geographic space, given as as axis-aligned bounding boxes based on WGS84
    pub bounding_boxes: SmallVec<[Rect; 1]>,
    #[serde(default)]
    /// Intervals describing the time-like extent of the dataset
    pub time_ranges: SmallVec<[TimeRange; 1]>,
    pub global_identifier: Option<GlobalIdentifier>,
    #[serde(default)]
    pub quality: Quality,
    #[serde(default)]
    pub status: Status,
}

impl_minified_serialize!(
    Dataset,
    title,
    description if is_none,
    r#types if is_empty,
    comment if is_none,
    origins if is_empty,
    license as LinkedLicense,
    mandatory_registration,
    organisations if is_empty,
    persons if is_empty,
    tags if is_empty as TagResolver,
    regions if is_empty as RegionResolver,
    issued if is_none,
    modified if is_none,
    source_url,
    source_url_explainer if is_none,
    machine_readable_source,
    alternatives if is_empty,
    resources if is_empty,
    language as LabelledLanguage,
    bounding_boxes if is_empty,
    time_ranges if is_empty,
    global_identifier if is_none,
    quality,
    status,
);

/// Previously deployed version of the above [`Dataset`] type.
///
/// It will be updated when a new harvester has been deployed. Feature branches should only modify [`Dataset`] and the mapping between both types defined by [`Dataset::read`].
#[derive(Debug, Deserialize)]
struct OldDataset {
    title: String,
    description: Option<String>,
    types: SmallVec<[Type; 1]>,
    comment: Option<String>,
    origins: SmallVec<[DefaultAtom; 1]>,
    license: License,
    mandatory_registration: bool,
    organisations: SmallVec<[Organisation; 2]>,
    persons: Vec<Person>,
    tags: Vec<Tag>,
    regions: SmallVec<[Region; 1]>,
    issued: Option<Date>,
    modified: Option<Date>,
    source_url: String,
    machine_readable_source: bool,
    alternatives: Vec<Alternative>,
    resources: SmallVec<[Resource; 4]>,
    language: Language,
    bounding_boxes: SmallVec<[Rect; 1]>,
    time_ranges: SmallVec<[TimeRange; 1]>,
    global_identifier: Option<GlobalIdentifier>,
    quality: Quality,
    status: Status,
}

const CURR_VER: u8 = 32;
const OLD_VER: u8 = 31;

impl Dataset {
    pub fn read_with<R>(mut reader: R, buf: &mut Vec<u8>) -> Result<Self>
    where
        R: Read,
    {
        buf.clear();
        reader.read_to_end(buf)?;

        Self::parse(buf)
    }

    pub async fn async_read_with<R>(reader: R, mut buf: Vec<u8>) -> Result<(Self, Vec<u8>)>
    where
        R: Read + Send + 'static,
    {
        spawn_blocking(move || {
            let val = Self::read_with(reader, &mut buf)?;
            Ok((val, buf))
        })
        .await
        .unwrap()
    }

    pub fn parse(buf: &[u8]) -> Result<Self> {
        let res = match buf.split_last() {
            Some((&CURR_VER, buf)) => deserialize::<Self>(buf),
            Some((&OLD_VER, buf)) => deserialize::<OldDataset>(buf).map(|old_val| Self {
                title: old_val.title,
                description: old_val.description,
                types: old_val.types,
                comment: old_val.comment,
                origins: old_val.origins,
                license: old_val.license,
                mandatory_registration: old_val.mandatory_registration,
                organisations: old_val.organisations,
                persons: old_val.persons,
                tags: old_val.tags,
                regions: old_val.regions,
                issued: old_val.issued,
                modified: old_val.modified,
                source_url: old_val.source_url,
                source_url_explainer: None,
                machine_readable_source: old_val.machine_readable_source,
                alternatives: old_val.alternatives,
                resources: old_val.resources,
                language: old_val.language,
                bounding_boxes: old_val.bounding_boxes,
                time_ranges: old_val.time_ranges,
                global_identifier: old_val.global_identifier,
                quality: old_val.quality,
                status: old_val.status,
            }),
            _ => bail!("Missing or invalid dataset format version"),
        };

        res.context("Invalid dataset format")
    }

    pub fn write(&self, mut file: File) -> Result<()> {
        let mut buf = Vec::new();
        self.store(&mut buf)?;

        file.write_all(&buf)?;

        Ok(())
    }

    pub async fn async_write(self, file: File) -> Result<()> {
        spawn_blocking(move || self.write(file)).await.unwrap()
    }

    pub fn store(&self, buf: &mut Vec<u8>) -> Result<()> {
        buf.clear();
        serialize_into(&mut *buf, self)?;
        buf.push(CURR_VER);

        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize, ToSchema)]
pub enum Alternative {
    Source {
        #[schema(value_type = String)]
        source: DefaultAtom,
        title: String,
        url: String,
    },
    Language {
        language: Language,
        url: String,
    },
}

impl Alternative {
    pub fn url(&self) -> &str {
        match self {
            Self::Source { url, .. } => url,
            Self::Language { url, .. } => url,
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct IdentifiedDataset {
    pub id: String,
    #[serde(flatten)]
    pub value: Dataset,
}

#[derive(Debug, Clone, Deserialize, Serialize, ToSchema)]
#[serde(deny_unknown_fields)]
pub struct UniquelyIdentifiedDataset {
    #[schema(value_type = String)]
    /// Name of the harvester creating this dataset
    pub source: DefaultAtom,
    /// Identifier of the dataset which is unique for this harvester
    pub id: String,
    #[serde(flatten)]
    pub value: Dataset,
}

#[derive(Debug, Clone, Serialize, ToSchema)]
pub struct ScoredDataset {
    #[serde(flatten)]
    pub value: UniquelyIdentifiedDataset,
    /// Score of this dataset by the search engine based on the given search parameters
    pub score: f32,
}

#[derive(ToSchema)]
#[allow(dead_code)] // used only to derive `ToSchema`
/// An axis-aligned bounding box defined by its two corners
pub struct BoundingBox {
    /// South-west corner of the bounding box
    min: Corner,
    /// North-east corner of the bounding box
    max: Corner,
}

#[derive(ToSchema)]
#[allow(dead_code)] // used only to derive `ToSchema`
pub struct Corner {
    /// Longitude using WGS84
    x: f64,
    /// Latitude using WGS84
    y: f64,
}
