use std::fmt;

use anyhow::{anyhow, Error};
use serde::{Deserialize, Serialize};
use tantivy::schema::Facet;
use utoipa::ToSchema;

use crate::dataset::Dataset;

#[derive(
    Debug,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    Deserialize,
    Serialize,
    Default,
    ToSchema,
    Clone,
    Copy,
)]
pub enum Language {
    #[default]
    Unknown,
    Multiple,
    German,
    English,
    Spanish,
    Albanian,
    Bulgarian,
    Catalan,
    Czech,
    Danish,
    Dutch,
    Estonian,
    Faroese,
    Finnish,
    French,
    Georgian,
    Greek,
    Croatian,
    Hungarian,
    Icelandic,
    Italian,
    Latvian,
    Lithuanian,
    Macedonian,
    Maltese,
    Norwegian,
    Polish,
    Portuguese,
    Romanian,
    Slovak,
    Slovenian,
    Serbian,
    Swedish,
    Ukrainian,
    GermanEasy,
    LowGerman,
    Russian,
    Chinese,
    Arabic,
    Armenian,
    Aromanian,
    Persian,
    Turkish,
}

impl Language {
    pub fn as_str(&self) -> &str {
        match self {
            Self::Unknown => "unbekannt",
            Self::Multiple => "mehrere",
            Self::German => "Deutsch",
            Self::English => "Englisch/English",
            Self::Spanish => "Spanisch/Español",
            Self::Albanian => "Albanisch/Shqip",
            Self::Arabic => "Arabisch/اَلْعَرَبِيَّةُ",
            Self::Armenian => "Armenisch/Հայերեն",
            Self::Aromanian => "Aromunisch/armãneashti",
            Self::Bulgarian => "Bulgarisch/Български",
            Self::Catalan => "Katalanisch/Català",
            Self::Czech => "Tschechisch/Čeština",
            Self::Danish => "Dänisch/Dansk",
            Self::Dutch => "Niederländisch/Nederlands",
            Self::Estonian => "Estnisch/Eesti",
            Self::Faroese => "Färöisch/Føroyskt",
            Self::Finnish => "Finnisch/Suomi",
            Self::French => "Französisch/Français",
            Self::Georgian => "Georgisch/ქართული",
            Self::Greek => "Griechisch/Ελληνικά",
            Self::Croatian => "Kroatisch/Hrvatski",
            Self::Hungarian => "Ungarisch/Magyar",
            Self::Icelandic => "Isländisch/Íslenska",
            Self::Italian => "Italienisch/Italiano",
            Self::Latvian => "Lettisch/Latviešu",
            Self::Lithuanian => "Litauisch/Lietuvių",
            Self::Macedonian => "Mazedonisch/Македонски",
            Self::Maltese => "Maltesisch/Malti",
            Self::Norwegian => "Norwegisch/Norsk",
            Self::Persian => "Persisch/زبان فارسی",
            Self::Polish => "Polnisch/Polski",
            Self::Portuguese => "Portugiesisch/Português",
            Self::Romanian => "Rumänisch/Română",
            Self::Slovak => "Slowakisch/Slovenčina",
            Self::Slovenian => "Slowenisch/Slovenščina",
            Self::Serbian => "Serbisch/Српски",
            Self::Swedish => "Schwedisch/Svenska",
            Self::Turkish => "Türkisch/Türkçe",
            Self::Ukrainian => "Ukrainisch/Українська",
            Self::GermanEasy => "Leichte Sprache",
            Self::LowGerman => "Plattdeutsch",
            Self::Russian => "Russisch",
            Self::Chinese => "Chinesisch",
        }
    }

    pub fn facet(&self) -> &'static [&'static str] {
        match self {
            Self::Unknown => &["unbekannt"],
            Self::Multiple => &["mehrere"],
            Self::German => &["Deutsch"],
            Self::English => &["Englisch"],
            Self::Spanish => &["andere", "Spanisch"],
            Self::Albanian => &["andere", "Albanisch"],
            Self::Arabic => &["andere", "Arabisch"],
            Self::Armenian => &["andere", "Armenisch"],
            Self::Aromanian => &["andere", "Aromunisch"],
            Self::Bulgarian => &["andere", "Bulgarisch"],
            Self::Catalan => &["andere", "Katalanisch"],
            Self::Czech => &["andere", "Tschechisch"],
            Self::Danish => &["andere", "Dänisch"],
            Self::Dutch => &["andere", "Niederländisch"],
            Self::Estonian => &["andere", "Estnisch"],
            Self::Faroese => &["andere", "Färöisch"],
            Self::Finnish => &["andere", "Finnisch"],
            Self::French => &["andere", "Französisch"],
            Self::Georgian => &["andere", "Georgisch"],
            Self::Greek => &["andere", "Griechisch"],
            Self::Croatian => &["andere", "Kroatisch"],
            Self::Hungarian => &["andere", "Ungarisch"],
            Self::Icelandic => &["andere", "Isländisch"],
            Self::Italian => &["andere", "Italienisch"],
            Self::Latvian => &["andere", "Lettisch"],
            Self::Lithuanian => &["andere", "Litauisch"],
            Self::Macedonian => &["andere", "Mazedonisch"],
            Self::Maltese => &["andere", "Maltesisch"],
            Self::Norwegian => &["andere", "Norwegisch"],
            Self::Persian => &["andere", "Persisch"],
            Self::Polish => &["andere", "Polnisch"],
            Self::Portuguese => &["andere", "Portugiesisch"],
            Self::Romanian => &["andere", "Rumänisch"],
            Self::Slovak => &["andere", "Slowakisch"],
            Self::Slovenian => &["andere", "Slowenisch"],
            Self::Serbian => &["andere", "Serbisch"],
            Self::Swedish => &["andere", "Schwedisch"],
            Self::Turkish => &["andere", "Türkisch"],
            Self::Ukrainian => &["andere", "Ukrainisch"],
            Self::GermanEasy => &["Leichte Sprache"],
            Self::LowGerman => &["andere", "Plattdeutsch"],
            Self::Russian => &["andere", "Russisch"],
            Self::Chinese => &["andere", "Chinesisch"],
        }
    }

    pub fn code(&self) -> &'static str {
        match self {
            Self::Multiple => "mul",        // ISO 639-3 code for multiple languages
            Self::German => "deu",          // ISO 639-3 code for German
            Self::English => "eng",         // ISO 639-3 code for English
            Self::Spanish => "spa",         // ISO 639-3 code for Spanish
            Self::Albanian => "sqi",        // ISO 639-3 code for Albanian
            Self::Arabic => "ara",          // ISO 639-3 code for Arabic
            Self::Armenian => "hye",        // ISO 639-3 code for Armenian
            Self::Aromanian => "rup",       // ISO 639-3 code for Aromanian
            Self::Bulgarian => "bul",       // ISO 639-3 code for Bulgarian
            Self::Catalan => "cat",         // ISO 639-3 code for Catalan
            Self::Czech => "ces",           // ISO 639-3 code for Czech
            Self::Danish => "dan",          // ISO 639-3 code for Danish
            Self::Dutch => "nld",           // ISO 639-3 code for Dutch
            Self::Estonian => "est",        // ISO 639-3 code for Estonian
            Self::Faroese => "fao",         // ISO 639-3 code for Faroese
            Self::Finnish => "fin",         // ISO 639-3 code for Finnish
            Self::French => "fra",          // ISO 639-3 code for French
            Self::Georgian => "kat",        // ISO 639-3 code for Georgian
            Self::Greek => "ell",           // ISO 639-3 code for Modern Greek
            Self::Croatian => "hrv",        // ISO 639-3 code for Croatian
            Self::Hungarian => "hun",       // ISO 639-3 code for Hungarian
            Self::Icelandic => "isl",       // ISO 639-3 code for Icelandic
            Self::Italian => "ita",         // ISO 639-3 code for Italian
            Self::Latvian => "lav",         // ISO 639-3 code for Latvian
            Self::Lithuanian => "lit",      // ISO 639-3 code for Lithuanian
            Self::Macedonian => "mkd",      // ISO 639-3 code for Macedonian
            Self::Maltese => "mlt",         // ISO 639-3 code for Maltese
            Self::Norwegian => "nor",       // ISO 639-3 code for Norwegian
            Self::Persian => "fas",         // ISO 639-3 code for Persian
            Self::Polish => "pol",          // ISO 639-3 code for Polish
            Self::Portuguese => "por",      // ISO 639-3 code for Portuguese
            Self::Romanian => "ron",        // ISO 639-3 code for Romanian
            Self::Slovak => "slk",          // ISO 639-3 code for Slovak
            Self::Slovenian => "slv",       // ISO 639-3 code for Slovenian
            Self::Serbian => "srp",         // ISO 639-3 code for Serbian
            Self::Swedish => "swe",         // ISO 639-3 code for Swedish
            Self::Turkish => "tur",         // ISO 639-3 code for Turkish
            Self::Ukrainian => "ukr",       // ISO 639-3 code for Ukrainian
            Self::GermanEasy => "deu-easy", // Non-standard, used to indicate simplified German
            Self::LowGerman => "nds",       // ISO 639-3 code for Low German
            Self::Russian => "rus",         // ISO 639-3 code for Russian
            Self::Chinese => "zho",         // ISO 639-3 code for Chinese
            Self::Unknown => "und",         // ISO 639-3 code for undefined language
        }
    }
}

impl From<&'_ str> for Language {
    fn from(val: &str) -> Self {
        match val.trim() {
            "mul" => Self::Multiple,
            "ger" | "de" | "deu" | "DEU" | "GER" | "Deutsch" | "deutsch" => Self::German,
            "en" | "eng" | "ENG" | "English" | "Englisch" => Self::English,
            "es" | "spa" | "Spanisch" => Self::Spanish,
            "alb" | "Albanisch" => Self::Albanian,
            "ara" | "Arabisch" => Self::Arabic,
            "arm" | "Armenisch" => Self::Armenian,
            "rup" => Self::Aromanian,
            "bul" | "Bulgarisch" => Self::Bulgarian,
            "cat" => Self::Catalan,
            "cze" | "Czech" | "Tschechisch" => Self::Czech,
            "dan" => Self::Danish,
            "nl" | "dut" => Self::Dutch,
            "est" => Self::Estonian,
            "fao" => Self::Faroese,
            "fin" => Self::Finnish,
            "fr" | "fra" | "fre" | "Französisch" => Self::French,
            "geo" => Self::Georgian,
            "gre" | "Griechisch" => Self::Greek,
            "hrv" | "Kroatisch" => Self::Croatian,
            "hun" | "Ungarisch" => Self::Hungarian,
            "ice" => Self::Icelandic,
            "it" | "ita" | "Italienisch" => Self::Italian,
            "lav" => Self::Latvian,
            "lit" => Self::Lithuanian,
            "mkd" | "Mazedonisch" => Self::Macedonian,
            "mlt" => Self::Maltese,
            "nor" => Self::Norwegian,
            "per" | "Farsi (Persisch)" => Self::Persian,
            "pol" | "Polnisch" => Self::Polish,
            "por" => Self::Portuguese,
            "rum" | "Rumänisch" => Self::Romanian,
            "slo" | "Slowakisch" => Self::Slovak,
            "slv" | "Slowenisch" => Self::Slovenian,
            "srp" => Self::Serbian,
            "swe" => Self::Swedish,
            "tur" | "Türkisch" => Self::Turkish,
            "ukr" | "Ukrainisch" => Self::Ukrainian,
            "de-easy" | "GermanEasy" => Self::GermanEasy,
            "nds" => Self::LowGerman,
            "rus" | "ru" | "Russisch" => Self::Russian,
            "zh" | "chi" | "zho" | "Chinesisch" => Self::Chinese,
            _ => {
                if !val.is_empty() {
                    tracing::debug!("Unable to parse language: {val}");
                }

                Self::Unknown
            }
        }
    }
}

impl TryFrom<Language> for lingua::Language {
    type Error = Error;

    fn try_from(lang: Language) -> Result<lingua::Language, Self::Error> {
        match lang {
            Language::German | Language::GermanEasy | Language::LowGerman => {
                Ok(lingua::Language::German)
            }
            Language::English => Ok(lingua::Language::English),
            Language::Persian => Ok(lingua::Language::Persian),
            Language::Arabic => Ok(lingua::Language::Arabic),
            Language::Dutch => Ok(lingua::Language::Dutch),
            Language::French => Ok(lingua::Language::French),
            Language::Russian => Ok(lingua::Language::Russian),
            Language::Chinese => Ok(lingua::Language::Chinese),
            lang => Err(anyhow!("Language {lang} is not enabled or not supported")),
        }
    }
}

impl From<lingua::Language> for Language {
    fn from(lang: lingua::Language) -> Self {
        match lang {
            lingua::Language::German => Self::German,
            lingua::Language::English => Self::English,
            lingua::Language::Persian => Self::Persian,
            lingua::Language::Arabic => Self::Arabic,
            lingua::Language::Dutch => Self::Dutch,
            lingua::Language::French => Self::French,
            lingua::Language::Russian => Self::Russian,
            lingua::Language::Chinese => Self::Chinese,
        }
    }
}

pub struct LanguageDetector(lingua::LanguageDetector);

impl LanguageDetector {
    pub fn build<L>(languages: L) -> Self
    where
        L: IntoIterator<Item = Language>,
    {
        Self(
            lingua::LanguageDetectorBuilder::from_languages(
                &languages
                    .into_iter()
                    .map(|language| language.try_into().unwrap())
                    .collect::<Vec<lingua::Language>>(),
            )
            .build(),
        )
    }

    pub fn guess_or(&self, dataset: &mut Dataset, default: Language) {
        let mut text = dataset.title.clone();

        if let Some(description) = &dataset.description {
            text.push_str("\n\n");
            text.push_str(description);
        }

        dataset.language = self
            .0
            .detect_language_of(text)
            .map_or(default, |lang| lang.into());
    }
}

impl fmt::Display for Language {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(self.as_str())
    }
}

/// The language used by the dataset
#[derive(Serialize, ToSchema)]
pub struct LabelledLanguage<'a> {
    id: &'a Language,
    #[schema(value_type = String)]
    path: Facet,
    label: &'a str,
}

impl<'a> From<&'a Language> for LabelledLanguage<'a> {
    fn from(val: &'a Language) -> Self {
        Self {
            id: val,
            path: Facet::from_path(val.facet()),
            label: val.as_str(),
        }
    }
}
