use std::fmt;

use phf::{phf_map, Map as PhfMap};
use serde::{Deserialize, Serialize};
use tantivy::schema::Facet;
use utoipa::ToSchema;

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub enum License {
    #[default]
    Unknown,
    AllRightsReserved,
    MixedClosed,
    CcBy,
    CcBy10,
    CcBy30,
    CcBy30De,
    CcBy40,
    CcByNc,
    CcByNc10,
    CcByNc30De,
    CcByNc40,
    CcByNcNd10,
    CcByNcNd30De,
    CcByNcNd40,
    CcByNcSa10,
    CcByNcSa40,
    CcByNd10,
    CcByNd30,
    CcByNd40,
    CcBySa10,
    CcBySa30,
    CcBySa40,
    CcPdm10,
    CcZero,
    DlDeBy10,
    DlDeBy20,
    DlDeByNc10,
    DlDeZero20,
    DorisBfs,
    GeoLizenz,
    Gpl3,
    NutzPotsdam,
    Odbl,
    OfficialWork,
    OtherClosed,
    OtherOpen,
    OtherOpenSource,
    Pddl,
    GeoNutz20130319,
    GeoNutz20131001,
    GeoNutzNi,
    GeoNutzSh20230801,
    LhwNutzSt,
    GeoNutzSl,
    Sentinel247904_690755,
    GeoNutzSt50,
    InspireNoConditionsApply,
    MixedOpen,
    DbCl10,
}

impl From<&'_ License> for License {
    fn from(val: &License) -> Self {
        *val
    }
}

impl License {
    pub fn as_str(&self) -> &str {
        match self {
            Self::Unknown => "unbekannt",
            Self::DlDeBy20 => "dl-by-de/2.0",
            Self::DlDeZero20 => "dl-zero-de/2.0",
            Self::CcBy40 => "cc-by/4.0",
            Self::CcBy30 => "cc-by/3.0",
            Self::CcBy10 => "cc-by",
            Self::CcBySa10 => "cc-by-sa",
            Self::CcByNcSa10 => "cc-by-nc-sa",
            Self::CcByNcNd10 => "cc-by-nc-nd",
            Self::OfficialWork => "officialWork",
            Self::DorisBfs => "doris-bfs",
            Self::GeoNutz20130319 => "geoNutz/20130319",
            Self::GeoNutz20131001 => "geoNutz/20131001",
            Self::GeoNutzNi => "geoNutzNI",
            Self::GeoNutzSt50 => "geoNutzST/5.0",
            Self::LhwNutzSt => "lhwNutzST",
            Self::GeoNutzSl => "geoNutzSL",
            Self::GeoNutzSh20230801 => "geoNutzSH/20230801",
            Self::Sentinel247904_690755 => "Sentinel/247904/690755",
            Self::OtherOpen => "other-open",
            Self::OtherOpenSource => "other-opensource",
            Self::OtherClosed => "other-closed",
            Self::Odbl => "odbl",
            Self::Pddl => "pddl",
            Self::CcBy30De => "cc-by-de/3.0",
            Self::CcZero => "cc-zero",
            Self::CcByNc30De => "cc-by-nc/3.0",
            Self::CcByNc40 => "cc-by-nc/4.0",
            Self::CcByNd40 => "cc-by-nd/4.0",
            Self::CcPdm10 => "ccpdm/1.0",
            Self::CcByNcNd30De => "cc-by-nc-nd/3.0",
            Self::DlDeByNc10 => "dl-de-by-nc/1.0",
            Self::CcBySa30 => "cc-by-sa/3.0",
            Self::CcBySa40 => "cc-by-sa/4.0",
            Self::CcBy => "cc-by",
            Self::DlDeBy10 => "dl-by-de/1.0",
            Self::CcByNc => "cc-by-nc",
            Self::CcByNcSa40 => "cc-by-nc-sa/4.0",
            Self::CcByNcNd40 => "cc-by-nc-nd/4.0",
            Self::Gpl3 => "gpl/3.0",
            Self::NutzPotsdam => "nutzPotsdam",
            Self::GeoLizenz => "geoLizenz",
            Self::CcByNd10 => "cc-by-nd",
            Self::CcByNd30 => "cc-by-nd/3.0",
            Self::CcByNc10 => "cc-by-nc",
            Self::MixedClosed => "mehrere (geschlossen)",
            Self::AllRightsReserved => "all-rights-reserved",
            Self::InspireNoConditionsApply => "no-conditions-apply",
            Self::MixedOpen => "mehrere (offen)",
            Self::DbCl10 => "dbcl/1.0",
        }
    }

    pub fn url(&self) -> Option<&'static str> {
        let val = match self {
            Self::Unknown => return None,
            Self::DlDeBy20 => "https://www.govdata.de/dl-de/by-2-0",
            Self::DlDeZero20 => "https://www.govdata.de/dl-de/zero-2-0",
            Self::CcBy40 => "https://creativecommons.org/licenses/by/4.0/",
            Self::CcBy30 => "https://creativecommons.org/licenses/by/3.0/",
            Self::CcBy10 => "https://creativecommons.org/licenses/by/1.0/",
            Self::CcBySa10 => "https://creativecommons.org/licenses/by-sa/1.0/",
            Self::CcByNcSa10 => "https://creativecommons.org/licenses/by-nc-sa/1.0/",
            Self::CcByNcNd10 => "https://creativecommons.org/licenses/by-nc-nd/1.0/",
            Self::OfficialWork => "https://www.gesetze-im-internet.de/urhg/__5.html",
            Self::DorisBfs => "https://doris.bfs.de/jspui/impressum/lizenz.html",
            Self::GeoNutz20130319 => "https://sg.geodatenzentrum.de/web_public/gdz/lizenz/geonutzv.pdf",
            Self::GeoNutz20131001 => "https://web.archive.org/web/20221024031446/https://www.stadtentwicklung.berlin.de/geoinformation/download/nutzIII.pdf",
            Self::GeoNutzNi => "https://www.lgln.niedersachsen.de/startseite/wir_uber_uns_amp_organisation/allgemeine_geschafts_und_nutzungsbedingungen_agnb/allgemeine-geschafts-und-nutzungsbedingungen-agnb-97401.html",
            Self::GeoNutzSt50 => "https://www.lvermgeo.sachsen-anhalt.de/de/datei/anzeigen/id/3567,501/nutzungsbedingungenv5.0_b.pdf",
            Self::LhwNutzSt => "https://gld.lhw-sachsen-anhalt.de/",
            Self::GeoNutzSh20230801 => "https://geoserver.gdi-sh.de/agnb",
            Self::GeoNutzSl => "https://www.shop.lvgl.saarland.de/cloud/index.php/s/Nutzungsbedingungen_Geofachdaten#pdfviewer",
            Self::Sentinel247904_690755 => "https://sentinel.esa.int/documents/247904/690755/Sentinel_Data_Legal_Notice",
            Self::OtherOpen => "https://www.dcat-ap.de/def/licenses/other-open",
            Self::OtherOpenSource => "https://www.dcat-ap.de/def/licenses/other-opensource",
            Self::OtherClosed => "https://www.dcat-ap.de/def/licenses/other-closed",
            Self::Odbl => "https://www.opendefinition.org/licenses/odc-odbl",
            Self::Pddl => "http://opendefinition.org/licenses/odc-pddl/",
            Self::CcZero => "https://www.opendefinition.org/licenses/cc-zero",
            Self::CcBy30De => "https://creativecommons.org/licenses/by/3.0/de/",
            Self::CcByNc30De => "https://creativecommons.org/licenses/by-nc/3.0/de/",
            Self::CcByNc40 => "https://creativecommons.org/licenses/by-nc/4.0/",
            Self::CcByNd40 => "https://creativecommons.org/licenses/by-nd/4.0/",
            Self::CcPdm10 => "https://creativecommons.org/publicdomain/mark/1.0/",
            Self::CcByNcNd30De => "https://creativecommons.org/licenses/by-nc-nd/3.0/de/",
            Self::DlDeByNc10 => "https://www.govdata.de/dl-de/by-nc-1-0",
            Self::CcBySa30 => "https://creativecommons.org/licenses/by-sa/3.0/de/legalcode",
            Self::CcBySa40 => "https://creativecommons.org/licenses/by-sa/4.0/",
            Self::CcBy => "https://opendefinition.org/licenses/cc-by/",
            Self::DlDeBy10 => "https://www.govdata.de/dl-de/by-1-0",
            Self::CcByNc => "http://dcat-ap.de/def/licenses/cc-by-nc",
            Self::CcByNcSa40 => "https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de",
            Self::CcByNcNd40 => "https://creativecommons.org/licenses/by-nc-nd/4.0/deed.de",
            Self::Gpl3 => "https://opensource.org/license/gpl-3-0/",
            Self::NutzPotsdam => "https://potsdam.opendatasoft.com/terms/terms-and-conditions/",
            Self::GeoLizenz => "https://web.archive.org/web/20161013090905/http://www.geolizenz.org/",
            Self::CcByNd10 => "https://creativecommons.org/licenses/by-nd/1.0/deed.de",
            Self::CcByNd30 => "https://creativecommons.org/licenses/by-nd/3.0/deed.de",
            Self::CcByNc10 => "https://creativecommons.org/licenses/by-nc/1.0/deed.de",
            Self::AllRightsReserved => "https://en.wikipedia.org/wiki/All_rights_reserved",
            Self::MixedClosed => "https://md.umwelt.info/mixed-licenses.html",
            Self::InspireNoConditionsApply => "https://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/noConditionsApply",
            Self::MixedOpen => "https://md.umwelt.info/mixed-licenses.html",
            Self::DbCl10 => "https://opendatacommons.org/licenses/dbcl/1-0/",
        };

        Some(val)
    }

    pub fn facet(&self) -> &'static [&'static str] {
        match self {
            Self::Unknown => &["unbekannt"],
            Self::DlDeBy20 => &["offen", "dl-de", "by", "2.0"],
            Self::DlDeZero20 => &["offen", "dl-de", "zero", "2.0"],
            Self::CcBy40 => &["offen", "cc", "by", "4.0"],
            Self::CcBy30 => &["offen", "cc", "by", "3.0"],
            Self::CcBy10 => &["offen", "cc", "by", "1.0"],
            Self::CcBySa10 => &["offen", "cc", "by-sa", "1.0"],
            Self::CcByNcSa10 => &["offen", "cc", "by-nc-sa", "1.0"],
            Self::CcByNcNd10 => &["offen", "cc", "by-nc-nd", "1.0"],
            Self::OfficialWork => &["offen", "officialWork"],
            Self::DorisBfs => &["offen", "doris-bfs"],
            Self::GeoNutz20130319 => &["offen", "geoNutz", "20130319"],
            Self::GeoNutz20131001 => &["offen", "geoNutz", "20131001"],
            Self::GeoNutzNi => &["offen", "geoNutz-ni"],
            Self::GeoNutzSt50 => &["geschlossen", "geoNutz-st", "5.0"],
            Self::LhwNutzSt => &["offen", "lhwNutz-st"],
            Self::GeoNutzSh20230801 => &["geschlossen", "geoNutz-sh", "20230108"],
            Self::GeoNutzSl => &["offen", "geoNutz-sl"],
            Self::Sentinel247904_690755 => &["offen", "sentinel"],
            Self::OtherOpen => &["offen", "andere"],
            Self::OtherOpenSource => &["offen", "andere"],
            Self::Odbl => &["offen", "odbl"],
            Self::Pddl => &["offen", "pddl"],
            Self::CcZero => &["offen", "cc", "zero"],
            Self::CcBy30De => &["offen", "cc", "by", "3.0", "de"],
            Self::CcByNc30De => &["offen", "cc", "by-nc", "3.0", "de"],
            Self::CcByNc40 => &["offen", "cc", "by-nc", "4.0"],
            Self::CcByNd40 => &["offen", "cc", "by-nd", "4.0"],
            Self::CcPdm10 => &["offen", "cc", "pdm", "1.0"],
            Self::CcByNcNd30De => &["offen", "cc", "by-nc-nd", "3.0"],
            Self::DlDeByNc10 => &["offen", "dl-de", "by", "nc", "1.0"],
            Self::CcBySa30 => &["offen", "cc", "by-sa", "3.0"],
            Self::CcBySa40 => &["offen", "cc", "by-sa", "4.0"],
            Self::CcBy => &["offen", "cc", "by"],
            Self::DlDeBy10 => &["offen", "dl-de", "by", "1.0"],
            Self::CcByNc => &["offen", "cc", "by-nc"],
            Self::CcByNcSa40 => &["offen", "cc", "by-nc-sa", "4.0"],
            Self::CcByNcNd40 => &["offen", "cc", "by-nc-nd", "4.0"],
            Self::Gpl3 => &["offen", "gpl", "3"],
            Self::NutzPotsdam => &["offen", "nutzPotsdam"],
            Self::GeoLizenz => &["offen", "geolizenz"],
            Self::CcByNd10 => &["offen", "cc", "by-nd", "1.0"],
            Self::CcByNd30 => &["offen", "cc", "by-nd", "3.0"],
            Self::CcByNc10 => &["offen", "cc", "by-nc", "1.0"],
            Self::AllRightsReserved => &["geschlossen", "all-rights-reserved"],
            Self::MixedClosed => &["geschlossen", "mehrere"],
            Self::OtherClosed => &["geschlossen", "andere"],
            Self::InspireNoConditionsApply => &[
                "geschlossen",
                "inspire",
                "conditions-applying-to-access-and-use",
                "no-conditions-apply",
            ],
            Self::MixedOpen => &["offen", "mehrere"],
            Self::DbCl10 => &["offen", "dbcl", "1.0"],
        }
    }

    pub fn is_open(&self) -> bool {
        self.facet().first() == Some(&"offen")
    }

    pub fn is_closed(&self) -> bool {
        self.facet().first() == Some(&"geschlossen")
    }
}

impl From<&'_ str> for License {
    fn from(val: &str) -> Self {
        static LICENSES: PhfMap<&'static str, License> = phf_map! {
            // Explicitly parse licenses marked as unknown
            "UNKNOWN" => License::Unknown,
            "unbekannt" => License::Unknown,
            "SOURCE" => License::Unknown,
            // generic License
            "copyright" => License::AllRightsReserved,
            "Die Daten unterliegen dem Urheberrecht."=> License::AllRightsReserved,
            "intellectual property rights"=> License::AllRightsReserved,
            "Diese Daten sind urheberrechtlich geschützt. Unbefugte Verbreitung und Verwendung verstoßen gegen das Urheberrecht."=> License::AllRightsReserved,
            // generic License for Datenlizenz Deutschland – Namensnennung
            "https://opendefinition.org/licenses/cc-by/" => License::CcBy,
            "http://www.opendefinition.org/licenses/cc-by" => License::CcBy,
            // Datenlizenz Deutschland – Namensnennung – Version 2.0
            "Datenlizenz Deutschland Namensnennung 2.0" => License::DlDeBy20,
            "https://www.govdata.de/dl-de/by-2-0" => License::DlDeBy20,
            "dl-by-de/2.0" => License::DlDeBy20,
            "dl-de-by-2.0" => License::DlDeBy20,
            "dl-de/by-2-0" => License::DlDeBy20,
            "http://www.govdata.de/dl-de/by-2-0" => License::DlDeBy20,
            "DL-DE->BY-2.0" => License::DlDeBy20,
            "http://dcat-ap.de/def/licenses/dl-by-de/2.0" => License::DlDeBy20,
            "http://dcat-ap.de/def/licenses/dl-by-de/2_0" => License::DlDeBy20,
            "dl-de/by/2.0" => License::DlDeBy20,
            "http://dcat-ap.de/def/licenses/dl-de/by/2.0" => License::DlDeBy20,
            // Datenlizenz Deutschland – Zero – Version 2.0
            "Datenlizenz Deutschland – Zero – Version 2.0" => License::DlDeZero20,
            "dl-zero-de/2.0" => License::DlDeZero20,
            "dl-de-zero-2.0" => License::DlDeZero20,
            "http://dcat-ap.de/def/licenses/dl-zero-de/2.0" => License::DlDeZero20,
            "http://dcat-ap.de/def/licenses/dl-zero-de/2_0" => License::DlDeZero20,
            "http://dcat-ap.de/def/licenses/dl-de-zero-2.0" => License::DlDeZero20,
            "https://www.govdata.de/dl-de/zero-2-0" => License::DlDeZero20,
            "https://www.dcat-ap.de/def/licenses/20210721#dl-zero-de/2.0" => License::DlDeZero20,
            // Datenlizenz Deutschland – Namensnennung – Version 2.0
            "Datenlizenz Deutschland Namensnennung 1.0" => License::DlDeBy10,
            // Creative Commons Namensnennung – 4.0 International CC BY 4.0)
            "cc-by/4.0" => License::CcBy40,
            "cc-by-4.0" => License::CcBy40,
            "Creative Commons Namensnennung 4.0 International"  => License::CcBy40,
            "http://dcat-ap.de/def/licenses/cc-by/4.0" => License::CcBy40,
            "http://dcat-ap.de/def/licenses/cc-by/4_0" => License::CcBy40,
            "http://dcat-ap.de/def/licenses/CC BY 4.0" => License::CcBy40,
            "https://creativecommons.org/licenses/by/4.0/" => License::CcBy40,
            "https://creativecommons.org/licenses/by-nd/4.0/deed.de" => License::CcBy40,
            "https://creativecommons.org/licenses/by/4.0/deed.de" => License::CcBy40,
            "https://creativecommons.org/licenses/by/4.0/legalcode.de" => License::CcBy40,
            "CC-BY-4.0" => License::CcBy40,
            "CC BY 4.0" => License::CcBy40,
            "http://creativecommons.org/licenses/by/4.0/" => License::CcBy40,
            // Creative Commons Attribution 3.0 Deutschland
            "cc-by/3.0" => License::CcBy30,
            "Creative Commons Namensnennung 3.0 Deutschland"=> License::CcBy30,
            // Creative Commons Attribution
            "Creative Commons Namensnennung (CC-BY)"  => License::CcBy10,
            "cc-by" => License::CcBy10,
            "BY" => License::CcBy10,
            "http://dcat-ap.de/def/licenses/cc-by" => License::CcBy10,
            // Creative Commons Attribution ShareAlike
            "Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen (CC-BY-SA)"=> License::CcBySa10,
            "cc-by-sa" => License::CcBySa10,
            "BY-SA" => License::CcBySa10,
            "http://dcat-ap.de/def/licenses/cc-by-sa" => License::CcBySa10,
            // Creative Commons Attribution NonCommercial ShareAlike
            "cc-by-nc-sa" => License::CcByNcSa10,
            "BY-NC-SA" => License::CcByNcSa10,
            // Creative Commons Attribution NonCommercial NoDerivatives
            "cc-by-nc-nd" => License::CcByNcNd10,
            "BY-NC-ND" => License::CcByNcNd10,
            // Amtliches Werk => public domain according to $5 UrhG.
            "Amtliches Werk, lizenzfrei nach §5 Abs. 1 UrhG" => License::OfficialWork,
            "officialWork" => License::OfficialWork,
            "UrhG-5" => License::OfficialWork,
            "https://www.gesetze-im-internet.de/urhg/__5.html" => License::OfficialWork,
            "http://www.gesetze-im-internet.de/urhg/__5.html" => License::OfficialWork,
            // Nutzungsbestimmungen für die Bereitstellung von Geodaten des Bundes
            "geoNutz/20130319" => License::GeoNutz20130319,
            "geonutz/20130319" => License::GeoNutz20130319,
            "http://dcat-ap.de/def/licenses/geonutz/20130319" => License::GeoNutz20130319,
            "geonutzv-de-2013-03-19" => License::GeoNutz20130319,
            "https://sg.geodatenzentrum.de/web_public/gdz/lizenz/geonutzv.pdf" => License::GeoNutz20130319,
            "http://www.gesetze-im-internet.de/bundesrecht/geonutzv/gesamt.pdf" => License::GeoNutz20130319,
            "https://www.gesetze-im-internet.de/geonutzv/GeoNutzV.pdf"  => License::GeoNutz20130319,
            "Nutzungsbestimmungen für die Bereitstellung von Geodaten des Bundes" => License::GeoNutz20130319,
            // Nutzungsbestimmungen für die Bereitstellung von Geodaten des Landes Berlin
            "geoNutz/20131001" => License::GeoNutz20131001,
            "geonutz/20131001" => License::GeoNutz20131001,
            "http://dcat-ap.de/def/licenses/geonutz/20131001" => License::GeoNutz20131001,
            // Nutzungsbestimmungen Niedersachsen
            "https://www.lgln.niedersachsen.de/startseite/wir_uber_uns_amp_organisation/allgemeine_geschafts_und_nutzungsbedingungen_agnb/allgemeine-geschafts-und-nutzungsbedingungen-agnb-97401.html" => License::GeoNutzNi,
            // Nutzungsbestimmungen des amtlichen Versmessungswesen Sachsen-Anhalt
            "https://www.lvermgeo.sachsen-anhalt.de/de/nutzungsbedingungen.html"  => License::GeoNutzSt50,
            "https://www.lvermgeo.sachsen-anhalt.de/de/datei/anzeigen/id/3567,501/nutzungsbedingungen_b.pdf" => License::GeoNutzSt50,
            // Die Allgemeine Geschäfts- und Nutzungsbedingungen (AGNB) des Geoservers Schleswig-Holstein Stand
            "https://geoserver.gdi-sh.de/agnb" => License::GeoNutzSh20230801,
            "https://gld.lhw-sachsen-anhalt.de/" => License::LhwNutzSt,
            // Nutzungsbedingungen
            "https://www.shop.lvgl.saarland.de/cloud/index.php/s/Nutzungsbedingungen_Geofachdaten" => License::GeoNutzSl,
            // Nutzungsbedingungen für Sentinel Daten:
            "https://sentinel.esa.int/documents/247904/690755/Sentinel_Data_Legal_Notice"  => License::Sentinel247904_690755,
            // Andere offene Lizenz
            "Es gelten keine Bedingungen" => License::OtherOpen,
            "Keine Beschränkungen"  => License::OtherOpen,
            "Keine Bedingungen" => License::OtherOpen,
            "other-open" => License::OtherOpen,
            "http://dcat-ap.de/def/licenses/other-open" => License::OtherOpen,
            // Andere Open Source Lizenz
            "other-opensource" => License::OtherOpenSource,
            "http://dcat-ap.de/def/licenses/other-opensource" => License::OtherOpenSource,
            "info:eu-repo/semantics/openAccess" => License::OtherOpenSource,
            // Andere geschlossene Lizenz
            "other-closed" => License::OtherClosed,
            "Andere geschlossene Lizenz"=> License::OtherClosed,
            "http://dcat-ap.de/def/licenses/other-closed" => License::OtherClosed,
            "info:eu-repo/semantics/closedAccess" => License::OtherClosed,
            "http://rightsstatements.org/vocab/InC/1.0/" => License::OtherClosed,
            // Open Data Commons Open Database License (ODbL)
            "odbl" => License::Odbl,
            "http://dcat-ap.de/def/licenses/odbl" => License::Odbl,
            "Open Data Commons Open Database License (ODbL)" => License::Odbl,
            "odc-odbl-1.0" => License::Odbl,
            "http://opendefinition.org/licenses/odc-odbl/" => License::Odbl,
            "http://www.opendefinition.org/licenses/odc-odbl"=> License::Odbl,
            // Open Data Commons Public Domain Dedication and Licence (PDDL)
            "pddl" => License::Pddl,
            "http://www.opendefinition.org/licenses/odc-pddl" => License::Pddl,
            // Creative Commons CC Zero License (cc-zero)
            "cc-zero" => License::CcZero,
            "https://opendefinition.org/licenses/cc-zero/" => License::CcZero,
            "http://dcat-ap.de/def/licenses/cc-zero" => License::CcZero,
            "http://www.opendefinition.org/licenses/cc-zero" => License::CcZero,
            "http://www.opendefinition.org/licenses/cc-zero/" => License::CcZero,
            // Creative Commons Namensnennung 3.0 Deutschland (CC BY 3.0 DE)
            "cc-by-de/3.0" => License::CcBy30De,
            "http://dcat-ap.de/def/licenses/cc-by-de/3.0" => License::CcBy30De,
            "CC BY 3.0 DE" => License::CcBy30De,
            "cc-by-3.0" => License::CcBy30De,
            "http://dcat-ap.de/def/licenses/cc-by-nc/3.0" => License::CcBy30De,
            // Creative Commons Namensnennung-Nicht kommerziell 3.0 Deutschland (CC BY-NC 3.0 DE)
            "cc-by-nc/3.0" => License::CcByNc30De,
            "cc-nc-3.0" => License::CcByNc30De,
            "http://creativecommons.org/licenses/by-nc/3.0/de/"  => License::CcByNc30De,
            "http://dcat-ap.de/def/licenses/cc-by-nc-de/3.0" => License::CcByNc30De,
            // Creative Commons Namensnennung - Nicht kommerziell 4.0 International (CC BY-NC 4.0)
            "Creative Commons Namensnennung - Nicht kommerziell 4.0 International" => License::CcByNc40,
            "cc-by-nc/4.0" => License::CcByNc40,
            "http://dcat-ap.de/def/licenses/cc-by-nc/4.0" => License::CcByNc40,
            "http://creativecommons.org/licenses/by-nc/4.0/" => License::CcByNc40,
            "https://creativecommons.org/licenses/by-nc/4.0/legalcode.de" => License::CcByNc40,
            "CC BY-NC 4.0" => License::CcByNc40,
            // Creative Commons Namensnennung - - Keine Bearbeitung 4.0 International (CC BY-ND 4.0)
            "Creative Commons Namensnennung - Keine Bearbeitungen 4.0 International" => License::CcByNd40,
            "cc-by-nd/4.0" => License::CcByNd40,
            "https://creativecommons.org/licenses/by-nd/4.0/" => License::CcByNd40,
            "http://dcat-ap.de/def/licenses/cc-by-nd/4.0" => License::CcByNd40,
            "CC BY-ND 4.0" => License::CcByNd40,
            // Public Domain Mark 1.0 (PDM)
            "ccpdm/1.0" => License::CcPdm10,
            "http://dcat-ap.de/def/licenses/ccpdm/1.0" => License::CcPdm10,
            // Creative Commons Namensnennung-Nicht kommerziell-Keine Bearbeitung 3.0 Deutschland (CC BY-NC-ND 3.0 DE)
            "cc-by-nc-nd/3.0" => License::CcByNcNd30De,
            "cc-by-nc-nd-3.0" => License::CcByNcNd30De,
            // Datenlizenz Deutschland – Namensnennung – nicht kommerziell – Version 1.0
            "dl-de-by-nc-1.0" => License::DlDeByNc10,
            "dl-by-nc-de/1.0" => License::DlDeByNc10,
            "http://dcat-ap.de/def/licenses/dl-by-nc-de/1.0" => License::DlDeByNc10,
            "https://www.govdata.de/dl-de/by-nc-1-0" => License::DlDeByNc10,
            // Creative Commons Attribution - ShareAlike 4.0 International (CC BY-SA 4.0)
            "cc-by-sa/4.0" => License::CcBySa40,
            "http://creativecommons.org/licenses/by-sa/4.0/deed.de" => License::CcBySa40,
            "https://creativecommons.org/licenses/by-sa/4.0/"=> License::CcBySa40,
            "http://dcat-ap.de/def/licenses/cc-by-sa/4.0" => License::CcBySa40,
            "cc-by-sa-4.0" => License::CcBySa40,
            // Datenlizenz Deutschland – Namensnennung – Version 1.0
            "http://dcat-ap.de/def/licenses/dl-by-de/1.0" => License::DlDeBy10,
            "dl-by-de/1.0" => License::DlDeBy10,
            // Creative Commons Namensnennung - Nicht kommerziell International (CC BY-NC)
            "http://dcat-ap.de/def/licenses/cc-by-nc" => License::CcByNc,
            // Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International (CC BY-NC-SA 4.0)
            "Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International"=> License::CcByNcSa40,
            "CC BY-NC-SA 4.0" => License::CcByNcSa40,
            "http://dcat-ap.de/def/licenses/cc-by-nc-sa/4.0" => License::CcByNcSa40,
            // Creative Commons Namensnennung - Nicht-kommerziell - Keine Bearbeitung 4.0 International (CC BY-NC-ND 4.0)
            "Creative Commons Namensnennung - Nicht kommerziell - Keine Bearbeitungen 4.0 International" => License::CcByNcNd40,
            "CC BY-NC-ND 4.0" => License::CcByNcNd40,
            // GNU General Public License version 3
            "GNU General Public License version 3.0 (GPLv3)" => License::Gpl3,
            "gpl/3.0" => License::Gpl3,
            // Nutzungsbedingungen für die Nutzung des Open-Government-Data-Portals der Landeshauptstadt Potsdam
            "https://potsdam.opendatasoft.com/terms/terms-of-use/" => License::NutzPotsdam,
            // GeoLizenz.org Webanwendung für einheitliche Lizenzierung von Geoinformationen
            "geolizenz-open" => License::GeoLizenz,
            "geolizenz-closed" => License::GeoLizenz,
            // Creative Commons Namensnennung-Keine Bearbeitung 1.0 Generic (CC BY-ND)
            "cc-by-nd" => License::CcByNd10,
            // Creative Commons Namensnennung-Keine Bearbeitung 3.0 (CC BY-ND)
            "cc-by-nd/30" => License::CcByNd30,
            "https://kwf2020.kwf-online.de/wp-content/uploads/2020/12/Anlage_1_Creative_Commons_Lizenz_CC-BY-ND_3.0.pdf"  => License::CcByNd30,
            // Creative Commons Namensnennung-Nicht kommerziell 1.0 Generic (CC BY-NC)
            "Creative Commons Namensnennung - Nicht kommerziell (CC BY-NC)"  => License::CcByNc10,
            "cc-by-nc" => License::CcByNc10,
            // INSPIRE: Conditions Applying To Access And Use: No Conditions Apply
            "http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/noConditionsApply" => License::InspireNoConditionsApply,
            "http://inspire.ec.europa.eu/metadatacodelist/ConditionsApplyingToAccessAndUse/noConditionsApply" => License::InspireNoConditionsApply,
            "http://inspire.ec.europa.eu/metadata-codelist/ ConditionsApplyingToAccessAndUse/noConditionsApply" => License::InspireNoConditionsApply,
        };

        let val = val.trim();

        if val.is_empty() {
            return Self::Unknown;
        }

        match LICENSES.get(val) {
            Some(license) => *license,
            None => {
                tracing::trace!("Failed to parse license: '{}'", val);

                License::Unknown
            }
        }
    }
}

impl From<Option<&'_ str>> for License {
    fn from(val: Option<&str>) -> Self {
        match val {
            Some(val) => val.into(),
            None => Self::Unknown,
        }
    }
}

impl fmt::Display for License {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(self.as_str())
    }
}

/// The license under which the dataset is distributed.
///
/// Here we provide an overview of our approach to assigning licenses for data records. The decisions made to date are documented here and will be continuously expanded as required.
///
/// ## Decisions
///
/// - In general, we are guided by [DCAT-AP.de](https://www.dcat-ap.de/def/licenses/20210721.html).
/// - If information on reserved rights is available in the form of "all rights reserved", "Alle Rechte vorbehalten" or the copyright symbol ©,  [`all-rights-reserved`](https://en.wikipedia.org/wiki/All_rights_reserved) is attributed.
/// - If information on reserved rights is available but not clearly specified or in case that there are multiple closed licenses, the license [`other-closed`](https://www.dcat-ap.de/def/licenses/20210721.html#other-closed) is attributed.
/// - If there is an unspecified open license, the license [`other-open`](https://www.dcat-ap.de/def/licenses/20210721.html#other-open) is attributed.
/// - If there are multiple open licenses, these are recorded as `MixedOpen`.
/// - If there are multiple partly open and partly closed or unknown licenses, these are recorded as `MixedClosed`.
/// - If information on license rights is unclear or erroneous, `Unknown` is attributed.
#[derive(Serialize, ToSchema)]
pub struct LinkedLicense<'a> {
    #[schema(value_type = String)]
    path: Facet,
    label: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    url: Option<&'static str>,
}

impl<'a> From<&'a License> for LinkedLicense<'a> {
    fn from(val: &'a License) -> Self {
        Self {
            path: Facet::from_path(val.facet()),
            label: val.as_str(),
            url: val.url(),
        }
    }
}
