use std::mem::swap;

use anyhow::Result;
use serde::{Deserialize, Serialize};
use time::{Date, Month};
use utoipa::ToSchema;

#[derive(
    Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize, ToSchema,
)]
/// Interval defined by its first and last dates
pub struct TimeRange {
    pub from: Date,
    pub until: Date,
}

impl TimeRange {
    pub fn whole_year(year: i32) -> Result<Self> {
        let from = Date::from_calendar_date(year, Month::January, 1)?;
        let until = Date::from_calendar_date(year, Month::December, 31)?;

        Ok(Self { from, until })
    }

    pub fn multiple_whole_years(start_year: i32, end_year: i32) -> Result<Self> {
        let from = Date::from_calendar_date(start_year, Month::January, 1)?;
        let until = Date::from_calendar_date(end_year, Month::December, 31)?;

        Ok(Self { from, until })
    }
}

impl From<Date> for TimeRange {
    fn from(at: Date) -> Self {
        Self {
            from: at,
            until: at,
        }
    }
}

impl<D> From<(D, D)> for TimeRange
where
    D: Into<Date>,
{
    fn from((from, until): (D, D)) -> Self {
        let mut from = from.into();
        let mut until = until.into();

        if from > until {
            tracing::error!("Fixing invalid time range from {from} until {until}");

            swap(&mut from, &mut until);
        }

        Self { from, until }
    }
}
