pub mod auto_classify;
mod cache;
pub mod similar_terms;

use std::borrow::Cow;
use std::env::var;
use std::fs::read;
use std::iter::once;
use std::ops::Deref;
use std::sync::Arc;
use std::time::{Duration, Instant, SystemTime};

use anyhow::{anyhow, bail, Result};
use arc_swap::{access::Access, ArcSwap};
use bincode::deserialize;
use concread::hashmap::HashMap as CowHashMap;
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use regex::{escape, Regex};
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use tantivy::schema::Facet;
use tantivy_fst::Map;
use url::Url;

use crate::data_path_from_env;

static BASE_URL: Lazy<Url> = Lazy::new(|| {
    var("UMTHES_BASE_URL")
        .ok()
        .and_then(|url| Url::parse(&url).ok())
        .unwrap_or_else(|| Url::parse("https://sns.uba.de/umthes/").unwrap())
});

pub static TAGS: Lazy<Tags> = Lazy::new(Tags::open);

pub struct Tags {
    tag_defs: ArcSwap<TagsInner>,
    placeholders: CowHashMap<u64, Arc<TagDefinition>>,
    reload: Mutex<()>,
}

struct TagsInner {
    values: CowHashMap<u64, Arc<TagDefinition>>,
    last_refresh: Instant,
    lowercase_labels: Map<Vec<u8>>,
}

impl Default for TagsInner {
    fn default() -> Self {
        let values = CowHashMap::new();
        let lowercase_labels = recompute_lowercase_labels(&values);

        Self {
            values,
            last_refresh: Instant::now(),
            lowercase_labels,
        }
    }
}

impl TagsInner {
    fn read() -> Result<Self> {
        let buf = read(data_path_from_env().join("auto-classify/tags"))?;

        let values = TagDefinition::parse(&buf)?;
        let lowercase_labels = recompute_lowercase_labels(&values);

        Ok(Self {
            values,
            last_refresh: Instant::now(),
            lowercase_labels,
        })
    }
}

impl Tags {
    #[cold]
    fn open() -> Self {
        let tag_defs = match TagsInner::read() {
            Ok(tag_defs) => tag_defs,
            Err(err) => {
                tracing::error!("Failed to open UMTHES tags: {err:#}");

                Default::default()
            }
        };

        Self {
            tag_defs: ArcSwap::from_pointee(tag_defs),
            placeholders: Default::default(),
            reload: Mutex::new(()),
        }
    }

    pub fn resolve(&self, id: u64) -> Arc<TagDefinition> {
        if let Some(val) = self.try_resolve(id) {
            return val;
        }

        match self.load(id) {
            Ok(val) => val,
            Err(err) => {
                let (val, inserted) = self.placeholder(id);

                if inserted {
                    tracing::error!(
                        "Failed to resolve definition of UMTHES tag {}: {:#}",
                        id,
                        err
                    );
                }

                val
            }
        }
    }

    fn try_resolve(&self, id: u64) -> Option<Arc<TagDefinition>> {
        let tag_defs = &*self.tag_defs.load();

        if tag_defs.last_refresh.elapsed() <= Duration::from_secs(24 * 60 * 60) {
            return tag_defs.values.read().get(&id).cloned();
        }

        None
    }

    pub fn lowercase_labels(&self) -> impl Deref<Target = Map<Vec<u8>>> {
        self.tag_defs
            .map(|tag_defs: &TagsInner| &tag_defs.lowercase_labels)
            .load()
    }

    #[cold]
    fn load(&self, id: u64) -> Result<Arc<TagDefinition>> {
        let _reload = self.reload.lock();

        if let Some(val) = self.try_resolve(id) {
            return Ok(val);
        }

        let tag_defs = TagsInner::read()?;

        let val = tag_defs.values.read().get(&id).cloned();

        self.tag_defs.store(Arc::new(tag_defs));

        val.ok_or_else(|| anyhow!("Missing even after load"))
    }

    #[cold]
    fn placeholder(&self, id: u64) -> (Arc<TagDefinition>, bool) {
        {
            let placeholders = self.placeholders.read();

            if let Some(val) = placeholders.get(&id) {
                return (val.clone(), false);
            }
        }

        let val = Arc::new(TagDefinition {
            label: format!("UMTHES/{id:08x}").into(),
            alt_labels: Default::default(),
            facets: vec![Facet::from(&format!("/UMTHES/{id:08x}"))],
            depth: 0,
            modified: SystemTime::UNIX_EPOCH,
            parents: SmallVec::new(),
        });

        let mut placeholders = self.placeholders.write();
        placeholders.insert(id, val.clone());
        placeholders.commit();

        (val, true)
    }
}

fn recompute_lowercase_labels(values: &CowHashMap<u64, Arc<TagDefinition>>) -> Map<Vec<u8>> {
    let mut labels = values
        .read()
        .iter()
        .flat_map(|(id, tag_def)| {
            tag_def
                .labels()
                .map(|label| (label.to_lowercase().into_bytes(), *id))
        })
        .collect::<Vec<_>>();

    labels.sort_unstable_by(|(lhs, _), (rhs, _)| lhs.cmp(rhs));
    labels.dedup_by(|(lhs, _), (rhs, _)| lhs == rhs);

    Map::from_iter(labels).unwrap()
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TagDefinition {
    pub label: Box<str>,
    pub alt_labels: Box<[Box<str>]>,
    pub facets: Vec<Facet>,
    pub depth: u8,
    modified: SystemTime,
    parents: SmallVec<[u64; 2]>,
}

#[derive(Debug, Serialize, Deserialize)]
struct OldTagDefinition {
    label: Box<str>,
    alt_labels: Box<[Box<str>]>,
    facets: Vec<Facet>,
    modified: SystemTime,
    parents: SmallVec<[u64; 2]>,
}

const CURR_VER: u8 = 1;
const OLD_VER: u8 = 0;

impl TagDefinition {
    pub fn labels(&self) -> impl Iterator<Item = &'_ str> + '_ {
        once(&*self.label).chain(self.alt_labels.iter().map(|alt_label| &**alt_label))
    }

    fn parse<S, T>(buf: &[u8]) -> Result<S>
    where
        S: FromIterator<(u64, T)>,
        T: From<Self>,
    {
        let vals = match buf.split_last() {
            Some((&CURR_VER, buf)) => deserialize::<HashMap<u64, Self>>(buf)?
                .into_iter()
                .map(|(id, val)| (id, val.into()))
                .collect(),
            Some((&OLD_VER, buf)) => deserialize::<HashMap<u64, OldTagDefinition>>(buf)?
                .into_iter()
                .map(|(id, old_val)| {
                    let depth = old_val
                        .facets
                        .iter()
                        .map(|facet| {
                            facet
                                .encoded_str()
                                .as_bytes()
                                .iter()
                                .filter(|byte| **byte == 0)
                                .count() as u8
                        })
                        .min()
                        .unwrap_or(0);

                    let val = Self {
                        label: old_val.label,
                        alt_labels: old_val.alt_labels,
                        facets: old_val.facets,
                        depth,
                        modified: old_val.modified,
                        parents: old_val.parents,
                    };

                    (id, val.into())
                })
                .collect(),
            _ => bail!("Missing or invalid tag definitions format version"),
        };

        Ok(vals)
    }
}

fn strip_disambiguation<T>(term: &str) -> T
where
    T: for<'a> From<Cow<'a, str>>,
{
    static DISAMBIGUATION: Lazy<Regex> = Lazy::new(|| Regex::new(r"\s+\[.+\]").unwrap()); // removes e.g. "[benutze Unterbegriffe]"

    DISAMBIGUATION.replace_all(term, "").into()
}

pub fn extract_id(link: &str) -> Result<u64> {
    static LINK: Lazy<Regex> = Lazy::new(|| {
        let base_url = escape(BASE_URL.as_str());

        Regex::new(&format!(
            r"(?:https?://(?:sns\.uba|(?:www\.)?semantic-network)\.de/(?:umthes/)?|{base_url})_([[:xdigit:]]+)"
        ))
        .unwrap()
    });

    let captures = LINK
        .captures(link)
        .ok_or_else(|| anyhow!("Failed to match link: {}", link))?;

    let id = u64::from_str_radix(&captures[1], 16)?;

    Ok(id)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn keep_results_but_remove_disambiguations() {
        assert_eq!(strip_disambiguation::<String>("Aller [Fluss]"), "Aller");

        assert_eq!(
            strip_disambiguation::<String>("Binnenschifffahrt"),
            "Binnenschifffahrt"
        );
    }

    #[test]
    fn extract_id_works() {
        assert_eq!(
            extract_id("https://sns.uba.de/umthes/_00013251").unwrap(),
            78417
        );

        assert_eq!(
            extract_id("https://sns.uba.de/umthes/_0e52de84").unwrap(),
            240311940
        );

        assert_eq!(
            extract_id("https://www.semantic-network.de/umthes/_00052284").unwrap(),
            336516
        );

        assert_eq!(
            extract_id("https://sns.uba.de/_00018829@Ozon").unwrap(),
            100393
        );
    }
}
