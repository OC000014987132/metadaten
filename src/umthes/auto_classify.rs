use std::mem::swap;
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use anyhow::{anyhow, ensure, Result};
use bincode::serialize;
use cap_std::fs::Dir;
use concread::hashmap::asynch::{HashMap as ConcHashMap, HashMapWriteTxn};
use fastrand::{bool as rand_bool, u64 as rand_u64};
use openssl::sha::Sha256;
use parking_lot::Mutex;
use reqwest::{Client, StatusCode};
use serde::{Deserialize, Serialize};
use serde_roxmltree::roxmltree::Document;
use smallvec::{smallvec, SmallVec};
use tantivy::schema::Facet;
use tokio::sync::Semaphore;

use crate::{
    dataset::{r#type::Type, Dataset, Language, Tag},
    metrics::Metrics,
    retry::{retry_request, retry_request_if},
    umthes::{
        cache::Cache, extract_id, strip_disambiguation, TagDefinition, BASE_URL, CURR_VER, TAGS,
    },
    HexBytes, HumanDuration,
};

pub struct AutoClassifyClient {
    dir: Dir,
    cache: Cache,
    max_age: Duration,
    age_smear: Duration,
    concurrency: Semaphore,
    tag_defs: ConcHashMap<u64, TagDefinition>,
}

impl AutoClassifyClient {
    pub fn new(config: &AutoClassifyConfig, dir: &Dir) -> Result<Option<Self>> {
        if config.disabled.unwrap_or(false) {
            return Ok(None);
        }

        let _ = dir.create_dir("auto-classify");
        let dir = dir.open_dir("auto-classify")?;

        let cache = Cache::open(&dir)?;

        let tag_defs = if let Ok(buf) = dir.read("tags") {
            TagDefinition::parse(&buf)?
        } else {
            Default::default()
        };

        let max_age = config
            .max_age
            .map_or(Duration::from_secs(30 * 24 * 60 * 60), Duration::from);

        let age_smear = config
            .age_smear
            .map_or(Duration::from_secs(14 * 24 * 60 * 60), Duration::from);

        ensure!(
            max_age >= age_smear,
            "AutoClassify maximum age must be equals to or larger than age smear"
        );

        Ok(Some(Self {
            dir,
            cache,
            max_age,
            age_smear,
            concurrency: Semaphore::new(config.concurrency.unwrap_or(10)),
            tag_defs,
        }))
    }

    pub async fn update_tags(
        &self,
        client: &Client,
        metrics: &Mutex<Metrics>,
        dataset: &mut Dataset,
    ) -> Result<()> {
        let max_age = smear_max_age(self.max_age, self.age_smear);

        if let Some(tags) = fetch_tags(
            max_age,
            client,
            metrics,
            &self.concurrency,
            &self.cache,
            dataset,
        )
        .await?
        {
            dataset.tags.extend(tags.into_iter().map(Tag::Umthes));
        }

        for tag in &mut dataset.tags {
            if let Tag::Other(val) = tag {
                if let Ok(id) = extract_id(val) {
                    *tag = Tag::Umthes(id);
                } else if let Some(id) = search(
                    max_age,
                    client,
                    metrics,
                    &self.concurrency,
                    &self.cache,
                    val,
                )
                .await?
                {
                    *tag = Tag::Umthes(id);
                }
            }

            if let Tag::Umthes(id) = tag {
                update_tag_def(&self.tag_defs, &self.cache, max_age, client, metrics, *id).await?;
            }
        }

        Ok(())
    }

    pub async fn write_tag_defs(&mut self, metrics: &mut Metrics) -> Result<()> {
        let tag_defs = self.tag_defs.read();

        let mut buf = serialize(&tag_defs)?;
        buf.push(CURR_VER);

        self.dir.write("tags.new", &buf)?;
        self.dir.rename("tags.new", &self.dir, "tags")?;

        metrics.umthes.tag_definitions = tag_defs.len();

        metrics.umthes.tags = self
            .cache
            .clean_auto_classify(self.max_age, self.age_smear)?;

        metrics.umthes.search_results = self.cache.clean_search(self.max_age, self.age_smear)?;

        self.cache.compact()?;

        Ok(())
    }
}

#[derive(Default, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AutoClassifyConfig {
    disabled: Option<bool>,
    max_age: Option<HumanDuration>,
    age_smear: Option<HumanDuration>,
    concurrency: Option<usize>,
}

#[derive(Serialize)]
struct AutoClassifyParams<'a> {
    title: &'a str,
    content: &'a str,
    language: Option<&'a str>,
}

#[derive(Debug, Deserialize)]
struct AutoClassifyResults {
    results: Vec<AutoClassifyResult>,
}

#[derive(Debug, Deserialize)]
struct AutoClassifyResult {
    link: String,
}

fn make_key(title: &str, description: &str) -> [u8; 32] {
    let mut ctx = Sha256::new();
    ctx.update(title.as_bytes());
    ctx.update(description.as_bytes());
    ctx.finish()
}

fn smear_max_age(max_age: Duration, age_smear: Duration) -> Duration {
    let age_smear = Duration::from_secs(rand_u64(0..=age_smear.as_secs()));

    if rand_bool() {
        max_age + age_smear
    } else {
        max_age - age_smear
    }
}

async fn update_tag_def(
    tag_defs: &ConcHashMap<u64, TagDefinition>,
    cache: &Cache,
    max_age: Duration,
    client: &Client,
    metrics: &Mutex<Metrics>,
    id: u64,
) -> Result<()> {
    // Fast path when the tag definition is already up-to-date.
    {
        let tag_defs = tag_defs.read();

        if let Some(tag_def) = tag_defs.get(&id) {
            let age = SystemTime::now().duration_since(tag_def.modified)?;
            if age <= max_age {
                return Ok(());
            }
        }
    }

    let mut tag_defs = tag_defs.write().await;

    // Ensure tag definition and all of its ancestors are up-to-date.
    let mut to_check: SmallVec<[u64; 2]> = smallvec![id];
    let mut updated: SmallVec<[u64; 3]> = SmallVec::new();

    while let Some(id) = to_check.pop() {
        if let Some(tag_def) = tag_defs.get_mut(&id) {
            let age = SystemTime::now().duration_since(tag_def.modified)?;
            if age <= max_age {
                continue;
            }

            let mut tag_def1 = fetch_tag_def(client, metrics, id).await?;
            updated.push(id);
            to_check.extend(tag_def1.parents.iter().copied());

            // Keep stale facets if errors occur before they are recomputed.
            swap(&mut tag_def.facets, &mut tag_def1.facets);
            *tag_def = tag_def1;
        } else {
            let tag_def = fetch_tag_def(client, metrics, id).await?;
            updated.push(id);
            to_check.extend(tag_def.parents.iter().copied());

            tag_defs.insert(id, tag_def);
        }
    }

    recompute_facets(&mut tag_defs, &updated)?;

    // Merge updated tag definitions into tags and search results.
    let tags = TAGS.tag_defs.load();

    let mut values = tags.values.write();

    for id in &updated {
        let tag_def = tag_defs.get(id).unwrap();

        values.insert(*id, Arc::new(tag_def.clone()));

        cache.update_search(*id, tag_def)?;
    }

    values.commit();

    tag_defs.commit();

    Ok(())
}

fn recompute_facets(
    tag_defs: &mut HashMapWriteTxn<u64, TagDefinition>,
    updated: &[u64],
) -> Result<()> {
    struct PartialFacet {
        path: String,
        id: u64,
        depth: u8,
    }

    let mut partial_facets = Vec::new();
    let mut facets = Vec::new();
    let mut min_depth;

    for id in updated {
        partial_facets.push(PartialFacet {
            path: String::new(),
            id: *id,
            depth: 0,
        });
        facets.clear();
        min_depth = u8::MAX;

        while let Some(partial_facet) = partial_facets.pop() {
            let tag_def = tag_defs.get(&partial_facet.id).unwrap();

            let path = format!("/{}{}", tag_def.label, partial_facet.path);

            if tag_def.parents.is_empty() {
                facets.push(Facet::from_text(&path)?);
                min_depth = min_depth.min(partial_facet.depth);
            } else if partial_facet.depth == 16 {
                tracing::warn!("Truncating facet based on UMTHES tag {id:08x}");

                facets.push(Facet::from_text(&path)?);
                min_depth = min_depth.min(partial_facet.depth);
            } else {
                partial_facets.extend(tag_def.parents.iter().map(|parent_id| PartialFacet {
                    path: path.clone(),
                    id: *parent_id,
                    depth: partial_facet.depth + 1,
                }));
            }
        }

        let tag_def = tag_defs.get_mut(id).unwrap();

        swap(&mut tag_def.facets, &mut facets);
        tag_def.depth = min_depth;
    }

    Ok(())
}

async fn fetch_tags(
    max_age: Duration,
    client: &Client,
    metrics: &Mutex<Metrics>,
    concurrency: &Semaphore,
    cache: &Cache,
    dataset: &Dataset,
) -> Result<Option<Vec<u64>>> {
    let description = match &dataset.description {
        Some(description) => description,
        _ => return Ok(None),
    };

    let mut extended_description;

    let description = {
        let mut measured_variables = dataset
            .types
            .iter()
            .filter_map(|r#type| match r#type {
                Type::Measurements {
                    measured_variables, ..
                } => Some(measured_variables),
                _ => None,
            })
            .flatten();

        if let Some(measured_variable) = measured_variables.next() {
            extended_description = description.to_owned();

            extended_description.push_str("\n\nVerfügbare Messwerte sind: ");
            extended_description.push_str(measured_variable);

            for measured_variable in measured_variables {
                extended_description.push_str(", ");
                extended_description.push_str(measured_variable);
            }

            &extended_description
        } else {
            description
        }
    };

    let language = match dataset.language {
        Language::German => Some("de"),
        Language::English => Some("en"),
        _ => None,
    };

    let key = make_key(&dataset.title, description);

    if let Some(tags) = cache.read_auto_classify(max_age, &key)? {
        return Ok(Some(tags));
    }

    let _concurrency = concurrency.acquire().await?;
    tracing::debug!("Fetching tags for {} from SNS", HexBytes(&key));
    metrics.lock().umthes.fetched_tags += 1;

    let response = retry_request_if(
        None,
        || async {
            client
                .post(BASE_URL.join("de/autoclassify/plain.json").unwrap())
                .form(&AutoClassifyParams {
                    title: &dataset.title,
                    content: description,
                    language,
                })
                .send()
                .await?
                .error_for_status()?
                .json::<AutoClassifyResults>()
                .await
        },
        |err| {
            !matches!(
                err.status(),
                Some(StatusCode::BAD_REQUEST) | Some(StatusCode::NOT_ACCEPTABLE)
            )
        },
    )
    .await?;

    let tags = response
        .results
        .into_iter()
        .map(|result| extract_id(&result.link))
        .collect::<Result<Vec<_>>>()?;

    let tags = cache.write_auto_classify(&key, tags)?;

    Ok(Some(tags))
}

async fn fetch_tag_def(
    client: &Client,
    metrics: &Mutex<Metrics>,
    id: u64,
) -> Result<TagDefinition> {
    tracing::debug!("Fetching definition of tag {:08x} from SNS", id);
    metrics.lock().umthes.fetched_tag_definitions += 1;

    let text = retry_request(None, || async {
        client
            .get(format!(
                "https://sns.uba.de/umthes/de/concepts/_{id:08x}.rdf"
            ))
            .send()
            .await?
            .error_for_status()?
            .text()
            .await
    })
    .await?;

    let document = Document::parse(&text)?;

    for element in document.root_element().children() {
        if !element.has_tag_name("Description") {
            continue;
        }

        if !element
            .children()
            .any(|element| element.has_tag_name("type"))
        {
            continue;
        }

        let label = match element
            .children()
            .find(|element| {
                element.has_tag_name("prefLabel")
                    && element
                        .attributes()
                        .any(|attribute| attribute.name() == "lang" && attribute.value() == "de")
            })
            .and_then(|element| element.text())
        {
            Some(label) => label,
            None => continue,
        };

        let label = strip_disambiguation(
            label
                .strip_prefix('[')
                .and_then(|label| label.strip_suffix(']'))
                .unwrap_or(label),
        );

        let alt_labels = element
            .children()
            .filter(|element| {
                element.has_tag_name("altLabel")
                    && element
                        .attributes()
                        .any(|attribute| attribute.name() == "lang" && attribute.value() == "de")
            })
            .filter_map(|element| element.text())
            .map(strip_disambiguation)
            .collect();

        let mut tag_def = TagDefinition {
            label,
            alt_labels,
            facets: Vec::new(),
            depth: 0,
            modified: SystemTime::now(),
            parents: SmallVec::new(),
        };

        for element in element.children() {
            if !element.has_tag_name("broader") {
                continue;
            }

            let resource = match element
                .attributes()
                .find(|attribute| attribute.name() == "resource")
                .map(|attribute| attribute.value())
            {
                Some(resource) => resource,
                None => continue,
            };

            let id = extract_id(resource)?;
            tag_def.parents.push(id);
        }

        return Ok(tag_def);
    }

    Err(anyhow!("Failed to parse definition of tag {:08x}", id))
}

async fn search(
    max_age: Duration,
    client: &Client,
    metrics: &Mutex<Metrics>,
    concurrency: &Semaphore,
    cache: &Cache,
    query: &str,
) -> Result<Option<u64>> {
    if let Some(id) = cache.read_search(max_age, query)? {
        return Ok(id);
    }

    let mut id = None;

    let _concurrency = concurrency.acquire().await?;
    tracing::debug!("Searching tag for `{}` in UMTHES", query);
    metrics.lock().umthes.searched_tags += 1;

    #[derive(Serialize)]
    struct Params<'a> {
        r#for: &'a str,
        qt: &'a str,
        q: &'a str,
    }

    let text = retry_request_if(
        None,
        || async {
            client
                .get(BASE_URL.join("de/search.rdf").unwrap())
                .query(&Params {
                    r#for: "concept",
                    qt: "exact",
                    q: query,
                })
                .send()
                .await?
                .error_for_status()?
                .text()
                .await
        },
        |err| {
            !matches!(
                err.status(),
                Some(StatusCode::BAD_REQUEST) | Some(StatusCode::NOT_ACCEPTABLE)
            )
        },
    )
    .await?;

    let document = Document::parse(&text)?;

    for element in document.root_element().children() {
        if !element.has_tag_name("Description") {
            continue;
        }

        let link = match element
            .children()
            .find(|element| element.has_tag_name("link"))
            .and_then(|element| {
                element
                    .attributes()
                    .find(|attribute| attribute.name() == "resource")
                    .map(|attribute| attribute.value())
            }) {
            Some(link) => link,
            None => continue,
        };

        if let Ok(id1) = extract_id(link) {
            id = Some(id1);
            break;
        }
    }

    cache.write_search(query, id)?;

    Ok(id)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn make_key_works() {
        let key = make_key("title", "description");
        assert_eq!(
            HexBytes(&key).to_string(),
            "c4ebc64ad7f978eb6ec5ed149321fe3e3caa019494f8984e5fcf27cfed54a128"
        );
    }

    #[tokio::test]
    async fn recompute_facets_works() {
        let tag_defs = ConcHashMap::new();
        let mut tag_defs = tag_defs.write().await;

        tag_defs.insert(
            1,
            TagDefinition {
                label: "foo".into(),
                alt_labels: Default::default(),
                facets: Vec::new(),
                depth: 0,
                modified: SystemTime::now(),
                parents: SmallVec::new(),
            },
        );
        tag_defs.insert(
            2,
            TagDefinition {
                label: "bar".into(),
                alt_labels: Default::default(),
                facets: Vec::new(),
                depth: 0,
                modified: SystemTime::now(),
                parents: smallvec![1],
            },
        );
        tag_defs.insert(
            3,
            TagDefinition {
                label: "baz".into(),
                alt_labels: Default::default(),
                facets: Vec::new(),
                depth: 0,
                modified: SystemTime::now(),
                parents: smallvec![2, 1],
            },
        );

        recompute_facets(&mut tag_defs, &[1, 2, 3]).unwrap();

        assert_eq!(
            tag_defs.get(&1).unwrap().facets,
            &[Facet::from_text("/foo").unwrap()]
        );
        assert_eq!(tag_defs.get(&1).unwrap().depth, 0);
        assert_eq!(
            tag_defs.get(&2).unwrap().facets,
            &[Facet::from_text("/foo/bar").unwrap()]
        );
        assert_eq!(tag_defs.get(&2).unwrap().depth, 1);
        assert_eq!(
            tag_defs.get(&3).unwrap().facets,
            &[
                Facet::from_text("/foo/baz").unwrap(),
                Facet::from_text("/foo/bar/baz").unwrap()
            ]
        );
        assert_eq!(tag_defs.get(&3).unwrap().depth, 1);
    }

    #[tokio::test]
    async fn recompute_facets_handles_cycles() {
        let tag_defs = ConcHashMap::new();
        let mut tag_defs = tag_defs.write().await;

        tag_defs.insert(
            1,
            TagDefinition {
                label: "foo".into(),
                alt_labels: Default::default(),
                facets: Vec::new(),
                depth: 0,
                modified: SystemTime::now(),
                parents: smallvec![2],
            },
        );
        tag_defs.insert(
            2,
            TagDefinition {
                label: "bar".into(),
                alt_labels: Default::default(),
                facets: Vec::new(),
                depth: 0,
                modified: SystemTime::now(),
                parents: smallvec![1],
            },
        );

        recompute_facets(&mut tag_defs, &[1, 2]).unwrap();

        assert_eq!(
            tag_defs.get(&1).unwrap().facets,
            &[Facet::from_text(
                "/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo"
            )
            .unwrap()]
        );
        assert_eq!(tag_defs.get(&1).unwrap().depth, 16);
        assert_eq!(
            tag_defs.get(&2).unwrap().facets,
            &[Facet::from_text(
                "/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar/foo/bar"
            )
            .unwrap()]
        );
        assert_eq!(tag_defs.get(&2).unwrap().depth, 16);
    }
}
