use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::Error;
use compact_str::CompactString;
use moka::future::Cache;
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use reqwest::Client;
use serde_roxmltree::roxmltree::Document;
use sketches_ddsketch::DDSketch;
use unicase::UniCase;

use crate::umthes::{strip_disambiguation, BASE_URL};

static CACHE: Lazy<Cache<CompactString, Arc<[String]>>> = Lazy::new(|| {
    Cache::builder()
        .time_to_live(Duration::from_secs(24 * 60 * 60))
        .build()
});

pub async fn fetch_similar_terms(
    client: &Client,
    request_duration: &Mutex<DDSketch>,
    terms: &[CompactString],
) -> Vec<(CompactString, Arc<[String]>)> {
    let mut results = Vec::with_capacity(terms.len());

    for term in terms {
        let results1 = CACHE
            .try_get_with_by_ref::<_, Error, _>(term, async {
                tracing::debug!("Fetching terms similar to {} from SNS", term);

                let start = Instant::now();

                let text = client
                    .get(BASE_URL.join("de/similar.rdf").unwrap())
                    .query(&[("terms", term)])
                    .send()
                    .await?
                    .error_for_status()?
                    .text()
                    .await?;

                request_duration.lock().add(start.elapsed().as_secs_f64());

                let document = Document::parse(&text)?;

                let term = UniCase::new(term);

                let results = document
                    .descendants()
                    .filter(|node| node.has_tag_name("altLabel"))
                    .filter(|element| {
                        element.attributes().any(|attribute| {
                            attribute.name() == "lang" && attribute.value() == "de"
                        })
                    })
                    .filter_map(|element| element.text())
                    .map(strip_disambiguation)
                    .filter(|result| UniCase::new(result) != term)
                    .collect();

                Ok(results)
            })
            .await;

        match results1 {
            Ok(results1) => {
                if !results1.is_empty() {
                    results.push((term.clone(), results1.clone()));
                }
            }
            Err(err) => tracing::warn!("Failed to fetch similar terms: {:#}", err),
        }
    }

    results
}
