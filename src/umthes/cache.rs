use std::time::{Duration, SystemTime};

use anyhow::Result;
use cap_std::fs::Dir;
use parking_lot::RwLock;
use serde::{Deserialize, Serialize};

use crate::{
    coarse_grained_timestamp,
    file_backed_hash_map::{Bincode, FileBackedHashMap},
    umthes::TagDefinition,
};

pub struct Cache {
    auto_classify: RwLock<FileBackedHashMap<[u8], Bincode<AutoClassifyEntry>>>,
    search: RwLock<FileBackedHashMap<str, Bincode<SearchEntry>>>,
}

#[derive(Serialize, Deserialize)]
struct AutoClassifyEntry {
    tags: Vec<u64>,
    #[serde(with = "coarse_grained_timestamp")]
    modified: SystemTime,
}

#[derive(Serialize, Deserialize)]
struct OldAutoClassifyEntry {
    tags: Vec<u64>,
    modified: SystemTime,
}

#[derive(Serialize, Deserialize)]
struct SearchEntry {
    id: Option<u64>,
    #[serde(with = "coarse_grained_timestamp")]
    modified: SystemTime,
}

#[derive(Serialize, Deserialize)]
struct OldSearchEntry {
    id: Option<u64>,
    modified: SystemTime,
}

impl Cache {
    pub fn open(dir: &Dir) -> Result<Self> {
        let mut auto_classify = FileBackedHashMap::open_path(dir, "cache_v1")?;

        if let Ok(old_auto_classify) =
            FileBackedHashMap::<[u8], Bincode<OldAutoClassifyEntry>>::open_path(dir, "cache_v0")
        {
            for res in old_auto_classify.iter() {
                let (key, entry) = res?;

                let entry = AutoClassifyEntry {
                    tags: entry.tags,
                    modified: entry.modified,
                };

                auto_classify.put(key, entry)?;
            }

            drop(old_auto_classify);
            dir.remove_file("cache_v0")?;
        }

        let mut search = FileBackedHashMap::open_path(dir, "search_cache_v1")?;

        if let Ok(old_search) =
            FileBackedHashMap::<str, Bincode<OldSearchEntry>>::open_path(dir, "search_cache_v0")
        {
            for res in old_search.iter() {
                let (key, entry) = res?;

                let entry = SearchEntry {
                    id: entry.id,
                    modified: entry.modified,
                };

                search.put(key, entry)?;
            }

            drop(old_search);
            dir.remove_file("search_cache_v0")?;
        }

        Ok(Self {
            auto_classify: RwLock::new(auto_classify),
            search: RwLock::new(search),
        })
    }

    pub fn read_auto_classify(
        &self,
        max_age: Duration,
        key: &[u8; 32],
    ) -> Result<Option<Vec<u64>>> {
        if let Some(entry) = self.auto_classify.read().get(key.as_slice())? {
            let age = SystemTime::now().duration_since(entry.modified)?;

            if age <= max_age {
                return Ok(Some(entry.tags));
            }
        }

        Ok(None)
    }

    pub fn write_auto_classify(&self, key: &[u8; 32], tags: Vec<u64>) -> Result<Vec<u64>> {
        let entry = AutoClassifyEntry {
            tags,
            modified: SystemTime::now(),
        };

        self.auto_classify.write().put(key.as_slice(), &entry)?;

        Ok(entry.tags)
    }

    pub fn clean_auto_classify(&self, max_age: Duration, age_smear: Duration) -> Result<usize> {
        let deadline = SystemTime::now() - max_age - age_smear;

        let len = self
            .auto_classify
            .write()
            .retain(|_key, entry| Ok(entry.modified >= deadline))?;

        Ok(len)
    }

    pub fn read_search(&self, max_age: Duration, query: &str) -> Result<Option<Option<u64>>> {
        if let Some(entry) = self.search.read().get(query)? {
            let age = SystemTime::now().duration_since(entry.modified)?;

            if age <= max_age {
                return Ok(Some(entry.id));
            }
        }

        Ok(None)
    }

    pub fn write_search(&self, query: &str, id: Option<u64>) -> Result<()> {
        let entry = SearchEntry {
            id,
            modified: SystemTime::now(),
        };

        self.search.write().put(query, &entry)?;

        Ok(())
    }

    pub fn update_search(&self, id: u64, tag_def: &TagDefinition) -> Result<()> {
        let entry = SearchEntry {
            id: Some(id),
            modified: tag_def.modified,
        };

        let mut search = self.search.write();

        for label in tag_def.labels() {
            if let Some(entry) = search.get(label)? {
                if entry.modified >= tag_def.modified {
                    continue;
                }
            }

            search.put(label, &entry)?;
        }

        Ok(())
    }

    pub fn clean_search(&self, max_age: Duration, age_smear: Duration) -> Result<usize> {
        let deadline = SystemTime::now() - max_age - age_smear;

        let len = self
            .search
            .write()
            .retain(|_key, entry| Ok(entry.modified >= deadline))?;

        Ok(len)
    }

    pub fn compact(&mut self) -> Result<()> {
        self.auto_classify.get_mut().compact()?;
        self.search.get_mut().compact()?;

        Ok(())
    }
}
