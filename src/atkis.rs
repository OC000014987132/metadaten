use std::fs::read;
use std::mem::replace;
use std::path::Path;
use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::{Context, Result};
use arc_swap::ArcSwap;
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use geo::Rect;
use hashbrown::hash_map::HashMap;
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::data_path_from_env;

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Database {
    pub entries: HashMap<u64, Arc<Entry>>,
}

impl Database {
    pub fn write(&self, dir: &Dir) -> Result<()> {
        let buf = serialize(self)?;

        dir.write("atkis.bin", &buf)?;

        Ok(())
    }

    fn read(path: &Path) -> Result<Self> {
        let buf = read(path)?;

        deserialize(&buf).context("Invalid ATKIS format")
    }
}

#[derive(Serialize, Deserialize)]
pub struct Entry {
    pub name: Box<str>,
    pub bounding_box: Option<Rect>,
}

pub static ATKIS: Lazy<Atkis> = Lazy::new(Atkis::open);

pub struct Atkis {
    inner: ArcSwap<AtkisInner>,
    refresh: Mutex<bool>,
}

impl Atkis {
    #[cold]
    fn open() -> Self {
        match AtkisInner::open() {
            Ok(val) => Self {
                inner: ArcSwap::from_pointee(val),
                refresh: Mutex::new(false),
            },
            Err(err) => {
                tracing::error!("Failed to open ATKIS: {err:#}");

                Self {
                    inner: Default::default(),
                    refresh: Mutex::new(true),
                }
            }
        }
    }

    pub fn resolve(&self, key: u64) -> Arc<Entry> {
        if let Some(item) = self.try_resolve(key) {
            return item;
        }

        self.refresh(key)
    }

    fn try_resolve(&self, key: u64) -> Option<Arc<Entry>> {
        let inner = self.inner.load();

        if inner.last_refresh.elapsed() < Duration::from_secs(24 * 60 * 60) {
            return inner.resolve(key);
        }

        None
    }

    #[cold]
    fn refresh(&self, key: u64) -> Arc<Entry> {
        let placeholder = || {
            Arc::new(Entry {
                name: format!("ATKIS/{key:04}").into(),
                bounding_box: None,
            })
        };

        let mut log_once = self.refresh.lock();

        if let Some(item) = self.try_resolve(key) {
            return item;
        }

        match AtkisInner::open() {
            Ok(val) => {
                let res = match val.resolve(key) {
                    Some(val) => val,
                    None => {
                        tracing::error!("Failed to resolve {} in ATKIS", key);

                        placeholder()
                    }
                };

                self.inner.store(Arc::new(val));

                *log_once = false;

                res
            }
            Err(err) => {
                if !replace(&mut log_once, true) {
                    tracing::error!("Failed to refresh ATKIS: {:#}", err);
                }

                placeholder()
            }
        }
    }

    pub fn extract(&self, val: &str) -> Option<u64> {
        self.inner.load().extract(val)
    }
}

struct AtkisInner {
    database: Database,
    last_refresh: Instant,
}

impl Default for AtkisInner {
    fn default() -> Self {
        Self {
            database: Default::default(),
            last_refresh: Instant::now(),
        }
    }
}

impl AtkisInner {
    fn open() -> Result<Self> {
        let path = data_path_from_env().join("datasets/atkis.not_indexed/atkis.bin");

        let database = Database::read(&path)?;

        Ok(Self {
            database,
            last_refresh: Instant::now(),
        })
    }

    fn resolve(&self, key: u64) -> Option<Arc<Entry>> {
        self.database.entries.get(&key).cloned()
    }

    fn extract(&self, val: &str) -> Option<u64> {
        static TK10: Lazy<Regex> = Lazy::new(|| Regex::new(r"(\d{4})-([A-Z]{2})").unwrap());

        let tk10 = TK10
            .captures(val)
            .map(|caps| {
                let val = caps[1].chars().collect::<String>();

                val.parse().unwrap()
            })
            .filter(|key| self.database.entries.contains_key(key));

        tk10
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_atkis_works() {
        let mut entries = HashMap::new();

        entries.insert(
            3652,
            Arc::new(Entry {
                name: "Jacobsdorf".into(),
                bounding_box: None,
            }),
        );

        let atkis = AtkisInner {
            database: Database { entries },
            last_refresh: Instant::now(),
        };

        assert_eq!(
            atkis.extract("Digitale Topographische Karte 1 : 10 000 - 3652-SW Jacobsdorf"),
            Some(3652)
        );

        assert_eq!(atkis.extract("ARS 100213652876"), None);
    }
}
