use std::fs::read;
use std::mem::replace;
use std::path::Path;
use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::{bail, Context, Result};
use arc_swap::ArcSwap;
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use serde::{Deserialize, Serialize};
use tantivy_fst::Map;

use crate::data_path_from_env;

pub static ORGANISATIONS: Lazy<Organisations> = Lazy::new(Organisations::open);

pub struct Organisations {
    inner: ArcSwap<OrganisationsInner>,
    refresh: Mutex<bool>,
}

impl Organisations {
    #[cold]
    fn open() -> Self {
        match OrganisationsInner::open() {
            Ok(val) => Self {
                inner: ArcSwap::from_pointee(val),
                refresh: Mutex::new(false),
            },
            Err(err) => {
                tracing::error!("Failed to open WikiData organisations: {err:#}");

                Self {
                    inner: Default::default(),
                    refresh: Mutex::new(true),
                }
            }
        }
    }

    pub fn r#match(&self, name: &str) -> Option<u64> {
        self.inner.load().r#match(name)
    }

    pub fn resolve(&self, identifier: u64) -> Arc<Organisation> {
        if let Some(val) = self.try_resolve(identifier) {
            return val;
        }

        self.refresh(identifier)
    }

    fn try_resolve(&self, identifier: u64) -> Option<Arc<Organisation>> {
        let inner = self.inner.load();

        if inner.last_refresh.elapsed() < Duration::from_secs(24 * 60 * 60) {
            return inner.resolve(identifier);
        }

        None
    }

    #[cold]
    fn refresh(&self, identifier: u64) -> Arc<Organisation> {
        let placeholder = || {
            Arc::new(Organisation {
                label: format!("WikiData/Q{identifier}").into(),
                aliases: Default::default(),
                websites: Default::default(),
            })
        };

        let mut log_once = self.refresh.lock();

        if let Some(val) = self.try_resolve(identifier) {
            return val;
        }

        match OrganisationsInner::open() {
            Ok(val) => {
                let res = match val.resolve(identifier) {
                    Some(val) => val,
                    None => {
                        tracing::error!(
                            "Failed to resolve {} in WikiData organisations",
                            identifier
                        );

                        placeholder()
                    }
                };

                self.inner.store(Arc::new(val));

                *log_once = false;

                res
            }
            Err(err) => {
                if !replace(&mut log_once, true) {
                    tracing::error!("Failed to refresh WikiData organisations: {err:#}");
                }

                placeholder()
            }
        }
    }
}

pub struct OrganisationsInner {
    database: OrganisationsDatabase,
    index: Map<Vec<u8>>,
    last_refresh: Instant,
}

impl Default for OrganisationsInner {
    fn default() -> Self {
        let database = OrganisationsDatabase::default();
        let index = recompute_organisations_index(&database.entries);

        Self {
            database,
            index,
            last_refresh: Instant::now(),
        }
    }
}

impl OrganisationsInner {
    fn open() -> Result<Self> {
        let path = data_path_from_env().join("datasets/wikidata.not_indexed/organisations.bin");

        let database = OrganisationsDatabase::read(&path)?;
        let index = recompute_organisations_index(&database.entries);

        Ok(Self {
            database,
            index,
            last_refresh: Instant::now(),
        })
    }

    fn r#match(&self, name: &str) -> Option<u64> {
        if name.is_empty() {
            return None;
        }

        self.index.get(name.to_lowercase())
    }

    fn resolve(&self, identifier: u64) -> Option<Arc<Organisation>> {
        self.database.entries.get(&identifier).cloned()
    }
}

#[derive(Default, Serialize, Deserialize)]
pub struct OrganisationsDatabase {
    pub entries: HashMap<u64, Arc<Organisation>>,
}

#[derive(Deserialize)]
struct OldOrganisationsDatabase {
    entries: HashMap<u64, OldOrganisation>,
}

const CURR_VER: u8 = 3;
const OLD_VER: u8 = 2;

impl OrganisationsDatabase {
    pub fn write(&self, dir: &Dir) -> Result<()> {
        let mut buf = serialize(self)?;
        buf.push(CURR_VER);

        dir.write("organisations.bin", &buf)?;

        Ok(())
    }

    fn read(path: &Path) -> Result<Self> {
        let buf = read(path)?;

        let res = match buf.split_last() {
            Some((&CURR_VER, buf)) => deserialize::<Self>(buf),
            Some((&OLD_VER, buf)) => {
                deserialize::<OldOrganisationsDatabase>(buf).map(|old_val| Self {
                    entries: old_val
                        .entries
                        .into_iter()
                        .map(|(id, old_val)| {
                            let val = Arc::new(Organisation {
                                label: old_val.label,
                                aliases: old_val.aliases,
                                websites: old_val.websites,
                            });

                            (id, val)
                        })
                        .collect(),
                })
            }
            _ => bail!("Missing or invalid WikiData organisations format version"),
        };

        res.context("Invalid WikiData organisations format")
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Organisation {
    pub label: Box<str>,
    pub aliases: Box<[Box<str>]>,
    pub websites: Box<[Box<str>]>,
}

#[derive(Deserialize)]
struct OldOrganisation {
    label: Box<str>,
    _description: Option<Box<str>>,
    aliases: Box<[Box<str>]>,
    websites: Box<[Box<str>]>,
}

fn recompute_organisations_index(entries: &HashMap<u64, Arc<Organisation>>) -> Map<Vec<u8>> {
    let mut labels = entries
        .iter()
        .fold(Vec::new(), |mut labels, (id, organisation)| {
            labels.push((organisation.label.to_lowercase().into_bytes(), *id));

            for alias in &organisation.aliases {
                labels.push((alias.to_lowercase().into_bytes(), *id));
            }

            labels
        });

    labels.sort_unstable_by(|(lhs, _), (rhs, _)| lhs.cmp(rhs));
    labels.dedup_by(|(lhs, _), (rhs, _)| lhs == rhs);

    Map::from_iter(labels).unwrap()
}
