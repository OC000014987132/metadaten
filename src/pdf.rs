use anyhow::Result;
use qpdf::{
    dict::QPdfDictionary,
    object::QPdfObjectType,
    stream::{ObjectStreamMode, QPdfStream, StreamDecodeLevel},
    QPdf, QPdfObjectLike,
};

/// Simplify PDF before caching them to reduce storage requirements.
pub fn simplify(buf: &[u8]) -> Result<Vec<u8>> {
    let pdf = QPdf::read_from_memory(buf)?;

    // Remove all image streams as we are interested only in text.
    for (index, page) in pdf.get_pages()?.into_iter().enumerate() {
        let resources = match page.get("/Resources") {
            Some(obj) if obj.get_type() == QPdfObjectType::Dictionary => QPdfDictionary::from(obj),
            _ => {
                tracing::trace!("Missing resources dictionary at page {index}");
                continue;
            }
        };

        let objects = match resources.get("/XObject") {
            Some(obj) if obj.get_type() == QPdfObjectType::Dictionary => QPdfDictionary::from(obj),
            _ => {
                tracing::trace!("Missing objects dictionary at page {index}");
                continue;
            }
        };

        for key in objects.keys() {
            let stream = match objects.get(&key) {
                Some(obj) if obj.get_type() == QPdfObjectType::Stream => QPdfStream::from(obj),
                _ => continue,
            };

            let dictionary = stream.get_dictionary();

            let subtype = match dictionary.get("/Subtype") {
                Some(obj) if obj.get_type() == QPdfObjectType::Name => obj.as_name(),
                _ => {
                    tracing::trace!("Missing subtype of stream at page {index}");
                    continue;
                }
            };

            if subtype != "/Image" {
                continue;
            }

            // Replace streams by nulls to avoid dangling references.
            let id = stream.get_id();
            let gen = stream.get_generation();
            pdf.replace_object(id, gen, pdf.new_null())?;
        }
    }

    // Disable as much of PDF's internal compression as possible
    // since we will compress the whole file using Zstd.
    let buf = pdf
        .writer()
        .compress_streams(false)
        .stream_decode_level(StreamDecodeLevel::Specialized)
        .object_stream_mode(ObjectStreamMode::Generate)
        .write_to_memory()?;

    Ok(buf)
}
