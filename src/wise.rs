use std::fs::read;
use std::mem::replace;
use std::ops::ControlFlow;
use std::path::Path;
use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::{bail, Context, Result};
use arc_swap::ArcSwap;
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{
    algorithm::{BoundingRect, Distance as _, Euclidean},
    Coord, LineString, Point, Polygon,
};
use hashbrown::HashMap;
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use serde::{Deserialize, Serialize};
use sif_rtree::{Distance, Object, RTree, DEF_NODE_LEN};
use tantivy_fst::Map;

use crate::data_path_from_env;

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Database {
    pub entries: HashMap<u64, Arc<Entry>>,
}

const CURR_VER: u8 = 1;

impl Database {
    pub fn write(&self, dir: &Dir) -> Result<()> {
        let mut buf = serialize(self)?;
        buf.push(CURR_VER);

        dir.write("wise.bin", &buf)?;

        Ok(())
    }

    fn read(path: &Path) -> Result<Self> {
        let buf = read(path)?;

        let res = match buf.split_last() {
            Some((&CURR_VER, buf)) => deserialize::<Self>(buf),
            _ => bail!("Missing or invalid WISE/WFD format version"),
        };

        res.context("Invalid WISE/WFD format")
    }
}

#[derive(Serialize, Deserialize)]
pub struct Entry {
    pub name: String,
    pub id: CompactString,
    pub shape: Polygon,
}

pub static WISE: Lazy<Wise> = Lazy::new(Wise::open);

pub struct Wise {
    inner: ArcSwap<WiseInner>,
    refresh: Mutex<bool>,
}

impl Wise {
    #[cold]
    fn open() -> Self {
        match WiseInner::open() {
            Ok(val) => Self {
                inner: ArcSwap::from_pointee(val),
                refresh: Mutex::new(false),
            },
            Err(err) => {
                tracing::error!("Failed to open WISE/WFD: {err:#}");

                Self {
                    inner: Default::default(),
                    refresh: Mutex::new(true),
                }
            }
        }
    }

    pub fn resolve(&self, id: u64) -> Arc<Entry> {
        if let Some(val) = self.try_resolve(id) {
            return val;
        }

        self.refresh(id)
    }

    fn try_resolve(&self, id: u64) -> Option<Arc<Entry>> {
        let inner = self.inner.load();

        if inner.last_refresh.elapsed() < Duration::from_secs(24 * 60 * 60) {
            return inner.resolve(id);
        }

        None
    }

    #[cold]
    fn refresh(&self, id: u64) -> Arc<Entry> {
        let placeholder = || {
            Arc::new(Entry {
                name: format!("WISE/{id}"),
                id: "DE????".into(),
                shape: Polygon::new(LineString::new(Vec::new()), Vec::new()),
            })
        };

        let mut log_once = self.refresh.lock();

        if let Some(val) = self.try_resolve(id) {
            return val;
        }

        match WiseInner::open() {
            Ok(val) => {
                let res = match val.resolve(id) {
                    Some(val) => val,
                    None => {
                        tracing::error!("Failed to resolve {} in WISE/WFD", id);

                        placeholder()
                    }
                };

                self.inner.store(Arc::new(val));

                *log_once = true;

                res
            }
            Err(err) => {
                if !replace(&mut log_once, true) {
                    tracing::error!("Failed to refresh WISE/WFD: {:#}", err);
                }

                placeholder()
            }
        }
    }

    pub fn match_name(&self, name: &str) -> Option<u64> {
        self.inner.load().r#match_name(name)
    }

    pub fn match_shape(&self, x: f64, y: f64) -> Option<u64> {
        self.inner.load().r#match_shape(x, y)
    }
}

struct WiseInner {
    database: Database,
    name_index: Map<Vec<u8>>,
    shape_index: Option<RTree<ShapeIndexItem>>,
    last_refresh: Instant,
}

impl Default for WiseInner {
    fn default() -> Self {
        let database = Database::default();
        let name_index = recompute_name_index(&database.entries);
        let shape_index = recompute_shape_index(&database.entries);

        Self {
            database,
            name_index,
            shape_index,
            last_refresh: Instant::now(),
        }
    }
}
impl WiseInner {
    fn open() -> Result<Self> {
        let path = data_path_from_env().join("datasets/wise.not_indexed/wise.bin");

        let database = Database::read(&path)?;
        let name_index = recompute_name_index(&database.entries);
        let shape_index = recompute_shape_index(&database.entries);

        Ok(Self {
            database,
            name_index,
            shape_index,
            last_refresh: Instant::now(),
        })
    }

    fn resolve(&self, id: u64) -> Option<Arc<Entry>> {
        self.database.entries.get(&id).cloned()
    }

    fn match_name(&self, name: &str) -> Option<u64> {
        self.name_index.get(name.to_uppercase())
    }

    fn match_shape(&self, x: f64, y: f64) -> Option<u64> {
        let mut id = None;

        if let Some(shape_index) = &self.shape_index {
            shape_index.look_up_at_point(&[x, y], |item| {
                id = Some(item.id);
                ControlFlow::Break(())
            });
        }

        id
    }
}

fn recompute_name_index(entries: &HashMap<u64, Arc<Entry>>) -> Map<Vec<u8>> {
    let mut names = entries
        .iter()
        .map(|(id, entry)| (entry.name.clone().into_bytes(), *id))
        .collect::<Vec<_>>();

    names.sort_unstable_by(|(lhs, _), (rhs, _)| lhs.cmp(rhs));
    names.dedup_by(|(lhs, _), (rhs, _)| lhs == rhs);

    Map::from_iter(names).unwrap()
}

fn recompute_shape_index(entries: &HashMap<u64, Arc<Entry>>) -> Option<RTree<ShapeIndexItem>> {
    let items = entries
        .iter()
        .map(|(id, entry)| {
            let rect = entry.shape.bounding_rect().unwrap();
            let aabb = ([rect.min().x, rect.min().y], [rect.max().x, rect.max().y]);

            ShapeIndexItem {
                entry: entry.clone(),
                aabb,
                id: *id,
            }
        })
        .collect::<Vec<_>>();

    if !items.is_empty() {
        Some(RTree::new(DEF_NODE_LEN, items))
    } else {
        None
    }
}

struct ShapeIndexItem {
    entry: Arc<Entry>,
    aabb: ([f64; 2], [f64; 2]),
    id: u64,
}

impl Object for ShapeIndexItem {
    type Point = [f64; 2];

    fn aabb(&self) -> (Self::Point, Self::Point) {
        self.aabb
    }
}

impl Distance<[f64; 2]> for ShapeIndexItem {
    fn distance_2(&self, &[x, y]: &[f64; 2]) -> f64 {
        Euclidean::distance(&self.entry.shape, &Point(Coord { x, y }))
    }
}
