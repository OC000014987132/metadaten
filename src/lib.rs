pub mod ars_ags;
pub mod atkis;
pub mod dataset;
pub mod file_backed_hash_map;
pub mod geonames;
pub mod index;
pub mod metrics;
pub mod pdf;
pub mod retry;
pub mod stats;
pub mod umthes;
pub mod wikidata;
pub mod wise;

use std::borrow::Cow;
use std::env::{var, var_os};
use std::fmt;
use std::io::{ErrorKind as IoErrorKind, Result as IoResult};
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::time::{Duration, SystemTime};

use anyhow::Result;
use cap_std::fs::{Dir, File, OpenOptions as FsOpenOptions};
use compact_str::CompactString;
use geo::{Coord, CoordFloat, Rect};
use humantime::{format_duration, parse_duration};
use miniproj::Projection;
use serde::{
    de::{Deserialize, Deserializer, Error},
    ser::{Serialize, Serializer},
};
use tantivy::{schema::Facet, Score};

pub fn data_path_from_env() -> PathBuf {
    var_os("DATA_PATH")
        .expect("Environment variable DATA_PATH not set")
        .into()
}

pub fn value_from_env<T>(key: &str) -> T
where
    T: FromStr,
{
    var(key)
        .unwrap_or_else(|_err| panic!("Environment variable {key} not set"))
        .parse()
        .unwrap_or_else(|_err| panic!("Environment variable {key} invalid"))
}

pub fn open_or_create_dir<P>(dir: &Dir, path: P) -> IoResult<Dir>
where
    P: AsRef<Path>,
{
    let path = path.as_ref();

    match dir.open_dir(path) {
        Ok(dir) => Ok(dir),
        Err(err) if err.kind() == IoErrorKind::NotFound => {
            let _ = dir.create_dir(path);
            dir.open_dir(path)
        }
        Err(err) => Err(err),
    }
}

pub fn create_new<P>(dir: &Dir, path: P) -> IoResult<Option<File>>
where
    P: AsRef<Path>,
{
    match dir.open_with(path, FsOpenOptions::new().write(true).create_new(true)) {
        Ok(file) => Ok(Some(file)),
        Err(err) if err.kind() == IoErrorKind::AlreadyExists => Ok(None),
        Err(err) => Err(err),
    }
}

pub const COMPRESSION_LEVEL: i32 = 17;

pub trait TrimExt: Deref<Target = str> {
    fn trim(self) -> Self;
}

impl TrimExt for &'_ str {
    fn trim(self) -> Self {
        self.trim()
    }
}

impl TrimExt for String {
    fn trim(mut self) -> Self {
        let val = self.as_str().trim();
        let new_len = val.len();

        if self.len() != new_len {
            let pos = val.as_ptr() as usize - self.as_ptr() as usize;
            if pos != 0 {
                self.drain(..pos);
            }
            self.truncate(new_len);
        }

        self
    }
}

impl TrimExt for Cow<'_, str> {
    fn trim(self) -> Self {
        match self {
            Self::Borrowed(val) => Self::Borrowed(val.trim()),
            Self::Owned(val) => Self::Owned(val.trim()),
        }
    }
}

impl TrimExt for CompactString {
    fn trim(mut self) -> Self {
        let val = self.as_str().trim();
        let new_len = val.len();

        if self.len() != new_len {
            let pos = val.as_ptr() as usize - self.as_ptr() as usize;
            if pos != 0 {
                let _ = self.drain(..pos);
            }
            self.truncate(new_len);
        }

        self
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct HumanDuration(pub Duration);

impl From<HumanDuration> for Duration {
    fn from(val: HumanDuration) -> Self {
        val.0
    }
}

impl Serialize for HumanDuration {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let val = format_duration(self.0).to_string();
        val.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for HumanDuration {
    fn deserialize<D>(de: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let val = Cow::<'de, str>::deserialize(de)?;
        let val = parse_duration(&val).map_err(|err| D::Error::custom(err.to_string()))?;
        Ok(Self(val))
    }
}

pub mod coarse_grained_timestamp {
    use super::*;

    pub fn serialize<S>(val: &SystemTime, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let secs = val
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        secs.serialize(serializer)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<SystemTime, D::Error>
    where
        D: Deserializer<'de>,
    {
        let secs = u64::deserialize(deserializer)?;

        Ok(SystemTime::UNIX_EPOCH + Duration::from_secs(secs))
    }
}

pub fn parse_str<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    D: Deserializer<'de>,
    T: FromStr,
    T::Err: fmt::Display,
{
    let val = <&str>::deserialize(deserializer)?;
    val.parse().map_err(Error::custom)
}

struct HexBytes<'a>(&'a [u8]);

impl fmt::Display for HexBytes<'_> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        for byte in self.0 {
            write!(fmt, "{byte:02x}")?;
        }

        Ok(())
    }
}

#[derive(Debug)]
pub struct FacetWeight {
    pub facet: Facet,
    pub weight: Score,
}

impl fmt::Display for FacetWeight {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}^{}", self.facet, self.weight)
    }
}

impl<'de> Deserialize<'de> for FacetWeight {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let text = Cow::<'de, str>::deserialize(deserializer)?;

        let (facet, weight) = text
            .split_once('^')
            .ok_or_else(|| D::Error::custom("Facet weight must be given as `<facet>^<weight>`"))?;

        let facet = Facet::from_text(facet)
            .map_err(|err| D::Error::custom(format!("Failed to parse facet: {err}")))?;
        let weight = weight
            .parse()
            .map_err(|err| D::Error::custom(format!("Failed to parse weight: {err}")))?;

        Ok(Self { facet, weight })
    }
}

impl Serialize for FacetWeight {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

pub fn proj_rect(rect: Rect, proj: &dyn Projection) -> Rect {
    let min = proj.projected_to_deg(rect.min().x, rect.min().y);
    let max = proj.projected_to_deg(rect.max().x, rect.max().y);

    Rect::new(Coord { x: min.0, y: min.1 }, Coord { x: max.0, y: max.1 })
}

#[inline]
pub fn merge_rect<T>(rect: &mut Rect<T>, other: Rect<T>)
where
    T: CoordFloat,
{
    let mut min = rect.min();
    let mut max = rect.max();

    min.x = min.x.min(other.min().x);
    min.y = min.y.min(other.min().y);
    max.x = max.x.max(other.max().x);
    max.y = max.y.max(other.max().y);

    rect.set_min(min);
    rect.set_max(max);
}

// FIXME: Replace by `cmp::minmax` when stabilized.
#[inline]
#[must_use]
pub fn minmax<T>(lhs: T, rhs: T) -> [T; 2]
where
    T: PartialOrd,
{
    if lhs <= rhs {
        [lhs, rhs]
    } else {
        [rhs, lhs]
    }
}

// FIXME: Replace by `slice::get_many_mut` when stabilized.
#[inline]
#[allow(unsafe_code)]
pub fn get_two_mut<T>(slice: &mut [T], idx1: usize, idx2: usize) -> (&mut T, &mut T) {
    assert!(idx1 < slice.len() && idx2 < slice.len() && idx1 != idx2);

    let ptr = slice.as_mut_ptr();

    let val1 = unsafe { &mut *ptr.add(idx1) };
    let val2 = unsafe { &mut *ptr.add(idx2) };

    (val1, val2)
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_json::{from_value, to_value, Value};

    #[test]
    fn trim_ext_works() {
        assert_eq!(
            String::from("  foo bar\n\t").trim(),
            String::from("foo bar")
        );
        assert_eq!(
            Cow::<str>::Owned(String::from("  foo bar\n\t")).trim(),
            Cow::<str>::Owned(String::from("foo bar"))
        );
        assert_eq!(
            CompactString::from("  foo bar\n\t").trim(),
            String::from("foo bar")
        );
    }

    #[test]
    fn facet_weight_deserialize_works() {
        let facet_weight =
            from_value::<FacetWeight>(Value::String("/Bund/UBA^2".to_owned())).unwrap();

        assert_eq!(facet_weight.facet, Facet::from_text("/Bund/UBA").unwrap());
        assert_eq!(facet_weight.weight, 2.);
    }

    #[test]
    fn facet_weight_serialize_works() {
        let facet_weight = FacetWeight {
            facet: Facet::from_text("/Bund/UBA").unwrap(),
            weight: 2.,
        };

        assert_eq!(
            to_value(facet_weight).unwrap(),
            Value::String("/Bund/UBA^2".to_owned())
        );
    }
}
