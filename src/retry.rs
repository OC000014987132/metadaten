use std::any::Any;
use std::env::var;
use std::future::Future;
use std::sync::atomic::{AtomicUsize, Ordering};

use anyhow::{Error, Result};
use once_cell::sync::Lazy;
use reqwest::{Error as HttpError, StatusCode};
use tokio::time::{sleep, Duration};

pub async fn retry_request<A, F, T, E>(fuse: Option<&AtomicUsize>, action: A) -> Result<T>
where
    A: FnMut() -> F,
    F: Future<Output = Result<T, E>>,
    E: Into<Error> + 'static,
{
    retry_request_if(fuse, action, |err| {
        let err = err as &dyn Any;

        let http_err = err.downcast_ref::<HttpError>().or_else(|| {
            err.downcast_ref::<Error>()
                .and_then(|err| err.downcast_ref::<HttpError>())
        });

        if let Some(http_err) = http_err {
            if matches!(
                http_err.status(),
                Some(StatusCode::NOT_FOUND | StatusCode::FORBIDDEN)
            ) {
                return false;
            }

            if http_err.is_redirect() {
                return false;
            }
        }

        true
    })
    .await
}

pub async fn retry_request_if<A, F, T, E, R>(
    fuse: Option<&AtomicUsize>,
    mut action: A,
    should_retry: R,
) -> Result<T>
where
    A: FnMut() -> F,
    F: Future<Output = Result<T, E>>,
    E: Into<Error>,
    R: Fn(&E) -> bool,
{
    let mut attempts = 0;
    let mut duration = Duration::from_secs(10);

    loop {
        match action().await {
            Ok(val) => {
                if let Some(fuse) = fuse {
                    let _ = fuse.fetch_update(Ordering::Relaxed, Ordering::Relaxed, |fuse| {
                        fuse.checked_sub(1)
                    });
                }

                return Ok(val);
            }
            Err(err) => {
                if let Some(fuse) = fuse {
                    fuse.fetch_add(1, Ordering::Relaxed);
                }

                if !(should_retry(&err) && attempts < *ATTEMPTS) {
                    return Err(err.into());
                }

                tracing::warn!("Request failed but will be retried: {:#}", err.into());

                sleep(duration).await;

                attempts += 1;
                duration *= 10;
            }
        }
    }
}

static ATTEMPTS: Lazy<usize> = Lazy::new(|| {
    var("RETRY_ATTEMPTS")
        .map(|val| val.parse().expect("Failed to parse RETRY_ATTEMPTS"))
        .unwrap_or(3)
});

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::anyhow;
    use tokio::time::{pause, Instant};

    #[tokio::test]
    async fn retry_request_fowards_success() {
        pause();
        let start = Instant::now();

        retry_request::<_, _, _, Error>(None, || async { Ok(Vec::<u8>::new()) })
            .await
            .unwrap();

        assert_eq!(start.elapsed().as_secs(), 0);
    }

    #[tokio::test]
    async fn retry_request_fowards_failure() {
        pause();
        let start = Instant::now();

        retry_request::<_, _, Vec<u8>, _>(None, || async { Err(anyhow!("failure")) })
            .await
            .unwrap_err();

        assert_eq!(start.elapsed().as_secs(), 10 + 100 + 1000);
    }

    #[tokio::test]
    async fn retry_request_retries_three_times() {
        pause();
        let start = Instant::now();

        let mut count = 0;

        retry_request(None, || {
            count += 1;

            async move {
                if count > 3 {
                    Ok(Vec::<u8>::new())
                } else {
                    Err(anyhow!("failure"))
                }
            }
        })
        .await
        .unwrap();

        assert_eq!(start.elapsed().as_secs(), 10 + 100 + 1000);
    }

    #[tokio::test]
    async fn retry_request_immediately_fowards_fatal_errors() {
        pause();
        let start = Instant::now();

        retry_request_if::<_, _, Vec<u8>, _, _>(
            None,
            || async { Err(anyhow!("fatal error")) },
            |_err| false,
        )
        .await
        .unwrap_err();

        assert_eq!(start.elapsed().as_secs(), 0);
    }

    #[tokio::test]
    async fn retry_request_decrements_fuse_on_success() {
        pause();
        let mut fuse = AtomicUsize::new(1);

        retry_request::<_, _, _, Error>(Some(&fuse), || async { Ok(Vec::<u8>::new()) })
            .await
            .unwrap();

        assert_eq!(*fuse.get_mut(), 0);

        retry_request::<_, _, _, Error>(Some(&fuse), || async { Ok(Vec::<u8>::new()) })
            .await
            .unwrap();

        assert_eq!(*fuse.get_mut(), 0);
    }

    #[tokio::test]
    async fn retry_request_increments_fuse_on_failure() {
        pause();
        let mut fuse = AtomicUsize::new(0);

        retry_request::<_, _, Vec<u8>, _>(Some(&fuse), || async { Err(anyhow!("failure")) })
            .await
            .unwrap_err();

        assert_eq!(*fuse.get_mut(), 3 + 1);

        retry_request::<_, _, Vec<u8>, _>(Some(&fuse), || async { Err(anyhow!("failure")) })
            .await
            .unwrap_err();

        assert_eq!(*fuse.get_mut(), 2 * (3 + 1));
    }
}
