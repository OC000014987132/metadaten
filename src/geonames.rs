use std::ops::Bound;

use anyhow::{anyhow, Result};
use geo::{algorithm::Contains, Coord, MultiPolygon, Rect};
use once_cell::sync::Lazy;
use regex::Regex;
use tantivy::{
    collector::{Collector, SegmentCollector, TopDocs},
    fastfield::Column,
    query::{BooleanQuery, Query, RangeQuery, TermQuery},
    schema::{Field, IndexRecordOption, OwnedValue, TantivyDocument},
    tokenizer::{LowerCaser, RawTokenizer, TextAnalyzer},
    Index, IndexReader, Result as TantivyResult, Score, Searcher, SegmentOrdinal, SegmentReader,
    Term,
};

use crate::{data_path_from_env, index::collector::FirstDoc};

pub struct PlaceName {
    pub name: String,
    pub coord: Option<Coord>,
}

pub static GEO_NAMES: Lazy<GeoNames> = Lazy::new(GeoNames::open);

pub struct GeoNames(Option<GeoNamesInner>);

impl GeoNames {
    fn open() -> Self {
        match GeoNamesInner::open() {
            Ok(val) => Self(Some(val)),
            Err(err) => {
                tracing::error!("Failed to open GeoNames index: {:#}", err);

                Self(None)
            }
        }
    }

    pub fn r#match(
        &self,
        name: &str,
        bounding_box: Option<&Rect>,
        shape: Option<&MultiPolygon>,
    ) -> Option<u64> {
        let this = self.0.as_ref()?;

        match this.r#match(name, bounding_box, shape) {
            Ok(val) => val,
            Err(err) => {
                tracing::error!("Failed to match {} against GeoNames: {:#}", name, err);

                None
            }
        }
    }

    pub fn resolve(&self, id: u64) -> PlaceName {
        let placeholder = || PlaceName {
            name: format!("GeoNames/{id}"),
            coord: None,
        };

        let this = match self.0.as_ref() {
            Some(this) => this,
            None => return placeholder(),
        };

        match this.resolve(id) {
            Ok(val) => val,
            Err(err) => {
                tracing::error!("Failed to resolve {} in GeoNames: {:#}", id, err);

                placeholder()
            }
        }
    }
}

struct GeoNamesInner {
    reader: IndexReader,
    id: Field,
    name: Field,
    tok_name: Field,
}

impl GeoNamesInner {
    fn open() -> Result<Self> {
        let index = Index::open_in_dir(data_path_from_env().join("geonames"))?;

        index.tokenizers().register(
            "lowercase",
            TextAnalyzer::builder(RawTokenizer::default())
                .filter(LowerCaser)
                .build(),
        );

        let reader = index.reader()?;

        let schema = index.schema();
        let id = schema.get_field("id").unwrap();
        let name = schema.get_field("name").unwrap();
        let tok_name = schema.get_field("name").unwrap();

        Ok(Self {
            reader,
            id,
            name,
            tok_name,
        })
    }

    fn r#match(
        &self,
        name: &str,
        bounding_box: Option<&Rect>,
        shape: Option<&MultiPolygon>,
    ) -> Result<Option<u64>> {
        let searcher = self.reader.searcher();

        if let Some(id) = self.exact_match(&searcher, name, bounding_box, shape)? {
            return Ok(Some(id));
        }

        self.tokenized_match(&searcher, name, bounding_box, shape)
    }

    fn exact_match(
        &self,
        searcher: &Searcher,
        name: &str,
        bounding_box: Option<&Rect>,
        shape: Option<&MultiPolygon>,
    ) -> Result<Option<u64>> {
        let mut tokenizer = searcher.index().tokenizer_for_field(self.name).unwrap();
        let mut query = Vec::<Box<dyn Query>>::with_capacity(2);

        tokenizer.token_stream(name).process(&mut |token| {
            query.push(Box::new(TermQuery::new(
                Term::from_field_text(self.name, &token.text),
                IndexRecordOption::Basic,
            )));
        });

        let query = contains(Box::new(BooleanQuery::union(query)), bounding_box);

        let collector = ShapeFilter {
            collector: TopDocs::with_limit(1),
            shape: shape.cloned(),
        };

        let mut docs = searcher.search(&query, &collector)?.into_iter();

        if let Some((_score, doc)) = docs.next() {
            let id = searcher
                .segment_reader(doc.segment_ord)
                .fast_fields()
                .u64("id")?
                .first(doc.doc_id)
                .unwrap();

            Ok(Some(id))
        } else {
            Ok(None)
        }
    }

    fn tokenized_match(
        &self,
        searcher: &Searcher,
        name: &str,
        bounding_box: Option<&Rect>,
        shape: Option<&MultiPolygon>,
    ) -> Result<Option<u64>> {
        let mut tokenizer = searcher.index().tokenizer_for_field(self.tok_name).unwrap();
        let mut query = Vec::<Box<dyn Query>>::new();

        tokenizer.token_stream(name).process(&mut |token| {
            query.push(Box::new(TermQuery::new(
                Term::from_field_text(self.tok_name, &token.text),
                IndexRecordOption::Basic,
            )));
        });

        let query = contains(Box::new(BooleanQuery::union(query)), bounding_box);

        let collector = ShapeFilter {
            collector: TopDocs::with_limit(2),
            shape: shape.cloned(),
        };

        let mut docs = searcher.search(&query, &collector)?.into_iter();

        if let Some((_score, doc)) = docs.next() {
            if docs.next().is_some() {
                return Ok(None);
            }

            let id = searcher
                .segment_reader(doc.segment_ord)
                .fast_fields()
                .u64("id")?
                .first(doc.doc_id)
                .unwrap();

            Ok(Some(id))
        } else {
            Ok(None)
        }
    }

    fn resolve(&self, id: u64) -> Result<PlaceName> {
        let query = TermQuery::new(Term::from_field_u64(self.id, id), IndexRecordOption::Basic);

        let searcher = self.reader.searcher();

        let doc_addr = searcher
            .search(&query, &FirstDoc)?
            .ok_or_else(|| anyhow!("Place name {id} not found"))?;

        let doc = searcher.doc::<TantivyDocument>(doc_addr)?;

        let mut name = None;

        for field_value in doc {
            if field_value.field == self.name {
                name = match field_value.value {
                    OwnedValue::Str(name) => Some(name),
                    _ => unreachable!(),
                };
            }
        }

        let lat = searcher
            .segment_reader(doc_addr.segment_ord)
            .fast_fields()
            .f64("lat")?
            .first(doc_addr.doc_id)
            .unwrap();

        let lon = searcher
            .segment_reader(doc_addr.segment_ord)
            .fast_fields()
            .f64("lon")?
            .first(doc_addr.doc_id)
            .unwrap();

        Ok(PlaceName {
            name: name.unwrap(),
            coord: Some(Coord { x: lon, y: lat }),
        })
    }
}

pub fn extract_id(link: &str) -> Result<u64> {
    static LINK: Lazy<Regex> =
        Lazy::new(|| Regex::new(r"https?://www\.geonames\.org/([[:xdigit:]]+)").unwrap());

    let captures = LINK
        .captures(link)
        .ok_or_else(|| anyhow!("Failed to match link: {}", link))?;

    let id = captures[1].parse::<u64>()?;

    Ok(id)
}

fn contains(query: Box<dyn Query>, bounding_box: Option<&Rect>) -> Box<dyn Query> {
    match bounding_box {
        Some(bounding_box) => {
            let min = bounding_box.min();
            let max = bounding_box.max();

            Box::new(BooleanQuery::intersection(vec![
                query,
                Box::new(RangeQuery::new_f64_bounds(
                    "lon".to_owned(),
                    Bound::Included(min.x),
                    Bound::Included(max.x),
                )),
                Box::new(RangeQuery::new_f64_bounds(
                    "lat".to_owned(),
                    Bound::Included(min.y),
                    Bound::Included(max.y),
                )),
            ]))
        }
        None => query,
    }
}

struct ShapeFilter<C> {
    collector: C,
    shape: Option<MultiPolygon>,
}

impl<C> Collector for ShapeFilter<C>
where
    C: Collector,
{
    type Fruit = C::Fruit;

    type Child = ShapeSegementFilter<C::Child>;

    fn for_segment(
        &self,
        segment_ord: SegmentOrdinal,
        segment_reader: &SegmentReader,
    ) -> TantivyResult<Self::Child> {
        let collector = self.collector.for_segment(segment_ord, segment_reader)?;

        let fast_fields = segment_reader.fast_fields();
        let lat = fast_fields.f64("lat")?;
        let lon = fast_fields.f64("lon")?;

        Ok(ShapeSegementFilter {
            collector,
            shape: self.shape.clone(),
            lat,
            lon,
        })
    }

    fn requires_scoring(&self) -> bool {
        self.collector.requires_scoring()
    }

    fn merge_fruits(
        &self,
        segment_fruits: Vec<<C::Child as SegmentCollector>::Fruit>,
    ) -> TantivyResult<C::Fruit> {
        self.collector.merge_fruits(segment_fruits)
    }
}

struct ShapeSegementFilter<C> {
    collector: C,
    shape: Option<MultiPolygon>,
    lat: Column<f64>,
    lon: Column<f64>,
}

impl<C> SegmentCollector for ShapeSegementFilter<C>
where
    C: SegmentCollector,
{
    type Fruit = C::Fruit;

    fn collect(&mut self, doc: u32, score: Score) {
        let accept = match &self.shape {
            Some(shape) => {
                let lat = self.lat.first(doc).unwrap();
                let lon = self.lon.first(doc).unwrap();

                shape.contains(&Coord { x: lon, y: lat })
            }
            None => true,
        };

        if accept {
            self.collector.collect(doc, score);
        }
    }

    fn harvest(self) -> C::Fruit {
        self.collector.harvest()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_id_works() {
        assert_eq!(
            extract_id("https://www.geonames.org/123456").unwrap(),
            123456
        );

        assert!(extract_id("https://www.uba.de/123456")
            .unwrap_err()
            .to_string()
            .contains("Failed to match link"),);
    }
}
