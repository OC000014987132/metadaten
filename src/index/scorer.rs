use hashbrown::hash_map::{Entry, HashMap};
use tantivy::{
    query::{BitSetDocSet, Scorer},
    DocId, DocSet, Score, COLLECT_BLOCK_BUFFER_LEN,
};
use tantivy_common::BitSet;

pub struct Scores {
    bitset: BitSet,
    repr: Repr,
}

enum Repr {
    Sparse(HashMap<DocId, Score>),
    Dense(Vec<Score>),
}

impl Scores {
    pub fn new(max_doc: DocId) -> Self {
        Self {
            bitset: BitSet::with_max_value(max_doc),
            repr: Repr::Sparse(HashMap::new()),
        }
    }

    pub fn set(&mut self, doc: DocId, score: Score) {
        match &mut self.repr {
            Repr::Sparse(scores) => match scores.entry(doc) {
                Entry::Vacant(entry) => {
                    entry.insert(score);
                }
                Entry::Occupied(entry) => {
                    let entry = entry.into_mut();

                    *entry = entry.max(score);
                }
            },
            Repr::Dense(scores) => {
                let entry = &mut scores[doc as usize];

                *entry = entry.max(score);
            }
        };

        self.bitset.insert(doc);

        // When the fraction of matching documents starts to exceed the threshold,
        // switch from the sparse to the then more efficient dense representation.
        if self.bitset.len() == self.bitset.max_value() as usize / 8 {
            self.condense();
        }
    }

    #[cold]
    #[inline(never)]
    fn condense(&mut self) {
        match &self.repr {
            Repr::Sparse(sparse) => {
                let mut dense = vec![0.0; self.bitset.max_value() as usize];

                for (doc, score) in sparse {
                    dense[*doc as usize] = *score;
                }

                self.repr = Repr::Dense(dense);
            }
            Repr::Dense(_dense) => (),
        }
    }

    pub fn into_scorer(self) -> Box<dyn Scorer> {
        let docset = self.bitset.into();

        match self.repr {
            Repr::Sparse(scores) => Box::new(SparseScorer { docset, scores }),
            Repr::Dense(scores) => Box::new(DenseScorer { docset, scores }),
        }
    }
}

struct SparseScorer {
    docset: BitSetDocSet,
    scores: HashMap<DocId, Score>,
}

impl DocSet for SparseScorer {
    fn advance(&mut self) -> DocId {
        self.docset.advance()
    }

    fn seek(&mut self, target: DocId) -> DocId {
        self.docset.seek(target)
    }

    fn fill_buffer(&mut self, buffer: &mut [DocId; COLLECT_BLOCK_BUFFER_LEN]) -> usize {
        self.docset.fill_buffer(buffer)
    }

    fn doc(&self) -> DocId {
        self.docset.doc()
    }

    fn size_hint(&self) -> u32 {
        self.docset.size_hint()
    }
}

impl Scorer for SparseScorer {
    fn score(&mut self) -> Score {
        self.scores[&self.doc()]
    }
}

struct DenseScorer {
    docset: BitSetDocSet,
    scores: Vec<Score>,
}

impl DocSet for DenseScorer {
    fn advance(&mut self) -> DocId {
        self.docset.advance()
    }

    fn seek(&mut self, target: DocId) -> DocId {
        self.docset.seek(target)
    }

    fn fill_buffer(&mut self, buffer: &mut [DocId; COLLECT_BLOCK_BUFFER_LEN]) -> usize {
        self.docset.fill_buffer(buffer)
    }

    fn doc(&self) -> DocId {
        self.docset.doc()
    }

    fn size_hint(&self) -> u32 {
        self.docset.size_hint()
    }
}

impl Scorer for DenseScorer {
    fn score(&mut self) -> Score {
        self.scores[self.doc() as usize]
    }
}
