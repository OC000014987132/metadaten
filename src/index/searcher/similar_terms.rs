use compact_str::CompactString;
use levenshtein_automata::{Distance, LevenshteinAutomatonBuilder, DFA, SINK_STATE};
use tantivy_fst::{Automaton, IntoStreamer, Streamer};

use crate::{
    dataset::Tag,
    stats::Stats,
    umthes::{TagDefinition, TAGS as UMTHES_TAGS},
};

pub struct SimilarTerms {
    automaton_builder: LevenshteinAutomatonBuilder,
}

impl SimilarTerms {
    pub fn new() -> Self {
        Self {
            automaton_builder: LevenshteinAutomatonBuilder::new(1, false),
        }
    }

    pub fn search(
        &self,
        stats: &Stats,
        terms: &[CompactString],
    ) -> Vec<(CompactString, CompactString)> {
        let lowercase_labels = UMTHES_TAGS.lowercase_labels();

        let mut similar_terms = Vec::new();

        for term in terms {
            if lowercase_labels.get(term).is_some() {
                continue;
            }

            let automaton = self.automaton_builder.build_dfa(term);

            let mut stream = lowercase_labels
                .search(DfaAutomaton(automaton))
                .into_stream();

            let mut similar_term = None;

            while let Some((value, id)) = stream.next() {
                let max_datasets = similar_term
                    .as_ref()
                    .map(|(_, max_datasets)| *max_datasets)
                    .unwrap_or_default();

                let datasets = stats
                    .tags
                    .get(&Tag::Umthes(id))
                    .copied()
                    .unwrap_or_default();

                if max_datasets <= datasets {
                    let tag_def = UMTHES_TAGS.resolve(id);

                    if let Some(label) = find_label(&tag_def, value) {
                        similar_term = Some((label.into(), datasets));
                    }
                }
            }

            if let Some((label, _)) = similar_term {
                similar_terms.push((term.clone(), label));
            }
        }

        similar_terms
    }

    pub fn complete(&self, stats: &Stats, term: &str) -> Vec<(CompactString, u64)> {
        let lowercase_labels = UMTHES_TAGS.lowercase_labels();

        if lowercase_labels.get(term).is_some() {
            return Vec::new();
        }

        let mut similar_terms = Vec::new();

        let automaton = self.automaton_builder.build_dfa(term);

        let mut stream = lowercase_labels
            .search(DfaAutomaton(automaton))
            .into_stream();

        while let Some((value, id)) = stream.next() {
            let datasets = stats
                .tags
                .get(&Tag::Umthes(id))
                .copied()
                .unwrap_or_default();

            let tag_def = UMTHES_TAGS.resolve(id);

            if let Some(label) = find_label(&tag_def, value) {
                similar_terms.push((label.into(), datasets));
            }
        }

        similar_terms
    }
}

fn find_label<'a>(tag_def: &'a TagDefinition, value: &[u8]) -> Option<&'a str> {
    tag_def.labels().find(|label| {
        label.as_bytes().eq_ignore_ascii_case(value) || label.to_lowercase().into_bytes() == value
    })
}

struct DfaAutomaton(DFA);

impl Automaton for DfaAutomaton {
    type State = u32;

    fn start(&self) -> Self::State {
        self.0.initial_state()
    }

    fn is_match(&self, state: &Self::State) -> bool {
        match self.0.distance(*state) {
            Distance::Exact(0) | Distance::AtLeast(_) => false,
            Distance::Exact(_) => true,
        }
    }

    fn can_match(&self, state: &u32) -> bool {
        *state != SINK_STATE
    }

    fn accept(&self, state: &Self::State, byte: u8) -> Self::State {
        self.0.transition(*state, byte)
    }
}
