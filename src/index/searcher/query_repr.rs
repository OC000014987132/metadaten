use std::fmt;

use anyhow::{anyhow, Result};
use openssl::sha::Sha256;
use serde::{Deserialize, Serialize};
use tantivy_query_grammar::{
    parse_query, Delimiter, Occur as TantivyOccur, UserInputAst, UserInputLeaf, UserInputLiteral,
};
use utoipa::ToSchema;

pub trait QueryRepr {
    fn is_default(&self) -> bool;

    fn to_ast(&self) -> Result<UserInputAst>;

    fn hash(&self, ctx: &mut Sha256);
}

impl QueryRepr for &'_ str {
    fn is_default(&self) -> bool {
        *self == "*"
    }

    fn to_ast(&self) -> Result<UserInputAst> {
        parse_query(self).map_err(|_err| anyhow!("Failed to parse query"))
    }

    fn hash(&self, ctx: &mut Sha256) {
        ctx.update(&[0]);

        ctx.update(&self.len().to_ne_bytes());
        ctx.update(self.as_bytes());
    }
}

#[derive(Debug, Serialize, Deserialize, ToSchema)]
#[serde(transparent)]
pub struct TextQuery(pub String);

impl Default for TextQuery {
    fn default() -> Self {
        Self("*".to_owned())
    }
}

impl fmt::Display for TextQuery {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(fmt)
    }
}

impl QueryRepr for TextQuery {
    fn is_default(&self) -> bool {
        self.0.as_str().is_default()
    }

    fn to_ast(&self) -> Result<UserInputAst> {
        self.0.as_str().to_ast()
    }

    fn hash(&self, ctx: &mut Sha256) {
        self.0.as_str().hash(ctx);
    }
}

/// Structured representation of a query as a list of clauses
///
/// An empty list or a single empty clause will be interpreted as the all query.
#[derive(Default, Debug, PartialEq, Eq, Serialize, Deserialize, ToSchema)]
#[serde(transparent)]
pub struct StructQuery(Vec<Clause>);

impl fmt::Display for StructQuery {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut clauses = self.0.iter();

        match clauses.next() {
            Some(clause) => write!(fmt, "{clause}")?,
            None => return fmt.write_str("*"),
        }

        for clause in clauses {
            write!(fmt, " {clause}")?;
        }

        Ok(())
    }
}

impl QueryRepr for StructQuery {
    fn is_default(&self) -> bool {
        matches!(&self.0[..], [] | [Clause::All {}])
    }

    fn to_ast(&self) -> Result<UserInputAst> {
        match &self.0[..] {
            [] => Ok(UserInputAst::Leaf(Box::new(UserInputLeaf::All))),
            [clause @ Clause::Phrase { occur: None, .. }] => {
                let (occur, ast) = clause.to_ast();

                debug_assert!(occur.is_none());

                Ok(ast)
            }
            clauses => {
                let clauses = clauses.iter().map(|clause| clause.to_ast()).collect();

                Ok(UserInputAst::Clause(clauses))
            }
        }
    }

    fn hash(&self, ctx: &mut Sha256) {
        ctx.update(&[1]);

        ctx.update(&self.0.len().to_ne_bytes());

        for clause in &self.0 {
            clause.hash(ctx);
        }
    }
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, ToSchema)]
#[serde(untagged)]
pub enum Clause {
    Phrase {
        occur: Option<Occur>,
        field_name: Option<String>,
        phrase: String,
    },
    All {},
}

impl Clause {
    fn to_ast(&self) -> (Option<TantivyOccur>, UserInputAst) {
        match self {
            Clause::Phrase {
                occur,
                field_name,
                phrase,
            } => (
                occur.map(Into::into),
                UserInputAst::Leaf(Box::new(UserInputLeaf::Literal(UserInputLiteral {
                    field_name: field_name.clone(),
                    phrase: phrase.clone(),
                    delimiter: Delimiter::None,
                    slop: 0,
                    prefix: false,
                }))),
            ),
            Clause::All {} => (None, UserInputAst::Leaf(Box::new(UserInputLeaf::All))),
        }
    }

    fn hash(&self, ctx: &mut Sha256) {
        match self {
            Self::Phrase {
                occur: None,
                field_name: None,
                phrase,
            } => {
                ctx.update(&[0]);
                ctx.update(&phrase.len().to_ne_bytes());
                ctx.update(phrase.as_bytes())
            }
            Self::Phrase {
                occur: Some(occur),
                field_name: None,
                phrase,
            } => {
                ctx.update(&[1, *occur as u8]);
                ctx.update(&phrase.len().to_ne_bytes());
                ctx.update(phrase.as_bytes())
            }
            Self::Phrase {
                occur: None,
                field_name: Some(field_name),
                phrase,
            } => {
                ctx.update(&[2]);
                ctx.update(&field_name.len().to_ne_bytes());
                ctx.update(field_name.as_bytes());
                ctx.update(&phrase.len().to_ne_bytes());
                ctx.update(phrase.as_bytes())
            }
            Self::Phrase {
                occur: Some(occur),
                field_name: Some(field_name),
                phrase,
            } => {
                ctx.update(&[3, *occur as u8]);
                ctx.update(&field_name.len().to_ne_bytes());
                ctx.update(field_name.as_bytes());
                ctx.update(&phrase.len().to_ne_bytes());
                ctx.update(phrase.as_bytes())
            }
            Self::All {} => ctx.update(&[4]),
        };
    }
}

impl fmt::Display for Clause {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Phrase {
                occur,
                field_name,
                phrase,
            } => {
                if let Some(occur) = occur {
                    let occur = match occur {
                        Occur::Should => "",
                        Occur::Must => "+",
                        Occur::MustNot => "-",
                    };

                    fmt.write_str(occur)?;
                }

                if let Some(field_name) = field_name {
                    write!(fmt, "{field_name}:")?;
                }

                write!(fmt, r#""{phrase}""#)
            }
            Self::All {} => fmt.write_str("*"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize, ToSchema)]
pub enum Occur {
    Should,
    Must,
    MustNot,
}

impl From<Occur> for TantivyOccur {
    fn from(value: Occur) -> Self {
        match value {
            Occur::Should => Self::Should,
            Occur::Must => Self::Must,
            Occur::MustNot => Self::MustNot,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_json::{from_value, json};

    #[test]
    fn struct_query_empty() {
        let query = from_value::<StructQuery>(json!([])).unwrap();

        assert_eq!(
            query.to_ast().unwrap(),
            UserInputAst::Leaf(Box::new(UserInputLeaf::All))
        );
    }

    #[test]
    fn struct_query_single_clause() {
        let query = from_value::<StructQuery>(json!([
            { "occur": "Should", "phrase": "Wasser"}
        ]))
        .unwrap();

        assert_eq!(
            query.to_ast().unwrap(),
            UserInputAst::Clause(vec![(
                Some(TantivyOccur::Should),
                UserInputAst::Leaf(Box::new(UserInputLeaf::Literal(UserInputLiteral {
                    field_name: None,
                    phrase: "Wasser".to_owned(),
                    delimiter: Delimiter::None,
                    slop: 0,
                    prefix: false,
                })))
            )])
        );
    }

    #[test]
    fn struct_query_single_clause_with_field_name() {
        let query = from_value::<StructQuery>(json!([
            { "occur": "Must", "field_name": "title", "phrase": "Wasser"}
        ]))
        .unwrap();

        assert_eq!(
            query.to_ast().unwrap(),
            UserInputAst::Clause(vec![(
                Some(TantivyOccur::Must),
                UserInputAst::Leaf(Box::new(UserInputLeaf::Literal(UserInputLiteral {
                    field_name: Some("title".to_owned()),
                    phrase: "Wasser".to_owned(),
                    delimiter: Delimiter::None,
                    slop: 0,
                    prefix: false,
                })))
            )])
        );
    }

    #[test]
    fn struct_query_single_clause_without_occur() {
        let query = from_value::<StructQuery>(json!([
            { "phrase": "Wasser"}
        ]))
        .unwrap();

        assert_eq!(
            query.to_ast().unwrap(),
            UserInputAst::Leaf(Box::new(UserInputLeaf::Literal(UserInputLiteral {
                field_name: None,
                phrase: "Wasser".to_owned(),
                delimiter: Delimiter::None,
                slop: 0,
                prefix: false,
            })))
        );
    }

    #[test]
    fn struct_query_multile_clauses() {
        let query = from_value::<StructQuery>(json!([
            { "phrase": "Wasser"},
            { "occur": "MustNot", "phrase": "Fluß" }
        ]))
        .unwrap();

        assert_eq!(
            query.to_ast().unwrap(),
            UserInputAst::Clause(vec![
                (
                    None,
                    UserInputAst::Leaf(Box::new(UserInputLeaf::Literal(UserInputLiteral {
                        field_name: None,
                        phrase: "Wasser".to_owned(),
                        delimiter: Delimiter::None,
                        slop: 0,
                        prefix: false,
                    })))
                ),
                (
                    Some(TantivyOccur::MustNot),
                    UserInputAst::Leaf(Box::new(UserInputLeaf::Literal(UserInputLiteral {
                        field_name: None,
                        phrase: "Fluß".to_owned(),
                        delimiter: Delimiter::None,
                        slop: 0,
                        prefix: false,
                    })))
                )
            ])
        );
    }

    #[test]
    fn struct_query_all_and_must_not_clauses() {
        let query = from_value::<StructQuery>(json!([
            {},
            { "occur": "MustNot", "phrase": "Meer" }
        ]))
        .unwrap();

        assert_eq!(
            query.to_ast().unwrap(),
            UserInputAst::Clause(vec![
                (None, UserInputAst::Leaf(Box::new(UserInputLeaf::All))),
                (
                    Some(TantivyOccur::MustNot),
                    UserInputAst::Leaf(Box::new(UserInputLeaf::Literal(UserInputLiteral {
                        field_name: None,
                        phrase: "Meer".to_owned(),
                        delimiter: Delimiter::None,
                        slop: 0,
                        prefix: false,
                    })))
                )
            ])
        );
    }

    #[test]
    fn struct_query_is_default() {
        let query = from_value::<StructQuery>(json!([])).unwrap();
        assert!(query.is_default());

        let query = from_value::<StructQuery>(json!([{}])).unwrap();
        assert!(query.is_default());
    }

    #[test]
    fn struct_query_display() {
        assert_eq!(StructQuery::default().to_string(), "*");

        let query = from_value::<StructQuery>(json!([
            { "occur": "Must", "field_name": "title", "phrase": "Wasser"},
            { "occur": "MustNot", "phrase": "Fluß" },
            { "phrase": "Meer" }
        ]))
        .unwrap();

        assert_eq!(query.to_string(), r#"+title:"Wasser" -"Fluß" "Meer""#);
    }
}
