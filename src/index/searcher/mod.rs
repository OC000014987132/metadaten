mod query_cache;
mod query_repr;
mod similar_terms;

use std::path::Path;
use std::str::from_utf8;
use std::sync::Arc;

use anyhow::{anyhow, Result};
use async_stream::try_stream;
use cap_std::{ambient_authority, fs::Dir};
use compact_str::CompactString;
use fastrand::Rng;
use futures_util::stream::Stream;
use geo::Rect;
use hashbrown::HashMap;
use regex::escape;
use tantivy::{
    collector::{
        Count, FacetCollector, FacetCounts, ScoreSegmentTweaker as TantivyScoreSegementTweaker,
        ScoreTweaker as TantivyScoreTweaker, TopDocs,
    },
    postings::TermInfo,
    query::{BooleanQuery, QueryParser, TermQuery},
    schema::{Facet, Field, IndexRecordOption, OwnedValue as Value, TantivyDocument as Document},
    DocAddress, DocId, DocSet, Index, IndexReader, InvertedIndexReader, Result as TantivyResult,
    Score, SegmentReader, Term, TERMINATED,
};
use tantivy_columnar::{ColumnValues, StrColumn};
use tantivy_fst::Regex;
use tantivy_query_grammar::{UserInputAst, UserInputLeaf};

use crate::{
    dataset::{Dataset, ScoredDataset, TimeRange, UniquelyIdentifiedDataset},
    index::{
        bounding_box::BoundingBoxes,
        collector::{AllDocs, FirstDoc},
        index_reader, register_tokenizers,
        spatial_cluster::{SpatialCluster, SpatialClustersCollector},
        time_range::TimeRanges,
        Fields,
    },
    stats::Stats,
    FacetWeight,
};

use query_cache::{complete_key, search_key, QueryCache};
use similar_terms::SimilarTerms;

pub use query_repr::{Clause, Occur, QueryRepr, StructQuery, TextQuery};

fn default_fields(fields: &Fields) -> [Field; 7] {
    [
        fields.title,
        fields.description,
        fields.measurements,
        fields.tags,
        fields.region,
        fields.origin_text,
        fields.type_text,
    ]
}

pub struct Searcher {
    index: Index,
    reader: IndexReader,
    parser: QueryParser,
    fields: Arc<Fields>,
    bounding_boxes: Arc<BoundingBoxes>,
    time_ranges: Arc<TimeRanges>,
    search_cache: QueryCache<SearchResults>,
    complete_cache: QueryCache<CompleteResults>,
    similar_terms: SimilarTerms,
}

impl Searcher {
    pub fn open(data_path: &Path) -> Result<Self> {
        let index = Index::open_in_dir(data_path.join("index"))?;
        register_tokenizers(data_path, &index)?;

        let fields = Arc::new(Fields::new(&index.schema()));

        let mut parser = QueryParser::for_index(&index, default_fields(&fields).to_vec());

        parser.set_field_boost(fields.title, 2.0);
        parser.set_field_boost(fields.tags, 3.0);
        parser.set_field_boost(fields.region, 3.0);

        parser.set_field_fuzzy(fields.tags, false, 1, true);
        parser.set_field_fuzzy(fields.region, false, 1, true);

        let dir = Arc::new(Dir::open_ambient_dir(data_path, ambient_authority())?);
        let bounding_boxes = Arc::new(BoundingBoxes::new(dir.clone(), "index"));
        let time_ranges = Arc::new(TimeRanges::new(dir, "index"));

        let reader = index_reader(&index, &bounding_boxes, &time_ranges)?;

        Ok(Self {
            index,
            reader,
            parser,
            fields,
            bounding_boxes,
            time_ranges,
            search_cache: QueryCache::new("SEARCH_CACHE_SIZE"),
            complete_cache: QueryCache::new("COMPLETE_CACHE_SIZE"),
            similar_terms: SimilarTerms::new(),
        })
    }

    pub fn register_tokenizers(&self, data_path: &Path) -> Result<()> {
        register_tokenizers(data_path, &self.index)
    }

    pub fn search_cache_entries(&self) -> u64 {
        self.search_cache.entries()
    }

    pub fn complete_cache_entries(&self) -> u64 {
        self.complete_cache.entries()
    }

    #[allow(clippy::too_many_arguments)]
    pub fn search(
        &self,
        stats: &Stats,
        query: &dyn QueryRepr,
        types_root: &Facet,
        topics_root: &Facet,
        origins_root: &Facet,
        licenses_root: &Facet,
        languages_root: &Facet,
        resource_types_root: &Facet,
        bounding_box: Option<Rect>,
        bounding_box_contains: bool,
        bounding_box_spatial_clusters: bool,
        time_range: Option<TimeRange>,
        origin_weights: &[FacetWeight],
        limit: usize,
        offset: usize,
    ) -> Result<SearchResults> {
        let searcher = self.reader.searcher();

        let key = if offset == 0 {
            let key = search_key(
                query,
                types_root,
                topics_root,
                origins_root,
                licenses_root,
                languages_root,
                resource_types_root,
                &bounding_box,
                bounding_box_contains,
                bounding_box_spatial_clusters,
                &time_range,
                origin_weights,
                searcher.generation().generation_id(),
            );

            if let Some(mut results) = self.search_cache.get(&key) {
                if results.results.len() >= limit {
                    results.results.truncate(limit);
                    return Ok(results);
                }
            }

            Some(key)
        } else {
            None
        };

        let ast = query.to_ast()?;

        let mut terms = Vec::new();
        collect_terms(&mut terms, &ast);

        let query = self.parser.build_query_from_user_input_ast(ast)?;

        let mut queries = Vec::with_capacity(8);
        queries.push(query);

        let mut push_facet_query = |field, root: &Facet| {
            // No need to filter for the root of a facet as all datasets are attached to all facets.
            if root.is_root() {
                return;
            }

            let query = Box::new(TermQuery::new(
                Term::from_facet(field, root),
                IndexRecordOption::Basic,
            ));

            queries.push(query);
        };

        push_facet_query(self.fields.r#type, types_root);
        push_facet_query(self.fields.topic, topics_root);
        push_facet_query(self.fields.origin, origins_root);
        push_facet_query(self.fields.license, licenses_root);
        push_facet_query(self.fields.language, languages_root);
        push_facet_query(self.fields.resource_type, resource_types_root);

        if let Some(bounding_box) = bounding_box {
            let bounding_box_query = self
                .bounding_boxes
                .query(bounding_box, bounding_box_contains)?;

            queries.push(bounding_box_query);
        }

        if let Some(time_range) = time_range {
            let time_range_query = self.time_ranges.query(time_range);

            queries.push(time_range_query);
        }

        let query = if queries.len() > 1 {
            Box::new(BooleanQuery::intersection(queries))
        } else {
            queries.into_iter().next().unwrap()
        };

        let mut types = FacetCollector::for_field("type");
        types.add_facet(types_root.clone());

        let mut topics = FacetCollector::for_field("topic");
        topics.add_facet(topics_root.clone());

        let mut origins = FacetCollector::for_field("origin");
        origins.add_facet(origins_root.clone());

        let mut licenses = FacetCollector::for_field("license");
        licenses.add_facet(licenses_root.clone());

        let mut languages = FacetCollector::for_field("language");
        languages.add_facet(languages_root.clone());

        let mut resource_types = FacetCollector::for_field("resource_type");
        resource_types.add_facet(resource_types_root.clone());

        let offset = offset.min(searcher.num_docs() as usize);

        let spatial_clusters =
            bounding_box
                .filter(|_| bounding_box_spatial_clusters)
                .map(|bounding_box| {
                    SpatialClustersCollector::new(
                        "bounding_box_min_x",
                        "bounding_box_min_y",
                        "bounding_box_max_x",
                        "bounding_box_max_y",
                        bounding_box,
                    )
                });

        let (
            count,
            docs,
            (types, topics, (origins, licenses, languages, resource_types)),
            spatial_clusters,
        ) = searcher.search(
            &query,
            &(
                Count,
                TopDocs::with_limit(limit)
                    .and_offset(offset)
                    .tweak_score(ScoreTweaker { origin_weights }),
                (
                    types,
                    topics,
                    (origins, licenses, languages, resource_types),
                ),
                spatial_clusters,
            ),
        )?;

        let results = docs
            .into_iter()
            .map(|(score, doc)| {
                let doc = searcher.doc(doc)?;

                let value = extract_fields(&self.fields, doc)?;

                Ok(ScoredDataset { value, score })
            })
            .collect::<Result<_>>()?;

        let collect_facets = |facets: FacetCounts, root: &Facet| {
            facets
                .get(root.clone())
                .map(|(facet, count)| (facet.clone(), count))
                .collect()
        };

        let types = collect_facets(types, types_root);
        let topics = collect_facets(topics, topics_root);
        let origins = collect_facets(origins, origins_root);
        let licenses = collect_facets(licenses, licenses_root);
        let languages = collect_facets(languages, languages_root);
        let resource_types = collect_facets(resource_types, resource_types_root);

        let similar_terms = self.similar_terms.search(stats, &terms);

        let results = SearchResults {
            count,
            results,
            types,
            topics,
            origins,
            licenses,
            languages,
            resource_types,
            spatial_clusters: spatial_clusters.unwrap_or_default(),
            terms,
            similar_terms,
        };

        if let Some(key) = key {
            self.search_cache.insert(key, &results);
        }

        Ok(results)
    }

    pub fn explain(&self, query: &dyn QueryRepr, source: &str, id: &str) -> Result<String> {
        let ast = query.to_ast()?;

        let searcher = self.reader.searcher();

        let query = self.parser.build_query_from_user_input_ast(ast)?;

        let doc = {
            let source_query = Box::new(TermQuery::new(
                Term::from_field_text(self.fields.source, source),
                IndexRecordOption::Basic,
            ));

            let id_query = Box::new(TermQuery::new(
                Term::from_field_text(self.fields.id, id),
                IndexRecordOption::Basic,
            ));

            let query = BooleanQuery::intersection(vec![query.box_clone(), source_query, id_query]);

            searcher
                .search(&query, &FirstDoc)?
                .ok_or_else(|| anyhow!("Dataset does not exist or does not match the query"))?
        };

        let mut explanation = format!("{:#?}", query.explain(&searcher, doc)?);
        let mut query = format!("{:#?}", query);

        for (field, entry) in searcher.schema().fields() {
            let from = format!("field={},", field.field_id());
            let to = format!("field=\"{}\",", entry.name());

            explanation = explanation.replace(&from, &to);
            query = query.replace(&from, &to);

            let from = format!("Field({})", field.field_id());
            let to = format!("Field(\"{}\")", entry.name());

            query = query.replace(&from, &to);

            let from = format!("    {},\n", field.field_id());
            let to = format!("    \"{}\",\n", entry.name());

            query = query.replace(&from, &to);
        }

        Ok(format!("query: {query},\nexplanation: {explanation},"))
    }

    pub fn search_all(
        &self,
        query: &dyn QueryRepr,
        bounding_box: Option<Rect>,
        bounding_box_contains: bool,
        time_range: Option<TimeRange>,
        sampling_fraction: Option<f32>,
    ) -> Result<impl Stream<Item = Result<UniquelyIdentifiedDataset>> + '_> {
        let ast = query.to_ast()?;

        let query = self.parser.build_query_from_user_input_ast(ast)?;

        let mut queries = Vec::with_capacity(3);
        queries.push(query);

        if let Some(bounding_box) = bounding_box {
            let bounding_box_query = self
                .bounding_boxes
                .query(bounding_box, bounding_box_contains)?;

            queries.push(bounding_box_query);
        }

        if let Some(time_range) = time_range {
            let time_range_query = self.time_ranges.query(time_range);

            queries.push(time_range_query);
        }

        let query = if queries.len() > 1 {
            Box::new(BooleanQuery::intersection(queries))
        } else {
            queries.into_iter().next().unwrap()
        };

        let searcher = self.reader.searcher();

        let mut sampler = Sampler::new(sampling_fraction);

        let docs = searcher.search(&query, &AllDocs)?;

        Ok(try_stream! {
            for mut segment in docs {
                loop {
                    let doc_id = segment.docs.advance();
                    if doc_id == TERMINATED {
                        break;
                    }
                    if sampler.skip() {
                        continue;
                    }

                    let doc = searcher.doc(DocAddress {
                        segment_ord: segment.segment_ord,
                        doc_id,
                    })?;

                    let value = extract_fields(&self.fields, doc)?;

                    yield value;
                }
            }
        })
    }

    pub fn complete(&self, stats: &Stats, query: &mut dyn QueryRepr) -> Result<CompleteResults> {
        let searcher = self.reader.searcher();

        let key = complete_key(query, searcher.generation().generation_id());

        if let Some(results) = self.complete_cache.get(&key) {
            return Ok(results);
        }

        let term_field = query
            .to_ast()
            .ok()
            .and_then(last_term)
            // The number of matching terms can become too large otherwise.
            .filter(|(term, _)| term.len() >= 3);

        let (term, field) = match term_field {
            Some(term_field) => term_field,
            None => return Ok(Default::default()),
        };

        let hold_given_field;
        let hold_default_fields;
        let fields = if let Some(field) = field {
            hold_given_field = [searcher.schema().get_field(&field)?];
            &hold_given_field[..]
        } else {
            hold_default_fields = default_fields(&self.fields);
            &hold_default_fields[..]
        };

        let similar_terms = self.similar_terms.complete(stats, &term);

        let facet_regex = prepare_facet_regex(&term);

        let term = term.into_bytes();
        let term_len = term.len();
        let mut lower = term.clone();
        lower.push(u8::MIN);
        let mut upper = term;
        upper.push(u8::MAX);

        let mut completions = HashMap::new();

        let mut facets = [
            self.fields.r#type,
            self.fields.topic,
            self.fields.origin,
            self.fields.license,
            self.fields.language,
            self.fields.resource_type,
        ]
        .map(|field| (field, HashMap::new()));

        for reader in searcher.segment_readers() {
            for field in fields {
                let reader = reader.inverted_index(*field)?;

                let mut stream = reader.terms().range().ge(&lower).le(&upper).into_stream()?;

                while let Some((completion, info)) = stream.next() {
                    let completion = from_utf8(&completion[term_len..]).unwrap();

                    let count = completions.entry_ref(completion).or_default();
                    read_count(&reader, info, count)?;
                }
            }

            if let Some(facet_regex) = &facet_regex {
                for (field, values) in &mut facets {
                    let reader = reader.inverted_index(*field)?;

                    let mut stream = reader.terms().search(facet_regex).into_stream()?;

                    while let Some((value, info)) = stream.next() {
                        let count = values.entry_ref(value).or_default();
                        read_count(&reader, info, count)?;
                    }
                }
            }
        }

        let completions = completions.into_iter().collect();

        let [types, topics, origins, licenses, languages, resource_types] =
            facets.map(|(_, values)| {
                values
                    .into_iter()
                    .map(|(value, count)| (Facet::from_encoded(value).unwrap(), count))
                    .collect()
            });

        let results = CompleteResults {
            completions,
            similar_terms,
            types,
            topics,
            origins,
            licenses,
            languages,
            resource_types,
        };

        self.complete_cache.insert(key, &results);

        Ok(results)
    }

    pub fn read(&self, source: &str, id: &str) -> Result<Option<UniquelyIdentifiedDataset>> {
        let source_query = Box::new(TermQuery::new(
            Term::from_field_text(self.fields.source, source),
            IndexRecordOption::Basic,
        ));

        let id_query = Box::new(TermQuery::new(
            Term::from_field_text(self.fields.id, id),
            IndexRecordOption::Basic,
        ));

        let query = BooleanQuery::intersection(vec![source_query, id_query]);

        let searcher = self.reader.searcher();

        let Some(doc) = searcher.search(&query, &FirstDoc)? else {
            return Ok(None);
        };

        let doc = searcher.doc(doc)?;

        let dataset = extract_fields(&self.fields, doc)?;

        Ok(Some(dataset))
    }

    pub fn read_all(
        &self,
        sampling_fraction: Option<f32>,
    ) -> impl Stream<Item = Result<UniquelyIdentifiedDataset>> + '_ {
        try_stream! {
            let searcher = self.reader.searcher();

            let mut sampler = Sampler::new(sampling_fraction);

            for segment in searcher.segment_readers() {
                let reader = segment.get_store_reader(1).unwrap();
                let alive = segment.alive_bitset();

                for doc in reader.iter(alive) {
                    let doc = doc?;

                if sampler.skip() {
                    continue;
                }

                    let dataset = extract_fields(&self.fields, doc)?;

                    yield dataset;
                }
            }
        }
    }

    pub fn read_random(&self) -> Result<UniquelyIdentifiedDataset> {
        let searcher = self.reader.searcher();

        let query = TermQuery::new(
            Term::from_field_bool(self.fields.eligible_for_random, true),
            IndexRecordOption::Basic,
        );

        let docs = searcher.search(
            &query,
            &TopDocs::with_limit(1).tweak_score(|reader: &SegmentReader| {
                let inherent_score = reader
                    .fast_fields()
                    .f64("inherent_score")
                    .unwrap()
                    .first_or_default_col(1.0);

                let mut rng = Rng::new();

                move |doc, score| {
                    let inherent_score = inherent_score.get_val(doc) as f32;

                    let random_boost = 1.0 + rng.f32();

                    score * inherent_score * random_boost
                }
            }),
        )?;

        let (_score, doc) = docs
            .into_iter()
            .next()
            .ok_or_else(|| anyhow!("Index is empty"))?;

        let doc = searcher.doc(doc)?;

        let dataset = extract_fields(&self.fields, doc)?;

        Ok(dataset)
    }
}

#[derive(Default, Clone)]
pub struct SearchResults {
    pub count: usize,
    pub results: Vec<ScoredDataset>,
    pub types: Vec<(Facet, u64)>,
    pub topics: Vec<(Facet, u64)>,
    pub origins: Vec<(Facet, u64)>,
    pub licenses: Vec<(Facet, u64)>,
    pub languages: Vec<(Facet, u64)>,
    pub resource_types: Vec<(Facet, u64)>,
    pub spatial_clusters: Vec<SpatialCluster>,
    pub terms: Vec<CompactString>,
    pub similar_terms: Vec<(CompactString, CompactString)>,
}

#[derive(Default, Clone)]
pub struct CompleteResults {
    pub completions: Vec<(CompactString, usize)>,
    pub similar_terms: Vec<(CompactString, u64)>,
    pub types: Vec<(Facet, usize)>,
    pub topics: Vec<(Facet, usize)>,
    pub origins: Vec<(Facet, usize)>,
    pub licenses: Vec<(Facet, usize)>,
    pub languages: Vec<(Facet, usize)>,
    pub resource_types: Vec<(Facet, usize)>,
}

fn collect_terms(terms: &mut Vec<CompactString>, ast: &UserInputAst) {
    match ast {
        UserInputAst::Clause(clauses) => {
            for (_occur, clause) in clauses {
                collect_terms(terms, clause);
            }
        }
        UserInputAst::Leaf(leaf) => {
            if let UserInputLeaf::Literal(lit) = &**leaf {
                if !lit.phrase.is_empty() {
                    terms.push(lit.phrase.to_lowercase().into());
                }
            }
        }
        UserInputAst::Boost(ast, _boost) => collect_terms(terms, ast),
    }
}

fn last_term(ast: UserInputAst) -> Option<(String, Option<String>)> {
    match ast {
        UserInputAst::Clause(mut clauses) => {
            clauses.pop().and_then(|(_occur, clause)| last_term(clause))
        }
        UserInputAst::Leaf(leaf) => {
            if let UserInputLeaf::Literal(lit) = *leaf {
                Some((lit.phrase.to_lowercase(), lit.field_name))
            } else {
                None
            }
        }
        UserInputAst::Boost(ast, _boost) => last_term(*ast),
    }
}

struct ScoreTweaker<'a> {
    origin_weights: &'a [FacetWeight],
}

impl ScoreTweaker<'_> {
    fn resolve_origin_weights(&self, origins: &StrColumn) -> TantivyResult<Vec<Score>> {
        let origins = origins.dictionary();

        let mut origin_weights = vec![1.0; origins.num_terms()];

        let mut limit = Vec::new();

        for origin_weight in self.origin_weights {
            let origin = origin_weight.facet.encoded_str().as_bytes();

            limit.clear();
            limit.reserve(origin.len() + 1);
            limit.extend(origin);

            let stream = origins.range().ge(&limit);

            limit.push(u8::MAX);

            let mut stream = stream.le(&limit).into_stream()?;

            while let Some((origin, ())) = stream.next() {
                let origin = origins.term_ord(origin)?.unwrap();

                origin_weights[origin as usize] = origin_weight.weight;
            }
        }

        Ok(origin_weights)
    }
}

impl TantivyScoreTweaker<Score> for ScoreTweaker<'_> {
    type Child = ScoreSegmentTweaker;

    fn segment_tweaker(&self, reader: &SegmentReader) -> TantivyResult<Self::Child> {
        let fast_fields = reader.fast_fields();
        let inherent_score = fast_fields.f64("inherent_score")?.first_or_default_col(1.0);
        let origins = fast_fields.str("origin")?.unwrap();

        let origin_weights = self.resolve_origin_weights(&origins)?;

        Ok(ScoreSegmentTweaker {
            inherent_score,
            origins,
            origin_weights,
        })
    }
}

struct ScoreSegmentTweaker {
    inherent_score: Arc<dyn ColumnValues<f64>>,
    origins: StrColumn,
    origin_weights: Vec<Score>,
}

impl TantivyScoreSegementTweaker<Score> for ScoreSegmentTweaker {
    fn score(&mut self, doc: DocId, mut score: Score) -> f32 {
        score *= self.inherent_score.get_val(doc) as f32;

        for origin in self.origins.term_ords(doc) {
            score *= self.origin_weights[origin as usize];
        }

        score
    }
}

enum Sampler {
    Disabled,
    Enabled { rng: Rng, sampling_fraction: f32 },
}

impl Sampler {
    fn new(sampling_fraction: Option<f32>) -> Self {
        match sampling_fraction {
            None => Self::Disabled,
            Some(sampling_fraction) => Self::Enabled {
                rng: Rng::new(),
                sampling_fraction,
            },
        }
    }

    fn skip(&mut self) -> bool {
        match self {
            Self::Disabled => false,
            Self::Enabled {
                rng,
                sampling_fraction,
            } => rng.f32() >= *sampling_fraction,
        }
    }
}

pub(super) fn extract_fields(fields: &Fields, doc: Document) -> Result<UniquelyIdentifiedDataset> {
    let mut source = None;
    let mut id = None;
    let mut content = None;

    for field_value in doc {
        if field_value.field == fields.source {
            source = match field_value.value {
                Value::Str(source) => Some(source.into()),
                _ => unreachable!(),
            };
        } else if field_value.field == fields.id {
            id = match field_value.value {
                Value::Str(id) => Some(id),
                _ => unreachable!(),
            };
        } else if field_value.field == fields.content {
            content = match field_value.value {
                Value::Bytes(id) => Some(id),
                _ => unreachable!(),
            };
        }
    }

    let value = Dataset::parse(&content.unwrap())?;

    Ok(UniquelyIdentifiedDataset {
        source: source.unwrap(),
        id: id.unwrap(),
        value,
    })
}

fn read_count(reader: &InvertedIndexReader, info: &TermInfo, count: &mut usize) -> Result<()> {
    let mut postings = reader.read_block_postings_from_terminfo(info, IndexRecordOption::Basic)?;

    loop {
        let block_len = postings.block_len();
        if block_len == 0 {
            break;
        }
        *count += block_len;
        postings.advance();
    }

    Ok(())
}

fn prepare_facet_regex(term: &str) -> Option<Regex> {
    let mut pattern = escape(term).into_bytes();

    let skip = usize::from(pattern[0] == b'/');

    for byte in &mut pattern {
        if *byte == b'/' {
            *byte = 0;
        }
    }

    let mut pattern = String::from_utf8(pattern).unwrap();

    pattern.replace_range(0..skip, "(?i).*\0");
    pattern.push_str(".*");

    Regex::new(&pattern).ok()
}
