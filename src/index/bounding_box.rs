//! Secondary index supporting spatial queries
//!
//! This module uses the [`sif_rtree`] crate to build memory-mapped R* trees
//! of the bounding boxes associated with the datasets. This enables efficiently
//! querying all datasets whose bounding boxes overlap a given rectangle. Overlapping
//! datasets are then scored using the maximum of the area-based Jaccard index.
use std::fmt;
use std::mem::{size_of, size_of_val};
use std::ops::ControlFlow;
use std::slice::from_raw_parts;
use std::sync::Arc;

use anyhow::{ensure, Result};
use arc_swap::ArcSwap;
use cap_std::fs::Dir;
use geo::Rect;
use hashbrown::{HashMap, HashSet};
use itertools::{zip_eq, Itertools};
use memmap2::Mmap;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use sif_rtree::{Node, Object, RTree, DEF_NODE_LEN};
use tantivy::{
    error::TantivyError,
    query::{
        BitSetDocSet, ConstScorer, EmptyQuery, EmptyScorer, EnableScoring, Explanation, Query,
        Scorer, Weight,
    },
    DocId, DocSet, Opstamp, Result as TantivyResult, Score, Searcher, SearcherGeneration,
    SegmentId, SegmentReader, Warmer,
};
use tantivy_common::BitSet;

use crate::{index::scorer::Scores, minmax};

type SegmentKey = (SegmentId, Option<Opstamp>);

type SegmentTree = Option<RTree<Item, MappedNodes>>;

pub struct BoundingBoxes {
    trees: ArcSwap<HashMap<SegmentKey, SegmentTree>>,
    dir: Arc<Dir>,
    index_dir_name: &'static str,
}

impl BoundingBoxes {
    pub fn new(dir: Arc<Dir>, index_dir_name: &'static str) -> Self {
        Self {
            trees: Default::default(),
            dir,
            index_dir_name,
        }
    }

    pub fn query(&self, bounding_box: Rect, contains: bool) -> Result<Box<dyn Query>> {
        let envelope = as_aabb(bounding_box)?;

        if envelope.0[0] > envelope.1[0] || envelope.0[1] > envelope.1[1] {
            return Ok(Box::new(EmptyQuery));
        }

        Ok(Box::new(BoundingBoxQuery {
            trees: self.trees.load_full(),
            envelope,
            contains,
        }))
    }
}

impl Warmer for BoundingBoxes {
    fn warm(&self, searcher: &Searcher) -> TantivyResult<()> {
        let mut trees = self.trees.load_full();

        let new_trees = searcher
            .segment_readers()
            .par_iter()
            .filter_map(|segment| {
                let key = (segment.segment_id(), segment.delete_opstamp());
                if !trees.contains_key(&key) {
                    Some((key, segment))
                } else {
                    None
                }
            })
            .map(|(key, segment)| {
                let tree = load_tree(&self.dir, self.index_dir_name, segment)
                    .map_err(|err| TantivyError::InternalError(err.to_string()))?;

                Ok((key, tree))
            })
            .collect::<TantivyResult<Vec<_>>>()?;

        Arc::make_mut(&mut trees).extend(new_trees);

        self.trees.store(trees);

        Ok(())
    }

    fn garbage_collect(&self, live_generations: &[&SearcherGeneration]) {
        let live_keys = live_generations
            .iter()
            .flat_map(|gen| gen.segments())
            .map(|(&segment_id, &opstamp)| (segment_id, opstamp))
            .collect::<HashSet<_>>();

        let mut trees = self.trees.load_full();

        Arc::make_mut(&mut trees).retain(|key, _tree| {
            if !live_keys.contains(key) {
                let file_name = file_name(self.index_dir_name, key.0);

                if let Err(err) = self.dir.remove_file(file_name) {
                    tracing::warn!(
                        "Failed to delete spatial index for segment {}: {}",
                        key.0,
                        err
                    );
                }

                false
            } else {
                true
            }
        });

        self.trees.store(trees);
    }
}

#[derive(Clone)]
struct BoundingBoxQuery {
    trees: Arc<HashMap<SegmentKey, SegmentTree>>,
    envelope: ([f32; 2], [f32; 2]),
    contains: bool,
}

impl fmt::Debug for BoundingBoxQuery {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.debug_struct("BoundingBoxQuery")
            .field("envelope", &self.envelope)
            .finish_non_exhaustive()
    }
}

impl Query for BoundingBoxQuery {
    fn weight(&self, enable_scoring: EnableScoring<'_>) -> TantivyResult<Box<dyn Weight>> {
        if enable_scoring.is_scoring_enabled() {
            if self.contains {
                Ok(Box::new(BoundingBoxWeight::<true, true>(self.clone())))
            } else {
                Ok(Box::new(BoundingBoxWeight::<true, false>(self.clone())))
            }
        } else if self.contains {
            Ok(Box::new(BoundingBoxWeight::<false, true>(self.clone())))
        } else {
            Ok(Box::new(BoundingBoxWeight::<false, false>(self.clone())))
        }
    }
}

#[derive(Clone)]
struct BoundingBoxWeight<const SCORING: bool, const CONTAINS: bool>(BoundingBoxQuery);

impl<const SCORING: bool, const CONTAINS: bool> Weight for BoundingBoxWeight<SCORING, CONTAINS> {
    fn scorer(
        &self,
        segment_reader: &SegmentReader,
        boost: Score,
    ) -> TantivyResult<Box<dyn Scorer>> {
        let key = (segment_reader.segment_id(), segment_reader.delete_opstamp());
        let tree = &self.0.trees[&key];

        if let Some(tree) = tree {
            if SCORING {
                let mut scores = Scores::new(segment_reader.max_doc());

                if CONTAINS {
                    tree.look_up_aabb_contains(&self.0.envelope, |Item { doc_id, aabb }| {
                        scores.set(*doc_id, compute_overlap(&self.0.envelope, aabb) * boost);
                        ControlFlow::Continue(())
                    });
                } else {
                    tree.look_up_aabb_intersects(&self.0.envelope, |Item { doc_id, aabb }| {
                        scores.set(*doc_id, compute_overlap(&self.0.envelope, aabb) * boost);
                        ControlFlow::Continue(())
                    });
                }

                Ok(scores.into_scorer())
            } else {
                let mut docs = BitSet::with_max_value(segment_reader.max_doc());

                if CONTAINS {
                    tree.look_up_aabb_contains(&self.0.envelope, |Item { doc_id, .. }| {
                        docs.insert(*doc_id);
                        ControlFlow::Continue(())
                    });
                } else {
                    tree.look_up_aabb_intersects(&self.0.envelope, |Item { doc_id, .. }| {
                        docs.insert(*doc_id);
                        ControlFlow::Continue(())
                    });
                }

                Ok(Box::new(ConstScorer::new(BitSetDocSet::from(docs), boost)))
            }
        } else {
            Ok(Box::new(EmptyScorer))
        }
    }

    fn explain(&self, segment_reader: &SegmentReader, doc: DocId) -> TantivyResult<Explanation> {
        let mut scorer = self.scorer(segment_reader, 1.0)?;
        if scorer.seek(doc) != doc {
            return Err(TantivyError::InvalidArgument(format!(
                "Document #({doc}) does not match"
            )));
        }

        let mut explanation = Explanation::new("BoundingBoxQuery", 1.0);
        explanation.add_context(format!("BoundingBox={:?}", self.0.envelope));
        Ok(explanation)
    }
}

#[derive(Clone, Copy)]
struct Item {
    aabb: ([f32; 2], [f32; 2]),
    doc_id: DocId,
}

impl Object for Item {
    type Point = [f32; 2];

    fn aabb(&self) -> (Self::Point, Self::Point) {
        self.aabb
    }
}

fn as_aabb(bounding_box: Rect) -> Result<([f32; 2], [f32; 2])> {
    let min = bounding_box.min();
    let max = bounding_box.max();

    ensure!(
        !(min.x.is_nan() || min.y.is_nan() || max.x.is_nan() || max.y.is_nan()),
        "NaN coordinates in bounding box"
    );

    Ok(([min.x as f32, min.y as f32], [max.x as f32, max.y as f32]))
}

/// Computes the area-based Jaccard index between `envelope` and `aabb`
fn compute_overlap(envelope: &([f32; 2], [f32; 2]), aabb: &([f32; 2], [f32; 2])) -> Score {
    let [min_lower_x, max_lower_x] = minmax(envelope.0[0], aabb.0[0]);
    let [min_lower_y, max_lower_y] = minmax(envelope.0[1], aabb.0[1]);
    let [min_upper_x, max_upper_x] = minmax(envelope.1[0], aabb.1[0]);
    let [min_upper_y, max_upper_y] = minmax(envelope.1[1], aabb.1[1]);

    let intersection = (min_upper_x - max_lower_x) * (min_upper_y - max_lower_y);
    let r#union = (max_upper_x - min_lower_x) * (max_upper_y - min_lower_y);

    if intersection != 0.0 && r#union != 0.0 {
        intersection / r#union
    } else {
        // This function assumes being called with overlapping AABB,
        // so we do not need to figure if we should actually return zero instead.
        1.0
    }
}

fn load_tree(dir: &Dir, index_dir_name: &str, segment: &SegmentReader) -> Result<SegmentTree> {
    let file_name = file_name(index_dir_name, segment.segment_id());

    if let Some(tree) = map_tree(dir, &file_name)? {
        return Ok(tree);
    }

    tracing::info!(
        "Building spatial index for segment {} containing {} documents",
        segment.segment_id(),
        segment.num_docs()
    );

    let fast_fields = segment.fast_fields();
    let min_x = fast_fields.f64("bounding_box_min_x")?;
    let min_y = fast_fields.f64("bounding_box_min_y")?;
    let max_x = fast_fields.f64("bounding_box_max_x")?;
    let max_y = fast_fields.f64("bounding_box_max_y")?;

    let items = segment
        .doc_ids_alive()
        .map(|doc_id| {
            let min_x = min_x.values_for_doc(doc_id);
            let min_y = min_y.values_for_doc(doc_id);
            let max_x = max_x.values_for_doc(doc_id);
            let max_y = max_y.values_for_doc(doc_id);

            let items = zip_eq(zip_eq(min_x, min_y), zip_eq(max_x, max_y)).map(
                move |((min_x, min_y), (max_x, max_y))| {
                    let aabb = ([min_x as f32, min_y as f32], [max_x as f32, max_y as f32]);

                    Item { aabb, doc_id }
                },
            );

            Ok(items)
        })
        .flatten_ok()
        .collect::<Result<Vec<_>>>()?;

    let tree = if !items.is_empty() {
        Some(RTree::new(DEF_NODE_LEN, items))
    } else {
        None
    };

    write_tree(dir, &file_name, &tree)?;

    let tree = map_tree(dir, &file_name)?.unwrap();

    Ok(tree)
}

#[derive(Clone)]
struct MappedNodes(Arc<Mmap>);

impl AsRef<[Node<Item>]> for MappedNodes {
    #[allow(unsafe_code)]
    fn as_ref(&self) -> &[Node<Item>] {
        let ptr = self.0.as_ptr().cast();
        let len = self.0.len() / size_of::<Node<Item>>();
        unsafe { from_raw_parts(ptr, len) }
    }
}

#[allow(unsafe_code)]
fn map_tree(dir: &Dir, file_name: &str) -> Result<Option<SegmentTree>> {
    if let Ok(file) = dir.open(file_name) {
        let mmap = unsafe { Mmap::map(&file)? };

        if !mmap.is_empty() {
            let tree = RTree::new_unchecked(MappedNodes(Arc::new(mmap)));

            Ok(Some(Some(tree)))
        } else {
            Ok(Some(None))
        }
    } else {
        Ok(None)
    }
}

#[allow(unsafe_code)]
fn write_tree(dir: &Dir, file_name: &str, tree: &Option<RTree<Item>>) -> Result<()> {
    if let Some(tree) = tree {
        let nodes = tree.as_ref();
        let ptr = nodes.as_ptr().cast();
        let len = size_of_val(nodes);
        let bytes = unsafe { from_raw_parts(ptr, len) };

        dir.write(file_name, bytes)?;
    } else {
        dir.create(file_name)?;
    }

    Ok(())
}

fn file_name(index_dir_name: &str, segment_id: SegmentId) -> String {
    format!(
        "{}/{}.bounding_box",
        index_dir_name,
        segment_id.uuid_string()
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn compute_overlap_works() {
        assert_eq!(
            compute_overlap(&([0.0, 0.0], [1.0, 1.0]), &([0.0, 0.0], [1.0, 1.0])),
            1.0
        );

        assert_eq!(
            compute_overlap(&([0.0, 0.0], [1.0, 2.0]), &([0.0, 0.0], [2.0, 2.0])),
            0.5
        );
        assert_eq!(
            compute_overlap(&([0.0, 0.0], [2.0, 1.0]), &([0.0, 0.0], [2.0, 2.0])),
            0.5
        );
        assert_eq!(
            compute_overlap(&([0.0, 0.0], [1.0, 1.0]), &([0.0, 0.0], [2.0, 2.0])),
            0.25
        );
        assert_eq!(
            compute_overlap(&([-1.0, -1.0], [0.0, 0.0]), &([-2.0, -2.0], [0.0, 0.0])),
            0.25
        );

        assert_eq!(
            compute_overlap(&([1.0, 1.0], [1.0, 1.0]), &([1.0, 1.0], [1.0, 1.0])),
            1.0
        );
        assert_eq!(
            compute_overlap(&([0.0, 0.0], [1.0, 1.0]), &([1.0, 1.0], [1.0, 1.0])),
            1.0
        );
        assert_eq!(
            compute_overlap(&([1.0, 1.0], [1.0, 1.0]), &([0.0, 0.0], [1.0, 1.0])),
            1.0
        );
    }
}
