//! Secondary index supporting temporal queries
//!
//! This module uses the [`sif_itree`] crate to build memory-mapped interval trees
//! of the time range associated with the datasets. This enables efficiently querying
//! all datasets whose time ranges overlap a given interval. Overlapping datasets
//! are then scored using the maximum of the duration-based Jaccard index.
use std::fmt;
use std::mem::{size_of, size_of_val};
use std::ops::{ControlFlow, Range};
use std::slice::from_raw_parts;
use std::sync::Arc;

use anyhow::Result;
use arc_swap::ArcSwap;
use cap_std::fs::Dir;
use hashbrown::{HashMap, HashSet};
use itertools::{zip_eq, Itertools};
use memmap2::Mmap;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use sif_itree::{ITree, Node};
use tantivy::{
    error::TantivyError,
    query::{
        BitSetDocSet, ConstScorer, EmptyQuery, EnableScoring, Explanation, Query, Scorer, Weight,
    },
    DocId, DocSet, Opstamp, Result as TantivyResult, Score, Searcher, SearcherGeneration,
    SegmentId, SegmentReader, Warmer,
};
use tantivy_common::BitSet;

use crate::{dataset::TimeRange, index::scorer::Scores, minmax};

type SegmentKey = (SegmentId, Option<Opstamp>);

type SegmentTree = ITree<i32, DocId, MappedNodes>;

pub struct TimeRanges {
    trees: ArcSwap<HashMap<SegmentKey, SegmentTree>>,
    dir: Arc<Dir>,
    index_dir_name: &'static str,
}

impl TimeRanges {
    pub fn new(dir: Arc<Dir>, index_dir_name: &'static str) -> Self {
        Self {
            trees: Default::default(),
            dir,
            index_dir_name,
        }
    }

    pub fn query(&self, time_range: TimeRange) -> Box<dyn Query> {
        let range = as_interval(time_range);

        if range.is_empty() {
            return Box::new(EmptyQuery);
        }

        Box::new(TimeRangeQuery {
            trees: self.trees.load_full(),
            range,
        })
    }
}

impl Warmer for TimeRanges {
    fn warm(&self, searcher: &Searcher) -> TantivyResult<()> {
        let mut trees = self.trees.load_full();

        let new_trees = searcher
            .segment_readers()
            .par_iter()
            .filter_map(|segment| {
                let key = (segment.segment_id(), segment.delete_opstamp());
                if !trees.contains_key(&key) {
                    Some((key, segment))
                } else {
                    None
                }
            })
            .map(|(key, segment)| {
                let tree = load_tree(&self.dir, self.index_dir_name, segment)
                    .map_err(|err| TantivyError::InternalError(err.to_string()))?;

                Ok((key, tree))
            })
            .collect::<TantivyResult<Vec<_>>>()?;

        Arc::make_mut(&mut trees).extend(new_trees);

        self.trees.store(trees);

        Ok(())
    }

    fn garbage_collect(&self, live_generations: &[&SearcherGeneration]) {
        let live_keys = live_generations
            .iter()
            .flat_map(|gen| gen.segments())
            .map(|(&segment_id, &opstamp)| (segment_id, opstamp))
            .collect::<HashSet<_>>();

        let mut trees = self.trees.load_full();

        Arc::make_mut(&mut trees).retain(|key, _tree| {
            if !live_keys.contains(key) {
                let file_name = file_name(self.index_dir_name, key.0);

                if let Err(err) = self.dir.remove_file(file_name) {
                    tracing::warn!(
                        "Failed to delete temporal index for segment {}: {}",
                        key.0,
                        err
                    );
                }

                false
            } else {
                true
            }
        });

        self.trees.store(trees);
    }
}

#[derive(Clone)]
struct TimeRangeQuery {
    trees: Arc<HashMap<SegmentKey, SegmentTree>>,
    range: Range<i32>,
}

impl fmt::Debug for TimeRangeQuery {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.debug_struct("TimeRangeQuery")
            .field("range", &self.range)
            .finish_non_exhaustive()
    }
}

impl Query for TimeRangeQuery {
    fn weight(&self, enable_scoring: EnableScoring<'_>) -> TantivyResult<Box<dyn Weight>> {
        if enable_scoring.is_scoring_enabled() {
            Ok(Box::new(TimeRangeWeight::<true>(self.clone())))
        } else {
            Ok(Box::new(TimeRangeWeight::<false>(self.clone())))
        }
    }
}

#[derive(Clone)]
struct TimeRangeWeight<const SCORING: bool>(TimeRangeQuery);

impl<const SCORING: bool> Weight for TimeRangeWeight<SCORING> {
    fn scorer(
        &self,
        segment_reader: &SegmentReader,
        boost: Score,
    ) -> TantivyResult<Box<dyn Scorer>> {
        let key = (segment_reader.segment_id(), segment_reader.delete_opstamp());
        let tree = &self.0.trees[&key];

        if SCORING {
            let mut scores = Scores::new(segment_reader.max_doc());

            tree.query(self.0.range.clone(), |(interval, doc_id)| {
                scores.set(*doc_id, compute_overlap(&self.0.range, interval) * boost);
                ControlFlow::Continue(())
            });

            Ok(scores.into_scorer())
        } else {
            let mut docs = BitSet::with_max_value(segment_reader.max_doc());

            tree.query(self.0.range.clone(), |(_interval, doc_id)| {
                docs.insert(*doc_id);
                ControlFlow::Continue(())
            });

            Ok(Box::new(ConstScorer::new(BitSetDocSet::from(docs), boost)))
        }
    }

    fn explain(&self, segment_reader: &SegmentReader, doc: DocId) -> TantivyResult<Explanation> {
        let mut scorer = self.scorer(segment_reader, 1.0)?;
        if scorer.seek(doc) != doc {
            return Err(TantivyError::InvalidArgument(format!(
                "Document #({doc}) does not match"
            )));
        }

        let mut explanation = Explanation::new("TimeRangeQuery", 1.0);
        explanation.add_context(format!("TimeRange={:?}", self.0.range));
        Ok(explanation)
    }
}

fn as_interval(TimeRange { from, until }: TimeRange) -> Range<i32> {
    let from = (from.year() << 9) | from.ordinal() as i32;
    let until = (until.year() << 9) | until.ordinal() as i32;

    from..(until + 1)
}

/// Computes the duration-based Jaccard index between `range` and `interval`
fn compute_overlap(range: &Range<i32>, interval: &Range<i32>) -> Score {
    let [min_start, max_start] = minmax(range.start, interval.start);
    let [min_end, max_end] = minmax(range.end, interval.end);

    let intersection = (min_end - max_start) as Score;
    let r#union = (max_end - min_start) as Score;

    if intersection != 0.0 && r#union != 0.0 {
        intersection / r#union
    } else {
        // This function assumes being called with overlapping intervals,
        // so we do not need to figure if we should actually return zero instead.
        1.0
    }
}

fn load_tree(dir: &Dir, index_dir_name: &str, segment: &SegmentReader) -> Result<SegmentTree> {
    let file_name = file_name(index_dir_name, segment.segment_id());

    if let Some(tree) = map_tree(dir, &file_name)? {
        return Ok(tree);
    }

    tracing::info!(
        "Building temporal index for segment {} containing {} documents",
        segment.segment_id(),
        segment.num_docs()
    );

    let fast_fields = segment.fast_fields();
    let from = fast_fields.i64("time_range_from")?;
    let until = fast_fields.i64("time_range_until")?;

    let tree = segment
        .doc_ids_alive()
        .map(|doc_id| {
            let from = from.values_for_doc(doc_id);
            let until = until.values_for_doc(doc_id);

            let elements = zip_eq(from, until).map(move |(from, until)| {
                let range = from as i32..(until as i32 + 1);

                (range, doc_id)
            });

            Ok(elements)
        })
        .flatten_ok()
        .collect::<Result<_>>()?;

    write_tree(dir, &file_name, &tree)?;

    let tree = map_tree(dir, &file_name)?.unwrap();

    Ok(tree)
}

#[derive(Clone)]
struct MappedNodes(Arc<Mmap>);

impl AsRef<[Node<i32, DocId>]> for MappedNodes {
    #[allow(unsafe_code)]
    fn as_ref(&self) -> &[Node<i32, DocId>] {
        let ptr = self.0.as_ptr().cast();
        let len = self.0.len() / size_of::<Node<i32, DocId>>();
        unsafe { from_raw_parts(ptr, len) }
    }
}

#[allow(unsafe_code)]
fn map_tree(dir: &Dir, file_name: &str) -> Result<Option<SegmentTree>> {
    if let Ok(file) = dir.open(file_name) {
        let mmap = unsafe { Mmap::map(&file)? };
        let tree = ITree::new_unchecked(MappedNodes(Arc::new(mmap)));

        Ok(Some(tree))
    } else {
        Ok(None)
    }
}

#[allow(unsafe_code)]
fn write_tree(dir: &Dir, file_name: &str, tree: &ITree<i32, DocId>) -> Result<()> {
    let nodes = tree.as_ref();
    let ptr = nodes.as_ptr().cast();
    let len = size_of_val(nodes);
    let bytes = unsafe { from_raw_parts(ptr, len) };

    dir.write(file_name, bytes)?;
    Ok(())
}

fn file_name(index_dir_name: &str, segment_id: SegmentId) -> String {
    format!("{}/{}.time_range", index_dir_name, segment_id.uuid_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn compute_overlap_works() {
        assert_eq!(compute_overlap(&(0..1), &(0..1)), 1.0);
        assert_eq!(compute_overlap(&(0..1), &(0..2)), 0.5);
        assert_eq!(compute_overlap(&(-1..0), &(-2..0)), 0.5);
        assert_eq!(compute_overlap(&(1..2), &(0..4)), 0.25);
        assert_eq!(compute_overlap(&(0..3), &(2..4)), 0.25);
        assert_eq!(compute_overlap(&(2..4), &(0..3)), 0.25);
        assert_eq!(compute_overlap(&(1..1), &(1..1)), 1.0);
        assert_eq!(compute_overlap(&(0..1), &(1..1)), 1.0);
        assert_eq!(compute_overlap(&(1..1), &(0..1)), 1.0);
    }
}
