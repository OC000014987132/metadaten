use std::fs::create_dir_all;
use std::path::{Path, PathBuf};
use std::sync::Arc;

use anyhow::Result;
use cap_std::{ambient_authority, fs::Dir};
use tantivy::{
    directory::MmapDirectory,
    schema::{Facet, TantivyDocument as Document},
    store::Compressor,
    Index, IndexSettings, IndexWriter,
};

use crate::{
    dataset::{
        quality::QualityLandingPage, r#type::Type, Alternative, Dataset, Organisation, Region,
        ResourceType, Status, Tag,
    },
    index::{
        bounding_box::BoundingBoxes, index_reader, register_tokenizers, schema,
        time_range::TimeRanges, Fields,
    },
    value_from_env,
    wise::WISE,
};

pub struct Indexer {
    index: Index,
    writer: IndexWriter,
    fields: Arc<Fields>,
    upgrade: Option<PathBuf>,
}

impl Indexer {
    pub fn start(data_path: &Path) -> Result<Self> {
        let mut index_path = data_path.join("index");
        let index_path_new = data_path.join("index.new");

        let upgrade = index_path_new.is_dir();
        if upgrade {
            tracing::info!("Performing index upgrade");

            index_path = index_path_new
        }

        create_dir_all(&index_path)?;

        let schema = schema();
        let fields = Arc::new(Fields::new(&schema));

        let index = Index::builder()
            .schema(schema)
            .settings(IndexSettings {
                docstore_compression: Compressor::Zstd(Default::default()),
                ..Default::default()
            })
            .open_or_create(MmapDirectory::open(&index_path)?)?;

        register_tokenizers(data_path, &index)?;

        let memory_budget = value_from_env::<usize>("MEMORY_BUDGET");

        let writer = index.writer(memory_budget * 1024 * 1024)?;
        writer.delete_all_documents()?;

        Ok(Self {
            index,
            writer,
            fields,
            upgrade: upgrade.then_some(index_path),
        })
    }

    pub fn add_document(
        &self,
        source: String,
        id: String,
        content: Vec<u8>,
        dataset: Dataset,
        accesses: u64,
    ) -> Result<()> {
        let mut doc = Document::default();

        doc.add_text(self.fields.title, dataset.title);

        if let Some(description) = dataset.description {
            doc.add_text(self.fields.description, description);
        }

        if dataset.types.is_empty() {
            doc.add_facet(self.fields.r#type, Facet::from_path(Type::default_facet()));
        }

        for r#type in dataset.types {
            doc.add_facet(self.fields.r#type, Facet::from_path(r#type.facet()));

            for component in r#type.facet() {
                doc.add_text(self.fields.type_text, component);
            }

            if let Type::Measurements {
                measured_variables,
                methods,
                ..
            } = r#type
            {
                for measured_variable in measured_variables {
                    doc.add_text(self.fields.measurements, measured_variable);
                }

                for method in methods {
                    doc.add_text(self.fields.measurements, method);
                }
            }
        }

        if let Some(comment) = dataset.comment {
            doc.add_text(self.fields.description, comment);
        }

        if dataset.resources.is_empty() {
            doc.add_facet(
                self.fields.resource_type,
                Facet::from_path(ResourceType::default_facet()),
            );
        }

        for resource in dataset.resources {
            if let Some(description) = resource.description {
                doc.add_text(self.fields.description, description);
            }

            doc.add_facet(
                self.fields.resource_type,
                Facet::from_path(resource.r#type.facet()),
            );
        }

        for organisation in dataset.organisations {
            match organisation {
                Organisation::Other { .. } => (),
                Organisation::WikiData { identifier, .. } => {
                    doc.add_u64(self.fields.wikidata_id, identifier)
                }
            }

            organisation.with_token(|token| {
                doc.add_text(self.fields.organisation, token);
            });
        }

        for person in dataset.persons {
            doc.add_text(self.fields.person, person.name);
        }

        if dataset.tags.is_empty() {
            doc.add_facet(self.fields.topic, Tag::default_facet());
        }

        for tag in &dataset.tags {
            tag.with_facet(|facet| {
                doc.add_facet(self.fields.topic, facet);
            });
        }

        for origin in &dataset.origins {
            doc.add_facet(self.fields.origin, Facet::from_text(origin)?);

            for component in origin[1..].split('/') {
                doc.add_text(self.fields.origin_text, component);
            }
        }

        doc.add_facet(
            self.fields.license,
            Facet::from_path(dataset.license.facet()),
        );

        doc.add_facet(
            self.fields.language,
            Facet::from_path(dataset.language.facet()),
        );

        for alternative in &dataset.alternatives {
            if let Alternative::Language { language, .. } = alternative {
                doc.add_facet(self.fields.language, Facet::from_path(language.facet()));
            }
        }

        for tag in dataset.tags {
            match tag {
                Tag::Other(_) => (),
                Tag::Umthes(id) => doc.add_u64(self.fields.umthes_id, id),
            }

            tag.with_token(|token| {
                doc.add_text(self.fields.tags, token);
            });
        }

        for region in dataset.regions {
            match region {
                Region::Other(_) => (),
                Region::GeoName(id) => doc.add_u64(self.fields.geonames_id, id),
                Region::RegionalKey(key) => doc.add_u64(self.fields.regional_key, key),
                Region::Atkis(key) => doc.add_u64(self.fields.atkis_key, key),
                Region::Watershed(id) => {
                    let entry = WISE.resolve(id);

                    doc.add_text(self.fields.watershed_id, &entry.id);
                }
            }

            region.with_token(|token| {
                doc.add_text(self.fields.region, token);
            });
        }

        doc.add_bool(
            self.fields.mandatory_registration,
            dataset.mandatory_registration,
        );

        const QUALITY_WEIGHT: f32 = 0.15;
        const POPULARITY_WEIGHT: f32 = 0.05;
        const BM25_WEIGHT: f32 = 1.0 - QUALITY_WEIGHT - POPULARITY_WEIGHT;
        const {
            assert!(BM25_WEIGHT > 0.0);
        }

        let quality = 1.0 + dataset.quality.score;
        let popularity = ((2 + accesses) as f32).log2();
        let inherent_score = quality.powf(QUALITY_WEIGHT / BM25_WEIGHT)
            * popularity.powf(POPULARITY_WEIGHT / BM25_WEIGHT)
            * dataset.quality.accessibility.landing_page.inherent_score()
            * dataset.status.inherent_score();

        doc.add_f64(self.fields.inherent_score, inherent_score as f64);

        let eligible_for_random = matches!(dataset.status, Status::Active)
            && matches!(
                dataset.quality.accessibility.landing_page,
                QualityLandingPage::Specific
            );

        doc.add_bool(self.fields.eligible_for_random, eligible_for_random);

        for bounding_box in dataset.bounding_boxes {
            let min = bounding_box.min();
            let max = bounding_box.max();

            if min.x.is_nan() || min.y.is_nan() || max.x.is_nan() || max.y.is_nan() {
                tracing::warn!(
                    "NaN coordinates in bounding box of dataset {} in {}",
                    id,
                    source,
                );

                continue;
            }

            doc.add_f64(self.fields.bounding_box_min_x, min.x as f32 as f64);
            doc.add_f64(self.fields.bounding_box_min_y, min.y as f32 as f64);
            doc.add_f64(self.fields.bounding_box_max_x, max.x as f32 as f64);
            doc.add_f64(self.fields.bounding_box_max_y, max.y as f32 as f64);
        }

        for time_range in dataset.time_ranges {
            let from = (time_range.from.year() << 9) | time_range.from.ordinal() as i32;
            let until = (time_range.until.year() << 9) | time_range.until.ordinal() as i32;

            doc.add_i64(self.fields.time_range_from, from as i64);
            doc.add_i64(self.fields.time_range_until, until as i64);
        }

        doc.add_text(self.fields.source, source);
        doc.add_text(self.fields.id, id);
        doc.add_bytes(self.fields.content, content);

        self.writer.add_document(doc)?;

        Ok(())
    }

    pub fn commit(mut self) -> Result<()> {
        self.writer.commit()?;

        if let Some(index_path) = self.upgrade {
            self.writer.wait_merging_threads()?;

            let dir = Arc::new(Dir::open_ambient_dir(index_path, ambient_authority())?);

            let bounding_boxes = Arc::new(BoundingBoxes::new(dir.clone(), "."));
            let time_ranges = Arc::new(TimeRanges::new(dir, "."));

            index_reader(&self.index, &bounding_boxes, &time_ranges)?;
        }

        Ok(())
    }
}
