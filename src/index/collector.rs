use tantivy::{
    collector::{Collector, SegmentCollector},
    query::BitSetDocSet,
    DocAddress, DocId, Result as TantivyResult, Score, SegmentOrdinal, SegmentReader,
};
use tantivy_common::BitSet;

pub struct FirstDoc;

impl Collector for FirstDoc {
    type Fruit = Option<DocAddress>;

    type Child = FirstSegmentDoc;

    fn requires_scoring(&self) -> bool {
        false
    }

    fn for_segment(
        &self,
        segment_ord: SegmentOrdinal,
        _reader: &SegmentReader,
    ) -> TantivyResult<Self::Child> {
        Ok(FirstSegmentDoc {
            segment_ord,
            doc: DocId::MAX,
        })
    }

    fn merge_fruits(
        &self,
        child_fruits: Vec<<Self::Child as SegmentCollector>::Fruit>,
    ) -> TantivyResult<Self::Fruit> {
        let fruit = child_fruits
            .into_iter()
            .find(|fruit| fruit.doc != DocId::MAX)
            .map(|fruit| DocAddress {
                segment_ord: fruit.segment_ord,
                doc_id: fruit.doc,
            });

        Ok(fruit)
    }
}

pub struct FirstSegmentDoc {
    segment_ord: SegmentOrdinal,
    doc: DocId,
}

impl SegmentCollector for FirstSegmentDoc {
    type Fruit = Self;

    fn collect(&mut self, doc: DocId, _score: Score) {
        if self.doc == DocId::MAX {
            self.doc = doc;
        }
    }

    fn harvest(self) -> Self::Fruit {
        self
    }
}

pub struct AllDocs;

impl Collector for AllDocs {
    type Fruit = Vec<AllSegementDocs<BitSetDocSet>>;

    type Child = AllSegementDocs<BitSet>;

    fn requires_scoring(&self) -> bool {
        false
    }

    fn for_segment(
        &self,
        segment_ord: SegmentOrdinal,
        reader: &SegmentReader,
    ) -> TantivyResult<Self::Child> {
        Ok(AllSegementDocs {
            segment_ord,
            docs: BitSet::with_max_value(reader.max_doc()),
        })
    }

    fn merge_fruits(
        &self,
        child_fruits: Vec<<Self::Child as SegmentCollector>::Fruit>,
    ) -> TantivyResult<Self::Fruit> {
        Ok(child_fruits)
    }
}

pub struct AllSegementDocs<D> {
    pub segment_ord: SegmentOrdinal,
    pub docs: D,
}

impl SegmentCollector for AllSegementDocs<BitSet> {
    type Fruit = AllSegementDocs<BitSetDocSet>;

    fn collect(&mut self, doc: DocId, _score: Score) {
        self.docs.insert(doc);
    }

    fn harvest(self) -> Self::Fruit {
        AllSegementDocs {
            segment_ord: self.segment_ord,
            docs: self.docs.into(),
        }
    }
}
