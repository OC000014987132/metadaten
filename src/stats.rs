use std::collections::BTreeMap;
use std::mem::take;
use std::time::{Duration, SystemTime};

use anyhow::{bail, Context, Result};
use bincode::config::{DefaultOptions, Options};
use cap_std::fs::Dir;
use compact_str::CompactString;
use hashbrown::HashMap;
use parking_lot::Mutex;
use serde::{Deserialize, Serialize};
use tantivy::schema::Facet;
use time::OffsetDateTime;
use utoipa::ToSchema;

use crate::{
    dataset::{ScoredDataset, Tag},
    metrics::Metrics,
};

#[derive(Default, Clone, Deserialize, Serialize)]
pub struct Stats {
    pub next_decay: Option<SystemTime>,
    pub accesses: HashMap<String, HashMap<String, u64>>,
    pub tags: HashMap<Tag, u64>,
    pub counts: BTreeMap<SystemTime, Counts>,
}

#[derive(Default, Debug, Clone, PartialEq, Deserialize, Serialize, ToSchema)]
pub struct Counts {
    /// Number of distinct datasets
    pub datasets: usize,
    /// Number of distinct data sources, i.e. technical interfaces yielding one or more datasets
    pub sources: usize,
    /// Number of distinct providers, i.e. institutions operating one or more data sources
    pub providers: usize,
    /// Number of data sources which we failed to harvest
    pub failed_harvests: usize,
    /// Sum of errors encountered by all harvesters
    pub errors: usize,
}

impl From<&'_ Metrics> for Counts {
    fn from(metrics: &Metrics) -> Self {
        Self {
            datasets: metrics.datasets,
            sources: metrics.sources.len(),
            providers: metrics.providers.len(),
            failed_harvests: metrics.failed_harvests.len(),
            errors: metrics
                .harvests
                .values()
                .map(|harvest| harvest.failed)
                .sum(),
        }
    }
}

#[derive(Deserialize)]
struct OldStats {
    next_decay: Option<SystemTime>,
    accesses: HashMap<String, HashMap<String, u64>>,
    _terms: HashMap<CompactString, u64>,
    tags: HashMap<Tag, u64>,
    counts: BTreeMap<SystemTime, Counts>,
}

const CURR_VER: u8 = 9;
const OLD_VER: u8 = 8;

impl Stats {
    pub fn read(dir: &Dir) -> Result<Self> {
        if let Ok(buf) = dir.read("stats") {
            let res = match buf.split_last() {
                Some((&CURR_VER, buf)) => options().deserialize::<Self>(buf),
                Some((&OLD_VER, buf)) => {
                    options().deserialize::<OldStats>(buf).map(|old_val| Self {
                        next_decay: old_val.next_decay,
                        accesses: old_val.accesses,
                        tags: old_val.tags,
                        counts: old_val.counts,
                    })
                }
                _ => bail!("Invalid or missing stats format version"),
            };

            res.context("Invalid stats format")
        } else {
            Ok(Default::default())
        }
    }

    pub fn write(this: &Mutex<Self>, dir: &Dir, metrics: &Metrics) -> Result<()> {
        let mut buf = {
            let mut this = this.lock();

            this.decay(metrics);

            options().serialize(&*this)?
        };
        buf.push(CURR_VER);

        dir.write("stats.new", &buf)?;
        dir.rename("stats.new", dir, "stats")?;

        Ok(())
    }

    pub fn overwrite(&self, dir: &Dir) -> Result<()> {
        let mut buf = options().serialize(self)?;
        buf.push(CURR_VER);

        dir.write("stats", &buf)?;

        Ok(())
    }

    fn decay(&mut self, metrics: &Metrics) {
        let now = SystemTime::now();

        let next_decay = self.next_decay.get_or_insert(now);

        if *next_decay > now {
            return;
        }

        *next_decay = now + DAY;

        tracing::info!("Decaying statistics");

        fn decay<const N: usize>(count: &mut u64) -> bool {
            // Decay by dividing by a power of two to improve efficiency,
            // i.e. this is equivalent to `count * (2^N - 1) / 2^N`.
            *count = ((*count << N) - *count) >> N;

            *count != 0
        }

        self.accesses.retain(|_source, accesses| {
            accesses.retain(|_id, accesses| decay::<4>(accesses));

            !accesses.is_empty()
        });

        self.tags.retain(|_tags, queries| decay::<4>(queries));

        thin_counts(&mut self.counts, now);

        self.counts.insert(now, Counts::from(metrics));
    }

    pub fn record_access(&mut self, source: &str, id: &str) -> u64 {
        let accesses = self
            .accesses
            .entry_ref(source)
            .or_default()
            .entry_ref(id)
            .or_default();

        *accesses += 1;

        *accesses
    }

    pub fn record_query(
        &mut self,
        terms: &[CompactString],
        topics_root: &Facet,
        results: &[ScoredDataset],
    ) {
        if !terms.is_empty() || !topics_root.is_root() {
            for dataset in results {
                for tag in &dataset.value.value.tags {
                    *self.tags.entry_ref(tag).or_default() += 1;
                }
            }
        }
    }
}

fn options() -> impl Options {
    DefaultOptions::new().with_fixint_encoding()
}

const DAY: Duration = Duration::from_secs(24 * 3600);

fn thin_counts(counts: &mut BTreeMap<SystemTime, Counts>, now: SystemTime) {
    // Completely rewrite the `counts` time series by `take`ing it
    // and then rebuilding it in order, keeping only the latest entry
    // for a given granularity (daily, weekly, monthly, yearly).
    let mut iter = take(counts).into_iter();

    let Some((mut prev_timestamp, mut prev_counts)) = iter.next() else {
        return;
    };

    let mut prev_date = OffsetDateTime::from(prev_timestamp).date();

    for (curr_timestamp, curr_counts) in iter {
        let curr_date = OffsetDateTime::from(curr_timestamp).date();
        let age = now.duration_since(curr_timestamp).unwrap();

        let keep = if age <= 30 * DAY {
            // keep daily values for a month
            curr_date.ordinal() != prev_date.ordinal()
        } else if age <= 365 * DAY {
            // keep weekly values for a year
            curr_date.iso_week() != prev_date.iso_week()
        } else if age <= 10 * 365 * DAY {
            // keep monthly values for a decade
            curr_date.month() != prev_date.month()
        } else {
            // keep yearly values otherwise
            curr_date.year() != prev_date.year()
        };

        if keep {
            counts.insert(prev_timestamp, prev_counts);
        }

        prev_timestamp = curr_timestamp;
        prev_counts = curr_counts;

        prev_date = curr_date;
    }

    counts.insert(prev_timestamp, prev_counts);
}

#[cfg(test)]
mod tests {
    use super::*;

    use time::macros::datetime;

    #[test]
    fn thin_counts_aggregates_daily_values() {
        let mut counts = BTreeMap::new();

        counts.insert(
            datetime!(2024-10-01 15:00 UTC).into(),
            Counts {
                datasets: 15,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-10-01 07:00 UTC).into(),
            Counts {
                datasets: 10,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-09-30 07:00 UTC).into(),
            Counts {
                datasets: 5,
                ..Default::default()
            },
        );

        thin_counts(&mut counts, datetime!(2024-10-02 03:00 UTC).into());

        let counts = counts
            .into_iter()
            .map(|(timestamp, counts)| (OffsetDateTime::from(timestamp), counts))
            .collect::<Vec<_>>();

        assert_eq!(
            counts,
            [
                (
                    datetime!(2024-09-30 07:00 UTC),
                    Counts {
                        datasets: 5,
                        ..Default::default()
                    }
                ),
                (
                    datetime!(2024-10-01 15:00 UTC),
                    Counts {
                        datasets: 15,
                        ..Default::default()
                    }
                ),
            ]
        );
    }

    #[test]
    fn thin_counts_aggregates_weekly_values() {
        let mut counts = BTreeMap::new();

        counts.insert(
            datetime!(2024-08-25 07:00 UTC).into(),
            Counts {
                datasets: 40,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-08-24 07:00 UTC).into(),
            Counts {
                datasets: 35,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-08-23 07:00 UTC).into(),
            Counts {
                datasets: 30,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-08-22 07:00 UTC).into(),
            Counts {
                datasets: 25,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-08-21 07:00 UTC).into(),
            Counts {
                datasets: 20,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-08-20 07:00 UTC).into(),
            Counts {
                datasets: 15,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-08-19 07:00 UTC).into(),
            Counts {
                datasets: 10,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2024-08-18 07:00 UTC).into(),
            Counts {
                datasets: 5,
                ..Default::default()
            },
        );

        thin_counts(&mut counts, datetime!(2024-10-02 03:00 UTC).into());

        let counts = counts
            .into_iter()
            .map(|(timestamp, counts)| (OffsetDateTime::from(timestamp), counts))
            .collect::<Vec<_>>();

        assert_eq!(
            counts,
            [
                (
                    datetime!(2024-08-18 07:00 UTC),
                    Counts {
                        datasets: 5,
                        ..Default::default()
                    }
                ),
                (
                    datetime!(2024-08-25 07:00 UTC),
                    Counts {
                        datasets: 40,
                        ..Default::default()
                    }
                ),
            ]
        );
    }

    #[test]
    fn thin_counts_aggregates_monthly_values() {
        let mut counts = BTreeMap::new();

        counts.insert(
            datetime!(2023-09-30 07:00 UTC).into(),
            Counts {
                datasets: 15,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2023-09-01 07:00 UTC).into(),
            Counts {
                datasets: 10,
                ..Default::default()
            },
        );

        counts.insert(
            datetime!(2023-08-31 07:00 UTC).into(),
            Counts {
                datasets: 5,
                ..Default::default()
            },
        );

        thin_counts(&mut counts, datetime!(2024-10-02 03:00 UTC).into());

        let counts = counts
            .into_iter()
            .map(|(timestamp, counts)| (OffsetDateTime::from(timestamp), counts))
            .collect::<Vec<_>>();

        assert_eq!(
            counts,
            [
                (
                    datetime!(2023-08-31 07:00 UTC),
                    Counts {
                        datasets: 5,
                        ..Default::default()
                    }
                ),
                (
                    datetime!(2023-09-30 07:00 UTC),
                    Counts {
                        datasets: 15,
                        ..Default::default()
                    }
                ),
            ]
        );
    }
}
