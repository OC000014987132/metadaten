use std::fmt;
use std::time::{Duration, SystemTime};

use anyhow::{bail, Context, Result};
use bincode::{deserialize, serialize};
use cap_std::fs::Dir;
use hashbrown::{HashMap, HashSet};
use serde::{Deserialize, Serialize};
use sketches_ddsketch::DDSketch;
use string_cache::DefaultAtom;

use crate::dataset::{Alternative, Dataset, License, OrganisationKey, Region, ResourceType, Tag};

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Metrics {
    pub last_harvest: Option<(SystemTime, Duration)>,
    pub harvests: HashMap<DefaultAtom, Harvest>,
    pub failed_harvests: HashSet<DefaultAtom>,
    pub datasets: usize,
    pub sources: HashSet<DefaultAtom>,
    pub providers: HashSet<String>,
    pub licenses: HashMap<DefaultAtom, HashMap<License, usize>>,
    pub resource_types: HashMap<DefaultAtom, HashMap<ResourceType, usize>>,
    pub tags: HashMap<Tag, usize>,
    pub regions: HashMap<Region, usize>,
    pub organisations: HashMap<OrganisationKey, usize>,
    pub alternatives: HashMap<usize, usize>,
    pub umthes: Umthes,
    pub quality: Quality,
}

#[derive(Deserialize)]
struct OldMetrics {
    last_harvest: Option<(SystemTime, Duration)>,
    harvests: HashMap<DefaultAtom, Harvest>,
    failed_harvests: HashSet<DefaultAtom>,
    datasets: usize,
    sources: HashSet<DefaultAtom>,
    providers: HashSet<String>,
    licenses: HashMap<DefaultAtom, HashMap<License, usize>>,
    resource_types: HashMap<DefaultAtom, HashMap<ResourceType, usize>>,
    tags: HashMap<Tag, usize>,
    regions: HashMap<Region, usize>,
    alternatives: HashMap<usize, usize>,
    umthes: Umthes,
    quality: Quality,
}

const CURR_VER: u8 = 12;
const OLD_VER: u8 = 11;

impl Metrics {
    pub fn read(dir: &Dir) -> Result<Self> {
        if let Ok(buf) = dir.read("metrics") {
            let res = match buf.split_last() {
                Some((&CURR_VER, buf)) => deserialize::<Self>(buf),
                Some((&OLD_VER, buf)) => deserialize::<OldMetrics>(buf).map(|old_val| Self {
                    last_harvest: old_val.last_harvest,
                    harvests: old_val.harvests,
                    failed_harvests: old_val.failed_harvests,
                    datasets: old_val.datasets,
                    sources: old_val.sources,
                    providers: old_val.providers,
                    licenses: old_val.licenses,
                    resource_types: old_val.resource_types,
                    tags: old_val.tags,
                    regions: old_val.regions,
                    organisations: Default::default(),
                    alternatives: old_val.alternatives,
                    umthes: old_val.umthes,
                    quality: old_val.quality,
                }),
                _ => bail!("Invalid or missing metrics format version"),
            };

            res.context("Invalid metrics format")
        } else {
            Ok(Default::default())
        }
    }

    pub fn write(&self, dir: &Dir) -> Result<()> {
        let mut buf = serialize(self)?;
        buf.push(CURR_VER);

        dir.write("metrics.new", &buf)?;
        dir.rename("metrics.new", dir, "metrics")?;

        Ok(())
    }

    pub fn record_harvest(&mut self, source_name: DefaultAtom, harvest: Harvest) {
        self.harvests.insert(source_name, harvest);
    }

    pub fn record_failed_harvest(&mut self, source_name: DefaultAtom) {
        self.failed_harvests.insert(source_name);
    }

    pub fn clear_datasets(&mut self) {
        self.datasets = 0;
        self.sources.clear();
        self.providers.clear();

        self.licenses.clear();
        self.resource_types.clear();
        self.tags.clear();
        self.regions.clear();
        self.organisations.clear();
        self.alternatives.clear();

        self.quality = Default::default();
    }

    pub fn record_dataset(&mut self, source_name: &DefaultAtom, dataset: &Dataset) {
        self.datasets += 1;

        self.sources
            .get_or_insert_with(source_name, ToOwned::to_owned);

        for alternative in &dataset.alternatives {
            if let Alternative::Source { source, .. } = alternative {
                self.sources.get_or_insert_with(source, ToOwned::to_owned);
            }
        }

        for origin in &dataset.origins {
            self.providers
                .get_or_insert_with(provider(origin), ToOwned::to_owned);
        }

        *self
            .licenses
            .entry_ref(source_name)
            .or_default()
            .entry_ref(&dataset.license)
            .or_default() += 1;

        let resource_types = self.resource_types.entry_ref(source_name).or_default();

        for resource in &dataset.resources {
            *resource_types.entry(resource.r#type).or_default() += 1;
        }

        for tag in &dataset.tags {
            *self.tags.entry_ref(tag).or_default() += 1;
        }

        for region in &dataset.regions {
            *self.regions.entry_ref(region).or_default() += 1;
        }

        for organisation in &dataset.organisations {
            *self
                .organisations
                .entry_ref(&organisation.key())
                .or_default() += 1;
        }

        *self
            .alternatives
            .entry(dataset.alternatives.len())
            .or_default() += 1;

        self.quality.overall.add(dataset.quality.score.into());

        self.quality
            .accessibility
            .add(dataset.quality.accessibility.score.into());
        self.quality
            .findability
            .add(dataset.quality.findability.score.into());
        self.quality
            .interoperabiltiy
            .add(dataset.quality.interoperability.score.into());
        self.quality
            .reusability
            .add(dataset.quality.reusability.score.into());
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Harvest {
    pub deleted: usize,
    pub age: Duration,
    pub duration: Duration,
    pub count: usize,
    pub transmitted: usize,
    pub failed: usize,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Umthes {
    pub fetched_tags: usize,
    pub fetched_tag_definitions: usize,
    pub searched_tags: usize,
    pub tags: usize,
    pub tag_definitions: usize,
    pub search_results: usize,
}

#[derive(Default, Serialize, Deserialize)]
pub struct Quality {
    pub overall: DDSketch,
    pub accessibility: DDSketch,
    pub findability: DDSketch,
    pub interoperabiltiy: DDSketch,
    pub reusability: DDSketch,
}

impl fmt::Debug for Quality {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt.debug_struct("Quality")
            .field("score", &"..")
            .field("accessibility", &"..")
            .field("findability", &"..")
            .field("interoperabiltiy", &"..")
            .field("reusability", &"..")
            .finish()
    }
}

fn provider(origin: &str) -> &str {
    origin
        .match_indices('/')
        .nth(2)
        .map_or(origin, |(index, _)| &origin[..index])
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn provider_works() {
        assert_eq!(provider("/Land/Berlin/Datenportal"), "/Land/Berlin");
        assert_eq!(provider("/Bund/UBA/GDI"), "/Bund/UBA");
        assert_eq!(provider("/Bund/GovData"), "/Bund/GovData");
        assert_eq!(provider("/Foo/Bar/Baz/Qux"), "/Foo/Bar");
        assert_eq!(provider("/Foo"), "/Foo");
    }
}
