use tiny_bench::{bench_with_configuration_labeled, black_box, BenchmarkConfig};

use metadaten::{data_path_from_env, index::Searcher};

fn main() {
    let searcher = Searcher::open(&data_path_from_env()).unwrap();

    let complete = |mut query| {
        searcher
            .complete(black_box(&Default::default()), black_box(&mut query))
            .unwrap()
    };

    let config = BenchmarkConfig {
        max_iterations: Some(100),
        ..Default::default()
    };

    bench_with_configuration_labeled("4_characters", &config, || complete("Bund"));

    bench_with_configuration_labeled("3_characters", &config, || complete("BfN"));

    bench_with_configuration_labeled("4_characters_few_results", &config, || complete("XYZA"));

    bench_with_configuration_labeled("4_characters_many_results", &config, || complete("Wass"));

    bench_with_configuration_labeled("non_ascii_characters", &config, || {
        complete("/?~Ѧ𝙱ƇᗞΣℱԍҤ١𝔍К𝓛𝓜ƝȎ𝚸𝑄Ṛ𝓢ṮṺƲᏔꓫ𝚈𝚭𝜶Ꮟçძ𝑒𝖿𝗀ḧ𝗂𝐣ҝɭḿ𝕟𝐨𝝔𝕢ṛ𝓼тú𝔳ẃ⤬𝝲𝗓1234567890!@#$%^&*()-_=+[{]};:',<.>/?~𝖠Β𝒞𝘋𝙴𝓕ĢȞỈ𝕵ꓗʟ𝙼ℕ০𝚸𝗤ՀꓢṰǓⅤ𝔚Ⲭ𝑌𝙕𝘢𝕤")
    });

    bench_with_configuration_labeled("non_ascii_characters_but_many_results", &config, || {
        complete("/?~Ѧ𝙱ƇᗞΣℱԍҤ١𝔍К𝓛𝓜ƝȎ𝚸𝑄Ṛ𝓢ṮṺƲᏔꓫ𝚈𝚭𝜶 Wass")
    });
}
