use geo::{Coord, Rect};
use smallvec::{smallvec, SmallVec};
use time::macros::date;
use tiny_bench::{bench_labeled, black_box};

use metadaten::dataset::{
    Dataset, GlobalIdentifier, Organisation, OrganisationRole, Person, Region, Resource,
    ResourceType, Tag, TimeRange,
};

fn main() {
    let dataset = Dataset {
            title: "Rechtliche Gliederung des Nationalparks mit den angrenzenden Gebieten 2001 (GK)".to_owned(),
            description: Some("Rechtliche Gliederung (Zonierung) im Bereich des Nationalparks Schleswig-Holsteinsches Wattenmeer und benachbarter Gebiete.".to_owned()),
            origins: smallvec!["/Land/Schleswig-Holstein/Umweltportal".into()],
            license: metadaten::dataset::License::DlDeBy20,
            mandatory_registration: false,
            organisations: smallvec![Organisation::WikiData {
                identifier: 1785301,
                role: OrganisationRole::Unknown
            }],
            persons: vec![Person {
                name: "Jörn Kohlus".to_owned(),
                role: metadaten::dataset::PersonRole::Contact,
                emails: smallvec!["joern.kohlus@lkn.landsh.de".to_owned()],
                ..Default::default()
            }],
            tags: vec![Tag::Umthes(0x160802), Tag::Other("Borders".into())],
            regions: smallvec![Region::MV],
            modified: Some(date!(2023 - 05 - 23)),
            source_url: "https://umweltportal.schleswig-holstein.de/trefferanzeige?docuuid=metadata-npash78574".to_owned(),
            machine_readable_source: true,
            resources: smallvec![Resource {
                r#type: ResourceType::Zip,
                url: "https://s-h.nokis.org/Geodaten/gk/mod100/s_jura/ubgkzo01fl/ubgkzo01fl.zip".to_owned(),
                direct_link: true,
                primary_content: true,
                description: Some("A long link".to_owned())
            }],
            language: metadaten::dataset::Language::German,
            bounding_boxes: smallvec![Rect::new(
                Coord { x: 6.25, y: 53.22 },
                Coord { x: 9.52, y: 55.2 }
            )],
            time_ranges: smallvec![TimeRange {
                from: date!(2001 - 01 - 01),
                until: date!(2001 - 12 - 31)
            }],
            global_identifier: Some(GlobalIdentifier::Url(
                "http://portalu.de/igc_2/40288a8115457aa50115457c6e072bb2".parse().unwrap(),
            )),
            ..Default::default()
    };

    fn blow_up<F, T>(field: &mut F)
    where
        for<'a> &'a F: IntoIterator<Item = &'a T>,
        for<'a> <&'a F as IntoIterator>::IntoIter: Clone,
        T: Clone,
        F: FromIterator<T>,
    {
        *field = field.into_iter().cycle().take(500).cloned().collect();
    }

    let mut large_dataset = dataset.clone();

    blow_up::<SmallVec<_>, _>(&mut large_dataset.organisations);
    blow_up::<Vec<_>, _>(&mut large_dataset.persons);

    blow_up::<SmallVec<_>, _>(&mut large_dataset.resources);

    blow_up::<SmallVec<_>, _>(&mut large_dataset.bounding_boxes);
    blow_up::<SmallVec<_>, _>(&mut large_dataset.time_ranges);

    blow_up::<Vec<_>, _>(&mut large_dataset.tags);

    bench_labeled("store", || {
        let mut buf = Vec::new();
        black_box(&dataset).store(black_box(&mut buf)).unwrap();
        buf
    });

    bench_labeled("store_large", || {
        let mut buf = Vec::new();
        black_box(&large_dataset)
            .store(black_box(&mut buf))
            .unwrap();
        buf
    });

    let mut buf = Vec::new();
    dataset.store(&mut buf).unwrap();

    bench_labeled("parse", || Dataset::parse(black_box(&buf)).unwrap());

    buf.clear();
    large_dataset.store(&mut buf).unwrap();

    bench_labeled("parse_large", || Dataset::parse(black_box(&buf)).unwrap());
}
