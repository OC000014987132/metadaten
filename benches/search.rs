use geo::{Coord, Rect};
use tantivy::schema::Facet;
use tiny_bench::{bench_with_configuration_labeled, black_box, BenchmarkConfig};

use metadaten::{data_path_from_env, index::Searcher};

fn main() {
    let searcher = Searcher::open(&data_path_from_env()).unwrap();

    let search = |query, bounding_box| {
        searcher
            .search(
                black_box(&Default::default()),
                black_box(&query),
                black_box(&Facet::root()),
                black_box(&Facet::root()),
                black_box(&Facet::root()),
                black_box(&Facet::root()),
                black_box(&Facet::root()),
                black_box(&Facet::root()),
                black_box(bounding_box),
                black_box(false),
                black_box(false),
                black_box(None),
                black_box(&[]),
                black_box(100),
                black_box(0),
            )
            .unwrap()
    };

    let big_bounding_box = Rect::new(
        Coord {
            x: -180.0,
            y: -90.0,
        },
        Coord { x: 180.0, y: 90.0 },
    );

    let small_bounding_box = Rect::new(
        Coord {
            x: 12.83,
            y: 52.598,
        },
        Coord {
            x: 13.007,
            y: 52.689,
        },
    );

    let config = BenchmarkConfig {
        max_iterations: Some(100),
        ..Default::default()
    };

    bench_with_configuration_labeled("all", &config, || search("*", None));

    bench_with_configuration_labeled("with_query", &config, || search("fluoranthen", None));

    bench_with_configuration_labeled("with_tags_query", &config, || {
        search("tags:Schutzstatus", None)
    });

    bench_with_configuration_labeled("with_big_bounding_box", &config, || {
        search("*", Some(big_bounding_box))
    });

    bench_with_configuration_labeled("with_small_bounding_box", &config, || {
        search("*", Some(small_bounding_box))
    });

    bench_with_configuration_labeled("with_query_and_small_bounding_box", &config, || {
        search("fluoranthen", Some(small_bounding_box))
    });

    bench_with_configuration_labeled("with_tags_query_and_small_bounding_box", &config, || {
        search("tags:Schutzstatus", Some(small_bounding_box))
    });
}
