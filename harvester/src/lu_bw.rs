use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use poppler::Document;
use regex::{Regex, RegexSet};
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_str;
use smallvec::{smallvec, SmallVec};
use std::borrow::Cow;
use time::{macros::format_description, Date};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_pages, parse_short_year, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();

    let source_url = source
        .source_url
        .as_deref()
        .ok_or_else(|| anyhow!("Missing source URL"))?;

    let source_url = &Url::parse(source_url)?;

    let (count, results, errors) =
        fetch_page(dir, client, source, &selectors, source_url, 0).await?;

    let pages = count.div_ceil(source.batch_size);

    // Do multiple requests for paginated search at the same time and fetch respective search result pages
    let (results, errors) = fetch_many(results, errors, 1..pages, |page| {
        fetch_page(
            dir,
            client,
            source,
            &selectors,
            source_url,
            page * source.batch_size,
        )
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    source_url: &Url,
    start: usize,
) -> Result<(usize, usize, usize)> {
    let start = start.to_string();
    tracing::trace!(
        "Fetching {} datasets starting from result number {}",
        source.batch_size,
        start
    );

    let mut url = source.url.clone();

    url.query_pairs_mut()
        .append_pair("se", "bw")
        .append_pair("q", "lubw.baden-wuerttemberg.de")
        .append_pair("page[offset]", &start)
        .append_pair("page[limit]", &source.batch_size.to_string());

    let text = client.fetch_text(source, start, &url).await?;
    let response = from_str::<SearchResponse>(&text)?;

    let count = response.meta.total;

    let (results, errors) = fetch_many(
        0,
        0,
        response.results.search_results,
        |search_result| async move {
            // only use results that have a title as this is the minimum requirement for metadata quality
            let Some(title) = search_result.title else {
                return Ok((1, 0, 0));
            };

            let search_result = CheckedSearchResult {
                url: search_result.url,
                title,
                id: search_result.meta.id,
            };

            let result_url = &search_result.url;

            if result_url.host() != source_url.host() {
                return Ok((1, 0, 0));
            }

            // FIXME: a few urls stand out by beeing extremely long
            // they often lead to corrupt results and are currently discarded
            if result_url.as_str().len() > 150 {
                return Ok((1, 0, 0));
            }

            let path = result_url.path();

            // some pages are discarded sinc they only are overview pages without actual content
            if selectors.paths_overview.is_match(path) {
                return Ok((1, 0, 0));
            }

            // use all results that link to /blog/ except /startseite,
            // which has a different layout and has content that isn't relevant for us
            if path.starts_with("/blog/") {
                write_blog_dataset(dir, client, source, selectors, search_result).await
            }
            // the path /-/ is a catchall folder for a number of lists
            // additional criteria may be needed to disentangle all categories within
            else if path.starts_with("/-/") {
                write_catchall_dataset(dir, client, source, selectors, search_result).await
            }
            // default category for a number of url folders
            else if selectors.paths_default.is_match(path) {
                write_default_dataset(dir, client, source, selectors, search_result).await
            }
            // harvesting documents from the /documents/ folder that are PDFs
            // documents which are not PDFs (eg. png, xlsx, doc, gif, xls) are ignored since we can't harvest them yet
            else if path.starts_with("/documents/") {
                if selectors.paths_pdf.is_match(path) {
                    write_pdf_dataset(dir, client, source, search_result).await
                } else {
                    Ok((1, 0, 0))
                }
            } else {
                tracing::debug!("Unhandled path: {path}");
                Ok((1, 0, 0))
            }
        },
    )
    .await;

    Ok((count, results, errors))
}

#[derive(Debug, Deserialize)]
struct SearchResponse<'a> {
    #[serde(borrow)]
    results: SearchResults<'a>,
    meta: SearchMeta,
}

#[derive(Debug, Deserialize)]
struct SearchMeta {
    total: usize,
}

#[derive(Debug, Deserialize)]
struct SearchResults<'a> {
    #[serde(borrow, rename = "entities")]
    search_results: Vec<SearchResult<'a>>,
}

#[derive(Debug, Deserialize)]
struct SearchResult<'a> {
    url: Url,
    title: Option<Cow<'a, str>>,
    meta: SearchResultMeta,
}

#[derive(Debug, Deserialize)]
struct SearchResultMeta {
    id: String,
}

struct CheckedSearchResult<'a> {
    url: Url,
    title: Cow<'a, str>,
    id: String,
}

async fn write_blog_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    search_result: CheckedSearchResult<'_>,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", search_result.url);

    let id = search_result.id;
    let text = client
        .fetch_text(source, format!("{id}.isol"), &search_result.url)
        .await?;

    let dataset = {
        let document = Html::parse_document(&text);
        let title = select_text(&document, &selectors.blog_title);
        let description = select_text(&document, &selectors.blog_description);

        // parse_short_year is necessary since the format is dd.mm.yy instead of dd.mm.yyyy
        let date_select = select_text(&document, &selectors.blog_date_select);
        let date_captures = selectors
            .blog_date_regex
            .captures(&date_select)
            .ok_or_else(|| anyhow!("Missing date in blog entry"))?;
        let issued = parse_short_year(2000, &date_captures[0])?;

        let resources = document
            .select(&selectors.blog_links)
            .map(|elem| {
                let href = elem.attr("href").unwrap();

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    url: search_result.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::BlogPost,
            }],
            license: License::OtherClosed,
            origins: source.origins.clone(),
            resources,
            regions: smallvec![Region::BW],
            issued: Some(issued),
            source_url: search_result.url.into(),
            language: Language::German,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, id, dataset).await
}

async fn write_catchall_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    search_result: CheckedSearchResult<'_>,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", &search_result.url);

    let id = search_result.id;
    let text = client
        .fetch_text(source, format!("{id}.isol"), &search_result.url)
        .await?;

    let dataset = {
        let document = Html::parse_document(&text);

        let title = search_result.title;

        // titles taken from lupo cloud are sometimes inflated with ideosynchratic additions
        // which should be removed
        let title = title
            .strip_suffix(" - Landesanstalt für Umwelt Baden-Württemberg")
            .unwrap_or(&*title)
            .to_owned();

        let description = select_text(&document, &selectors.blog_description);

        let types =
            if title.contains("Pressemitteilung") || description.contains("Pressemitteilung") {
                smallvec![Type::Text {
                    text_type: TextType::PressRelease,
                }]
            } else {
                smallvec![Type::Text {
                    text_type: TextType::Editorial
                }]
            };

        let date = select_text(&document, &selectors.default_date);
        let issued = Date::parse(&date, format_description!("[day].[month].[year]")).ok();

        let resources = document
            .select(&selectors.default_links)
            .map(|elem| {
                let href = elem.attr("href").unwrap();

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    url: search_result.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title,
            description: Some(description),
            types,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: search_result.url.into(),
            issued,
            regions: smallvec![Region::BW],
            resources,
            language: Language::German,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, id, dataset).await
}

async fn write_default_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    search_result: CheckedSearchResult<'_>,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", &search_result.url);

    let id = search_result.id;
    let text = client
        .fetch_text(source, format!("{id}.isol"), &search_result.url)
        .await?;

    let dataset = {
        let document = Html::parse_document(&text);

        let title = select_text(&document, &selectors.default_title);

        let title = title
            .strip_prefix("Produkt-Administrationsmenü ")
            .unwrap_or(&*title)
            .to_owned();

        let description = select_text(&document, &selectors.default_description);

        let mut resources = document
            .select(&selectors.default_links)
            .map(|elem| {
                let href = elem.attr("href").unwrap();

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    url: search_result.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        let images = document
            .select(&selectors.default_images)
            .filter_map(|elem| {
                let src = elem.attr("src").unwrap();

                if !src.contains("/image/company_logo?") {
                    Some(src)
                } else {
                    None
                }
            })
            .map(|src| {
                Ok(Resource {
                    r#type: ResourceType::Image,
                    url: search_result.url.join(src)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<Vec<_>>>()?;

        resources.extend(images);

        let language = if search_result.url.path().contains("leichte-sprache") {
            Language::GermanEasy
        } else {
            Language::German
        };

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial
            }],
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: search_result.url.into(),
            resources,
            regions: smallvec![Region::BW],
            language,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, id, dataset).await
}

async fn write_pdf_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    search_result: CheckedSearchResult<'_>,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", &search_result.url);

    let id = search_result.id;
    let bytes = client
        .fetch_pdf(source, format!("{id}.isol"), &search_result.url)
        .await?;

    let dataset = {
        let title = search_result.title;

        // titles taken from lupo cloud are sometimes inflated with ideosynchratic additions
        // which should be removed
        let title = title.strip_prefix("zur PDF:").unwrap_or(&*title).to_owned();

        let document = Document::from_bytes(&bytes, None)?;

        let description = collect_pages(&document, 0..3);

        Dataset {
            title,
            description: Some(description),
            license: License::OtherClosed,
            origins: source.origins.clone(),
            regions: smallvec![Region::BW],
            source_url: search_result.url.into(),
            language: Language::German,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, id, dataset).await
}

selectors! {
    paths_overview:
        r"^/startseite",
        r"^/blog/\-/blogs/de/startseite",
        r"^/blog/\-/blogs/de/de/startseite",
        r"^/klimawandel\-und\-anpassung/projektbeschreibung\-klimopass",
        r"^/natur\-und\-landschaft/map\-endfassungen\-uebersicht"
        as RegexSet,
    paths_default:
        r"^/klimawandel\-und\-anpassung",
        r"^/natur\-und\-landschaft",
        r"^/luft",
        r"^/elektromagnetische\-felder",
        r"^/umweltinformationssystem",
        r"^/radioaktivitaet",
        r"^/betrieblicher\-umweltschutz",
        r"^/laerm\-und\-erschborrowuetterungen",
        r"^/medienuebergreifende\-umweltbeobachtung",
        r"^/erneuerbare\-energien",
        r"^/leistungsspektrum",
        r"^/umweltdaten\-umweltindikatoren",
        r"^/boden",
        r"^/nachhaltigkeit",
        r"^/nanomaterialien",
        r"^/abfall\-und\-kreislaufwirtschaft",
        r"^/chemikalien",
        r"^/marktueberwachung",
        r"^/arbeitsschutz",
        r"^/wasser",
        r"^/unsere\-labore",
        r"^/die\-lubw",
        r"^/altlasten",
        r"^/umweltforschung",
        r"^/standorte",
        r"^/fachinformationen",
        r"^/ausschreibungen\-und\-vergabe",
        r"^/emas\-in\-der\-lubw",
        r"^/leichte\-sprache",
        r"^/laerm\-und\-erschuetterungen"
        as RegexSet,
    paths_pdf: r"\.pdf/", r"\.PDF/", r"\.pdf$", r"\.PDF$" as RegexSet,

    default_title: "div h1, h3, .d-inline",
    default_description: "p",
    default_links: "p a[href]:not([href^='javascript:']), div.weiterer-text-unten a[href]:not([href^='javascript:'])",
    default_date: "b",
    default_images: "img[src]",
    default_maps: "#map",

    themen_title: "h1",
    themen_links: "#layout-column_column-2 a",

    blog_title: "div h3",
    blog_description: "div p",
    blog_date_select: ".autofit-col-expand div",
    blog_date_regex: r"\d\d\.\d\d\.\d\d" as Regex,
    blog_links: "#_com_liferay_blogs_web_portlet_BlogsPortlet_fm1 a[href]:not([href^='javascript:'])",

    wasser_title: "div[id='wrapper'] h1",
    wasser_title_alternative: "div h1",
    wasser_description: "div p",
    wasser_date_select: "div id",
}
