use std::borrow::Cow;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use geo::{algorithm::BoundingRect, Coord, Rect};
use geozero::{geojson::GeoJson, ToGeo};
use itertools::Itertools;
use regex::Regex;
use serde::Deserialize;
use serde_roxmltree::{from_doc, roxmltree::Document};
use smallvec::{smallvec, SmallVec};
use time::{Date, Month};

use harvester::{client::Client, fetch_many, selectors, write_dataset, Source};
use metadaten::dataset::{
    r#type::{Domain, Station, Type},
    Alternative, Dataset, GlobalIdentifier, Language, License, Organisation, OrganisationRole,
    Person, PersonRole, Region, Resource, ResourceType, TimeRange,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_observations(dir, client, source, selectors).await?;

    let (count1, results1, errors1) = fetch_meta(
        dir,
        client,
        source,
        "Probenparameter",
        "analytes",
        "analyte",
        &selectors.define_analyte,
    )
    .await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_meta(
        dir,
        client,
        source,
        "Probenart",
        "specimen_types",
        "specimen",
        &selectors.define_specimen,
    )
    .await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_meta(
        dir,
        client,
        source,
        "Probenahmegebiet",
        "sampling_areas",
        "area",
        &selectors.define_area,
    )
    .await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_observations(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("/measurements/metadata.rdf")?;

    let text = client
        .fetch_text(source, "observation".to_owned(), &url)
        .await?;

    let doc = Document::parse(&text)?;

    let rdf = from_doc::<Rdf>(&doc)?;

    let count = rdf.descriptions.len();

    let (results, errors) = fetch_many(0, 0, rdf.descriptions, |description| {
        translate_dataset(dir, client, source, selectors, description)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    description: Description<'_>,
) -> Result<(usize, usize, usize)> {
    let id = match selectors.about_observation.captures(description.about) {
        Some(captures) => captures.name("id").unwrap().as_str(),
        None => return Ok((1, 0, 0)),
    };

    let analyte = description
        .analyte
        .ok_or_else(|| anyhow!("Missing analyte for observation {id}"))?
        .description;

    let analyte_url = analyte.defined.resource;

    let analyte_label = german_text(id, &analyte.pref_labels)?;
    let analyte_note = german_text(id, &analyte.notes)?;

    let specimen = description
        .specimen
        .ok_or_else(|| anyhow!("Missing specimen for observation {id}"))?
        .description;

    let specimen_url = specimen.defined.resource;

    let specimen_label = german_text(id, &specimen.pref_labels)?;
    let specimen_note = german_text(id, &specimen.notes)?;

    let area = description
        .area
        .ok_or_else(|| anyhow!("Missing area for observation {id}"))?
        .description;

    let area_url = area.defined.resource;

    let area_label = german_text(id, &area.pref_labels)?;
    let area_note = german_text(id, &area.notes)?;

    let coord = if !area.lat.is_empty() && !area.long.is_empty() {
        Some(Coord {
            x: area.long.parse::<f64>()?,
            y: area.lat.parse::<f64>()?,
        })
    } else {
        None
    };

    let geo = if !area.geo_json.is_empty() {
        Some(GeoJson(area.geo_json).to_geo()?)
    } else {
        None
    };

    let amount = description
        .amount
        .ok_or_else(|| anyhow!("Missing amount for observation {id}"))?;

    let start_year = description
        .start_year
        .ok_or_else(|| anyhow!("Missing start year for observation {id}"))?;
    let end_year = description
        .end_year
        .ok_or_else(|| anyhow!("Missing end year for observation {id}"))?;

    let source_url = german_text(id, &description.source_url)?.to_owned();

    let alternatives = description
        .source_url
        .iter()
        .find(|translated| translated.lang == Some("en"))
        .map(|translated| translated.text.as_ref().to_owned())
        .map(|url| Alternative::Language {
            language: Language::English,
            url,
        })
        .into_iter()
        .collect();

    let title = format!(
        "Umweltprobenbank Nr. {}: {} / {} / {}",
        id, analyte_label, specimen_label, area_label
    );

    let description = format!(
        "Anzahl der Proben: {}\nGemessener Parameter: {}\n\nProbenart: {}\n{}\n\nProbenahmegebiet: {}\n{}",
        amount, analyte_note, specimen_label, specimen_note, area_label, area_note,
    );

    let resources = smallvec![
        Resource {
            r#type: ResourceType::RdfXml,
            url: source_url.replace("/results?", "/results.rdf?"),
            primary_content: true,
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::RdfXml,
            url: format!("{analyte_url}.rdf"),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            url: analyte_url.to_owned(),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::RdfXml,
            url: format!("{specimen_url}.rdf"),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            url: specimen_url.to_owned(),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::RdfXml,
            url: format!("{area_url}.rdf"),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            url: area_url.to_owned(),
            ..Default::default()
        },
    ];

    let region_name = selectors.region_extras.replace_all(area_label, "");

    let region = Region::Other(region_name.into());

    let bounding_boxes = coord
        .map(|coord| Rect::new(coord, coord))
        .into_iter()
        .chain(geo.and_then(|geo| geo.bounding_rect()).into_iter())
        .collect();

    let time_ranges = smallvec![TimeRange {
        from: Date::from_calendar_date(start_year, Month::January, 1)?,
        until: Date::from_calendar_date(end_year, Month::December, 31)?
    }];

    let dataset = Dataset {
        title,
        description: Some(description),
        types: smallvec![Type::Measurements {
            domain: Domain::Unspecified,
            station: Some(Station {
                id: Some(area_url.into()),
                ..Default::default()
            }),
            measured_variables: smallvec![format!("{analyte_label} in {specimen_label}")],
            methods: Default::default(),
        }],
        resources,
        regions: smallvec![region],
        bounding_boxes,
        time_ranges,
        persons: contact_info(),
        organisations: publisher_info(),
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url,
        machine_readable_source: true,
        alternatives,
        ..Default::default()
    };

    write_dataset(dir, client, source, id.to_string(), dataset).await
}

async fn fetch_meta(
    dir: &Dir,
    client: &Client,
    source: &Source,
    name: &str,
    key: &str,
    dataset_key: &str,
    dataset_id: &Regex,
) -> Result<(usize, usize, usize)> {
    let url = source
        .url
        .join(&format!("/de/documents/profiles/{key}.rdf"))?;

    let text = client.fetch_text(source, key.to_owned(), &url).await?;

    let doc = Document::parse(&text)?;

    let rdf = from_doc::<MetaRdf>(&doc)?;

    let count = rdf.descriptions.len();

    let (results, errors) = fetch_many(0, 0, rdf.descriptions, |description| {
        translate_meta_dataset(
            dir,
            client,
            source,
            name,
            dataset_key,
            dataset_id,
            description,
        )
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_meta_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    name: &str,
    key: &str,
    id: &Regex,
    description: MetaDescription<'_>,
) -> Result<(usize, usize, usize)> {
    let id = match id.captures(description.about) {
        Some(captures) => captures.name("id").unwrap().as_str(),
        None => return Ok((1, 0, 0)),
    };

    let global_identifier = GlobalIdentifier::Url(description.about.parse()?);

    let source_url = description.about.to_owned();

    let pref_label = german_text(id, &description.pref_labels)?;
    let note = german_text(id, &description.notes)?;

    let alt_labels = description
        .alt_labels
        .iter()
        .filter(|translated| translated.lang == Some("de"))
        .map(|translated| translated.text.as_ref())
        .join("\n");

    let title = format!("Umweltprobenbank {} Nr. {}: {}", name, id, pref_label);

    let description = format!("{}\n{}\nErläuterung: {}", pref_label, alt_labels, note);

    let station = if key == "area" {
        Some(Station {
            id: Some(id.into()),
            ..Default::default()
        })
    } else {
        None
    };

    let measured_variables = if key == "analyte" {
        smallvec![pref_label.to_owned()]
    } else {
        Default::default()
    };

    let types = smallvec![Type::Measurements {
        domain: Domain::Unspecified,
        station,
        measured_variables,
        methods: Default::default(),
    }];

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        global_identifier: Some(global_identifier),
        persons: contact_info(),
        organisations: publisher_info(),
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url,
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("{key}-{id}"), dataset).await
}

fn contact_info() -> Vec<Person> {
    vec![Person {
        name: "Fachgebiet II 1.2 Toxikologie, Gesundheitsbezogene Umweltbeobachtung".to_owned(),
        role: PersonRole::Contact,
        emails: smallvec!["upb@uba.de".to_owned()],
        ..Default::default()
    }]
}

fn publisher_info() -> SmallVec<[Organisation; 2]> {
    smallvec![Organisation::Other {
        name: "Umweltbundesamt".to_owned(),
        role: OrganisationRole::Publisher,
        websites: smallvec!["https://www.umweltbundesamt.de/".to_owned()],
    }]
}

#[derive(Debug, Deserialize)]
#[serde(rename = "RDF")]
struct Rdf<'a> {
    #[serde(rename = "Description", borrow)]
    descriptions: Vec<Description<'a>>,
}

#[derive(Debug, Deserialize)]
struct Description<'a> {
    #[serde(rename = "about")]
    about: &'a str,
    #[serde(borrow)]
    analyte: Option<Analyte<'a>>,
    #[serde(rename = "specimenType", borrow)]
    specimen: Option<Specimen<'a>>,
    #[serde(rename = "samplingArea", borrow)]
    area: Option<Area<'a>>,
    #[serde(rename = "timeSeriesAmount")]
    amount: Option<i32>,
    #[serde(rename = "startYear")]
    start_year: Option<i32>,
    #[serde(rename = "endYear")]
    end_year: Option<i32>,
    #[serde(rename = "investigationUrl", default)]
    source_url: SmallVec<[Translated<'a>; 2]>,
}

#[derive(Debug, Deserialize)]
#[serde(rename = "RDF")]
struct MetaRdf<'a> {
    #[serde(rename = "Description", borrow)]
    descriptions: Vec<MetaDescription<'a>>,
}

#[derive(Debug, Deserialize)]
struct MetaDescription<'a> {
    #[serde(rename = "about")]
    about: &'a str,
    #[serde(rename = "prefLabel", borrow)]
    pref_labels: SmallVec<[Translated<'a>; 2]>,
    #[serde(rename = "altLabel", borrow)]
    alt_labels: Vec<Translated<'a>>,
    #[serde(rename = "note", borrow)]
    notes: SmallVec<[Translated<'a>; 2]>,
}

#[derive(Debug, Deserialize)]
struct Analyte<'a> {
    #[serde(rename = "Description", borrow)]
    description: AnalyteDescription<'a>,
}

#[derive(Debug, Deserialize)]
struct AnalyteDescription<'a> {
    #[serde(rename = "prefLabel", borrow)]
    pref_labels: SmallVec<[Translated<'a>; 2]>,
    #[serde(rename = "note", borrow)]
    notes: SmallVec<[Translated<'a>; 2]>,
    #[serde(rename = "isDefinedBy", borrow)]
    defined: Defined<'a>,
}

#[derive(Debug, Deserialize)]
struct Specimen<'a> {
    #[serde(rename = "Description", borrow)]
    description: SpecimenDescription<'a>,
}

#[derive(Debug, Deserialize)]
struct SpecimenDescription<'a> {
    #[serde(rename = "prefLabel", borrow)]
    pref_labels: SmallVec<[Translated<'a>; 2]>,
    #[serde(rename = "note", borrow)]
    notes: SmallVec<[Translated<'a>; 2]>,
    #[serde(rename = "isDefinedBy", borrow)]
    defined: Defined<'a>,
}

#[derive(Debug, Deserialize)]
struct Area<'a> {
    #[serde(rename = "Description", borrow)]
    description: AreaDescription<'a>,
}

#[derive(Debug, Deserialize)]
struct AreaDescription<'a> {
    #[serde(rename = "prefLabel", borrow)]
    pref_labels: SmallVec<[Translated<'a>; 2]>,
    #[serde(rename = "note", borrow)]
    notes: SmallVec<[Translated<'a>; 2]>,
    lat: &'a str,
    long: &'a str,
    #[serde(rename = "geoJSONLiteral")]
    geo_json: &'a str,
    #[serde(rename = "isDefinedBy", borrow)]
    defined: Defined<'a>,
}

#[derive(Debug, Deserialize)]
struct Translated<'a> {
    #[serde(rename = "lang")]
    lang: Option<&'a str>,
    #[serde(rename = "$text", borrow, default)]
    text: Cow<'a, str>,
}

fn german_text<'a>(id: &str, translated: &'a [Translated<'a>]) -> Result<&'a str> {
    translated
        .iter()
        .find(|translated| translated.lang == Some("de"))
        .map(|translated| translated.text.as_ref())
        .ok_or_else(|| anyhow!("Missing German text for {id}"))
}

#[derive(Debug, Deserialize)]
struct Defined<'a> {
    #[serde(rename = "resource")]
    resource: &'a str,
}

selectors! {
    about_observation: r"/measurements/metadata\.rdf#os\-(?<id>\d+)" as Regex,
    define_analyte: r"/analyte\-?(?<id>[[:alnum:]]+)" as Regex,
    define_specimen: r"/specimenType\-?(?<id>[[:alnum:]]+)" as Regex,
    define_area: r"/samplingArea\-?(?<id>\d+)" as Regex,
    region_extras: r"(\s+\(km \d+(,\d+)?\))|((\-)|(\s+)\(?(Fichte)|(Buche)|(Regenwurm)\)?$)|(^SB\-)" as Regex,
}
