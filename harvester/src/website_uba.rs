use std::iter::successors;

use anyhow::{anyhow, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::HashSet;
use regex::RegexSet;
use scraper::{Element, ElementRef, Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationKey, OrganisationRole, Region, Resource,
    ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let all_pages = AtomicRefCell::new(HashSet::new());

    let (results, errors) = fetch_links(
        client,
        source,
        0,
        0,
        &all_pages,
        SubPage {
            path: "umwelttipps-fuer-den-alltag/",
            selector: &selectors.tipps,
            theme_selector: Some(&selectors.subpages_tipps),
            num_selector: None,
        },
    )
    .await?;

    let (results, errors) = fetch_links(
        client,
        source,
        results,
        errors,
        &all_pages,
        SubPage {
            path: "umweltzustand-trends/",
            selector: &selectors.zustand_trends,
            theme_selector: Some(&selectors.subpages_zustand_trends),
            num_selector: Some(&selectors.page_number_zustand_trends),
        },
    )
    .await?;

    let (results, errors) = fetch_links(
        client,
        source,
        results,
        errors,
        &all_pages,
        SubPage {
            path: "themen/",
            selector: &selectors.themen,
            theme_selector: Some(&selectors.subpages_themen),
            num_selector: Some(&selectors.page_number_themen),
        },
    )
    .await?;

    let (results, errors) = fetch_links(
        client,
        source,
        results,
        errors,
        &all_pages,
        SubPage {
            path: "daten/umweltindikatoren",
            selector: &selectors.indikatoren,
            theme_selector: None,
            num_selector: None,
        },
    )
    .await?;

    let (results, errors) = fetch_links(
        client,
        source,
        results,
        errors,
        &all_pages,
        SubPage {
            path: "presse/downloads",
            selector: &selectors.downloads,
            theme_selector: Some(&selectors.subpages_downloads),
            num_selector: None,
        },
    )
    .await?;

    let all_pages = all_pages.into_inner();

    let count = all_pages.len();

    let (results, errors) = fetch_many(results, errors, all_pages, |page| {
        fetch_page(dir, client, source, selectors, page)
    })
    .await;

    let (results, errors) =
        fetch_scrollytelling(dir, client, source, selectors, results, errors).await?;

    Ok((count, results, errors))
}

struct SubPage<'a> {
    path: &'a str,
    selector: &'a Selector,
    theme_selector: Option<&'a Selector>,
    num_selector: Option<&'a Selector>,
}

async fn fetch_scrollytelling(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    results: usize,
    errors: usize,
) -> Result<(usize, usize)> {
    let url = source.url.join("/stories")?;

    let key = "stories".to_owned();

    let text = client.fetch_text(source, key, &url).await?;
    let document = Html::parse_document(&text);

    let links = document
        .select(&selectors.stories)
        .map(|elem| elem.attr("href").unwrap())
        .map(|href| href.to_owned())
        .collect::<Vec<_>>();

    let (results, errors) = fetch_many(results, errors, links, |link| async move {
        write_scrollytelling(dir, client, source, selectors, link).await
    })
    .await;

    Ok((results, errors))
}

async fn write_scrollytelling(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: String,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&link).into_owned();

    let url = source.url.join(&link)?;

    let text = client
        .fetch_text(source, format!("{}.isol", key), &url)
        .await?;
    let document = Html::parse_document(&text);

    let mut title = select_text(&document, &selectors.title_stories);
    let subtitle = select_text(&document, &selectors.subtitle_stories);
    title = format! {"{}: {} - Scrollytelling Umweltbundesamt", title, subtitle};

    let description = select_text(&document, &selectors.description_stories);

    let resources = document
        .select(&selectors.resources_stories)
        .map(|element| {
            let href = element.attr("href").unwrap();
            Ok(Resource {
                r#type: ResourceType::WebPage,
                url: source.url.join(href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect::<Result<SmallVec<_>>>()?;

    let types = smallvec![Type::Text {
        text_type: TextType::Editorial
    }];

    let dataset = Dataset {
        title,
        description: Some(description),
        organisations: smallvec![ORGANISATION_UBA],
        types,
        origins: source.origins.clone(),
        license: License::AllRightsReserved,
        source_url: url.into(),
        language: Language::German,
        resources,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_links(
    client: &Client,
    source: &Source,
    results: usize,
    errors: usize,
    all_pages: &AtomicRefCell<HashSet<String>>,
    sub_page: SubPage<'_>,
) -> Result<(usize, usize)> {
    let url = source.url.join(sub_page.path)?;

    let links = {
        let key = make_key(sub_page.path).into_owned();

        let text = client.fetch_text(source, key, &url).await?;
        let document = Html::parse_document(&text);

        document
            .select(sub_page.selector)
            .map(|elem| elem.attr("href").unwrap())
            .map(|href| href.to_owned())
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(results, errors, links, |page_link| async move {
        let url = &source.url.join(&page_link)?;
        let key = &make_key(&page_link).into_owned();

        all_pages.borrow_mut().insert(page_link);

        let mut last_pages = {
            let text = client.fetch_text(source, key.clone(), url).await?;
            let document = Html::parse_document(&text);

            if let Some(theme_selector) = sub_page.theme_selector {
                all_pages.borrow_mut().extend(
                    document
                        .select(theme_selector)
                        .map(|elem| elem.attr("href").unwrap().to_owned()),
                )
            };

            if let Some(num_selector) = sub_page.num_selector {
                document
                    .select(num_selector)
                    .map(|elem| elem.attr("href").unwrap().to_owned())
                    .collect::<Vec<_>>()
            } else {
                // for "umwelttipps-fuer-den-alltag" there is always only one page, so it is sufficient to collect only this page and not required to iterate over several pages
                Vec::new()
            }
        };

        last_pages.sort_unstable();
        last_pages.dedup();

        let count = last_pages.len();

        let (results, errors) = fetch_many(0, 0, last_pages, |last_page| async move {
            let num_of_pages = last_page
                .rsplit_once('=')
                .ok_or_else(|| anyhow!("Malformed last page: {last_page}"))?
                .1
                .parse()?;

            for current_page in 1..=num_of_pages {
                let mut current_url = url.clone();

                current_url
                    .query_pairs_mut()
                    .append_pair("page", &current_page.to_string());

                let text = client
                    .fetch_text(source, format!("{}-{}", key, &current_page), &current_url)
                    .await?;
                let document = Html::parse_document(&text);

                if let Some(theme_selector) = sub_page.theme_selector {
                    all_pages.borrow_mut().extend(
                        document
                            .select(theme_selector)
                            .map(|elem| elem.attr("href").unwrap().to_owned()),
                    )
                };
            }

            Ok((1, 1, 0))
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: String,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&page)?;
    let key = make_key(&page).into_owned();

    let dataset = {
        let text = client
            .fetch_text(source, format!("{}.isol", key), &url)
            .await?;
        let document = Html::parse_document(&text);

        let title = match document.select(&selectors.title).next() {
            Some(title) => title
                .value()
                .attr("content")
                .ok_or_else(|| anyhow!("Missing content for og:title meta"))?
                .to_owned(),
            None => {
                tracing::debug!("Ignoring {url} due to missing title");
                return Ok((1, 0, 1));
            }
        };

        let description = select_text(&document, &selectors.description);

        let region = document
            .select(&selectors.region)
            .next()
            .ok_or_else(|| anyhow!("Missing region for {page}"))?
            .value()
            .attr("content")
            .ok_or_else(|| anyhow!("Missing content for og:locality meta"))?;

        let date = document
            .select(&selectors.date)
            .next()
            .map(|elem| collect_text(elem.text()));
        let date = date
            .map(|date| Date::parse(&date, format_description!("[day].[month].[year]")))
            .transpose()?;

        // TODO: implement scraping for in-text links and add them as resource or make them clickable in our description
        let mut resources = document
            .select(&selectors.resources)
            .map(|element| {
                let href = element.attr("href").unwrap();

                let description = extract_figcaption(selectors, element)
                    .or_else(|| extract_title(element))
                    .or_else(|| extract_text(element));

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description,
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        let type_matched = selectors.type_matcher.matches(url.path());

        if type_matched.matched(0) {
            // "/presse/downloads/pressebilder"
            for element in document.select(&selectors.resources_pressimages) {
                for text in element.select(&selectors.pressimages_link) {
                    let href = text.attr("href").unwrap();

                    let description = select_text(element, &selectors.pressimages_figcap);

                    resources.push(
                        Resource {
                            r#type: ResourceType::Image,
                            description: Some(description),
                            url: source.url.join(href)?.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            }
        }

        let license = if type_matched.matched(2) {
            // "/presse/downloads/videos",
            License::CcBy30
        } else {
            License::AllRightsReserved
        };

        let types = type_matched
            .into_iter()
            .map(|r#type| match r#type {
                0 | 1 => Type::Image,
                2 => Type::Video,
                3 => Type::Audio,
                4 => Type::Text {
                    text_type: TextType::PressRelease,
                },
                5 => Type::Text {
                    text_type: TextType::Publication,
                },
                6 => Type::Software,
                7 => Type::Text {
                    text_type: TextType::Editorial,
                },
                _ => Type::Text {
                    text_type: TextType::Editorial,
                },
            })
            .collect();

        Dataset {
            title,
            description: Some(description),
            organisations: smallvec![ORGANISATION_UBA],
            types,
            origins: source.origins.clone(),
            license,
            source_url: url.clone().into(),
            language: Language::German,
            regions: smallvec![Region::Other(region.into())],
            issued: date,
            resources,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

fn extract_figcaption(selectors: &Selectors, link: ElementRef) -> Option<String> {
    successors(link.parent_element(), Element::parent_element)
        .find(|element| element.value().name() == "figcaption")
        .and_then(|element| element.select(&selectors.figcaption_value).next())
        .map(|element| collect_text(element.text()))
}

fn extract_title(link: ElementRef) -> Option<String> {
    link.attr("title").map(|title| {
        title
            .strip_prefix("Gehe zu ")
            .unwrap_or(title)
            .trim_matches('"')
            .to_owned()
    })
}

fn extract_text(link: ElementRef) -> Option<String> {
    let text = collect_text(link.text());
    if !text.is_empty() {
        Some(text)
    } else {
        None
    }
}

const ORGANISATION_UBA: Organisation = Organisation::WikiData {
    identifier: OrganisationKey::UBA,
    role: OrganisationRole::Publisher,
};

selectors! {
    themen: "div.theme-box a[href^='/']",
    tipps: "div.theme-icons a[href^='/']",
    indikatoren: "div.row-fluid div.content-box a[href^='/daten/umweltindikatoren']",
    stories: "div.article-content a[href^='https://stories']",
    downloads: "div.article-content a[href]",
    zustand_trends: "div a[class='w21peb-tile-link'][href^='/']",
    subpages_themen: "article.clearfix a[href^='/'], aside ul ul li a[href^='/']",
    subpages_tipps: "div.styled-list-content a[href^='/']",
    subpages_downloads: "div.article-content  a[href*='/themen/'], div.article-content p a[href^='/'], div.slider-content figure a[href]",
    subpages_zustand_trends: "article.clearfix a[href^='/'], aside ul ul li a[href^='/'], h5 a[href^='/']",
    title: "meta[property='og:title']",
    title_stories: "div.stage__text h1, div.stage__text h1",
    subtitle_stories: "div.stage__headline div.field__content",
    description: "#main-content div.article-content li, #main-content div.article-content p, #main-content div.article-content h2, h5 a[title], #main-content div.styled-list li, #main-content div.no-background a, #main-content div.teaser, #main-content div.article-text",
    description_stories: "div.field div.field__content p:not(a[href])",
    region: "meta[property='og:locality']",
    date: "div.article-meta time[datetime]",
    resources: "div.styled-list-content span a[href], div.content-box span a[href], p+ ul a[href]:not(.tooltip-link), .article-content p a[href]:not(.tooltip-link), div.article-content h3 a[href], div.row-fluid ul li p span a[href]",
    resources_pressimages: "div.row-fluid div.article-content ul li figure",
    resources_stories: "div.field__content a[href^='https://']",
    pressimages_link: "a[href]",
    pressimages_figcap: "figcaption",
    // the second selector is for "/themen/digitalisierung", which contains only 2 pages and thus does not have the class = 'pager-total'
    page_number_themen: "li[class='pager-total'] a[href], li[class='next last'] a[aria-label='Zur letzten Seite']",
    page_number_zustand_trends: "li a[aria-label='Zur letzten Seite']",
    figcaption_value: "strong",

    type_matcher: r"/presse/downloads/pressebilder",
    r"/presse/downloads/infografiken",
    r"/presse/downloads/videos",
    r"/presse/downloads/presse-o-toene",
    r"/presse/pressemitteilungen",
    r"/publikationen",
    r"/themen/.*\b(gw)?app",
    r"/"  as RegexSet,

}
