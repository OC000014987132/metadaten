use std::cmp::Ordering;
use std::sync::atomic::{AtomicUsize, Ordering as AtomicOrdering};

use anyhow::{anyhow, ensure, Error, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use cow_utils::CowUtils;
use hashbrown::HashMap;
use itertools::Itertools;
use poppler::Document as PdfDocument;
use regex::{Regex, RegexSet};
use scraper::{Element, Html, Selector};
use serde::Serialize;
use smallvec::smallvec;
use time::Date;
use url::Url;

use harvester::{
    client::{Client, HttpRequestBuilderExt, HttpResponseExt},
    fetch_many, selectors,
    utilities::{
        collect_pages, collect_text, make_suffix_key, parse_attribute, parse_text, parse_texts,
        select_text, yesterday, GermanDate,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, LanguageDetector, License, Person, PersonRole, Region, Resource,
    ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, results, errors) = fetch_sitemap(dir, client, source, selectors).await?;

    let presse_items = fetch_press_links(client, source, selectors).await?;

    count += presse_items.len();

    let (results, errors) = fetch_many(results, errors, presse_items, |press_item| {
        write_datasets_sitemap(dir, client, source, selectors, press_item)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_sitemap(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let source_url = source.url.join("/startseite/service/sitemap/")?;

    let mut links = {
        let text = client
            .fetch_text(source, "sitemap_links".to_owned(), &source_url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.sitemap_link_selector)
            .filter_map(|element| {
                let href = element.attr("href").unwrap();
                let title = collect_text(element.text());

                if selectors.exclude_pages.is_match(&title) {
                    return None;
                }

                Some(Item {
                    title,
                    href: href.to_owned(),
                    date: None,
                })
            })
            .collect::<Vec<_>>()
    };

    links.sort_unstable_by(|lhs, rhs| lhs.href.cmp(&rhs.href));
    links.dedup_by(|lhs, rhs| lhs.href == rhs.href);

    let count = &AtomicUsize::new(links.len());

    let language_detector = &LanguageDetector::build([
        Language::German,
        Language::English,
        Language::Persian,
        Language::Arabic,
        Language::Dutch,
        Language::French,
    ]);

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        if selectors.pdf_collections.is_match(&link.href) {
            let (pdf_count, results, errors) =
                write_datasets_pdf_links(dir, client, source, selectors, language_detector, link)
                    .await?;

            count.fetch_add(pdf_count, AtomicOrdering::Relaxed);

            Ok((pdf_count, results, errors))
        } else {
            write_datasets_sitemap(dir, client, source, selectors, link).await
        }
    })
    .await;

    Ok((count.load(AtomicOrdering::Relaxed), results, errors))
}

#[derive(Debug, PartialEq, Eq, Default, PartialOrd, Ord)]
struct Item {
    title: String,
    href: String,
    date: Option<Date>,
}

async fn write_datasets_pdf_links(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    language_detector: &LanguageDetector,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_suffix_key(&item.href, &source.url).into_owned();

    let source_url = &Url::parse(&item.href).unwrap();

    let pdf_items = {
        let text = client
            .fetch_text(source, format!("{key}.isol"), source_url)
            .await?;
        let document = Html::parse_document(&text);

        document
            .select(&selectors.download_links)
            .filter_map(|element| {
                let title = selectors
                    .shorten_title
                    .replace_all(&collect_text(element.text()), "")
                    .into_owned();

                if selectors.exclude_pages.is_match(&title) {
                    return None;
                }

                let href = element.attr("href").unwrap().to_owned();

                Some(Item {
                    title,
                    href,
                    date: None,
                })
            })
            .collect::<Vec<Item>>()
    };

    let count = pdf_items.len();

    let (results, errors) = fetch_many(0, 0, pdf_items, |item| async move {
        let href = &item.href;
        let mut title = item.title;

        let key = make_suffix_key(href, &source.url).into_owned();
        let url = source.url.join(href)?;

        let types = smallvec![if title.contains("Kinder") {
            Type::LearningResource
        } else {
            Type::Text {
                text_type: TextType::Publication,
            }
        }];

        let description = if url.path().ends_with(".pdf") {
            let bytes = client
                .fetch_pdf(source, format!("pdf-{}.isol", key), &url)
                .await?;

            let document = PdfDocument::from_bytes(&bytes, None)?;

            if let Some(pdf_title) = document.title() {
                if title.is_empty() {
                    tracing::debug!("healing empty title using PDF metadata: '{pdf_title}'");
                    title = pdf_title.into();
                }
            }

            Some(collect_pages(&document, 0..3))
        } else {
            None
        };

        let mut dataset = Dataset {
            title,
            description,
            types,
            source_url: url.into(),
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            ..Default::default()
        };

        language_detector.guess_or(&mut dataset, Language::German);

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn write_datasets_sitemap(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_suffix_key(&item.href, &source.url).into_owned();
    let url = source.url.join(&item.href)?;

    let text = client
        .fetch_text(source, format!("{key}.isol"), &url)
        .await?;
    let document = Html::parse_document(&text);

    let breadcrumb = select_text(&document, &selectors.breadcrumb_selector);

    let r#type = if breadcrumb.contains("Startseite Aktuelles Veranstaltungen ") {
        Type::Event
    } else if breadcrumb.contains("Ausleihbare Ausstellungen") {
        Type::LearningResource
    } else if breadcrumb.contains("Förderprogramme") {
        Type::SupportProgram
    } else if breadcrumb.contains("Jahresberichte") {
        Type::Text {
            text_type: TextType::Report,
        }
    } else if breadcrumb.contains("Veröffentlichungen") {
        Type::Text {
            text_type: TextType::Publication,
        }
    } else if breadcrumb.contains("Pressemitteilungen") {
        Type::Text {
            text_type: TextType::PressRelease,
        }
    } else {
        Type::Text {
            text_type: TextType::Editorial,
        }
    };

    let language = if url.path().contains("information_in_english")
        || url.path().contains("english_version")
    {
        Language::English
    } else if url.path().contains("informatie_in_het_nederlands") {
        Language::Dutch
    } else {
        Language::German
    };

    let mut title = select_text(&document, &selectors.link_title);

    if title.is_empty() {
        title = item.title;
    }

    if count_words(selectors, &title) < 4 {
        title = format!(
            "{}: {}",
            selectors
                .words
                .split(&breadcrumb.cow_replace(&title, ""))
                .skip(2)
                .join(" "),
            title
        );
    };

    let description = select_text(&document, &selectors.link_description);

    let mut contacts = {
        let creator = select_text(&document, &selectors.creators);

        if creator.starts_with("Von ") {
            creator
                .replace("Von ", "")
                .replace(" und ", ", ")
                .split(',')
                .map(|name| Person {
                    name: name.trim().to_owned(),
                    role: PersonRole::Creator,
                    ..Default::default()
                })
                .collect::<Vec<_>>()
        } else {
            Default::default()
        }
    };

    let mut resources = Vec::new();
    let mut issued = Option::<Date>::None;
    let mut modified = Option::<Date>::None;

    for graue_box in document.select(&selectors.gray_box) {
        issued = issued.or_else(|| {
            parse_texts::<_, GermanDate>(
                graue_box,
                &selectors.paragraphs,
                &selectors.re_issued,
                "issued",
            )
            .filter_map(Result::ok)
            .next()
            .map(Into::into)
        });

        modified = modified.or_else(|| {
            parse_texts::<_, GermanDate>(
                graue_box,
                &selectors.paragraphs,
                &selectors.re_modified,
                "modified",
            )
            .filter_map(Result::ok)
            .next()
            .map(Into::into)
        });

        for element in graue_box.select(&selectors.link) {
            let href = element.attr("href").unwrap();
            let description = collect_text(element.text());

            if description.contains("E-Mail an Ansprechpartner/in") {
                let name = graue_box
                    .select(&selectors.paragraphs)
                    .map(|elem| collect_text(elem.text()))
                    .find(|name| name.contains("Ansprechpartner"))
                    .unwrap_or("Kontaktformular".to_owned());

                let contact_form = element.attr("href").unwrap();
                let contact_form = source
                    .url
                    .join(contact_form)
                    .map_or_else(|_| contact_form.to_owned(), |url| url.into());

                contacts.push(Person {
                    name,
                    role: PersonRole::Contact,
                    forms: smallvec!(contact_form.to_owned()),
                    ..Default::default()
                });
            } else {
                let description = selectors
                    .clean_link_titles
                    .replace_all(&description, "")
                    .into_owned();

                resources.push(
                    Resource {
                        r#type: ResourceType::WebPage,
                        description: Some(description),
                        url: source
                            .url
                            .join(href)
                            .map_or_else(|_| href.to_owned(), |url| url.into()),
                        ..Default::default()
                    }
                    .guess_or_keep_type(),
                );
            }
        }
    }

    for element in document.select(&selectors.link_resources) {
        let href = element.attr("href").unwrap();
        if href.contains("#weiterlesen") {
            continue;
        }

        let description = if let Some(title) = element.attr("title") {
            title.to_owned()
        } else if let Some(parent) = element.parent_element() {
            collect_text(parent.text())
        } else {
            collect_text(element.text())
        };

        let r#type = if href.contains("assets/image") {
            ResourceType::Image
        } else if href.contains("download") {
            ResourceType::Document
        } else {
            ResourceType::WebPage
        };

        if let Some(mail_capture) = selectors.re_name_from_email.captures(href) {
            contacts.push(Person {
                name: mail_capture[1].to_owned(),
                role: PersonRole::Unknown,
                emails: smallvec![mail_capture[2].to_owned()],
                ..Default::default()
            })
        } else {
            let description = selectors
                .clean_link_titles
                .replace_all(&description, "")
                .into_owned();

            resources.push(
                Resource {
                    r#type,
                    description: Some(description),
                    url: source
                        .url
                        .join(href)
                        .map_or_else(|_| href.to_owned(), |url| url.into()),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            )
        };
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        types: smallvec![r#type],
        resources: resources.into(),
        regions: smallvec![Region::NI],
        language,
        origins: source.origins.clone(),
        persons: contacts,
        source_url: item.href,
        license: License::AllRightsReserved,
        issued,
        modified,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_press_links(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<Item>> {
    let cookies = &AtomicRefCell::new(HashMap::new());

    set_press_date(client, source, selectors, cookies).await?;

    let mut batch_size = 30;
    let mut offset = 0;

    let mut press_releases = Vec::new();

    loop {
        let mut press_release_set =
            fetch_press_page(client, source, selectors, cookies, offset).await?;

        let batch_count = press_release_set.len() as u32;

        press_releases.append(&mut press_release_set);

        match batch_count.cmp(&batch_size) {
            Ordering::Greater => {
                batch_size = batch_count;
                offset += batch_count;
            }
            Ordering::Equal => {
                offset += batch_count;
            }
            Ordering::Less => {
                break;
            }
        }
    }

    press_releases.sort_unstable_by(|lhs, rhs| lhs.href.cmp(&rhs.href));
    press_releases.dedup_by(|lhs, rhs| lhs.href == rhs.href);

    Ok(press_releases)
}

async fn set_press_date(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    cookies: &AtomicRefCell<HashMap<CompactString, String>>,
) -> Result<()> {
    let startdate = Date::from_ordinal_date(2000, 1).unwrap();

    let url = source
        .url
        .join("startseite/aktuelles/presse_und_offentlichkeitsarbeit/pressemitteilungen/")?;

    #[derive(Serialize)]
    struct Params {
        startdate: Date,
        enddate: Date,
        search: &'static str,
        institution: i32,
    }

    let text = client
        .make_request(
            source,
            "press-set-date".to_owned(),
            Some(&url),
            |client| async {
                client
                    .post(url.clone())
                    .form(&Params {
                        startdate,
                        enddate: yesterday(),
                        search: "press",
                        institution: 26,
                    })
                    .send()
                    .await?
                    .error_for_status()?
                    .set_cookies(&mut cookies.borrow_mut())?
                    .text()
                    .await
                    .map_err(Error::from)
            },
        )
        .await?;

    let document = Html::parse_document(&text);

    let startdate_set: Date = parse_attribute::<_, GermanDate>(
        &document,
        &selectors.press_results_startdate,
        &selectors.german_date,
        "value",
        "Search form Start Date",
    )?
    .into();

    ensure!(
        startdate_set == startdate,
        "Failed to set search form start date"
    );

    Ok(())
}

async fn fetch_press_page(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    cookies: &AtomicRefCell<HashMap<CompactString, String>>,
    offset: u32,
) -> Result<Vec<Item>> {
    let url = source.url.join("/assets/scripts/searchLoader.php")?;

    #[derive(Serialize)]
    struct Params {
        r#type: &'static str,
        #[serde(rename = "rangeStart")]
        range_start: u32,
    }

    let text = client
        .make_request(
            source,
            format!("press-with-offset-{offset}"),
            Some(&url),
            |client| async {
                client
                    .post(url.clone())
                    .form(&Params {
                        r#type: "press",
                        range_start: offset,
                    })
                    .cookies(&cookies.borrow())
                    .send()
                    .await?
                    .error_for_status()?
                    .text()
                    .await
                    .map_err(Error::from)
            },
        )
        .await?;

    let document = Html::parse_fragment(&text);

    let press_releases = document
        .select(&selectors.press_results)
        .map(|element| {
            let href = element
                .select(&selectors.link)
                .next()
                .ok_or_else(|| anyhow!("no href found"))?
                .attr("href")
                .unwrap()
                .to_owned();

            let title = select_text(element, &selectors.press_results_title);

            let date = parse_text::<_, GermanDate>(
                element,
                &selectors.press_results_date,
                &selectors.german_date,
                "Datum",
            )
            .ok()
            .map(Into::into);

            Ok(Item { href, title, date })
        })
        .collect::<Result<Vec<_>>>()?;

    Ok(press_releases)
}

fn count_words(selectors: &Selectors, text: &str) -> usize {
    selectors
        .words
        .split(text)
        .filter(|word| !word.is_empty())
        .count()
}

selectors! {
    sitemap_link_selector: "ul.level_02 a[href]",

    breadcrumb_selector : ".breadcrumb",

    link_title: "div.group.main.span3of4 h2.themen,
                 div.group.section.span3of4 h1.margt0",

    link_description: "div.group.section.span3of4 div[class='col span3of4 cline'] p,
                       div.group.section.span3of4 div[class='col span3of4 cline'] b,
                       div.group.section.span3of4 div.group.section.noline.span3of4 p,
                       div.group.section.span3of4 div.group.section.noline.span3of4 b",

    gray_box: "div.span1of4 div.content",
    link: "a[href]",
    paragraphs: "p",

    images: ".img",
    figcaption_value: ".picinfo",

    link_resources: "div.group.section.span3of4 div.content h3.margt0 a[href],
                     ul.linkliste a[href], ul.downloadliste a[href], 
                     div.group.section.noline.span3of4.ftext a[href]",

    exclude_pages: r"Sitemap",
        r"Pressemitteilung",
        r"Impressum",
        r"Anreise",
        r"Karriere",
        r"WebShop",
        r"Webshop" as RegexSet,

    re_issued: r"erstellt am:\s+(\d{2}\.\d{2}\.\d{4})" as Regex,
    re_modified: r"zuletzt aktualisiert am:\s+(\d{2}\.\d{2}\.\d{4})" as Regex,

    pdf_collections: r"schriften_zum_downloaden" as Regex,

    download_links: ".articleContent .download[href]:not([target='_self'])",

    shorten_title: r" - Download.*" as Regex,
    clean_link_titles:  r"(Link zu:)?(\(nicht vollständig barrierefrei\))?(\(.+\,?.*MB\))?" as Regex,

    creators: "div.group.section.span3of4 > p.c2",

    press_results: r".singleResult",
    press_results_startdate: r"#press div span input",
    press_results_date: r".datum",
    press_results_title: r"strong",
    german_date: r"(\d{2}\.\d{2}\.\d{4})" as Regex,
    re_name_from_email: r"mailto:((.*)@\S*)" as Regex,

    words: r"\W+" as Regex,
}
