use std::env::var_os;
use std::path::PathBuf;
use std::process::Stdio;

use anyhow::{ensure, Context, Result};
use cap_std::fs::Dir;
use serde::{Deserialize, Serialize};
use serde_json::{from_slice, to_writer};
use tokio::{
    io::{AsyncBufReadExt, AsyncWriteExt, BufReader},
    process::{ChildStdin, ChildStdout, Command},
};
use url::Url;

use harvester::{client::Client, write_dataset, Source};
use metadaten::{data_path_from_env, dataset::IdentifiedDataset};

pub async fn harvest(
    dir: &Dir,
    client: &Client,
    source: &Source,
    name: &str,
) -> Result<(usize, usize, usize)> {
    let data_path = data_path_from_env();
    let external_path = external_path_from_env();

    let program = external_path.join(name).canonicalize()?;

    let mut child = Command::new(program)
        .current_dir(data_path)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .kill_on_drop(true)
        .spawn()
        .context("Failed to spawn external harvester")?;

    tracing::info!(
        "Spawned external harvester {name} with PID {:?}",
        child.id()
    );

    let mut stdin = child.stdin.take().unwrap();
    let mut stdout = BufReader::new(child.stdout.take().unwrap());
    let mut buf = Vec::new();

    Outgoing::Config(source).write(&mut stdin, &mut buf).await?;

    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    while let Some(msg) = Incoming::read(&mut stdout, &mut buf).await? {
        match msg {
            Incoming::Dataset(dataset) => {
                tracing::trace!("Writing dataset {} for external harvester", dataset.id);
                match write_dataset(dir, client, source, dataset.id, dataset.value).await {
                    Ok((count1, results1, errors1)) => {
                        count += count1;
                        results += results1;
                        errors += errors1;
                    }
                    Err(err) => {
                        tracing::error!("{:#}", err);

                        errors += 1;
                    }
                }
            }
            Incoming::Error(error) => {
                tracing::error!("{}", error);

                errors += 1;
            }
            Incoming::FetchText {
                mut key,
                url,
                isolated,
            } => {
                tracing::trace!("Fetching text of {url} for external harvester");

                if isolated {
                    key.push_str(".isol");
                }

                let res = client.fetch_text(source, key, &url).await;

                let msg = match &res {
                    Ok(text) => Outgoing::Text(text),
                    Err(err) => Outgoing::Error(err.to_string()),
                };

                msg.write(&mut stdin, &mut buf).await?;
            }
        }
    }

    drop(stdin);
    drop(stdout);

    let status = child.wait().await?;

    ensure!(
        status.success(),
        "External harvesters failed with status: {status:?}"
    );

    Ok((count, results, errors))
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
enum Incoming {
    Dataset(IdentifiedDataset),
    Error(String),
    FetchText {
        key: String,
        url: Url,
        isolated: bool,
    },
}

impl Incoming {
    async fn read(stdin: &mut BufReader<ChildStdout>, buf: &mut Vec<u8>) -> Result<Option<Self>> {
        buf.clear();

        if stdin.read_until(b'\n', buf).await? == 0 {
            return Ok(None);
        }

        let msg = from_slice(buf)?;

        Ok(msg)
    }
}

#[derive(Debug, Serialize)]
enum Outgoing<'a> {
    Config(&'a Source),
    Text(&'a str),
    Error(String),
}

impl Outgoing<'_> {
    async fn write(&self, stdin: &mut ChildStdin, buf: &mut Vec<u8>) -> Result<()> {
        buf.clear();
        to_writer(&mut *buf, self)?;
        buf.push(b'\n');

        stdin.write_all(buf).await?;

        Ok(())
    }
}

fn external_path_from_env() -> PathBuf {
    var_os("EXTERNAL_PATH")
        .expect("Environment variable EXTERNAL_PATH not set")
        .into()
}
