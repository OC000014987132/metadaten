use anyhow::{anyhow, ensure, Result};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, parse_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, GlobalIdentifier, License, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();

    let rpp = source.batch_size;

    let (count, results, errors) = fetch_datasets(dir, client, source, &selectors, rpp, 0).await?;
    tracing::info!("Harvesting {} datasets", count);

    let requests = count.div_ceil(rpp);
    let offset = (1..requests).map(|request| request * rpp);

    let (results, errors) = fetch_many(results, errors, offset, |offset| {
        fetch_datasets(dir, client, source, &selectors, rpp, offset)
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip(dir, client, source, selectors))]
async fn fetch_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    rpp: usize,
    offset: usize,
) -> Result<(usize, usize, usize)> {
    tracing::debug!("Fetching {} datasets starting at {}", rpp, offset);

    let count;
    let handles;

    {
        let mut url = source.url.join("/jspui/browse")?;

        url.query_pairs_mut()
            .append_pair("rpp", &rpp.to_string())
            .append_pair("offset", &offset.to_string());

        let body = client
            .fetch_text(source, format!("browse-{offset}"), &url)
            .await?;

        let document = Html::parse_document(&body);

        count = parse_text(
            &document,
            &selectors.range,
            &selectors.range_count,
            "number of documents",
        )?;

        handles = document
            .select(&selectors.handle)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>();
    }

    ensure!(
        !handles.is_empty(),
        "Could not parse handles at offset {}",
        offset
    );

    let (results, errors) = fetch_many(0, 0, handles, |handle| {
        fetch_dataset(dir, client, source, selectors, handle)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    handle: String,
) -> Result<(usize, usize, usize)> {
    tracing::debug!("Fetching dataset at {}", handle);

    let key = handle.rsplit('/').next().unwrap();
    let url = source.url.join(&handle)?;

    let identifier;
    let title;
    let r#abstract;
    let resources;

    {
        let body = client.fetch_text(source, key.to_owned(), &url).await?;

        let document = Html::parse_document(&body);

        identifier = document
            .select(&selectors.identifier)
            .filter_map(|element| element.attr("content"))
            .find(|identifier| identifier.starts_with("urn:"))
            .ok_or_else(|| anyhow!("Missing identifier"))?
            .to_owned();

        title = document
            .select(&selectors.title)
            .next()
            .and_then(|element| element.attr("content"))
            .ok_or_else(|| anyhow!("Missing title"))?
            .to_owned();

        r#abstract = document
            .select(&selectors.r#abstract)
            .next()
            .and_then(|element| element.attr("content"))
            .map(ToOwned::to_owned);

        resources = document
            .select(&selectors.resource_table)
            .map(|elem| {
                let href = elem.attr("href").unwrap();
                let description = collect_text(elem.text());
                let primary_content = !description.contains("Abstracts");

                Ok(Resource {
                    url: source.url.join(href)?.into(),
                    description: Some(description),
                    r#type: ResourceType::Document,
                    primary_content,
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;
    }

    let global_identifier = GlobalIdentifier::parse(identifier.as_str());

    let dataset = Dataset {
        title,
        types: smallvec![Type::Text {
            text_type: TextType::Publication,
        }],
        description: r#abstract,
        origins: source.origins.clone(),
        license: License::DorisBfs,
        source_url: url.into(),
        global_identifier,
        resources,
        ..Default::default()
    };

    write_dataset(dir, client, source, identifier, dataset).await
}

selectors! {
    range: "div.browse_range",
    range_count: r"Anzeige der Treffer \d+ bis \d+ von (\d+)" as Regex,
    handle: "td[headers=t2] a[href]",
    identifier: "head > meta[name='DC.identifier']",
    title: "head > meta[name='DC.title']",
    resource_table: ".evenRowEvenCol table a[href]",
    r#abstract: "head > meta[name='DCTERMS.abstract']",
}
