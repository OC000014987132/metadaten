use std::fmt::Write;

use anyhow::{anyhow, ensure, Context, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use memchr::memmem::find;
use miniproj::get_projection;
use regex::Regex;
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::{smallvec, SmallVec};
use time::{Date, Month};

use harvester::{
    client::Client,
    fetch_many,
    utilities::{contains_or, point_like_bounding_box},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25833).unwrap();

    let years_pattern = &Regex::new(r"(?<from>\d{4})-(?<until>\d{4})").unwrap();

    let object_ids = {
        let mut url = source.url.join("0/query")?;
        url.query_pairs_mut()
            .append_pair("f", "json")
            .append_pair("returnIdsOnly", "true");

        let bytes = client
            .checked_fetch_bytes(source, inline_errors, "object-ids".to_owned(), &url)
            .await?;
        from_slice::<ObjectIds>(&bytes)?
    };

    let count = object_ids.object_ids.len();

    let (results, errors) = fetch_many(0, 0, object_ids.object_ids, |object_id| async move {
        let response = {
            let mut url = source.url.join("0/query")?;
            url.query_pairs_mut()
                .append_pair("f", "json")
                .append_pair("outFields", "*")
                .append_pair("objectIds", &object_id.to_string());

            let bytes = client
                .checked_fetch_bytes(
                    source,
                    inline_errors,
                    format!("object-{object_id}.isol"),
                    &url,
                )
                .await?;
            from_slice::<Response>(&bytes)
                .with_context(|| format!("Invalid JSON for object {object_id}"))?
        };

        let feature = response
            .features
            .into_iter()
            .next()
            .ok_or_else(|| anyhow!("Missing feature in server response"))?;

        let id = feature.attributes.id;

        let label = feature.attributes.label;

        let title = format!("Grundwassermessstelle {label} ({id})");

        let mut description = format!("Dieser Datensatz beschreibt die {title}.");

        match feature.attributes.measurement_device {
            Some(measurement_device) if !measurement_device.is_empty() => write!(
                &mut description,
                " Die Messstelle ist ein {measurement_device}."
            )?,
            _ => (),
        }

        if let Some(years_missed) = feature.attributes.years_missed {
            write!(
                &mut description,
                " Es fehlen Messwerte von den Jahren {years_missed}."
            )?;
        } else {
            write!(&mut description, " Es gibt keine Fehljahre im Datensatz.")?;
        }

        let bounding_boxes = smallvec![proj_rect(
            point_like_bounding_box(feature.geometry.y, feature.geometry.x),
            proj
        )];

        let regions = smallvec![contains_or(label, Region::SN)];

        let time_ranges = years_pattern
            .captures_iter(&feature.attributes.years)
            .map(|captures| {
                let start_year = captures["from"].parse()?;
                let start_date = Date::from_calendar_date(start_year, Month::January, 1)?;

                let end_year = captures["until"].parse()?;
                let end_date = Date::from_calendar_date(end_year, Month::December, 31)?;

                Ok((start_date, end_date).into())
            })
            .collect::<Result<SmallVec<_>>>()?;

        let organisations = smallvec![Organisation::Other {
            name: "Sächsisches Landesamt für Umwelt, Landwirtschaft und Geologie".to_owned(),
            role: OrganisationRole::Publisher,
            websites: smallvec!["https://lfulg.sachsen.de/".to_owned()],
        }];

        let resources = smallvec![
            Resource {
                r#type: ResourceType::Pdf,
                description: Some("Kurzanleitung zur Nutzung von iDA".to_owned()),
                url: "https://luis.sachsen.de/download/Kurzanleitung-iDA.pdf".to_owned(),
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::WebPage,
                description: Some("iDA - Das Datenportal für Sachsen".to_owned()),
                url: "https://www.umwelt.sachsen.de/umwelt/infosysteme/ida/pages/home/index.xhtml"
                    .to_owned(),
                direct_link: false,
                ..Default::default()
            }
        ];

        let source_url = feature
            .attributes
            .diagram
            .unwrap_or_else(|| source.source_url().to_owned());

        let types = smallvec![Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(id.clone()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasserstand".to_owned()],
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            regions,
            bounding_boxes,
            time_ranges,
            organisations,
            resources,
            language: Language::German,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url,
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, id.into(), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

fn inline_errors(bytes: &[u8]) -> Result<()> {
    ensure!(
        find(bytes, b"<h1>Bad Gateway!</h1>").is_none(),
        "HTML response indicates bad gateway"
    );

    ensure!(
        find(bytes, br#"{"error":{"code":500"#).is_none(),
        "JSON response indicates query error"
    );

    Ok(())
}

#[derive(Debug, Deserialize)]
struct ObjectIds {
    #[serde(rename = "objectIds")]
    object_ids: Vec<usize>,
}

#[derive(Debug, Deserialize)]
struct Response {
    features: SmallVec<[Feature; 1]>,
}

#[derive(Debug, Deserialize)]
struct Feature {
    attributes: Attributes,
    geometry: Geometry,
}

#[derive(Debug, Deserialize)]
struct Attributes {
    #[serde(rename = "MKZG")]
    id: CompactString,
    #[serde(rename = "MENA")]
    label: String,
    #[serde(rename = "GANGLINIE")]
    diagram: Option<String>,
    #[serde(rename = "MA")]
    measurement_device: Option<String>,
    #[serde(rename = "DATENBASIS_HAUPTWERTE")]
    years: String,
    #[serde(rename = "FEHLJAHRE")]
    years_missed: Option<String>,
}

#[derive(Debug, Deserialize)]
struct Geometry {
    x: f64,
    y: f64,
}
