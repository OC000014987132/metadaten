use std::borrow::Cow;

use anyhow::{anyhow, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use geo::{Coord, Rect};
use hashbrown::HashMap;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use serde_json::from_slice;
use smallvec::{smallvec, SmallVec};
use time::{format_description::FormatItem, macros::format_description, Date};

use harvester::{
    client::{Client, TrackedResponse},
    fetch_many,
    utilities::{uppercase_words, yesterday},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{Domain, ReportingObligation, Station, Type},
    Dataset, Language, License, Region, Resource, ResourceType, TimeRange,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let url = source
        .url
        .join("rest/BaseController/Element/APPL_COUNT_MSMNT_STATIONSPARAM/1")?;

    let body = client.fetch_bytes(source, "count".to_owned(), &url).await?;
    let response = from_slice::<Count>(&body)?;

    let mut count = response.project_stations;
    let pages = count.div_ceil(source.batch_size);

    let (mut results, mut errors) = fetch_many(0, 0, 0..pages, |page| {
        fetch_stations(dir, client, source, page)
    })
    .await;

    let (count1, results1, errors1) = fetch_plc_stations(dir, client, source).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let body = inquire_api(
        client,
        source,
        format!("response-p{page}"),
        Vec::new(),
        source.batch_size,
        source.batch_size * page,
        "V_MUDAB_PROJECTSTATION",
    )
    .await?;

    let response = from_slice::<Response>(&body)?;

    let count = response.results.len();

    let (results, errors) = fetch_many(0, 0, response.results, |document| {
        translate_stations(dir, client, source, document)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
    document: Document<'_>,
) -> Result<(usize, usize, usize)> {
    let institutes_str = create_institutes_str(&document);
    let parameter_str = create_parameter_str(&document);
    let purpose_str = create_purpose_str(&document);
    let helcom_str = create_helcom_str(&document);
    let blmp_str = create_blmp_str(&document);
    let tmap_str = create_tmap_str(&document);
    let wfd_str = create_wfd_str(&document);

    let id = document.id.to_string();
    let name = document.name;

    let region = document
        .region
        .ok_or_else(|| anyhow!("Missing region in MUBAD for station {name}"))?;

    let sea_str = format!(" in der Region {}", region);

    let region = Region::Other(region.into());

    let y = document.latitude;
    let x = document.longitude;
    let dy = document.range_latitude.unwrap_or(0.0);
    let dx = document.range_longitude.unwrap_or(0.0);
    let bounding_boxes = smallvec![Rect::new(
        Coord {
            x: x - dx,
            y: y - dy,
        },
        Coord {
            x: x + dx,
            y: y + dy,
        },
    )];

    let active_from = Date::parse(document.date_from, DATE_FORMAT)?;
    let active_to = document
        .date_to
        .map(|date_to| Date::parse(date_to, DATE_FORMAT))
        .transpose()?
        .unwrap_or_else(yesterday);
    let time_ranges = smallvec![(active_from, active_to).into()];

    let title = format!("Messdaten der Projekstation {}", &name);

    let description = format!("Dieser Datensatz enthält die Messdaten der Projektstation {name} mit der Identifikationsnummer 
    {id}{sea_str}.{institutes_str}{parameter_str}{purpose_str}{helcom_str}{blmp_str}{tmap_str}{wfd_str}");

    let resources = smallvec![
        Resource {
            r#type: ResourceType::Pdf,
            description: Some("Anleitung der Meeresumweltdatenbank".to_owned()),
            url: String::from(
                "https://www.wasserblick.net/servlet/is/1/Dokumentation_MUDABAnwendung.pdf",
            ),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::Pdf,
            description: Some("Codelisten der Meeresumweltdatenbank".to_owned()),
            url: String::from("https://geoportal.bafg.de/birt_viewer/frameset?__report=MUDAB_CODELISTEN.rptdesign&__format=pdf"),
            ..Default::default()
        },
        Resource {
            url: "https://mudab.api.bund.dev/".to_owned(),
            description: Some("Beschreibung der MUDAB API erstellt durch bundDEV".to_owned()),
            r#type: ResourceType::WebPage,
            ..Default::default()
        },
    ];

    let types = smallvec![
        Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(id.as_str().into()),
                reporting_obligations: smallvec![
                    ReportingObligation::Ospar,
                    ReportingObligation::SoeReport,
                ],
                ..Default::default()
            }),
            measured_variables: smallvec!["Inhaltsstoffe".to_owned()],
            methods: Default::default(),
        },
        Type::Measurements {
            domain: Domain::Sea,
            station: Some(Station {
                id: Some(id.as_str().into()),
                reporting_obligations: smallvec![
                    ReportingObligation::Ospar,
                    ReportingObligation::SoeReport,
                ],
                ..Default::default()
            }),
            measured_variables: Default::default(),
            methods: Default::default(),
        },
    ];

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        origins: source.origins.clone(),
        source_url: source.url.clone().into(),
        bounding_boxes,
        time_ranges,
        regions: smallvec![region],
        resources,
        language: Language::German,
        license: License::OtherOpen,
        ..Default::default()
    };

    write_dataset(dir, client, source, id, dataset).await
}

async fn fetch_plc_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let documents = AtomicRefCell::new(HashMap::<String, SmallVec<[DocumentPLC; 1]>>::new());

    let mut count = 0;

    for page in 0.. {
        let body = inquire_api(
            client,
            source,
            format!("response-plc-p{page}"),
            Vec::new(),
            source.batch_size,
            source.batch_size * page,
            "V_PLC_STATION",
        )
        .await?;
        let response = from_slice::<ResponsePLCStation>(&body)?;

        let done = response.results.len() < source.batch_size;

        count += response.results.len();

        response.results.into_iter().for_each(|document| {
            documents
                .borrow_mut()
                .entry_ref(&document.station_name)
                .or_default()
                .push(document);
        });

        if done {
            break;
        }
    }

    let (results, errors) = fetch_many(
        0,
        0,
        documents.into_inner(),
        |(_station_name, documents)| translate_plc_stations(dir, client, source, documents),
    )
    .await;

    Ok((count, results, errors))
}

async fn translate_plc_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
    documents: SmallVec<[DocumentPLC; 1]>,
) -> Result<(usize, usize, usize)> {
    let document = documents.first().unwrap();

    let mut monitor_types = documents
        .iter()
        .map(|document| document.mon_type.clone())
        .collect::<Vec<String>>();
    monitor_types.sort_unstable();
    monitor_types.dedup();

    let mut station_ids = documents
        .iter()
        .map(|document| document.station_code.clone())
        .collect::<Vec<String>>();
    station_ids.sort_unstable();
    station_ids.dedup();

    let title = format!(
        "MUDAB: HELCOM PLC Station {} - {}",
        uppercase_words(&document.station_name.to_lowercase()),
        &document.subcm_name,
    );

    let mut description = format!(
        "PLC Stationsdaten für {}\n\nBeobachtungstypen: {}",
        title,
        monitor_types.join(" - "),
    );

    let resources = smallvec![
        Resource {
            url: "https://www.umweltbundesamt.de/tags/helcom".to_owned(),
            description: Some("Themenseite des Umweltbundesamtes zu HELCOM".to_owned()),
            r#type: ResourceType::WebPage,
            ..Default::default()
        },
        Resource {
            url: "https://mudab.api.bund.dev/".to_owned(),
            description: Some("Beschreibung der MUDAB API erstellt durch bundDEV".to_owned()),
            r#type: ResourceType::WebPage,
            ..Default::default()
        }
    ];

    let mut bounding_boxes = SmallVec::new();
    if let (Some(st_lon), Some(st_lat)) = (document.st_lon, document.st_lat) {
        if st_lon != 0.0 && st_lat != 0.0 {
            let position = Coord {
                x: st_lon,
                y: st_lat,
            };
            bounding_boxes.push(Rect::new(position, position));
        }
    }

    let mut time_ranges = SmallVec::new();
    let mut measured_variables = SmallVec::new();

    for station_id in station_ids {
        let mut station_params = Vec::<String>::new();

        for page in 0.. {
            let body = inquire_api(
                client,
                source,
                format!("response-plc-para-{}-p{}.isol", document.station_code, page),
                vec![And {
                    col: "STATION_CODE",
                    op: "eq",
                    value: &document.station_code,
                }],
                source.batch_size,
                source.batch_size * page,
                "V_GEMESSENE_PARA_PLC",
            )
            .await?;

            let response = from_slice::<ResponsePLCStationParam>(&body)?;

            let done = response.results.len() < source.batch_size;

            station_params.extend(response.results.into_iter().map(|result| result.parameter));

            if done {
                break;
            }
        }

        let mut years_active = Vec::<i32>::new();

        for page in 0.. {
            let body = inquire_api(
                client,
                source,
                format!("response-plc-msrt-{}-p{}.isol", document.station_code, page),
                vec![And {
                    col: "STATION_CODE",
                    op: "eq",
                    value: &document.station_code,
                }],
                source.batch_size,
                source.batch_size * page,
                "V_MESSWERTE_PLC",
            )
            .await?;

            let response = from_slice::<ResponsePLCStationMSWRT>(&body)?;

            let done = response.results.len() < source.batch_size;

            years_active.reserve(response.results.len());

            for result in response.results {
                years_active.push(result.period.parse()?);
            }

            if done {
                break;
            }
        }

        years_active.sort_unstable();
        years_active.dedup();

        time_ranges = years_active
            .into_iter()
            .map(TimeRange::whole_year)
            .collect::<Result<_, _>>()?;

        description = format!(
            "{}\n\nStation {}\nGemessene Werte: {}",
            description,
            station_id,
            station_params.join("; "),
        );

        for station_param in station_params {
            measured_variables.push(station_param.to_owned());
        }
    }

    let types = smallvec![
        Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(document.station_name.as_str().into()),
                reporting_obligations: smallvec![ReportingObligation::Helcom],
                ..Default::default()
            }),
            measured_variables: measured_variables.clone(),
            methods: Default::default(),
        },
        Type::Measurements {
            domain: Domain::Sea,
            station: Some(Station {
                id: Some(document.station_name.as_str().into()),
                reporting_obligations: smallvec![ReportingObligation::Helcom],
                ..Default::default()
            }),
            measured_variables,
            methods: Default::default(),
        },
    ];

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        origins: source.origins.clone(),
        source_url: source.url.clone().into(),
        bounding_boxes,
        time_ranges,
        resources,
        license: License::OtherOpen,
        language: Language::German,
        ..Default::default()
    };

    write_dataset(
        dir,
        client,
        source,
        document.station_code.to_owned(),
        dataset,
    )
    .await
}

fn create_institutes_str(document: &Document) -> String {
    if let Some(institutes) = &document.institutes {
        let mut institutes_str = String::from(" Die Messstation wird betrieben");

        const INSTITUTES: &[(&str, &str)] = &[
            ("AWI", " vom Alfred-Wegener-Institut (AWI),"),
            ("BFA", " von der Bundesforschungsanstalt für Fischerei (BFA),"),
            ("BFG", " vom Bundesamt für Gewässerkunde (BFG),"),
            ("BFN", " vom Bundesamt für Naturschutz (BFN),"),
            ("BSH", " vom Bundesamt für Seeschifffahrt und Hydrographie (BSH),"),
            ("DEMV", " von Mecklenburg-Vorpommern,"),
            ("DENI", " von Niedersachsen,"),
            ("DESH", " von Schleswig-Holstein,"),
            ("DHI", " vom Deutsche Hydrographische Institut (DHI),"),
            ("FOE", " vom Institut für Fischereiökologie (FOE),"),
            ("LANU", " vom LANU,"),
            ("LLUR", " vom Schleswig-Holstein Landesamt für Landwirtschaft, Umwelt und ländliche Räume (LLUR),"),
            ("LUNG", " vom Schleswig-Holstein Landesamt für Umwelt, Naturschutz und Geologie (LUNG),"),
            ("NLOE", " vom NLOE,"),
            ("NLWHI", " vom NLWHI,"),
            ("NLWKN", " vom Niedersächsischen Landesbetriebs für Wasserwirtschaft, Küsten- und Naturschutz (NLWKN),"),
            ("IOW", " vom Leibniz-Institut für Ostseeforschung Warnemünde (IOW),"),
            ("ISH", " vom Institut für Seefischerei (ISH),"),
            ("UPB", " vom UPB,"),
            ("VTI", " vom Johann Heinrich von Thünen-Institut (vTI),"),
            ("WGEHH", " vom WGEHH,"),
            ("WGWHI", " vom WGWHI,"),
        ];

        for (short, long) in INSTITUTES {
            if institutes.contains(short) {
                institutes_str.push_str(long);
            }
        }

        institutes_str.pop();
        institutes_str.push('.');
        institutes_str
    } else {
        String::new()
    }
}

fn create_parameter_str(document: &Document) -> String {
    let checks = [
        (&document.zoobenthos_data, "Zoobenthos Daten"),
        (&document.zooplankton_data, "Zooplankton Daten"),
        (
            &document.bio_effects_params_biota,
            "biologische Effekte im Biota",
        ),
        (&document.cont_params_biota, "Schadstoffe im Biota"),
        (
            &document.disease_params_biota,
            "Infektionsparameter im Wasser",
        ),
        (
            &document.bio_effects_params_water,
            "biologische Effekte im Wasser",
        ),
        (&document.cont_params_water, "Schadstoffe im Wasser"),
        (&document.nutrients_water, "Nährstoffe im Wasser"),
        (
            &document.chem_eutr_effect_params_water,
            "chemische Eutrophierungseffekte",
        ),
        (
            &document.bio_effects_params_sediment,
            "biologische Effekte im Sediment",
        ),
        (&document.cont_params_sediment, "Schadstoffe im Sediment"),
    ];

    let messages = checks
        .iter()
        .filter(|(field, _)| field.as_deref() == Some("Y"))
        .map(|(_, message)| *message)
        .join(", ");

    if !messages.is_empty() {
        format!(" Gemessene Größen sind {}.", messages)
    } else {
        String::new()
    }
}

fn create_purpose_str(document: &Document) -> String {
    let checks = [
        (&document.purpose_bio_effects, "biologische Effekte"),
        (&document.purpose_euthroph_effects, "Euthrophierungseffekte"),
        (&document.purpose_health, "Gesundheitsrisiken"),
        (&document.purpose_spatial, "räumliche Verteilung"),
        (&document.purpose_temporal, "zeitliche Entwicklung"),
    ];

    let messages = checks
        .iter()
        .filter(|(field, _)| field.as_deref() == Some("Y"))
        .map(|(_, message)| *message)
        .join(", ");

    if !messages.is_empty() {
        format!(" Beobachtungszwecke sind {}.", messages)
    } else {
        String::new()
    }
}

// several of the fields in this tab don't have any data in any of the stations and are therefore ommitted
fn create_helcom_str(document: &Document) -> String {
    let checks = &[
        (&document.helcom_chl_a, "Chlorophyll-a"),
        (&document.helcom_frequent, "Häufigkeit"),
        (&document.helcom_humic, "Huminstoffe"),
        (&document.helcom_hydrography, "Hydrographie"),
        (&document.helcom_nutrients, "Nährstoffe"),
        (&document.helcom_particle_matter, "Feinstaub"),
        (&document.helcom_phythoplankton, "Phytoplankton"),
        (&document.helcom_zoobenthos, "Zoobenthos"),
        (&document.helcom_zooplankton, "Zooplankton"),
    ];

    let names = checks
        .iter()
        .filter(|(field, _)| field.is_some())
        .map(|(_, name)| *name)
        .chain(document.helcom_remarks.as_deref())
        .join(", ");

    if !names.is_empty() {
        format!(" HELCOM Daten sind {}.", names)
    } else {
        String::new()
    }
}

fn create_blmp_str(document: &Document) -> String {
    let checks = [
        (&document.blmp_bmp, "Ostsee Makrophyten"),
        (&document.blmp_bmz, "Ostsee Makrozoobenthos"),
        (&document.blmp_n, "Nährstoffe"),
        (&document.blmp_opw, "organische Schadstoffe im Wasser"),
        (&document.blmp_nmp, "Nordsee Makrophyten"),
        (&document.blmp_nmz, "Nordsee Makrozoobenthos"),
        (&document.blmp_tmo, "Spurenmetalle in Organismen"),
        (&document.blmp_tms, "Spurenmetalle im Sediment"),
        (&document.blmp_tmw, "Spurenmetalle im Wasser"),
        (&document.blmp_zp, "Zooplankton"),
    ];

    let names = checks
        .iter()
        .filter(|(field, _)| field.as_deref() == Some("Y"))
        .map(|(_, name)| *name)
        .join(", ");

    if !names.is_empty() {
        format!(
            " Daten aus dem Bund-Länder-Messprogramm (BLMP) sind {}.",
            names
        )
    } else {
        String::new()
    }
}

fn create_tmap_str(document: &Document) -> String {
    let checks = [
        (&document.tmap_airt, "tmap_airt"),
        (&document.tmap_airt, "tmap_airt"),
        (&document.tmap_beab, "tmap_beab"),
        (&document.tmap_bedu, "tmap_bedu"),
        (&document.tmap_blmb, "tmap_blmb"),
        (&document.tmap_breb, "tmap_breb"),
        (&document.tmap_btas, "tmap_btas"),
        (&document.tmap_cibe, "Schadstoffe in Vogeleiern"),
        (&document.tmap_cibm, "Schadstoffe in Muscheln"),
        (&document.tmap_cifl, "Schadstoffe in Flundern"),
        (&document.tmap_copm, "tmap_copm"),
        (&document.tmap_eelg, "tmap_eelg"),
        (&document.tmap_fldg, "tmap_fldg"),
        (&document.tmap_fmcs, "tmap_fmcs"),
        (&document.tmap_geom, "tmap_geom"),
        (&document.tmap_guit, "tmap_guit"),
        (&document.tmap_hydr, "Hydrologie"),
        (&document.tmap_lndu, "tmap_lndu"),
        (&document.tmap_maca, "tmap_maca"),
        (&document.tmap_mets, "Metalle im Sediment"),
        (&document.tmap_migb, "tmap_migb"),
        (&document.tmap_mzbc, "tmap_mzbc"),
        (&document.tmap_nutw, "Nährstoffe im Wasser"),
        (&document.tmap_ppkt, "tmap_ppkt"),
        (&document.tmap_sama, "tmap_sama"),
        (&document.tmap_seal, "Robben"),
        (&document.tmap_tbws, "tmap_tbws"),
        (&document.tmap_weac, "tmap_weac"),
    ];

    let names = checks
        .iter()
        .filter(|(field, _)| field.as_deref() == Some("Y"))
        .map(|(_, name)| *name)
        .join(", ");

    if !names.is_empty() {
        format!(" Daten des trilateralen Monitoring und Assessment Programms (TMAP - Trilateral Monitoring and Assessment Program) sind {}.", names)
    } else {
        String::new()
    }
}

fn create_wfd_str(document: &Document) -> String {
    let wfds = [
        (&document.wfd_1_1, "WFD 1.1"),
        (&document.wfd_1_2, "WFD 1.2"),
        (&document.wfd_1_3, "WFD 1.3"),
        (&document.wfd_1_4, "WFD 1.4"),
        (&document.wfd_1_5, "WFD 1.5"),
        (&document.wfd_2_1, "WFD 2.1"),
        (&document.wfd_2_2, "WFD 2.2"),
        (&document.wfd_2_3, "WFD 2.3"),
        (&document.wfd_2_4, "WFD 2.4"),
        (&document.wfd_2_5, "WFD 2.5"),
        (&document.wfd_2_6, "WFD 2.6"),
        (&document.wfd_2_7, "WFD 2.7"),
        (&document.wfd_2_8, "WFD 2.8"),
        (&document.wfd_3_1, "WFD 3.1"),
        (&document.wfd_3_2, "WFD 3.2"),
        (&document.wfd_3_3, "WFD 3.3"),
        (&document.wfd_3_4, "WFD 3.4"),
    ];

    let wfds = wfds
        .iter()
        .filter(|(field, _)| field.is_some())
        .map(|(_, name)| *name)
        .join(", ");

    if !wfds.is_empty() {
        format!(
            " Daten der Wasserrahmenrichtlinie (WFD - Water Framework Directive) sind: {}",
            wfds
        )
    } else {
        String::new()
    }
}

async fn inquire_api(
    client: &Client,
    source: &Source,
    response_id: String,
    filter: Vec<And<'_>>,
    count: usize,
    from: usize,
    api_key: &str,
) -> Result<TrackedResponse<Vec<u8>>> {
    let url = source
        .url
        .join(&format!("rest/BaseController/FilterElements/{}", api_key))?;

    client
        .make_request(source, response_id, Some(&url), |client| async {
            client
                .post(url.clone())
                .json(&Request {
                    filter: Filter {
                        and: filter.clone(),
                    },
                    orderby: Vec::new(),
                    range: Range { count, from },
                })
                .send()
                .await?
                .error_for_status()?
                .bytes()
                .await
                .map(Vec::from)
        })
        .await
}

#[derive(Deserialize, Debug)]
struct Count {
    #[serde(rename = "PROJECTSTATION_COUNT")]
    project_stations: usize,
}

#[derive(Serialize)]
struct Request<'a> {
    #[serde(borrow)]
    filter: Filter<'a>,
    orderby: Vec<Orderby>,
    range: Range,
}

#[derive(Serialize)]
struct Filter<'a> {
    #[serde(borrow)]
    and: Vec<And<'a>>,
}

#[derive(Serialize, Clone)]
struct And<'a> {
    col: &'a str,
    op: &'a str,
    value: &'a str,
}

#[derive(Serialize)]
struct Orderby {}

#[derive(Serialize)]
struct Range {
    count: usize,
    from: usize,
}

#[derive(Deserialize, Debug)]
struct Response<'a> {
    #[serde(rename = "V_MUDAB_PROJECTSTATION", borrow)]
    results: Vec<Document<'a>>,
}

#[derive(Deserialize, Debug)]
struct Document<'a> {
    #[serde(rename = "PROJECTSTATIONID")]
    id: usize,
    #[serde(rename = "NAME_PS", borrow)]
    name: Cow<'a, str>,
    #[serde(rename = "START_PS")]
    date_from: &'a str,
    #[serde(rename = "STOP_PS")]
    date_to: Option<&'a str>,
    #[serde(rename = "REGION", borrow)]
    region: Option<Cow<'a, str>>,
    // #[serde(rename = "INSTITUTE")]
    // institute: Option<Cow<'a, str>>,
    #[serde(rename = "INSTITUTES")]
    institutes: Option<&'a str>,
    #[serde(rename = "RANGE_LATITUDE")]
    range_latitude: Option<f64>,
    #[serde(rename = "RANGE_LONGITUDE")]
    range_longitude: Option<f64>,
    #[serde(rename = "LATITUDE")]
    latitude: f64,
    #[serde(rename = "LONGITUDE")]
    longitude: f64,
    // #[serde(rename = "HELCOM_SUBBASIN")]
    // sub_region: Option<Cow<'a, str>>,
    #[serde(rename = "ZOOBENTHOS_DATA")]
    zoobenthos_data: Option<&'a str>,
    #[serde(rename = "ZOOPLANKTON_DATA")]
    zooplankton_data: Option<&'a str>,
    #[serde(rename = "BIO_EFFECTS_PARAMS_BIOTA")]
    bio_effects_params_biota: Option<&'a str>,
    #[serde(rename = "CONT_PARAMS_BIOTA")]
    cont_params_biota: Option<&'a str>,
    #[serde(rename = "DISEASE_PARAMS_BIOTA")]
    disease_params_biota: Option<&'a str>,
    #[serde(rename = "NUTRIENTS_WATER")]
    nutrients_water: Option<&'a str>,
    #[serde(rename = "CONT_PARAMS_WATER")]
    cont_params_water: Option<&'a str>,
    #[serde(rename = "BIO_EFFECTS_PARAMS_WATER")]
    bio_effects_params_water: Option<&'a str>,
    #[serde(rename = "CHEM_EUTR_EFFECT_PARAMS_WATER")]
    chem_eutr_effect_params_water: Option<&'a str>,
    #[serde(rename = "BIO_EFFECTS_PARAMS_SEDIMENT")]
    bio_effects_params_sediment: Option<&'a str>,
    #[serde(rename = "CONT_PARAMS_SEDIMENT")]
    cont_params_sediment: Option<&'a str>,

    #[serde(rename = "PURPOSE_SPATIAL")]
    purpose_spatial: Option<&'a str>,
    #[serde(rename = "PURPOSE_TEMPORAL")]
    purpose_temporal: Option<&'a str>,
    #[serde(rename = "PURPOSE_HUMAN_HEALTH")]
    purpose_health: Option<&'a str>,
    #[serde(rename = "PURPOSE_BIOLOGICAL_EFFECTS")]
    purpose_bio_effects: Option<&'a str>,
    #[serde(rename = "PURPOSE_EUTROPHICATION_EFFECTS")]
    purpose_euthroph_effects: Option<&'a str>,

    // #[serde(rename = "HELCOM_MAP_WINTER_NUTRIENTS_HE")]
    // helcom_map_winter_nutrients_he: Option<&'a str>,
    #[serde(rename = "HELCOM_FREQUENT")]
    helcom_frequent: Option<&'a str>,
    // #[serde(rename = "HELCOM_HIGH_FREQUENT")]
    // helcom_high_frequent: Option<&'a str>,
    #[serde(rename = "HELCOM_NUTRIENTS")]
    helcom_nutrients: Option<&'a str>,
    #[serde(rename = "HELCOM_PARTICLE_MATTER")]
    helcom_particle_matter: Option<&'a str>,
    #[serde(rename = "HELCOM_HUMIC")]
    helcom_humic: Option<&'a str>,
    #[serde(rename = "HELCOM_CHL_A")]
    helcom_chl_a: Option<&'a str>,
    #[serde(rename = "HELCOM_PHYTOPLANKTON")]
    helcom_phythoplankton: Option<&'a str>,
    #[serde(rename = "HELCOM_ZOOPLANKTON")]
    helcom_zooplankton: Option<&'a str>,
    #[serde(rename = "HELCOM_ZOOBENTHOS")]
    helcom_zoobenthos: Option<&'a str>,
    // #[serde(rename = "HELCOM_MICROBIOLOGY")]
    // helcom_microbiology: Option<&'a str>,
    // #[serde(rename = "HELCOM_PRIMARY_PRODUCTION")]
    // helcom_primary_production: Option<&'a str>,
    // #[serde(rename = "HELCOM_BUOY")]
    // helcom_buoy: Option<&'a str>,
    #[serde(rename = "HELCOM_HYDROGRAPHY")]
    helcom_hydrography: Option<&'a str>,
    // #[serde(rename = "HELCOM_BMP_STATION_NR")]
    // helcom_bmp_station_nr: Option<&'a str>,
    #[serde(rename = "HELCOM_REMARKS", borrow)]
    helcom_remarks: Option<Cow<'a, str>>,
    // #[serde(rename = "HELCOM_REMARKS1")]
    // helcom_remarks1: Option<Cow<'a, str>>,
    #[serde(rename = "BLMP_BMP")]
    blmp_bmp: Option<&'a str>,
    #[serde(rename = "BLMP_BMZ")]
    blmp_bmz: Option<&'a str>,
    #[serde(rename = "BLMP_N")]
    blmp_n: Option<&'a str>,
    #[serde(rename = "BLMP_NMP")]
    blmp_nmp: Option<&'a str>,
    #[serde(rename = "BLMP_NMZ")]
    blmp_nmz: Option<&'a str>,
    // #[serde(rename = "BLMP_OPO")]
    // blmp_opo: Option<&'a str>,
    // #[serde(rename = "BLMP_OPS")]
    // blmp_ops: Option<&'a str>,
    #[serde(rename = "BLMP_OPW")]
    blmp_opw: Option<&'a str>,
    // #[serde(rename = "BLMP_PP")]
    // blmp_pp: Option<&'a str>,
    #[serde(rename = "BLMP_TMO")]
    blmp_tmo: Option<&'a str>,
    #[serde(rename = "BLMP_TMS")]
    blmp_tms: Option<&'a str>,
    #[serde(rename = "BLMP_TMW")]
    blmp_tmw: Option<&'a str>,
    #[serde(rename = "BLMP_ZP")]
    blmp_zp: Option<&'a str>,

    #[serde(rename = "TMAP_MZBC")]
    tmap_mzbc: Option<&'a str>,
    #[serde(rename = "TMAP_PPKT")]
    tmap_ppkt: Option<&'a str>,
    #[serde(rename = "TMAP_BREB")]
    tmap_breb: Option<&'a str>,
    #[serde(rename = "TMAP_MIGB")]
    tmap_migb: Option<&'a str>,
    #[serde(rename = "TMAP_BEAB")]
    tmap_beab: Option<&'a str>,
    #[serde(rename = "TMAP_SEAL")]
    tmap_seal: Option<&'a str>,
    #[serde(rename = "TMAP_TBWS")]
    tmap_tbws: Option<&'a str>,
    #[serde(rename = "TMAP_METS")]
    tmap_mets: Option<&'a str>,
    #[serde(rename = "TMAP_NUTW")]
    tmap_nutw: Option<&'a str>,
    #[serde(rename = "TMAP_CIBM")]
    tmap_cibm: Option<&'a str>,
    #[serde(rename = "TMAP_CIFL")]
    tmap_cifl: Option<&'a str>,
    #[serde(rename = "TMAP_CIBE")]
    tmap_cibe: Option<&'a str>,
    #[serde(rename = "TMAP_SAMA")]
    tmap_sama: Option<&'a str>,
    #[serde(rename = "TMAP_MACA")]
    tmap_maca: Option<&'a str>,
    #[serde(rename = "TMAP_EELG")]
    tmap_eelg: Option<&'a str>,
    #[serde(rename = "TMAP_BLMB")]
    tmap_blmb: Option<&'a str>,
    #[serde(rename = "TMAP_BEDU")]
    tmap_bedu: Option<&'a str>,
    #[serde(rename = "TMAP_GEOM")]
    tmap_geom: Option<&'a str>,
    #[serde(rename = "TMAP_LNDU")]
    tmap_lndu: Option<&'a str>,
    #[serde(rename = "TMAP_FMCS")]
    tmap_fmcs: Option<&'a str>,
    #[serde(rename = "TMAP_BTAS")]
    tmap_btas: Option<&'a str>,
    #[serde(rename = "TMAP_GUIT")]
    tmap_guit: Option<&'a str>,
    #[serde(rename = "TMAP_AIRT")]
    tmap_airt: Option<&'a str>,
    #[serde(rename = "TMAP_COPM")]
    tmap_copm: Option<&'a str>,
    #[serde(rename = "TMAP_FLDG")]
    tmap_fldg: Option<&'a str>,
    #[serde(rename = "TMAP_WEAC")]
    tmap_weac: Option<&'a str>,
    #[serde(rename = "TMAP_HYDR")]
    tmap_hydr: Option<&'a str>,

    #[serde(rename = "WFD_1_1")]
    wfd_1_1: Option<&'a str>,
    #[serde(rename = "WFD_1_2")]
    wfd_1_2: Option<&'a str>,
    #[serde(rename = "WFD_1_3")]
    wfd_1_3: Option<&'a str>,
    #[serde(rename = "WFD_1_4")]
    wfd_1_4: Option<&'a str>,
    #[serde(rename = "WFD_1_5")]
    wfd_1_5: Option<&'a str>,
    #[serde(rename = "WFD_2_1")]
    wfd_2_1: Option<&'a str>,
    #[serde(rename = "WFD_2_2")]
    wfd_2_2: Option<&'a str>,
    #[serde(rename = "WFD_2_3")]
    wfd_2_3: Option<&'a str>,
    #[serde(rename = "WFD_2_4")]
    wfd_2_4: Option<&'a str>,
    #[serde(rename = "WFD_2_5")]
    wfd_2_5: Option<&'a str>,
    #[serde(rename = "WFD_2_6")]
    wfd_2_6: Option<&'a str>,
    #[serde(rename = "WFD_2_7")]
    wfd_2_7: Option<&'a str>,
    #[serde(rename = "WFD_2_8")]
    wfd_2_8: Option<&'a str>,
    #[serde(rename = "WFD_3_1")]
    wfd_3_1: Option<&'a str>,
    #[serde(rename = "WFD_3_2")]
    wfd_3_2: Option<&'a str>,
    #[serde(rename = "WFD_3_3")]
    wfd_3_3: Option<&'a str>,
    #[serde(rename = "WFD_3_4")]
    wfd_3_4: Option<&'a str>,
}

#[derive(Deserialize, Debug)]
struct ResponsePLCStation {
    #[serde(rename = "V_PLC_STATION")]
    results: Vec<DocumentPLC>,
}

#[derive(Deserialize, Debug, Clone)]
struct DocumentPLC {
    #[serde(rename = "STATION_CODE")]
    station_code: String,
    #[serde(rename = "STATION_NAME")]
    station_name: String,
    // #[serde(rename = "LAND_CD")]
    // land_cd: String,
    #[serde(rename = "ST_LAT")]
    st_lat: Option<f64>,
    #[serde(rename = "ST_LON")]
    st_lon: Option<f64>,
    // #[serde(rename = "SUBCM_CODE")]
    // subcm_code: String,
    #[serde(rename = "SUBCM_NAME")]
    subcm_name: String,
    #[serde(rename = "MON_TYPE")]
    mon_type: String,
}

#[derive(Deserialize, Debug)]
struct ResponsePLCStationParam {
    #[serde(rename = "V_GEMESSENE_PARA_PLC")]
    results: Vec<DocumentPLCParam>,
}

#[derive(Deserialize, Debug, Clone)]
struct DocumentPLCParam {
    #[serde(rename = "PARAMETER")]
    parameter: String,
    // #[serde(rename = "PARAMETERGRUPPE")]
    // parametergruppe: String,
    // #[serde(rename = "ANZ_MESSWERTE")]
    // anz_messwerte: usize,
    // #[serde(rename = "LETZTE_MESSUNG")]
    // letzte_messung: String,
}

#[derive(Deserialize, Debug)]
struct ResponsePLCStationMSWRT<'a> {
    #[serde(rename = "V_MESSWERTE_PLC", borrow)]
    results: Vec<DocumentPLCMesswert<'a>>,
}

#[derive(Deserialize, Debug, Clone)]
struct DocumentPLCMesswert<'a> {
    #[serde(rename = "PERIOD_NAME")]
    period: &'a str,
}

const DATE_FORMAT: &[FormatItem] = format_description!("[year]-[month]-[day]");
