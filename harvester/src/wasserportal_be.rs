use anyhow::{anyhow, Context, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use miniproj::{get_projection, Projection};
use scraper::{ElementRef, Html, Selector};
use smallvec::{smallvec, SmallVec};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, point_like_bounding_box, GermanDate},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
        Tag,
    },
    proj_rect,
    wise::WISE,
};

/// Harvester für alle Messstellen des Berliner Wasserportals
///
/// <https://daten.berlin.de/datensaetze/gew%C3%A4sserkundliche-messdaten>
/// <https://wasserportal.berlin.de/download/wasserportal_berlin_getting_data.pdf>
pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();

    let proj = get_projection(25833).unwrap();

    let (count1, results1, errors1) =
        fetch_stations::<Surface>(dir, client, source, &selectors, proj).await?;

    let (count2, results2, errors2) =
        fetch_stations::<Soil>(dir, client, source, &selectors, proj).await?;

    let (count3, results3, errors3) =
        fetch_stations::<Ground>(dir, client, source, &selectors, proj).await?;

    Ok((
        count1 + count2 + count3,
        results1 + results2 + results3,
        errors1 + errors2 + errors3,
    ))
}

async fn fetch_stations<E>(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: &dyn Projection,
) -> Result<(usize, usize, usize)>
where
    E: ExtractStation,
{
    let table = E::TABLE;

    let results = {
        let url = source.url.join(&format!(
            "start.php?anzeige=tabelle_{table}&messanzeige=ms_all"
        ))?;

        let text = client
            .fetch_text(source, format!("ms_all-{table}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.rows)
            .map(|row| E::overview(source, selectors, proj, row))
            .collect::<Vec<_>>()
    };

    let count = results.len();

    let (results, errors) = fetch_many(0, 0, results, |result| async {
        let (id, mut dataset) = result?;

        E::details(client, source, selectors, &id, &mut dataset)
            .await
            .with_context(|| format!("Failed to get details for {table}-{id}"))?;

        write_dataset(dir, client, source, format!("{table}-{id}"), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

trait ExtractStation {
    const TABLE: &'static str;

    fn overview(
        source: &Source,
        selectors: &Selectors,
        proj: &dyn Projection,
        row: ElementRef,
    ) -> Result<(String, Dataset)>;

    async fn details(
        client: &Client,
        source: &Source,
        selectors: &Selectors,
        id: &str,
        dataset: &mut Dataset,
    ) -> Result<()>;
}

struct Surface;

impl ExtractStation for Surface {
    const TABLE: &'static str = "ow";

    fn overview(
        source: &Source,
        selectors: &Selectors,
        proj: &dyn Projection,
        row: ElementRef,
    ) -> Result<(String, Dataset)> {
        let mut cells = row.select(&selectors.cells);

        let title;
        let href;
        let id;
        {
            let site_id = cells
                .next()
                .ok_or_else(|| anyhow!("Missing ID column"))?
                .select(&selectors.link)
                .next()
                .ok_or_else(|| anyhow!("Missing link"))?;

            let source_url = site_id.attr("href").unwrap().to_owned();

            id = site_id.text().collect::<String>();

            let name = cells
                .next()
                .ok_or_else(|| anyhow!("Missing name column"))?
                .text()
                .collect::<String>();

            title = format!("Oberflächengewässer-Messstelle {name} (Messstellen-Nr.: {id})");

            href = if source_url.starts_with("station.php") {
                source.url.join(&source_url)?.into()
            } else {
                source_url
            };
        }

        let region;
        let mut organisations = SmallVec::new();
        let mut resources = smallvec![Resource {
            r#type: ResourceType::Pdf,
            url: source
                .url
                .join("/download/wasserportal_berlin_getting_data.pdf")?
                .into(),
            description: Some("API-Dokumentation".to_owned()),
            ..Default::default()
        }];
        let description;
        let mut types = SmallVec::new();

        {
            let mut operator = cells
                .next()
                .ok_or_else(|| anyhow!("Missing operator column"))?
                .text()
                .collect::<CompactString>();

            let kind = cells
                .next()
                .ok_or_else(|| anyhow!("Missing kind column"))?
                .text()
                .collect::<String>();

            let body = cells
                .next()
                .ok_or_else(|| anyhow!("Missing body column"))?
                .text()
                .collect::<CompactString>();

            let mut status = cells
                .nth(1)
                .ok_or_else(|| anyhow!("Missing status column"))?
                .text()
                .collect::<CompactString>();

            if status.is_empty() {
                status = "nicht näher angegeben".into()
            } else if status == "Aktiv" {
                status = "aktiv".into()
            } else if status == "Inaktiv" {
                status = "inaktiv".into()
            } else if status == "Beendet/geschlossen" {
                status = "beendet/geschlossen".into()
            }

            description = Some(format!("Die Messstelle dient der Überwachung des Oberflächengewässers {body}. Der Status der Messstelle ist {status}."));

            if operator == "WSV" {
                operator = "Wasser- und Schifffahrtsverwaltung des Bundes".into();
            }

            let municipality = if operator == "Land Berlin" {
                "Berlin"
            } else if operator == "Land Brandenburg" || operator == "LfU Brandenburg" {
                "Brandenburg"
            } else if operator == "Land Mecklenburg_Vorpommern" {
                "Mecklenburg-Vorpommern"
            } else {
                Default::default()
            };

            organisations.push(Organisation::Other {
                name: operator.clone().into(),
                role: OrganisationRole::Operator,
                websites: Default::default(),
            });

            region = municipality.into();

            for parameter in [
                "owt", "olf", "oph", "oog", "oos", "ows", "odf", "opq", "wws",
            ] {
                for (format, resource_type) in [("c", ResourceType::Csv), ("x", ResourceType::Xml)]
                {
                    let wert = "ew";

                    let wertebereich = "Einzelwert";

                    let mut url_figures = source.url.join("/station.php")?;
                    url_figures
                        .query_pairs_mut()
                        .append_pair("anzeige", "g")
                        .append_pair("sgrafik", wert)
                        .append_pair("station", &id)
                        .append_pair("thema", parameter);

                    let mut url_downloads = source.url.join("/station.php")?;
                    url_downloads
                        .query_pairs_mut()
                        .append_pair("anzeige", "d")
                        .append_pair("station", &id)
                        .append_pair("thema", parameter)
                        .append_pair("sreihe", wert)
                        .append_pair("smode", format);

                    let parameter_description = match parameter {
                        "owt" => "Wassertemperatur",
                        "olf" => "Leitfähigkeit",
                        "oph" => "pH-Wert",
                        "oog" => "Sauerstoffgehalt",
                        "oos" => "Sauerstoffsättigung",
                        "ows" => "Wasserstand",
                        "odf" => "Durchfluss",
                        "opq" => "Probenahme",
                        "wws" => "Wasserstand",
                        _ => "keiner",
                    };

                    match (operator.as_str(), kind.as_str(), parameter) {
                        (
                            "Land Berlin",
                            " Wasserstand"
                            | " Wasserstand und Durchfluss"
                            | " Wasserstand (Lattenpegel)",
                            "ows" | "owt" | "wws" | "odf",
                        )
                        | (
                            "Land Berlin",
                            "Online-Messstelle" | "Messstation",
                            "owt" | "oph" | "olf" | "oog" | "oos",
                        )
                        | ("Land Berlin", "Probenahme", "opq")
                        | (
                            "Wasser- und Schifffahrtsverwaltung des Bundes",
                            " Wasserstand",
                            "wws",
                        ) => {
                            resources.push(Resource {
                                r#type: ResourceType::WebPage,
                                url: url_figures.clone().into(),
                                description: Some(format!(
                                    "Diagramm {wertebereich} {parameter_description}"
                                )),
                                ..Default::default()
                            });

                            resources.push(Resource {
                                r#type: resource_type,
                                url: url_downloads.clone().into(),
                                description: Some(format!(
                                    "Download {wertebereich} {parameter_description}"
                                )),
                                primary_content: true,
                                ..Default::default()
                            });

                            match parameter {
                                "wws" | "ows" | "odf" => {
                                    types.push(Type::Measurements {
                                        domain: Domain::Surfacewater,
                                        station: Some(Station {
                                            id: Some(id.as_str().into()),
                                            ..Default::default()
                                        }),
                                        measured_variables: smallvec![
                                            parameter_description.to_owned()
                                        ],
                                        methods: Default::default(),
                                    });
                                }
                                "owt" | "olf" | "oph" | "oog" | "oos" | "opq" => {
                                    types.push(Type::Measurements {
                                        domain: Domain::Chemistry,
                                        station: Some(Station {
                                            id: Some(id.as_str().into()),
                                            ..Default::default()
                                        }),
                                        measured_variables: smallvec![
                                            parameter_description.to_owned()
                                        ],
                                        methods: Default::default(),
                                    });
                                }
                                _ => (),
                            }
                        }
                        _ => (),
                    }
                }
            }
        }

        let longitude = cells
            .next()
            .ok_or_else(|| anyhow!("Missing longitude column"))?
            .text()
            .collect::<CompactString>()
            .parse::<f64>()?;

        let latitude = cells
            .next()
            .ok_or_else(|| anyhow!("Missing latitude column"))?
            .text()
            .collect::<CompactString>()
            .parse::<f64>()?;

        let (longitude, latitude) = proj.projected_to_deg(longitude, latitude);
        let bounding_boxes = smallvec![point_like_bounding_box(latitude, longitude)];

        let mut regions = smallvec![region];
        regions.extend(WISE.match_shape(longitude, latitude).map(Region::Watershed));

        let dataset = Dataset {
            title,
            description,
            types,
            regions,
            bounding_boxes,
            organisations,
            resources,
            language: Language::German,
            license: License::DlDeBy20,
            origins: source.origins.clone(),
            source_url: href.to_owned(),
            tags: vec![Tag::Other("Wasserportal Berlin".into())],
            ..Default::default()
        };

        Ok((id, dataset))
    }

    async fn details(
        client: &Client,
        source: &Source,
        selectors: &Selectors,
        id: &str,
        dataset: &mut Dataset,
    ) -> Result<()> {
        let mut url = source.url.join("/station.php")?;
        url.query_pairs_mut()
            .append_pair("anzeige", "d")
            .append_pair("thema", "opq")
            .append_pair("station", id);

        let text = client
            .fetch_text(source, format!("ow-{id}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let from = document
            .select(&selectors.from)
            .next()
            .map(|elem| elem.attr("value").unwrap().parse::<GermanDate>())
            .transpose()?;

        let until = document
            .select(&selectors.until)
            .next()
            .map(|elem| elem.attr("value").unwrap().parse::<GermanDate>())
            .transpose()?;

        if let (Some(from), Some(until)) = (from, until) {
            dataset.time_ranges.push((from, until).into());
        }

        Ok(())
    }
}

struct Soil;

impl ExtractStation for Soil {
    const TABLE: &'static str = "bw";

    fn overview(
        source: &Source,
        selectors: &Selectors,
        proj: &dyn Projection,
        row: ElementRef,
    ) -> Result<(String, Dataset)> {
        let mut cells = row.select(&selectors.cells);

        let title;
        let href;
        let name;
        {
            let site_name = cells.next().ok_or_else(|| anyhow!("Missing name column"))?;

            let id = site_name
                .select(&selectors.link)
                .next()
                .ok_or_else(|| anyhow!("Missing link"))?;

            let source_url = id.attr("href").unwrap().to_owned();

            name = site_name.text().collect::<String>();

            title = format!("Bodenwassermessstelle {name}");

            href = if source_url.starts_with("station.php") {
                source.url.join(&source_url)?.into()
            } else {
                source_url.clone()
            };
        }

        let description;
        let bounding_boxes;
        let region;
        let mut organisations = SmallVec::new();
        {
            let operator = cells
                .next()
                .ok_or_else(|| anyhow!("Missing operator column"))?
                .text()
                .collect::<String>();

            let district = cells
                .next()
                .ok_or_else(|| anyhow!("Missing district column"))?
                .text()
                .collect::<CompactString>();

            let street = cells
                .next()
                .ok_or_else(|| anyhow!("Missing street column"))?
                .text()
                .collect::<String>();

            let position = cells
                .next()
                .ok_or_else(|| anyhow!("Missing position column"))?
                .text()
                .collect::<String>();

            let depths = cells
                .next()
                .ok_or_else(|| anyhow!("Missing depths column"))?
                .text()
                .collect::<String>();

            let longitude = cells
                .next()
                .ok_or_else(|| anyhow!("Missing longitude column"))?
                .text()
                .collect::<CompactString>()
                .parse::<f64>()?;

            let latitude = cells
                .next()
                .ok_or_else(|| anyhow!("Missing latitude column"))?
                .text()
                .collect::<CompactString>()
                .parse::<f64>()?;

            description = Some(format!(
                r"Die Messstelle {name} dient der Überwachung des Bodenwassers.
Die Messstelle befindet im Bezirk {district} ({street}).
Das Bodenwasser wird in einer Tiefe {depths} gemessen.
Der Standort ist wie folgt beschrieben: {position}."
            ));

            region = district.into();

            bounding_boxes = smallvec![proj_rect(
                point_like_bounding_box(latitude, longitude),
                proj
            )];

            organisations.push(Organisation::Other {
                name: operator.clone(),
                role: OrganisationRole::Operator,
                websites: Default::default(),
            });
        }

        let mut resources = smallvec![Resource {
            r#type: ResourceType::Pdf,
            url: source
                .url
                .join("/download/wasserportal_berlin_getting_data.pdf")?
                .into(),
            description: Some("API-Dokumentation".to_owned()),
            ..Default::default()
        }];

        for werte in ["ew", "tw", "mw"] {
            for thema in ["bbf", "bbt"] {
                let mut url_parameter_fig = source.url.join("/station.php")?;
                url_parameter_fig
                    .query_pairs_mut()
                    .append_pair("anzeige", "g")
                    .append_pair("sgrafik", werte)
                    .append_pair("thema", thema)
                    .append_pair("station", &name);

                let mut url_parameter_down = source.url.join("/station.php")?;
                url_parameter_down
                    .query_pairs_mut()
                    .append_pair("anzeige", "d")
                    .append_pair("station", &name)
                    .append_pair("thema", thema)
                    .append_pair("sreihe", werte)
                    .append_pair("smode", "c");

                if thema == "bbf" {
                    let wertebereich = match werte {
                        "ew" => "Bodenfeuchte als Tiefenprofil",
                        "tw" => "Tagesmittelwerte der Bodenfeuchte als Summe über alle Bodentiefen",
                        "mw" => "Tagesmittelwerte der Bodenfeuchte der einzelnen Bodentiefen",
                        _ => "keiner",
                    };

                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        url: url_parameter_fig.into(),
                        description: Some(format!("Diagramm {wertebereich}")),
                        ..Default::default()
                    });
                } else if thema == "bbt" && werte == "ew" || werte == "mw" {
                    let wertebereich = match werte {
                        "ew" => "Bodentemperatur als Tiefenprofil",
                        "mw" => "Tagesmittelwerte der Bodentemperatur der einzelnen Bodentiefen",
                        _ => "keiner",
                    };

                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        url: url_parameter_fig.into(),
                        description: Some(format!("Diagramm {wertebereich}")),
                        ..Default::default()
                    });
                }

                if thema == "bbf" && werte == "ew" {
                    let wertebereich = match werte {
                        "ew" => "Einzelwerte Bodenfeuchte",
                        _ => "keiner",
                    };

                    resources.push(Resource {
                        r#type: ResourceType::Csv,
                        url: url_parameter_down.into(),
                        description: Some(format!("Download {wertebereich}")),
                        primary_content: true,
                        ..Default::default()
                    });
                } else if thema == "bbt" && werte == "ew" {
                    let wertebereich = match werte {
                        "ew" => "Einzelwerte Bodentemperatur",
                        _ => "keiner",
                    };

                    resources.push(Resource {
                        r#type: ResourceType::Csv,
                        url: url_parameter_down.into(),
                        description: Some(format!("Download {wertebereich}")),
                        primary_content: true,
                        ..Default::default()
                    });
                }
            }
        }

        let types = smallvec![Type::Measurements {
            domain: Domain::Soil,
            station: Some(Station {
                id: Some(name.as_str().into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Bodenfeuchte".to_owned(), "Bodentemparatur".to_owned()],
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description,
            types,
            regions: smallvec![region],
            bounding_boxes,
            organisations,
            resources,
            language: Language::German,
            license: License::DlDeBy20,
            origins: source.origins.clone(),
            source_url: href,
            tags: vec![Tag::Other("Wasserportal Berlin".into())],
            ..Default::default()
        };

        Ok((name, dataset))
    }

    async fn details(
        _client: &Client,
        _source: &Source,
        _selectors: &Selectors,
        _id: &str,
        _dataset: &mut Dataset,
    ) -> Result<()> {
        Ok(())
    }
}

struct Ground;

impl ExtractStation for Ground {
    const TABLE: &'static str = "gw";

    fn overview(
        source: &Source,
        selectors: &Selectors,
        proj: &dyn Projection,
        row: ElementRef,
    ) -> Result<(String, Dataset)> {
        let mut cells = row.select(&selectors.cells);

        let title;
        let href;
        let id;
        {
            let site_id = cells
                .next()
                .ok_or_else(|| anyhow!("Missing ID column"))?
                .select(&selectors.link)
                .next()
                .ok_or_else(|| anyhow!("Missing link"))?;

            let source_url = site_id.attr("href").unwrap().to_owned();

            id = site_id.text().collect::<String>();

            href = if source_url.starts_with("station.php") {
                source.url.join(&source_url)?.into()
            } else {
                source_url.clone()
            };

            title = format!("Grundwasser-Messstelle {id}");
        }

        let region_name = collect_text(
            cells
                .next()
                .ok_or_else(|| anyhow!("Missing district column"))?
                .text(),
        );

        let region = Region::Other(region_name.into());

        let organisations = {
            let mut operator = cells
                .next()
                .ok_or_else(|| anyhow!("Missing operator column"))?
                .text()
                .collect::<String>();

            operator = format!("{} Berlin", operator);

            smallvec![Organisation::Other {
                name: operator.clone(),
                role: OrganisationRole::Operator,
                websites: Default::default(),
            }]
        };

        let description;
        let mut resources = smallvec![Resource {
            r#type: ResourceType::Pdf,
            url: source
                .url
                .join("/download/wasserportal_berlin_getting_data.pdf")?
                .into(),
            description: Some("API-Dokumentation".to_owned()),
            ..Default::default()
        }];

        let mut types = SmallVec::new();

        {
            let kind = cells
                .next()
                .ok_or_else(|| anyhow!("Missing kind column"))?
                .text()
                .collect::<String>();

            let aquifer = cells
                .next()
                .ok_or_else(|| anyhow!("Missing aquifer column"))?
                .text()
                .collect::<String>();

            let gok = cells
                .next()
                .ok_or_else(|| anyhow!("Missing GOK column"))?
                .text()
                .collect::<String>();

            let rok = cells
                .next()
                .ok_or_else(|| anyhow!("Missing ROK column"))?
                .text()
                .collect::<String>();

            let fok = cells
                .next()
                .ok_or_else(|| anyhow!("Missing FOK column"))?
                .text()
                .collect::<String>();

            let fuk = cells
                .next()
                .ok_or_else(|| anyhow!("Missing FUK column"))?
                .text()
                .collect::<String>();

            description = Some(format!(
                r"Die Messstelle dient der Überwachung des Grundwassers.
Überwacht wird der {aquifer}.
Die Höhe der Geländeoberkante (GOK) liegt bei {gok} m über NHN.
Die Rohroberkante (ROK) liegt bei {rok} m über NHN.
Die Filteroberkante (FOK) liegt bei {fok} m unter GOK.
Die Filterunterkante (FUK) liegt bei {fuk} m unter GOK."
            ));

            for parameter in ["gws", "gwq"] {
                let wert = "ew";

                let mut url_figures = source.url.join("/station.php")?;
                url_figures
                    .query_pairs_mut()
                    .append_pair("anzeige", "g")
                    .append_pair("sgrafik", wert)
                    .append_pair("station", &id)
                    .append_pair("thema", parameter);

                let mut url_downloads = source.url.join("/station.php")?;
                url_downloads
                    .query_pairs_mut()
                    .append_pair("anzeige", "d")
                    .append_pair("station", &id)
                    .append_pair("smode", "c")
                    .append_pair("thema", parameter)
                    .append_pair("exportthema", "gw")
                    .append_pair("sreihe", wert)
                    .append_pair("sdatum", "01.01.1900");

                let parameter_description = match parameter {
                    "gws" => "Grundwasserstand",
                    "gwq" => "Grundwasserqualität",
                    _ => "keiner",
                };

                if parameter == "gws" && kind.contains("GW-Stand") {
                    types.push(Type::Measurements {
                        domain: Domain::Groundwater,
                        station: Some(Station {
                            id: Some(id.as_str().into()),
                            ..Default::default()
                        }),
                        measured_variables: smallvec!["Wasserstand".to_owned()],
                        methods: Default::default(),
                    });

                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        url: url_figures.into(),
                        description: Some(format!("Diagramm {parameter_description}")),
                        ..Default::default()
                    });

                    resources.push(Resource {
                        r#type: ResourceType::Csv, // Following API documentation, "smode" is always "c"
                        url: url_downloads.into(),
                        description: Some(format!("Download {parameter_description}")),
                        primary_content: true,
                        ..Default::default()
                    });
                } else if parameter == "gwq" && kind.contains("GW-Güte") {
                    types.push(Type::Measurements {
                        domain: Domain::Chemistry,
                        station: Some(Station {
                            id: Some(id.as_str().into()),
                            ..Default::default()
                        }),
                        measured_variables: smallvec!["Grundwasserqualität".to_owned()],
                        methods: Default::default(),
                    });

                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        url: url_figures.clone().into(),
                        description: Some(format!("Diagramm {parameter_description}")),
                        ..Default::default()
                    });
                }
            }
        }

        let bounding_boxes = {
            let longitude = cells
                .next()
                .ok_or_else(|| anyhow!("Missing longitude column"))?
                .text()
                .collect::<CompactString>()
                .parse::<f64>()?;

            let latitude = cells
                .next()
                .ok_or_else(|| anyhow!("Missing latitude column"))?
                .text()
                .collect::<CompactString>()
                .parse::<f64>()?;

            smallvec![proj_rect(
                point_like_bounding_box(latitude, longitude),
                proj
            )]
        };

        let dataset = Dataset {
            title,
            description,
            types,
            regions: smallvec![region],
            bounding_boxes,
            organisations,
            resources,
            language: Language::German,
            license: License::DlDeBy20,
            origins: source.origins.clone(),
            source_url: href,
            tags: vec![Tag::Other("Wasserportal Berlin".into())],
            ..Default::default()
        };

        Ok((id, dataset))
    }

    async fn details(
        client: &Client,
        source: &Source,
        selectors: &Selectors,
        id: &str,
        dataset: &mut Dataset,
    ) -> Result<()> {
        let thema = if dataset
            .resources
            .iter()
            .any(|resource| resource.url.contains("&thema=gwq"))
        {
            "gwq"
        } else {
            "gws"
        };

        let mut url = source.url.join("/station.php")?;
        url.query_pairs_mut()
            .append_pair("anzeige", "d")
            .append_pair("thema", thema)
            .append_pair("station", id);

        let text = client
            .fetch_text(source, format!("gw-{id}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let from = document
            .select(&selectors.from)
            .next()
            .ok_or_else(|| anyhow!("Missing from"))?
            .attr("value")
            .unwrap()
            .parse::<GermanDate>()?;

        let until = document
            .select(&selectors.until)
            .next()
            .ok_or_else(|| anyhow!("Missing until"))?
            .attr("value")
            .unwrap()
            .parse::<GermanDate>()?;

        dataset.time_ranges.push((from, until).into());

        Ok(())
    }
}

selectors! {
    rows: "table#pegeltab > tbody > tr",
    cells: "td",
    link: "a[href]",
    from: "input#input_1_11b[value]",
    until: "input#input_1_11c[value]",
}
