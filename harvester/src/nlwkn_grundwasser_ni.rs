use anyhow::Result;
use cap_std::fs::Dir;
use scraper::{Html, Selector};
use smallvec::smallvec;
use time::{format_description::FormatItem, macros::format_description, Date, Month};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, contains_or, point_like_bounding_box, yesterday},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{Domain, Station, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) = get_main_pages(dir, client, source).await?;

    let (count1, results1, errors1) = get_station_data(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn get_main_pages(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let title = "Interaktive Karte der Grundwassermessstellen des NLWKN".to_owned();

    let description = r#"Die Karte bietet einen Überblick der aktuellen Pegelstände aller Grundwassermessstellen in Niedersachsen.
Die Detailansicht der Stationen gibt weitere Informationen.

Die dargestellten Grundwassermessstellen informieren anhand tagesaktueller Daten über die landesweite Situation des Grundwasserstands und sollen die natürliche, witterungs- und klimatisch bedingte Grundwasserstandsentwicklung sichtbar machen. Die Farbe der Grundwassermessstellen entspricht der Klassifikation des aktuellen Grundwasserstands. Diese ermöglicht eine Einordnung der tagesaktuellen Daten in die langjährige Grundwasserstandsdynamik an der jeweiligen Grundwassermessstelle.
Der Referenzzeitraum ist i.d.R. 1991-2020."#.to_owned();

    let from = Date::from_calendar_date(1991, Month::January, 1)?;
    let time_ranges = smallvec![(from, yesterday()).into()];

    let dataset = Dataset {
        title,
        description: Some(description),
        time_ranges,
        regions: smallvec![Region::NI],
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: source.url.join("Karte")?.into(),
        types: smallvec![Type::MapService],
        ..Default::default()
    };
    write_dataset(dir, client, source, "karte".to_owned(), dataset).await?;

    let title =
        "Übersicht aller aktuellen Messwerte  der Grundwassermessstellen des NLWKN".to_owned();

    let description = r#"Tabelarische Darstellung der Grundwassermessstellen in Niedersachsen.

Zu den Grundwassermessstellen sind die folgenden Informationen gelistet:
- Grundwassermessstelle
- ID
- Landkreis
- Betreiber
- Datum
- Grundwasserstand [m NHN]
- Grundwasserstand [m u. GOK]
- Klassifikation des Messwertes


Die Detailseiten der Grundwassermessstellen sind verlinkt."#
        .to_owned();

    let resources = smallvec![Resource {
        r#type: ResourceType::WebPage,
        description: Some("Erläuterungen zu Anwendung sowie zur verwendeten Klassifikation des aktuellen Grundwasserstands".to_owned()),
        url: "https://www.grundwasserstandonline.nlwkn.niedersachsen.de/Hinweis".to_owned(),
        ..Default::default()
    }.guess_or_keep_type()];

    let from = Date::from_calendar_date(1991, Month::January, 1)?;
    let time_ranges = smallvec![(from, yesterday()).into()];

    let source_url = source.url.join("Messwerte")?.into();

    let types = smallvec![Type::Measurements {
        domain: Domain::Groundwater,
        station: None,
        measured_variables: smallvec!["Grundwasserstand".to_owned()],
        methods: Default::default(),
    }];

    let dataset = Dataset {
        title,
        description: Some(description),
        regions: smallvec![Region::NI],
        resources,
        time_ranges,
        license: License::OtherClosed,
        language: Language::German,
        origins: source.origins.clone(),
        source_url,
        types,
        ..Default::default()
    };
    write_dataset(dir, client, source, "messwerte".to_owned(), dataset).await?;

    Ok((2, 2, 0))
}

async fn get_station_data(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let station_data = {
        let url = source.url.join("Messwerte")?;

        let text = client
            .fetch_text(source, "messwerte".to_owned(), &url)
            .await?;
        let document = Html::parse_document(&text);

        document
            .select(&selectors.station_id)
            .map(|elem| collect_text(elem.text().to_owned()))
            .collect::<Vec<_>>()
    };

    let count = station_data.len() / 11;

    let (results, errors) = fetch_many(
        0,
        0,
        station_data.chunks_exact(11),
        |station_data| async move {
            let name = &station_data[0];

            let station_id_alt = &station_data[1];
            let station_id = &station_data[2];

            let county = &station_data[3];
            let operator = &station_data[4];

            let end_date = Date::parse(&station_data[5], DATE_FORMAT)?;

            let lon = station_data[9].parse::<f64>()?;
            let lat = station_data[10].parse::<f64>()?;

            let source_url = source.url.join(&format!("/Station/ID/{station_id_alt}"))?;

            let key = format!("station-{station_id}");

            let dataset = {
                let text = client
                    .fetch_text(source, format!("{key}.isol"), &source_url)
                    .await?;
                let document = Html::parse_document(&text);

                let title = format!("Grundwassermessstelle {name}, {station_id} im Landkreis {county}, Niedersachsen");

                let description = "Der NLWKN betreibt im Rahmen der Tätigkeiten als Gewässerkundlicher Landesdienst Niedersachsens
                    zahlreiche Grundwassermessstellen in ganz Niedersachsen,
                    die je nach Fragestellung unterschiedlichen Messnetzen und Messprogrammen innerhalb des Gewässerüberwachungssystems Niedersachsen (GÜN) zugeordnet sind.
                    \n\n
                    Auf diesem Informationsportal werden verschiedene Messwerte und Datenauswertungen veröffentlicht.
                    Die messstellenspezifischen Messwerte und Datenauswertungen liegen sowohl in tabellarischer als auch in graphischer Form vor,
                    der Darstellungszeitraum ist auswählbar.
                    Zusätzlich sind die für die Grundwassermessstellen zugrundeliegenden Stammdaten inklusive Ausbauschema und Bohrprofil verfügbar.".to_owned();

                let community = collect_text(
                    document
                        .select(&selectors.region_small)
                        .flat_map(|element| element.text()),
                );
                let region = contains_or(community, Region::NI);

                let bounding_boxes = smallvec![point_like_bounding_box(lat, lon)];

                let organisations = smallvec![Organisation::Other {
                    name: operator.to_owned(),
                    role: OrganisationRole::Operator,
                    websites: Default::default(),
                }];

                let resources = smallvec![
                    Resource {
                        r#type: ResourceType::Jpeg,
                        description: Some("Lage der Grundwassermessstelle in Niedersachsen".to_owned()),
                        url: format!("https://nlwknbiscloudprod.blob.core.windows.net/pictures/{station_id}_3.jpg"),
                        ..Default::default()
                    },
                    Resource {
                        r#type: ResourceType::Jpeg,
                        description: Some("Bild der Grundwassermessstelle".to_owned()),
                        url: format!("https://nlwknbiscloudprod.blob.core.windows.net/pictures/{station_id}_1.jpg"),
                        ..Default::default()
                    },
                    Resource {
                        r#type: ResourceType::Pdf,
                        description: Some("Datenblatt der Grundwassermessstelle".to_owned()),
                        url: format!("https://nlwknbiscloudprod.blob.core.windows.net/pdf/{station_id}.PDF"),
                        ..Default::default()
                    },
                    Resource {
                        r#type: ResourceType::Gif,
                        description: Some("Ausbauschema der Grundwassermessstelle".to_owned()),
                        url: format!("https://nlwknbiscloudprod.blob.core.windows.net/pictures/{station_id}.gif"),
                        ..Default::default()
                    }
                ];

                let start_date = collect_text(
                    document
                        .select(&selectors.start_date)
                        .flat_map(|element| element.text()),
                );

                let time_ranges = smallvec![(Date::parse(&start_date, DATE_FORMAT)?, end_date).into()];

                let types = smallvec![Type::Measurements {
                    domain: Domain::Groundwater,
                    station: Some(Station {
                        id: Some(station_id.into()),
                        ..Default::default()
                    }),
                    measured_variables: smallvec!["Grundwasserstand".to_owned()],
                    methods: Default::default(),
                }];

                Dataset {
                    title,
                    description: Some(description),
                    types,
                    regions: smallvec![region],
                    bounding_boxes,
                    organisations,
                    resources,
                    time_ranges,
                    source_url: source_url.into(),
                    origins: source.origins.clone(),
                    license: License::OtherClosed,
                    language: Language::German,
                    ..Default::default()
                }
            };

            write_dataset(dir, client, source, key, dataset).await
        },
    )
    .await;

    Ok((count, results, errors))
}

selectors! {
    station_id: "td",
    default_images: "#Ctr_Tab_MessstellenInfo_fvPegel_Image1",
    region_small: "tr:nth-child(3) .labelColumnGWInfoValue",
    start_date: "tr:nth-child(8) .labelColumnGWInfoValue",
}

const DATE_FORMAT: &[FormatItem] = format_description!("[day].[month].[year]");
