use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{selectable::Selectable, Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::Date;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_text, make_key, make_suffix_key, parse_attribute, select_text, GermanDate,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Alternative, Dataset, GlobalIdentifier, Language, License, Person, PersonRole, Resource,
    ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let subpages = [
        SubPage1 {
            path: "de/documents/selected_results",
            selector_resources: &selectors.resources_results,
            selector_persons: None,
            r#type: Type::Text {
                text_type: TextType::Report,
            },
        },
        SubPage1 {
            path: "de/documents/publications",
            selector_resources: &selectors.resources_publications,
            selector_persons: Some(&selectors.persons),
            r#type: Type::Text {
                text_type: TextType::Publication,
            },
        },
    ];

    let mut results = 0;
    let mut errors = 0;

    for subpage in &subpages {
        let (max_page, results1, errors1) =
            fetch_results(dir, client, source, selectors, 1, subpage).await?;

        let (results1, errors1) = fetch_many(results1, errors1, 2..=max_page, |page| {
            fetch_results(dir, client, source, selectors, page, subpage)
        })
        .await;
        results += results1;
        errors += errors1;
    }

    let subpages2 = [
        SubPage2 {
            path: "de/documents/10017",
            selector_links: &selectors.links,
            selector_description: &selectors.description,
            extract_resources: extract_resources_grundlagen,
            r#type: Type::Text {
                text_type: TextType::Editorial,
            },
        },
        SubPage2 {
            path: "de/documents/profiles/ecosystems",
            selector_links: &selectors.links_ecosystems,
            selector_description: &selectors.description_ecosystems,
            extract_resources: extract_resources_ecosystems,
            r#type: Type::Text {
                text_type: TextType::Editorial,
            },
        },
    ];
    for subpage in &subpages2 {
        let (results2, errors2) =
            fetch_grundlagen_ökosysteme(dir, client, source, selectors, subpage).await?;
        results += results2;
        errors += errors2;
    }

    Ok((results, results, errors))
}

struct SubPage1<'a> {
    path: &'a str,
    selector_resources: &'a Selector,
    selector_persons: Option<&'a Selector>,
    r#type: Type,
}

struct SubPage2<'a> {
    path: &'a str,
    selector_links: &'a Selector,
    selector_description: &'a Selector,
    extract_resources: ExtractResources,
    r#type: Type,
}

async fn fetch_results(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
    sub_page: &SubPage1<'_>,
) -> Result<(usize, usize, usize)> {
    let links;
    let max_page;

    {
        let mut url = source.url.join(sub_page.path)?;
        url.query_pairs_mut().append_pair("page", &page.to_string());

        let key = format!("{}-page-{}", make_key(sub_page.path), page);

        let text = client.fetch_text(source, key, &url).await?;
        let document = Html::parse_document(&text);

        links = document
            .select(&selectors.links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>();

        max_page = document
            .select(&selectors.last_page)
            .map(|element| collect_text(element.text()))
            .collect::<Vec<_>>()
            .iter()
            .filter_map(|s| s.parse::<usize>().ok())
            .max()
            .ok_or_else(|| anyhow!("No maximum page found"))?;
    }

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        fetch_details(dir, client, source, selectors, link, sub_page).await
    })
    .await;

    Ok((max_page, results, errors))
}

async fn fetch_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: String,
    sub_page: &SubPage1<'_>,
) -> Result<(usize, usize, usize)> {
    let dataset;
    let key;
    {
        let url = source.url.join(&link)?;
        key = make_suffix_key(&link, &source.url).into_owned();

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        let title = collect_text(
            document
                .select(&selectors.title)
                .next()
                .ok_or_else(|| anyhow!("Missing title"))?
                .text(),
        );

        let description = select_text(&document, &selectors.description);

        let modified = document
            .select(&selectors.modified_results)
            .next()
            .map(|element| collect_text(element.text()))
            .and_then(|text| {
                text.strip_prefix("Aktualisiert am:")
                    .map(|suffix| suffix.trim().parse::<GermanDate>())
            })
            .transpose()?
            .map(|date| date.into());

        let mut issued = document
            .select(&selectors.issued_publications)
            .next()
            .map(|element| collect_text(element.text()))
            .and_then(|text| {
                text.rsplit_once("online:")
                    .map(|(_prefix, suffix)| suffix.trim().parse::<GermanDate>())
            })
            .transpose()?
            .map(|date| date.into());

        if issued.is_none() {
            issued = document
                .select(&selectors.issued_year)
                .next()
                .map(|element| collect_text(element.text()))
                .and_then(|text| {
                    text.rsplit_once(',')
                        .map(|(prefix, _suffix)| prefix.trim().parse::<i32>())
                })
                .transpose()?
                .map(|year| Date::from_ordinal_date(year, 1))
                .transpose()?;
        }

        let mut resources = document
            .select(sub_page.selector_resources)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = collect_text(element.text());
                let primary_content = href.contains("doi.org");

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    primary_content,
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        for (src_elem, title_elem) in document
            .select(&selectors.resources_figure_links)
            .zip(document.select(&selectors.resources_figure_titles))
        {
            let href = src_elem.attr("href").unwrap();
            let description = collect_text(title_elem.text());

            resources.push(Resource {
                r#type: ResourceType::InteractivePlot,
                description: Some(description),
                url: source.url.join(href)?.into(),
                primary_content: true,
                ..Default::default()
            });
        }

        let persons = if let Some(element) = sub_page
            .selector_persons
            .and_then(|selector| document.select(selector).next())
        {
            element
                .attr("content")
                .unwrap()
                .split(';')
                .map(|name| Person {
                    name: name.trim().to_owned(),
                    role: PersonRole::Creator,
                    ..Default::default()
                })
                .collect()
        } else {
            Default::default()
        };

        let global_identifier = parse_attribute(
            &document,
            &selectors.doi,
            &selectors.doi_value,
            "href",
            "DOI",
        )
        .ok()
        .map(GlobalIdentifier::Doi);

        dataset = Dataset {
            title,
            description: Some(description),
            global_identifier,
            language: Language::German,
            alternatives: vec![Alternative::Language {
                language: Language::English,
                url: url.as_str().replace("/de/", "/en/"),
            }],
            license: License::OtherClosed,
            origins: source.origins.clone(),
            resources,
            persons,
            modified,
            issued,
            source_url: url.into(),
            types: smallvec![sub_page.r#type.clone()],
            ..Default::default()
        };
    }

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_grundlagen_ökosysteme(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    sub_page: &SubPage2<'_>,
) -> Result<(usize, usize)> {
    let hrefs = {
        let url = source.url.join(sub_page.path)?;
        let key = make_key(url.path()).into_owned();
        let text = client.fetch_text(source, key, &url).await?;
        let document = Html::parse_document(&text);

        document
            .select(sub_page.selector_links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };
    let (results, errors) = fetch_many(0, 0, hrefs, |href| {
        fetch_item(dir, client, source, selectors, href, sub_page)
    })
    .await;

    Ok((results, errors))
}

async fn fetch_item(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    href: String,
    sub_page: &SubPage2<'_>,
) -> Result<(usize, usize, usize)> {
    let key;
    let dataset;
    {
        let url = source.url.join(&href)?;
        key = make_key(url.path()).into_owned();

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        let title = collect_text(
            document
                .select(&selectors.title)
                .next()
                .ok_or_else(|| anyhow!("Missing title"))?
                .text(),
        );

        let description = select_text(&document, sub_page.selector_description);

        let resources = (sub_page.extract_resources)(source, selectors, &document)?;

        dataset = Dataset {
            title,
            description: Some(description),
            language: Language::German,
            alternatives: vec![Alternative::Language {
                language: Language::English,
                url: url.as_str().replace("/de/", "/en/"),
            }],
            license: License::OtherClosed,
            origins: source.origins.clone(),
            resources,
            source_url: url.into(),
            types: smallvec![sub_page.r#type.clone()],
            ..Default::default()
        };
    }
    write_dataset(dir, client, source, key, dataset).await
}

type ExtractResources = fn(&Source, &Selectors, &Html) -> Result<SmallVec<[Resource; 4]>>;

fn extract_resources_grundlagen(
    source: &Source,
    selectors: &Selectors,
    document: &Html,
) -> Result<SmallVec<[Resource; 4]>> {
    let mut resources = document
        .select(&selectors.resources_grundlagen)
        .map(|element| {
            let href = element.attr("href").unwrap();
            let description = collect_text(element.text());

            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: Some(description),
                url: source.url.join(href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect::<Result<SmallVec<_>>>()?;

    resources.extend(
        document
            .select(&selectors.resources_videos)
            .zip(document.select(&selectors.resources_video_titles))
            .map(|(src_elem, title_elem)| {
                let src = src_elem.attr("src").unwrap();
                let description = collect_text(title_elem.text());

                Resource {
                    r#type: ResourceType::Video,
                    description: Some(description),
                    url: src.to_owned(),
                    primary_content: true,
                    ..Default::default()
                }
                .guess_or_keep_type()
            }),
    );

    Ok(resources)
}

fn extract_resources_ecosystems(
    source: &Source,
    selectors: &Selectors,
    document: &Html,
) -> Result<SmallVec<[Resource; 4]>> {
    document
        .select(&selectors.resources_ecosystems)
        .map(|element| {
            let href = element
                .select(&selectors.resources_ecosystems_links)
                .next()
                .ok_or_else(|| anyhow!("Missing href"))?
                .attr("href")
                .unwrap();
            let description = collect_text(element.text());

            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: Some(description),
                url: source.url.join(href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect()
}

selectors! {
    links: "#main h2 a[href]",
    links_ecosystems: ".profile",
    last_page: ".pagination a",
    title: "h1, .summary",
    description: "main p",
    description_ecosystems: "#desc_text",
    modified_results: "em",
    issued_publications: "p.summary strong",
    issued_year: ".created",
    persons: "meta[name='author'][content]",
    resources_grundlagen: ".internal",
    resources_results: "li a.profile[href], ul.bullet li a[href]",
    resources_figure_links: "div.cap_large + div a[href]",
    resources_figure_titles: "div.cap_large div.icaption",
    resources_publications: ".external, p a.pdf[href], p a[href$='.pdf']",
    resources_videos: "div.embed-responsive iframe[src]",
    resources_video_titles: "div.embed-responsive + p",
    resources_ecosystems_links: ".profile[href]",
    resources_ecosystems: "li.list-group-item div.row",
    doi: "a[href*='doi.org/']",
    doi_value: r"doi.org/(.+)" as Regex,
}
