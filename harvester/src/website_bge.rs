use std::hash::{Hash, Hasher};

use anyhow::{Context, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::HashSet;
use poppler::Document;
use regex::Regex;
use reqwest::header::{HeaderValue, USER_AGENT};
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};

use harvester::{
    client::{CachedDocument, Client},
    fetch_many, selectors,
    utilities::{collect_pages, collect_text, make_key, parse_text, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Person, PersonRole, Region,
    Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    // HACK: www.bge.de blocks anything with the word "harvester" in the user agent...
    client.insert_header(
        USER_AGENT,
        HeaderValue::from_static("umwelt.info h4rv3573r"),
    );

    let selectors = &Selectors::default();
    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    let items = AtomicRefCell::new(HashSet::new());

    let (count_de, results_de, errors_de) =
        fetch_search(client, source, selectors, &items, "de/suche/").await?;
    count += count_de;
    results += results_de;
    errors += errors_de;

    let (count_en, results_en, errors_en) =
        fetch_search(client, source, selectors, &items, "en/search/").await?;
    count += count_en;
    results += results_en;
    errors += errors_en;

    let items = items.into_inner();
    count += items.len();

    let (results, errors) = fetch_many(results, errors, items, |item| {
        fetch_doc(dir, client, source, selectors, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_search(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    items: &AtomicRefCell<HashSet<Item>>,
    path: &str,
) -> Result<(usize, usize, usize)> {
    let (count, results, errors) =
        fetch_search_page(client, source, selectors, items, path, 1).await?;

    let pages = count.div_ceil(BATCH_SIZE);

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_search_page(client, source, selectors, items, path, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_search_page(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    items: &AtomicRefCell<HashSet<Item>>,
    path: &str,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let count;
    let results;

    {
        let mut url = source.url.join(path)?;

        url.query_pairs_mut()
            .append_pair("tx_solr[page]", &page.to_string());

        let key = if path == "en/search/" {
            format!("page-{page}")
        } else {
            format!("seite-{page}")
        };

        let text = client.fetch_text(source, key, &url).await?;

        let document = Html::parse_document(&text);

        count = parse_text(
            &document,
            &selectors.count,
            if path == "en/search/" {
                &selectors.count_value_en
            } else {
                &selectors.count_value_de
            },
            "count",
        )?;

        let mut items = items.borrow_mut();

        let old_len = items.len();

        items.extend(
            document
                .select(&selectors.items)
                .map(|item| Item {
                    title: item.attr("title").unwrap().to_owned(),
                    href: item.attr("href").unwrap().replace("?searchWord=", ""),
                })
                .filter(|item| !item.href.is_empty()),
        );

        let new_len = items.len();

        results = new_len - old_len;
    }

    Ok((count, results, 0))
}

async fn fetch_doc(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.href).into_owned();

    let dataset = {
        let url = source.url.join(&item.href)?;

        let doc = client
            .fetch_doc(source, format!("{key}.isol"), &url)
            .await?;

        // items are derived from the internal search of bge.de and some no longer refer to live webpages
        if matches!(item.title.as_str(), "403" | "404" | "Seite nicht gefunden")
            || item.href.contains("/Stellenausschreibungen")
        {
            return Ok((1, 0, 0));
        }

        let description;
        let r#type;
        let mut resources = SmallVec::new();

        match &*doc {
            CachedDocument::Html(text) => {
                let document = Html::parse_document(text);

                description = select_text(&document, &selectors.description);

                // approximately 60 items we get from the search list have empty descriptions
                // they appear to be irrelevant pages listing other pages without any further content
                if description.is_empty() {
                    return Ok((1, 0, 0));
                }

                r#type = if item.href.contains("/blog") {
                    Type::Text {
                        text_type: TextType::BlogPost,
                    }
                } else if item.href.contains("/meldungen-und-pressemitteilungen")
                    || item.href.contains("/presse")
                    || item.href.contains("/en/press")
                {
                    Type::Text {
                        text_type: TextType::PressRelease,
                    }
                } else if item.href.contains("/meldungen")
                    || item.href.contains("/newsletter")
                    || item.href.contains("/news")
                {
                    Type::Text {
                        text_type: TextType::News,
                    }
                } else if item.href.contains("/unterlagen") {
                    Type::Text {
                        text_type: TextType::Report,
                    }
                } else {
                    Type::Text {
                        text_type: TextType::Editorial,
                    }
                };

                resources = document
                    .select(&selectors.resources)
                    .map(|element| {
                        let href = element.attr("href").unwrap();
                        let description = collect_text(element.text());

                        Ok(Resource {
                            r#type: ResourceType::Pdf,
                            description: Some(description),
                            url: source.url.join(href)?.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type())
                    })
                    .collect::<Result<SmallVec<_>>>()?;
            }
            CachedDocument::Pdf(bytes) => {
                let document = Document::from_bytes(bytes, None)
                    .with_context(|| format!("Failed to parse PDF at `{}`", url))?;

                description = collect_pages(&document, 0..3);

                r#type = Type::Text {
                    text_type: TextType::Unspecified,
                };
            }
        }

        let region = Region::GeoName(2855047); // Peine

        let provider = Organisation::WikiData {
            identifier: 26794436, // BGE
            role: OrganisationRole::Provider,
        };

        // Zitat bge.de/de/impressum/: "Die auf diesen Seiten veröffentlichten Inhalte unterliegen grundsätzlich dem deutschen Urheber- und Leistungsschutzrecht."
        let license = License::OtherClosed;

        let contact = Person {
            // retrieved from https://www.bge.de/de/kontakt/
            name: "Öffentlichkeitsarbeit".to_owned(),
            role: PersonRole::Contact,
            emails: smallvec!["dialog(at)bge.de".to_owned()],
            ..Default::default()
        };

        let language = if item.href.starts_with("/de/leichte-sprache") {
            Language::GermanEasy
        } else if item.href.starts_with("/en") {
            Language::English
        } else {
            Language::German
        };

        Dataset {
            title: item.title,
            description: Some(description),
            types: smallvec![r#type],
            resources,
            regions: smallvec![region],
            organisations: smallvec![provider],
            persons: vec![contact],
            license,
            language,
            origins: source.origins.clone(),
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

const BATCH_SIZE: usize = 10;

struct Item {
    title: String,
    href: String,
}

impl PartialEq for Item {
    fn eq(&self, other: &Self) -> bool {
        self.href == other.href
    }
}

impl Eq for Item {}

impl Hash for Item {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        self.href.hash(hasher);
    }
}

selectors! {
    count: "span.result-found",
    count_value_de: r"Es wurden (\d+) Ergebnisse gefunden\." as Regex,
    count_value_en: r"Found (\d+) results\." as Regex,
    items: "div.results-list > div > h3 > a[title][href]",
    description: ".ce-bodytext",
    resources: "a.uploads-filelink.list-style-links[href]",
}
