use std::mem::take;
use std::time::Duration;

use anyhow::{anyhow, ensure, Context, Error, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use csv::ReaderBuilder;
use fantoccini::{
    actions::{InputSource, MouseActions, PointerAction},
    Locator,
};
use geo::Rect;
use hashbrown::HashMap;
use miniproj::{get_projection, Projection};
use serde::Deserialize;
use smallvec::{smallvec, SmallVec};
use time::{serde::format_description, Date};
use tokio::fs::read_to_string;

use harvester::{
    browser::Browser,
    client::Client,
    fetch_many,
    utilities::{contains_or, make_key, point_like_bounding_box},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Region,
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25832).unwrap();

    let counties = fetch_level_counties(client, source)
        .await
        .context("Level counties download failed")?;

    let level_bounding_boxes = &AtomicRefCell::new(HashMap::new());

    let (results, errors) = fetch_many(0, 0, counties, |county| async move {
        let text = client
            .make_request(
                source,
                format!("county-level-{}.isol", make_key(&county)),
                Some(&source.url),
                |_client| download_level_csv(source, &county),
            )
            .await
            .with_context(|| format!("Level CSV download for {county} failed"))?;

        let records = ReaderBuilder::new()
            .delimiter(b';')
            .from_reader(text.as_bytes())
            .into_deserialize::<LevelRecord>();

        let mut results = 0;
        let mut errors = 0;

        let mut batch = Vec::<LevelRecord>::new();

        for record in records {
            let record = record?;

            if batch
                .last()
                .is_some_and(|last| last.station != record.station)
            {
                match translate_level_record_batch(
                    dir,
                    client,
                    source,
                    proj,
                    level_bounding_boxes,
                    &mut batch,
                )
                .await
                {
                    Ok((_, results1, errors1)) => {
                        results += results1;
                        errors += errors1;
                    }
                    Err(err) => {
                        tracing::error!("Failed to translate level record batch: {err:#}");

                        errors += 1;
                    }
                }

                batch.clear();
            }

            batch.push(record);
        }

        match translate_level_record_batch(
            dir,
            client,
            source,
            proj,
            level_bounding_boxes,
            &mut batch,
        )
        .await
        {
            Ok((_, results1, errors1)) => {
                results += results1;
                errors += errors1;
            }
            Err(err) => {
                tracing::error!("Failed to translate level record batch: {err:#}");

                errors += 1;
            }
        }

        Ok((results, results, errors))
    })
    .await;

    let counties = fetch_counties(client, source)
        .await
        .context("Counties download failed")?;

    let level_bounding_boxes = &level_bounding_boxes.borrow();

    let (results, errors) = fetch_many(results, errors, counties, |county| async move {
        let text = client
            .make_request(
                source,
                format!("county-{}.isol", make_key(&county)),
                Some(&source.url),
                |_client| download_csv(source, &county),
            )
            .await
            .with_context(|| format!("CSV download for {county} failed"))?;

        let records = ReaderBuilder::new()
            .delimiter(b';')
            .from_reader(text.as_bytes())
            .into_deserialize::<Record>();

        let mut results = 0;
        let mut errors = 0;

        let mut batch = Vec::<Record>::new();

        for record in records {
            let record = record?;

            if batch
                .last()
                .is_some_and(|last| last.station != record.station)
            {
                match translate_record_batch(dir, client, source, level_bounding_boxes, &mut batch)
                    .await
                {
                    Ok((_, results1, errors1)) => {
                        results += results1;
                        errors += errors1;
                    }
                    Err(err) => {
                        tracing::error!("Failed to translate record batch: {err:#}");

                        errors += 1;
                    }
                }

                batch.clear();
            }

            batch.push(record);
        }

        match translate_record_batch(dir, client, source, level_bounding_boxes, &mut batch).await {
            Ok((_, results1, errors1)) => {
                results += results1;
                errors += errors1;
            }
            Err(err) => {
                tracing::error!("Failed to translate record batch: {err:#}");

                errors += 1;
            }
        }

        Ok((results, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "Messstelle")]
    station: String,
    #[serde(rename = "GW-Nummer")]
    groundwater: String,
    #[serde(rename = "Gemeinde")]
    municipality: String,
    #[serde(rename = "Stoffgruppe")]
    substance_group: String,
    #[serde(rename = "Parameter")]
    substance: String,
    #[serde(rename = "Messzeitpunkt", with = "timestamp_format")]
    timestamp: Date,
}

#[derive(Debug, Deserialize)]
struct LevelRecord {
    #[serde(rename = "Messstelle")]
    station: String,
    #[serde(rename = "Grundwassernummer")]
    groundwater: String,
    #[serde(rename = "Gemeinde")]
    municipality: String,
    #[serde(rename = "Nordwert")]
    latitude: String,
    #[serde(rename = "Ostwert")]
    longitude: String,
    #[serde(rename = "Komponente")]
    component: String,
    #[serde(rename = "Messzeitpunkt", with = "timestamp_format")]
    timestamp: Date,
}

format_description!(timestamp_format, Date, "[day].[month].[year]");

async fn translate_record_batch(
    dir: &Dir,
    client: &Client,
    source: &Source,
    level_bounding_boxes: &HashMap<CompactString, SmallVec<[Rect; 1]>>,
    batch: &mut [Record],
) -> Result<(usize, usize, usize)> {
    let mut substance_groups = batch
        .iter_mut()
        .map(|record| take(&mut record.substance_group))
        .collect::<Vec<_>>();
    substance_groups.sort_unstable();
    substance_groups.dedup();

    let mut substances = batch
        .iter_mut()
        .map(|record| take(&mut record.substance))
        .collect::<SmallVec<_>>();
    substances.sort_unstable();
    substances.dedup();

    let first = batch.first().unwrap();

    let key = make_key(&first.station).into_owned();

    let title = format!("Grundwassermessstelle {}", first.station);
    let description = format!(
        "Die Messstelle {} (Grundwasser-Nr: {}) befindet sich in der Gemeinde {}. Die Messstelle dient zur Erfassung der Konzentrationen abiotischer Parameter.",
        first.station,
        first.groundwater,
        first.municipality,
    );

    let min_timestamp = batch.iter().map(|record| record.timestamp).min().unwrap();
    let max_timestamp = batch.iter().map(|record| record.timestamp).max().unwrap();
    let time_ranges = smallvec![(min_timestamp, max_timestamp).into()];

    let bounding_boxes = level_bounding_boxes
        .get(first.groundwater.as_str())
        .cloned()
        .unwrap_or_default();

    let regions = smallvec![contains_or(&first.municipality, Region::BW)];

    let types = smallvec![
        Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(first.groundwater.as_str().into()),
                ..Default::default()
            }),
            measured_variables: substances.clone(),
            methods: Default::default(),
        },
        Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(first.groundwater.as_str().into()),
                ..Default::default()
            }),
            measured_variables: substances,
            methods: Default::default(),
        },
    ];

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        time_ranges,
        bounding_boxes,
        regions,
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: source.url.clone().into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn translate_level_record_batch(
    dir: &Dir,
    client: &Client,
    source: &Source,
    proj: &dyn Projection,
    level_bounding_boxes: &AtomicRefCell<HashMap<CompactString, SmallVec<[Rect; 1]>>>,
    batch: &mut [LevelRecord],
) -> Result<(usize, usize, usize)> {
    let mut components = batch
        .iter_mut()
        .map(|record| take(&mut record.component))
        .collect::<SmallVec<_>>();
    components.sort_unstable();
    components.dedup();

    let first = batch.first().unwrap();

    let key = make_key(&first.station).into_owned();

    let title = format!("Grundwassermessstelle {}", first.station);
    let description = format!(
        "Die Messstelle {} (Grundwasser-Nr: {}) in der Gemeinde {} dient der Messung des Grundwasserpegels.",
        first.station,
        first.groundwater,
        first.municipality,
    );

    let min_timestamp = batch.iter().map(|record| record.timestamp).min().unwrap();
    let max_timestamp = batch.iter().map(|record| record.timestamp).max().unwrap();
    let time_ranges = smallvec![(min_timestamp, max_timestamp).into()];

    let bounding_boxes = smallvec![proj_rect(
        point_like_bounding_box(
            first.latitude.replace('.', "").parse()?,
            first.longitude.replace('.', "").parse()?
        ),
        proj
    )];

    level_bounding_boxes
        .borrow_mut()
        .entry_ref(first.groundwater.as_str())
        .or_insert_with(|| bounding_boxes.clone());

    let regions = smallvec![contains_or(&first.municipality, Region::BW)];

    let types = smallvec![Type::Measurements {
        domain: Domain::Groundwater,
        station: Some(Station {
            id: Some(first.groundwater.as_str().into()),
            ..Default::default()
        }),
        measured_variables: components,
        methods: Default::default(),
    }];

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        time_ranges,
        bounding_boxes,
        regions,
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: source.url.clone().into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_counties(client: &Client, source: &Source) -> Result<Vec<String>> {
    let text = client
        .make_request(
            source,
            "counties".to_owned(),
            Some(&source.url),
            |_client| async move {
                let browser = Browser::open().await?;

                open_county_filter(source, &browser).await?;

                // turn the data table contents into text for caching
                let mut counties = Vec::new();

                for element in browser.find_all(Locator::Css("d-condition-table-modal table.dataTable tbody tr:has(td > input[type='checkbox']) td[class]")).await? {
                    counties.push(element.text().await?);
                }

                ensure!(!counties.is_empty(), "No counties fetched");

                Ok::<_, Error>(counties.join("\n"))
            },
        )
        .await
        .context("Failed to fetch counties")?;

    Ok(text.lines().map(|county| county.to_owned()).collect())
}

async fn download_csv(source: &Source, county: &str) -> Result<String> {
    let browser = Browser::open().await?;

    browser.clear_downloads().await?;

    open_county_filter(source, &browser).await?;

    // select the checkbox matching the county
    let mut matching_element = None;

    for element in browser
        .find_all(Locator::Css(
            "d-condition-table-modal table.dataTable tbody tr:has(td > input[type='checkbox'])",
        ))
        .await?
    {
        let text = element
            .find(Locator::Css("td[class]"))
            .await?
            .text()
            .await?;

        if text == county {
            matching_element = Some(
                element
                    .find(Locator::Css("td > input[type='checkbox']"))
                    .await?,
            );

            break;
        }
    }

    matching_element
        .ok_or_else(|| anyhow!("No checkbox matching {} found!", county))?
        .click()
        .await?;

    // select "Übernehmen"
    browser
        .wait()
        .for_element(Locator::Css(
            "d-condition-table-modal footer button.button-primary",
        ))
        .await?
        .click()
        .await?;

    // move pointer to toolbar
    let element = browser
        .find(Locator::Css("d-worksheet-view > d-worksheet-view-toolbar"))
        .await?;

    browser
        .perform_actions(
            MouseActions::new("mouse".to_owned()).then(PointerAction::MoveToElement {
                element,
                duration: Some(Duration::from_secs(5)),
                x: 0,
                y: 0,
            }),
        )
        .await?;

    // Select the "Exportieren" icon in the "Messwerte" window
    browser
        .wait()
        .for_element(Locator::Css(
            "d-worksheet-view > d-worksheet-view-toolbar button[title='Exportieren']",
        ))
        .await?
        .click()
        .await?;

    // Select "CSV-Datei"
    let mut matching_element = None;

    for element in browser
        .find_all(Locator::Css("div.d-menu.d-popup button"))
        .await?
    {
        if element.text().await? == "CSV-Datei" {
            matching_element = Some(element);

            break;
        }
    }

    matching_element
        .ok_or_else(|| anyhow!("CSV file button not found"))?
        .click()
        .await?;

    // Confirm large downloads
    if let Ok(element) = browser
        .wait()
        .at_most(Duration::from_secs(10))
        .for_element(Locator::Css(
            "d-file-export-with-can-export-progress-dialog footer button.button-primary",
        ))
        .await
    {
        element.click().await?;
    }

    let text = browser
        .wait_for_download(|entry| async move {
            let file_name = entry.file_name();
            let file_name = file_name.as_encoded_bytes();

            if !file_name.starts_with(b"Messwerte") || !file_name.ends_with(b".csv") {
                return Ok(None);
            }

            let text = read_to_string(entry.path()).await?;

            Ok(Some(text))
        })
        .await?;

    Ok(text)
}

async fn open_county_filter(source: &Source, browser: &Browser) -> Result<()> {
    browser.goto(source.url.as_str()).await?;

    // click the "Wasser" button
    browser
        .wait()
        .for_element(Locator::Css("a#d-nav-tree-node_ROOT-Wasser_firstContent"))
        .await?
        .click()
        .await?;

    // click the "Grundwasser" button
    browser
        .wait()
        .for_element(Locator::Css(
            "a#d-nav-tree-node_ROOT-Wasser-Grundwasser_firstContent",
        ))
        .await?
        .click()
        .await?;

    // click the "Grundwassergüte" button
    browser
        .wait()
        .for_element(Locator::Css("a#d-nav-tree-node_ROOT-Wasser-Grundwasser-uXwTw947n7bRQG4iOuXI-ElW90OFKM4Yp6SuaXMUC_firstContent"))
        .await?
        .click()
        .await?;

    // click the "Messwerttabelle" button
    browser
        .wait()
        .for_element(Locator::Css("d-cadenza-link[data-workbook-name='ElW90OFKM4Yp6SuaXMUC'][data-worksheet-id='uLSUz2jIT4XNn9Ftr2gN']"))
        .await?
        .click()
        .await?;

    // wait for the main table to load
    browser
        .wait()
        .for_element(Locator::Css(
            "d-worksheet-grid d-embedded-content d-workbook-table-view",
        ))
        .await?;

    // click the "Erweiterte Auswahl" button below "Kreis"
    browser
        .wait()
        .for_element(Locator::Css(
            "d-table-condition[condition-label='Kreis'] button[title='Erweiterte Auswahl']",
        ))
        .await?
        .click()
        .await?;

    // wait for the data table to appear
    browser
        .wait()
        .for_element(Locator::Css(
            "d-condition-table-modal table.dataTable tbody",
        ))
        .await?;

    Ok(())
}

async fn fetch_level_counties(client: &Client, source: &Source) -> Result<Vec<String>> {
    let text = client
        .make_request(
            source,
            "counties-level".to_owned(),
            Some(&source.url),
            |_client| async move {
                let browser = Browser::open().await?;

                open_level_county_filter(source, &browser).await?;

                // turn the data table contents into text for caching
                let mut counties = Vec::new();

                for element in browser.find_all(Locator::Css("d-condition-table-modal table.dataTable tbody tr:has(td > input[type='checkbox']) td[class]")).await? {
                    counties.push(element.text().await?);
                }

                ensure!(!counties.is_empty(), "No level counties fetched");

                Ok::<_, Error>(counties.join("\n"))
            },
        )
        .await
        .context("Failed to fetch counties")?;

    Ok(text.lines().map(|county| county.to_owned()).collect())
}

async fn download_level_csv(source: &Source, county: &str) -> Result<String> {
    let browser = Browser::open().await?;

    browser.clear_downloads().await?;

    open_level_county_filter(source, &browser).await?;

    // select the checkbox matching the county
    let mut matching_element = None;

    for element in browser
        .find_all(Locator::Css(
            "d-condition-table-modal table.dataTable tbody tr:has(td > input[type='checkbox'])",
        ))
        .await?
    {
        let text = element
            .find(Locator::Css("td[class]"))
            .await?
            .text()
            .await?;

        if text == county {
            matching_element = Some(
                element
                    .find(Locator::Css("td > input[type='checkbox']"))
                    .await?,
            );

            break;
        }
    }

    matching_element
        .ok_or_else(|| anyhow!("No checkbox matching {} found!", county))?
        .click()
        .await?;

    // select "Übernehmen"
    browser
        .wait()
        .for_element(Locator::Css(
            "d-condition-table-modal footer button.button-primary",
        ))
        .await?
        .click()
        .await?;

    // move pointer to toolbar
    let element = browser
        .find(Locator::Css("d-worksheet-view > d-worksheet-view-toolbar"))
        .await?;

    browser
        .perform_actions(
            MouseActions::new("mouse".to_owned()).then(PointerAction::MoveToElement {
                element,
                duration: Some(Duration::from_secs(5)),
                x: 0,
                y: 0,
            }),
        )
        .await?;

    // Select the "Exportieren" icon in the "Messwerte" window
    browser
        .wait()
        .for_element(Locator::Css(
            "d-worksheet-view > d-worksheet-view-toolbar button[title='Exportieren']",
        ))
        .await?
        .click()
        .await?;

    // Select "CSV-Datei"
    let mut matching_element = None;

    for element in browser
        .find_all(Locator::Css("div.d-menu.d-popup button"))
        .await?
    {
        if element.text().await? == "CSV-Datei" {
            matching_element = Some(element);

            break;
        }
    }

    matching_element
        .ok_or_else(|| anyhow!("CSV file button not found"))?
        .click()
        .await?;

    // Confirm large downloads
    if let Ok(element) = browser
        .wait()
        .at_most(Duration::from_secs(10))
        .for_element(Locator::Css(
            "d-file-export-with-can-export-progress-dialog footer button.button-primary",
        ))
        .await
    {
        element.click().await?;
    }

    let text = browser
        .wait_for_download(|entry| async move {
            let file_name = entry.file_name();
            let file_name = file_name.as_encoded_bytes();

            if !file_name.starts_with(b"Messwerte") || !file_name.ends_with(b".csv") {
                return Ok(None);
            }

            let text = read_to_string(entry.path()).await?;

            Ok(Some(text))
        })
        .await?;

    Ok(text)
}

async fn open_level_county_filter(source: &Source, browser: &Browser) -> Result<()> {
    browser.goto(source.url.as_str()).await?;

    // click the "Wasser" button
    browser
        .wait()
        .for_element(Locator::Css("a#d-nav-tree-node_ROOT-Wasser_firstContent"))
        .await?
        .click()
        .await?;

    // click the "Grundwasser" button
    browser
        .wait()
        .for_element(Locator::Css(
            "a#d-nav-tree-node_ROOT-Wasser-Grundwasser_firstContent",
        ))
        .await?
        .click()
        .await?;

    // click the "Grundwassermenge" button
    browser
        .wait()
        .for_element(Locator::Css("a#d-nav-tree-node_ROOT-Wasser-Grundwasser-uXwTw947n7bRQG4iOuXI-yoX0xyCZqYcNYAEYqBI4_firstContent"))
        .await?
        .click()
        .await?;

    // click the "Messwerttabelle" button
    browser
        .wait()
        .for_element(Locator::Css("d-worksheet-view[data-id='B8suKHf7a5dAURS43-sy']:has(d-cadenza-link[data-workbook-name='yoX0xyCZqYcNYAEYqBI4'][data-worksheet-id='kOSziKKXB6IIqNw9kSLL'])"))
        .await?
        .click()
        .await?;

    // wait for the main table to load
    browser
        .wait()
        .for_element(Locator::Css(
            "d-worksheet-grid d-embedded-content d-workbook-table-view",
        ))
        .await?;

    // click the "Erweiterte Auswahl" button below "Kreis"
    browser
        .wait()
        .for_element(Locator::Css(
            "d-table-condition[condition-label='Kreis'] button[title='Erweiterte Auswahl']",
        ))
        .await?
        .click()
        .await?;

    // wait for the data table to appear
    browser
        .wait()
        .for_element(Locator::Css(
            "d-condition-table-modal table.dataTable tbody",
        ))
        .await?;

    Ok(())
}
