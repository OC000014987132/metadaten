use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use poppler::Document;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{format_description::well_known::Iso8601, Date};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_pages, collect_text, strip_jsessionid},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (count, results, errors) = fetch_page(dir, client, source, selectors, 1).await?;

    let pages = count.div_ceil(source.batch_size);
    tracing::info!("Fetching {count} datasets from {pages} pages");

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let count;
    let items;

    {
        let mut url = source
            .url
            .join("/SiteGlobals/Forms/Suche/webs/Endlagersuche/DE/Servicesuche_Formular.html")?;

        url.query_pairs_mut()
            .append_pair("gtp", &format!("13788870_list%3D{page}"))
            .append_pair("resultsPerPage", &source.batch_size.to_string());

        let text = client
            .fetch_text(source, format!("serp-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let results = document
            .select(&selectors.results)
            .next()
            .ok_or_else(|| anyhow!("Missing results"))?
            .text()
            .collect::<String>();

        count = selectors
            .results_count
            .captures(&results)
            .ok_or_else(|| anyhow!("Malformed results"))?
            .get(1)
            .unwrap()
            .as_str()
            .replace('.', "")
            .parse()?;

        items = document
            .select(&selectors.items)
            .map(|item| {
                let href = item
                    .select(&selectors.item_link)
                    .next()
                    .ok_or_else(|| anyhow!("Missing item link on page {page}"))?
                    .attr("href")
                    .unwrap()
                    .to_owned();

                let gallery_href = item
                    .select(&selectors.item_gallery_link)
                    .next()
                    .map(|link| link.attr("data-lightbox-href").unwrap().to_owned());

                let title = collect_text(
                    item.select(&selectors.item_title)
                        .next()
                        .ok_or_else(|| anyhow!("Missing item title on page {page}"))?
                        .text(),
                );

                let dates = item
                    .select(&selectors.item_dates)
                    .map(|date| date.attr("datetime").unwrap())
                    .filter(|date| !date.is_empty())
                    .map(|date| Date::parse(date, &Iso8601::DEFAULT))
                    .collect::<Result<Vec<_>, _>>()?;

                Ok(Item {
                    href,
                    gallery_href,
                    title,
                    dates,
                })
            })
            .collect::<Result<Vec<_>>>()?;
    }

    let (results, errors) = fetch_many(0, 0, items, |item| {
        fetch_item(dir, client, source, selectors, item)
    })
    .await;

    Ok((count, results, errors))
}

struct Item {
    href: String,
    gallery_href: Option<String>,
    title: String,
    dates: Vec<Date>,
}

async fn fetch_item(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    mut item: Item,
) -> Result<(usize, usize, usize)> {
    let mut resources = SmallVec::new();

    let href = if let Some(gallery_href) = item.gallery_href {
        let mut url = source.url.join(&item.href)?;
        strip_jsessionid(&mut url);

        resources.push(Resource {
            r#type: ResourceType::WebPage,
            description: None,
            url: url.into(),
            ..Default::default()
        });

        gallery_href
    } else {
        item.href
    };

    let time_range = match item.dates.len() {
        1 => {
            let at = item.dates.pop().unwrap();
            Some(at.into())
        }
        2 => {
            let until = item.dates.pop().unwrap();
            let from = item.dates.pop().unwrap();
            Some((from, until).into())
        }
        _ => None,
    };

    let mut url = source.url.join(&href)?;
    strip_jsessionid(&mut url);

    let path = url.path();
    let key = path.replace(['/', '?', '&'], "-");

    let description = if path.ends_with(".pdf") || path.ends_with(".PDF") {
        let bytes = client
            .fetch_pdf(source, format!("document-{key}.isol"), &url)
            .await?;

        let document = Document::from_bytes(&bytes, None)?;

        collect_pages(&document, 0..3)
    } else {
        let text = client
            .fetch_text(source, format!("article-{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        collect_text(
            document
                .select(&selectors.item_description)
                .flat_map(|element| element.text()),
        )
    };

    let types = if item.title.starts_with("Einvernehmen zum Vorhaben") {
        smallvec![Type::Text {
            text_type: TextType::OfficialFile,
        }]
    } else if url.path().ends_with(".html") {
        smallvec![Type::Text {
            text_type: TextType::Editorial,
        }]
    } else {
        Default::default()
    };

    let dataset = Dataset {
        title: item.title,
        description: Some(description),
        types,
        resources,
        time_ranges: time_range.into_iter().collect(),
        origins: source.origins.clone(),
        license: License::OtherClosed,
        language: Language::German,
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    results: "h2.solrSortResults.c-solrsort-result",
    results_count: r"([\d\.]+) Suchergebnisse" as Regex,
    items: "div.l-search-list__item",
    item_link: "a[href]",
    item_gallery_link: "a[data-lightbox-href].c-search-result.js-lightbox-button",
    item_title: "h3.c-search-result__headline",
    item_dates: "time[datetime]",
    item_description: "main div.row",
}
