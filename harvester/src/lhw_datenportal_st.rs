use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{Coord, Rect};
use hashbrown::HashMap;
use scraper::{Html, Selector};
use serde_json::{from_str, Map, Value};
use serde_roxmltree::roxmltree::Document;
use smallvec::{smallvec, SmallVec};
use time::Date;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, select_first_text, select_text, GermanDate},
    write_dataset, Source,
};
use metadaten::dataset::{r#type::Type, Dataset, Language, License, Resource, ResourceType};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let topics = fetch_topics(client, source, selectors).await?;

    let (results, errors) = fetch_many(0, 0, topics, |topic| async move {
        let topic_id = &topic.id;
        let topic_text = &topic.text;

        let subtopic_infos = &fetch_subtopic_infos(client, source, selectors, topic_id).await?;

        let count = topic.subtopics.len();

        let (results, errors) = fetch_many(0, 0, topic.subtopics, |subtopic| async move {
            let subtopic_info = subtopic_infos
                .get(&subtopic.text)
                .ok_or_else(|| anyhow!("Missing subtopic info for `{}`", subtopic.text))?;

            let key = format!("{}-{}", topic_id, subtopic.id);
            let title = format!("{}: {}", topic_text, subtopic.text);

            let ogc_id = subtopic.id.split_once('.').map_or(&*subtopic.id, |(_prefix, suffix)| suffix);

            let mut resources = smallvec![
                Resource {
                    r#type: ResourceType::Image,
                    description: Some("Legende".to_owned()),
                    url: subtopic_info.legend.clone(),
                    ..Default::default()
                }.guess_or_keep_type(),
                Resource {
                    r#type: ResourceType::WebService,
                    description: Some("OGC-Dienste - WMS".to_owned()),
                    url: source.url.join(&format!("/public/ogcsl.ashx?nodeId={ogc_id}&Service=WMS&request=GetCapabilities"))?.into(),
                    primary_content: true,
                    ..Default::default()
                },
                Resource {
                    r#type: ResourceType::WebService,
                    description: Some("OGC-Dienste - WFS".to_owned()),
                    url: source.url.join(&format!("/public/ogcsl.ashx?nodeId={ogc_id}&Service=WFS&request=GetCapabilities"))?.into(),
                    primary_content: true,
                    ..Default::default()
                },
            ];

            if let Some(further_reading) = &subtopic_info.further_reading {
                resources.push(Resource {
                    r#type: ResourceType::Document,
                    description: Some("Weiterführende Informationen".to_owned()),
                    url: source.url.join(further_reading)?.into(),
                    ..Default::default()
                }.guess_or_keep_type());
            }

            let bounding_boxes = fetch_bounding_boxes(client, source, ogc_id).await?;

            let dataset = Dataset {
                title,
                description: subtopic_info.description.clone(),
                types: smallvec![Type::MapService],
                modified: subtopic_info.modified,
                resources,
                bounding_boxes,
                source_url: source.url.clone().into(),
                origins: source.origins.clone(),
                language: Language::German,
                license: License::AllRightsReserved,
                ..Default::default()
            };

            write_dataset(dir, client, source, key, dataset).await
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_bounding_boxes(
    client: &Client,
    source: &Source,
    ogc_id: &str,
) -> Result<SmallVec<[Rect; 1]>> {
    let mut bounding_boxes = SmallVec::new();

    let url = source.url.join(&format!(
        "/public/ogcsl.ashx?nodeId={ogc_id}&Service=WMS&request=GetCapabilities"
    ))?;

    let key = format!("bounding-boxes-{ogc_id}.isol");

    let text = client.fetch_text(source, key, &url).await?;

    let document = Document::parse(&text)?;

    for element in document.root_element().descendants() {
        if !element.has_tag_name("EX_GeographicBoundingBox") {
            continue;
        }

        let mut min_x = None;
        let mut max_x = None;
        let mut min_y = None;
        let mut max_y = None;

        for child in element.children() {
            let Some(text) = child.text() else {
                continue;
            };
            let text = text.parse();

            match child.tag_name().name() {
                "westBoundLongitude" => min_x = Some(text?),
                "eastBoundLongitude" => max_x = Some(text?),
                "southBoundLatitude" => min_y = Some(text?),
                "northBoundLatitude" => max_y = Some(text?),
                _ => (),
            }
        }

        if let (Some(min_x), Some(min_y), Some(max_x), Some(max_y)) = (min_x, min_y, max_x, max_y) {
            bounding_boxes.push(Rect::new(
                Coord { x: min_x, y: min_y },
                Coord { x: max_x, y: max_y },
            ));
        }
    }

    Ok(bounding_boxes)
}

#[derive(Debug, Default)]
struct SubtopicInfo {
    description: Option<String>,
    modified: Option<Date>,
    legend: String,
    further_reading: Option<String>,
}

async fn fetch_subtopic_infos(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    topic_id: &str,
) -> Result<HashMap<String, SubtopicInfo>> {
    let mut infos = HashMap::new();

    let mut url = source.url.join("content.aspx")?;

    url.query_pairs_mut()
        .append_pair("themeIds", topic_id)
        .append_pair("method", "ThemeLegend,ThemeDescription");

    let key = format!("topic-{}", topic_id);

    let text = client
        .fetch_text(source, format!("{key}.isol"), &url)
        .await?;

    let document = Html::parse_document(&text);

    let mut title = String::new();
    let mut info = SubtopicInfo::default();

    for element in document.select(&selectors.subtopics_elements) {
        if selectors.subtopics_separator.matches(&element) {
            if !title.is_empty() {
                infos.insert(title, info);
            }

            title = String::new();
            info = SubtopicInfo::default();

            info.modified = element
                .select(&selectors.subtopics_modified)
                .next()
                .map(|element| collect_text(element.text()))
                .map(|text| text.parse::<GermanDate>())
                .transpose()?
                .map(|date| date.into());
        } else if selectors.subtopics_description.matches(&element) {
            let text = select_text(element, &selectors.subtopics_description_value);

            if !text.is_empty() {
                info.description = Some(text);
            }

            info.further_reading = element
                .select(&selectors.subtopics_further_reading)
                .next()
                .map(|element| element.attr("href").unwrap().to_owned());
        } else if selectors.subtopics_legend.matches(&element) {
            element
                .select(&selectors.subtopics_legend_value)
                .next()
                .ok_or_else(|| anyhow!("Missing legend"))?
                .attr("src")
                .unwrap()
                .clone_into(&mut info.legend);

            title = select_first_text(element, &selectors.subtopics_title, "title")?;
        }
    }

    if !title.is_empty() {
        infos.insert(title, info);
    }

    Ok(infos)
}

#[derive(Debug)]
struct Topic {
    id: CompactString,
    text: String,
    subtopics: Vec<Subtopic>,
}

#[derive(Debug)]
struct Subtopic {
    id: CompactString,
    text: String,
}

async fn fetch_topics(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<Topic>> {
    let text = client
        .fetch_text(source, "topics".to_owned(), &source.url)
        .await?;

    let document = Html::parse_document(&text);

    let script = document
        .select(&selectors.topics_script)
        .find_map(|element| {
            let text = collect_text(element.text());

            if text
                .contains(r#"Enum.registerType("IDU.Core.Web.Controls.Map.AxTypes.AxOMapActions""#)
            {
                Some(text)
            } else {
                None
            }
        })
        .ok_or_else(|| anyhow!("Missing script"))?;

    let start_idx = script.find(r#""javaScriptTypeName":"IDU.cardoMap3.WebV2.Controls.ThemeSelectors.DefaultThemeTreeExtJs4.DefaultThemeTreeExtJs4""#).ok_or_else(|| anyhow!("Missing start"))?;

    let end_idx = script[start_idx..]
        .find("});")
        .ok_or_else(|| anyhow!("Missing end"))?;

    let value = script
        .get(start_idx - 22..start_idx + end_idx + 1)
        .ok_or_else(|| anyhow!("Missing value"))?;

    let value = from_str::<Value>(value)?;

    let topics = value
        .pointer("/instantiationInfo/configObject/themeTree/children")
        .and_then(|values| values.as_array())
        .ok_or_else(|| anyhow!("Missing topics"))?;

    topics
        .iter()
        .map(|topic| {
            let topic = topic
                .as_object()
                .ok_or_else(|| anyhow!("Malformed topic"))?;

            let id = topic
                .get("id")
                .and_then(|id| id.as_str())
                .ok_or_else(|| anyhow!("Missing topic ID"))?
                .into();

            let text = topic
                .get("text")
                .and_then(|text| text.as_str())
                .ok_or_else(|| anyhow!("Missing topic text"))?
                .trim()
                .to_owned();

            fn traverse(object: &Map<String, Value>, subtopics: &mut Vec<Subtopic>) -> Result<()> {
                if let Some(children) = object
                    .get("children")
                    .and_then(|children| children.as_array())
                    .filter(|children| !children.is_empty())
                {
                    for child in children.iter().filter_map(|child| child.as_object()) {
                        traverse(child, subtopics)?;
                    }
                } else {
                    let id = object
                        .get("id")
                        .and_then(|id| id.as_str())
                        .ok_or_else(|| anyhow!("Missing subtopic ID"))?
                        .into();

                    let text = object
                        .get("text")
                        .and_then(|text| text.as_str())
                        .ok_or_else(|| anyhow!("Missing subtopic text"))?
                        .trim()
                        .to_owned();

                    subtopics.push(Subtopic { id, text });
                }

                Ok(())
            }

            let mut subtopics = Vec::new();
            traverse(topic, &mut subtopics)?;

            Ok(Topic {
                id,
                text,
                subtopics,
            })
        })
        .collect()
}

selectors! {
    topics_script: r#"script[type="text/javascript"]"#,
    subtopics_elements: "body > *",
    subtopics_separator: "div.cm3-content-headerCt",
    subtopics_modified: "b",
    subtopics_description: "div:not([class]",
    subtopics_description_value: "p",
    subtopics_further_reading: "a[href^='project/cardoMap/']",
    subtopics_legend: "div.cm3-content-legend",
    subtopics_legend_value: "img[src]",
    subtopics_title: "h3",
}
