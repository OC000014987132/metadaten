use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use miniproj::{get_projection, Projection};
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::smallvec;
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{contains_or, make_key, point_like_bounding_box, select_text},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region,
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();
    let proj = get_projection(25832).unwrap();

    let mut all_links = fetch_mainlinks(client, source, selectors).await?;
    let sublinks1 = fetch_sublinks(client, source, &all_links, selectors).await?;
    let sublinks2 = fetch_sublinks(client, source, &sublinks1, selectors).await?;
    for links in [sublinks1, sublinks2] {
        all_links.extend(links);
    }
    all_links.retain(|link| link.contains("/tabellen"));
    all_links.sort_unstable();
    all_links.dedup();

    let mut sites = Vec::<Site>::new();

    for link in all_links {
        let parameter = if link.contains("/pegel") {
            Parameter::Pegel
        } else if link.contains("/speicher") {
            Parameter::Speicher
        } else if link.contains("/schnee") {
            Parameter::Schnee
        } else if link.contains("/grundwasser") {
            Parameter::Grundwasser
        } else if link.contains("/niederschlag") {
            Parameter::Niederschlag
        } else {
            tracing::error!("No parameter known for {link}");
            continue;
        };

        let url = Url::parse(&link)?;
        let key = make_key(&link).into_owned();

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        for row in document.select(&selectors.rows) {
            let mut cells = row.select(&selectors.cells);

            let site = cells.next().ok_or_else(|| anyhow!("Missing name column"))?;

            let link = site
                .select(&selectors.link_selector)
                .next()
                .ok_or_else(|| anyhow!("Missing link"))?;
            let href = link.attr("href").unwrap().to_owned();

            let site = Site { href, parameter };

            sites.push(site);
        }
    }

    sites.sort_unstable();
    sites.dedup();

    let count = sites.len();

    let (results, errors) = fetch_many(0, 0, sites, |link| {
        translate_dataset(dir, client, source, selectors, proj, link)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_mainlinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let text = client
        .fetch_text(source, "mainlinks".to_owned(), &source.url)
        .await?;
    let document = Html::parse_document(&text);

    let links = document
        .select(&selectors.mainlink_selector)
        .filter_map(|element| {
            let link = element.attr("href").unwrap();

            if !link.contains("pegel") {
                return None;
            }

            Some(link.to_owned())
        })
        .collect();

    Ok(links)
}

async fn fetch_sublinks(
    client: &Client,
    source: &Source,
    links: &Vec<String>,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let mut sublinks = Vec::new();

    for link in links {
        let url = source.url.join(link)?;
        let key = make_key(link).into_owned();

        let text = client.fetch_text(source, key.to_owned(), &url).await?;
        let document = Html::parse_document(&text);

        sublinks.extend(
            document
                .select(&selectors.sublink_selector)
                .map(|element| element.attr("href").unwrap().to_owned()),
        );
    }

    Ok(sublinks)
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: &dyn Projection,
    item: Site,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.href).into_owned();

    let dataset = {
        let url = fetch_stammdaten(client, source, &item.href, selectors).await?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        let stammdaten_text = select_text(&document, &selectors.stammdaten_text);
        let name = select_text(&document, &selectors.name_selector);

        let site_id = selectors
            .messstellen_id_regex
            .captures(&stammdaten_text)
            .map(|captures| captures.get(1).unwrap().as_str());

        let municipality = selectors
            .municipality_regex
            .captures(&stammdaten_text)
            .map(|captures| captures.get(1).unwrap().as_str());

        let groundwater = selectors
            .groundwater_regex
            .captures(&stammdaten_text)
            .map(|captures| captures.get(1).unwrap().as_str());

        let ostwert = selectors
            .ostwert_regex
            .captures(&stammdaten_text)
            .map(|captures| captures[1].parse::<f64>())
            .transpose()?;

        let nordwert = selectors
            .nordwert_regex
            .captures(&stammdaten_text)
            .map(|captures| captures[1].parse::<f64>())
            .transpose()?;

        let organisations = selectors
            .betreiber_regex
            .captures(&stammdaten_text)
            .map(|captures| Organisation::Other {
                name: captures[1].to_owned(),
                role: OrganisationRole::Operator,
                websites: Default::default(),
            })
            .into_iter()
            .collect();

        let title = if let Some(site_id) = &site_id {
            format!(
                "Messstelle (Nr. {}) - {} ({})",
                site_id,
                item.parameter.title(),
                name
            )
        } else {
            format!("Messstelle - {} ({})", item.parameter.title(), name)
        };

        let description = {
            if let (Some(site_id), Some(municipality)) = (site_id, municipality) {
                if let Some(groundwater) = groundwater {
                    format!("Die Messstelle {name} (Messstellen-Nr: {site_id}) befindet sich im Grundwasserleiter {groundwater}, im Landkreis {municipality}.")
                } else {
                    format!("Die Messstelle {name} (Messstellen-Nr: {site_id}) befindet sich im Landkreis {municipality}.")
                }
            } else {
                format!("Messstelle {name}.")
            }
        };

        let domain = item.parameter.domain();

        let measured_variables = item
            .parameter
            .measured_variable()
            .map(Into::into)
            .into_iter()
            .collect();

        let types = smallvec![Type::Measurements {
            domain,
            station: Some(Station {
                id: Some(site_id.unwrap_or("Unbekannt").into()),
                ..Default::default()
            }),
            measured_variables,
            methods: Default::default(),
        }];

        let region = municipality.map_or(Region::BY, |municipality| {
            contains_or(municipality, Region::BY)
        });

        let mut regions = smallvec![region];

        let bounding_boxes = if let (Some(lon), Some(lat)) = (ostwert, nordwert) {
            let (lon, lat) = proj.projected_to_deg(lon, lat);

            if domain == Domain::Rivers {
                regions.extend(WISE.match_shape(lon, lat).map(Region::Watershed));
            }

            smallvec![point_like_bounding_box(lat, lon)]
        } else {
            Default::default()
        };

        Dataset {
            title,
            description: Some(description),
            types,
            regions,
            bounding_boxes,
            organisations,
            language: Language::German,
            license: License::CcBy40,
            origins: source.origins.clone(),
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_stammdaten(
    client: &Client,
    source: &Source,
    entry_link: &str,
    selectors: &Selectors,
) -> Result<Url> {
    let url = source.url.join(entry_link)?;
    let key = format!("{}-stammdaten.isol", make_key(url.path()));

    let text = client.fetch_text(source, key, &url).await?;
    let document = Html::parse_document(&text);

    let url = document
        .select(&selectors.stammdaten)
        .map(|element| element.attr("href").unwrap())
        .find(|element| element.contains("stammdaten"))
        .ok_or_else(|| anyhow!("missing Stammdaten for {}", &url.path()))?
        .parse()?;

    Ok(url)
}

#[derive(Debug, PartialEq, Ord, PartialOrd, Eq, Clone)]
struct Site {
    href: String,
    parameter: Parameter,
}

#[derive(Debug, PartialEq, Ord, PartialOrd, Eq, Clone, Copy)]
enum Parameter {
    Pegel,
    Speicher,
    Grundwasser,
    Schnee,
    Niederschlag,
}

impl Parameter {
    fn title(&self) -> &'static str {
        match self {
            Self::Pegel => "Pegel",
            Self::Speicher => "Speicher",
            Self::Grundwasser => "Grundwasser",
            Self::Schnee => "Schnee",
            Self::Niederschlag => "Niederschlag",
        }
    }

    fn measured_variable(&self) -> Option<&'static str> {
        match self {
            Self::Pegel => Some("Pegelstand"),
            Self::Speicher => None,
            Self::Grundwasser => Some("Grundwasserstand"),
            Self::Schnee => Some("Schneehöhe"),
            Self::Niederschlag => Some("Niederschlagshöhe"),
        }
    }

    fn domain(&self) -> Domain {
        match self {
            Self::Pegel => Domain::Rivers,
            Self::Speicher => Domain::Unspecified,
            Self::Grundwasser => Domain::Groundwater,
            Self::Schnee => Domain::Air,
            Self::Niederschlag => Domain::Air,
        }
    }
}

selectors! {
    mainlink_selector: "div#navi_horizontal a[href]",
    sublink_selector: "#navi_horizontal_sub a[href], h4+ ul li+ li a[href]",

    rows: "tbody > tr",
    cells: "td",
    link_selector: "a[href]",

    stammdaten: "#navi_links_3c a",
    stammdaten_text: "div.col > p, td",
    name_selector: "h4",

    messstellen_id_regex: r"Messstellen-Nr.:\s+(\w+)" as Regex,
    municipality_regex: r"Landkreis:\s+(.+?)\s+(?:\w+:|Zuständiges)" as Regex,
    groundwater_regex: r"Grundwasserleiter:\s+(\w+)" as Regex,

    ostwert_regex: r"Ostwert:\s+(\w+)" as Regex,
    nordwert_regex: r"Nordwert:\s+(\w+)" as Regex,
    betreiber_regex: r"(?:Betreiber|Zuständiges\s+Amt):\s+(.+?)\s+(?:\w+:|Messstellen)" as Regex,
}
