use std::borrow::Cow;

use anyhow::{Error, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use hashbrown::HashMap;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::smallvec;
use time::{macros::format_description, Date};

use harvester::{
    client::{Client, HttpRequestBuilderExt, HttpResponseExt},
    fetch_many, selectors,
    utilities::{parse_attributes, select_first_text, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();
    let cookies = AtomicRefCell::new(HashMap::new());

    let docs = {
        let text = client
            .make_request(
                source,
                "dokumentenliste".to_owned(),
                Some(&source.url),
                |client| async {
                    client
                        .get(source.url.clone())
                        .send()
                        .await?
                        .error_for_status()?
                        .set_cookies(&mut cookies.borrow_mut())?
                        .text()
                        .await
                        .map_err(Error::from)
                },
            )
            .await?;

        let document = Html::parse_document(&text);

        parse_attributes(
            &document,
            &selectors.docs,
            &selectors.docs_value,
            "href",
            "ID",
        )
        .collect::<Result<Vec<usize>>>()?
    };

    let cookies = &cookies.borrow();

    let count = docs.len();

    let (results, errors) = fetch_many(0, 0, docs, |id| {
        fetch_doc(dir, client, source, selectors, cookies, id)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_doc(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    cookies: &HashMap<CompactString, String>,
    id: usize,
) -> Result<(usize, usize, usize)> {
    let key = format!("dokument-{id}");

    let dataset = {
        let mut url = source.url.join("detail.php")?;

        url.query_pairs_mut().append_pair("id", &id.to_string());

        let text = client
            .make_request(source, key.clone(), Some(&url), |client| async {
                client
                    .get(url.clone())
                    .cookies(cookies)
                    .send()
                    .await?
                    .error_for_status()?
                    .text()
                    .await
                    .map_err(Error::from)
            })
            .await?;

        let document = Html::parse_document(&text);

        let number = select_first_text(&document, &selectors.number, "number")?;
        let name = select_first_text(&document, &selectors.name, "name")?;

        fn replace_if_empty(value: String) -> Cow<'static, str> {
            if !value.is_empty() {
                Cow::Owned(value)
            } else {
                Cow::Borrowed("nicht vorhanden.")
            }
        }

        let description_1 = replace_if_empty(select_text(&document, &selectors.description_1));
        let description_2 = replace_if_empty(select_text(&document, &selectors.description_2));
        let description_3 = replace_if_empty(select_text(&document, &selectors.description_3));
        let description_4 = replace_if_empty(select_text(&document, &selectors.description_4));

        let description = format!(
            r#"Beitrag im Rahmen der FKTG: {description_1}
Stellungnahme der BGE: {description_2}
Initiale Rückmeldung im Rahmen der FKTG: {description_3}
Stellungnahme einer externen Prüfstelle:{description_4}"#
        );

        let r#type = Type::Text {
            text_type: TextType::Unspecified,
        };

        let resources = select_text(&document, &selectors.resources)
            .split_whitespace()
            .map(|url| {
                Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(
                        "Hyperlink zum Dokument mit der Stellungnahme der BGE".to_owned(),
                    ),
                    url: url.to_owned(),
                    direct_link: false,
                    ..Default::default()
                }
                .guess_or_keep_type()
            })
            .collect();

        let date = Date::parse(
            &select_text(&document, &selectors.date),
            format_description!("[year]-[month]-[day]"),
        );

        let region = Region::GeoName(2855047); // Peine

        let provider = Organisation::WikiData {
            identifier: 26794436, // BGE
            role: OrganisationRole::Provider,
        };

        // Zitat beteiligung.bge.de/impressum.php: "Die auf diesen Seiten veröffentlichten Inhalte unterliegen grundsätzlich dem deutschen Urheber- und Leistungsschutzrecht."
        let license = License::OtherClosed;

        Dataset {
            title: format!("Nr. {number}: {name}"),
            description: Some(description),
            types: smallvec![r#type],
            resources,
            language: Language::German,
            issued: date.ok(),
            regions: smallvec![region],
            organisations: smallvec![provider],
            origins: source.origins.clone(),
            license,
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    docs: "table#mainTable > tbody > tr > td > a[href^='detail.php?id=']",
    docs_value: r"detail\.php\?id=(\d+)" as Regex,
    number: "div#boxOben > table.datentable > tbody > tr:nth-of-type(2) > td.datenfeld:nth-of-type(1)",
    name: "div#boxUnten > table.datentable > tbody > tr.datenzeile:nth-of-type(4) > td.datenfeld:nth-of-type(1)",
    description_1: "#boxMitte1Links .datenfeld",
    description_2: "#boxMitte2Links .datenfeld",
    description_3: "#boxMitte1Rechts .datenfeld",
    description_4: "#boxMitte2Rechts .datenfeld",
    resources: "#boxUnten2 .datenfeld:nth-child(1)",
    date: "#boxUnten .datenfeld:nth-child(3) ~ .datenfeld",
}
