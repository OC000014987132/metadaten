use std::collections::BTreeSet;

use anyhow::{anyhow, ensure, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{Element, Html, Selector};
use smallvec::{smallvec, SmallVec};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, parse_attributes, parse_text},
    write_dataset, Source,
};
use metadaten::dataset::{r#type::Type, Dataset, Language};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (search_group_ids, regulation_ids) = fetch_source_groups(client, source, selectors).await?;

    let handles = AtomicRefCell::new(BTreeSet::new());

    let mut results = 0;
    let mut errors = 0;

    for search_group_id in search_group_ids {
        let (count, _, _) = fetch_handles(
            client,
            source,
            selectors,
            &regulation_ids,
            &search_group_id,
            1,
            &handles,
        )
        .await?;

        tracing::info!("Harvesting {count} results for search group {search_group_id}");
        let pages = count.div_ceil(source.batch_size);

        (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
            fetch_handles(
                client,
                source,
                selectors,
                &regulation_ids,
                &search_group_id,
                page,
                &handles,
            )
        })
        .await;
    }

    let handles = handles.into_inner();

    let count = handles.len();
    tracing::info!("Harvesting {count} datasets");

    (results, errors) = fetch_many(results, errors, handles, |handle| {
        fetch_dataset(dir, client, source, selectors, handle)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_source_groups(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(Vec<String>, Vec<String>)> {
    let url = source.url.join("WisiaQuery?lang=deu")?;

    let text = client
        .checked_fetch_text(source, database_overload, "search-groups".to_owned(), &url)
        .await?;

    let document = Html::parse_document(&text);

    let mut search_group_ids = document
        .select(&selectors.search_group_ids)
        .map(|elem| elem.attr("value").unwrap().to_owned())
        .collect::<Vec<_>>();

    search_group_ids.sort_unstable();
    search_group_ids.dedup();

    let mut regulation_ids = document
        .select(&selectors.regulation_ids)
        .map(|elem| elem.attr("value").unwrap().to_owned())
        .collect::<Vec<_>>();

    regulation_ids.sort_unstable();
    regulation_ids.dedup();

    Ok((search_group_ids, regulation_ids))
}

async fn fetch_handles(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    regulation_ids: &[String],
    search_group_id: &str,
    page: usize,
    handles: &AtomicRefCell<BTreeSet<u64>>,
) -> Result<(usize, usize, usize)> {
    let taxon = match search_group_id {
        "0" | "" => "*",
        _ => "",
    };

    let mut url = source.url.join("GetNames")?;

    url.query_pairs_mut()
        .append_pair("page", &page.to_string())
        .append_pair("workgroup_id", search_group_id)
        .append_pair("taxon", taxon);

    for regulation_id in regulation_ids {
        url.query_pairs_mut().append_pair("check_rw", regulation_id);
    }

    let text = client
        .checked_fetch_text(
            source,
            database_overload,
            format!("search-group-{search_group_id}-page-{page}"),
            &url,
        )
        .await?;

    let document = Html::parse_document(&text);

    let count = parse_text(
        &document,
        &selectors.results,
        &selectors.results_count,
        "number of results",
    )?;

    let mut handles = handles.borrow_mut();

    for handle in parse_attributes(
        &document,
        &selectors.handles,
        &selectors.handle_node_id,
        "href",
        "node ID",
    ) {
        handles.insert(handle?);
    }

    Ok((count, 0, 0))
}

#[tracing::instrument(skip(dir, client, source, selectors))]
async fn fetch_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    handle: u64,
) -> Result<(usize, usize, usize)> {
    let handle = handle.to_string();

    let mut url = source.url.join("GetTaxInfo")?;

    url.query_pairs_mut().append_pair("knoten_id", &handle);

    let mut title;
    let scientific_name;
    let mut common_names = SmallVec::new();

    {
        let text = client
            .checked_fetch_text(
                source,
                database_overload,
                format!("document-{handle}.isol"),
                &url,
            )
            .await?;

        let document = Html::parse_document(&text);

        scientific_name = extract_by_category(&document, selectors, &handle, "gültiger Name:")?;
        title = scientific_name.clone();

        if let Ok(common_name) =
            extract_by_category(&document, selectors, &handle, "Landespr. Namen:")
        {
            title = format!("{title} ({common_name})");
            common_names.push(common_name);
        }
    }

    let description = format!("Informationsseite zur Taxonomie und Schutzstatus von {title}");

    let types = smallvec![Type::Taxon {
        scientific_name,
        common_names,
    }];

    let dataset = Dataset {
        title: format!("Taxonomie und Schutzstatus von {title}"),
        description: Some(description),
        types,
        origins: source.origins.clone(),
        source_url: url.into(),
        language: Language::German,
        ..Default::default()
    };

    write_dataset(dir, client, source, handle, dataset).await
}

fn extract_by_category(
    document: &Html,
    selectors: &Selectors,
    handle: &str,
    category: &str,
) -> Result<String> {
    let elem = document
        .select(&selectors.category)
        .find(|elem| elem.text().collect::<String>() == category)
        .ok_or_else(|| anyhow!("Failed to find category {category} in page {handle}"))?
        .next_sibling_element()
        .ok_or_else(|| {
            anyhow!("Failed to retrieve content of category {category} in page {handle}")
        })?;

    let text = collect_text(elem.text());

    ensure!(
        !text.is_empty(),
        "Content of category {category} in page {handle} is empty"
    );

    Ok(text)
}

// HACK: Workaround for malformed responses due to database overload
fn database_overload(text: &str) -> Result<()> {
    ensure!(
        !text.contains("ORA-01000")
            && !text.contains("Closed Connection")
            && !text.contains("No more data to read from socket"),
        "Database overload"
    );

    Ok(())
}

selectors! {
    search_group_ids: "span.anznameinfo > select > option[value]",
    regulation_ids: "input[name=check_rw][value]",
    results: "body.getnames > h4",
    results_count: r"Treffer.*von.?(\d+)" as Regex,
    handles: "body.getnames > table.ergebnis > tbody > tr > td > a.main_link",
    handle_node_id: r".*knoten_id=(\d+)" as Regex,
    category: "td.taxinforubrik",
}
