use std::borrow::Cow;
use std::collections::BTreeMap;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_str;
use smallvec::{smallvec, SmallVec};
use time::Date;

use harvester::{
    client::Client,
    fetch_many, selectors, try_fetch,
    utilities::{collect_text, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{Domain, Type},
    Dataset, Language, License, Resource, ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) = fetch_map(dir, client, source).await?;

    try_fetch(
        &mut count,
        &mut results,
        &mut errors,
        fetch_main_page(dir, client, source, selectors),
    )
    .await;

    Ok((results, results, errors))
}

async fn fetch_map(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let map_url = &source.url.join("/diffuse-quellen/map.html?type=luft")?;

    let title = {
        let text = client.fetch_text(source, "map".to_owned(), map_url).await?;
        let document = Html::parse_document(&text);

        collect_text(
            document
                .select(&Selector::parse("span.title").unwrap())
                .flat_map(|element| element.text()),
        )
    };

    let description = {
        let text = client
            .fetch_text(source, "homepage".to_owned(), &source.url)
            .await?;
        let document = Html::parse_document(&text);

        collect_text(
            document
                .select(&Selector::parse("div.col-1-2 > p").unwrap())
                .flat_map(|element| element.text()),
        )
    };

    let title = &title;
    let description = &description;

    let url = &source.url.join("/api/pollutants/?medium=luft")?;

    let text = client
        .fetch_text(source, "pollutants".to_owned(), url)
        .await?;

    let pollutants = from_str::<Vec<Pollutant>>(&text)?;

    let count = pollutants.len();

    let (results, errors) = fetch_many(0, 0, pollutants, |pollutant| async move {
        let title = format!("{}: {}", title, pollutant.long_name);

        let mut url = source.url.join("api/pollutant/")?;

        url.query_pairs_mut()
            .append_pair("poll", &pollutant.short_name);

        let mut resources = smallvec![Resource {
            r#type: ResourceType::WebPage,
            description: Some("Was sind Diffuse Quellen?".to_owned()),
            url: source.url.clone().into(),
            ..Default::default()
        }
        .guess_or_keep_type()];

        let mut types = smallvec![Type::MapService];

        let latest_year;

        {
            let text = client
                .fetch_text(source, pollutant.short_name.clone().into_owned(), &url)
                .await?;

            let measurements =
                from_str::<BTreeMap<Cow<str>, BTreeMap<i32, Vec<Measurement>>>>(&text)?;

            let measurements = measurements.get(&pollutant.short_name).ok_or_else(|| {
                anyhow!("Missing pollutant {} in API response", pollutant.short_name)
            })?;

            for (year, measurements) in measurements {
                for measurement in measurements {
                    resources.push(Resource {
                        r#type: ResourceType::Svg,
                        description: format!(
                            "{}: {} ({}, {})",
                            year, measurement.long_name, measurement.short_name, measurement.unit
                        )
                        .into(),
                        url: source
                            .url
                            .join(&format!(
                                "/svg/luft_{}_{}_{}.svg",
                                pollutant.short_name, measurement.short_name, year
                            ))?
                            .into(),
                        ..Default::default()
                    });

                    types.push(Type::Measurements {
                        domain: Domain::Air,
                        station: None,
                        measured_variables: smallvec![
                            measurement.long_name.clone().into_owned(),
                            measurement.short_name.clone().into_owned(),
                        ],
                        methods: Default::default(),
                    });
                }
            }

            latest_year = measurements.keys().max().copied();
        }

        let modified = latest_year
            .map(|year| Date::from_ordinal_date(year, 1))
            .transpose()?;

        let dataset = Dataset {
            title,
            types,
            description: Some(description.clone()),
            modified,
            resources,
            language: Language::German,
            origins: source.origins.clone(),
            source_url: map_url.clone().into(),
            license: License::AllRightsReserved,
            tags: vec![
                Tag::Other("Gridding-Tool GRETA".into()),
                Tag::RASTERDATEN,
                Tag::Other("NFR-Einteilung".into()),
            ], // tags were suggested by dhS on 06.11.24
            ..Default::default()
        };

        write_dataset(
            dir,
            client,
            source,
            pollutant.short_name.into_owned(),
            dataset,
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_main_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let key = "main_page".to_owned();

    let text = client.fetch_text(source, key.clone(), &source.url).await?;

    let document = Html::parse_document(&text);

    let title = select_text(&document, &selectors.title);
    let description = select_text(&document, &selectors.description);

    let resources = document
        .select(&selectors.resources)
        .map(|element| {
            let href = element.attr("href").unwrap();
            let url = source.url.join(href)?;

            let description = collect_text(element.text());

            Ok(Resource {
                description: Some(description),
                url: url.into(),
                ..Default::default()
            })
        })
        .collect::<Result<SmallVec<_>>>()?;

    let dataset = Dataset {
        title,
        description: Some(description),
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url: source.url.clone().into(),
        resources,
        tags: vec![
            Tag::Other("Gridding-Tool GRETA".into()),
            Tag::RASTERDATEN,
            Tag::Other("NFR-Einteilung".into()), // Nomenclature for Reporting
        ], // tags were suggested by dhS on 06.11.24
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Deserialize)]
struct Pollutant<'a> {
    #[serde(rename = "name_s", borrow)]
    short_name: Cow<'a, str>,
    #[serde(rename = "name_l", borrow)]
    long_name: Cow<'a, str>,
}

#[derive(Debug, Deserialize)]
struct Measurement<'a> {
    #[serde(rename = "name_s", borrow)]
    short_name: Cow<'a, str>,
    #[serde(rename = "name_l", borrow)]
    long_name: Cow<'a, str>,
    #[serde(borrow)]
    unit: Cow<'a, str>,
}

selectors! {
    title: "main article h1",
    description: "main article p",
    resources: "main article a.button",
}
