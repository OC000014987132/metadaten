use std::io::Cursor;

use anyhow::{anyhow, bail, Result};
use cap_std::fs::Dir;
use miniproj::get_projection;
use regex::Regex;
use shapefile::{
    dbase::{encoding::Unicode, FieldValue, Reader as DbaseReader},
    Reader, ShapeReader,
};
use smallvec::smallvec;
use tempfile::TempDir;
use zip::ZipArchive;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{from_dbase_date, make_key, point_like_bounding_box, yesterday},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();
    let proj = get_projection(25833).expect("Projection not implemented.");

    let source_url = source
        .source_url
        .as_ref()
        .expect("No source_url defined in configuration");

    assert!(
        source_url.contains("site_id"),
        "Configured source_url should contain {{site_id}} placeholder"
    );

    let tmp_dir = TempDir::new()?;

    let file_name;

    {
        let bytes = client
            .fetch_bytes(source, "basisdaten_zip".to_owned(), &source.url)
            .await?;

        let mut archive = ZipArchive::new(Cursor::new(&*bytes))?;
        archive.extract(tmp_dir.path())?;

        file_name = archive
            .file_names()
            .find(|name| name.ends_with(".shp"))
            .ok_or_else(|| anyhow!("Did not find Shapefile in ZIP archive"))?
            .to_owned();
    }

    let path = tmp_dir.path().join(file_name);

    let mut reader = Reader::new(
        ShapeReader::from_path(&path)?,
        DbaseReader::from_path_with_encoding(path.with_extension("dbf"), Unicode)?,
    );

    let (results, errors) = fetch_many(0, 0, reader.iter_shapes_and_records(), |item| async move {
        let (_shape, record) = item?;

        let site_id = match record.get("MKZ") {
            Some(FieldValue::Character(Some(site_id))) => site_id,
            _ => bail!("Missing or unexpected MKZ value"),
        };

        let site_name = match record.get("MENA") {
            Some(FieldValue::Character(Some(site_name))) => {
                selectors.site_cleaner.replace_all(site_name, "")
            }
            _ => bail!("Missing or unexpected MENA value"),
        };

        let title = format!(
            "Grundwassermessstelle {} (Messstellen-Nr.: {})",
            site_name, site_id
        );

        let lage = match record.get("LAGE") {
            Some(FieldValue::Character(Some(lage))) => format!(" ({lage})"),
            Some(FieldValue::Character(None)) => Default::default(),
            lage => {
                tracing::debug!("Unexpected LAGE value found: {lage:?}");
                Default::default()
            }
        };

        let responsibility = match record.get("ARBEITSBER") {
            Some(FieldValue::Character(Some(responsibility))) => responsibility.trim(),
            _ => bail!("Missing or unexpected ARBEITSBER value"),
        };

        let longitude = match record.get("GIS_RW") {
            Some(FieldValue::Numeric(longitude)) => longitude,
            _ => {
                tracing::warn!("Missing or unexpected GIS_RW value");
                &None
            }
        };

        let latitude = match record.get("GIS_HW") {
            Some(FieldValue::Numeric(latitude)) => latitude,
            _ => {
                tracing::warn!("Missing or unexpected GIS_HW value");
                &None
            }
        };

        let bounding_boxes = match (latitude, longitude) {
            (&Some(lat), &Some(lon)) => {
                smallvec![proj_rect(point_like_bounding_box(lat, lon), proj)]
            }
            _ => {
                tracing::warn!("Failed determining bounding box");
                Default::default()
            }
        };

        let region = selectors
            .region_finder
            .captures(&site_name)
            .map(|capture| Region::Other(capture[1].into()));

        let affiliated = match record.get("BESCHAFF") {
            Some(FieldValue::Character(Some(affiliated))) => affiliated,
            _ => bail!("Missing or unexpected BESCHAFF value"),
        };

        let geodetic_system = match record.get("HS") {
            Some(FieldValue::Character(Some(geodetic_altitude))) => geodetic_altitude,
            _ => bail!("Missing or unexpected HS value"),
        };

        let site_elevation = match record.get("MPMH") {
            Some(FieldValue::Numeric(Some(site_elevation))) => site_elevation,
            _ => bail!("Missing or unexpected MPMH value"),
        };

        let elevation = match record.get("GLH") {
            Some(FieldValue::Numeric(Some(elevation))) => elevation,
            _ => bail!("Missing or unexpected GLH value"),
        };

        let filter_top_edge = match record.get("FIOK") {
            Some(FieldValue::Numeric(Some(filter_top_edge))) => filter_top_edge,
            _ => bail!("Missing or unexpected FIOK value"),
        };

        let filter_bottom_edge = match record.get("FIUK") {
            Some(FieldValue::Numeric(Some(filter_bottom_edge))) => filter_bottom_edge,
            _ => bail!("Missing or unexpected FIUK value"),
        };

        let sohle_last_measurement = match record.get("SOHLE") {
            Some(FieldValue::Numeric(Some(sohle_last_measurement))) => sohle_last_measurement,
            _ => bail!("Missing or unexpected SOHLE value"),
        };

        let base_extension = match record.get("AUSO") {
            Some(FieldValue::Numeric(Some(base_extension))) => base_extension,
            _ => bail!("Missing or unexpected AUSO value"),
        };

        let height_profile = format!(r#"Die Messstelle wurde im Höhensystem {geodetic_system} eingemessen. Das Höhenprofil in diesem System ist:
Messpunkthöhe:             {site_elevation} m
Geländehöhe:               {elevation:2.2} m
Filteroberkante:           {filter_top_edge} m
Filterunterkante:          {filter_bottom_edge} m
Sohle (letzte Einmessung): {sohle_last_measurement} m
Sohle bei Ausbau:          {base_extension} m"#);

        let sampling_cycle = match record.get("ZYKLUS") {
            Some(FieldValue::Character(Some(sampling_cycle))) => sampling_cycle,
            _ => bail!("Missing or unexpected ZYKLUS value"),
        };

        let site_type = match record.get("MA") {
            Some(FieldValue::Character(Some(site_type))) => site_type,
            _ => bail!("Missing or unexpected MA value"),
        };

        let start_date = match record.get("STAB") {
            Some(FieldValue::Date(issued)) => issued.to_owned(),
            _ => Default::default(),
        };

        let constructed = match record.get("BAUJAHR") {
            Some(FieldValue::Numeric(Some(constructed))) => {
                if constructed != &-9999.0 {
                    format!("Die Anlage wurde im Jahr {constructed} erbaut")
                } else {
                    Default::default()
                }
            }
            _ => bail!("Missing or unexpected BAUJAHR value"),
        };

        let end_date = match record.get("STAE") {
            Some(FieldValue::Date(end_date)) => end_date,
            _ => bail!("Missing or unexpected STAE value"),
        };

        let borehole_name = match record.get("BOHR_NR") {
            Some(FieldValue::Character(Some(borehole_name))) => {
                format!("\nNummer des Bohrloches: {borehole_name}.")
            }
            _ => Default::default(),
        };

        let aquifer = match record.get("STOCK") {
            Some(FieldValue::Character(Some(aquifer))) => {
                format!("\nDer Grundwasserleiter wird beschrieben als: {aquifer}.")
            }
            _ => Default::default(),
        };

        let gw_type = match record.get("GWART") {
            Some(FieldValue::Character(Some(gw_type))) => {
                format!("\nDer Zustand des Grundwassers wird beschrieben als: {gw_type}.")
            }
            _ => Default::default(),
        };

        let gw_body = match record.get("GWK") {
            Some(FieldValue::Character(Some(gw_body))) => {
                format!("\nDer zugehörige Grundwasserkörper ist: {gw_body}.")
            }
            _ => Default::default(),
        };
        let network_type = match record.get("NETZART") {
            Some(FieldValue::Character(Some(network_type))) => match network_type.as_str() {
                "J" => " Die Messstelle gehört zum Basisnetz des LfU.".to_owned(),
                _ => Default::default(),
            },
            _ => {
                tracing::debug!("Missing or unexpected NETZART value");
                Default::default()
            }
        };

        let soil_type = match record.get("SCHICHTVER") {
            Some(FieldValue::Character(Some(soil_type))) => match soil_type.as_str() {
                "J" => " Ein Schichtverzeichnis liegt vor.".to_owned(),
                "N" => " Ein Schichtverzeichnis liegt nicht vor.".to_owned(),
                _ => Default::default(),
            },
            _ => bail!("Missing or unexpected SCHICHTVER value"),
        };

        let affiliated = match affiliated.as_str() {
            "J" => "\nDie Messstation gehört zum Beschaffenheitsmessnetz.\n",
            _ => Default::default(),
        };

        let description = format!(r#"Die Grundwasser-Messstelle mit Messstellen-ID {site_id} wird vom Landesamt für Umwelt Brandenburg betrieben, in Zuständigkeit des Standorts {responsibility}.
Sie befindet sich in {site_name}{lage}.{affiliated}{network_type}

Die Messstellenart ist {site_type}.{borehole_name}{aquifer}{gw_type}{gw_body}

Der Messzyklus ist {sampling_cycle}.

{constructed}.{soil_type}
{height_profile}"#);

        let description = selectors
            .missing_number_cleaner
            .replace_all(&description, "(keine Angabe)");

        let organisations = smallvec![
            Organisation::Other {
                name: "Landesamt für Umwelt Brandenburg".to_owned(),
                role: OrganisationRole::Provider,
                websites: smallvec!["https://lfu.brandenburg.de/lfu/de/service/kontakt/#".to_owned()],
            },
            Organisation::Other {
                name: responsibility.to_owned(),
                role: OrganisationRole::Operator,
                websites: Default::default(),
            },
        ];

        let time_ranges = if let Some(start_date) = start_date {
            let start_date = from_dbase_date(start_date)?;
            let end_date = end_date
                .map(from_dbase_date)
                .transpose()?
                .unwrap_or_else(yesterday);

            smallvec![(start_date, end_date).into()]
        } else {
            Default::default()
        };

        let resources = smallvec![Resource {
            r#type: ResourceType::WebApp,
            description: Some(
                "Auskunftplattform Wasser des Landesamtes für Umwelt Brandenburg".to_owned()
            ),
            url: "https://apw.brandenburg.de/".to_owned(),
            ..Default::default()
        }];

        let source_url = source_url.replace("{{site_id}}", site_id);

        let key = make_key(&title).into_owned();

        let types = smallvec![Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(site_id.as_str().into()),
                measurement_frequency: Some(sampling_cycle.into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasserstand".to_owned()],
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description.into()),
            types,
            regions: region.into_iter().collect(),
            bounding_boxes,
            time_ranges,
            organisations,
            resources,
            language: Language::German,
            license: License::DlDeBy20,
            origins: source.origins.clone(),
            source_url,
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((results + errors, results, errors))
}

selectors! {
    region_finder: r"(^([^,\d]+))" as Regex,
    site_cleaner: r"(,?\s+[Bb]esch.\-?[mM]st.)" as Regex,
    missing_number_cleaner: r"(-9999(?:\.00)?(?: m)?)" as Regex,
}
