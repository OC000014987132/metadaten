use std::fmt::Write;
use std::io::{Cursor, Read};

use anyhow::{Error, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use csv::{Error as CsvError, ReaderBuilder};
use encoding_rs::{Encoding, WINDOWS_1252};
use encoding_rs_io::DecodeReaderBytesBuilder;
use geo::{Coord, Rect};
use hashbrown::{HashMap, HashSet};
use miniproj::{get_projection, Projection};
use serde::Deserialize;
use smallvec::{smallvec, SmallVec};
use time::Date;
use zip::read::ZipArchive;

use harvester::{
    client::{Client, TrackedResponse},
    fetch_many, write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, ReportingObligation, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25832).unwrap();

    let substances = fetch_substances(client, source).await?;

    let chemicals = fetch_chemicals(client, source).await?;
    let groundwater = fetch_groundwater(client, source).await?;

    let text = download_and_extract(
        client,
        source,
        "messstelle".to_owned(),
        "OpenHygrisC_gw-messstelle_EPSG25832_CSV.zip",
        "OpenHygrisC_gw-messstelle.csv",
        Some(WINDOWS_1252),
    )
    .await?;

    let mut reader = ReaderBuilder::new()
        .delimiter(b';')
        .from_reader(text.as_bytes());

    let (results, errors) = fetch_many(0, 0, reader.deserialize(), |record| {
        translate_record(
            dir,
            client,
            source,
            proj,
            &substances,
            &chemicals,
            &groundwater,
            record,
        )
    })
    .await;

    Ok((results, results, errors))
}

/// <https://www.opengeodata.nrw.de/produkte/umwelt_klima/wasser/grundwasser/hygrisc/OpenHygrisC_meta.zip>
#[derive(Deserialize)]
struct Record {
    #[serde(rename = "messstelle_id")]
    id: CompactString,
    name: CompactString,
    #[serde(rename = "messstellenart")]
    bauart: CompactString,
    #[serde(rename = "wasserstandsmessstelle")]
    pegel_bool: CompactString,
    #[serde(rename = "guetemessstelle")]
    guete_bool: CompactString,
    #[serde(rename = "im_wrrl_messnetz_chemie")]
    wrrl_chemie: CompactString,
    #[serde(rename = "im_wrrl_messnetz_wasserstand")]
    wrrl_pegel: CompactString,
    #[serde(rename = "messprogramm")]
    programm: CompactString,
    #[serde(rename = "gwhorizont")]
    horizont: CompactString,
    #[serde(rename = "gwleiter")]
    leiter: CompactString,
    #[serde(rename = "wasserart")]
    wasserart: CompactString,
    #[serde(rename = "e32")]
    ostwert: CompactString,
    #[serde(rename = "n32")]
    nordwert: CompactString,
    gemeinde_id: CompactString,
    eigentuemer: CompactString,
    betreiber: CompactString,
}

#[allow(clippy::too_many_arguments)]
async fn translate_record(
    dir: &Dir,
    client: &Client,
    source: &Source,
    proj: &dyn Projection,
    substances: &HashMap<usize, String>,
    chemicals: &HashMap<CompactString, Chemical>,
    groundwater: &HashMap<CompactString, Groundwater>,
    record: Result<Record, CsvError>,
) -> Result<(usize, usize, usize)> {
    let record = record?;

    let mut title = format!("{} {}", record.bauart.trim(), record.name.trim());

    let programm = record.programm.trim();
    if !programm.is_empty() {
        write!(&mut title, " ({})", programm)?;
    }

    let mut description = format!("Grundwassermessstellen dienen der Überwachung des Grundwassers.\nDieser Datensatz enthält die Messdaten der Messstelle {}.\n", record.name.trim());

    let mut add_line = |label, value: &str| -> Result<_> {
        let value = value.trim();
        if !value.is_empty() {
            writeln!(&mut description, " {label}: {value}")?;
        }
        Ok(())
    };

    add_line("Horizont", &record.horizont)?;
    add_line("Leiter", &record.leiter)?;
    add_line("Wasserart", &record.wasserart)?;

    let stoffdaten = chemicals
        .get(&record.id)
        .into_iter()
        .flat_map(|chemical| {
            chemical
                .substances
                .iter()
                .filter_map(|substance| match substances.get(substance) {
                    Some(substance) => Some(substance.clone()),
                    None => {
                        tracing::error!("Unknown substance ID {substance}");
                        None
                    }
                })
        })
        .collect::<SmallVec<_>>();

    let mut organisations = SmallVec::new();
    let eigentuemer = record.eigentuemer.trim();
    if !eigentuemer.is_empty() && eigentuemer != "keine Angabe" {
        organisations.push(Organisation::Other {
            name: eigentuemer.to_owned(),
            role: OrganisationRole::Owner,
            websites: Default::default(),
        });
    }
    let betreiber = record.betreiber.trim();
    if !betreiber.is_empty() && betreiber != "keine Angabe" {
        organisations.push(Organisation::Other {
            name: betreiber.to_owned(),
            role: OrganisationRole::Operator,
            websites: Default::default(),
        });
    }

    // These will mostly be resolved as ARS/AGS.
    let region = Region::Other(record.gemeinde_id);

    let bounding_box = if record.ostwert.contains('X') || record.nordwert.contains('X') {
        let x1 = record.ostwert.replace('X', "0").parse::<f64>()?;
        let x2 = record.ostwert.replace('X', "9").parse::<f64>()?;

        let y1 = record.nordwert.replace('X', "0").parse::<f64>()?;
        let y2 = record.nordwert.replace('X', "9").parse::<f64>()?;

        Rect::new(Coord { x: x1, y: y1 }, Coord { x: x2, y: y2 })
    } else {
        let x = record.ostwert.parse::<f64>()?;
        let y = record.nordwert.parse::<f64>()?;

        Rect::new(Coord { x, y }, Coord { x, y })
    };

    let time_ranges = chemicals
        .get(&record.id)
        .map(|chemical| (chemical.min_date, chemical.max_date).into())
        .into_iter()
        .chain(
            groundwater
                .get(&record.id)
                .map(|groundwater| (groundwater.min_date, groundwater.max_date).into()),
        )
        .collect();

    let resources = smallvec![
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("Download der Daten der Grundwassermessstellen in NRW".to_owned()),
            url: source.url.clone().into(),
            primary_content: true,
            direct_link: false,
        },
        Resource {
            r#type: ResourceType::Zip,
            description: Some("Datensatzbeschreibung: Hinweise zu den bereitgestellten Grundwasserdaten".to_owned()),
            url: source.url.join("OpenHygrisC_meta.zip")?.into(),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("Online-Hilfe von ELWAS-WEB".to_owned()),
            url: "https://www.elwasweb.nrw.de/elwas-web/help/index.xhtml".into(),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("Metadaten im Geoportal NRW".to_owned()),
            url: "https://www.geoportal.nrw/?activetab=map#/datasets/iso/34673d09-7dbd-4d93-b30b-c7279638a1c8".into(),
            direct_link: false,
            ..Default::default()
        },
    ];

    let mut types = SmallVec::new();

    types.push(Type::Measurements {
        domain: Domain::Groundwater,
        station: Some(Station {
            id: Some(record.name.clone()),
            measurement_frequency: None,
            reporting_obligations: if record.wrrl_pegel.contains("ja") {
                smallvec![ReportingObligation::Wrrl]
            } else {
                Default::default()
            },
        }),
        measured_variables: if record.pegel_bool.contains("ja") {
            smallvec!["Grundwasserstand".to_owned()]
        } else {
            Default::default()
        },
        methods: Default::default(),
    });

    if record.guete_bool.contains("ja") {
        types.push(Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(record.name),
                measurement_frequency: None,
                reporting_obligations: if record.wrrl_chemie.contains("ja") {
                    smallvec![ReportingObligation::Wrrl]
                } else {
                    Default::default()
                },
            }),
            measured_variables: stoffdaten,
            methods: Default::default(),
        });
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        regions: smallvec![region],
        bounding_boxes: smallvec![proj_rect(bounding_box, proj)],
        time_ranges,
        organisations,
        resources,
        language: Language::German,
        license: License::DlDeZero20,
        origins: source.origins.clone(),
        source_url: source.source_url.clone().unwrap(),
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(
        dir,
        client,
        source,
        format!("messstelle-{}", record.id),
        dataset,
    )
    .await
}

#[derive(Deserialize)]
struct SubstanceRecord {
    #[serde(rename = "stoff_nr")]
    id: usize,
    #[serde(rename = "name")]
    name: String,
    #[serde(rename = "beschreibung")]
    description: String,
}

async fn fetch_substances(client: &Client, source: &Source) -> Result<HashMap<usize, String>> {
    let mut substances = HashMap::new();

    let text = download_and_extract(
        client,
        source,
        "stoffliste".to_owned(),
        "/produkte/umwelt_klima/wasser/stoffliste/nrw-stoffe_EPSG25832_CSV.zip",
        "nrw-stoffe.csv",
        None,
    )
    .await?;

    let mut reader = ReaderBuilder::new()
        .delimiter(b';')
        .from_reader(text.as_bytes());

    for record in reader.deserialize::<SubstanceRecord>() {
        match record {
            Ok(record) => {
                let mut name = record.name.trim().to_owned();

                let description = record.description.trim();
                if !description.is_empty() {
                    write!(&mut name, " ({description})")?;
                }

                if !name.is_empty() {
                    substances.insert(record.id, name);
                }
            }
            Err(err) => tracing::warn!("Failed to read substance record: {err}"),
        }
    }

    Ok(substances)
}

#[derive(Deserialize)]
struct ChemicalRecord {
    #[serde(rename = "messstelle_id")]
    id: CompactString,
    #[serde(rename = "datum_pn")]
    date: Date,
    #[serde(rename = "stoff_nr")]
    substance: usize,
}

struct Chemical {
    min_date: Date,
    max_date: Date,
    substances: HashSet<usize>,
}

impl Default for Chemical {
    fn default() -> Self {
        Self {
            min_date: Date::MAX,
            max_date: Date::MIN,
            substances: HashSet::new(),
        }
    }
}

async fn fetch_chemicals(
    client: &Client,
    source: &Source,
) -> Result<HashMap<CompactString, Chemical>> {
    let mut chemicals = HashMap::new();

    for range in [
        "1922-1969",
        "1970-1989",
        "1990-1999",
        "2000-2009",
        "2010-2019",
        "2020-2029",
    ] {
        let text = download_and_extract(
            client,
            source,
            format!("chemischer-messwert-{range}"),
            &format!("OpenHygrisC_gw-chemischer-messwert_{range}_EPSG25832_CSV.zip"),
            &format!("OpenHygrisC_gw-chemischer-messwert_{range}.csv"),
            Some(WINDOWS_1252),
        )
        .await?;

        let mut reader = ReaderBuilder::new()
            .delimiter(b';')
            .from_reader(text.as_bytes());

        for record in reader.deserialize::<ChemicalRecord>() {
            match record {
                Ok(record) => {
                    let chemical: &mut Chemical = chemicals.entry(record.id).or_default();

                    chemical.min_date = chemical.min_date.min(record.date);
                    chemical.max_date = chemical.max_date.max(record.date);

                    chemical.substances.insert(record.substance);
                }
                Err(err) => tracing::warn!("Failed to read chemical record: {err}"),
            }
        }
    }

    Ok(chemicals)
}

#[derive(Deserialize)]
struct GroundwaterRecord {
    #[serde(rename = "messstelle_id")]
    id: CompactString,
    #[serde(rename = "datum_messung")]
    date: Date,
}

struct Groundwater {
    min_date: Date,
    max_date: Date,
}

impl Default for Groundwater {
    fn default() -> Self {
        Self {
            min_date: Date::MAX,
            max_date: Date::MIN,
        }
    }
}

async fn fetch_groundwater(
    client: &Client,
    source: &Source,
) -> Result<HashMap<CompactString, Groundwater>> {
    let mut groundwater = HashMap::new();

    for range in [
        "1900-1969",
        "1970-1989",
        "1990-1999",
        "2000-2009",
        "2010-2019",
        "2020-2029",
    ] {
        let text = download_and_extract(
            client,
            source,
            format!("wasserstand-{range}"),
            &format!("OpenHygrisC_gw-wasserstand_{range}_EPSG25832_CSV.zip"),
            &format!("OpenHygrisC_gw-wasserstand_{range}.csv"),
            Some(WINDOWS_1252),
        )
        .await?;

        let mut reader = ReaderBuilder::new()
            .delimiter(b';')
            .from_reader(text.as_bytes());

        for record in reader.deserialize::<GroundwaterRecord>() {
            match record {
                Ok(record) => {
                    let groundwater: &mut Groundwater = groundwater.entry(record.id).or_default();

                    groundwater.min_date = groundwater.min_date.min(record.date);
                    groundwater.max_date = groundwater.max_date.max(record.date);
                }
                Err(err) => tracing::warn!("Failed to read groundwater record: {err}"),
            }
        }
    }

    Ok(groundwater)
}

async fn download_and_extract(
    client: &Client,
    source: &Source,
    key: String,
    archive: &str,
    file: &str,
    encoding: Option<&'static Encoding>,
) -> Result<TrackedResponse<String>> {
    let url = source.url.join(archive)?;

    client
        .make_request(source, key, Some(&url), |client| async {
            let bytes = client
                .get(url.clone())
                .send()
                .await?
                .error_for_status()?
                .bytes()
                .await?;

            let mut archive = ZipArchive::new(Cursor::new(&*bytes))?;

            let mut text = String::new();

            DecodeReaderBytesBuilder::new()
                .encoding(encoding)
                .build(archive.by_name(file)?)
                .read_to_string(&mut text)?;

            Ok::<_, Error>(text)
        })
        .await
}
