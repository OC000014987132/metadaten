use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use scraper::{Html, Selector};
use smallvec::smallvec;
use time::{macros::format_description, Date};
use url::Url;

use harvester::{
    client::Client, fetch_many, selectors, utilities::collect_text, write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let mut overview_old_url = None;

    let links_recent = {
        let text = client
            .fetch_text(source, "overview-recent".to_owned(), &source.url)
            .await?;
        let document = Html::parse_document(&text);

        document
            .select(&selectors.recent_hrefs)
            .filter_map(|element| {
                let href = element.attr("href").unwrap();

                if !href.starts_with("http://") {
                    Some(href)
                } else {
                    overview_old_url = Some(href.to_owned());

                    None
                }
            })
            .map(|href| {
                let url = source.url.join(href)?;
                let key = href.replace(['/', '#'], "-");

                let title = String::new();

                Ok(Link { url, key, title })
            })
            .collect::<Result<Vec<_>>>()?
    };

    let mut count = links_recent.len();

    let (results, errors) = fetch_many(0, 0, links_recent, |link| {
        fetch_submission_recent(dir, client, source, selectors, link)
    })
    .await;

    let links_old = {
        let url =
            Url::parse(&overview_old_url.ok_or_else(|| anyhow!("Missing URL to old overview"))?)?;

        let text = client
            .fetch_text(source, "overview-old".to_owned(), &url)
            .await?;
        let document = Html::parse_document(&text);

        document
            .select(&selectors.old_hrefs)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let url = Url::parse(href)?;
                let key = href.replace(['/', '#'], "-");

                let title = collect_text(element.text());

                Ok(Link { url, key, title })
            })
            .collect::<Result<Vec<_>>>()?
    };

    count += links_old.len();

    let (results, errors) = fetch_many(results, errors, links_old, |link| {
        fetch_submission_old(dir, client, source, selectors, link)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_submission_recent(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Link,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", &link.url);

    let text = client
        .fetch_text(source, format!("submission-recent-{}", link.key), &link.url)
        .await?;

    let dataset = {
        let document = Html::parse_document(&text);

        let title = format!("{} German Informative Inventory Report", link.key);

        let description = collect_text(
            document
                .select(&selectors.recent_description)
                .flat_map(|element| element.text()),
        );

        let modified = document
            .select(&selectors.recent_modified)
            .filter_map(|elem| {
                let text = collect_text(elem.text());

                if text.starts_with("start.txt") {
                    Some(text)
                } else {
                    None
                }
            })
            .next()
            .map(|text| -> Result<Date> {
                let date = text
                    .split_ascii_whitespace()
                    .nth(4)
                    .ok_or_else(|| anyhow!("Missing modification date"))?;

                let date = Date::parse(date, format_description!("[year]/[month]/[day]"))?;

                Ok(date)
            })
            .transpose()?;

        let issued = document
            .select(&selectors.recent_issued)
            .filter_map(|element| {
                let text = collect_text(element.text());

                if text.starts_with("Report to be completed in:") {
                    Some(text)
                } else {
                    None
                }
            })
            .next()
            .map(|text| -> Result<Date> {
                let mut text = text.split_ascii_whitespace();

                let month = text
                    .nth(5)
                    .ok_or_else(|| anyhow!("Missing month in issued date"))?;
                let year = text
                    .next()
                    .ok_or_else(|| anyhow!("Missing year in issued date"))?;

                let date = Date::from_calendar_date(year.parse()?, month.parse()?, 1)?;

                Ok(date)
            })
            .transpose()?;

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Report,
            }],
            modified,
            issued,
            origins: source.origins.clone(),
            source_url: link.url.into(),
            language: Language::English,
            license: License::AllRightsReserved,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, link.key, dataset).await
}

async fn fetch_submission_old(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Link,
) -> Result<(usize, usize, usize)> {
    let text = client
        .fetch_text(source, format!("submission-old-{}", link.key), &link.url)
        .await?;

    let dataset = {
        let document = Html::parse_document(&text);

        let title = link.title;

        let description = collect_text(
            document
                .select(&selectors.old_description)
                .flat_map(|element| element.text()),
        );

        let year = title
            .split_ascii_whitespace()
            .next()
            .ok_or_else(|| anyhow!("Missing year in title"))?;

        let issued = Date::from_ordinal_date(year.parse()?, 1)?;

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Report,
            }],
            issued: Some(issued),
            origins: source.origins.clone(),
            source_url: link.url.into(),
            language: Language::English,
            license: License::AllRightsReserved,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, link.key, dataset).await
}

#[derive(Debug)]
struct Link {
    url: Url,
    key: String,
    title: String,
}

selectors! {
    recent_hrefs: "a[href]",
    recent_description: "h3[id], div.level3 > p",
    recent_modified: "div.page-footer",
    recent_issued: "div.wrap_center.wrap_round.wrap_info.plugin_wrap > p",
    old_hrefs: "div.feature.feature-header a[href]",
    old_description: "h1[id='toc0'], h2[id='toc1']",
}
