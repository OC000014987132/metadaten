use std::fmt::Write;

use anyhow::{anyhow, ensure, Result};
use cap_std::fs::Dir;
use csv::ReaderBuilder;
use miniproj::get_projection;
use regex::Regex;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_roxmltree::from_str;
use smallvec::{smallvec, SmallVec};
use time::{serde::format_description, Date};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{point_like_bounding_box, select_text},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, ReportingObligation, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();
    let proj = get_projection(25832).unwrap();

    let source_url = &Url::parse(
        source
            .source_url
            .as_deref()
            .expect("No source URL configured"),
    )?;

    let response = {
        let mut url = source.url.join("/WFS_UWAT")?;
        url.query_pairs_mut()
            .append_pair("service", "wfs")
            .append_pair("version", "2.0.0")
            .append_pair("request", "GetFeature")
            .append_pair("typeNames", "app:gwmn");

        let key = "wfs".to_owned();
        let text = client.fetch_text(source, key, &url).await?;
        from_str::<Collection>(&text)?
    };

    let count = response.members.len();

    let (results, errors) = fetch_many(0, 0, response.members, |member| async move {
        // these meta data are taken from the XML file provided by the Umweltportal Schleswig Holstein and are assumed to always exist
        let id = member.gwmn.id;

        let title = format!("Grundwassermessstelle {id}");
        let mut description = format!("Dieser Datensatz beschreibt die {title}.");

        let mut position = member.gwmn.geom.point.pos.split_whitespace();
        let rechtswert = position
            .next()
            .ok_or_else(|| anyhow!("Missing Rechtswert"))?
            .parse::<f64>()?;
        let hochwert = position
            .next()
            .ok_or_else(|| anyhow!("Missing Hochwert"))?
            .parse::<f64>()?;
        let bounding_boxes = smallvec![proj_rect(
            point_like_bounding_box(hochwert, rechtswert),
            proj
        )];

        // these meta data are taken from the landing page of the respective measurement station
        // consequently, not all data may exist
        let text = client
            .checked_fetch_text(
                source,
                maintenance_mode,
                format!("{id}.isol"),
                &member.gwmn.link,
            )
            .await?;
        let document = Html::parse_document(&text);

        // not all resources are always available
        let resource_links = document
            .select(&selectors.resource_links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>();

        let mut resources = SmallVec::new();
        let mut time_ranges = SmallVec::new();
        let mut types = SmallVec::new();

        for resource_link in resource_links {
            let resource_url = source_url.join(&resource_link)?;

            if resource_link.contains("grundwasserkoerper") {
                resources.push(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some("Steckbrief des beprobten Grundwasserkörpers".to_owned()),
                    url: resource_url.into(),
                    ..Default::default()
                });
            } else if resource_link.contains("BILDER") {
                resources.push(Resource {
                    r#type: ResourceType::Image,
                    description: Some("Bild der Messstation".to_owned()),
                    url: resource_url.into(),
                    ..Default::default()
                });
            } else if resource_link.contains("AUSBAU") {
                resources.push(Resource {
                    r#type: ResourceType::Image,
                    description: Some("Schematische Darstellung der Messstation".to_owned()),
                    url: resource_url.into(),
                    ..Default::default()
                });
            } else if resource_link.contains("grundwasserstand") {
                resources.push(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some("Tabelle der letzten 100 Grundwasserstände".to_owned()),
                    url: resource_url.into(),
                    ..Default::default()
                });
            } else if resource_link.contains("ganglinie") {
                resources.push(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some("Ganglinie der Grundwasserstände".to_owned()),
                    url: resource_url.clone().into(),
                    ..Default::default()
                });

                let (_, ortnr) = resource_url
                    .query_pairs()
                    .find(|(key, _)| key == "ortnr")
                    .ok_or_else(|| anyhow!("Missing ortnr in ganglinie URL"))?;

                let download_url = source_url.join(&format!(
                    "GrundwasserChemie/ganglinie_download.php?inst_id=10&ortnr={}",
                    ortnr
                ))?;

                resources.push(Resource {
                    r#type: ResourceType::Csv,
                    description: Some("CSV-Datei der Grundwasserstände".to_owned()),
                    url: download_url.clone().into(),
                    primary_content: true,
                    ..Default::default()
                });

                let text = client
                    .checked_fetch_text(
                        source,
                        maintenance_mode,
                        format!("csv-file-{}.isol", ortnr),
                        &download_url,
                    )
                    .await?;
                let mut reader = ReaderBuilder::new()
                    .delimiter(b';')
                    .from_reader(text.as_bytes());

                let mut start_date = Date::MAX;
                let mut end_date = Date::MIN;

                for record in reader.deserialize::<Record>() {
                    let record = record?;

                    start_date = start_date.min(record.datum);
                    end_date = end_date.max(record.datum);
                }

                if start_date != Date::MAX && end_date != Date::MIN {
                    time_ranges.push((start_date, end_date).into());
                }
            } else if resource_link.contains("proben") {
                resources.push(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some("Übersicht der genommenen Proben".to_owned()),
                    url: resource_url.into(),
                    ..Default::default()
                });
            } else if resource_link.contains("parameter") {
                resources.push(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some("Messwerte einzelner Beschaffenheits-Parameter".to_owned()),
                    url: resource_url.into(),
                    ..Default::default()
                });
            } else if resource_link.contains("Methodenhandb") {
                resources.push(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some("Methodenhandbuch".to_owned()),
                    url: resource_url.into(),
                    ..Default::default()
                });
            }
        }

        let page_description = select_text(&document, &selectors.page_description);

        if let Some(groundw_body) = selectors.regex_groundw_body.captures(&page_description) {
            write!(
                &mut description,
                " Die Messstelle liegt im Grundwasserkörper {}.",
                &groundw_body[1]
            )?;
        }
        if let Some(wasserst) = selectors.regex_wasserst.captures(&page_description) {
            write!(
                &mut description,
                " Es liegen insgesamt {} Messwerte vor.",
                &wasserst[1]
            )?;
            types.push(Type::Measurements {
                domain: Domain::Groundwater,
                station: Some(Station {
                    id: Some(id.as_str().into()),
                    reporting_obligations: smallvec![ReportingObligation::Wrrl],
                    ..Default::default()
                }),
                measured_variables: smallvec!["Grundwasserstand".to_owned()],
                methods: Default::default(),
            });
        }
        if let Some(proben) = selectors.regex_proben.captures(&page_description) {
            write!(
                &mut description,
                " Es liegen außerdem {} Probenentnahmen vor (siehe Resourcen).",
                &proben[1]
            )?;
            types.push(Type::Measurements {
                domain: Domain::Chemistry,
                station: Some(Station {
                    id: Some(id.as_str().into()),
                    reporting_obligations: smallvec![ReportingObligation::Wrrl],
                    ..Default::default()
                }),
                measured_variables: smallvec!["Grundwassergüte".to_owned()],
                methods: Default::default(),
            });
        }

        let organisations = smallvec![Organisation::Other {
            name: "Ministerium für Energiewende, Klimaschutz, Umwelt und Natur Schleswig-Holstein"
                .to_owned(),
            role: OrganisationRole::Management,
            websites: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            regions: smallvec![Region::SH],
            bounding_boxes,
            time_ranges,
            organisations,
            resources,
            language: Language::German,
            license: License::DlDeBy20,
            origins: source.origins.clone(),
            source_url: member.gwmn.link.into(),
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, id, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

fn maintenance_mode(text: &str) -> Result<()> {
    ensure!(!text.contains("<p>Liebe(r) Besucher(in), die von Ihnen gewünschte Seite befindet sich grade im Wartungsmodus.</p>"), "HTML response indicates maintenance mode");

    Ok(())
}

#[derive(Debug, Deserialize)]
struct Collection {
    #[serde(rename = "member")]
    members: Vec<Member>,
}

#[derive(Debug, Deserialize)]
struct Member {
    gwmn: Gwmn,
}

#[derive(Debug, Deserialize)]
struct Gwmn {
    #[serde(rename = "id")]
    id: String,
    #[serde(rename = "Link")]
    link: Url,
    #[serde(rename = "GEOM")]
    geom: Geom,
}

#[derive(Debug, Deserialize)]
struct Geom {
    #[serde(rename = "Point")]
    point: Point,
}

#[derive(Debug, Deserialize)]
struct Point {
    #[serde(rename = "pos")]
    pos: String,
}

#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "Datum", with = "german_date")]
    datum: Date,
}

format_description!(
    german_date,
    Date,
    "[day].[month].[year] [hour]:[minute]:[second]"
);

selectors! {
    resource_links: "a[href]",
    extra_links: "p:nth-child(23) a",
    page_description: "#gsb_content",
    regex_groundw_body: "Grundwasserkörper(.*?) Überwachungsaufgabe" as Regex,
    regex_proben: "Probenahmen Es liegen (.*?) Proben vor." as Regex,
    regex_wasserst: "Es liegen (.*?) Wasserstände vor." as Regex,
    regex_name: "Grundwassermessstelle(.*?)Grundwasserkörper" as Regex,
}
