use std::io::Cursor;
use std::iter::once;
use std::sync::Arc;

use anyhow::{anyhow, bail, Context, Result};
use atomic_refcell::AtomicRefCell;
use calamine::{open_workbook_from_rs, DataType, Reader as _, Xlsx};
use cap_std::fs::Dir;
use geo::{
    algorithm::{BooleanOps, BoundingRect, MapCoordsInPlace},
    Coord, MultiPolygon, Rect,
};
use miniproj::get_projection;
use shapefile::{dbase::FieldValue, Reader, Shape};
use tempfile::TempDir;
use url::Url;
use zip::ZipArchive;

use harvester::{client::Client, fetch_many, utilities::make_key, Source};
use metadaten::{
    ars_ags::{Database, Entry},
    merge_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let database = AtomicRefCell::new(Database::default());

    {
        // Add customized ARS and AGS for Germany since the datasets are at least located in Germany
        let mut database = database.borrow_mut();

        database.ars.insert(
            0,
            Arc::new(Entry {
                name: "Bundesrepublik Deutschland".into(),
                bounding_box: None,
                shape: None,
            }),
        );

        database.ags.insert(0, 0);
    }

    let (mut count, mut results, mut errors) = fetch_excel(client, source, &database).await?;

    let (count1, results1, errors1) = fetch_shapefile(client, source, &database).await?;
    count += count1;
    results += results1;
    errors += errors1;

    database.borrow().write(dir)?;

    Ok((count, results, errors))
}

async fn fetch_shapefile(
    client: &Client,
    source: &Source,
    database: &AtomicRefCell<Database>,
) -> Result<(usize, usize, usize)> {
    let temp_dir = TempDir::new()?;

    {
        let bytes = client
            .fetch_bytes(source, "shapefile".to_owned(), &source.url)
            .await?;

        let mut archive = ZipArchive::new(Cursor::new(&*bytes))?;
        archive.extract(temp_dir.path())?;
    }

    let mut count = 0;
    let mut results = 0;

    let proj = get_projection(25832).unwrap();

    for layer in ["LAN", "RBZ", "KRS", "VWG", "GEM"] {
        let mut reader = Reader::from_path(temp_dir.path().join(format!(
            "vg250_01-01.utm32s.shape.ebenen/vg250_ebenen_0101/VG250_{layer}.shp"
        )))
        .with_context(|| format!("Failed to open layer {layer}"))?;

        for entry in reader.iter_shapes_and_records() {
            count += 1;

            let (geometry, attributes) = entry?;

            let mut new_shape = match geometry {
                Shape::Polygon(polygon) => MultiPolygon::from(polygon),
                _ => bail!("Unexpected type of geometry"),
            };

            new_shape.map_coords_in_place(|coord| {
                let (x, y) = proj.projected_to_deg(coord.x, coord.y);

                Coord { x, y }
            });

            let new_bounding_box = new_shape.bounding_rect().unwrap();

            let ars_key = match attributes.get("ARS_0") {
                Some(FieldValue::Character(Some(content))) => match content.parse::<u64>() {
                    Ok(key) => key,
                    Err(_err) => continue,
                },
                _ => continue,
            };

            if let Some(entry) = database.borrow_mut().ars.get_mut(&ars_key) {
                results += 1;

                let entry = Arc::get_mut(entry).unwrap();

                match &mut entry.bounding_box {
                    Some(old_bounding_box) => merge_rect(old_bounding_box, new_bounding_box),
                    old_bounding_box @ None => *old_bounding_box = Some(new_bounding_box),
                }

                match &mut entry.shape {
                    Some(old_shape) => *old_shape = old_shape.union(&new_shape),
                    old_shape @ None => *old_shape = Some(new_shape),
                }
            }
        }
    }

    Ok((count, results, 0))
}

async fn fetch_excel(
    client: &Client,
    source: &Source,
    database: &AtomicRefCell<Database>,
) -> Result<(usize, usize, usize)> {
    let file_names = (2010..=2022)
        .map(|year| format!("GVAuszugJ/3112{year}_Auszug_GV"))
        .chain(once("GVAuszugQ/AuszugGV3QAktuell".to_owned()));

    let (results, errors) = fetch_many(0, 0, file_names, |file_name| async move {
        let url = source
            .source_url()
            .replace("{{file_name}}", &file_name)
            .parse::<Url>()?;

        let key = make_key(&file_name).into_owned();

        let bytes = client.fetch_bytes(source, key, &url).await?;

        let mut workbook: Xlsx<_> = open_workbook_from_rs(Cursor::new(&*bytes))?;

        let (_name, reader) = workbook
            .worksheets()
            .into_iter()
            .find(|(name, _reader)| {
                name.starts_with("Gemeindedaten") || name.starts_with("Onlineprodukt_Gemeinden")
            })
            .ok_or_else(|| anyhow!("Worksheet not found"))?;

        let mut count = 0;
        let mut results = 0;

        for row in reader.rows().skip(6) {
            count += 1;

            if row.len() < 16 {
                continue;
            }

            results += 1;

            let region = row[2]
                .get_string()
                .unwrap_or_default()
                .parse::<u64>()
                .unwrap_or_default();
            let district = row[3]
                .get_string()
                .unwrap_or_default()
                .parse::<u64>()
                .unwrap_or_default();
            let county = row[4]
                .get_string()
                .unwrap_or_default()
                .parse::<u64>()
                .unwrap_or_default();
            let association = row[5]
                .get_string()
                .unwrap_or_default()
                .parse::<u64>()
                .unwrap_or_default();
            let municipality = row[6]
                .get_string()
                .unwrap_or_default()
                .parse::<u64>()
                .unwrap_or_default();

            let name = row[7].get_string().unwrap_or_default().into();

            let bounding_box = match (row[14].get_string(), row[15].get_string()) {
                (Some(longitude), Some(latitude))
                    if !longitude.is_empty() && !latitude.is_empty() =>
                {
                    let coord = Coord {
                        x: longitude.replace(',', ".").parse()?,
                        y: latitude.replace(',', ".").parse()?,
                    };

                    Some(Rect::new(coord, coord))
                }
                _ => None,
            };

            let ars_key = region * 10000000000
                + district * 1000000000
                + county * 10000000
                + association * 1000
                + municipality;

            let ags_key = region * 1000000 + district * 100000 + county * 1000 + municipality;

            if ars_key == 0 {
                continue;
            }

            let mut database = database.borrow_mut();

            database.ars.insert(
                ars_key,
                Arc::new(Entry {
                    name,
                    bounding_box,
                    shape: None,
                }),
            );

            database.ags.insert(ags_key, ars_key);
        }

        Ok((count, results, 0))
    })
    .await;

    Ok((results, results, errors))
}
