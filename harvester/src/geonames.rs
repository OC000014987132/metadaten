use std::fs::create_dir_all;
use std::io::{Cursor, Read};

use anyhow::{anyhow, Error, Result};
use cap_std::fs::Dir;
use csv::{ReaderBuilder, Result as CsvResult};
use serde::{Deserialize, Deserializer};
use tantivy::{
    directory::MmapDirectory,
    schema::{
        Field, IndexRecordOption, NumericOptions, Schema, TantivyDocument, TextFieldIndexing,
        TextOptions,
    },
    store::Compressor,
    tokenizer::{LowerCaser, RawTokenizer, TextAnalyzer},
    Index, IndexSettings, IndexWriter,
};
use tokio::task::spawn_blocking;
use zip::ZipArchive;

use harvester::{client::Client, Source};
use metadaten::data_path_from_env;

pub async fn harvest(
    _dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let file_name = source
        .url
        .path_segments()
        .and_then(|mut segments| segments.next_back())
        .ok_or_else(|| anyhow!("Failed to extract file name from URL"))?
        .replace(".zip", ".txt");

    let bytes = client
        .make_request(
            source,
            "dump".to_owned(),
            Some(&source.url),
            |client| async {
                let bytes = client
                    .get(source.url.clone())
                    .send()
                    .await?
                    .error_for_status()?
                    .bytes()
                    .await?;

                let mut archive = ZipArchive::new(Cursor::new(&*bytes))?;

                let mut bytes = Vec::new();
                archive.by_name(&file_name)?.read_to_end(&mut bytes)?;

                Ok::<_, Error>(bytes)
            },
        )
        .await?;

    spawn_blocking(move || write_index(&bytes)).await.unwrap()
}

fn write_index(bytes: &[u8]) -> Result<(usize, usize, usize)> {
    let exact = TextOptions::default()
        .set_indexing_options(
            TextFieldIndexing::default()
                .set_tokenizer("lowercase")
                .set_fieldnorms(false)
                .set_index_option(IndexRecordOption::Basic),
        )
        .set_stored();

    let tokenized = TextOptions::default().set_indexing_options(
        TextFieldIndexing::default()
            .set_tokenizer("default")
            .set_fieldnorms(false)
            .set_index_option(IndexRecordOption::Basic),
    );

    let mut schema = Schema::builder();

    let id = schema.add_u64_field("id", NumericOptions::default().set_indexed().set_fast());

    let name = schema.add_text_field("name", exact.clone());
    let tok_name = schema.add_text_field("tok_name", tokenized);

    let lat = schema.add_f64_field("lat", NumericOptions::default().set_fast());
    let lon = schema.add_f64_field("lon", NumericOptions::default().set_fast());

    let fields = Fields {
        id,
        name,
        tok_name,
        lat,
        lon,
    };

    let schema = schema.build();

    let index_path = data_path_from_env().join("geonames");

    create_dir_all(&index_path)?;

    let index = Index::builder()
        .schema(schema)
        .settings(IndexSettings {
            docstore_compression: Compressor::Zstd(Default::default()),
            ..Default::default()
        })
        .open_or_create(MmapDirectory::open(index_path)?)?;

    index.tokenizers().register(
        "lowercase",
        TextAnalyzer::builder(RawTokenizer::default())
            .filter(LowerCaser)
            .build(),
    );

    let mut writer = index.writer(256 << 20)?;

    writer.delete_all_documents()?;

    let mut reader = ReaderBuilder::new()
        .delimiter(b'\t')
        .has_headers(false)
        .from_reader(bytes);

    let mut results = 0;
    let mut errors = 0;

    for record in reader.deserialize::<Record>() {
        match translate_record(&mut writer, &fields, record) {
            Ok(()) => results += 1,
            Err(err) => {
                tracing::error!("Failed to translate GeoNames record: {err:#}");

                errors += 1;
            }
        }
    }

    writer.commit()?;

    Ok((results, results, errors))
}

fn translate_record(
    writer: &mut IndexWriter,
    fields: &Fields,
    record: CsvResult<Record>,
) -> Result<()> {
    let record = record?;

    let mut doc = TantivyDocument::default();

    doc.add_u64(fields.id, record.id);

    doc.add_text(fields.name, record.name.clone());
    doc.add_text(fields.tok_name, record.name);

    doc.add_f64(fields.lat, record.lat);
    doc.add_f64(fields.lon, record.lon);

    writer.add_document(doc)?;

    Ok(())
}

struct Fields {
    id: Field,
    name: Field,
    tok_name: Field,
    lat: Field,
    lon: Field,
}

#[derive(Debug, Deserialize)]
struct Record {
    id: u64,
    name: String,
    #[serde(deserialize_with = "skip_field")]
    _ascii_name: (),
    #[serde(deserialize_with = "skip_field")]
    _alt_names: (),
    lat: f64,
    lon: f64,
}

fn skip_field<'de, D>(deserializer: D) -> Result<(), D::Error>
where
    D: Deserializer<'de>,
{
    <&str>::deserialize(deserializer)?;

    Ok(())
}
