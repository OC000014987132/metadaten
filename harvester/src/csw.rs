use std::mem::take;

use anyhow::{anyhow, Context, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use cow_utils::CowUtils;
use geo::{Coord, Rect};
use once_cell::sync::Lazy;
use regex::{Regex, RegexSet};
use serde::Deserialize;
use serde_json::from_str as from_json_str;
use serde_roxmltree::{from_doc as from_xml_doc, roxmltree::Document};
use smallvec::SmallVec;
use time::{
    error::Parse as ParseError, format_description::well_known::Rfc3339,
    macros::format_description, Date,
};

use harvester::{
    client::Client,
    fetch_many,
    utilities::{parse_year_only, yesterday},
    write_dataset, Source,
};
use metadaten::{
    ars_ags::ARS_AGS,
    dataset::{
        Dataset, GlobalIdentifier, License, Organisation, OrganisationRole, Person, PersonRole,
        Region, Resource, ResourceType, Status, Tag, TimeRange,
    },
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let max_records = source.batch_size;

    let (count, results, errors) = fetch_datasets(dir, client, source, max_records, 1).await?;
    tracing::info!("Harvesting {} datasets", count);

    let requests = count.div_ceil(max_records);
    let start_pos = (1..requests).map(|request| 1 + request * max_records);

    let (results, errors) = fetch_many(results, errors, start_pos, |start_pos| async move {
        fetch_datasets(dir, client, source, max_records, start_pos)
            .await
            .with_context(|| format!("Failed to parse datasets at {start_pos}"))
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip(dir, client, source))]
async fn fetch_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    max_records: usize,
    start_pos: usize,
) -> Result<(usize, usize, usize)> {
    let start_pos = start_pos.to_string();

    tracing::debug!(
        "Fetching {} datasets starting at {}",
        max_records,
        start_pos
    );

    let mut url = source.url.clone();

    url.query_pairs_mut()
        .append_pair("service", "CSW")
        .append_pair("version", "2.0.2")
        .append_pair("request", "GetRecords")
        .append_pair("resultType", "results")
        .append_pair("ElementSetName", "full")
        .append_pair("outputSchema", "http://www.isotc211.org/2005/gmd")
        .append_pair("typeNames", "csw:Record")
        .append_pair("maxRecords", &max_records.to_string())
        .append_pair("startPosition", &start_pos);

    let body = client.fetch_text(source, start_pos, &url).await?;

    let document = Document::parse(&body)?;

    let response = from_xml_doc::<Response>(&document)?;

    let count = response.results.num_records_matched;
    let (results, errors) = fetch_many(0, 0, response.results.records, |record| {
        translate_dataset(dir, client, source, record)
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip_all, fields(key = record.file_identifier.text))]
pub async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    mut record: Record<'_>,
) -> Result<(usize, usize, usize)> {
    let identifier = record.file_identifier.text;

    let mut identification = record
        .identification_info
        .into_iter()
        .fold(None, |res, val| match (res, val) {
            // HACK: If more than one kind of identification is given, we prefer service over data
            // which is correct in the one case where we observed this so far.
            (None, val) => Some(val),
            (res @ Some(IdentificationInfo::Data(_)), IdentificationInfo::Data(_)) => res,
            (res @ Some(IdentificationInfo::Service(_)), IdentificationInfo::Service(_)) => res,
            (Some(IdentificationInfo::Data(_)), val @ IdentificationInfo::Service(_)) => Some(val),
            (res @ Some(IdentificationInfo::Service(_)), IdentificationInfo::Data(_)) => res,
            (res, IdentificationInfo::Empty) => res,
            (Some(IdentificationInfo::Empty), val) => Some(val),
        })
        .and_then(|identification_info| identification_info.identification())
        .ok_or_else(|| anyhow!("Missing identification for dataset {identifier}"))?;

    let license = identification.license();

    let citation = identification.citation;

    let global_identifier = GlobalIdentifier::parse(
        citation
            .inner
            .identifier
            .into_iter()
            .filter_map(|identifier| {
                identifier
                    .inner
                    .and_then(|inner| inner.code.text)
                    .filter(|text| !text.is_empty())
            })
            .next()
            .unwrap_or(identifier),
    );

    static CLEAN_LOCALE_ENG: Lazy<Regex> =
        Lazy::new(|| Regex::new(r"\(?\#?locale\-eng:.*").unwrap());

    let title = CLEAN_LOCALE_ENG
        .replace_all(citation.inner.title.text, "")
        .into_owned();

    let description = identification
        .r#abstract
        .and_then(|r#abstract| r#abstract.text)
        .map(|text| CLEAN_LOCALE_ENG.replace_all(text, "").into_owned());

    let modified = record
        .datestamp
        .map(DateStamp::parse)
        .transpose()?
        .flatten();

    let mut resources = SmallVec::new();

    if let Some(distribution_info) = record.distribution_info {
        for transfer in distribution_info.data.transfer {
            if let Some(options) = transfer.digital {
                for online_resource in options.online {
                    for resource in online_resource.resource {
                        if let Some(url) = resource.linkage.url {
                            let mut resource = Resource {
                                r#type: ResourceType::WebPage,
                                url,
                                ..Default::default()
                            }
                            .guess_or_keep_type();

                            resource.direct_link = resource.r#type != ResourceType::WebPage;
                            resource.primary_content = matches!(
                                resource.r#type,
                                ResourceType::Csv
                                    | ResourceType::Xml
                                    | ResourceType::PlainText
                                    | ResourceType::Zip
                                    | ResourceType::Tiff
                                    | ResourceType::Wfs
                                    | ResourceType::Wms
                            );

                            resources.push(resource);
                        }
                    }
                }
            }
        }
    }

    let language = identification
        .languages
        .iter()
        .find_map(|language| language.code())
        .or_else(|| {
            record
                .language
                .as_ref()
                .and_then(|language| language.code())
        })
        .map(|code| code.into())
        .unwrap_or_default();

    let mut tags = Vec::new();

    for keyword_group in identification.keywords {
        if let Some(outer) = keyword_group.outer {
            for keyword in outer.inner {
                if let Some(text) = keyword.value {
                    tags.push(Tag::Other(text));
                }
            }
        }
    }

    let persons = record
        .contacts
        .iter_mut()
        .filter_map(|contact| {
            let name = contact
                .responsible_party
                .as_mut()
                .and_then(|party| party.individual_name.take())
                .and_then(|name| name.text)
                .filter(|name| !name.is_empty());

            let mail_addresses = contact
                .responsible_party
                .as_mut()
                .and_then(|party| party.contact_info.as_mut())
                .into_iter()
                .flat_map(|contact| contact.inner.address.as_mut())
                .flat_map(|address| take(&mut address.inner.mail_addresses))
                .flat_map(|mail_address| mail_address.text)
                .collect();

            name.map(|name| (name, mail_addresses))
        })
        .map(|(name, mail_addresses)| Person {
            name,
            role: PersonRole::Contact,
            emails: mail_addresses,
            ..Default::default()
        })
        .collect();

    let organisations = record
        .contacts
        .iter_mut()
        .filter_map(|contact| {
            let name = contact
                .responsible_party
                .as_mut()
                .and_then(|party| party.organisation_name.take())
                .and_then(|name| name.text);

            let url = contact
                .responsible_party
                .as_mut()
                .and_then(|party| party.contact_info.as_mut())
                .and_then(|contact| contact.inner.online.as_mut())
                .filter(|online| {
                    // Some datasets contain links to employee profile images which we drop.
                    online
                        .inner
                        .profile
                        .as_ref()
                        .and_then(|profile| profile.text)
                        .is_none_or(|text| !text.starts_with("image/"))
                })
                .and_then(|online| online.inner.linkage.url.take());

            name.map(|name| (name, url))
        })
        .map(|(name, url)| Organisation::Other {
            name,
            role: OrganisationRole::Unknown,
            websites: url.into_iter().collect(),
        })
        .collect();

    let bounding_boxes = identification
        .extents
        .iter()
        .filter_map(|extent| extent.outer.as_ref())
        .flat_map(|extent| &extent.geographic)
        .flat_map(|geographic| &geographic.bounding_boxes)
        .filter(|bounding_box| {
            !bounding_box.lon_west.value.is_empty()
                && !bounding_box.lat_south.value.is_empty()
                && !bounding_box.lon_east.value.is_empty()
                && !bounding_box.lat_north.value.is_empty()
        })
        .map(|bounding_box| {
            // Due to ISO 19139 the CRS is WGS84: https://www.isotc211.org/2005/gmd/extent.xsd
            Ok(Rect::new(
                Coord {
                    x: bounding_box.lon_west.value.parse()?,
                    y: bounding_box.lat_south.value.parse()?,
                },
                Coord {
                    x: bounding_box.lon_east.value.parse()?,
                    y: bounding_box.lat_north.value.parse()?,
                },
            ))
        })
        .collect::<Result<SmallVec<_>>>()?;

    let time_ranges = identification
        .extents
        .iter()
        .filter_map(|extent| extent.outer.as_ref())
        .flat_map(|extent| &extent.temporal)
        .flat_map(|temporal| &temporal.extent)
        .filter_map(|extent| extent.inner.period.as_ref())
        .filter_map(|period| period.parse().transpose())
        .collect::<Result<SmallVec<_>, _>>()?;

    let status = if let Some(progress_code) = identification
        .status
        .into_iter()
        .filter_map(|status| status.progress_code)
        .next()
    {
        match progress_code.value {
            "historicalArchive"
            | "in operation"
            | "kontinuierliche Aktualisierung"
            | "onGoing"
            | "ongoing"
            | "required"
            | "completed" => Status::Active,
            "planned" => Status::Planned,
            "obsolete" | "superseded" => Status::Obsolete,
            "underDevelopment" => Status::UnderDevelopment,
            "nicht ausgewählt" => Default::default(),
            not_accounted => {
                tracing::debug!("Found unaccounted status {not_accounted}");
                Default::default()
            }
        }
    } else {
        Default::default()
    };

    let regions = identification
        .extents
        .iter_mut()
        .filter_map(|extent| extent.outer.as_mut())
        .flat_map(|extent| &mut extent.geographic)
        .filter_map(|inner| {
            inner
                .geo_description
                .as_mut()
                .and_then(|geo_description| geo_description.geo_identifier.as_mut())
                .and_then(|geo_identifier| geo_identifier.inner.as_mut())
                .and_then(|geo_identifier| {
                    geo_identifier
                        .geo_code
                        .anchor
                        .as_ref()
                        .and_then(|anchor| anchor.href)
                        .and_then(|href| ARS_AGS.extract(href))
                        .map(Region::RegionalKey)
                        .or_else(|| geo_identifier.geo_code.text.take().map(Region::from))
                })
        })
        .collect();

    let dataset = Dataset {
        title,
        description,
        origins: source.origins.clone(),
        license,
        regions,
        source_url: source.source_url().replace("{{id}}", identifier),
        machine_readable_source: true,
        resources,
        modified,
        language,
        tags,
        persons,
        organisations,
        bounding_boxes,
        time_ranges,
        global_identifier,
        status,
        ..Default::default()
    };

    write_dataset(dir, client, source, identifier.to_owned(), dataset).await
}

#[derive(Debug, Deserialize)]
struct Response<'a> {
    #[serde(rename = "SearchResults", borrow)]
    results: SearchResults<'a>,
}

#[derive(Debug, Deserialize)]
struct SearchResults<'a> {
    #[serde(rename = "numberOfRecordsMatched")]
    num_records_matched: usize,
    #[serde(rename = "MD_Metadata", borrow)]
    records: Vec<Record<'a>>,
}

#[derive(Debug, Deserialize)]
pub struct Record<'a> {
    #[serde(rename = "fileIdentifier", borrow)]
    file_identifier: FileIdentifier<'a>,
    #[serde(rename = "identificationInfo", borrow, default)]
    identification_info: SmallVec<[IdentificationInfo<'a>; 1]>,
    #[serde(rename = "contact", borrow, default)]
    contacts: Vec<Contact<'a>>,
    #[serde(rename = "distributionInfo", borrow)]
    distribution_info: Option<DistributionInfo<'a>>,
    #[serde(rename = "dateStamp", borrow)]
    datestamp: Option<DateStamp<'a>>,
    #[serde(borrow)]
    language: Option<Language<'a>>,
}

#[derive(Debug, Deserialize)]
struct FileIdentifier<'a> {
    #[serde(rename = "CharacterString")]
    text: &'a str,
}

#[derive(Debug, Deserialize)]
enum IdentificationInfo<'a> {
    #[serde(rename = "MD_DataIdentification", borrow)]
    Data(Identification<'a>),
    #[serde(rename = "SV_ServiceIdentification", borrow)]
    Service(Identification<'a>),
    #[serde(rename = "$text")]
    Empty,
}

impl<'a> IdentificationInfo<'a> {
    fn identification(self) -> Option<Identification<'a>> {
        match self {
            Self::Data(identification) => Some(identification),
            Self::Service(identification) => Some(identification),
            Self::Empty => None,
        }
    }
}

#[derive(Debug, Deserialize)]
struct Identification<'a> {
    #[serde(borrow)]
    citation: Citation<'a>,
    #[serde(borrow)]
    r#abstract: Option<Abstract<'a>>,
    #[serde(default, borrow)]
    status: Vec<CswStatus<'a>>,
    #[serde(rename = "resourceConstraints", default, borrow)]
    resource_constraints: Vec<ResourceConstraints<'a>>,
    #[serde(rename = "language", default, borrow)]
    languages: Vec<Language<'a>>,
    #[serde(rename = "descriptiveKeywords", default)]
    keywords: Vec<KeywordGroup>,
    #[serde(rename = "extent", default, borrow)]
    extents: Vec<ExtentGroup<'a>>,
}

#[derive(Debug, Deserialize)]
struct CswStatus<'a> {
    #[serde(rename = "MD_ProgressCode", borrow)]
    progress_code: Option<RestrictionCode<'a>>,
}

#[derive(Debug, Deserialize)]
struct KeywordGroup {
    #[serde(rename = "MD_Keywords")]
    outer: Option<Keyword>,
}

#[derive(Debug, Deserialize)]
struct Keyword {
    #[serde(rename = "keyword", default)]
    inner: Vec<KeywordInner>,
}

#[derive(Debug, Deserialize)]
struct KeywordInner {
    #[serde(rename = "CharacterString")]
    value: Option<CompactString>,
}

#[derive(Debug, Deserialize)]
struct ExtentGroup<'a> {
    #[serde(rename = "EX_Extent", borrow)]
    outer: Option<Extent<'a>>,
}

#[derive(Debug, Deserialize)]
struct Extent<'a> {
    #[serde(rename = "geographicElement", default, borrow)]
    geographic: Vec<GeographicElement<'a>>,
    #[serde(rename = "temporalElement", default, borrow)]
    temporal: Vec<TemporalElement<'a>>,
}

#[derive(Debug, Deserialize)]
struct GeographicElement<'a> {
    #[serde(rename = "EX_GeographicBoundingBox", default, borrow)]
    bounding_boxes: Vec<BoundingBox<'a>>,
    #[serde(rename = "EX_GeographicDescription", borrow)]
    geo_description: Option<GeoDescription<'a>>,
}

#[derive(Debug, Default, Deserialize)]
struct BoundingBox<'a> {
    #[serde(rename = "westBoundLongitude", default, borrow)]
    lon_west: Decimal<'a>,
    #[serde(rename = "eastBoundLongitude", default, borrow)]
    lon_east: Decimal<'a>,
    #[serde(rename = "southBoundLatitude", default, borrow)]
    lat_south: Decimal<'a>,
    #[serde(rename = "northBoundLatitude", default, borrow)]
    lat_north: Decimal<'a>,
}

#[derive(Debug, Deserialize)]
struct GeoDescription<'a> {
    #[serde(rename = "geographicIdentifier", borrow)]
    geo_identifier: Option<GeoIdentifier<'a>>,
}

#[derive(Debug, Deserialize)]
struct GeoIdentifier<'a> {
    #[serde(rename = "MD_Identifier", borrow)]
    inner: Option<GeoIdentifierInner<'a>>,
}

#[derive(Debug, Deserialize)]
struct GeoIdentifierInner<'a> {
    #[serde(rename = "code", borrow)]
    geo_code: GeoCode<'a>,
}

#[derive(Debug, Deserialize)]
struct GeoCode<'a> {
    #[serde(rename = "CharacterString")]
    text: Option<CompactString>,
    #[serde(rename = "Anchor", borrow)]
    anchor: Option<Anchor<'a>>,
}

#[derive(Debug, Default, Deserialize)]
struct Decimal<'a> {
    #[serde(rename = "Decimal")]
    value: &'a str,
}

#[derive(Debug, Deserialize)]
struct TemporalElement<'a> {
    #[serde(rename = "EX_TemporalExtent", default, borrow)]
    extent: Vec<TemporalExtent<'a>>,
}

#[derive(Debug, Deserialize)]
struct TemporalExtent<'a> {
    #[serde(rename = "extent", borrow)]
    inner: TemporalExtentInner<'a>,
}

#[derive(Debug, Deserialize)]
struct TemporalExtentInner<'a> {
    #[serde(rename = "TimePeriod", borrow)]
    period: Option<TimePeriod<'a>>,
}

#[derive(Debug, Deserialize, Clone)]
struct TimePeriod<'a> {
    #[serde(rename = "beginPosition")]
    begin: Option<&'a str>,
    #[serde(rename = "endPosition")]
    end: Option<&'a str>,
}

impl TimePeriod<'_> {
    fn parse(&self) -> Result<Option<TimeRange>, ParseError> {
        let begin = match self.begin {
            Some(begin) if !begin.is_empty() => parse_date(begin)?,
            _ => return Ok(None),
        };

        let end = match self.end {
            Some(end) if !end.is_empty() => parse_date(end)?,
            _ => yesterday(),
        };

        Ok(Some((begin, end).into()))
    }
}

impl Identification<'_> {
    /// Extract the license ID for Open Data licenses
    ///
    /// Based on section 3.6 from [Konventionen zu Metadaten][https://www.gdi-de.org/download/AK_Metadaten_Konventionen_zu_Metadaten.pdf].
    fn license(&self) -> License {
        for resource_constraints in &self.resource_constraints {
            if let Some(legal_constraints) = &resource_constraints.legal_constraints {
                for use_constraints in &legal_constraints.use_constraints {
                    if use_constraints.restriction_code.value == "otherRestrictions" {
                        for other_constraints in &legal_constraints.other_constraints {
                            if let Some(text) = &other_constraints.text {
                                if let Ok(license) = from_json_str::<CswLicense>(text) {
                                    let license = license.id.into();

                                    if license != License::Unknown {
                                        return license;
                                    }
                                }
                            }
                        }

                        for other_constraints in &legal_constraints.other_constraints {
                            if let Some(anchor) = &other_constraints.anchor {
                                if let Some(href) = anchor.href {
                                    let license = href.into();

                                    if license != License::Unknown {
                                        return license;
                                    }
                                }
                            }
                        }

                        for other_constraints in &legal_constraints.other_constraints {
                            if let Some(text) = &other_constraints.text {
                                let mut license = text.cow_replace('"', "").as_ref().into();

                                if license == License::Unknown {
                                    license = match_remaining_licenses(text.trim());
                                }

                                if license != License::Unknown {
                                    return license;
                                }
                            }
                        }

                        break;
                    }
                }
            }
        }

        License::Unknown
    }
}

fn match_remaining_licenses(text: &str) -> License {
    struct Patterns {
        parse_url: Regex,
        parse_name: Regex,
        dl_de_by: RegexSet,
        dl_de_zero: RegexSet,
        cc_by_sa_40: Regex,
        geo_nutz: Regex,
        all_rights_reserved: RegexSet,
    }

    impl Default for Patterns {
        fn default() -> Self {
            Self {
                parse_url: Regex::new(r"(?<href>https?://(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9(@:%_\+.~,#?&//=]*))").unwrap(),
                parse_name: Regex::new(r"(?:Licence|License|Lizenz): (?<name>[^;]+)(?:;|$)").unwrap(),
                dl_de_by: RegexSet::new([
                    r#"^(\{ "id"\: "dl\-de\/by\-2\-0", "name"\: "Datenlizenz Deutschland – Namensnennung – Version 2\.0",)"#,
                    r"^(Datenlizenz Deutschland – Namensnennung – Version 2\.0 )",
                    r"^(Die Geodaten können unter den Bedingungen der Lizenz „Datenlizenz Deutschland – Namensnennung – Version 2\.0“.*kostenfrei)",
                    r"^(Dieser (?:Datensatz|Dienst)\s?kann.*[lL]izenz.*Deutschland[–\s-]*?Namensnennung[–\s-]*?(?:Version)?\s*2\.0.*genutzt)",
                    r"^(Es gelten keine Bedingungen Dieser Datensatz kann gemäß der Lizenz.*Datenlizenz Deutschland Namensnennung 2.0)",
                    r"^(Für die Nutzung der Daten ist die  Datenlizenz Deutschland - Namensnennung - Version 2.0 anzuwenden)"
                ])
                .unwrap(),
                dl_de_zero: RegexSet::new([
                    r#"^(\{ "id"\: "dl\-de\/by\-2\-0", "name"\: "Datenlizenz Deutschland – Namensnennung – Version 2\.0",)"#,
                    r"^(Es gelten die Lizenzbedingungen.*Datenlizenz Deutschland.*Zero.*Version 2.0)",
                ])
                .unwrap(),
                cc_by_sa_40: Regex::new(r"^Creative Commons Attribution-ShareAlike 4\.0").unwrap(),
                geo_nutz: Regex::new(r"(Nutzungsbedingungen: Die Bundesanstalt für Gewässerkunde stellt die Geodaten im Rahmen der Geodatennutzungsverordnung des Bundes \(GeoNutzV\))").unwrap(),
                all_rights_reserved: RegexSet::new([
                    r"^[Uu]rheber",
                    r"^(Copyright: Das rote gotische Apotheken-A mit Kelch und Schlange)",
                    r"^(&copy; L)",
                    r"©LZW",
                    r"^Nutzungsbedingungen: intellectual property rights",
                    r"(intellectual property rightsSächsisches Umweltinformationsgesetz \(SächsUIG\))",
                    r"^(Quellenvermerk:)",
                    r"Alle Nutzungsarten sind frei, aber grundsätzlich wird nicht auf das Urheberrecht verzichtet.",
                    r"^Diese.*([uU]rheberrechtlich geschützt)",
                    r"^Allgemeine Geschäftsbedingungen",
                    r"^(Terms of Use: )?General (Standard )?(T|t)erms and (C|c)onditions",
                ])
                .unwrap(),
            }
        }
    }

    static PATTERNS: Lazy<Patterns> = Lazy::new(Patterns::default);

    let patterns = &*PATTERNS;

    if patterns.dl_de_by.is_match(text) {
        return License::DlDeBy20;
    }

    if patterns.dl_de_zero.is_match(text) {
        return License::DlDeZero20;
    }

    if patterns.cc_by_sa_40.is_match(text) {
        return License::CcBySa40;
    }

    if patterns.geo_nutz.is_match(text) {
        return License::GeoNutz20130319;
    }

    if patterns.all_rights_reserved.is_match(text) {
        return License::AllRightsReserved;
    }

    if let Some(capture) = patterns.parse_url.captures(text) {
        let license: License = capture["href"].into();

        if license != License::Unknown {
            return license;
        }
    }

    if let Some(capture) = patterns.parse_name.captures(text) {
        let license: License = capture["name"].into();

        if license != License::Unknown {
            return license;
        }
    }

    match text {
        "Beschränkt" => License::OtherClosed,
        "otherRestrictions"  => License::OtherClosed,
        "Es gelten keine Bedingungen." => License::OtherOpen,
        "es gelten keine Bedingungen" => License::OtherOpen,
        "Es gelten keine Nutzungseinschränkungen" => License::OtherOpen,
        "Es gelten keine Zugriffsbeschränkungen." => License::OtherOpen,
        "Es gelten keine Zugriffsbeschränkungen" => License::OtherOpen,
        "Keine" => License::OtherOpen,
        "Nutzungsbedingungen: Es gelten keine Bedingungen" => License::OtherOpen,
        "Nutzungseinschränkungen: Es gelten keine Bedingungen" => License::OtherOpen,
        "CC BY-NC-ND 4.0: Die Nutzung des Wortguts steht unter einer Creative Commons Namensnennung – Nicht kommerziell – keine Bearbeitungen 4.0 International Lizenz." => License::CcByNcNd40,
        _ => {
            // In this case as the free-text nature of the field parsed here the non-matching licenses are "Unknown" instead of "Other"
            tracing::trace!("Failed to parse license (CSW): '{}'", text);

            License::Unknown
        }
    }
}

#[derive(Debug, Deserialize)]
struct Contact<'a> {
    #[serde(rename = "CI_ResponsibleParty", borrow)]
    responsible_party: Option<ResponsibleParty<'a>>,
}

#[derive(Debug, Deserialize)]
struct ResponsibleParty<'a> {
    #[serde(rename = "individualName")]
    individual_name: Option<IndividualName>,
    #[serde(rename = "organisationName")]
    organisation_name: Option<OrganisationName>,
    #[serde(rename = "contactInfo", borrow)]
    contact_info: Option<ContactInfo<'a>>,
}

#[derive(Debug, Deserialize)]
struct IndividualName {
    #[serde(rename = "CharacterString")]
    text: Option<String>,
}

#[derive(Debug, Deserialize)]
struct OrganisationName {
    #[serde(rename = "CharacterString")]
    text: Option<String>,
}

#[derive(Debug, Deserialize)]
struct ContactInfo<'a> {
    #[serde(rename = "CI_Contact", borrow)]
    inner: ContactInfoInner<'a>,
}

#[derive(Debug, Deserialize)]
struct ContactInfoInner<'a> {
    address: Option<Address>,
    #[serde(rename = "onlineResource", borrow)]
    online: Option<OnlineResource<'a>>,
}

#[derive(Debug, Deserialize)]
struct Address {
    #[serde(rename = "CI_Address")]
    inner: AddressInner,
}

#[derive(Debug, Deserialize)]
struct AddressInner {
    #[serde(rename = "electronicMailAddress", default)]
    mail_addresses: Vec<MailAddress>,
}

#[derive(Debug, Deserialize)]
struct MailAddress {
    #[serde(rename = "CharacterString")]
    text: Option<String>,
}

#[derive(Debug, Deserialize)]
struct DistributionInfo<'a> {
    #[serde(rename = "MD_Distribution", borrow)]
    data: Distribution<'a>,
}

#[derive(Debug, Deserialize)]
struct Distribution<'a> {
    #[serde(rename = "transferOptions", borrow, default)]
    transfer: Vec<TransferOptions<'a>>,
}

#[derive(Debug, Deserialize)]
struct TransferOptions<'a> {
    #[serde(rename = "MD_DigitalTransferOptions", borrow)]
    digital: Option<DigitalTransferOptions<'a>>,
}

#[derive(Debug, Deserialize)]
struct DigitalTransferOptions<'a> {
    #[serde(rename = "onLine", borrow, default)]
    online: Vec<OnlineTransferOptions<'a>>,
}

#[derive(Debug, Deserialize)]
struct OnlineTransferOptions<'a> {
    #[serde(rename = "CI_OnlineResource", borrow, default)]
    resource: Vec<OnlineResourceInner<'a>>,
}

#[derive(Debug, Deserialize)]
struct OnlineResource<'a> {
    #[serde(rename = "CI_OnlineResource", borrow)]
    inner: OnlineResourceInner<'a>,
}

#[derive(Debug, Deserialize)]
struct OnlineResourceInner<'a> {
    #[serde(rename = "linkage")]
    linkage: Linkage,
    #[serde(rename = "applicationProfile", borrow)]
    profile: Option<ApplicationProfile<'a>>,
}

#[derive(Debug, Deserialize)]
struct Linkage {
    #[serde(rename = "URL")]
    url: Option<String>,
}

#[derive(Debug, Deserialize)]
struct ApplicationProfile<'a> {
    #[serde(rename = "CharacterString", borrow)]
    text: Option<&'a str>,
}

#[derive(Debug, Deserialize)]
struct Citation<'a> {
    #[serde(rename = "CI_Citation", borrow)]
    inner: CitationInner<'a>,
}

#[derive(Debug, Deserialize)]
struct CitationInner<'a> {
    #[serde(borrow)]
    title: Title<'a>,
    #[serde(default, borrow)]
    identifier: Vec<Identifier<'a>>,
}

#[derive(Debug, Deserialize)]
struct Title<'a> {
    #[serde(rename = "CharacterString", borrow)]
    text: &'a str,
}

#[derive(Debug, Deserialize)]
struct Identifier<'a> {
    #[serde(rename = "MD_Identifier", borrow)]
    inner: Option<IdentifierInner<'a>>,
}

#[derive(Debug, Deserialize, Clone)]
struct IdentifierInner<'a> {
    #[serde(rename = "code", borrow)]
    code: Code<'a>,
}

#[derive(Debug, Deserialize, Clone)]
struct Code<'a> {
    #[serde(rename = "CharacterString")]
    text: Option<&'a str>,
}

#[derive(Debug, Deserialize)]
struct Abstract<'a> {
    #[serde(rename = "CharacterString", borrow)]
    text: Option<&'a str>,
}

#[derive(Debug, Deserialize)]
struct ResourceConstraints<'a> {
    #[serde(rename = "MD_LegalConstraints", borrow)]
    legal_constraints: Option<LegalConstraints<'a>>,
}

#[derive(Debug, Deserialize)]
struct LegalConstraints<'a> {
    #[serde(rename = "useConstraints", default, borrow)]
    use_constraints: Vec<UseConstraints<'a>>,
    #[serde(rename = "otherConstraints", default, borrow)]
    other_constraints: Vec<OtherConstraints<'a>>,
}

#[derive(Debug, Deserialize)]
struct UseConstraints<'a> {
    #[serde(rename = "MD_RestrictionCode", borrow)]
    restriction_code: RestrictionCode<'a>,
}

#[derive(Debug, Deserialize)]
struct RestrictionCode<'a> {
    #[serde(rename = "codeListValue", borrow)]
    value: &'a str,
}

#[derive(Debug, Deserialize)]
struct OtherConstraints<'a> {
    #[serde(rename = "CharacterString", borrow)]
    text: Option<&'a str>,
    #[serde(rename = "Anchor", borrow)]
    anchor: Option<Anchor<'a>>,
}

#[derive(Debug, Deserialize)]
struct Anchor<'a> {
    #[serde(borrow)]
    href: Option<&'a str>,
}

#[derive(Debug, Deserialize)]
struct CswLicense<'a> {
    #[serde(borrow)]
    id: &'a str,
}

#[derive(Debug, Deserialize)]
enum DateStamp<'a> {
    #[serde(borrow)]
    Date(&'a str),
    #[serde(borrow)]
    DateTime(&'a str),
}

impl DateStamp<'_> {
    fn parse(self) -> Result<Option<Date>, ParseError> {
        match self {
            Self::Date(val) if !val.is_empty() => parse_date(val).map(Some),
            Self::DateTime(val) if !val.is_empty() => parse_date(val).map(Some),
            _ => Ok(None),
        }
    }
}

#[derive(Debug, Deserialize)]
struct Language<'a> {
    #[serde(borrow, rename = "LanguageCode")]
    code: Option<LanguageCode<'a>>,
    #[serde(borrow, rename = "CharacterString")]
    string: Option<&'a str>,
}

impl<'a> Language<'a> {
    fn code(&self) -> Option<&'a str> {
        self.code
            .as_ref()
            .and_then(|code| {
                if !code.text.is_empty() {
                    Some(code.text)
                } else if let Some(value) = code.value {
                    Some(value)
                } else {
                    None
                }
            })
            .or(self.string)
    }
}

#[derive(Debug, Deserialize)]
struct LanguageCode<'a> {
    #[serde(borrow, rename = "$text")]
    text: &'a str,
    #[serde(borrow, rename = "codeListValue")]
    value: Option<&'a str>,
}

fn parse_date(val: &str) -> Result<Date, ParseError> {
    Date::parse(val, &format_description!("[year][first [-][.]][month][first [-][.]][day]"))
        .or_else(|_err| {
            Date::parse(
                val,
                &format_description!(
                    version = 2,
                    "[year][first [-][.]][month][first [-][.]][day]T[hour]:[minute]:[second][optional [.[subsecond]]]"
                ),
            )
        })
        .or_else(|_err| Date::parse(val, &Rfc3339))
        .or_else(|_err| Date::parse(val, &format_description!("[day].[month].[year]")))
        .or_else(|_err| parse_year_only(val))
}
