use std::fmt::Write;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use miniproj::get_projection;
use miniproj::Projection;
use serde::Deserialize;
use serde_roxmltree::from_str;
use smallvec::{smallvec, SmallVec};
use time::Date;

use harvester::{
    client::Client, fetch_many, utilities::point_like_bounding_box, write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, ReportingObligation, Station, Type},
        Dataset, Language, License, Region,
    },
    proj_rect,
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25832).unwrap();

    let source_url = source
        .source_url
        .as_deref()
        .expect("No source URL configured");

    let (mut count, mut results, mut errors) =
        groundwater::harvest(dir, client, source, proj, source_url).await?;

    let (count1, results1, errors1) =
        surfacewater::harvest(dir, client, source, proj, source_url).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        levels::harvest(dir, client, source, proj, source_url).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

mod groundwater {
    use super::*;

    pub async fn harvest(
        dir: &Dir,
        client: &Client,
        source: &Source,
        proj: &dyn Projection,
        source_url: &str,
    ) -> Result<(usize, usize, usize)> {
        let source_url = &source_url.replace("{{layer}}", "Messstellen_Grundwasser");

        let response = {
            let mut url = source.url.clone();
            url.query_pairs_mut()
                .append_pair("REQUEST", "GetFeature")
                .append_pair("SERVICE", "WFS")
                .append_pair("TYPENAME", "Messstellen_Grundwasser");

            let key = "wfs_grundwasser".to_owned();
            let text = client.fetch_text(source, key, &url).await?;
            from_str::<Collection>(&text)?
        };
        let count = response.members.len();

        let (results, errors) = fetch_many(0, 0, response.members, |member| async move {
            let id = member.station.id;
            let title = format!("Messstelle Grundwasser {}", member.station.label);
            let construction_year = member.station.construction_year;
            let issued = if construction_year != 0 {
                Some(Date::from_ordinal_date(construction_year, 1)?)
            } else {
                None
            };
            let gauge_type = member.station.gauge_type;
            let ground_level = member.station.ground_level;
            let depth = member.station.depth;

            let type_a = member.station.type_a;
            let type_h = member.station.type_h;
            let type_j = member.station.type_j;
            let type_l = member.station.type_l;
            let type_n = member.station.type_n;

            let all_types = type_a
                .into_iter()
                .chain(type_h)
                .chain(type_j)
                .chain(type_l)
                .chain(type_n)
                .collect::<Vec<_>>();

            // description is taken partly from MetaVer https://www.metaver.de/trefferanzeige?docuuid=be731f81-5102-468f-ac2b-ca719c7c57b9
            let mut description = format!("Für die Überwachung des Grundwassers wurde ein Netz von Messstellen für die qualitative und quantitative Analyse aufgebaut.\nDer Datensatz zeigt die Standorte der Grundwassermessstellen im Saarland. Die Messstellennummer lautet {}. Bei der Messstelle handelt es sich um eine {}.", id, gauge_type);
            if ground_level != 99999.0 {
                write!(
                    &mut description,
                    " Die Geländehöhe der Messstelle beträgt {} m.",
                    ground_level
                )?;
            }
            if gauge_type == "Bohrung" {
                write!(
                    &mut description,
                    " Die Tiefe der Messstelle beträgt {} m.",
                    depth
                )?;
            }

            let mut coordinates = member.station.location.point.coordinates.split_whitespace();

            let rechtswert = coordinates
                .next()
                .ok_or_else(|| anyhow!("Missing Rechtswert"))?
                .parse::<f64>()?;
            let hochwert = coordinates
                .next()
                .ok_or_else(|| anyhow!("Missing Hochwert"))?
                .parse::<f64>()?;

            let bounding_boxes = smallvec![proj_rect(
                point_like_bounding_box(hochwert, rechtswert),
                proj
            )];

            let any_nitrat = all_types.iter().any(|r#type| {
                r#type.starts_with("Messstelle Nitrat AVV") || r#type == "EUA-Nitrat-Messstelle"
            });

            let any_wasserstand = all_types
                .iter()
                .any(|r#type| r#type == "Messstelle Wasserstand");

            let any_hydrochemie = all_types
                .iter()
                .any(|r#type| r#type == "Hydrochemie- Messstelle" /* (sic) */);

            let measured_variables = if any_wasserstand && any_nitrat {
                smallvec!["Grundwasserstand".to_owned(), "Nitrat".to_owned()]
            } else if any_wasserstand {
                smallvec!["Grundwasserstand".to_owned()]
            } else if any_nitrat {
                smallvec!["Nitrat".to_owned()]
            } else {
                Default::default()
            };

            let mut types = SmallVec::new();

            if any_nitrat || any_hydrochemie {
                types.push(Type::Measurements {
                    domain: Domain::Chemistry,
                    station: Some(Station {
                        id: Some(id.clone()),
                        measurement_frequency: None,
                        reporting_obligations: smallvec![ReportingObligation::Wrrl],
                    }),
                    measured_variables: measured_variables.clone(),
                    methods: Default::default(),
                });
            }

            types.push(Type::Measurements {
                domain: Domain::Groundwater,
                station: Some(Station {
                    id: Some(id.clone()),
                    measurement_frequency: None,
                    reporting_obligations: smallvec![ReportingObligation::Wrrl],
                }),
                measured_variables,
                methods: Default::default(),
            });

            let dataset = Dataset {
                title,
                description: Some(description),
                types,
                issued,
                regions: smallvec![Region::SL],
                bounding_boxes,
                language: Language::German,
                license: License::OtherClosed,
                origins: source.origins.clone(),
                source_url: source_url.clone(),
                machine_readable_source: true,
                ..Default::default()
            };

            write_dataset(dir, client, source, id.into(), dataset).await
        })
        .await;

        Ok((count, results, errors))
    }

    #[derive(Debug, Deserialize)]
    struct Collection {
        #[serde(rename = "member")]
        members: Vec<Member>,
    }

    #[derive(Debug, Deserialize)]
    struct Member {
        #[serde(rename = "Messstellen_Grundwasser")]
        station: GroundwaterStation,
    }

    #[derive(Debug, Deserialize)]
    struct GroundwaterStation {
        #[serde(rename = "MSTNR")]
        id: CompactString,
        #[serde(rename = "OBJBEZ")]
        label: String,
        #[serde(rename = "BAUJAHR")]
        construction_year: i32,
        #[serde(rename = "MSTARTL")]
        gauge_type: String,
        #[serde(rename = "MSTGELH")]
        ground_level: f64,
        #[serde(rename = "MSTTIEF")]
        depth: f64,
        #[serde(rename = "Shape")]
        location: Location,
        #[serde(rename = "A")]
        type_a: Option<String>,
        #[serde(rename = "H")]
        type_h: Option<String>,
        #[serde(rename = "J")]
        type_j: Option<String>,
        #[serde(rename = "L")]
        type_l: Option<String>,
        #[serde(rename = "N")]
        type_n: Option<String>,
    }

    #[derive(Debug, Deserialize)]
    struct Location {
        #[serde(rename = "Point")]
        point: Coordinates,
    }

    #[derive(Debug, Deserialize)]
    struct Coordinates {
        #[serde(rename = "pos")]
        coordinates: String,
    }
}

mod surfacewater {
    use super::*;

    pub async fn harvest(
        dir: &Dir,
        client: &Client,
        source: &Source,
        proj: &dyn Projection,
        source_url: &str,
    ) -> Result<(usize, usize, usize)> {
        let source_url = &source_url.replace("{{layer}}", "Messstellen_Oberflaechenwasser");

        let response = {
            let mut url = source.url.clone();
            url.query_pairs_mut()
                .append_pair("REQUEST", "GetFeature")
                .append_pair("SERVICE", "WFS")
                .append_pair("TYPENAME", "Messstellen_Oberflaechenwasser");

            let key = "wfs_oberflaechenwasser".to_owned();
            let text = client.fetch_text(source, key, &url).await?;
            from_str::<Collection>(&text)?
        };
        let count = response.members.len();

        let (results, errors) = fetch_many(0, 0, response.members, |member| async move {
            let id = member.station.id;
            let title = format!("Messstelle Oberflächenwasser {}", member.station.label);
            let operative = member.station.operative;
            let overview = member.station.overview;
            let validation = member.station.validation;
            let description = format!("Der Datensatz zeigt die Standorte der Messstellen im Saarland.\nDie Messstellennummer lautet {}. Es handelt sich um a) eine operative Messstelle: {}, b) eine Überblicksmessstelle: {}, c) eine Validierungsmessstelle: {}", id, operative, overview, validation);

            let mut coordinates = member.station.location.point.coordinates.split_whitespace();

            let rechtswert = coordinates
                .next()
                .ok_or_else(|| anyhow!("Missing Rechtswert"))?
                .parse::<f64>()?;
            let hochwert = coordinates
                .next()
                .ok_or_else(|| anyhow!("Missing Hochwert"))?
                .parse::<f64>()?;

            let (longitude, latitude) = proj.projected_to_deg(rechtswert, hochwert);
            let bounding_boxes = smallvec![point_like_bounding_box(latitude, longitude)];

            let reporting_obligations = member.station.wrrl.map(|_| ReportingObligation::Wrrl).into_iter().collect();

            let types = smallvec![Type::Measurements {
                domain: Domain::Surfacewater,
                station: Some(Station {
                    id: Some(id.clone()),
                    measurement_frequency: None,
                    reporting_obligations,
                }),
                measured_variables: Default::default(),
                methods: Default::default(),
            }];

            let mut regions = smallvec![Region::SL];
            regions.extend(WISE.match_shape(longitude, latitude).map(Region::Watershed));

            let dataset = Dataset {
                title,
                description: Some(description),
                types,
                regions,
                bounding_boxes,
                language: Language::German,
                license: License::OtherClosed,
                origins: source.origins.clone(),
                source_url: source_url.clone(),
                machine_readable_source: true,
                ..Default::default()
            };

            write_dataset(dir, client, source, id.into(), dataset).await
        })
        .await;

        Ok((count, results, errors))
    }

    #[derive(Debug, Deserialize)]
    struct Collection {
        #[serde(rename = "member")]
        members: Vec<Member>,
    }

    #[derive(Debug, Deserialize)]
    struct Member {
        #[serde(rename = "Messstellen_Oberflaechenwasser")]
        station: SurfaceStation,
    }

    #[derive(Debug, Deserialize)]
    struct SurfaceStation {
        #[serde(rename = "OBJBEZ")]
        label: String,
        #[serde(rename = "MSTNR")]
        id: CompactString,
        #[serde(rename = "OPERATIV")]
        operative: String,
        #[serde(rename = "UEBERBLICK")]
        overview: String,
        #[serde(rename = "VALIDIERUNG")]
        validation: String,
        #[serde(rename = "WRRL")]
        wrrl: Option<String>,
        #[serde(rename = "Shape")]
        location: Location,
    }

    #[derive(Debug, Deserialize)]
    struct Location {
        #[serde(rename = "Point")]
        point: Coordinates,
    }

    #[derive(Debug, Deserialize)]
    struct Coordinates {
        #[serde(rename = "pos")]
        coordinates: String,
    }
}

mod levels {
    use super::*;

    pub async fn harvest(
        dir: &Dir,
        client: &Client,
        source: &Source,
        proj: &dyn Projection,
        source_url: &str,
    ) -> Result<(usize, usize, usize)> {
        let source_url = &source_url.replace("{{layer}}", "Messstellen_Pegel");

        let response = {
            let mut url = source.url.clone();
            url.query_pairs_mut()
                .append_pair("REQUEST", "GetFeature")
                .append_pair("SERVICE", "WFS")
                .append_pair("TYPENAME", "Messstellen_Pegel");

            let key = "wfs_pegel".to_owned();
            let text = client.fetch_text(source, key, &url).await?;
            from_str::<Collection>(&text)?
        };
        let count = response.members.len();

        let (results, errors) = fetch_many(0, 0, response.members, |member| async move {
            let id = member.station.id;
            let title = format!("Messstelle Pegel {}", member.station.remark);
            let distance_and_side_to_outlet = member.station.distance_and_side_to_outlet;
            let gauge_zero_point = member.station.gauge_zero_point;

            // description is taken partly from MetaVer https://metaver.de/trefferanzeige?docuuid=be36cc36-1ecb-4ca3-9f53-5cb1e51691fb
            let description = format!("An den Pegeln wird kontinuierlich der Wasserstand und zum Teil auch der Abfluss an ausgewählten Abschnitten von Oberflächengewässern gemessen.\nDer Datensatz zeigt die Standorte der Messstellen im Saarland.\nDie Messstellennummer lautet {}. Die Entfernung und Seite oberhalb der Mündung ist {}. Der Pegelnullpunkt beträgt {} m.", id, distance_and_side_to_outlet, gauge_zero_point);

            let mut coordinates = member.station.location.point.coordinates.split_whitespace();

            let rechtswert = coordinates
                .next()
                .ok_or_else(|| anyhow!("Missing Rechtswert"))?
                .parse::<f64>()?;
            let hochwert = coordinates
                .next()
                .ok_or_else(|| anyhow!("Missing Hochwert"))?
                .parse::<f64>()?;

            let (longitude, latitude) = proj.projected_to_deg(rechtswert, hochwert);
            let bounding_boxes = smallvec![point_like_bounding_box(latitude, longitude)];

            let types = smallvec![Type::Measurements {
                domain: Domain::Rivers,
                station: Some(Station {
                    id: Some(id.clone()),
                    ..Default::default()
                }),
                measured_variables: Default::default(),
                methods: Default::default(),
            }];

            let mut regions =  smallvec![Region::SL];
            regions.extend(WISE.match_shape(longitude, latitude).map(Region::Watershed));

            let dataset = Dataset {
                title,
                description: Some(description),
                types,
                regions,
                bounding_boxes,
                language: Language::German,
                license: License::OtherClosed,
                origins: source.origins.clone(),
                source_url: source_url.clone(),
                machine_readable_source: true,
                ..Default::default()
            };

            write_dataset(dir, client, source, id.into(), dataset).await
        })
        .await;

        Ok((count, results, errors))
    }

    #[derive(Debug, Deserialize)]
    struct Collection {
        #[serde(rename = "member")]
        members: Vec<Member>,
    }

    #[derive(Debug, Deserialize)]
    struct Member {
        #[serde(rename = "Messstellen_Pegel")]
        station: MeasurementStation,
    }

    #[derive(Debug, Deserialize)]
    struct MeasurementStation {
        #[serde(rename = "PGLG2")]
        distance_and_side_to_outlet: String,
        #[serde(rename = "PGNP")]
        gauge_zero_point: f64,
        #[serde(rename = "MSTNR")]
        id: CompactString,
        #[serde(rename = "MSTBEM")]
        remark: String,
        #[serde(rename = "SHAPE")]
        location: Location,
    }

    #[derive(Debug, Deserialize)]
    struct Location {
        #[serde(rename = "Point")]
        point: Coordinates,
    }

    #[derive(Debug, Deserialize)]
    struct Coordinates {
        #[serde(rename = "pos")]
        coordinates: String,
    }
}
