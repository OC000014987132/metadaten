use std::borrow::Cow;

use anyhow::{anyhow, Context, Result};
use cap_std::fs::Dir;
use futures_util::stream::{FuturesUnordered, StreamExt};
use serde::Deserialize;
use serde_roxmltree::{from_doc as from_xml_doc, roxmltree::Document};
use smallvec::SmallVec;
use time::{format_description::well_known::Rfc3339, macros::format_description, Date};
use tokio::{
    select,
    sync::mpsc::{unbounded_channel, UnboundedSender},
};

use harvester::{client::Client, fetch_many, utilities::make_key, write_dataset, Source};
use metadaten::dataset::{
    Dataset, GlobalIdentifier, Organisation, OrganisationRole, Person, PersonRole, Resource,
    ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let (token_sender, mut token_receiver) = unbounded_channel();

    let mut scheduler = FuturesUnordered::new();

    scheduler.push(fetch_list(dir, client, source, &token_sender, None));

    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    loop {
        select! {
            biased;

            Some(token) = token_receiver.recv(), if scheduler.len() < 8 => {
                scheduler.push(fetch_list(dir, client, source, &token_sender, Some(token)));
            }

            response = scheduler.next() => match response {
                Some(Ok((count1, results1, errors1))) => {
                    count = count.max(count1);
                    results += results1;
                    errors += errors1;
                }
                Some(Err(err)) => {
                    tracing::error!("{:#}", err);

                    errors += 1;
                }
                None => break,
            }
        }
    }

    Ok((count, results, errors))
}

async fn fetch_list(
    dir: &Dir,
    client: &Client,
    source: &Source,
    token_sender: &UnboundedSender<String>,
    token: Option<String>,
) -> Result<(usize, usize, usize)> {
    let key;

    let mut url = source.url.clone();

    if let Some(token) = &token {
        key = make_key(token).into_owned();

        url.query_pairs_mut()
            .append_pair("verb", "ListRecords")
            .append_pair("resumptionToken", token);
    } else {
        key = "initial".to_owned();

        url.query_pairs_mut()
            .append_pair("verb", "ListRecords")
            .append_pair("metadataPrefix", "oai_dc");
    }

    let body = client.fetch_text(source, key, &url).await?;

    let document = Document::parse(&body).context("Failed parsing document")?;

    let response = from_xml_doc::<Response>(&document).context("Failed parsing XML")?;

    let count = response
        .records
        .token
        .as_ref()
        .and_then(|token| token.count)
        .unwrap_or_default();

    match response.records.token {
        Some(token) if !token.value.is_empty() => {
            token_sender.send(token.value).unwrap();
        }
        _ => (),
    }

    let (results, errors) = fetch_many(0, 0, response.records.inner, |record| {
        translate_dataset(dir, client, source, record)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    record: Record<'_>,
) -> Result<(usize, usize, usize)> {
    if record.header.status == Some("deleted") {
        return Ok((1, 0, 0));
    }

    let identifier = make_key(record.header.identifier).into_owned();

    let metadata = record
        .metadata
        .ok_or_else(|| anyhow!("Missing metadata for record {}", identifier))?
        .inner;

    let title = metadata
        .title
        .into_iter()
        .next()
        .ok_or_else(|| anyhow!("Missing title"))?;

    let description = metadata.description.into_iter().next();

    let source_url = metadata
        .identifiers
        .iter()
        .find(|identifier| identifier.contains("frontdoor") || identifier.contains("handle"))
        .map_or_else(|| source.source_url(), |&url| url)
        .to_owned();

    let global_identifier = metadata
        .identifiers
        .iter()
        .find(|identifier| identifier.contains("nbn-resolving.org"))
        .and_then(|identifier| GlobalIdentifier::extract(identifier))
        .or_else(|| {
            metadata
                .identifiers
                .iter()
                .find(|identifier| identifier.contains("doi.org"))
                .and_then(|identifier| GlobalIdentifier::extract(identifier))
        });

    let mut resources = SmallVec::new();

    if let Some(&url) = metadata
        .identifiers
        .iter()
        .find(|identifier| identifier.contains("files"))
    {
        resources.push(
            Resource {
                r#type: ResourceType::Pdf,
                url: url.to_owned(),
                primary_content: true,
                ..Default::default()
            }
            .guess_or_keep_type(),
        );
    }

    let issued = metadata
        .date
        .iter()
        .filter_map(|date| {
            Date::parse(date, &Rfc3339)
                .or_else(|_err| {
                    Date::parse(date, &format_description!("[year]-[month]-[day]")).or_else(
                        |_err| -> Result<_> {
                            let date = date.parse()?;
                            let date = Date::from_ordinal_date(date, 1)?;
                            Ok(date)
                        },
                    )
                })
                .ok()
        })
        .next_back();

    let language = metadata
        .language
        .into_iter()
        .next()
        .map(Into::into)
        .unwrap_or_default();

    let license = metadata
        .licenses
        .into_iter()
        .next()
        .map(|license| license.into())
        .unwrap_or_default();

    let organisations = metadata
        .publishers
        .into_iter()
        .map(|name| Organisation::Other {
            name,
            role: OrganisationRole::Publisher,
            websites: Default::default(),
        })
        .collect();

    let persons = metadata
        .creators
        .into_iter()
        .map(|name| Person {
            name,
            role: PersonRole::Creator,
            ..Default::default()
        })
        .chain(metadata.contributors.into_iter().map(|name| Person {
            name,
            role: PersonRole::Contributor,
            ..Default::default()
        }))
        .collect();

    let tags = metadata
        .subjects
        .into_iter()
        .map(|subject| Tag::Other(subject.as_ref().into()))
        .collect();

    let dataset = Dataset {
        title,
        description,
        origins: source.origins.clone(),
        machine_readable_source: true,
        language,
        license,
        organisations,
        persons,
        source_url,
        issued,
        resources,
        global_identifier,
        tags,
        ..Default::default()
    };

    write_dataset(dir, client, source, identifier, dataset).await
}

#[derive(Debug, Deserialize)]
struct Response<'a> {
    #[serde(rename = "ListRecords", borrow)]
    records: ListRecords<'a>,
}

#[derive(Debug, Deserialize)]
struct ListRecords<'a> {
    #[serde(rename = "record", borrow)]
    inner: Vec<Record<'a>>,
    #[serde(rename = "resumptionToken")]
    token: Option<Token>,
}

#[derive(Debug, Deserialize)]
struct Token {
    #[serde(rename = "completeListSize")]
    count: Option<usize>,
    #[serde(rename = "$text")]
    value: String,
}

#[derive(Debug, Deserialize)]
struct Record<'a> {
    #[serde(borrow)]
    header: Header<'a>,
    #[serde(borrow)]
    metadata: Option<Metadata<'a>>,
}

#[derive(Debug, Deserialize)]
struct Header<'a> {
    identifier: &'a str,
    status: Option<&'a str>,
}

#[derive(Debug, Deserialize)]
struct Metadata<'a> {
    #[serde(rename = "dc", borrow)]
    inner: DublinCore<'a>,
}

#[derive(Debug, Deserialize)]
struct DublinCore<'a> {
    #[serde(default)]
    title: SmallVec<[String; 1]>,
    #[serde(default)]
    description: SmallVec<[String; 1]>,
    #[serde(rename = "creator", default)]
    creators: Vec<String>,
    #[serde(rename = "contributor", default)]
    contributors: Vec<String>,
    #[serde(rename = "publisher", default)]
    publishers: Vec<String>,
    #[serde(default)]
    language: SmallVec<[&'a str; 1]>,
    #[serde(rename = "identifier", default, borrow)]
    identifiers: Vec<&'a str>,
    #[serde(rename = "subject", default, borrow)]
    subjects: Vec<Cow<'a, str>>,
    #[serde(rename = "rights", default, borrow)]
    licenses: Vec<&'a str>,
    #[serde(default, borrow)]
    date: SmallVec<[&'a str; 1]>,
}
