use std::borrow::Cow;

use anyhow::Result;
use cap_std::fs::Dir;
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::smallvec;
use time::{format_description::well_known::Iso8601, Date};

use harvester::{
    client::Client,
    fetch_many,
    utilities::{contains_or, point_like_bounding_box, remove_pom_suffix},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Region, Resource, ResourceType,
    },
    parse_str,
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let (mut count, mut results, mut errors) = fetch_stations(dir, client, source).await?;

    let (count1, results1, errors1) = fetch_reports(dir, client, source).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("fileadmin/Bibliothek/Politik_und_Verwaltung/MLU/HVZ/KISTERS/data/internet/layers/2000/index.json")?;

    let bytes = client
        .fetch_bytes(source, "stations".to_owned(), &url)
        .await?;

    let stations = from_slice::<Vec<PegelStation>>(&bytes)?;

    let count = stations.len();

    let (results, errors) = fetch_many(0, 0, stations, |station| async move {
        let key = format!("messstation-{}", station.metadata_station_no);

        let title = format!(
            "Hochwasservorhersagezentrale Sachsen-Anhalt: Messstation {} ({})",
            station.metadata_station_name, station.metadata_river_name,
        );

        let modified = Date::parse(station.timestamp, &Iso8601::DEFAULT)?;

        let description = format!(
            "{} Zuständigkeit: {} Wasserstand am {}: {}{}",
            station.metadata_catchment_name,
            station.metadata_body_responsible,
            modified,
            station.value,
            station.unit,
        );

        let bounding_boxes = smallvec![point_like_bounding_box(station.latitude, station.longitude)];

        let mut regions = smallvec![contains_or(remove_pom_suffix(&station.metadata_station_name), Region::ST)];
        regions.extend(WISE.match_shape(station.longitude, station.latitude).map(Region::Watershed));

        let mut resources = smallvec![Resource {
            r#type: ResourceType::WebPage,
            description: Some("Gewässerkundliche Hauptwerte".to_owned()),
            url: source
                .url
                .join(&format!(
                    "static-wiski-web-public/config/gewkundhw/web/index.html#{}",
                    station.metadata_station_no
                ))?
                .into(),
            primary_content: true,
            ..Default::default()
        }];

        for period in ["Woche", "Monat", "Jahr"] {
            for metric in ["W/Wasserstand", "Q/Durchfluss"] {
                resources.push(Resource {
                    r#type: ResourceType::MicrosoftExcelSpreadsheet,
                    description: Some(format!("{metric}, {period}")),
                    url: source.url.join(&format!("fileadmin/Bibliothek/Politik_und_Verwaltung/MLU/HVZ/KISTERS/data/internet/stations/{}/{}/{metric}_{period}.xlsx", station.metadata_site_name, station.metadata_station_no))?.into(),
                    primary_content: true,
                    ..Default::default()
                });
            }
        }

        let types = smallvec![
            Type::Measurements {
                domain: Domain::Rivers,
                station: Some(Station {
                    id: Some(station.metadata_catchment_name.into()),
                    ..Default::default()
                }),
                measured_variables: smallvec!["Pegel".to_owned()],
                methods: Default::default(),
            },
        ];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            regions,
            bounding_boxes,
            modified: Some(modified),
            resources,
            language: Language::German,
            license: License::AllRightsReserved,
            source_url: format!("{}/messwerte/wasserstand", source.source_url()),
            origins: source.origins.clone(),
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_reports(
    dir: &Dir,
    client: &Client,
    source: &Source,
) -> Result<(usize, usize, usize)> {
    // TODO: test new kinds of reports (the last four) when they appear (there were none at the time of development)
    const REPORTS: &[&str] = &[
        "Vorhersagen",
        "Tagesbericht",
        "Wochenbericht",
        "Monatsbericht",
        "Hochwasserberichte",
        "Hochwasserwarnungen",
        "Hochwasserinformationen",
        "Lageberichte",
        "Pressemitteilungen",
    ];

    let count = REPORTS.len();

    let base_url = &source
        .url
        .join("fileadmin/Bibliothek/Politik_und_Verwaltung/MLU/HVZ/KISTERS/data/documents/")?;

    let (results, errors) = fetch_many(0, 0, REPORTS, |&report_name| async move {
        let url = base_url.join(&format!("{report_name}/index.json"))?;

        let bytes = client
            .fetch_bytes(source, report_name.to_owned(), &url)
            .await?;

        let reports = from_slice::<Vec<Report>>(&bytes)?;

        let title;
        let report_dir;

        match report_name {
            "Tagesbericht" | "Wochenbericht" | "Monatsbericht" => {
                title = format!(
                    "Hochwasservorhersagezentrale Sachsen-Anhalt: Hydrologischer {report_name}"
                );
                report_dir = format!("hydrologische-berichte/{}", report_name.to_lowercase());
            }
            "Hochwasserberichte" => {
                title = format!(
                    "Hochwasservorhersagezentrale Sachsen-Anhalt: Hydrologische {report_name}"
                );
                report_dir = format!("hydrologische-berichte/{}", report_name.to_lowercase());
            }
            "Vorhersagen"
            | "Hochwasserwarnungen"
            | "Hochwasserinformationen"
            | "Lageberichte"
            | "Pressemitteilungen" => {
                title = format!("Hochwasservorhersagezentrale Sachsen-Anhalt: {report_name}");
                report_dir = format!(
                    "hochwasserwarnungen-informationen/{}",
                    report_name.to_lowercase()
                );
            }
            _ => unreachable!(),
        }

        let resources = reports
            .into_iter()
            .map(|report| {
                let url = base_url
                    .join(&format!("{report_name}/{}", report.filename))?
                    .into();

                Ok(Resource {
                    r#type: ResourceType::Document,
                    description: Some(report.filename),
                    url,
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<_>>()?;

        let dataset = Dataset {
            title,
            regions: smallvec![Region::ST],
            resources,
            language: Language::German,
            license: License::AllRightsReserved,
            source_url: format!("{}/{report_dir}", source.source_url()),
            origins: source.origins.clone(),
            ..Default::default()
        };

        write_dataset(dir, client, source, report_name.to_owned(), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Deserialize)]
struct PegelStation<'a> {
    metadata_station_no: &'a str,
    #[serde(borrow)]
    metadata_station_name: Cow<'a, str>,
    #[serde(borrow)]
    metadata_river_name: Cow<'a, str>,
    #[serde(borrow)]
    metadata_site_name: Cow<'a, str>,
    #[serde(borrow)]
    metadata_catchment_name: Cow<'a, str>,
    #[serde(rename = "metadata_BODY_RESPONSIBLE", borrow)]
    metadata_body_responsible: Cow<'a, str>,
    #[serde(rename = "L1_timestamp")]
    timestamp: &'a str,
    #[serde(rename = "L1_ts_value")]
    value: i32,
    #[serde(rename = "L1_ts_unitsymbol", borrow)]
    unit: Cow<'a, str>,
    #[serde(rename = "metadata_station_longitude", deserialize_with = "parse_str")]
    longitude: f64,
    #[serde(rename = "metadata_station_latitude", deserialize_with = "parse_str")]
    latitude: f64,
}

#[derive(Deserialize)]
struct Report {
    filename: String,
}
