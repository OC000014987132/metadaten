use std::borrow::Cow;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use serde::Deserialize;
use serde_json::from_str;
use smallvec::smallvec;
use time::{format_description::well_known::Rfc3339, Date};
use url::Url;

use harvester::{
    client::Client, fetch_many, utilities::point_like_bounding_box, write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{Domain, Station, Type},
    Dataset, Language, License, Region, Resource, ResourceType, Tag, TimeRange,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let source_url = &Url::parse(source.source_url())?;

    let text = client
        .make_request(
            source,
            "stations".to_owned(),
            Some(&source.url),
            |client| async {
                client
                    .post(source.url.clone())
                    .body(r#"
<GetFeature xmlns="http://www.opengis.net/wfs" service="WFS" version="1.1.0" outputFormat="application/json">
    <Query typeName="odlinfo_odl_1h_latest"/>
</GetFeature>
                    "#)
                    .send()
                    .await?
                    .error_for_status()?
                    .text()
                    .await
            },
        )
        .await?;

    let json = from_str::<Json>(&text)?;

    let count = json.features.len();

    let (results, errors) = fetch_many(0, 0, json.features, |feature| async move {
        let title = format!(
            "Ortsdosisleistung (ODL): {} {} ({})",
            feature.properties.plz, feature.properties.name, feature.properties.site_status_text
        );

        let region = Region::Other(feature.properties.name.into());

        let bounding_boxes = smallvec![point_like_bounding_box(
            feature.geometry.coordinates[1],
            feature.geometry.coordinates[0]
        )];

        let time_ranges = match fetch_time_series(client, source, feature.properties.kenn).await {
            Ok(time_range) => smallvec![time_range],
            Err(err) => {
                tracing::warn!(
                    "Failed to fetch time series for station {}: {:#}",
                    feature.properties.kenn,
                    err
                );

                Default::default()
            }
        };

        let resources = smallvec![
            Resource {
                r#type: ResourceType::Wfs,
                description: Some("Kartendienst mit Messstellen in Deutschland".to_owned()),
                url: {
                    let mut url = source.url.clone();
                    url.query_pairs_mut()
                        .append_pair("service", "WFS")
                        .append_pair("request", "GetCapabilities");
                    url.into()
                },
                primary_content: true,
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::WebPage,
                description: Some("Messstellen in Deutschland (Listenansicht)".to_owned()),
                url: source_url
                    .join("ODL/DE/themen/wo-stehen-die-sonden/liste/liste_node.html")?
                    .into(),
                direct_link: false,
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::WebPage,
                description: Some("Messstellen in Deutschland (Kartenansicht)".to_owned()),
                url: source_url
                    .join("ODL/DE/themen/wo-stehen-die-sonden/karte/karte_node.html")?
                    .into(),
                direct_link: false,
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::WebPage,
                description: Some("Download historischer ODL-Daten".to_owned()),
                url: source_url
                    .join("ODL/DE/service/downloadbereich/downloadbereich.html")?
                    .into(),
                direct_link: false,
                ..Default::default()
            },
        ];

        let types = smallvec![
            Type::Measurements {
                domain: Domain::Radioactivity,
                station: Some(Station {
                    id: Some(feature.properties.kenn.into()),
                    ..Default::default()
                }),
                measured_variables: smallvec!["Ortsdosisleistung".to_owned()],
                methods: Default::default(),
            },
            Type::Measurements {
                domain: Domain::Air,
                station: Some(Station {
                    id: Some(feature.properties.kenn.into()),
                    ..Default::default()
                }),
                measured_variables: smallvec!["Ortsdosisleistung".to_owned()],
                methods: Default::default(),
            }
        ];

        let tags = vec![Tag::ODL];

        let mut source_url = source_url
            .join("ODL/DE/themen/wo-stehen-die-sonden/karte/_documents/Messstelle.html")?;

        source_url
            .query_pairs_mut()
            .append_pair("id", feature.properties.kenn);

        let dataset = Dataset {
            title,
            types,
            regions: smallvec![region],
            bounding_boxes,
            time_ranges,
            resources,
            tags,
            language: Language::German,
            license: License::AllRightsReserved,
            origins: source.origins.clone(),
            source_url: source_url.into(),
            ..Default::default()
        };

        write_dataset(
            dir,
            client,
            source,
            feature.properties.kenn.to_owned(),
            dataset,
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_time_series(client: &Client, source: &Source, kenn: &str) -> Result<TimeRange> {
    let text = client
        .make_request(
            source,
            format!("time-series-{kenn}.isol"),
            Some(&source.url),
            |client| async {
                client
                    .post(source.url.clone())
                    .body(format!(r#"
<GetFeature xmlns="http://www.opengis.net/wfs" service="WFS" version="1.1.0" outputFormat="application/json" viewParams="kenn:{kenn}">
    <Query typeName="odlinfo_timeseries_odl_24h">
        <Filter xmlns="http://www.opengis.net/ogc">
            <PropertyIsEqualTo>
                <PropertyName>kenn</PropertyName>
                <Literal>{kenn}</Literal>
            </PropertyIsEqualTo>
        </Filter>
    </Query>
</GetFeature>
                    "#))
                    .send()
                    .await?
                    .error_for_status()?
                    .text()
                    .await
            },
        )
        .await?;

    let json = from_str::<TimeseriesJson>(&text)?;

    let start = json
        .features
        .iter()
        .map(|feature| feature.properties.start_measure)
        .try_fold(Date::MAX, |min_start, start| {
            Date::parse(start, &Rfc3339).map(|start| min_start.min(start))
        })?;

    let end = json
        .features
        .iter()
        .map(|feature| feature.properties.end_measure)
        .try_fold(Date::MIN, |max_end, end| {
            Date::parse(end, &Rfc3339).map(|end| max_end.max(end))
        })?;

    if start != Date::MAX && end != Date::MIN {
        Ok((start, end).into())
    } else {
        Err(anyhow!("Missing data"))
    }
}

#[derive(Deserialize)]
struct Json<'a> {
    #[serde(borrow)]
    features: Vec<Feature<'a>>,
}

#[derive(Deserialize)]
struct Feature<'a> {
    #[serde(borrow)]
    properties: Properties<'a>,
    geometry: Geometry,
}

#[derive(Deserialize)]
struct Properties<'a> {
    kenn: &'a str,
    #[serde(borrow)]
    name: Cow<'a, str>,
    plz: &'a str,
    site_status_text: &'a str,
}

#[derive(Deserialize)]
struct Geometry {
    coordinates: [f64; 2],
}

#[derive(Deserialize)]
struct TimeseriesJson<'a> {
    #[serde(borrow)]
    features: Vec<TimeseriesFeature<'a>>,
}

#[derive(Deserialize)]
struct TimeseriesFeature<'a> {
    #[serde(borrow)]
    properties: TimeseriesProperties<'a>,
}

#[derive(Deserialize)]
struct TimeseriesProperties<'a> {
    start_measure: &'a str,
    end_measure: &'a str,
}
