use anyhow::Result;
use cap_std::fs::Dir;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let main_pages = fetch_main_pages(client, source, selectors).await?;

    let count = main_pages.len();

    let (results, errors) = fetch_many(0, 0, main_pages, |page| {
        fetch_page_details(dir, client, source, selectors, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_page_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: String,
) -> Result<(usize, usize, usize)> {
    let dataset;
    let key;

    {
        let url = source.url.join(&page)?;
        key = make_key(&page).into_owned();

        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        let page_title = collect_text(
            document
                .select(&selectors.page_title)
                .flat_map(|element| element.text()),
        );

        let page_description = collect_text(
            document
                .select(&selectors.page_description)
                .flat_map(|element| element.text()),
        );

        let page_resources = document
            .select(&selectors.page_resources)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = collect_text(element.text());

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        dataset = Dataset {
            title: page_title,
            description: Some(page_description),
            types: smallvec![Type::Text {
                text_type: TextType::Manual,
            }],
            language: Language::German,
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            resources: page_resources,
            source_url: url.into(),
            mandatory_registration: true,
            ..Default::default()
        };
    }

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_main_pages(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let text = client
        .fetch_text(source, "online_tools".to_owned(), &source.url)
        .await?;

    let document = Html::parse_document(&text);

    let main_pages = document
        .select(&selectors.main_pages)
        .map(|elem| elem.attr("href").unwrap().to_owned())
        .collect();

    Ok(main_pages)
}

selectors! {
    main_pages: "div#online_methoden_box a.btn[href]",
    page_title: "div.mod_breadcrumb.block div.container",
    page_description: "div.col-lg-12",
    page_resources: "div.col-lg-12 a[href]",
}
