use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use csv::ReaderBuilder;
use encoding_rs::WINDOWS_1252;
use smallvec::smallvec;

use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::dataset::{
    r#type::{Domain, Type},
    Dataset, Language, License, Resource, ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let url = &source
        .url
        .join("/SiteGlobals/Functions/SAR-Werte/SarWerteCsv.csv?__blob=publicationFile")?;

    let text = client
        .fetch_bytes(source, "csv".to_owned(), url)
        .await
        .map(|bytes| WINDOWS_1252.decode(&bytes).0.into_owned())?;

    let (header, rows) = text
        .split_once('\n')
        .ok_or_else(|| anyhow!("Need at least a single header line"))?;

    let count = rows.lines().count();

    let reader = ReaderBuilder::new()
        .delimiter(b';')
        .has_headers(false)
        .from_reader(rows.as_bytes());

    let find_column = |name| {
        header
            .split(';')
            .enumerate()
            .find(|(_, header)| header.contains(name))
            .ok_or_else(|| anyhow!("Missing column `{name}`"))
    };

    let (nummer_pos, _) = find_column("Nr.")?;
    let (hersteller_pos, _) = find_column("Hersteller")?;
    let (typ_pos, _) = find_column("Typ")?;
    let (bemerkungen_pos, _) = find_column("Bemerkungen")?;

    let (am_ohr_pos, am_ohr_header) = find_column("am Ohr")?;
    let (am_körper_pos, am_körper_header) = find_column("am Körper")?;
    let (messabstand_pos, messabstand_header) = find_column("Messabstand")?;

    let (results, errors) = fetch_many(0, 0, reader.into_records(), |record| async move {
        let record = record?;

        let nummer = &record[nummer_pos];
        let hersteller = &record[hersteller_pos];
        let typ = &record[typ_pos];
        let bemerkungen = &record[bemerkungen_pos];

        let am_ohr = &record[am_ohr_pos];
        let am_körper = &record[am_körper_pos];
        let messabstand = &record[messabstand_pos];

        let title = format!("Spezifische Absorptionsraten {hersteller} {typ}");
        let description = format!("{am_ohr_header}: {am_ohr} - {am_körper_header}: {am_körper} - {messabstand_header}: {messabstand}");

        let source_url = source.url.join(&format!("/SiteGlobals/Forms/Suche/BfS/DE/SARsuche_Formular.html?sarQueryString={hersteller}+{typ}"))?.into();

        let types = smallvec![
            Type::Measurements {
                domain: Domain::NonionisingRadiation,
                station: None,
                measured_variables: smallvec!["Spezifische Absorptionsraten".to_owned()],
                methods: Default::default(),
            },
        ];

        let resources = smallvec![Resource {
            r#type: ResourceType::Csv,
            description: Some("Sammlung aller verfügbaren SAR-Werte".to_owned()),
            url: url.clone().into(),
            primary_content: true,
            ..Default::default()
        }];

        write_dataset(
            dir,
            client,
            source,
            nummer.to_owned(),
            Dataset {
                title,
                description: Some(description),
                comment: Some(bemerkungen.to_owned()),
                resources,
                license: License::AllRightsReserved,
                language: Language::German,
                tags: vec![Tag::Umthes(0x603741)],
                origins: source.origins.clone(),
                source_url,
                types,
                ..Default::default()
            },
        )
        .await
    })
    .await;

    Ok((count, results, errors))
}
