use std::sync::Arc;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use geo::{Coord, Rect};
use miniproj::get_projection;
use serde::Deserialize;
use serde_roxmltree::{defaults, roxmltree::Document, Options};

use harvester::{client::Client, Source};
use metadaten::{
    atkis::{Database, Entry},
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25832).unwrap();

    let text = client
        .fetch_text(source, "vertriebseinheiten".to_owned(), &source.url)
        .await?;

    let doc = Document::parse(&text)?;

    let vertriebseinheiten = defaults()
        .namespaces()
        .from_doc::<Vertriebseinheiten>(&doc)?;

    let results = vertriebseinheiten.members.len();

    let entries = vertriebseinheiten
        .members
        .into_iter()
        .map(|member| {
            let lower = {
                let mut lower = member
                    .b25_utm32s
                    .bounded_by
                    .envelope
                    .lower
                    .split_ascii_whitespace();
                let x = lower
                    .next()
                    .ok_or_else(|| anyhow!("Missing lower x coordinate"))?
                    .parse()?;
                let y = lower
                    .next()
                    .ok_or_else(|| anyhow!("Missing lower y coordinate"))?
                    .parse()?;
                Coord { x, y }
            };

            let upper = {
                let mut upper = member
                    .b25_utm32s
                    .bounded_by
                    .envelope
                    .upper
                    .split_ascii_whitespace();
                let x = upper
                    .next()
                    .ok_or_else(|| anyhow!("Missing lower x coordinate"))?
                    .parse()?;
                let y = upper
                    .next()
                    .ok_or_else(|| anyhow!("Missing lower y coordinate"))?
                    .parse()?;
                Coord { x, y }
            };

            let bounding_box = Some(proj_rect(Rect::new(lower, upper), proj));

            Ok((
                member.b25_utm32s.id,
                Arc::new(Entry {
                    name: member.b25_utm32s.name,
                    bounding_box,
                }),
            ))
        })
        .collect::<Result<_>>()?;

    Database { entries }.write(dir)?;

    Ok((results, results, 0))
}

#[derive(Deserialize)]
struct Vertriebseinheiten<'a> {
    #[serde(rename = "{http://www.opengis.net/wfs/2.0}member", borrow)]
    members: Vec<Member<'a>>,
}

#[derive(Deserialize)]
struct Member<'a> {
    #[serde(rename = "{http://bkg.bund.de/vertriebseinheiten}b25_utm32s", borrow)]
    b25_utm32s: B25Utm32S<'a>,
}

#[derive(Deserialize)]
struct B25Utm32S<'a> {
    #[serde(rename = "{http://bkg.bund.de/vertriebseinheiten}id")]
    id: u64,
    #[serde(rename = "{http://bkg.bund.de/vertriebseinheiten}blattname")]
    name: Box<str>,
    #[serde(rename = "{http://www.opengis.net/gml/3.2}boundedBy", borrow)]
    bounded_by: BoundedBy<'a>,
}

#[derive(Deserialize)]
struct BoundedBy<'a> {
    #[serde(rename = "{http://www.opengis.net/gml/3.2}Envelope", borrow)]
    envelope: Envelope<'a>,
}

#[derive(Deserialize)]
struct Envelope<'a> {
    #[serde(rename = "{http://www.opengis.net/gml/3.2}lowerCorner")]
    lower: &'a str,
    #[serde(rename = "{http://www.opengis.net/gml/3.2}upperCorner")]
    upper: &'a str,
}
