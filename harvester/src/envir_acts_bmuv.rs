use std::borrow::Cow;
use std::fmt::Write;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::Date;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{make_key, parse_text, select_text, GermanDate},
    write_dataset, Source,
};
use metadaten::dataset::{r#type::Type, Dataset, Language, License, Resource, ResourceType};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (count, results, errors) = fetch_page(dir, client, source, selectors, 1).await?;

    const BATCH_SIZE: usize = 20;
    let pages = count.div_ceil(BATCH_SIZE);

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let page = page.to_string();

    let mut url = source.url.clone();

    url.query_pairs_mut()
        .append_pair("tx_bmublegal_legal[currentPage]", &page)
        .append_key_only("tx_bmublegal_legal[filter][subject]")
        .append_pair("tx_bmublegal_legal[filter][subtype][1][11]", "11")
        .append_pair("tx_bmublegal_legal[filter][subtype][1][12]", "12")
        .append_key_only("tx_bmublegal_legal[filter][subtype][1][13]")
        .append_key_only("tx_bmublegal_legal[filter][subtype][1][14]")
        .append_key_only("tx_bmublegal_legal[filter][subtype][1][16]")
        .append_key_only("tx_bmublegal_legal[filter][subtype][2][21]")
        .append_key_only("tx_bmublegal_legal[filter][subtype][2][22]")
        .append_key_only("tx_bmublegal_legal[filter][subtype][2][23]")
        .append_key_only("tx_bmublegal_legal[filter][subtype][3][31]")
        .append_key_only("tx_bmublegal_legal[filter][subtype][3][32]")
        .append_key_only("tx_bmublegal_legal[filter][sword]");

    let count;
    let items;

    {
        let text = client
            .fetch_text(source, format!("page_{}", page), &url)
            .await?;

        let document = Html::parse_document(&text);

        count = parse_text::<_, usize>(
            &document,
            &selectors.page_search_results,
            &selectors.page_results_value,
            "Counts",
        )?;

        items = document
            .select(&selectors.page_container_item)
            .map(|element| {
                let act_framework = Some(select_text(element, &selectors.page_act_meta));

                let container_body = element
                    .select(&selectors.page_container_body)
                    .next()
                    .ok_or_else(|| anyhow!("Missing body entry"))?;

                let act_title = select_text(container_body, &selectors.page_act_title);

                let features = select_text(container_body, &selectors.page_act_category);

                let (act_issued, act_legal_level, act_category) =
                    if let Some(captures) = selectors.page_dlc.captures(&features) {
                        let issued = captures
                            .name("issued")
                            .map(|issued| issued.as_str().parse::<GermanDate>().map(Into::into))
                            .transpose()?;

                        let legal_level = captures["legal_level"].to_owned();
                        let category = captures["category"].to_owned();

                        (issued, Some(legal_level), Some(category))
                    } else {
                        (None, None, None)
                    };

                let act_href = container_body
                    .select(&selectors.page_act_href)
                    .next()
                    .ok_or_else(|| anyhow!("Missing href entry"))?
                    .attr("href")
                    .unwrap()
                    .to_owned();

                Ok(Pagedata {
                    act_framework,
                    act_title,
                    act_issued,
                    act_legal_level,
                    act_category,
                    act_href,
                })
            })
            .collect::<Result<Vec<_>>>()?;
    }

    let (results, errors) = fetch_many(0, 0, items, |item| {
        translate_pagedata(dir, client, source, selectors, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_pagedata(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Pagedata,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.act_href).into_owned();
    let source_url = source.url.join(&item.act_href)?;

    let text = client.fetch_text(source, key.clone(), &source_url).await?;
    let document = Html::parse_document(&text);

    let act_category = item.act_category.as_deref().unwrap_or("unbekannt");

    let legal_level = item.act_legal_level.as_deref().unwrap_or("unbekannt");

    let act_framework = item.act_framework.as_deref().unwrap_or("unbekannt.");

    let legal_description = format!("{} {}", act_category, legal_level);

    // FIXME: This should become structured metadata in `Type::Legal`.
    let legal_description = match legal_description.as_str() {
        "Gesetze National" => "Es handelt sich um ein Gesetz auf nationaler Ebene",
        "Verordnungen National" => "Es handelt sich um eine Verordnung auf nationaler Ebene",
        _ => "Die rechtliche Kategorie wie auch die rechtliche Gültigkeit ist unbekannt",
    };

    let act_framework = if act_framework.is_empty() {
        Cow::Borrowed("Ein übergeordnetes rechtliches Rahmenwerk ist nicht vorhanden")
    } else {
        Cow::Owned(format!(
            "Der übergeordnete Rahmen ist die/das {}",
            act_framework
        ))
    };

    let mut description = select_text(&document, &selectors.link_description);
    write!(
        &mut description,
        " {}. {}.",
        legal_description, act_framework
    )
    .unwrap();

    let modified = parse_text::<_, GermanDate>(
        &document,
        &selectors.link_modified,
        &selectors.regex_modified,
        "Modified date",
    )
    .ok()
    .map(Into::into);

    let resources = document
        .select(&selectors.resources_item)
        .filter_map(|element| {
            let href = element
                .select(&selectors.resources_href)
                .next()?
                .attr("href")
                .unwrap()
                .to_owned();

            if href.is_empty() {
                return None;
            }

            let description = select_text(element, &selectors.resource_title);

            if description.is_empty() {
                return None;
            }

            Some((Some(description), href))
        })
        .chain(
            document
                .select(&selectors.direct_link_item)
                .filter_map(|element| {
                    let href = element
                        .select(&selectors.direct_link)
                        .next()?
                        .attr("href")
                        .unwrap()
                        .to_owned();

                    if href.is_empty() {
                        return None;
                    }

                    let description = parse_text::<_, String>(
                        &document,
                        &selectors.direct_link_item,
                        &selectors.direct_link_description_regex,
                        "Direct link description",
                    )
                    .ok();

                    Some((description, href))
                }),
        )
        .map(|(description, href)| {
            Ok(Resource {
                r#type: ResourceType::WebPage, // FIXME: Hier gibt es auch Pressemitteilungen.
                description,
                url: source.url.join(&href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect::<Result<SmallVec<_>>>()?;

    let types = smallvec![Type::Legal];

    let dataset = Dataset {
        title: item.act_title,
        description: Some(description),
        issued: item.act_issued,
        modified,
        resources,
        types,
        language: Language::German,
        license: License::OfficialWork,
        origins: source.origins.clone(),
        source_url: source_url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Clone)]
struct Pagedata {
    act_framework: Option<String>,
    act_title: String,
    act_category: Option<String>,
    act_legal_level: Option<String>,
    act_issued: Option<Date>,
    act_href: String,
}

selectors! {
    page_container_item: ".c-articles-list__item-container",
    page_container_meta: ".c-articles-list__meta",
    page_act_meta: ".c-articles-list__number",
    page_container_body: ".c-articles-list__body",
    page_act_title: ".c-articles-list__title",
    page_act_category: ".c-articles-list__category",
    page_dlc: r"(?<issued>\d{2}.\d{2}.\d{4})?[\s|]*(?<legal_level>\w*)[\s|]*(?<category>\w*)" as Regex,
    page_act_href: ".c-articles-list__title > a[href]",
    page_search_results: ".c-list-header__results",
    page_results_value: r"von\s+(\d*)" as Regex,
    link_description: r"div.c-ce-formated",
    link_modified: r"small",
    regex_modified: r"Aktualisierungsdatum: (\d{2}.\d{2}.\d{4})" as Regex,
    resources_item: ".c-teaser__content",
    resources_href: "a[href]",
    resource_title: ".c-teaser__headline",
    direct_link_item: ".c-download-list__list",
    direct_link: "a[href]",
    direct_link_description_regex: r"\w*:\s*(\w.*)" as Regex,
}
