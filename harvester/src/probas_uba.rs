use anyhow::{anyhow, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use hashbrown::HashSet;
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::{smallvec, SmallVec};
use time::Date;
use url::Url;
use uuid::Uuid;

use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, Organisation, OrganisationRole, Person, PersonRole, Region, Resource,
    ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let pdf_url = &Url::parse(&source.url.as_str().replace("data.probas", "pdf.probas"))?;

    let processes = AtomicRefCell::new(HashSet::new());

    let (pages, mut results, mut errors) = fetch_processes(client, source, &processes, 0).await?;

    (results, errors) = fetch_many(results, errors, 1..pages, |page| {
        fetch_processes(client, source, &processes, page)
    })
    .await;

    let processes = processes.into_inner();

    let count = processes.len();

    (results, errors) = fetch_many(results, errors, processes, |id| {
        fetch_process(dir, client, source, pdf_url, id)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_processes(
    client: &Client,
    source: &Source,
    processes: &AtomicRefCell<HashSet<Uuid>>,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let mut url = source
        .url
        .join("resource/datastocks/ebee4288-5f27-4d18-8e2d-c98e985cda5a/processes")?;

    url.query_pairs_mut()
        .append_pair("format", "json")
        .append_pair("startIndex", &(50 * page).to_string())
        .append_pair("pageSize", "50");

    let bytes = client
        .fetch_bytes(source, format!("processes-json-{page}"), &url)
        .await?;

    #[derive(Deserialize)]
    struct Response {
        #[serde(rename = "totalCount")]
        total_count: usize,
        data: Vec<Data>,
    }

    #[derive(Deserialize)]
    struct Data {
        uuid: Uuid,
    }

    let response = from_slice::<Response>(&bytes)?;

    let pages = response.total_count / 50;

    processes
        .borrow_mut()
        .extend(response.data.into_iter().map(|data| data.uuid));

    Ok((pages, 1, 0))
}

async fn fetch_process(
    dir: &Dir,
    client: &Client,
    source: &Source,
    pdf_url: &Url,
    id: Uuid,
) -> Result<(usize, usize, usize)> {
    tracing::debug!("Fetching process {id}");

    let source_url = source.url.join(&format!(
        "resource/datastocks/ebee4288-5f27-4d18-8e2d-c98e985cda5a/processes/{id}"
    ))?;

    let mut url = source_url.clone();

    url.query_pairs_mut().append_pair("format", "json");

    let bytes = client
        .fetch_bytes(source, format!("process-json-{id}.isol"), &url)
        .await?;

    let mut process = from_slice::<Process>(&bytes)?;

    let title = process
        .process_info
        .dataset_info
        .name
        .base_name
        .german("title", &id)?;

    let description = process
        .process_info
        .tech_info
        .and_then(|tech_info| tech_info.description)
        .map(|description| description.german("description", &id))
        .transpose()?;

    let comment = process
        .process_info
        .dataset_info
        .comment
        .map(|comment| comment.german("comment", &id))
        .transpose()?;

    let region = process
        .geo_info
        .map(|geo_info| Region::Other(geo_info.location.location));

    let issued = process
        .time_info
        .map(|time_info| Date::from_ordinal_date(time_info.year, 1))
        .transpose()?;

    let tags = process
        .process_info
        .dataset_info
        .class_info
        .map(|class_info| {
            class_info
                .classifications
                .into_iter()
                .flat_map(|classification| classification.classes)
                .map(|class| Tag::Other(class.value))
                .collect()
        })
        .unwrap_or_default();

    let resources = smallvec![
        Resource {
            r#type: ResourceType::Zip,
            description: None,
            url: {
                let mut url = source_url.clone();
                url.path_segments_mut()
                    .map_err(|()| anyhow!("Invalid source URL"))?
                    .push("zipexport");
                url.into()
            },
            primary_content: true,
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::MicrosoftExcelSpreadsheet,
            description: None,
            url: source
                .url
                .join(&format!("resource/convert/{id}/xlsx"))?
                .into(),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::Pdf,
            description: None,
            url: {
                let mut html_url = source_url.clone();
                html_url.query_pairs_mut().append_pair("format", "html");

                let mut pdf_url = pdf_url.clone();
                pdf_url
                    .query_pairs_mut()
                    .append_pair("forceExpand", "true")
                    .append_pair("url", html_url.as_str());

                pdf_url.into()
            },
            ..Default::default()
        },
    ];

    // TODO: Organisation names are ambiguous and should be resolved into a controlled vocabulary.

    let organisations = process
        .admin_info
        .as_mut()
        .and_then(|admin_info| admin_info.commissioning.take())
        .map(|commissioning| {
            commissioning
                .commissioners
                .into_iter()
                .map(|commissioner| -> Result<_> {
                    Ok(Organisation::Other {
                        name: commissioner.description.german("organisation name", &id)?,
                        role: OrganisationRole::Publisher,
                        websites: SmallVec::new(),
                    })
                })
                .collect()
        })
        .transpose()?
        .unwrap_or_default();

    let persons = process
        .admin_info
        .map(|admin_info| -> Result<_> {
            let mut persons = Vec::new();

            if let Some(creator) = admin_info.data_entry.creator {
                persons.push(Person {
                    name: creator.description.german("creator name", &id)?,
                    role: PersonRole::Creator,
                    ..Default::default()
                });
            }

            if let Some(owner) = admin_info.publication.owner {
                persons.push(Person {
                    name: owner.description.german("owner name", &id)?,
                    role: PersonRole::Manager,
                    ..Default::default()
                });
            }

            Ok(persons)
        })
        .transpose()?
        .unwrap_or_default();

    // TODO: Consider the additional metadata available at "http://www.gemis.de/de/doc/prc/{{{id}}}"

    let dataset = Dataset {
        title,
        description,
        comment,
        regions: region.into_iter().collect(),
        issued,
        tags,
        resources,
        organisations,
        persons,
        language: Language::German,
        origins: source.origins.clone(),
        source_url: source_url.into(),
        types: smallvec![Type::Text {
            text_type: TextType::Report
        }],
        ..Default::default()
    };

    write_dataset(dir, client, source, id.to_string(), dataset).await
}

#[derive(Deserialize)]
struct Process {
    #[serde(rename = "processInformation")]
    process_info: ProcessInfo,
    #[serde(rename = "geography")]
    geo_info: Option<GeoInfo>,
    #[serde(rename = "time")]
    time_info: Option<TimeInfo>,
    #[serde(rename = "administrativeInformation")]
    admin_info: Option<AdminInfo>,
}

#[derive(Deserialize)]
struct ProcessInfo {
    #[serde(rename = "dataSetInformation")]
    dataset_info: DatasetInfo,
    #[serde(rename = "technology")]
    tech_info: Option<TechInfo>,
}

#[derive(Deserialize)]
struct DatasetInfo {
    name: Name,
    #[serde(rename = "generalComment")]
    comment: Option<TranslatedValue>,
    #[serde(rename = "classificationInformation")]
    class_info: Option<ClassInfo>,
}

#[derive(Deserialize)]
struct Name {
    #[serde(rename = "baseName")]
    base_name: TranslatedValue,
}

#[derive(Deserialize)]
struct ClassInfo {
    #[serde(rename = "classification", default)]
    classifications: Vec<Classification>,
}

#[derive(Deserialize)]
struct Classification {
    #[serde(rename = "class", default)]
    classes: Vec<Class>,
}

#[derive(Deserialize)]
struct Class {
    value: CompactString,
}

#[derive(Deserialize)]
struct TechInfo {
    #[serde(rename = "technologyDescriptionAndIncludedProcesses")]
    description: Option<TranslatedValue>,
}

#[derive(Deserialize)]
struct GeoInfo {
    #[serde(rename = "locationOfOperationSupplyOrProduction")]
    location: Location,
}

#[derive(Deserialize)]
struct Location {
    location: CompactString,
}

#[derive(Deserialize)]
struct TimeInfo {
    #[serde(rename = "referenceYear")]
    year: i32,
}

#[derive(Deserialize)]
struct AdminInfo {
    #[serde(rename = "commissionerAndGoal")]
    commissioning: Option<Commissioning>,
    #[serde(rename = "dataEntryBy")]
    data_entry: DataEntry,
    #[serde(rename = "publicationAndOwnership")]
    publication: Publication,
}

#[derive(Deserialize)]
struct Commissioning {
    #[serde(rename = "referenceToCommissioner", default)]
    commissioners: Vec<NamedEntity>,
}

#[derive(Deserialize)]
struct DataEntry {
    #[serde(rename = "referenceToPersonOrEntityEnteringTheData")]
    creator: Option<NamedEntity>,
}

#[derive(Deserialize)]
struct Publication {
    #[serde(rename = "referenceToOwnershipOfDataSet")]
    owner: Option<NamedEntity>,
}

#[derive(Deserialize)]
struct NamedEntity {
    #[serde(rename = "shortDescription")]
    description: TranslatedValue,
}

#[derive(Deserialize)]
#[serde(transparent)]
struct TranslatedValue(Vec<LangValuePair>);

impl TranslatedValue {
    fn german(self, field: &str, id: &Uuid) -> Result<String> {
        self.0
            .into_iter()
            .find(|lvp| matches!(lvp.lang.as_deref(), Some("de") | None))
            .map(|lvp| lvp.value)
            .ok_or_else(|| anyhow!("Missing German translation of {field} for process {id}"))
    }
}

#[derive(Deserialize)]
struct LangValuePair {
    value: String,
    lang: Option<CompactString>,
}
