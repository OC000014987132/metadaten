use std::fmt::Write;

use anyhow::{Error, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use cow_utils::CowUtils;
use miniproj::get_projection;
use serde::Deserialize;
use serde_roxmltree::from_str;
use smallvec::smallvec;
use time::{serde::format_description, Date};

use harvester::{
    client::Client,
    fetch_many,
    utilities::{contains_or, point_like_bounding_box},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25832).unwrap();

    let source_url = &source.url;

    let response = {
        let mut url = source_url.join("public/ogcsl.ashx")?;
        url.query_pairs_mut()
            .append_pair("nodeId", "263")
            .append_pair("service", "WFS")
            .append_pair("request", "GetFeature")
            .append_pair("typeNames", "cls:L197");

        let text = client
            .make_request(source, "wfs".to_owned(), Some(&url), |client| async {
                let mut resp = client.get(url.clone()).send().await?.error_for_status()?;

                let mut chunks = Vec::new();

                loop {
                    match resp.chunk().await {
                        Ok(Some(chunk)) => chunks.push(chunk),
                        Ok(None) => break,
                        // HACK: CardoMap3 uses chunked encoding but skips the EOF chunk.
                        Err(err) if err.is_decode() => break,
                        Err(err) => return Err(Error::from(err)),
                    }
                }

                let len = chunks.iter().map(|chunk| chunk.len()).sum::<usize>();

                let mut bytes = Vec::with_capacity(len);

                for chunk in chunks {
                    bytes.extend(chunk);
                }

                let text = String::from_utf8(bytes)?;

                Ok(text)
            })
            .await?;

        from_str::<FeatureCollection>(&text)?
    };

    let count = response.members.len();

    let (results, errors) = fetch_many(0, 0, response.members, |member| async move {
        let id = member.layer.id;

        let station_name = member.layer.name;

        let title = format!("Grundwassermessstelle {station_name} ({id})");

        let mut description = format!("Dieser Datensatz beschreibt die {title}.");
        if !member.layer.jan_mean.is_empty() {
            write!(
                &mut description,
                " Der Datensatz enthält mehrjährige Monats-Mittelwerte aller Monate."
            )?;
        };

        if !member.layer.low_level.is_empty() {
            write!(
                &mut description,
                " Der Datensatz enthält Extremwerte der Pegelstände."
            )?;
        };

        let sampling_device = member.layer.measurement_device;

        if !sampling_device.is_empty() {
            write!(
                &mut description,
                " Die Messstelle ist ein {sampling_device}."
            )?;
        };

        let soil_condition = member.layer.scs;

        if !soil_condition.is_empty() {
            write!(
                &mut description,
                " Die Bodenzustandserhebung entspricht: {soil_condition}."
            )?;
        };

        let sampling_frequency = member.layer.periodicity;

        if !sampling_frequency.is_empty() {
            write!(
                &mut description,
                " Die Probennahmehäufigkeit ist {sampling_frequency}."
            )?;
        };

        let start_date = member.layer.start_date;
        let end_date = member.layer.end_date;
        let time_ranges = smallvec![(start_date, end_date).into()];

        let x_coord = member.layer.hochwert;
        let y_coord = member.layer.rechtswert;
        let bounding_boxes = smallvec![proj_rect(point_like_bounding_box(x_coord, y_coord), proj)];

        let gemeinde = member.layer.gemeinden.cow_replace(", Stadt", "");
        let region = contains_or(gemeinde, Region::ST);

        let organisations = smallvec![Organisation::Other {
            name: "Landesbetrieb für Hochwasserschutz und Wasserwirtschaft".to_owned(),
            role: OrganisationRole::Publisher,
            websites: smallvec!["https://lhw.sachsen-anhalt.de".to_owned()],
        }];

        let pdf_stations = member.layer.pdf_station;
        let internal_ids = member.layer.internal_id;

        let low_levels = member.layer.low_level;
        let high_levels = member.layer.high_level;
        let mean_levels = member.layer.mean_level;

        let config = if sampling_device == "GW-Lattenpegel" {
            "GW_WASSERSTAND_LPEGEL_NORM"
        } else if sampling_device == "Quellschüttungen" {
            "GW_ABFLUSS"
        } else {
            "GW_WASSERSTAND_NORM"
        };

        let resources = smallvec![
            Resource {
                r#type: ResourceType::Pdf,
                description: Some("Steckbrief der Grundwassermessstelle".to_owned()),
                url: {
                    let mut url = source.url.join(&format!(
                        "project/cardoMap/Datenblatt/FIW_GW/{pdf_stations}"
                    ))?;
                    url.query_pairs_mut()
                        .append_pair("CARDOMAP_TH_TITLE", "Grundwassermessstellen")
                        .append_pair("CARDOMAP_TH_ID", "10.263")
                        .append_pair("CARDOMAP_L_ID", "L197");
                    url.into()
                },
                primary_content: true,
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::WebPage,
                description: Some("LHW-Datenportal mit Kartenanwendung".to_owned()),
                url: source.url.clone().into(),
                ..Default::default()
            }
        ];

        let mut source_url = source
            .url
            .join("project/cardo3Apps/IDU_CMAPP_LFUBRB_Diagramme/Diagramm.aspx")?;
        source_url
            .query_pairs_mut()
            .append_pair("diagramm_config", config)
            .append_pair("id_messstelle", &internal_ids)
            .append_pair("URPRM_GOK_NW", &low_levels)
            .append_pair("QURPRM_GOK_MW", &mean_levels)
            .append_pair("QURPRM_GOK_HW", &high_levels);

        let types = smallvec![Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(id.clone()),
                measurement_frequency: Some(sampling_frequency.into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasserstand".to_owned()],
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            regions: smallvec![region],
            bounding_boxes,
            time_ranges,
            organisations,
            resources,
            language: Language::German,
            license: License::OtherOpen,
            origins: source.origins.clone(),
            source_url: source_url.into(),
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, id.into(), dataset).await
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Debug, Deserialize)]
struct FeatureCollection {
    #[serde(rename = "member")]
    members: Vec<Member>,
}

#[derive(Debug, Deserialize)]
struct Member {
    #[serde(rename = "L197")]
    layer: Layer,
}

#[derive(Debug, Deserialize)]
struct Layer {
    #[serde(rename = "nummer")]
    id: CompactString,
    #[serde(rename = "int_mst_id")]
    internal_id: String,
    #[serde(rename = "name")]
    name: String,
    #[serde(rename = "werte_von", with = "german_date")]
    start_date: Date,
    #[serde(rename = "werte_bis", with = "german_date")]
    end_date: Date,
    #[serde(rename = "mst_art")]
    measurement_device: String,
    #[serde(rename = "bze")]
    scs: String,
    #[serde(rename = "turnus")]
    periodicity: String,
    #[serde(rename = "hochwert")]
    hochwert: f64,
    #[serde(rename = "rechtswert")]
    rechtswert: f64,
    #[serde(rename = "gemeinde")]
    gemeinden: String,
    #[serde(rename = "mw01")]
    jan_mean: String,
    #[serde(rename = "nw")]
    low_level: String,
    #[serde(rename = "hw")]
    high_level: String,
    #[serde(rename = "mw")]
    mean_level: String,
    #[serde(rename = "datenblatt")]
    pdf_station: String,
}

format_description!(german_date, Date, "[day].[month].[year]");
