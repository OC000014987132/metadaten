use std::borrow::Cow;

use anyhow::{anyhow, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{algorithm::Contains, Coord, Rect};
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::smallvec;
use time::{format_description::well_known::Iso8601, Date};

use harvester::{client::Client, fetch_many, utilities::contains_or, write_dataset, Source};
use metadaten::dataset::{
    r#type::{Domain, Station, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let url = source.url.join("/data/map.json")?;

    let bytes = client.fetch_bytes(source, "map".to_owned(), &url).await?;

    let map = from_slice::<Map>(&bytes)?;

    let last_modified = AtomicRefCell::new(Date::MIN);

    let count = map.gauges.len();

    let (results, errors) = fetch_many(0, 0, map.gauges, |gauge| {
        fetch_station(dir, client, source, &last_modified, gauge)
    })
    .await;

    let dataset = Dataset {
        title: "Wasserstandsvorhersage Nordsee des BSH".to_owned(),
        description: Some(format!("Aktuelle Vorhersage: {}", map.forecast_text)),
        modified: Some(last_modified.into_inner()),
        organisations: smallvec![Organisation::Other {
            name: "Wasserstandsvorhersagedienst Nordsee".to_owned(),
            role: OrganisationRole::Unknown,
            websites: smallvec!["https://www.bsh.de/DE/Home/home_node.html".to_owned()],
        }],
        resources: smallvec![Resource {
            r#type: ResourceType::Pdf,
            description: Some("Übersicht zu den Wasserstandsvorhersagen des BSH".to_owned()),
            url: "https://www2.bsh.de/aktdat/wvd/Wasserstandsvorhersage.pdf".to_owned(),
            primary_content: true,
            ..Default::default()
        }],
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        source_url: source.url.clone().into(),
        ..Default::default()
    };

    write_dataset(
        dir,
        client,
        source,
        "wasserstandsvorhersage".to_owned(),
        dataset,
    )
    .await?;

    Ok((count + 1, results + 1, errors))
}

async fn fetch_station(
    dir: &Dir,
    client: &Client,
    source: &Source,
    last_modified: &AtomicRefCell<Date>,
    gauge: Gauge<'_>,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&format!("data/DE__{}.json", gauge.bshnr))?;

    let bytes = client
        .fetch_bytes(source, gauge.seo_id.to_owned(), &url)
        .await?;

    let station = from_slice::<MeasurementStation>(&bytes)?;

    if station.status == 0 {
        tracing::debug!(
            "Station {} is (perhaps temporarily) unavailable",
            gauge.seo_id
        );
        return Ok((1, 0, 0));
    }

    let creation_forecast = station
        .creation_forecast
        .split_once(' ')
        .map_or(station.creation_forecast, |(prefix, _suffix)| prefix);

    let modified = Date::parse(creation_forecast, &Iso8601::DEFAULT)?;

    {
        let mut last_modified = last_modified.borrow_mut();
        *last_modified = last_modified.max(modified);
    }

    let coord = Coord {
        x: station.longitude,
        y: station.latitude,
    };

    let bounding_boxes = smallvec![Rect::new(coord, coord)];

    const COASTAL_FEDERAL_STATES: [Region; 5] =
        [Region::SH, Region::HH, Region::NI, Region::HB, Region::MV];

    let mut region = None;

    for federal_state in COASTAL_FEDERAL_STATES {
        if let Some(bounding_box) = federal_state.bounding_box() {
            if bounding_box.contains(&coord) {
                if let Some(shape) = federal_state.shape() {
                    if shape.contains(&coord) {
                        region = Some(contains_or(station.area, federal_state));
                        break;
                    }
                }
            }
        }
    }

    let mut resources = smallvec![Resource {
        r#type: ResourceType::WebPage,
        description: Some("Aktuelle Messdaten über PEGELONLINE".to_owned()),
        url: station.station_url,
        direct_link: false,
        ..Default::default()
    }];

    for period in ["Tag_1-2", "Tag_1-6"] {
        resources.push(Resource {
            r#type: ResourceType::Png,
            description: Some(format!(
                "Diagram für Vorhersagen für {period}: Höhe über pegelnullpunkt [cm]"
            )),
            url: format!(
                "https://www.sturmflutwarndienst.de/ersatzgrafik/DE__{}_{period}_PNP.png",
                gauge.bshnr
            ),
            ..Default::default()
        });
    }

    let station_id = Some(station.station_name.as_ref().into());
    let types = smallvec![Type::Measurements {
        domain: Domain::Sea,
        station: Some(Station {
            id: station_id,
            ..Default::default()
        }),
        measured_variables: smallvec!["Wasserstand".to_owned()],
        methods: Default::default(),
    }];

    let dataset = Dataset {
            title: format!("Wasserstandsvorhersage Nordsee, {}", station.station_name),
            description: Some(format!(
                "Mittleres Hochwasser (MHW): {}m über Pegelnullpunkt, Mittleres Niedrigwasser (MNW): {}m über Pegelnullpunkt",
                station.hochwasser.as_int()? / 100, station.niedrigwasser.as_int()? / 100,
            )),
            types,
            modified: Some(modified),
            organisations: smallvec![Organisation::Other {
                name: "Wasserstandsvorhersagedienst Nordsee".to_owned(),
                role: OrganisationRole::Unknown,
                websites: smallvec!["https://www.bsh.de/DE/Home/home_node.html".to_owned()],
            }],
            resources,
            regions: region.into_iter().collect(),
            bounding_boxes,
            language: Language::German,
            license: License::AllRightsReserved,
            origins: source.origins.clone(),
            source_url: source.url.join(gauge.seo_id)?.into(),
            ..Default::default()
        };

    write_dataset(dir, client, source, gauge.seo_id.to_owned(), dataset).await
}

#[derive(Deserialize)]
struct Map<'a> {
    #[serde(borrow)]
    forecast_text: Cow<'a, str>,
    gauges: Vec<Gauge<'a>>,
}

#[derive(Deserialize)]
struct Gauge<'a> {
    seo_id: &'a str,
    bshnr: &'a str,
}

#[derive(Deserialize)]
struct MeasurementStation<'a> {
    status: i32,
    #[serde(borrow)]
    station_name: Cow<'a, str>,
    station_url: String,
    creation_forecast: &'a str,
    #[serde(rename = "MHW")]
    hochwasser: IntOrNone<'a>,
    #[serde(rename = "MNW")]
    niedrigwasser: IntOrNone<'a>,
    area: CompactString,
    longitude: f64,
    latitude: f64,
}

#[derive(Deserialize)]
#[serde(untagged)]
enum IntOrNone<'a> {
    Int(i32),
    None(&'a str),
}

impl IntOrNone<'_> {
    fn as_int(&self) -> Result<i32> {
        match self {
            Self::Int(val) => Ok(*val),
            Self::None(val) => Err(anyhow!("Expected integer value but got `{val}`")),
        }
    }
}
