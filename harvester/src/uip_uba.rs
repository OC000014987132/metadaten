use std::fmt::Write;

use anyhow::{anyhow, Context, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use itertools::Itertools;
use regex::Regex;
use scraper::{selectable::Selectable, Html, Selector};
use serde::Deserialize;
use serde_json::from_str as from_json_str;
use smallvec::{smallvec, smallvec_inline, SmallVec};
use time::{
    format_description::{well_known::Iso8601, FormatItem},
    macros::format_description,
    Date,
};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_text, since_year},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Person, PersonRole, Region,
    Resource, ResourceType, TimeRange,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut results, mut errors) =
        process_funding_info_categories(dir, client, source, selectors).await?;

    let (_count1, results1, errors1) = process_easy_german(dir, client, source, selectors).await?;
    results += results1;
    errors += errors1;

    let (results1, errors1) = process_about_us_categories(dir, client, source, selectors).await?;
    results += results1;
    errors += errors1;

    let (results1, errors1) = process_events(dir, client, source, selectors).await?;
    results += results1;
    errors += errors1;

    let (results1, errors1) = process_news(dir, client, source, selectors).await?;
    results += results1;
    errors += errors1;

    let (results1, errors1) = process_search_results(dir, client, source, selectors).await?;
    results += results1;
    errors += errors1;

    Ok((results, results, errors))
}

async fn process_funding_info_categories(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize)> {
    let funding_info_url = source.url.join("foerderinformationen")?;

    let text = client
        .fetch_text(source, "foerderinformationen".to_owned(), &funding_info_url)
        .await?;
    let funding_info_page = Html::parse_document(&text);

    let download_url = source.url.join("hinweise-vordrucke")?;

    let text = client
        .fetch_text(source, "hinweise-vordruck".to_owned(), &download_url)
        .await?;
    let download_page = Html::parse_document(&text);

    let mut stubs = Vec::new();

    stubs.push(process_funding_faqs(
        selectors,
        &funding_info_page,
        &funding_info_url,
    ));

    let mut persons = Vec::new();
    stubs.push(process_downloadable_files(
        source,
        selectors,
        &funding_info_page,
        &download_page,
        &download_url,
        &mut persons,
    ));

    stubs.push(process_quickcheck(
        selectors,
        &funding_info_page,
        &funding_info_url,
        &persons,
    ));

    stubs.push(process_funding_info(
        source,
        selectors,
        &funding_info_page,
        &funding_info_url,
        &persons,
    ));

    let (results, errors) = fetch_many(0, 0, stubs, |stub| async move {
        let stub = stub?;

        let dataset = Dataset {
            title: stub.title,
            description: stub.description,
            types: stub.types.into_iter().collect(),
            source_url: stub.source_url,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            resources: stub.resources,
            persons: stub.persons,
            ..Default::default()
        };

        write_dataset(dir, client, source, stub.key, dataset).await
    })
    .await;

    Ok((results, errors))
}

fn process_funding_faqs(
    selectors: &Selectors,
    funding_info_page: &Html,
    source_url: &Url,
) -> Result<DatasetStub> {
    let title = "Umweltinnovationsprogramm - Förderinformationen: Fragen und Antworten".to_owned();

    let description = funding_info_page
        .select(&selectors.faq_question_sel)
        .zip(funding_info_page.select(&selectors.faq_answer_sel))
        .map(|(question, answer)| {
            format!(
                "{}: {}",
                collect_text(question.text()),
                collect_text(answer.text())
            )
        })
        .join("; ");

    let persons = vec![Person {
        name: "pmi".to_owned(),
        role: PersonRole::Contact,
        emails: smallvec!["pmi@uba.de".into()],
        ..Default::default()
    }];

    Ok(DatasetStub {
        key: "funding-faqs".to_owned(),
        title,
        description: Some(description),
        types: Some(Type::Text {
            text_type: TextType::Editorial,
        }),
        source_url: source_url.clone().into(),
        persons,
        ..Default::default()
    })
}

fn process_downloadable_files(
    source: &Source,
    selectors: &Selectors,
    funding_info_page: &Html,
    download_page: &Html,
    source_url: &Url,
    persons: &mut Vec<Person>,
) -> Result<DatasetStub> {
    let title = collect_text(
        download_page
            .select(&selectors.page_title_sel)
            .next()
            .ok_or_else(|| anyhow!("Missing title of downloadable files"))?
            .text(),
    );

    let description = collect_text(
        funding_info_page
            .select(&selectors.downloadable_description_sel)
            .nth(1)
            .ok_or_else(|| anyhow!("Missing description of downloadable files"))?
            .text(),
    );

    let resources = download_page
        .select(&selectors.downloadable_files_sel)
        .map(|downloadable_file| {
            let description = collect_text(
                downloadable_file
                    .select(&selectors.file_description_sel)
                    .next()
                    .ok_or_else(|| anyhow!("Missing resource description for download files"))?
                    .text(),
            );

            let href = downloadable_file.attr("href").unwrap();

            Ok(Resource {
                r#type: ResourceType::Document,
                description: Some(description),
                url: source.url.join(href)?.into(),
                primary_content: true,
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect::<Result<_>>()?;

    let emails = collect_text(
        funding_info_page
            .select(&selectors.text_sel)
            .nth(1)
            .ok_or_else(|| anyhow!("Missing mails for downloadable files"))?
            .text(),
    );

    let emails = selectors
        .email_sep
        .captures_iter(&emails)
        .map(|capture| capture[1].to_owned())
        .collect();

    *persons = vec![Person {
        name: "pmi".to_string(),
        role: PersonRole::Contact,
        emails,
        ..Default::default()
    }];

    Ok(DatasetStub {
        key: "downloadable-files".to_owned(),
        title,
        description: Some(description),
        source_url: source_url.clone().into(),
        resources,
        persons: persons.clone(),
        ..Default::default()
    })
}

fn process_quickcheck(
    selectors: &Selectors,
    funding_info_page: &Html,
    source_url: &Url,
    persons: &[Person],
) -> Result<DatasetStub> {
    let title = "Umweltinnovationsprogramm - Förderinformationen: Quickcheck".to_owned();

    let description = collect_text(
        funding_info_page
            .select(&selectors.text_sel)
            .next()
            .ok_or_else(|| anyhow!("Missing description for quickcheck"))?
            .text(),
    );

    let questions = funding_info_page
        .select(&selectors.question_sel)
        .map(|elem| collect_text(elem.text()))
        .join("\n");

    let description = format!("{description}\n\t{questions}");

    Ok(DatasetStub {
        key: "quickcheck".to_owned(),
        title,
        description: Some(description),
        source_url: source_url.clone().into(),
        persons: persons.to_owned(),
        types: Some(Type::Text {
            text_type: TextType::Editorial,
        }),
        ..Default::default()
    })
}

fn process_funding_info(
    source: &Source,
    selectors: &Selectors,
    funding_info_page: &Html,
    source_url: &Url,
    persons: &[Person],
) -> Result<DatasetStub> {
    let title = "Umweltinnovationsprogramm - Förderinformationen".to_owned();

    let description = funding_info_page
        .select(&selectors.text_summary_sel)
        .zip(funding_info_page.select(&selectors.text_description_sel))
        .map(|(part1, part2)| {
            format!(
                "{}: {}",
                collect_text(part1.text()),
                collect_text(part2.text())
            )
        })
        .join("; ");

    let resources = funding_info_page
        .select(&selectors.funding_info_faq_que_sel)
        .map(|resource| {
            let description = collect_text(
                resource
                    .select(&selectors.funding_info_faq_ans_sel)
                    .next()
                    .ok_or_else(|| anyhow!("Missing resource description for funding info"))?
                    .text(),
            )
            .replace(" Mehr erfahren", "");

            let resource_url = funding_info_page
                .select(&selectors.funding_info_faq_links_sel)
                .next()
                .ok_or_else(|| anyhow!("Missing link element in resource"))?
                .attr("href")
                .unwrap();

            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: Some(description),
                url: source.url.join(resource_url)?.into(),
                ..Default::default()
            })
        })
        .collect::<Result<_>>()?;

    Ok(DatasetStub {
        key: "funding-info".to_owned(),
        title,
        description: Some(description),
        types: Some(Type::Text {
            text_type: TextType::Editorial,
        }),
        resources,
        persons: persons.to_owned(),
        source_url: source_url.clone().into(),
        ..Default::default()
    })
}

async fn process_easy_german(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("leichte-sprache")?;

    let description = {
        let text = client
            .fetch_text(source, "leichte-sprache".to_owned(), &url)
            .await?;
        let document = Html::parse_document(&text);

        select_text(&document, &selectors.easy_german_text)
    };

    let resources = smallvec![
        Resource {
            url: url.join("ueber-uns")?.into(),
            r#type: ResourceType::WebPage,
            description: Some(
                "Was das Umweltinnovationsprogramm über sich selbst schreibt (nicht in leichter Sprache)".to_string(),
            ),
            ..Default::default()
        },
        Resource {
            url: url.join("foerderinformationen")?.into(),
            r#type: ResourceType::WebPage,
            description: Some(
                "Wie man durch das Umweltinnovationsprogramm gefördert werden kann (nicht in leichter Sprache)".to_string(),
            ),
            ..Default::default()
        },
        Resource {
            url: url.join("projekte")?.into(),
            r#type: ResourceType::WebPage,
            description: Some(
                "Eine Liste der durch das Umweltinnovationsprogramm geförderten Projekte (nicht in leichter Sprache)"
                    .to_string(),
            ),
            ..Default::default()
        },
    ];

    let dataset = Dataset {
        title: "Umweltinnovationsprogramm: Übersicht in Leichter Sprache".to_owned(),
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::Editorial,
        }],
        resources,
        source_url: url.into(),
        origins: source.origins.clone(),
        language: Language::GermanEasy,
        license: License::AllRightsReserved,
        ..Default::default()
    };

    write_dataset(dir, client, source, "easy-german".into(), dataset).await
}

async fn process_about_us_categories(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize)> {
    let mut stubs = Vec::new();

    {
        let mut funding_priorities = Vec::new();
        let mut resources = SmallVec::new();

        stubs.push(
            process_about_us(
                source,
                client,
                selectors,
                &mut funding_priorities,
                &mut resources,
            )
            .await,
        );

        for (href, title) in funding_priorities {
            stubs.push(process_funding_priority(source, client, selectors, href, title).await);
        }
    }

    let (results, errors) = fetch_many(0, 0, stubs, |stub| async move {
        let stub = stub?;

        let dataset = Dataset {
            title: stub.title,
            description: stub.description,
            types: stub.types.into_iter().collect(),
            source_url: stub.source_url,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            resources: stub.resources,
            persons: stub.persons,
            ..Default::default()
        };

        write_dataset(dir, client, source, stub.key, dataset).await
    })
    .await;

    Ok((results, errors))
}

async fn process_about_us(
    source: &Source,
    client: &Client,
    selectors: &Selectors,
    funding_priorities: &mut Vec<(String, String)>,
    resources: &mut SmallVec<[Resource; 4]>,
) -> Result<DatasetStub> {
    let key = "ueber-uns".to_owned();
    let url = source.url.join("ueber-uns")?;

    let text = client.fetch_text(source, key.clone(), &url).await?;
    let document = Html::parse_document(&text);

    let title = "Umweltinnovationsprogramm: Über uns";

    let description = document
        .select(&selectors.about_us_text_sel)
        .zip(document.select(&selectors.about_us_more_text_sel))
        .map(|(part1, part2)| {
            format!(
                "{}: {}",
                collect_text(part1.text()),
                collect_text(part2.text())
            )
        })
        .collect::<Vec<_>>()
        .join("; ")
        .to_string();

    *funding_priorities = document
        .select(&selectors.funding_priorities_sel)
        .map(|elem| elem.attr("href").unwrap().to_owned())
        .zip(
            document
                .select(&selectors.funding_priorities_title)
                .map(|elem| elem.attr("title").unwrap().to_owned()),
        )
        .collect();

    *resources = document
        .select(&selectors.url_sel)
        .map(|element| {
            let href = element.attr("href").unwrap();
            let description = collect_text(element.text());

            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: Some(description),
                url: source.url.join(href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .collect::<Result<_>>()?;

    Ok(DatasetStub {
        key: "about-us".to_owned(),
        title: title.to_string(),
        description: Some(description),
        types: Some(Type::Text {
            text_type: TextType::Editorial,
        }),
        source_url: url.into(),
        ..Default::default()
    })
}

async fn process_funding_priority(
    source: &Source,
    client: &Client,
    selectors: &Selectors,
    href: String,
    title: String,
) -> Result<DatasetStub> {
    let url = source.url.join(&href)?;
    let key = make_key(&href).into_owned();

    let title = format!("Umweltinnovationsprogramm - Förderschwerpunkte: {}", title);

    let description;
    let resources;
    {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        description = collect_text(
            document
                .select(&selectors.desc_text_sel)
                .next()
                .unwrap()
                .text(),
        );

        resources = document
            .select(&selectors.funding_resource_sel)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = collect_text(element.text());

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<_>>()?;
    }

    Ok(DatasetStub {
        key,
        title,
        description: Some(description),
        types: Some(Type::Text {
            text_type: TextType::Editorial,
        }),
        resources,
        source_url: url.into(),
        ..Default::default()
    })
}

async fn process_events(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize)> {
    let mut next_url = Some(source.url.join("/ueber-uns/veranstaltungen?page=0")?);
    let mut stubs = Vec::new();

    while let Some(url) = next_url.take() {
        process_events_page(client, source, selectors, &url, &mut next_url, &mut stubs).await?;
    }

    let (results, errors) = fetch_many(0, 0, stubs, |stub| async move {
        let stub = stub?;

        let dataset = Dataset {
            title: stub.title,
            description: stub.description,
            types: smallvec![Type::Event],
            issued: stub.issued,
            regions: stub.region,
            source_url: stub.source_url,
            origins: source.origins.clone(),
            language: Language::German,
            license: License::AllRightsReserved,
            ..Default::default()
        };

        write_dataset(dir, client, source, stub.key, dataset).await
    })
    .await;

    Ok((results, errors))
}

async fn process_events_page(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    url: &Url,
    next_url: &mut Option<Url>,
    stubs: &mut Vec<Result<DatasetStub>>,
) -> Result<()> {
    let key = make_key(url.as_str()).into_owned();

    let text = client.fetch_text(source, key, url).await?;
    let events_page = Html::parse_document(&text);

    *next_url = if let Some(next_page_link) = events_page.select(&selectors.next_event_page).next()
    {
        let href = next_page_link.attr("href").unwrap();

        let next_url = source
            .url
            .join(&format!("/ueber-uns/veranstaltungen{}", href))?;

        Some(next_url)
    } else {
        None
    };

    stubs.extend(
        events_page
            .select(&selectors.event_sel)
            .map(|event_element| {
                let title = format!(
                    "Umweltinnovationsprogramm - Veranstaltung: {}",
                    collect_text(
                        event_element
                            .select(&selectors.event_title_text_sel)
                            .next()
                            .ok_or_else(|| anyhow!("Missing title"))?
                            .text(),
                    )
                );

                let description = Some(collect_text(
                    event_element
                        .select(&selectors.event_text_sel)
                        .next()
                        .ok_or_else(|| anyhow!("Missing description"))?
                        .text(),
                ));

                let uip_event = event_element
                    .select(&selectors.event_title_sel)
                    .next()
                    .ok_or_else(|| anyhow!("Missing event number"))?
                    .attr("id")
                    .unwrap();

                let mut source_url = url.clone();

                source_url
                    .query_pairs_mut()
                    .append_pair("uip_event", uip_event);

                let key = make_key(source_url.as_str()).into_owned();

                let event_info = collect_text(
                    event_element
                        .select(&selectors.event_info_text_sel)
                        .next()
                        .ok_or_else(|| anyhow!("Missing event info"))?
                        .text(),
                );

                let mut event_info = event_info.split('|');
                let event_date = event_info
                    .next()
                    .ok_or_else(|| anyhow!("Missing date"))?
                    .trim();
                let event_region = event_info
                    .next()
                    .ok_or_else(|| anyhow!("Missing region"))?
                    .trim();

                let event_date = Date::parse(event_date, DATE_FORMAT_EVENT)?;

                let region = smallvec![Region::Other(event_region.into())];

                Ok(DatasetStub {
                    key,
                    title,
                    description,
                    source_url: source_url.into(),
                    issued: Some(event_date),
                    region,
                    ..Default::default()
                })
            }),
    );

    Ok(())
}

async fn process_news(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize)> {
    let news_entries = {
        let url = source.url.join("ueber-uns/neuigkeiten")?;

        let text = client
            .fetch_text(source, url.to_string().replace([' ', '/'], "-"), &url)
            .await?;
        let news_page = Html::parse_document(&text);

        news_page
            .select(&selectors.news_link_sel)
            .map(|elem| elem.attr("href").unwrap().to_owned())
            .zip(
                news_page
                    .select(&selectors.news_link_sel)
                    .map(|elem| elem.attr("title").unwrap().to_owned()),
            )
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(0, 0, news_entries, |(href, title)| {
        write_news_entry(dir, client, source, selectors, href, title)
    })
    .await;

    Ok((results, errors))
}

async fn write_news_entry(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    href: String,
    title: String,
) -> Result<(usize, usize, usize)> {
    let key = href.replace([' ', '/'], "-");
    let url = source.url.join(&href)?;

    let dataset = {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let news = Html::parse_document(&text);

        let title = format!("Umweltinnovationsprogramm - Neuigkeiten : {}", title);

        let news_description = news
            .select(&selectors.desc_text_sel)
            .next()
            .ok_or_else(|| anyhow!("Missing description for news"))?;

        let description = collect_text(news_description.text());

        let mut resources = news_description
            .select(&selectors.url_sel)
            .map(|element| {
                let href = element.attr("href").unwrap();
                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: None,
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        for file in news.select(&selectors.news_downloadable_files_sel) {
            let href = file.attr("href").unwrap();

            let description = collect_text(
                file.select(&selectors.file_description_sel)
                    .next()
                    .ok_or_else(|| anyhow!("Missing resource description for news"))?
                    .text(),
            );

            resources.push(
                Resource {
                    r#type: ResourceType::Document,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::News,
            }],
            resources,
            source_url: url.into(),
            origins: source.origins.clone(),
            language: Language::German,
            license: License::AllRightsReserved,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn process_search_results(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize)> {
    let mut url = source.url.join("projekte")?;
    url.query_pairs_mut().append_key_only("search");

    let last_page = {
        let text = client
            .fetch_text(source, "projekte".to_owned(), &url)
            .await?;
        let document = Html::parse_document(&text);

        let last_page = document
            .select(&selectors.last_project_page)
            .next()
            .ok_or_else(|| anyhow!("No last page found"))?
            .value()
            .attr("href")
            .unwrap();

        last_page
            .rsplit_once('=')
            .ok_or_else(|| anyhow!("Malformed last page: {last_page}"))?
            .1
            .parse()?
    };

    let project_urls = AtomicRefCell::new(Vec::new());

    let (results, errors) = fetch_many(0, 0, 0..=last_page, |page| {
        let mut url = url.clone();
        url.query_pairs_mut().append_pair("page", &page.to_string());

        let key = format!("projekte-{page}");

        let project_urls = &project_urls;

        async move {
            let text = client.fetch_text(source, key, &url).await?;
            let document = Html::parse_document(&text);

            project_urls.borrow_mut().extend(
                document
                    .select(&selectors.project_object_sel)
                    .map(|item| item.attr("href").unwrap().to_string()),
            );

            Ok((1, 1, 0))
        }
    })
    .await;

    let (results, errors) = fetch_many(
        results,
        errors,
        project_urls.into_inner(),
        |project_url| async move {
            let key = make_key(&project_url).into_owned();

            let mut dataset = Dataset {
                types: smallvec![Type::SupportProgram],
                origins: source.origins.clone(),
                language: Language::German,
                license: License::AllRightsReserved,
                ..Default::default()
            };

            {
                let url = source.url.join(&project_url)?;

                let text = client
                    .fetch_text(source, format!("{key}.isol"), &url)
                    .await?;
                let document = Html::parse_document(&text);

                // title is filled in extract_ld_json()
                let mut description = select_text(&document, &selectors.project_description_sel)
                    .replace("Kurzbeschreibung ", "");

                for (label, item) in document
                    .select(&selectors.content_label)
                    .zip(document.select(&selectors.content_item))
                {
                    let label = collect_text(label.text());
                    let item = collect_text(item.text());

                    write!(&mut description, "\n{}: {}", label, item).unwrap();
                }

                dataset.description = Some(description);

                dataset.organisations.push(Organisation::Other {
                    name: select_text(&document, &selectors.funding_recepient),
                    role: OrganisationRole::Operator,
                    websites: Default::default(),
                });

                dataset.regions = smallvec![Region::Other(
                    select_text(&document, &selectors.funding_region).into(),
                )];

                let intervals = select_text(&document, &selectors.funding_interval_candidates);

                if !intervals.is_empty() {
                    if let Some(capture) = selectors.cont_interval_match_regex.captures(&intervals)
                    {
                        let start_year = capture[1]
                            .parse::<i32>()
                            .with_context(|| format!("Failed parsing year from {}", &intervals))?;

                        dataset.time_ranges.push(since_year(start_year)?);
                    } else if let Some(capture) =
                        selectors.interval_match_regex.captures(&intervals)
                    {
                        let start_year = capture[1].parse::<i32>().with_context(|| {
                            format!("Failed parsing start year from {}", &intervals)
                        })?;
                        let end_year = capture[2].parse::<i32>().with_context(|| {
                            format!("Failed parsing end year from {}", &intervals)
                        })?;

                        dataset
                            .time_ranges
                            .push(TimeRange::multiple_whole_years(start_year, end_year)?);
                    };
                };

                for element in document.select(&selectors.downloadable_files_sel) {
                    let href = element.attr("href").unwrap();
                    let url = source.url.join(href)?.into();

                    let description = select_text(element, &selectors.file_description_sel);

                    dataset.resources.push(
                        Resource {
                            url,
                            r#type: ResourceType::Document,
                            description: Some(description),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }

                let metadata_text = document
                    .select(&selectors.project_ld_json)
                    .next()
                    .ok_or_else(|| anyhow!("Expected ld+json in project page but not found"))
                    .unwrap()
                    .text()
                    .collect::<String>();

                extract_ld_json(&metadata_text, &mut dataset)?;
            }

            write_dataset(dir, client, source, key, dataset).await
        },
    )
    .await;

    Ok((results, errors))
}

fn extract_ld_json(ld_json_text: &str, dataset: &mut Dataset) -> Result<()> {
    let response = from_json_str::<Response>(ld_json_text)?;

    for triple in response.graph {
        match triple {
            Triple::Article {
                title,
                modified,
                issued,
                source_url,
            } => {
                dataset.title = title;
                dataset.modified = Some(Date::parse(modified, &Iso8601::DEFAULT)?);
                dataset.issued = Some(Date::parse(issued, &Iso8601::DEFAULT)?);
                dataset.source_url = source_url;
            }
            Triple::Organization { name, brand } => {
                dataset.organisations.push(Organisation::Other {
                    name,
                    role: OrganisationRole::Publisher,
                    websites: smallvec_inline![brand.url],
                });
            }
            _ => (),
        }
    }

    Ok(())
}

#[derive(Debug, Deserialize)]
struct Response<'a> {
    #[serde(rename = "@graph", borrow)]
    graph: Vec<Triple<'a>>,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "@type")]
enum Triple<'a> {
    Article {
        #[serde(rename = "name")]
        title: String,
        #[serde(rename = "dateModified")]
        modified: &'a str,
        #[serde(rename = "datePublished")]
        issued: &'a str,
        #[serde(rename = "mainEntityOfPage")]
        source_url: String,
    },
    Organization {
        name: String,
        brand: OrganizationBrand,
    },
    WebPage {},
    WebSite {},
}

#[derive(Debug, Deserialize)]
struct OrganizationBrand {
    url: String,
}

#[derive(Debug, Clone, Default)]
struct DatasetStub {
    key: String,
    title: String,
    description: Option<String>,
    types: Option<Type>,
    source_url: String,
    resources: SmallVec<[Resource; 4]>,
    persons: Vec<Person>,
    issued: Option<Date>,
    region: SmallVec<[Region; 1]>,
}

selectors! {
    page_title_sel              : "head title",
    about_us_text_sel           : "section.started.gray",
    about_us_more_text_sel      : "div.paragraph.paragraph--type--partnerteaser.paragraph--view-mode--default.has_background div.text_wrapper",
    about_us_links_sel          : "a[title^='Geförderte Projekte']",
    news_link_sel               : "a[href^='/ueber-uns/neuigkeiten']",
    news_downloadable_files_sel : "div.paragraph.paragraph--type--download.paragraph--view-mode--default a[href]",
    event_sel                   : "div.teaser_wrapper_event",
    desc_text_sel               : "div.clearfix.text-formatted.field.field--name-body.field--type-text-with-summary.field--label-hidden.field__item",
    search_result_descr_sel     : "div.clearfix.text-formatted.field.field--name-field-text.field--type-text-with-summary.field--label-hidden.field__item p",
    next_event_page             : "li.pager__item.pager__item--next.is_active a[href]",
    url_sel                     : "a[href]",
    funding_resource_sel        : "span.button_wrapper.editor.bg_green > a[href], a.button.green[href]",
    event_title_sel             : "div.event_accordion_title",
    event_text_sel              : "div.event_accordion_text",
    event_title_text_sel        : "h2.teaser_title",
    event_info_text_sel         : "div.event_accordion_title div.event_info",
    funding_priorities_sel      : "div.medium-20.small-12 div.mob a[href]",
    funding_priorities_title    :"div.medium-20.small-12 div.desk img[title]",
    funding_info_faq_que_sel    : "div.faq-question",
    funding_info_faq_ans_sel    : "div.faq-answer",
    funding_info_faq_links_sel  : "div.faq-answer a[href]",
    text_sel                    : "div.text_wrapper p",
    question_sel                : "div.q",
    faq_question_sel            : "div.question",
    faq_answer_sel              : "div.answer",
    downloadable_description_sel: "div.clearfix.text-formatted.field.field--name-field-formatiert-lang.field--type-text-long.field--label-hidden.field__item",
    downloadable_files_sel      : "div.node__content a[href^='/sites/default/']",
    text_summary_sel            : "div.clearfix.text-formatted.field.field--name-body.field--type-text-with-summary.field--label-hidden.field__item",
    file_description_sel        : "div.file_name",
    text_description_sel        : "div.clearfix.text-formatted.field.field--name-field-formatiert-lang.field--type-text-long.field--label-hidden.field__item",
    project_title_sel           : "div.flex-container.flex-wrap.justify-space-between h3",
    project_header_sel          : " div.node__content span",
    project_link_sel            : "div.button_project a[href]",
    project_description_sel     : ".text_wrapper",
    project_ld_json             : "head script",
    ul_pag_sel                  : "li.pager__item.pager__item--next.is_active a[href][title='Zur nächsten Seite']",
    content_label               : "div.text_item div.field__label",
    content_item                : "div.text_item div.field__item",
    easy_german_text            : ".field__item",
    project_object_sel          : ".button_project >a[href]",
    funding_recepient           : "div.field.field--name-field-foerdernehmer.field--type-string.field--label-above div.field__item",
    funding_region              : "div.field.field--name-field-bundesland.field--type-entity-reference.field--label-above div.field__item",
    funding_interval_candidates : ".text_item > .field__item",
    funding_interval            : "div",
    cont_interval_match_regex   : r"seit\s+(\d+)" as Regex,
    interval_match_regex        : r"(\d+)\s+\-\s+(\d+)" as Regex,
    interval_regex              : r"\d+" as Regex,
    email_sep                   : r"E\-Mail:\s+(?<email>\b\w+@[\S]+\.[\w]{2,}\b)" as Regex,
    last_project_page           : "li.pager__item.pager__item--last a[href]",
}

const DATE_FORMAT_EVENT: &[FormatItem] = format_description!("[day].[month repr:short] [year]");
