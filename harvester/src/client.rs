use std::env::var_os;
use std::future::Future;
use std::io::{Read, Write};
use std::ops::Deref;
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc,
};
use std::time::SystemTime;

use anyhow::{anyhow, ensure, Context, Error, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::{Dir, DirEntry};
use compact_str::CompactString;
use glib::Bytes as GBytes;
use hashbrown::HashMap;
use itertools::Itertools;
use parking_lot::Mutex;
use reqwest::{
    header::{HeaderMap, HeaderValue, IntoHeaderName, CONTENT_TYPE, COOKIE, SET_COOKIE},
    Client as HttpClient, RequestBuilder as HttpRequestBuilder, Response as HttpResponse,
};
use tokio::{
    sync::{Mutex as AsyncMutex, OwnedSemaphorePermit, Semaphore},
    task::{block_in_place, spawn_blocking},
    time::{interval, Duration, Interval, MissedTickBehavior},
};
use url::Url;
use xattr::FileExt;
use zstd::bulk::{decompress, Compressor};

use crate::{duplicates::Duplicates, Config, Source};
use metadaten::{
    create_new, dataset::Dataset, metrics::Metrics, open_or_create_dir,
    pdf::simplify as simplify_pdf, retry::retry_request, umthes::auto_classify::AutoClassifyClient,
    value_from_env, COMPRESSION_LEVEL,
};

pub struct BaseClient {
    http_client: HttpClient,
    auto_classify_client: Option<AutoClassifyClient>,
    pub duplicates: Duplicates,
    pub metrics: Mutex<Metrics>,
    check_source_url: bool,
    keep_stale_responses: bool,
    responses_in_flight: Arc<Semaphore>,
    host_based_rate_limits: HashMap<String, AsyncMutex<Interval>>,
    dir: Dir,
}

impl BaseClient {
    pub fn new(config: &Config, dir: &Dir) -> Result<Self> {
        let http_client = HttpClient::builder()
            .user_agent("umwelt.info harvester")
            .timeout(Duration::from_secs(300))
            .build()?;

        let auto_classify_client = AutoClassifyClient::new(&config.auto_classify, dir)?;

        let duplicates = Duplicates::new(&config.sources);

        let check_source_url = var_os("CHECK_SOURCE_URL").is_some();

        let keep_stale_responses = var_os("KEEP_STALE_RESPONSES").is_some();

        let responses_in_flight = Arc::new(Semaphore::new(value_from_env("RESPONSES_IN_FLIGHT")));

        let host_based_rate_limits = config
            .host_based_rate_limits
            .iter()
            .map(|(host, &rate_limit)| {
                let mut interval = interval(rate_limit.into());
                interval.set_missed_tick_behavior(MissedTickBehavior::Delay);
                (host.clone(), AsyncMutex::new(interval))
            })
            .collect();

        let _ = dir.create_dir("responses");
        let dir = dir.open_dir("responses")?;

        Ok(Self {
            http_client,
            auto_classify_client,
            duplicates,
            metrics: Default::default(),
            check_source_url,
            keep_stale_responses,
            responses_in_flight,
            host_based_rate_limits,
            dir,
        })
    }

    pub async fn write_tag_defs(&mut self) -> Result<()> {
        if let Some(client) = &mut self.auto_classify_client {
            client.write_tag_defs(self.metrics.get_mut()).await
        } else {
            Ok(())
        }
    }
}

pub struct Client {
    pub base: Arc<BaseClient>,
    headers: AtomicRefCell<HeaderMap>,
    concurrency: Semaphore,
    rate_limit: Option<AsyncMutex<Interval>>,
    fuse: Option<AtomicUsize>,
    dir: Arc<Dir>,
}

impl Client {
    pub fn new(base: Arc<BaseClient>, source: &Source) -> Result<Self> {
        let concurrency = Semaphore::new(source.concurrency);

        let rate_limit = source.rate_limit.map(|rate_limit| {
            let mut interval = interval(rate_limit.into());
            interval.set_missed_tick_behavior(MissedTickBehavior::Delay);
            AsyncMutex::new(interval)
        });

        let fuse = source.fuse_limit.map(|_| AtomicUsize::new(0));

        let dir = Arc::new(open_or_create_dir(&base.dir, &*source.name)?);

        Ok(Self {
            base,
            headers: AtomicRefCell::new(HeaderMap::new()),
            concurrency,
            rate_limit,
            fuse,
            dir,
        })
    }

    pub fn insert_header<K>(&self, key: K, val: HeaderValue)
    where
        K: IntoHeaderName,
    {
        self.headers.borrow_mut().insert(key, val);
    }

    pub fn append_header<K, V>(&self, key: K, val: HeaderValue)
    where
        K: IntoHeaderName,
    {
        self.headers.borrow_mut().append(key, val);
    }

    pub async fn fetch_text(
        &self,
        source: &Source,
        key: String,
        url: &Url,
    ) -> Result<TrackedResponse<String>> {
        self.make_request(source, key, Some(url), |client| async {
            client
                .get(url.clone())
                .headers(self.headers.borrow().clone())
                .send()
                .await?
                .error_for_status()?
                .text()
                .await
        })
        .await
    }

    pub async fn checked_fetch_text(
        &self,
        source: &Source,
        check: fn(&str) -> Result<()>,
        key: String,
        url: &Url,
    ) -> Result<TrackedResponse<String>> {
        self.make_request(source, key, Some(url), |client| async {
            let text = client
                .get(url.clone())
                .headers(self.headers.borrow().clone())
                .send()
                .await?
                .error_for_status()?
                .text()
                .await?;

            check(&text)?;

            Ok::<_, Error>(text)
        })
        .await
    }

    pub async fn fetch_bytes(
        &self,
        source: &Source,
        key: String,
        url: &Url,
    ) -> Result<TrackedResponse<Vec<u8>>> {
        self.make_request(source, key, Some(url), |client| async {
            client
                .get(url.clone())
                .headers(self.headers.borrow().clone())
                .send()
                .await?
                .error_for_status()?
                .bytes()
                .await
                .map(Vec::from)
        })
        .await
    }

    pub async fn checked_fetch_bytes(
        &self,
        source: &Source,
        check: fn(&[u8]) -> Result<()>,
        key: String,
        url: &Url,
    ) -> Result<TrackedResponse<Vec<u8>>> {
        self.make_request(source, key, Some(url), |client| async {
            let bytes = client
                .get(url.clone())
                .headers(self.headers.borrow().clone())
                .send()
                .await?
                .error_for_status()?
                .bytes()
                .await
                .map(Vec::from)?;

            check(&bytes)?;

            Ok::<_, Error>(bytes)
        })
        .await
    }

    pub async fn fetch_pdf(
        &self,
        source: &Source,
        key: String,
        url: &Url,
    ) -> Result<TrackedResponse<GBytes>> {
        self.make_request(source, key, Some(url), |client| async {
            let buf = client
                .get(url.clone())
                .headers(self.headers.borrow().clone())
                .send()
                .await?
                .error_for_status()?
                .bytes()
                .await?;

            let buf =
                simplify_pdf(&buf).with_context(|| format!("Failed to simplify PDF at `{url}`"))?;

            Ok::<_, Error>(GBytes::from_owned(buf))
        })
        .await
    }

    pub async fn fetch_doc(
        &self,
        source: &Source,
        key: String,
        url: &Url,
    ) -> Result<TrackedResponse<CachedDocument>> {
        self.make_request(source, key, Some(url), |client| async {
            let resp = client
                .get(url.clone())
                .headers(self.headers.borrow().clone())
                .send()
                .await?
                .error_for_status()?;

            static PDF: HeaderValue = HeaderValue::from_static("application/pdf");

            let doc = if resp.headers().get(CONTENT_TYPE) == Some(&PDF) {
                let buf = resp.bytes().await?;

                let buf = simplify_pdf(&buf)
                    .with_context(|| format!("Failed to simplify PDF at `{url}`"))?;

                CachedDocument::Pdf(GBytes::from_owned(buf))
            } else {
                let text = resp.text().await?;

                CachedDocument::Html(text)
            };

            Ok::<_, Error>(doc)
        })
        .await
    }

    pub async fn make_request<'a, A, F, T, E>(
        &'a self,
        source: &Source,
        key: String,
        source_url: Option<&Url>,
        mut action: A,
    ) -> Result<TrackedResponse<T>>
    where
        A: FnMut(&'a HttpClient) -> F,
        F: Future<Output = Result<T, E>>,
        T: Response + Send + 'static,
        E: Into<Error> + 'static,
    {
        let _permit = self
            .base
            .responses_in_flight
            .clone()
            .acquire_owned()
            .await?;

        self.check_fuse(source)?;

        if let Ok(file) = self.dir.open(&key) {
            let cache_errors = source.cache_errors;
            let source_url = source_url.filter(|_| self.base.check_source_url).cloned();

            return spawn_blocking(move || {
                let mut file = file.into_std();
                let file_len = file.metadata()?.len();

                if cache_errors && file_len == 0 {
                    if let Some(error_message) = file.get_xattr("user.error_message")? {
                        return Err(anyhow!(
                            "Cached error: {}",
                            String::from_utf8(error_message).unwrap(),
                        ));
                    }
                }

                let mut buf = Vec::with_capacity(file_len as usize);
                file.read_to_end(&mut buf)?;
                let buf = decompress(&buf, usize::MAX)?;

                let response_type = file.get_xattr("user.response_type")?;

                let response = T::from_buf(buf, response_type)?;

                if let Some(source_url) = source_url {
                    let cached_source_url = file.get_xattr("user.source_url")?;

                    if let Some(cached_source_url) = cached_source_url {
                        assert_eq!(
                            source_url.as_str(),
                            String::from_utf8(cached_source_url).unwrap(),
                            "Cached response {file:?} has unexpected source URL"
                        );
                    }
                }

                Ok(TrackedResponse { response, _permit })
            })
            .await
            .unwrap();
        }

        let response = {
            let _concurrency = self.concurrency.acquire().await?;

            self.check_fuse(source)?;

            if let Some(rate_limit) = &self.rate_limit {
                rate_limit.lock().await.tick().await;
            }

            if let Some(host) = source_url.and_then(|url| url.host_str()) {
                if let Some(rate_limit) = &self.base.host_based_rate_limits.get(host) {
                    rate_limit.lock().await.tick().await;
                }
            }

            tracing::debug!("Fetching response {key}");
            retry_request(self.fuse.as_ref(), || action(&self.base.http_client)).await
        };

        let response = match response {
            Ok(response) => response,
            Err(error) => {
                if source.cache_errors {
                    let dir = self.dir.clone();
                    let error_message = format!("{error:#}");

                    spawn_blocking(move || -> Result<()> {
                        if let Some(file) = create_new(&dir, key)? {
                            let file = file.into_std();

                            file.set_xattr("user.error_message", error_message.as_bytes())?;
                        } else {
                            tracing::debug!("Dropping already cached error");
                        }

                        Ok(())
                    })
                    .await
                    .unwrap()?;
                }

                return Err(error);
            }
        };

        let dir = self.dir.clone();
        let source_url = source_url.cloned();

        spawn_blocking(move || {
            if let Some(file) = create_new(&dir, key)? {
                let mut file = file.into_std();

                let (buf, response_type) = response.as_buf();

                let mut compressor = Compressor::new(COMPRESSION_LEVEL)?;
                compressor.include_checksum(false)?;
                compressor.include_dictid(false)?;
                let buf = compressor.compress(buf)?;

                file.write_all(&buf)?;

                if let Some(response_type) = response_type {
                    file.set_xattr("user.response_type", response_type)?;
                }

                if let Some(source_url) = source_url {
                    file.set_xattr("user.source_url", source_url.as_str().as_bytes())?;
                }
            } else {
                tracing::debug!("Dropping already cached response");
            }

            Ok(TrackedResponse { response, _permit })
        })
        .await
        .unwrap()
    }

    pub fn check_fuse(&self, source: &Source) -> Result<()> {
        if let Some(fuse_limit) = source.fuse_limit {
            let fuse = self.fuse.as_ref().unwrap().load(Ordering::Relaxed);

            ensure!(fuse < fuse_limit, "Fuse blown");
        }

        Ok(())
    }

    pub fn clean_responses(&self, source: &Source) -> Result<(usize, Duration)> {
        // All non-isolated responses are either kept or deleted
        // based on a single check against the maximum age defined by the source
        // whereas isolated responses are handled separately.
        block_in_place(move || {
            let mut deleted_isolated = 0;

            let mut delete_non_isolated = None;
            let mut age_non_isolated = Duration::ZERO;

            let check_entry = {
                let now = SystemTime::now();

                move |entry: &DirEntry| -> Result<(Duration, bool)> {
                    let modified = entry.metadata()?.modified()?.into_std();
                    let age = now.duration_since(modified)?;

                    let stale = !self.base.keep_stale_responses && age > source.max_age();

                    Ok((age, stale))
                }
            };

            for entry in self.dir.entries()? {
                let entry = entry?;

                let isolated = entry
                    .file_name()
                    .to_str()
                    .is_some_and(|file_name| file_name.ends_with(".isol"));

                if isolated {
                    let (_age, stale) = check_entry(&entry)?;

                    if stale {
                        entry.remove_file()?;
                        deleted_isolated += 1;
                    }

                    continue;
                }

                match delete_non_isolated {
                    Some(true) => entry.remove_file()?,
                    Some(false) => (),
                    None => {
                        let (age, stale) = check_entry(&entry)?;

                        if stale {
                            tracing::debug!("Deleting non-isolated responses of {}", source.name);
                            entry.remove_file()?;

                            delete_non_isolated = Some(true);
                        } else {
                            delete_non_isolated = Some(false);
                            age_non_isolated = age;
                        }
                    }
                }
            }

            if deleted_isolated != 0 {
                tracing::debug!(
                    "Deleted {} isolated responses of {}",
                    deleted_isolated,
                    source.name
                );
            }

            Ok((deleted_isolated, age_non_isolated))
        })
    }

    pub async fn update_tags(&self, dataset: &mut Dataset) -> Result<()> {
        if let Some(client) = &self.base.auto_classify_client {
            client
                .update_tags(&self.base.http_client, &self.base.metrics, dataset)
                .await
        } else {
            Ok(())
        }
    }
}

#[derive(Debug)]
pub struct TrackedResponse<T> {
    response: T,
    _permit: OwnedSemaphorePermit,
}

impl<T> Deref for TrackedResponse<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.response
    }
}

pub trait Response: Sized {
    fn from_buf(buf: Vec<u8>, response_type: Option<Vec<u8>>) -> Result<Self>;
    fn as_buf(&self) -> (&[u8], Option<&[u8]>);
}

impl Response for Vec<u8> {
    fn from_buf(buf: Vec<u8>, response_type: Option<Vec<u8>>) -> Result<Self> {
        debug_assert!(response_type.is_none());

        Ok(buf)
    }

    fn as_buf(&self) -> (&[u8], Option<&[u8]>) {
        (self, None)
    }
}

impl Response for String {
    fn from_buf(buf: Vec<u8>, response_type: Option<Vec<u8>>) -> Result<Self> {
        debug_assert!(response_type.is_none());

        let text = String::from_utf8(buf)?;

        Ok(text)
    }

    fn as_buf(&self) -> (&[u8], Option<&[u8]>) {
        (self.as_bytes(), None)
    }
}

impl Response for GBytes {
    fn from_buf(buf: Vec<u8>, response_type: Option<Vec<u8>>) -> Result<Self> {
        debug_assert!(response_type.is_none());

        Ok(GBytes::from_owned(buf))
    }

    fn as_buf(&self) -> (&[u8], Option<&[u8]>) {
        (self, None)
    }
}

#[derive(Debug)]
pub enum CachedDocument {
    Html(String),
    Pdf(GBytes),
}

impl Response for CachedDocument {
    fn from_buf(buf: Vec<u8>, response_type: Option<Vec<u8>>) -> Result<Self> {
        match response_type.as_deref() {
            None | Some(b"html") => {
                let text = String::from_utf8(buf)?;

                Ok(Self::Html(text))
            }
            Some(b"pdf") => Ok(Self::Pdf(GBytes::from_owned(buf))),
            Some(response_type) => Err(anyhow!("Unexpected response type `{response_type:x?}`")),
        }
    }

    fn as_buf(&self) -> (&[u8], Option<&[u8]>) {
        match self {
            Self::Html(text) => (text.as_bytes(), Some(b"html")),
            Self::Pdf(bytes) => (bytes, Some(b"pdf")),
        }
    }
}

pub trait HttpRequestBuilderExt {
    fn cookies(self, cookies: &HashMap<CompactString, String>) -> Self;
}

impl HttpRequestBuilderExt for HttpRequestBuilder {
    fn cookies(self, cookies: &HashMap<CompactString, String>) -> Self {
        let cookie = cookies
            .iter()
            .map(|(key, value)| format!("{key}={value}"))
            .join("; ");

        self.header(COOKIE, cookie)
    }
}

pub trait HttpResponseExt: Sized {
    fn set_cookies(self, cookies: &mut HashMap<CompactString, String>) -> Result<Self>;
}

impl HttpResponseExt for HttpResponse {
    fn set_cookies(self, cookies: &mut HashMap<CompactString, String>) -> Result<Self> {
        for header in self.headers().get_all(SET_COOKIE) {
            let (key, value) = header
                .to_str()?
                .split_once(';')
                .and_then(|(header, _)| header.split_once('='))
                .ok_or_else(|| anyhow!("Malformed cookie header"))?;

            cookies.insert(key.into(), value.to_owned());
        }

        Ok(self)
    }
}
