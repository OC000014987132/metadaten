use std::hash::{Hash, Hasher};

use anyhow::{anyhow, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::HashSet;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_text, uppercase_words},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationKey, OrganisationRole, Person,
    PersonRole, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) = fetch_links(dir, client, source, selectors).await?;

    let (count1, results1, errors1) = fetch_publications(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_press(dir, client, source, selectors, "presse").await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) =
        fetch_press(dir, client, source, selectors, "presse/presse-archiv").await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_links(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let mut results = 0;
    let mut errors = 0;

    let all_links = AtomicRefCell::new(HashSet::new());

    {
        let mut new_links = fetch_mainlinks(client, source, selectors, &all_links).await?;

        for _level in 1..=5 {
            new_links = fetch_sublinks(
                client,
                source,
                selectors,
                &mut results,
                &mut errors,
                &all_links,
                new_links,
            )
            .await;
        }
    }

    let mut all_links = all_links.into_inner();

    let page_links = fetch_page_sublinks(
        client,
        source,
        selectors,
        &mut results,
        &mut errors,
        &all_links,
    )
    .await?;

    all_links.extend(page_links);

    let count = all_links.len();

    (results, errors) = fetch_many(results, errors, all_links, |link| {
        translate_page_dataset(dir, client, source, selectors, link)
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Debug, Clone)]
struct Item {
    link: String,
    title: String,
    date: Option<Date>,
}

impl PartialEq for Item {
    fn eq(&self, other: &Self) -> bool {
        self.link == other.link
    }
}

impl Eq for Item {}

impl Hash for Item {
    fn hash<H>(&self, state: &mut H)
    where
        H: Hasher,
    {
        self.link.hash(state);
    }
}

fn filter_link(link: &str) -> bool {
    if !link.starts_with('/') && !link.starts_with("www.lanuv.nrw.de") {
        return true;
    }

    if link.starts_with("/publikationen") || link.starts_with("/presse") {
        return true;
    }

    if link.ends_with(".pdf") {
        return true;
    }

    false
}

async fn fetch_mainlinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    all_links: &AtomicRefCell<HashSet<Item>>,
) -> Result<Vec<Item>> {
    let text = client
        .fetch_text(source, "topic".to_owned(), &source.url)
        .await?;

    let document = Html::parse_document(&text);

    let mut new_links = document
        .select(&selectors.mainlink_selector)
        .filter_map(|element| {
            let link = element.attr("href").unwrap();

            if filter_link(link) {
                return None;
            }

            let title = collect_text(element.text());

            Some(Item {
                link: link.to_owned(),
                title,
                date: None,
            })
        })
        .collect::<Vec<_>>();

    let mut all_links = all_links.borrow_mut();

    // `retain` and `all_links` deduplicate `new_links` as even the initial pass can yield duplicates.
    new_links.retain(|item| all_links.insert(item.clone()));

    Ok(new_links)
}

async fn fetch_sublinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    results: &mut usize,
    errors: &mut usize,
    all_links: &AtomicRefCell<HashSet<Item>>,
    new_links: Vec<Item>,
) -> Vec<Item> {
    let sublinks = AtomicRefCell::new(Vec::new());

    (*results, *errors) = fetch_many(*results, *errors, new_links, |item| {
        let sublinks = &sublinks;

        async move {
            let key = make_key(&item.link).into_owned();

            let url = source.url.join(&item.link)?;

            let text = client.fetch_text(source, key.to_owned(), &url).await?;
            let document = Html::parse_document(&text);

            let mut all_links = all_links.borrow_mut();

            sublinks.borrow_mut().extend(
                document
                    .select(&selectors.sublink_selector)
                    .filter_map(|element| {
                        let link = element.attr("href").unwrap();

                        if filter_link(link) {
                            return None;
                        }

                        let title = collect_text(element.text());

                        Some(Item {
                            link: link.to_owned(),
                            title,
                            date: None,
                        })
                    })
                    .filter(|item| all_links.insert(item.clone())),
            );

            Ok((1, 1, 0))
        }
    })
    .await;

    sublinks.into_inner()
}

async fn fetch_page_sublinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    results: &mut usize,
    errors: &mut usize,
    items: &HashSet<Item>,
) -> Result<Vec<Item>> {
    let sublinks = AtomicRefCell::new(Vec::new());

    (*results, *errors) = fetch_many(*results, *errors, items, |item| async {
        let key = make_key(&item.link);

        let url = source.url.join(&item.link)?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let mut sublinks = sublinks.borrow_mut();

        for element in document.select(&selectors.page_link_href) {
            let link = element.attr("href").unwrap();

            if filter_link(link) {
                continue;
            }

            let title = collect_text(element.text());

            let date = document
                .select(&selectors.page_link_date)
                .next()
                .map(|element| {
                    let text = collect_text(element.text());
                    Date::parse(&text, format_description!("[day].[month].[year]"))
                })
                .transpose()?;

            sublinks.push(Item {
                link: link.to_owned(),
                title,
                date,
            });
        }

        Ok((1, 1, 0))
    })
    .await;

    Ok(sublinks.into_inner())
}

async fn translate_page_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let dataset = {
        let url = source.url.join(&item.link)?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let mut title = select_text(&document, &selectors.link_title);

        if title.is_empty() {
            title = item.title;
        }

        let description = select_text(&document, &selectors.link_description);

        let resources = document
            .select(&selectors.link_resources)
            .filter_map(|element| {
                let href = element.attr("href").unwrap();

                if href.starts_with("javascript:") {
                    return None;
                }

                let description = collect_text(element.text());

                if description.is_empty() {
                    return None;
                }

                Some((description, href))
            })
            .map(|(description, href)| {
                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        let types = if url.path().contains("/foerderprogramme") {
            smallvec![Type::SupportProgram]
        } else {
            Default::default()
        };

        let persons = document
            .select(&selectors.person)
            .map(|element| {
                let contact = collect_text(element.text());

                let contact_name = &contact.replace(".", " ");

                let contact_name = contact_name
                    .split("(at)")
                    .collect::<Vec<_>>()
                    .into_iter()
                    .next()
                    .unwrap();

                let name = uppercase_words(contact_name);
                let email = contact.replace("(at)", "@");

                Person {
                    name,
                    role: PersonRole::Contact,
                    emails: smallvec![email],
                    ..Default::default()
                }
            })
            .collect();

        let language = if url.path().starts_with("/leichte-sprache") {
            Language::GermanEasy
        } else {
            Language::German
        };

        let organisations = smallvec![Organisation::WikiData {
            identifier: OrganisationKey::LANUV,
            role: OrganisationRole::Publisher,
        }];

        Dataset {
            title,
            description: Some(description),
            persons,
            types,
            resources,
            organisations,
            issued: item.date,
            regions: smallvec![Region::NRW],
            language,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_publications(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let mut results = 0;
    let mut errors = 0;

    let mut page = 0;

    let mut next_href = fetch_publications_page(
        dir,
        client,
        source,
        selectors,
        &mut results,
        &mut errors,
        page,
        "publikationen".to_owned(),
    )
    .await?;

    while let Some(href) = next_href {
        page += 1;

        next_href = fetch_publications_page(
            dir,
            client,
            source,
            selectors,
            &mut results,
            &mut errors,
            page,
            href,
        )
        .await?;
    }

    Ok((page - 1, results, errors))
}

#[allow(clippy::too_many_arguments)]
async fn fetch_publications_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    results: &mut usize,
    errors: &mut usize,
    page: usize,
    href: String,
) -> Result<Option<String>> {
    let url = source.url.join(&href)?;

    let text = client
        .fetch_text(source, format!("publikationen-{}", page), &url)
        .await?;

    let document = Html::parse_fragment(&text);

    let items = document
        .select(&selectors.publications)
        .filter_map(|element| {
            element
                .select(&selectors.publications_links)
                .next()
                .and_then(|link| {
                    let link = link.attr("href").unwrap();

                    if link == "/publikationen/natur-in-nrw" {
                        return None;
                    }

                    let title = select_text(element, &selectors.publications_title);

                    Some(Item {
                        link: link.to_owned(),
                        title,
                        date: None,
                    })
                })
        })
        .collect::<Vec<_>>();

    let next_href = document
        .select(&selectors.publication_next_page)
        .next()
        .map(|element| element.attr("href").unwrap().to_owned());

    (*results, *errors) = fetch_many(*results, *errors, items, |item| async move {
        translate_publication_dataset(dir, client, source, selectors, item).await
    })
    .await;

    Ok(next_href)
}

async fn translate_publication_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let url = source.url.join(&item.link)?;

    let text = client
        .fetch_text(source, format!("{key}.isol"), &url)
        .await?;

    let document = Html::parse_document(&text);

    let description = select_text(&document, &selectors.link_description);

    let resources = document
        .select(&selectors.publications_download)
        .map(|element| {
            let href = element.attr("href").unwrap();
            let description = collect_text(element.text());

            let primary_content = description == "Download";

            Ok(Resource {
                r#type: ResourceType::Pdf,
                description: Some(description),
                url: source.url.join(href)?.into(),
                primary_content,
                ..Default::default()
            })
        })
        .chain(
            document
                .select(&selectors.publications_further)
                .map(|element| {
                    let href = element
                        .select(&selectors.publications_resources_link)
                        .next()
                        .ok_or_else(|| anyhow!("Missing link"))?
                        .attr("href")
                        .unwrap();

                    let description = select_text(element, &selectors.publication_resources_title);

                    Ok(Resource {
                        r#type: ResourceType::Unknown,
                        description: Some(description),
                        url: source.url.join(href)?.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                }),
        )
        .collect::<Result<SmallVec<_>>>()?;

    let organisations = smallvec![Organisation::WikiData {
        identifier: OrganisationKey::LANUV,
        role: OrganisationRole::Publisher,
    }];

    let dataset = Dataset {
        title: item.title,
        description: Some(description),
        organisations,
        resources,
        types: smallvec![Type::Text {
            text_type: TextType::Publication
        }],
        regions: smallvec![Region::NRW],
        language: Language::German,
        license: License::OtherClosed,
        origins: source.origins.clone(),
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_press(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    path: &str,
) -> Result<(usize, usize, usize)> {
    let collect_items = |document: &Html| -> Result<Vec<_>> {
        document
            .select(&selectors.press_content)
            .map(|element| {
                let link = element
                    .select(&selectors.press_link)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link"))?;

                let link = link.attr("href").unwrap().to_owned();

                let title = select_text(element, &selectors.press_title);

                let date_text = select_text(element, &selectors.press_pagination_time);
                let date = Date::parse(&date_text, format_description!("[day].[month].[year]"))?;

                Ok(Item {
                    link,
                    title,
                    date: Some(date),
                })
            })
            .collect()
    };

    let mut items;
    let further_paths;

    {
        let url = source.url.join(path)?;

        let text = client
            .fetch_text(source, path.replace("/", "-"), &url)
            .await?;

        let document = Html::parse_document(&text);

        items = collect_items(&document)?;

        further_paths = document
            .select(&selectors.press_pagination_link)
            .map(|href| href.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>();
    }

    for path in further_paths {
        let url = source.url.join(&path)?;

        let text = client
            .fetch_text(source, path.replace("/", "-"), &url)
            .await?;

        let document = Html::parse_document(&text);

        items.append(&mut collect_items(&document)?);
    }

    let count = items.len();

    let (results, errors) = fetch_many(0, 0, items, |item| {
        translate_press_dataset(dir, client, source, selectors, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_press_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();

    let dataset = {
        let url = source.url.join(&item.link)?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let description = select_text(&document, &selectors.link_description);

        let resources = document
            .select(&selectors.press_resource_link)
            .map(|element| {
                let link = element.attr("href").unwrap();

                Ok(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some("Pressemitteilung".to_owned()),
                    url: source.url.join(link)?.into(),
                    primary_content: true,
                    ..Default::default()
                })
            })
            .chain(
                document
                    .select(&selectors.press_further_resources)
                    .map(|element| {
                        let href = element.attr("href").unwrap();
                        let description = collect_text(element.text());

                        Ok(Resource {
                            r#type: ResourceType::Unknown,
                            description: Some(description),
                            url: source.url.join(href)?.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type())
                    }),
            )
            .collect::<Result<SmallVec<_>>>()?;

        let organisations = smallvec![Organisation::WikiData {
            identifier: OrganisationKey::LANUV,
            role: OrganisationRole::Publisher,
        }];

        Dataset {
            title: item.title,
            description: Some(description),
            resources,
            organisations,
            issued: item.date,
            regions: smallvec![Region::NRW],
            types: smallvec![Type::Text {
                text_type: TextType::PressRelease,
            }],
            language: Language::German,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    mainlink_selector: ".mainnav-linkgroup > a[href], .d-none a[href^='/leichte-sprache']",
    sublink_selector: "div#left ul#ulevel2 a[href], div a.stretched-link[href]:not([href*='publikationen/publikation'])",
    link_description: ".col-12.col-lg-8,
                       .mt-21.mt-lg-34 p",
    link_resources: ".card.h-100 > .card__body a[href]:not(.mail),
                     .wrapper.bg-theme .container a[href], 
                     .ce-bodytext a[href]:not(.link-email) ,
                     .card.mb-16 a[href],
                     .tx-lanuvfis .card__body a[href]",
    link_title: "h1[class='h-underline mb-0']",
    page_link_href: "div[class='produkt_text'] a[href],
    div.article.articletype-0 a[href],
    div.article.articletype-1 h1 a[href],
    div.article.articletype-2 h2 a[href]",
    page_link_date: "time",
    person: "a[data-mailto-token]",

    press_content: "div.news-list-view div.card-outer",
    press_link: "a.stretched-link[href]",
    press_title: "h5.mb-8",
    press_pagination_time: "p time",
    press_resource_link: ".link-file a[href]",
    press_pagination_link: ".pagination__item[href]",
    press_further_resources: ".news-text-wrap ul li a[href]",

    publication_next_page : "a[aria-label='Nächste Seite'][href]",
    publications: "div.card-outer",
    publications_links: "div a.stretched-link[href]",
    publications_title: "h5.mb-16",
    publications_download: "div.mt-55 div[class='d-flex flex-wrap gap-8'] a[href]:not([href^='mailto'])",
    publications_further: "div[class='card-outer card-outer_hover position-relative mb-32 h-auto theme-light-blue']:has(a[href])",
    publication_resources_title: "h5",
    publications_resources_link: "a[href]",
}
