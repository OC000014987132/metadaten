pub mod browser;
pub mod client;
pub mod duplicates;
pub mod utilities;

use std::borrow::Cow;
use std::cmp::Reverse;
use std::env::var_os;
use std::fmt;
use std::fs::read_to_string;
use std::future::Future;
use std::mem::{swap, take};
use std::time::Duration;

use anyhow::{ensure, Result};
use cap_std::fs::Dir;
use fastrand::{bool as rand_bool, u64 as rand_u64};
use futures_util::stream::{FuturesUnordered, StreamExt};
use geo::{algorithm::Contains, Rect};
use hashbrown::HashMap;
use rustc_hash::FxHashSet;
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use string_cache::DefaultAtom;
use toml::from_str;
use url::Url;

use crate::client::Client;
use metadaten::{
    create_new,
    dataset::{
        r#type::Type, Dataset, Organisation, OrganisationRole, Person, PersonRole, Region,
        Resource, Tag, TimeRange,
    },
    get_two_mut,
    umthes::auto_classify::AutoClassifyConfig,
    HumanDuration, TrimExt,
};

#[tracing::instrument(skip(dir, client, source, dataset))]
pub async fn write_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    id: String,
    mut dataset: Dataset,
) -> Result<(usize, usize, usize)> {
    ensure!(
        !id.is_empty() && !id.contains(['/', '?', '&', '#', ' ', '\t', '\n']),
        "Identifier {id} must not be empty and not contain characters which change its interpretation as a file name or URL."
    );

    dataset.title = dataset.title.trim();

    ensure!(!dataset.title.is_empty(), "Dataset {id} has empty title");

    ensure!(!dataset.origins.is_empty(), "Dataset {id} has no origins");

    dataset.persons.retain(|person| {
        if person.name.is_empty() {
            tracing::error!("Dropping person with empty name");
            false
        } else {
            true
        }
    });

    dataset.organisations.retain(|organisation| {
        if organisation.name().is_empty() {
            tracing::error!("Dropping organisation with empty name");
            false
        } else {
            true
        }
    });

    for bounding_box in &dataset.bounding_boxes {
        let min = bounding_box.min();
        let max = bounding_box.max();

        ensure!(
            !(min.x.is_nan() || min.y.is_nan() || max.x.is_nan() || max.y.is_nan()),
            "Dataset {id} has NaN coordinates in a bounding box"
        );
    }

    let file = match create_new(dir, &id)? {
        Some(file) => file,
        None => {
            tracing::debug!("Dropping identical dataset");
            return Ok((1, 0, 0));
        }
    };

    if let Some(description) = &mut dataset.description {
        *description = take(description).trim();

        if description.is_empty() || description == &dataset.title {
            dataset.description = None;
        }
    }

    for r#type in &dataset.types {
        r#type.with_tags(|tag| dataset.tags.push(tag));
    }

    dataset.tags = dataset.tags.into_iter().filter_map(Tag::align).collect();

    if let Err(err) = client.update_tags(&mut dataset).await {
        tracing::warn!("Failed to update UMTHES-based tags for dataset: {:#}", err);
    }

    dataset.regions = dataset
        .regions
        .into_iter()
        .filter_map(Region::align)
        .collect();

    if dataset.time_ranges.is_empty() {
        // If no time-like extent is given, try to infer one based on `issued` and `modified`.
        let time_range = match (dataset.issued, dataset.modified) {
            (Some(issued), Some(modified)) => suppress_errors(|| Some((issued, modified).into())),
            (Some(issued), None) => Some(issued.into()),
            (None, Some(modified)) => Some(modified.into()),
            (None, None) => None,
        };

        if let Some(time_range) = time_range {
            dataset.time_ranges.push(time_range);
        }
    }

    dataset.resources.iter_mut().for_each(Resource::align);

    dataset.organisations = dataset
        .organisations
        .into_iter()
        .filter_map(Organisation::align)
        .collect();

    postprocess_dataset(&mut dataset);

    client
        .base
        .duplicates
        .add_dataset(source.name.clone(), id, &dataset);

    dataset.async_write(file).await?;

    Ok((1, 1, 0))
}

fn postprocess_dataset(dataset: &mut Dataset) {
    dataset.tags.sort_unstable();
    dataset.tags.dedup();

    dataset
        .tags
        .sort_unstable_by_key(|tag| Reverse(tag.depth()));

    dedup_types(&mut dataset.types);

    dedup_organisations(&mut dataset.organisations);
    dedup_persons(&mut dataset.persons);

    dedup_resources(&mut dataset.resources);

    dataset.regions.sort_unstable_by(|lhs, rhs| rhs.cmp(lhs));
    dataset.regions.dedup();

    refine_bounding_boxes(&mut dataset.bounding_boxes, &dataset.regions);

    simplify_time_ranges(&mut dataset.time_ranges);

    dataset.alternatives.sort_unstable();
    dataset.alternatives.dedup();
}

fn refine_bounding_boxes(bounding_boxes: &mut SmallVec<[Rect; 1]>, regions: &[Region]) {
    for region in regions {
        if let Some(region_bounding_box) = region.bounding_box() {
            // Only add region-based bounding boxes, for example those of the manually attached Länder,
            // if they would not swallow a point-like bounding box or anyway be tightened by another bounding box after refinement.
            if !bounding_boxes
                .iter()
                .any(|bounding_box| region_bounding_box.contains(bounding_box))
            {
                bounding_boxes.push(region_bounding_box);
            }
        }
    }

    // If any two bounding boxes are contained in each other, keep only the smallest,
    // except if a bounding box is point-like, then drop it if it is contained in any other.
    let mut base_idx = 0;

    'base: while base_idx < bounding_boxes.len() {
        let base = &bounding_boxes[base_idx];
        if base.min() != base.max() {
            let mut last_idx = base_idx;

            for curr_idx in (last_idx + 1)..bounding_boxes.len() {
                let (base, curr) = get_two_mut(bounding_boxes, base_idx, curr_idx);

                if curr.contains(base) {
                    // `curr` is larger than `base`, hence we drop `curr`
                } else if base.contains(curr) {
                    if curr.min() != curr.max() {
                        // `base` is larger than `curr`, but `curr` is not point-like
                        // hence we drop `base` and keep `curr` instead
                        *base = *curr;
                    }
                } else {
                    // unrelated, i.e. keep both
                    last_idx += 1;
                    bounding_boxes[last_idx] = *curr;
                }
            }

            bounding_boxes.truncate(last_idx + 1);
        } else {
            // `base` is point-like, hence keep it only
            // if there is no other bounding box which contains it
            for curr_idx in (base_idx + 1)..bounding_boxes.len() {
                let curr = &bounding_boxes[curr_idx];

                if curr.contains(base) {
                    bounding_boxes[base_idx] = *curr;
                    bounding_boxes.swap_remove(curr_idx);
                    continue 'base;
                }
            }
        }

        base_idx += 1;
    }
}

fn simplify_time_ranges(time_ranges: &mut SmallVec<[TimeRange; 1]>) {
    time_ranges.sort_unstable();

    let mut last_idx = 0;

    for curr_idx in 1..time_ranges.len() {
        let (last, curr) = get_two_mut(time_ranges, last_idx, curr_idx);

        if last.until.next_day().unwrap() >= curr.from {
            last.until = last.until.max(curr.until);
        } else {
            last_idx += 1;
            time_ranges[last_idx] = *curr;
        }
    }

    time_ranges.truncate(last_idx + 1);
}

fn dedup_types(types: &mut SmallVec<[Type; 1]>) {
    types.sort_unstable();

    let mut last_idx = 0;

    for curr_idx in 1..types.len() {
        let (last, curr) = get_two_mut(types, last_idx, curr_idx);

        match (last, curr) {
            (
                Type::Measurements {
                    domain: last_domain,
                    station: None,
                    measured_variables: last_measured_variables,
                    methods: last_methods,
                },
                Type::Measurements {
                    domain: curr_domain,
                    station: None,
                    measured_variables: curr_measured_variables,
                    methods: curr_methods,
                },
            ) if last_domain == curr_domain => {
                let existing = last_measured_variables.iter().collect::<FxHashSet<_>>();
                curr_measured_variables
                    .retain(|measured_variable| !existing.contains(measured_variable));
                last_measured_variables.append(curr_measured_variables);

                let existing = last_methods.iter().collect::<FxHashSet<_>>();
                curr_methods.retain(|method| !existing.contains(method));
                last_methods.append(curr_methods);
            }
            (
                Type::Measurements {
                    domain: last_domain,
                    station: Some(last_station),
                    measured_variables: last_measured_variables,
                    methods: last_methods,
                },
                Type::Measurements {
                    domain: curr_domain,
                    station: Some(curr_station),
                    measured_variables: curr_measured_variables,
                    methods: curr_methods,
                },
            ) if last_domain == curr_domain && last_station.id == curr_station.id => {
                if last_station.measurement_frequency.is_none() {
                    last_station.measurement_frequency =
                        take(&mut curr_station.measurement_frequency);
                }

                last_station
                    .reporting_obligations
                    .append(&mut curr_station.reporting_obligations);
                last_station.reporting_obligations.sort_unstable();
                last_station.reporting_obligations.dedup();

                let existing = last_measured_variables.iter().collect::<FxHashSet<_>>();
                curr_measured_variables
                    .retain(|measured_variable| !existing.contains(measured_variable));
                last_measured_variables.append(curr_measured_variables);

                let existing = last_methods.iter().collect::<FxHashSet<_>>();
                curr_methods.retain(|method| !existing.contains(method));
                last_methods.append(curr_methods);
            }
            (last, curr) if last == curr => (),
            _ => {
                last_idx += 1;
                types.swap(last_idx, curr_idx);
            }
        }
    }

    types.truncate(last_idx + 1);
}

fn dedup_organisations(organisations: &mut SmallVec<[Organisation; 2]>) {
    organisations
        .sort_unstable_by(|lhs, rhs| (lhs.name(), lhs.role()).cmp(&(rhs.name(), rhs.role())));

    let mut last_idx = 0;

    for curr_idx in 1..organisations.len() {
        let (last, curr) = get_two_mut(organisations, last_idx, curr_idx);

        // `Unknown` is the smallest `OrganisationRole`, i.e. will sort first if present.
        if last.name() == curr.name()
            && (last.role() == curr.role() || last.role() == OrganisationRole::Unknown)
        {
            last.merge(curr);
        } else {
            last_idx += 1;
            organisations.swap(last_idx, curr_idx);
        }
    }

    organisations.truncate(last_idx + 1);
}

fn dedup_persons(persons: &mut Vec<Person>) {
    persons.sort_unstable_by(|lhs, rhs| (&lhs.name, lhs.role).cmp(&(&rhs.name, rhs.role)));

    let mut last_idx = 0;

    for curr_idx in 1..persons.len() {
        let (last, curr) = get_two_mut(persons, last_idx, curr_idx);

        // `Unknown` is the smallest `PersonRole`, i.e. will sort first if present.
        if last.name == curr.name && (last.role == curr.role || last.role == PersonRole::Unknown) {
            last.role = curr.role;

            last.emails.extend(take(&mut curr.emails));
            last.emails.sort_unstable();
            last.emails.dedup();

            last.forms.extend(take(&mut curr.forms));
            last.forms.sort_unstable();
            last.forms.dedup();
        } else {
            last_idx += 1;
            persons.swap(last_idx, curr_idx);
        }
    }

    persons.truncate(last_idx + 1);
}

fn dedup_resources(resources: &mut SmallVec<[Resource; 4]>) {
    resources.sort_unstable_by(|lhs, rhs| (&lhs.url, lhs.r#type).cmp(&(&rhs.url, rhs.r#type)));

    let mut last_idx = 0;

    for curr_idx in 1..resources.len() {
        let (last, curr) = get_two_mut(resources, last_idx, curr_idx);

        // `Unknown` is the smallest `ResourceType`, i.e. will sort first if present.
        if last.url == curr.url {
            last.r#type = curr.r#type;

            let last_len = last.description.as_deref().map_or(0, str::len);
            let curr_len = curr.description.as_deref().map_or(0, str::len);
            if last_len < curr_len {
                swap(&mut last.description, &mut curr.description);
            }
        } else {
            last_idx += 1;
            resources.swap(last_idx, curr_idx);
        }
    }

    resources.truncate(last_idx + 1);
}

pub async fn remove_datasets<K>(
    dir: &Dir,
    client: &Client,
    source: &Source,
    keys: &[K],
) -> Result<Vec<Dataset>>
where
    K: AsRef<str>,
{
    let mut datasets = Vec::with_capacity(keys.len());

    let mut dataset;
    let mut buf = Vec::new();

    for key in keys {
        let key = key.as_ref();
        let file = dir.open(key)?;

        (dataset, buf) = Dataset::async_read_with(file, buf).await?;

        dir.remove_file(key)?;
        client
            .base
            .duplicates
            .remove_dataset(&source.name, key, &dataset);

        datasets.push(dataset);
    }

    Ok(datasets)
}

pub async fn fetch_many<R, T, M, F>(
    mut results: usize,
    mut errors: usize,
    requests: R,
    make_request: M,
) -> (usize, usize)
where
    R: IntoIterator<Item = T>,
    M: Fn(T) -> F,
    F: Future<Output = Result<(usize, usize, usize)>>,
{
    let mut scheduler = FuturesUnordered::new();

    let mut requests = requests.into_iter().map(make_request);

    for request in requests.by_ref().take(32) {
        scheduler.push(request);
    }

    while let Some(response) = scheduler.next().await {
        match response {
            Ok((_count, results1, errors1)) => {
                results += results1;
                errors += errors1;
            }
            Err(err) => {
                tracing::error!("{:#}", err);

                errors += 1;
            }
        }

        if let Some(request) = requests.next() {
            scheduler.push(request);
        }
    }

    (results, errors)
}

pub async fn try_fetch<F>(count: &mut usize, results: &mut usize, errors: &mut usize, request: F)
where
    F: Future<Output = Result<(usize, usize, usize)>>,
{
    match request.await {
        Ok((count1, results1, errors1)) => {
            *count += count1;
            *results += results1;
            *errors += errors1;
        }
        Err(err) => {
            tracing::warn!("{:#}", err);

            *errors += 1;
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    #[serde(default)]
    pub host_based_rate_limits: HashMap<String, HumanDuration>,
    #[serde(default = "default_keep_failed_sources")]
    pub keep_failed_sources: bool,
    #[serde(default = "default_merge_duplicates")]
    pub merge_duplicates: bool,
    #[serde(default)]
    pub auto_classify: AutoClassifyConfig,
    pub sources: Vec<Source>,
}

fn default_keep_failed_sources() -> bool {
    true
}

fn default_merge_duplicates() -> bool {
    true
}

impl Config {
    pub fn read() -> Result<Self> {
        let path = var_os("CONFIG_PATH").expect("Environment variable CONFIG_PATH not set");

        let val = from_str::<Self>(&read_to_string(path)?)?;

        Ok(val)
    }
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Source {
    pub name: DefaultAtom,
    pub r#type: String,
    pub url: Url,
    pub origins: SmallVec<[DefaultAtom; 1]>,
    pub filter: Option<String>,
    pub source_url: Option<String>,
    #[serde(default)]
    tier: u8,
    #[serde(default)]
    primary: bool,
    #[serde(default)]
    pub not_indexed: bool,
    #[serde(default = "default_max_age")]
    max_age: HumanDuration,
    #[serde(default = "default_age_smear")]
    age_smear: HumanDuration,
    #[serde(default = "default_concurrency")]
    concurrency: usize,
    #[serde(default = "default_batch_size")]
    pub batch_size: usize,
    rate_limit: Option<HumanDuration>,
    fuse_limit: Option<usize>,
    #[serde(default)]
    cache_errors: bool,
}

fn default_max_age() -> HumanDuration {
    HumanDuration(Duration::from_secs(24 * 60 * 60))
}

fn default_age_smear() -> HumanDuration {
    HumanDuration(Duration::ZERO)
}

fn default_concurrency() -> usize {
    1
}

fn default_batch_size() -> usize {
    100
}

impl Source {
    pub fn source_url(&self) -> &str {
        self.source_url
            .as_deref()
            .unwrap_or_else(|| self.url.as_str())
    }

    pub fn datasets_dir_name(&self) -> Cow<'_, str> {
        if self.not_indexed {
            Cow::Owned(format!("{}.not_indexed", self.name))
        } else {
            Cow::Borrowed(&*self.name)
        }
    }

    pub fn max_age(&self) -> Duration {
        let age_smear = Duration::from_secs(rand_u64(0..=self.age_smear.0.as_secs()));

        let max_age = if rand_bool() {
            self.max_age.0 + age_smear
        } else {
            self.max_age.0 - age_smear
        };

        // Reduce maximum age by 12h to handle slack and jitter of the harvester schedule.
        max_age.saturating_sub(Duration::from_secs(12 * 60 * 60))
    }
}

impl fmt::Debug for Source {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.debug_struct("Source")
            .field("name", &self.name.as_ref())
            .finish_non_exhaustive()
    }
}

pub fn suppress_errors<F, T>(f: F) -> T
where
    F: FnOnce() -> T,
{
    tracing::error_span!("suppress_errors").in_scope(f)
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Context;
    use geo::Coord;
    use hashbrown::HashSet;
    use regex::Regex;
    use smallvec::smallvec;
    use tantivy::schema::Facet;
    use time::macros::date;

    use metadaten::dataset::{
        r#type::{Domain, Station, TextType},
        ResourceType,
    };

    #[test]
    fn refine_bounding_boxes_tightens_nested() {
        let mut bounding_boxes = smallvec![
            Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 1.0, y: 1.0 }),
            Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 1.0, y: 1.0 }),
            Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 2.0, y: 2.0 }),
            Rect::new(Coord { x: 3.0, y: 3.0 }, Coord { x: 4.0, y: 4.0 }),
        ];
        refine_bounding_boxes(&mut bounding_boxes, &[]);
        assert_eq!(
            &bounding_boxes[..],
            [
                Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 1.0, y: 1.0 }),
                Rect::new(Coord { x: 3.0, y: 3.0 }, Coord { x: 4.0, y: 4.0 }),
            ],
        );
    }

    #[test]
    fn refine_bounding_boxes_keeps_separate_points() {
        let mut bounding_boxes = smallvec![
            Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 1.0, y: 1.0 }),
            Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 2.0, y: 2.0 }),
            Rect::new(Coord { x: 1.0, y: 1.0 }, Coord { x: 1.0, y: 1.0 }),
            Rect::new(Coord { x: 3.0, y: 3.0 }, Coord { x: 3.0, y: 3.0 }),
        ];
        refine_bounding_boxes(&mut bounding_boxes, &[]);
        assert_eq!(
            &bounding_boxes[..],
            [
                Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 1.0, y: 1.0 }),
                Rect::new(Coord { x: 3.0, y: 3.0 }, Coord { x: 3.0, y: 3.0 }),
            ],
        );

        let mut bounding_boxes = smallvec![
            Rect::new(Coord { x: 1.0, y: 1.0 }, Coord { x: 1.0, y: 1.0 }),
            Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 1.0, y: 1.0 }),
            Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 2.0, y: 2.0 }),
            Rect::new(Coord { x: 3.0, y: 3.0 }, Coord { x: 3.0, y: 3.0 }),
        ];
        refine_bounding_boxes(&mut bounding_boxes, &[]);
        assert_eq!(
            &bounding_boxes[..],
            [
                Rect::new(Coord { x: 0.0, y: 0.0 }, Coord { x: 1.0, y: 1.0 }),
                Rect::new(Coord { x: 3.0, y: 3.0 }, Coord { x: 3.0, y: 3.0 }),
            ],
        );
    }

    #[test]
    fn simplify_time_ranges_removes_duplicate_time_ranges() {
        let mut time_ranges = SmallVec::new();
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(&*time_ranges, []);

        let mut time_ranges = smallvec![TimeRange {
            from: date!(2024 - 01 - 01),
            until: date!(2024 - 01 - 02),
        }];
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(
            &*time_ranges,
            [TimeRange {
                from: date!(2024 - 01 - 01),
                until: date!(2024 - 01 - 02),
            }]
        );

        let mut time_ranges = smallvec![
            TimeRange {
                from: date!(2024 - 01 - 01),
                until: date!(2024 - 01 - 02),
            },
            TimeRange {
                from: date!(2024 - 01 - 05),
                until: date!(2024 - 01 - 07),
            },
        ];
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(
            &*time_ranges,
            [
                TimeRange {
                    from: date!(2024 - 01 - 01),
                    until: date!(2024 - 01 - 02),
                },
                TimeRange {
                    from: date!(2024 - 01 - 05),
                    until: date!(2024 - 01 - 07),
                },
            ]
        );

        let mut time_ranges = smallvec![
            TimeRange {
                from: date!(2024 - 01 - 01),
                until: date!(2024 - 01 - 02),
            },
            TimeRange {
                from: date!(2024 - 01 - 05),
                until: date!(2024 - 01 - 07),
            },
            TimeRange {
                from: date!(2024 - 01 - 01),
                until: date!(2024 - 01 - 02),
            },
        ];
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(
            &*time_ranges,
            [
                TimeRange {
                    from: date!(2024 - 01 - 01),
                    until: date!(2024 - 01 - 02),
                },
                TimeRange {
                    from: date!(2024 - 01 - 05),
                    until: date!(2024 - 01 - 07),
                },
            ]
        );
    }

    #[test]
    fn simplify_time_ranges_merges_adjacent_time_ranges() {
        let mut time_ranges = smallvec![
            TimeRange {
                from: date!(2024 - 01 - 01),
                until: date!(2024 - 01 - 02),
            },
            TimeRange {
                from: date!(2024 - 01 - 04),
                until: date!(2024 - 01 - 05),
            },
            TimeRange {
                from: date!(2024 - 01 - 03),
                until: date!(2024 - 01 - 04),
            },
            TimeRange {
                from: date!(2024 - 01 - 02),
                until: date!(2024 - 01 - 03),
            },
            TimeRange {
                from: date!(2024 - 01 - 10),
                until: date!(2024 - 01 - 15),
            },
        ];
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(
            &*time_ranges,
            [
                TimeRange {
                    from: date!(2024 - 01 - 01),
                    until: date!(2024 - 01 - 05),
                },
                TimeRange {
                    from: date!(2024 - 01 - 10),
                    until: date!(2024 - 01 - 15),
                },
            ]
        );

        let mut time_ranges = smallvec![
            TimeRange {
                from: date!(2009 - 01 - 01),
                until: date!(2009 - 12 - 31),
            },
            TimeRange {
                from: date!(2011 - 01 - 01),
                until: date!(2011 - 12 - 31),
            },
            TimeRange {
                from: date!(2010 - 01 - 01),
                until: date!(2010 - 12 - 31),
            },
        ];
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(
            &*time_ranges,
            [TimeRange {
                from: date!(2009 - 01 - 01),
                until: date!(2011 - 12 - 31),
            }]
        );
    }

    #[test]
    fn simplify_time_ranges_merged_overlapping_time_ranges() {
        let mut time_ranges = smallvec![
            TimeRange {
                from: date!(2024 - 01 - 01),
                until: date!(2024 - 01 - 03),
            },
            TimeRange {
                from: date!(2024 - 01 - 02),
                until: date!(2024 - 01 - 05),
            },
            TimeRange {
                from: date!(2024 - 01 - 10),
                until: date!(2024 - 01 - 15),
            },
        ];
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(
            &*time_ranges,
            [
                TimeRange {
                    from: date!(2024 - 01 - 01),
                    until: date!(2024 - 01 - 05),
                },
                TimeRange {
                    from: date!(2024 - 01 - 10),
                    until: date!(2024 - 01 - 15),
                },
            ]
        );

        let mut time_ranges = smallvec![
            TimeRange {
                from: date!(2024 - 01 - 01),
                until: date!(2024 - 01 - 05),
            },
            TimeRange {
                from: date!(2024 - 01 - 02),
                until: date!(2024 - 01 - 03),
            },
            TimeRange {
                from: date!(2024 - 01 - 10),
                until: date!(2024 - 01 - 15),
            },
        ];
        simplify_time_ranges(&mut time_ranges);
        assert_eq!(
            &*time_ranges,
            [
                TimeRange {
                    from: date!(2024 - 01 - 01),
                    until: date!(2024 - 01 - 05),
                },
                TimeRange {
                    from: date!(2024 - 01 - 10),
                    until: date!(2024 - 01 - 15),
                },
            ]
        );
    }

    #[test]
    fn dedup_types_works() {
        let type_measurement_1 = Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some("1234".into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasser".into(), "Leitfähigkeit".into()],
            methods: smallvec!["Messung".into(), "Simulation".into()],
        };
        let type_measurement_2 = Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some("1234".into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Temperatur".into(), "Leitfähigkeit".into()],
            methods: smallvec!["Simulation".into(), "Schätzung".into()],
        };
        let type_measurement_3 = Type::Measurements {
            domain: Domain::Air,
            station: Some(Station {
                id: Some("1234".into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Partikeldichte".into()],
            methods: smallvec!["Messung".into(), "Simulation".into()],
        };
        let type_measurement_4 = Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some("5678".into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasser".into(), "Leitfähigkeit".into()],
            methods: smallvec!["Simulation".into(), "Schätzung".into()],
        };
        let type_measurement_12 = Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some("1234".into()),
                ..Default::default()
            }),
            measured_variables: smallvec![
                "Grundwasser".into(),
                "Leitfähigkeit".into(),
                "Temperatur".into(),
            ],
            methods: smallvec!["Messung".into(), "Simulation".into(), "Schätzung".into()],
        };

        let mut types = smallvec![
            type_measurement_1.clone(),
            type_measurement_2,
            type_measurement_1,
            type_measurement_3.clone(),
            type_measurement_3.clone(),
            type_measurement_4.clone(),
        ];
        dedup_types(&mut types);
        assert_eq!(
            &types[..],
            &[type_measurement_3, type_measurement_12, type_measurement_4]
        );

        let type_text_1 = Type::Text {
            text_type: TextType::Editorial,
        };
        let type_text_2 = Type::Text {
            text_type: TextType::Publication,
        };
        let mut types = smallvec![
            type_text_1.clone(),
            type_text_1.clone(),
            type_text_2.clone(),
            type_text_1.clone(),
        ];
        dedup_types(&mut types);
        assert_eq!(&types[..], &[type_text_2, type_text_1]);
    }

    #[test]
    fn dedup_organisations_merges_by_name_and_role() {
        let mut organisations = smallvec![
            Organisation::Other {
                name: "foo".to_owned(),
                role: OrganisationRole::Publisher,
                websites: smallvec!["foo1".to_owned()],
            },
            Organisation::Other {
                name: "foo".to_owned(),
                role: OrganisationRole::Publisher,
                websites: smallvec!["foo2".to_owned()],
            },
            Organisation::Other {
                name: "foo".to_owned(),
                role: OrganisationRole::Organiser,
                websites: smallvec!["foo2".to_owned()],
            },
            Organisation::Other {
                name: "bar".to_owned(),
                role: OrganisationRole::Operator,
                websites: smallvec!["bar1".to_owned()],
            },
            Organisation::Other {
                name: "bar".to_owned(),
                role: OrganisationRole::Management,
                websites: smallvec!["bar2".to_owned()],
            },
        ];
        dedup_organisations(&mut organisations);
        assert_eq!(
            &organisations[..],
            &[
                Organisation::Other {
                    name: "bar".to_owned(),
                    role: OrganisationRole::Operator,
                    websites: smallvec!["bar1".to_owned()]
                },
                Organisation::Other {
                    name: "bar".to_owned(),
                    role: OrganisationRole::Management,
                    websites: smallvec!["bar2".to_owned()]
                },
                Organisation::Other {
                    name: "foo".to_owned(),
                    role: OrganisationRole::Publisher,
                    websites: smallvec!["foo1".to_owned(), "foo2".to_owned()]
                },
                Organisation::Other {
                    name: "foo".to_owned(),
                    role: OrganisationRole::Organiser,
                    websites: smallvec!["foo2".to_owned()]
                },
            ]
        );
    }

    #[test]
    fn dedup_organisations_merges_unknown_into_other() {
        let mut organisations = smallvec![
            Organisation::Other {
                name: "foo".to_owned(),
                role: OrganisationRole::Unknown,
                websites: smallvec!["foo1".to_owned()],
            },
            Organisation::Other {
                name: "foo".to_owned(),
                role: OrganisationRole::Unknown,
                websites: smallvec!["foo2".to_owned()],
            },
            Organisation::Other {
                name: "foo".to_owned(),
                role: OrganisationRole::Publisher,
                websites: smallvec!["foo2".to_owned()],
            },
            Organisation::Other {
                name: "bar".to_owned(),
                role: OrganisationRole::Unknown,
                websites: smallvec!["bar1".to_owned()],
            },
            Organisation::Other {
                name: "bar".to_owned(),
                role: OrganisationRole::Unknown,
                websites: smallvec!["bar2".to_owned()],
            },
        ];
        dedup_organisations(&mut organisations);
        assert_eq!(
            &organisations[..],
            &[
                Organisation::Other {
                    name: "bar".to_owned(),
                    role: OrganisationRole::Unknown,
                    websites: smallvec!["bar1".to_owned(), "bar2".to_owned()]
                },
                Organisation::Other {
                    name: "foo".to_owned(),
                    role: OrganisationRole::Publisher,
                    websites: smallvec!["foo1".to_owned(), "foo2".to_owned()]
                },
            ]
        );
    }

    #[test]
    fn dedup_persons_merges_by_name_and_role() {
        let mut persons = vec![
            Person {
                name: "foo".to_owned(),
                role: PersonRole::Contributor,
                emails: smallvec!["foo1".to_owned()],
                forms: smallvec!["qux1".to_owned()],
            },
            Person {
                name: "foo".to_owned(),
                role: PersonRole::Contributor,
                emails: smallvec!["foo2".to_owned()],
                forms: smallvec!["qux2".to_owned()],
            },
            Person {
                name: "foo".to_owned(),
                role: PersonRole::Contact,
                emails: smallvec!["foo2".to_owned()],
                forms: smallvec!["qux2".to_owned()],
            },
            Person {
                name: "bar".to_owned(),
                role: PersonRole::Creator,
                emails: smallvec!["bar1".to_owned()],
                forms: smallvec!["baz1".to_owned()],
            },
            Person {
                name: "bar".to_owned(),
                role: PersonRole::Manager,
                emails: smallvec!["bar2".to_owned()],
                forms: smallvec!["baz2".to_owned()],
            },
        ];
        dedup_persons(&mut persons);
        assert_eq!(
            persons,
            [
                Person {
                    name: "bar".to_owned(),
                    role: PersonRole::Creator,
                    emails: smallvec!["bar1".to_owned()],
                    forms: smallvec!["baz1".to_owned()],
                },
                Person {
                    name: "bar".to_owned(),
                    role: PersonRole::Manager,
                    emails: smallvec!["bar2".to_owned()],
                    forms: smallvec!["baz2".to_owned()],
                },
                Person {
                    name: "foo".to_owned(),
                    role: PersonRole::Contact,
                    emails: smallvec!["foo2".to_owned()],
                    forms: smallvec!["qux2".to_owned()],
                },
                Person {
                    name: "foo".to_owned(),
                    role: PersonRole::Contributor,
                    emails: smallvec!["foo1".to_owned(), "foo2".to_owned()],
                    forms: smallvec!["qux1".to_owned(), "qux2".to_owned()],
                },
            ]
        );
    }

    #[test]
    fn dedup_persons_merges_unknown_into_other() {
        let mut persons = vec![
            Person {
                name: "foo".to_owned(),
                role: PersonRole::Unknown,
                emails: smallvec!["foo1".to_owned()],
                forms: smallvec!["qux1".to_owned()],
            },
            Person {
                name: "foo".to_owned(),
                role: PersonRole::Unknown,
                emails: smallvec!["foo2".to_owned()],
                forms: smallvec!["qux2".to_owned()],
            },
            Person {
                name: "foo".to_owned(),
                role: PersonRole::Contact,
                emails: smallvec!["foo2".to_owned()],
                forms: smallvec!["qux2".to_owned()],
            },
            Person {
                name: "bar".to_owned(),
                role: PersonRole::Unknown,
                emails: smallvec!["bar1".to_owned()],
                forms: smallvec!["baz1".to_owned()],
            },
            Person {
                name: "bar".to_owned(),
                role: PersonRole::Unknown,
                emails: smallvec!["bar2".to_owned()],
                forms: smallvec!["baz2".to_owned()],
            },
        ];
        dedup_persons(&mut persons);
        assert_eq!(
            persons,
            [
                Person {
                    name: "bar".to_owned(),
                    role: PersonRole::Unknown,
                    emails: smallvec!["bar1".to_owned(), "bar2".to_owned()],
                    forms: smallvec!["baz1".to_owned(), "baz2".to_owned()],
                },
                Person {
                    name: "foo".to_owned(),
                    role: PersonRole::Contact,
                    emails: smallvec!["foo1".to_owned(), "foo2".to_owned()],
                    forms: smallvec!["qux1".to_owned(), "qux2".to_owned()],
                },
            ]
        );
    }

    #[test]
    fn dedup_resources_works() {
        let mut resources = smallvec![
            Resource {
                r#type: ResourceType::Unknown,
                description: Some("foo12".to_owned()),
                url: "foo".to_owned(),
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::Unknown,
                description: Some("bar1".to_owned()),
                url: "bar".to_owned(),
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::Document,
                description: Some("foo1".to_owned()),
                url: "foo".to_owned(),
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::Pdf,
                description: Some("foo123".to_owned()),
                url: "foo".to_owned(),
                ..Default::default()
            },
        ];
        dedup_resources(&mut resources);
        assert_eq!(
            &resources[..],
            &[
                Resource {
                    r#type: ResourceType::Unknown,
                    description: Some("bar1".to_owned()),
                    url: "bar".to_owned(),
                    ..Default::default()
                },
                Resource {
                    r#type: ResourceType::Pdf,
                    description: Some("foo123".to_owned()),
                    url: "foo".to_owned(),
                    ..Default::default()
                },
            ]
        );
    }

    #[test]
    fn validate_configuration() -> Result<()> {
        let config = from_str::<Config>(&read_to_string("../deployment/harvester.toml")?)?;

        let source_name_format = Regex::new(r"^[a-z][a-z0-9\-]+$").unwrap();

        let mut source_names = HashSet::new();

        for source in &config.sources {
            let name = &source.name;

            ensure!(
                source_name_format.is_match(name),
                "Source names must consist only of small letters, digits and hyphens but {name} does not",
            );

            ensure!(
                source_names.insert(name),
                "Source names must be unique but {name} was used twice",
            );

            if !source.not_indexed && &*source.name != "manual" {
                ensure!(!source.origins.is_empty(), "Source {name} has no origins");

                for origin in &source.origins {
                    Facet::from_text(origin)
                        .with_context(|| format!("Source {name} has invalid origin {origin}"))?;
                }
            }

            ensure!(
                source.max_age >= source.age_smear,
                "Source maximum age must be equals to or larger than age smear."
            )
        }

        Ok(())
    }
}
