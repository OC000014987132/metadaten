use std::io::Cursor;

use anyhow::{anyhow, bail, Result};
use cap_std::fs::Dir;
use miniproj::get_projection;
use shapefile::{
    dbase::{encoding::Unicode, FieldValue, Reader as DbaseReader},
    Reader, ShapeReader,
};
use smallvec::smallvec;
use tempfile::TempDir;
use zip::ZipArchive;

use harvester::{
    client::Client,
    fetch_many,
    utilities::{contains_or, point_like_bounding_box},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Region,
    },
    proj_rect,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(25832).expect("Projection not implemented.");

    let tmp_dir = TempDir::new()?;

    let file_name;

    {
        let bytes = client
            .fetch_bytes(source, "Grundwassermessstellen_HB".to_owned(), &source.url)
            .await?;

        let mut archive = ZipArchive::new(Cursor::new(&*bytes))?;
        archive.extract(tmp_dir.path())?;

        file_name = archive
            .file_names()
            .find(|name| name.ends_with(".shp"))
            .ok_or_else(|| anyhow!("Did not find Shapefile in ZIP archive"))?
            .to_owned();
    }

    let path = tmp_dir.path().join(file_name);

    let mut reader = Reader::new(
        ShapeReader::from_path(&path)?,
        DbaseReader::from_path_with_encoding(path.with_extension("dbf"), Unicode)?,
    );

    let (results, errors) = fetch_many(0, 0, reader.iter_shapes_and_records(), |item| async move {
        let (_shape, record) = item?;

        let id = match record.get("ID") {
            Some(FieldValue::Character(Some(id))) => id,
            Some(FieldValue::Character(None)) => return Ok((1, 0, 0)),
            _ => bail!("Missing or unexpected ID value"),
        };

        let key = id.replace(' ', "_");

        let pegel = match record.get("BEZ_PEGEL") {
            Some(FieldValue::Character(Some(pegel))) => pegel,
            _ => bail!("Missing or unexpected BEZ_PEGEL value"),
        };

        let stadtteil = match record.get("STADTTEIL") {
            Some(FieldValue::Character(Some(stadtteil))) => stadtteil,
            _ => bail!("Missing or unexpected STADTTEIL value"),
        };

        let title = format!("Grundwasser-Messstelle {pegel} ({stadtteil})");
        let description =
            format!("Dieser Datensatz beschreibt die {title} der Freien Hansestadt Bremen.");

        let longitude = match record.get("R_ETRS") {
            Some(FieldValue::Numeric(longitude)) => longitude,
            _ => {
                tracing::warn!("Missing or unexpected R_ETRS value");
                &None
            }
        };

        let latitude = match record.get("H_ETRS") {
            Some(FieldValue::Numeric(latitude)) => latitude,
            _ => {
                tracing::warn!("Missing or unexpected H_ETRS value");
                &None
            }
        };

        let bounding_boxes = match (latitude, longitude) {
            (&Some(lat), &Some(lon)) => {
                smallvec![proj_rect(point_like_bounding_box(lat, lon), proj)]
            }
            _ => {
                tracing::warn!("Failed determining bounding box");
                Default::default()
            }
        };

        let regions = smallvec![contains_or(stadtteil, Region::HB)];

        let types = smallvec![Type::Measurements {
            domain: Domain::Groundwater,
            station: Some(Station {
                id: Some(id.as_str().into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Grundwasserstand".to_owned()],
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            bounding_boxes,
            regions,
            language: Language::German,
            license: License::CcBy,
            origins: source.origins.clone(),
            source_url: source.source_url().to_owned(),
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((results + errors, results, errors))
}
