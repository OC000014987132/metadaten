use std::hash::{Hash, Hasher};

use anyhow::{anyhow, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::HashSet;
use regex::Regex;
use reqwest::{Error as HttpError, StatusCode};
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_first_text, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Alternative, Dataset, Language, License, Organisation, OrganisationRole, Person, PersonRole,
    Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) = fetch_links(dir, client, source, selectors).await?;

    let (count1, results1, errors1) = fetch_publications(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_links(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let mut results = 0;
    let mut errors = 0;

    let all_links = AtomicRefCell::new(HashSet::new());

    {
        let mut new_links = fetch_mainlinks(client, source, selectors, &all_links).await?;

        for _level in 1..=6 {
            new_links = fetch_sublinks(
                client,
                source,
                selectors,
                &mut results,
                &mut errors,
                &all_links,
                new_links,
            )
            .await;
        }
    }

    let mut all_links = all_links.into_inner();

    // FIXME: We should try to download and index those.
    all_links.retain(|item| !item.url.path().ends_with(".pdf"));

    let count = all_links.len();

    let (results, errors) = fetch_many(0, 0, all_links, |link| {
        fetch_dataset(dir, client, source, selectors, link)
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Debug, Clone)]
struct Item {
    url: Url,
    title: String,
    date: Option<Date>,
}

impl PartialEq for Item {
    fn eq(&self, other: &Self) -> bool {
        self.url.path() == other.url.path()
    }
}

impl Eq for Item {}

impl Hash for Item {
    fn hash<H>(&self, state: &mut H)
    where
        H: Hasher,
    {
        self.url.path().hash(state);
    }
}

async fn fetch_mainlinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    all_links: &AtomicRefCell<HashSet<Item>>,
) -> Result<Vec<Item>> {
    let text = client
        .fetch_text(source, "topic".to_owned(), &source.url)
        .await?;
    let document = Html::parse_document(&text);

    let mut new_links = document
        .select(&selectors.main_link_selector)
        .map(|element| {
            let link = element.attr("href").unwrap();
            let title = collect_text(element.text());

            Ok(Item {
                url: source.url.join(link)?,
                title,
                date: None,
            })
        })
        .collect::<Result<Vec<_>>>()?;

    let mut all_links = all_links.borrow_mut();

    new_links.retain(|item| {
        if !item.url.as_str().starts_with(source.source_url()) {
            return false;
        }

        all_links.insert(item.clone())
    });

    Ok(new_links)
}

async fn fetch_sublinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    results: &mut usize,
    errors: &mut usize,
    all_links: &AtomicRefCell<HashSet<Item>>,
    new_links: Vec<Item>,
) -> Vec<Item> {
    let sublinks = AtomicRefCell::new(Vec::new());

    (*results, *errors) = fetch_many(*results, *errors, new_links, |item| {
        let sublinks = &sublinks;

        async move {
            let key = make_key(item.url.path()).into_owned();

            let text = client.fetch_text(source, key, &item.url).await?;
            let document = Html::parse_document(&text);

            let new_links = document
                .select(&selectors.sub_link_selector)
                .map(|element| {
                    let link = element.attr("href").unwrap();
                    let title = collect_text(element.text());

                    Ok(Item {
                        url: source.url.join(link)?,
                        title,
                        date: None,
                    })
                })
                .collect::<Result<Vec<_>>>()?;

            let mut all_links = all_links.borrow_mut();

            sublinks.borrow_mut().extend(
                new_links
                    .into_iter()
                    .filter(|item| item.url.as_str().starts_with(source.source_url()))
                    .filter(|item| all_links.insert(item.clone())),
            );

            Ok((1, 1, 0))
        }
    })
    .await;

    sublinks.into_inner()
}

async fn fetch_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let path = item.url.path();

    let key = make_key(path).into_owned();

    let dataset = {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &item.url)
            .await?;
        let document = Html::parse_document(&text);

        let mut title = document
            .select(&selectors.title)
            .map(|element| element.attr("content").unwrap())
            .collect::<String>();

        if title.is_empty() {
            title = item.title
        }

        let description = select_text(
            &document,
            if path.contains("presse/pressemitteilungen") {
                &selectors.press_description
            } else if path.contains("/karten/") {
                &selectors.maps_decription
            } else {
                &selectors.description
            },
        );

        let types = smallvec![if path.contains("presse/pressemitteilungen") {
            Type::Text {
                text_type: TextType::PressRelease,
            }
        } else if path.contains("/newsletter/") {
            Type::Text {
                text_type: TextType::News,
            }
        } else if path.contains("service/rechtsvorschriften/") {
            Type::Legal
        } else if path.contains("/karten/") {
            Type::MapService
        } else if path.contains("/formulare/")
            && !path.contains("/technische-hinweise/")
            && !path.contains("/datenschutz/")
        {
            Type::Form
        } else if path.contains("/merkblaetter-ratgeber-und-broschueren/")
            || path.contains("/fischereiamt/service/publikationen/")
        {
            Type::Text {
                text_type: TextType::Booklet,
            }
        } else {
            Type::Text {
                text_type: TextType::Editorial,
            }
        }];

        let mut resources = document
            .select(&selectors.resources)
            .filter_map(|element| {
                let href = element.attr("href").unwrap();

                let description = collect_text(element.text());
                if description.is_empty() {
                    return None;
                }

                Some((description, href))
            })
            .map(|(description, href)| {
                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        let mut downloads = document
            .select(&selectors.resources_download)
            .map(|element| {
                let description = select_first_text(
                    element,
                    &selectors.resources_description,
                    "resource description for download files",
                )?;

                let link = element
                    .select(&selectors.resources_link)
                    .next()
                    .ok_or_else(|| anyhow!("Missing download link"))?;

                Ok(Resource {
                    r#type: ResourceType::Document,
                    description: Some(description),
                    url: source.url.join(link.attr("href").unwrap())?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<[_; 4]>>>()?;

        resources.append(&mut downloads);

        let date = document
            .select(&selectors.date)
            .next()
            .map(|element| {
                let content = element.attr("content").unwrap();
                Date::parse(content, format_description!("[year]-[month]-[day]"))
            })
            .transpose()?;

        let press_date = document
            .select(&selectors.press_date)
            .next()
            .and_then(|element| {
                let text = collect_text(element.text());

                selectors.press_date_value.captures(&text).map(|captures| {
                    Date::parse(&captures[1], format_description!("[day].[month].[year]"))
                })
            })
            .transpose()?;

        let (select_name, select_mail) = if path.contains("/umweltatlas/") {
            (&selectors.maps_contact_name, &selectors.contact_email)
        } else if path.contains("/pflanzenschutzamt/")
            || path.contains("/fischereiamt/")
            || path.contains("/forsten/service/")
        {
            (&selectors.amt_contact_name, &selectors.amt_contact_email)
        } else {
            (&selectors.contact_name, &selectors.contact_email)
        };

        let persons = document
            .select(&selectors.contact_all)
            .filter_map(|element| {
                element
                    .select(select_name)
                    .next()
                    .map(|name| {
                        let name = collect_text(name.text());

                        let emails = element
                            .select(select_mail)
                            .map(|element| element.attr("href").unwrap().replace("mailto:", ""))
                            .collect();

                        Person {
                            name,
                            role: PersonRole::Contact,
                            emails,
                            ..Default::default()
                        }
                    })
                    .filter(|person| !person.emails.is_empty())
            })
            .collect::<Vec<_>>();

        let organisation_name = document
            .select(&selectors.organisation_name)
            .next()
            .map(|elem| collect_text(elem.text()));

        let organisations = organisation_name
            .map(|organisation_name| {
                let organisation_suffix = document
                    .select(&selectors.organisation_suffix)
                    .next()
                    .map(|elem| collect_text(elem.text()));

                let name = if let Some(organisation_suffix) = organisation_suffix {
                    format!("{organisation_suffix} {organisation_name}")
                } else {
                    organisation_name
                };

                Organisation::Other {
                    name,
                    role: OrganisationRole::Publisher,
                    websites: Default::default(),
                }
            })
            .into_iter()
            .collect();

        let language = if path.contains("/leichte-sprache/") {
            Language::GermanEasy
        } else {
            Language::German
        };

        let alternatives = match document.select(&selectors.language_selector).next() {
            Some(link) => {
                let url = source.url.join(link.attr("href").unwrap())?;

                vec![Alternative::Language {
                    language: Language::English,
                    url: url.into(),
                }]
            }
            None => Default::default(),
        };

        Dataset {
            title,
            description: Some(description),
            types,
            resources,
            issued: press_date.or(date),
            persons,
            organisations,
            regions: smallvec![Region::BE],
            license: License::OtherClosed,
            language,
            alternatives,
            origins: source.origins.clone(),
            source_url: item.url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_publications(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let (max_page, results, errors) =
        match fetch_publications_page(dir, client, source, selectors, 1).await {
            Ok(val) => val,
            Err(err) => {
                if let Some(err) = err.downcast_ref::<HttpError>() {
                    if err.status() == Some(StatusCode::NOT_FOUND) {
                        return Ok((0, 0, 0));
                    }
                }

                return Err(err);
            }
        };

    let (results, errors) = fetch_many(results, errors, 2..=max_page, |page| {
        fetch_publications_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_publications_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let max_page;
    let items;

    {
        let url = &source.url.join(&format!(
            "service/publikationen/?q=&page={page}#searchresults"
        ))?;

        let text = client
            .fetch_text(source, format!("publikationen-{page}"), url)
            .await?;
        let document = Html::parse_document(&text);

        max_page = document
            .select(&selectors.max_pages)
            .next_back()
            .map(|elem| collect_text(elem.text()).parse())
            .unwrap_or(Ok(0))?;

        items = document
            .select(&selectors.publications_items)
            .map(|element| {
                let link = element
                    .select(&selectors.publications_info)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link"))?;

                let url = source.url.join(link.attr("href").unwrap())?;

                let title = select_first_text(element, &selectors.publications_title, "title")?;

                let date = element
                    .select(&selectors.publication_prop_text)
                    .filter_map(|element| {
                        let text = collect_text(element.text());
                        let captures = selectors.publication_prop_text_year.captures(&text)?;
                        Some(captures[1].to_owned())
                    })
                    .next()
                    .map(|year| -> Result<Date> {
                        let year = year.parse()?;
                        let date = Date::from_ordinal_date(year, 1)?;
                        Ok(date)
                    })
                    .transpose()?;

                Ok(Item { url, title, date })
            })
            .collect::<Result<Vec<_>>>()?;
    }

    let (results, errors) = fetch_many(0, 0, items, |item| {
        fetch_publications_details(dir, client, source, selectors, item)
    })
    .await;

    Ok((max_page, results, errors))
}

async fn fetch_publications_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(item.url.path()).into_owned();

    let dataset = {
        let text = client.fetch_text(source, key.clone(), &item.url).await?;
        let document = Html::parse_document(&text);

        let description = select_text(&document, &selectors.publication_detail_description);

        let resources = document
            .select(&selectors.publications_download)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = Some(collect_text(element.text()));

                let mut resource = Resource {
                    r#type: ResourceType::WebPage,
                    description,
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type();

                resource.primary_content =
                    matches!(resource.r#type, ResourceType::Pdf | ResourceType::Zip);

                Ok(resource)
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title: item.title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Publication
            }],
            resources,
            issued: item.date,
            language: Language::German,
            license: License::OtherClosed,
            origins: source.origins.clone(),
            source_url: item.url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    main_link_selector: "#layout-grid__area--maincontent .more[href], #content-footer li a[href*='/leichte-sprache']",
    sub_link_selector: "#layout-grid__area--maincontent .more[href], #layout-grid__area--maincontent a[href*='/leichte-sprache'], #headline_1_3+ .text a, ul.list--tablelist.ruler.has-date div a, .has-submenu a[href]",
    press_description: ".pressnumber+ .textile p, .text p",
    press_date: "p.pressnumber",
    press_date_value: r"vom\s+(\d{2}\.\d{2}\.\d{4})" as Regex,
    organisation_suffix: ".institution",
    organisation_name: "#header .title",
    contact_all: ".modul-contact",
    contact_name: "h3, h4",
    contact_email: "li.email a[href]",
    title: "head meta[name='dcterms.title'][content]",
    description: ".modul-text_bild .text .textile, article.modul-teaser",
    maps_decription: ".inner p",
    maps_contact_name: "p",
    amt_contact_name: "strong",
    amt_contact_email: ".email a[href]",
    resources: ".textile ul[href]:not([href^='mailto:']), .textile a[href]:not([href^='mailto:'])",
    resources_download: "li.modul-download",
    resources_description: "strong.title",
    resources_link: "a.link--download[href]",
    date: "head meta[name='dcterms.date'][content]",
    max_pages: ".last a[href]",
    publications_next_link: ".pager-item a[href]",
    publications_items: ".cartitem",
    publications_info: ".more[href]",
    publications_title: "#results .title, .image+ .inner .title",
    publications_subtitle: ".subtitle",
    publications_download: ".link--download[href]",
    publication_prop_text: "#results .text",
    publication_prop_text_year: r"Erscheinungsjahr:\s+(\d{4})" as Regex,
    publication_detail_description: "#layout-grid__area--maincontent .text",
    language_selector: "#language_select > li > a[href]",
}
