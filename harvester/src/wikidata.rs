//! Pseudo-harvester for WikiData-based vocabularies
//!
//! This is not really a harvester producing datasets,
//! but rather it populates our local versions of controlled
//! vocabularies backed by WikiData. It is only implemented
//! as a harvester to re-use the network and cache infrastructure.
use std::borrow::Cow;
use std::sync::Arc;

use anyhow::{anyhow, ensure, Context, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::{HashMap, HashSet};
use memchr::memmem::find;
use serde::Deserialize;
use serde_json::from_slice;

use harvester::{client::Client, fetch_many, Source};
use metadaten::wikidata::{Organisation, OrganisationsDatabase};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let identifiers = fetch_organisations(client, source).await?;

    let count = identifiers.len();

    let database = &AtomicRefCell::new(OrganisationsDatabase::default());

    let (results, errors) = fetch_many(0, 0, identifiers, |identifier| async move {
        fetch_organisation_details(client, source, database, identifier)
            .await
            .with_context(|| format!("Failed to fetch details of organistation {identifier}"))
    })
    .await;

    database.borrow().write(dir)?;

    Ok((count, results, errors))
}

async fn fetch_organisations(client: &Client, source: &Source) -> Result<HashSet<u64>> {
    const PROPERTIES: &[u64] = &[
        31,   // instance of (P31) that class of which this subject is a particular example and member
        1454, // legal form (P1454) legal form of an entity
    ];

    const LEGAL_FORMS: &[u64] = &[
        2659904, // government organization (Q2659904) organization administrated by a government authority or agency
        1507070, // Landesbetrieb (Q1507070) rechtlich unselbständiger Teil einer Landesverwaltung in Deutschland
        1780029, // Kommanditgesellschaft (Q1780029) legal form of limited partnerschip company in Germany
        19823288, // Kommanditgesellschaft auf Aktien (Q19823288) German version of the KGaA
        460178, // Gesellschaft mit beschränkter Haftung (Q460178) form of a company in Germany with limited liability
        33134157, // Gesellschaft bürgerlichen Rechts (Q33134157) form of a company in Germany
        422062, // Aktiengesellschaft (Q422062) joint-stock company under German law
        9299236, // eingetragener Verein (Q9299236) registered voluntary association in Germany
        1500859, // gemeinnützige GmbH (Q1500859) legal form for non-profit companies in Germany
        15734684, // Körperschaft des öffentlichen Rechts (Q15734684) form of corporate personhood in Germany
        157031,   // foundation (Q157031) type of nonprofit organization
        31855,    // research institute (Q31855) organization whose primary purpose is research
        387917, // administrative divisions of Germany (Q387917) entities in the administrative structure of Germany
    ];

    let mut identifiers = HashSet::new();

    for property in PROPERTIES {
        for legal_form in LEGAL_FORMS {
            if let Err(err) = fetch_organisation_legal_form(
                client,
                source,
                &mut identifiers,
                *property,
                *legal_form,
            )
            .await
            {
                tracing::error!(
                    "Failed to fetch legal form {legal_form} via property {property}: {err:#}"
                );
            }
        }
    }

    Ok(identifiers)
}

async fn fetch_organisation_legal_form(
    client: &Client,
    source: &Source,
    identifiers: &mut HashSet<u64>,
    property: u64,
    legal_form: u64,
) -> Result<()> {
    let query = format!(
        r#"SELECT DISTINCT ?item WHERE {{
    ?item (wdt:P{property}/(wdt:P279*)) wd:Q{legal_form} ;
        wdt:P17 wd:Q183 . # Germany (Q183)
}}"#
    );

    let mut url = source.url.clone();

    url.query_pairs_mut()
        .append_pair("query", &query)
        .append_pair("format", "json");

    let bytes = client
        .checked_fetch_bytes(
            source,
            timeout_exception,
            format!("organisations-{property}-{legal_form}.isol"),
            &url,
        )
        .await?;

    let response = from_slice::<SparqlResponse>(&bytes)?;

    for binding in response.results.bindings {
        let SparqlItem { r#type, value } = binding.item;

        ensure!(
            binding.item.r#type == "uri",
            "Unexpected item type: {}",
            r#type
        );

        let (_, identifier) = value
            .rsplit_once('/')
            .and_then(|(_, identifier)| identifier.split_at_checked(1))
            .filter(|(prefix, _)| *prefix == "Q")
            .ok_or_else(|| anyhow!("Unexpected item value: {}", value))?;

        identifiers.insert(identifier.parse()?);
    }

    Ok(())
}

fn timeout_exception(bytes: &[u8]) -> Result<()> {
    ensure!(
        find(bytes, b"\njava.util.concurrent.TimeoutException\n").is_none(),
        "JSON response indicates timeout exception"
    );

    Ok(())
}

#[derive(Deserialize)]
struct SparqlResponse<'a> {
    #[serde(borrow)]
    results: SparqlResults<'a>,
}

#[derive(Deserialize)]
struct SparqlResults<'a> {
    #[serde(borrow)]
    bindings: Vec<SparqlBinding<'a>>,
}

#[derive(Deserialize)]
struct SparqlBinding<'a> {
    #[serde(borrow)]
    item: SparqlItem<'a>,
}

#[derive(Deserialize)]
struct SparqlItem<'a> {
    r#type: &'a str,
    value: &'a str,
}

async fn fetch_organisation_details(
    client: &Client,
    source: &Source,
    database: &AtomicRefCell<OrganisationsDatabase>,
    identifier: u64,
) -> Result<(usize, usize, usize)> {
    let url = source
        .source_url()
        .replace("{{identifier}}", &identifier.to_string())
        .parse()?;

    let bytes = client
        .fetch_bytes(source, format!("entity-data-{identifier}.isol"), &url)
        .await?;

    let response = from_slice::<LdiResponse>(&bytes)?;

    ensure!(
        response.entities.len() == 1,
        "Unexpected number of entities: {}",
        response.entities.len()
    );

    let (_, entity) = response.entities.into_iter().next().unwrap();

    ensure!(
        entity.r#type == "item",
        "Unexpected entity type: {}",
        entity.r#type
    );

    let mut aliases = entity
        .aliases
        .de
        .into_iter()
        .chain(entity.aliases.en.into_iter())
        .chain(
            [&entity.labels.de, &entity.labels.en]
                .into_iter()
                .flatten()
                .cloned(),
        )
        .map(|string_value| string_value.value.into_owned().into())
        .collect::<Vec<_>>();

    aliases.sort_unstable();
    aliases.dedup();

    let Some(label) = entity
        .labels
        .de
        .or(entity.labels.en)
        .map(|string_value| string_value.value.into_owned().into())
    else {
        return Ok((1, 0, 0));
    };

    let mut websites = Vec::with_capacity(entity.claims.websites.len());

    for snak in entity.claims.websites {
        let data_type = snak.main_snak.data_type;

        ensure!(
            data_type == "url",
            "Unexpected main snak data type: {}",
            data_type
        );

        let Some(data_value) = snak.main_snak.data_value else {
            continue;
        };

        ensure!(
            data_value.r#type == "string",
            "Unexpected main snak data value type: {}",
            data_value.r#type
        );

        let url = data_value.value.into_owned().into();

        websites.push(url);
    }

    database.borrow_mut().entries.insert(
        identifier,
        Arc::new(Organisation {
            label,
            aliases: aliases.into(),
            websites: websites.into(),
        }),
    );

    Ok((1, 1, 0))
}

#[derive(Deserialize)]
struct LdiResponse<'a> {
    #[serde(borrow)]
    entities: HashMap<&'a str, LdiEntity<'a>>,
}

#[derive(Deserialize)]
struct LdiEntity<'a> {
    r#type: &'a str,
    #[serde(borrow)]
    labels: LdiTranslatedString<'a>,
    #[serde(borrow)]
    aliases: LdiTranslatedStringList<'a>,
    #[serde(borrow)]
    claims: LdiClaims<'a>,
}

#[derive(Debug, Deserialize)]
struct LdiTranslatedString<'a> {
    #[serde(borrow)]
    de: Option<LdiStringValue<'a>>,
    #[serde(borrow)]
    en: Option<LdiStringValue<'a>>,
}

#[derive(Debug, Deserialize)]
struct LdiTranslatedStringList<'a> {
    #[serde(borrow, default)]
    de: Vec<LdiStringValue<'a>>,
    #[serde(borrow, default)]
    en: Vec<LdiStringValue<'a>>,
}

#[derive(Debug, Clone, Deserialize)]
struct LdiStringValue<'a> {
    #[serde(borrow)]
    value: Cow<'a, str>,
}

#[derive(Debug, Deserialize)]
struct LdiClaims<'a> {
    #[serde(rename = "P856", borrow, default)]
    websites: Vec<LdiSnak<'a>>,
}

#[derive(Debug, Deserialize)]
struct LdiSnak<'a> {
    #[serde(rename = "mainsnak", borrow)]
    main_snak: LdiMainSnak<'a>,
}

#[derive(Debug, Deserialize)]
struct LdiMainSnak<'a> {
    #[serde(rename = "datatype")]
    data_type: &'a str,
    #[serde(rename = "datavalue", borrow)]
    data_value: Option<LdiDataValue<'a>>,
}

#[derive(Debug, Deserialize)]
struct LdiDataValue<'a> {
    r#type: &'a str,
    #[serde(borrow)]
    value: Cow<'a, str>,
}
