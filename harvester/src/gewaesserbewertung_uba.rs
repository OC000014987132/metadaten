use anyhow::Result;
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use hashbrown::HashMap;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};

use harvester::{
    client::Client,
    fetch_many, remove_datasets, selectors,
    utilities::{collect_text, make_key, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let main_pages = fetch_main_pages(client, source, selectors).await?;

    let pages = fetch_all_pages(client, source, selectors, main_pages).await?;

    let count = pages.len();

    tracing::info!("Scraping {} pages", count);

    let clusters = AtomicRefCell::new(HashMap::new());

    let (results, errors) = fetch_many(0, 0, pages, |page| {
        fetch_page_details(dir, client, source, selectors, &clusters, page)
    })
    .await;

    for (_cluster_key, keys) in clusters.into_inner() {
        if keys.len() > 1 {
            merge_cluster(dir, client, source, keys).await?;
        }
    }

    Ok((count, results, errors))
}

async fn fetch_page_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    clusters: &AtomicRefCell<HashMap<String, SmallVec<[String; 1]>>>,
    page: String,
) -> Result<(usize, usize, usize)> {
    let dataset;
    let key;

    {
        let url = source.url.join(&page)?;
        key = make_key(&page).into_owned();

        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        let title = select_text(&document, &selectors.article_breadcrumbs);

        let description = select_text(&document, &selectors.article_description);

        let resources = document
            .select(&selectors.article_resource_link) // Links in weiterführende Literatur
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = collect_text(element.text());
                let primary_content = href.contains("make_pdf");

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    primary_content,
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        let types = smallvec![if title.contains("Software") {
            Type::Software
        } else {
            Type::Text {
                text_type: TextType::Editorial,
            }
        }];

        dataset = Dataset {
            title,
            description: Some(description),
            types,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            resources,
            source_url: url.into(),
            ..Default::default()
        };
    }

    let cluster_key = dataset.title.replace(" Weiterführende Literatur", "");

    clusters
        .borrow_mut()
        .entry(cluster_key)
        .or_default()
        .push(key.clone());

    write_dataset(dir, client, source, key, dataset).await
}

async fn merge_cluster(
    dir: &Dir,
    client: &Client,
    source: &Source,
    mut keys: SmallVec<[String; 1]>,
) -> Result<()> {
    let mut datasets = remove_datasets(dir, client, source, &keys).await?;

    let base_idx = datasets
        .iter()
        .enumerate()
        .min_by_key(|(_, dataset)| &dataset.title)
        .unwrap()
        .0;

    let base_key = keys.swap_remove(base_idx);
    let mut base_dataset = datasets.swap_remove(base_idx);

    for dataset in datasets {
        base_dataset.resources.extend(dataset.resources);
    }

    write_dataset(dir, client, source, base_key, base_dataset).await?;

    Ok(())
}

async fn fetch_main_pages(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let text = client
        .fetch_text(source, "navbar".to_owned(), &source.url)
        .await?;

    let document = Html::parse_document(&text);

    let main_pages = document
        .select(&selectors.main_navbar_pages)
        .map(|elem| elem.attr("href").unwrap().to_owned())
        .collect();

    Ok(main_pages)
}

async fn fetch_all_pages(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    main_pages: Vec<String>,
) -> Result<Vec<String>> {
    let mut pages = Vec::new();

    for page in main_pages {
        let url = source.url.join(&page)?;
        let key = page.replace('/', "-");

        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        for element in document.select(&selectors.sub_navbar_pages) {
            let page = element.attr("href").unwrap().to_owned();
            pages.push(page);
        }
    }

    pages.sort_unstable();
    pages.dedup();

    Ok(pages)
}

selectors! {
    main_navbar_pages: "nav.ym-vlist.mainNav a[href]", // für die Navigationsleiste der Hauptseite
    sub_navbar_pages: "ul[class='menu'] a[href]", // für die Sub-Navigationsleiste einzelner Hauptseiten
    article_breadcrumbs: ".sitepath a[href]",// für die Navigationsführung einzelner Seiten
    article_description: "div[class='ym-col3'] p, div[class='ym-col3'] ul",
    article_resource_link: "div.make-pdf a[href], table.literatur a[href], div.ym-col3 > div.ym-cbox > ul li strong a[href], div.ym-col3 > div.ym-cbox ul a[href], div.ym-col3 > div.ym-cbox > table a[href], div.ym-col3 > div.ym-cbox > p > a[href]",
}
