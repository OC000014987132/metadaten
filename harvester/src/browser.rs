use std::env::var_os;
use std::future::Future;
use std::ops::Deref;
use std::path::PathBuf;
use std::sync::Arc;

use anyhow::{anyhow, Result};
use fantoccini::{Client, ClientBuilder};
use hyper_util::client::legacy::connect::HttpConnector;
use once_cell::sync::Lazy;
use serde_json::{json, Value};
use tokio::{
    fs::{read_dir, remove_file, DirEntry},
    sync::{Mutex, OwnedMutexGuard},
    time::{sleep, Duration},
};

/// Handle to control a browser using a WebDriver client
///
/// Our VM have ChromeDriver pre-installed to facilitate the development of harvesters
/// which require a full browser to work. An instance of ChromeDriver must however
/// be started manually before any other work can begin.
///
/// To invoke ChromeDriver first run
///
/// ```console
/// > chromium.chromedriver --port=9515
/// ```
///
/// in a separate terminal which will display the browser on a virtual framebuffer.
///
/// Additionally if you would like to display the browser locally, the following steps are required.
/// Working from a Linux desktop and using [X forwarding][X-forwarding] you can display the
/// browser on that desktop even though it is running on the VM. This is often helpful
/// to see the state of the web sites or applications as the browser interacts with them.
///
/// To do that, a separate terminal running
///
/// ```console
/// ssh -X entwicklung
/// ```console
///
/// must be used on the desktop and the environment variable `BROWSER_NO_HEADLESS` must be set on the VM.
///
/// However, the first run of the harveste using a newly provisioned VM will not work
/// as the X connection required by the Chromium browser is rejected. To fix this, run
///
/// ```console
/// ln -s /home/ubuntu/.Xauthority /home/ubuntu/snap/chromium/current/.Xauthority
/// ```
///
/// and reestablish the SSH session to fix this.
///
/// [X-forwarding]: https://en.wikipedia.org/wiki/X_Window_System#Remote_desktop
pub struct Browser {
    _guard: OwnedMutexGuard<()>,
    client: Client,
}

static MUTEX: Lazy<Arc<Mutex<()>>> = Lazy::new(|| Arc::new(Mutex::new(())));

impl Deref for Browser {
    type Target = Client;

    fn deref(&self) -> &Self::Target {
        &self.client
    }
}

impl Browser {
    pub async fn open() -> Result<Self> {
        let _guard = MUTEX.clone().lock_owned().await;

        let args = if var_os("BROWSER_NO_HEADLESS").is_none() {
            &["--headless", "--no-sandbox", "--disable-gpu"][..]
        } else {
            &["--no-sandbox", "--disable-gpu"][..]
        };

        let caps = match json!({
            "goog:chromeOptions": {
                "args": args,
            }
        }) {
            Value::Object(caps) => caps,
            _ => unreachable!(),
        };

        let client = ClientBuilder::new(HttpConnector::new())
            .capabilities(caps)
            .connect("http://localhost:9515")
            .await?;

        Ok(Self { _guard, client })
    }

    pub async fn clear_downloads(&self) -> Result<()> {
        let dir = download_dir_from_env();

        if let Ok(mut dir) = read_dir(dir).await {
            while let Some(entry) = dir.next_entry().await? {
                if entry.file_type().await?.is_file() {
                    remove_file(entry.path()).await?;
                }
            }
        }

        Ok(())
    }

    pub async fn wait_for_download<A, F, R>(&self, mut action: A) -> Result<R>
    where
        A: FnMut(DirEntry) -> F,
        F: Future<Output = Result<Option<R>>>,
    {
        let dir = download_dir_from_env();

        'poll: for _ in 0..300 {
            tracing::debug!("Waiting for download to complete...");
            sleep(Duration::from_secs(1)).await;

            if let Ok(mut dir) = read_dir(&dir).await {
                while let Some(entry) = dir.next_entry().await? {
                    if entry.file_type().await?.is_file() {
                        let file_name = entry.file_name();

                        if file_name.as_encoded_bytes().ends_with(b".crdownload") {
                            continue 'poll;
                        }
                    }
                }
            }

            if let Ok(mut dir) = read_dir(&dir).await {
                while let Some(entry) = dir.next_entry().await? {
                    if entry.file_type().await?.is_file() {
                        match action(entry).await {
                            Ok(None) => (),
                            Ok(Some(result)) => return Ok(result),
                            Err(err) => return Err(err),
                        }
                    }
                }
            }
        }

        Err(anyhow!("Download did not complete within 5 minutes"))
    }
}

fn download_dir_from_env() -> PathBuf {
    var_os("BROWSER_DOWNLOAD_DIR")
        .expect("Environment variable BROWSER_DOWNLOAD_DIR not set")
        .into()
}
