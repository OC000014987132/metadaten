use std::borrow::Cow;

use anyhow::Result;
use cap_std::fs::Dir;
use serde::Deserialize;
use serde_json::from_str;
use serde_roxmltree::{from_doc, roxmltree::Document as XmlDocument};
use smallvec::SmallVec;

use crate::csw;
use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::dataset::Dataset;

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let rows = source.batch_size;

    let (count, results, errors) = fetch_datasets(dir, client, source, rows, 0).await?;
    tracing::info!("Harvesting {} datasets", count);

    let requests = count.div_ceil(rows);
    let start = (1..requests).map(|request| request * rows);

    let (results, errors) = fetch_many(results, errors, start, |start| {
        fetch_datasets(dir, client, source, rows, start)
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip(dir, client, source))]
async fn fetch_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    rows: usize,
    start: usize,
) -> Result<(usize, usize, usize)> {
    let start = start.to_string();

    tracing::debug!("Fetching {} datasets starting at {}", rows, start);

    let mut url = source.url.clone();

    url.query_pairs_mut()
        .append_pair("start", &start)
        .append_pair("rows", &rows.to_string())
        .append_pair("q", source.filter.as_deref().unwrap_or("*"));

    let body = client.fetch_text(source, start, &url).await?;

    let response = from_str::<SelectResponse>(&body)?;

    let count = response.results.num_found;
    let (results, errors) = fetch_many(0, 0, response.results.docs, |doc| {
        translate_dataset(dir, client, source, doc)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    doc: Document<'_>,
) -> Result<(usize, usize, usize)> {
    if doc.format.contains(&"application/vnd.iso.19139+xml") {
        let document = XmlDocument::parse(&doc.content)?;

        let record = from_doc::<csw::Record>(&document)?;

        return csw::translate_dataset(dir, client, source, record).await;
    }

    let title = match doc.title {
        Some(title) => title,
        None => {
            tracing::debug!("Document {} has no title", doc.id);
            return Ok((1, 0, 1));
        }
    };

    let dataset = Dataset {
        title,
        description: doc.description,
        origins: source.origins.clone(),
        source_url: source.source_url().replace("{{id}}", &doc.id),
        ..Default::default()
    };

    write_dataset(dir, client, source, doc.id.into_owned(), dataset).await
}

#[derive(Debug, Deserialize)]
struct SelectResponse<'a> {
    #[serde(rename = "response", borrow)]
    results: Results<'a>,
}

#[derive(Debug, Deserialize)]
struct Results<'a> {
    #[serde(rename = "numFound")]
    num_found: usize,
    #[serde(borrow)]
    docs: Vec<Document<'a>>,
}

#[derive(Debug, Deserialize)]
struct Document<'a> {
    #[serde(borrow)]
    id: Cow<'a, str>,
    title: Option<String>,
    description: Option<String>,
    #[serde(borrow)]
    format: SmallVec<[&'a str; 1]>,
    #[serde(borrow)]
    content: Cow<'a, str>,
}
