//! This harvester maps the "Recherche" function available at Wasser-DE into our catalogue.
//!
//! | Original field            | Mapped field       | Comment                                                      |
//! | ------------------------- | ------------------ | ------------------------------------------------------------ |
//! | ID                        | id                 | Assumed to be numeric and redundant                          |
//! | metadataid                |                    |                                                              |
//! | NAME                      | title              | Document skipped if missing                                  |
//! | TEASERTEXT                | description        | TEASERTEXT preferred over AUTOTEASERTEXT if both are present |
//! | AUTOTEASERTEXT            | description        |                                                              |
//! | LICENSE_ID                |                    |                                                              |
//! | LICENSE_NAME_KURZ         | license            | LICENSE_ID and LICENSE_NAME_LANG considered redundant        |
//! | LICENSE_NAME_LANG         |                    |                                                              |
//! | RICHTLINIE_IDS            | tags               |                                                              |
//! | URL                       | resource           |                                                              |
//! | CONTENTTYPE               | resource.type      | Mime-type of the linked ressource, reduced to category       |
//! | JAHR_VEROEFFENTLICHUNG    | issued             | http://purl.org/dc/terms/issued                              |
//! | KOMMENTAR                 | comment            |                                                              |
//! | LAST_CHECKED              | last_checked       | Last time the Wasser-DE staff checked this document          |
//! | REGION_NAME               | region             | Geographic region. Can be a city, country, state or river    |
//! | REGION_ID                 |                    |                                                              |
//! | SOURCE_NAME               | publisher.name     | Legal publisher of the dataset.                              |
//! | SOURCE_ID                 |                    | Dropped                                                      |
//! | ANSPRECHPARTNER_NAME      | contact_names      |                                                              |
//! | ANSPRECHPARTNER_EMAIL     | contact_emails     |                                                              |
//! | AP_NAME                   | contact_names      |                                                              |
//! | AP_EMAIL                  | contact_emails     |                                                              |
//! | ANSPRECHPARTNER_NAME_RL1  | contact_names      |                                                              |
//! | ANSPRECHPARTNER_EMAIL_RL1 | contact_emails     |                                                              |
//! | ANSPRECHPARTNER_NAME_RL2  | contact_names      |                                                              |
//! | ANSPRECHPARTNER_EMAIL_RL2 | contact_emails     |                                                              |
//! | ANSPRECHPARTNER_NAME_RL3  | contact_names      |                                                              |
//! | ANSPRECHPARTNER_EMAIL_RL3 | contact_emails     |                                                              |
//! | ANSPRECHPARTNER_NAME_RL4  | contact_names      |                                                              |
//! | ANSPRECHPARTNER_EMAIL_RL4 | contact_emails     |                                                              |
//!  
use anyhow::Result;
use cap_std::fs::Dir;
use compact_str::CompactString;
use serde::{Deserialize, Serialize};
use serde_json::from_slice;
use smallvec::smallvec;
use time::{macros::format_description, Date};

use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::dataset::{
    Dataset, Organisation, OrganisationRole, Person, PersonRole, Resource, ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let url = source.url.join("rest/api/reportingelement")?;

    let body = client
        .make_request(source, "response".to_owned(), Some(&url), |client| async {
            client
                .post(url.clone())
                .json(&Request {
                    filter: Filter {
                        and: vec![And { or: Vec::new() }],
                    },
                })
                .send()
                .await?
                .error_for_status()?
                .bytes()
                .await
                .map(Vec::from)
        })
        .await?;

    let response = from_slice::<Response>(&body)?;

    let count = response.results.len();
    tracing::info!("Retrieved {count} documents");

    let (results, errors) = fetch_many(0, 0, response.results, |document| {
        translate_dataset(dir, client, source, document)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    document: Document,
) -> Result<(usize, usize, usize)> {
    let tags = document.tags();

    let title = match document.name {
        Some(title) => title,
        None => {
            tracing::debug!("Document {} has no title", document.id);
            // We decided to not count the error here, as it was nothing we can change in the source by applying our curation process.
            return Ok((1, 0, 0));
        }
    };

    let description = document.teaser_text.or(document.auto_teaser_text);

    let issued = document
        .year_issued
        .map(|year_issued| Date::from_ordinal_date(year_issued, 1))
        .transpose()?;

    let modified = document
        .last_checked
        .map(|last_checked| Date::parse(&last_checked, format_description!("[year]-[month]-[day]")))
        .transpose()?;

    let organisations = document
        .source
        .into_iter()
        .map(|name| Organisation::Other {
            name,
            role: OrganisationRole::Publisher,
            websites: Default::default(),
        })
        .collect();

    let mut persons = Vec::new();

    let mut push_contact = |name: Option<String>, email: Option<String>| {
        let name = match (name.as_deref(), &email) {
            (Some(" , ") | None, Some(email)) => email
                .split_once('@')
                .map(|(namepart, _)| namepart.to_owned()),
            _ => name,
        };

        match name {
            Some(name) if !name.is_empty() => {
                let emails = email.into_iter().filter(|email| email != "@").collect();

                persons.push(Person {
                    name,
                    role: PersonRole::Contact,
                    emails,
                    ..Default::default()
                });
            }
            _ => (),
        }
    };

    push_contact(document.contact_name, document.contact_email);
    push_contact(document.contact_name_ap, document.contact_email_ap);
    push_contact(document.contact_name_rl1, document.contact_email_rl1);
    push_contact(document.contact_name_rl2, document.contact_email_rl2);
    push_contact(document.contact_name_rl3, document.contact_email_rl3);
    push_contact(document.contact_name_rl4, document.contact_email_rl4);

    let r#type = document.content_type.as_deref().into();

    let resources = smallvec![Resource {
        r#type,
        url: document.url,
        description: None,
        primary_content: r#type == ResourceType::Pdf,
        ..Default::default()
    }];

    let dataset = Dataset {
        title,
        description,
        comment: document.comment,
        origins: source.origins.clone(),
        license: document.license.as_deref().into(),
        organisations,
        persons,
        tags,
        regions: document.region_name.map(Into::into).into_iter().collect(),
        issued,
        modified,
        source_url: source.url.clone().into(),
        resources,
        ..Default::default()
    };

    write_dataset(dir, client, source, document.id.to_string(), dataset).await
}

#[derive(Serialize)]
struct Request {
    filter: Filter,
}

#[derive(Serialize)]
struct Filter {
    and: Vec<And>,
}

#[derive(Serialize)]
struct And {
    or: Vec<Or>,
}

#[derive(Serialize)]
struct Or {}

#[derive(Deserialize)]
struct Response {
    #[serde(rename = "V_REP_BASE_API")]
    results: Vec<Document>,
}

#[derive(Deserialize)]
struct Document {
    #[serde(rename = "ID")]
    id: usize,
    #[serde(rename = "NAME")]
    name: Option<String>,
    #[serde(rename = "TEASERTEXT")]
    teaser_text: Option<String>,
    /// An alternative text to TEASERTEXT.
    #[serde(rename = "AUTOTEASERTEXT")]
    auto_teaser_text: Option<String>,
    #[serde(rename = "LICENSE_NAME_KURZ")]
    license: Option<String>,
    #[serde(rename = "SOURCE_NAME")]
    source: Option<String>,
    #[serde(rename = "RICHTLINIE_IDS")]
    directive: Option<String>,
    #[serde(rename = "URL")]
    url: String,
    #[serde(rename = "JAHR_VEROEFFENTLICHUNG")]
    year_issued: Option<i32>,
    #[serde(rename = "KOMMENTAR")]
    comment: Option<String>,
    #[serde(rename = "LAST_CHECKED")]
    last_checked: Option<String>,
    #[serde(rename = "REGION_NAME")]
    region_name: Option<CompactString>,
    #[serde(rename = "ANSPRECHPARTNER_NAME")]
    contact_name: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_EMAIL")]
    contact_email: Option<String>,
    #[serde(rename = "AP_NAME")]
    contact_name_ap: Option<String>,
    #[serde(rename = "AP_EMAIL")]
    contact_email_ap: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_NAME_RL1")]
    contact_name_rl1: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_EMAIL_RL1")]
    contact_email_rl1: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_NAME_RL2")]
    contact_name_rl2: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_EMAIL_RL2")]
    contact_email_rl2: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_NAME_RL3")]
    contact_name_rl3: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_EMAIL_RL3")]
    contact_email_rl3: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_NAME_RL4")]
    contact_name_rl4: Option<String>,
    #[serde(rename = "ANSPRECHPARTNER_EMAIL_RL4")]
    contact_email_rl4: Option<String>,
    #[serde(rename = "CONTENTTYPE")]
    content_type: Option<String>,
}

impl Document {
    fn tags(&self) -> Vec<Tag> {
        let mut tags = Vec::new();

        if let Some(directive) = &self.directive {
            if directive.contains("1#") {
                tags.push(Tag::WRRL);
            }
            if directive.contains("2#") {
                tags.push(Tag::HWRM_RL);
            }
            if directive.contains("3#") {
                tags.push(Tag::MSR_RL);
            }
            if directive.contains("4#") {
                tags.push(Tag::BGRL);
            }
            if directive.contains("5#") {
                tags.push(Tag::TWRL);
            }
        }

        tags
    }
}
