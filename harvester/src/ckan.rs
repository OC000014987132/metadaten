use std::borrow::Cow;
use std::cmp::Ordering;
use std::mem::take;

use anyhow::{ensure, Context, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use cow_utils::CowUtils;
use geo::algorithm::BoundingRect;
use geozero::{geojson::GeoJson, ToGeo};
use serde::Deserialize;
use serde_json::{from_slice, Value};
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};

use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::{
    atkis::ATKIS,
    dataset::{
        Dataset, GlobalIdentifier, License, Organisation, OrganisationRole, Person, PersonRole,
        Region, Resource, ResourceType,
    },
    geonames::extract_id as extract_geonames_id,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let rows = source.batch_size;

    let (count, results, errors) = fetch_datasets(dir, client, source, 0, rows).await?;
    tracing::info!("Harvesting {} datasets", count);

    let requests = count.div_ceil(rows);
    let start = (1..requests).map(|request| request * rows);

    let (results, errors) = fetch_many(results, errors, start, |start| {
        fetch_datasets(dir, client, source, start, rows)
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip(dir, client, source))]
async fn fetch_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    start: usize,
    rows: usize,
) -> Result<(usize, usize, usize)> {
    let start = start.to_string();

    tracing::debug!("Fetching {} datasets starting at {}", rows, start);

    let mut url = source.url.join("api/action/package_search")?;

    url.query_pairs_mut()
        .append_pair("start", &start)
        .append_pair("rows", &rows.to_string());

    if let Some(filter) = &source.filter {
        url.query_pairs_mut().append_pair("fq", filter);
    }

    let body = client.fetch_bytes(source, start, &url).await?;

    let response = from_slice::<PackageSearch>(&body)?;

    ensure!(
        response.success,
        "Failed to fetch packages: {}",
        response
            .error
            .as_ref()
            .map_or("Malformed response", |err| &err.message)
    );

    let count = response.result.count;
    let (results, errors) = fetch_many(0, 0, response.result.results, |package| {
        translate_dataset(dir, client, source, package)
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip_all, fields(key = package.id.as_ref()))]
async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    mut package: Package<'_>,
) -> Result<(usize, usize, usize)> {
    let source_url = package.placeholders(source.source_url());

    let license = package.license();

    let language = package
        .extras
        .iter()
        .find(|extra| extra.is("language"))
        .map(|extra| {
            extra
                .value
                .trim_start_matches("[\"")
                .trim_start_matches("http://publications.europa.eu/resource/authority/language/")
                .trim_end_matches("\"]")
                .into()
        })
        .unwrap_or_default();

    let resources = package
        .resources
        .into_iter()
        .map(|resource| {
            let mut resource = if let Some(mimetype) = resource.mimetype {
                let resource_type = mimetype.into();

                Resource {
                    url: resource.url,
                    r#type: resource_type,
                    ..Default::default()
                }
            } else {
                Resource {
                    url: resource.url,
                    r#type: ResourceType::WebPage,
                    ..Default::default()
                }
                .guess_or_keep_type()
            };

            resource.align();

            resource.primary_content = matches!(
                resource.r#type,
                ResourceType::Csv
                    | ResourceType::Xml
                    | ResourceType::PlainText
                    | ResourceType::Zip
                    | ResourceType::Tiff
                    | ResourceType::Wfs
                    | ResourceType::Wms
            );

            resource
        })
        .collect();

    let bounding_boxes = package
        .extras
        .iter()
        .filter(|extra| extra.is("spatial"))
        .filter_map(|extra| GeoJson(&extra.value).to_geo().ok()?.bounding_rect())
        .collect();

    let organisations = package
        .extras
        .iter_mut()
        .filter(|extra| extra.is("publisher_name"))
        .map(|extra| Organisation::Other {
            name: take(&mut extra.value).into_owned(),
            role: OrganisationRole::Publisher,
            websites: Default::default(),
        })
        .chain(
            package
                .organization
                .title
                .filter(|name| !name.is_empty())
                .map(|name| Organisation::Other {
                    name,
                    role: OrganisationRole::Provider,
                    websites: Default::default(),
                })
                .into_iter(),
        )
        .collect::<SmallVec<_>>();

    let persons = package
        .author
        .filter(|name| !name.is_empty())
        .map(|name| {
            let emails = package
                .contact_email
                .or(package.author_email)
                .into_iter()
                .collect();

            Person {
                name,
                role: PersonRole::Contact,
                emails,
                ..Default::default()
            }
        })
        .into_iter()
        .collect();

    let global_identifier = package
        .extras
        .iter_mut()
        .filter(|extra| extra.is("identifier"))
        .find_map(|extra| GlobalIdentifier::parse(take(&mut extra.value)))
        // fall back to GUID only if no identifier is available
        .or_else(|| {
            package
                .extras
                .iter_mut()
                .filter(|extra| extra.is("guid"))
                .find_map(|extra| GlobalIdentifier::parse(take(&mut extra.value)))
        });

    let extract_date = |key| {
        package
            .extras
            .iter()
            .find(|extra| extra.is(key) && !extra.value.is_empty())
            .map(|extra| {
                let value = extra.value.trim_matches('"');
                let value = value.split_once('T').map_or(value, |(date, _time)| date);

                Date::parse(
                    value,
                    format_description!(
                        version = 2,
                        "[year]-[first [[month]] [[month padding:none]]]-[first [[day]] [[day padding:none]]]"
                    )
                )
                .with_context(|| format!("Invalid date {}", extra.value))
            })
            .transpose()
    };

    let issued = extract_date("issued")?;
    let modified = extract_date("modified")?;

    let temporal_start = extract_date("temporal_start")?;
    let temporal_end = extract_date("temporal_end")?;

    let time_ranges =
        if let (Some(temporal_start), Some(temporal_end)) = (temporal_start, temporal_end) {
            smallvec![(temporal_start, temporal_end).into()]
        } else {
            Default::default()
        };

    let regions = package
        .geographical_coverage
        .map(Region::from)
        .into_iter()
        .chain({
            package
                .extras
                .iter()
                .filter(|extra| extra.is("spatial_uri") && !extra.value.is_empty())
                .filter_map(|extra| {
                    let res = extract_geonames_id(&extra.value)
                        .map(Region::GeoName)
                        .or_else(|err| {
                            if let Some(key) = extra
                                .value
                                .strip_prefix("http://dcat-ap.de/def/politicalGeocoding/")
                            {
                                if let Some(key) = key.strip_prefix("stateKey/") {
                                    return Ok(Region::RegionalKey(
                                        key.parse::<u64>()? * 10 * 100 * 10000 * 1000,
                                    ));
                                } else if let Some(key) = key.strip_prefix("districtKey/") {
                                    return Ok(Region::RegionalKey(
                                        key.parse::<u64>()? * 10000 * 1000,
                                    ));
                                } else if let Some(key) = key.strip_prefix("regionalKey/") {
                                    return Ok(Region::RegionalKey(key.parse::<u64>()?));
                                }
                            }

                            Err(err)
                        });

                    match res {
                        Ok(region) => Some(region),
                        Err(err) => {
                            tracing::debug!("Failed to parse spatial URI: {:#}", err);
                            None
                        }
                    }
                })
        })
        .chain({
            package
                .extras
                .iter()
                .filter(|extra| extra.is("geocodingText"))
                .filter_map(|extra| {
                    let value = extra.value.parse::<Value>().ok()?;
                    let value = value.as_array()?.first()?.as_str()?;
                    Some(Region::from(value))
                })
        })
        .chain(ATKIS.extract(&package.title).map(Region::Atkis))
        .collect();

    let dataset = Dataset {
        title: package.title,
        description: package.notes,
        origins: source.origins.clone(),
        license,
        language,
        regions,
        source_url,
        machine_readable_source: true,
        resources,
        issued,
        modified,
        time_ranges,
        bounding_boxes,
        global_identifier,
        organisations,
        persons,
        ..Default::default()
    };

    write_dataset(dir, client, source, package.id.into_owned(), dataset).await
}

#[derive(Deserialize)]
struct PackageSearch<'a> {
    success: bool,
    #[serde(borrow)]
    error: Option<CkanError<'a>>,
    #[serde(borrow)]
    result: PackageSearchResult<'a>,
}

#[derive(Deserialize)]
struct PackageSearchResult<'a> {
    count: usize,
    #[serde(borrow)]
    results: Vec<Package<'a>>,
}

#[derive(Default, Deserialize)]
struct Package<'a> {
    #[serde(borrow)]
    id: Cow<'a, str>,
    #[serde(borrow)]
    name: Cow<'a, str>,
    title: String,
    notes: Option<String>,
    #[serde(borrow)]
    license_id: Option<Cow<'a, str>>,
    #[serde(borrow)]
    license_url: Option<Cow<'a, str>>,
    geographical_coverage: Option<CompactString>,
    #[serde(borrow, default)]
    extras: Vec<Extra<'a>>,
    #[serde(borrow)]
    resources: Vec<CkanResource<'a>>,
    organization: CkanOrganization,
    author: Option<String>,
    author_email: Option<String>,
    contact_email: Option<String>,
}

impl Package<'_> {
    fn placeholders(&self, val: &str) -> String {
        let val = val.cow_replace("{{name}}", &self.name);
        val.into_owned()
    }

    fn license(&self) -> License {
        if let Some(license_url) = &self.license_url {
            if !license_url.is_empty() {
                return license_url.as_ref().into();
            }
        }

        if let Some(license_id) = &self.license_id {
            if !license_id.is_empty() {
                return license_id.as_ref().into();
            }
        }

        match self.resources.len().cmp(&1) {
            Ordering::Less => License::Unknown,
            Ordering::Equal => self.resources[0].license.as_deref().into(),
            Ordering::Greater => {
                let licenses = self
                    .resources
                    .iter()
                    .filter_map(|resource| {
                        resource
                            .license
                            .as_ref()
                            .map(|license| license.as_ref().into())
                    })
                    .collect::<Vec<License>>();

                if let Some((head, tail)) = licenses.split_first() {
                    if tail.iter().all(|license| license == head) {
                        *head
                    } else if licenses.iter().all(|license| license.is_open()) {
                        License::MixedOpen
                    } else {
                        License::MixedClosed
                    }
                } else {
                    License::Unknown
                }
            }
        }
    }
}

#[derive(Default, Deserialize)]
struct Extra<'a> {
    #[serde(borrow)]
    key: Cow<'a, str>,
    #[serde(borrow)]
    value: Cow<'a, str>,
}

impl Extra<'_> {
    fn is(&self, name: &str) -> bool {
        let key = self.key.as_ref();

        key.strip_prefix("extra:").unwrap_or(key) == name
    }
}

#[derive(Default, Deserialize)]
struct CkanOrganization {
    title: Option<String>,
}

#[derive(Default, Deserialize)]
struct CkanResource<'a> {
    url: String,
    #[serde(borrow)]
    license: Option<Cow<'a, str>>,
    #[serde(borrow)]
    mimetype: Option<&'a str>,
}

#[derive(Deserialize)]
struct CkanError<'a> {
    #[serde(borrow)]
    message: Cow<'a, str>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_license_no_resources() {
        let package = Package::default();

        assert_eq!(package.license(), License::Unknown);
    }

    #[test]
    fn non_empty_license_no_resources() {
        let package = Package {
            license_id: Some("foobar".into()),
            ..Default::default()
        };

        assert_eq!(package.license(), License::Unknown);
    }

    #[test]
    fn empty_license_single_resource() {
        let package = Package {
            license_id: Some("".into()),
            resources: vec![CkanResource {
                license: Some("foobar".into()),
                ..Default::default()
            }],
            ..Default::default()
        };

        assert_eq!(package.license(), License::Unknown);
    }

    #[test]
    fn empty_license_multiple_matching_resources() {
        let package = Package {
            license_id: Some("".into()),
            resources: vec![
                CkanResource {
                    license: Some("cc-by/4.0".into()),
                    ..Default::default()
                },
                CkanResource {
                    license: Some("cc-by/4.0".into()),
                    ..Default::default()
                },
            ],
            ..Default::default()
        };

        assert_eq!(package.license(), License::CcBy40);
    }

    #[test]
    fn empty_license_multiple_distinct_resources() {
        let package = Package {
            license_id: None,
            resources: vec![
                CkanResource {
                    license: Some("cc-by".into()),
                    ..Default::default()
                },
                CkanResource {
                    license: Some("copyright".into()),
                    ..Default::default()
                },
            ],
            ..Default::default()
        };

        assert_eq!(package.license(), License::MixedClosed);
    }

    #[test]
    fn non_empty_license_multiple_distinct_resources() {
        let package = Package {
            license_id: Some("foobar".into()),
            resources: vec![
                CkanResource {
                    license: Some("cc-by".into()),
                    ..Default::default()
                },
                CkanResource {
                    license: Some("copyright".into()),
                    ..Default::default()
                },
            ],
            ..Default::default()
        };

        assert_eq!(package.license(), License::Unknown);
    }
}
