use std::fmt::Write;
use std::iter::successors;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use geo::Rect;
use hashbrown::HashMap;
use itertools::Itertools;
use poppler::Document;
use regex::Regex;
use scraper::{CaseSensitivity, ElementRef, Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_pages, collect_text, make_key, parse_german_coord, parse_short_year,
        remove_pom_suffix, select_first_text, yesterday, GermanDate,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, IdentifiedDataset, Language, License, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_search_index(dir, client, source, selectors).await?;

    let (count1, results1, errors1) =
        fetch_downloadable_documents(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_shipping_levels(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_ice_reports(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_search_index(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let (count, results, errors) =
        fetch_search_index_page(dir, client, source, selectors, 1).await?;

    let pages = count.div_ceil(source.batch_size);

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_search_index_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_search_index_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let count;
    let articles;
    let publications;

    {
        let mut url = source
            .url
            .join("SiteGlobals/Forms/Suche/Expertensuche_Formular.html")?;

        url.query_pairs_mut()
            .append_pair("templateQueryString", " ")
            .append_pair(
                "gtp",
                &format!("%267bf15d2a-fa20-4c99-bfca-6c401d1417f1_list%3D{page}"),
            )
            .append_pair("resultsPerPage", &source.batch_size.to_string());

        let text = client
            .fetch_text(source, format!("search-index-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let count_text = collect_text(
            document
                .select(&selectors.search_index_count)
                .next()
                .ok_or_else(|| anyhow!("Missing count on page {page}"))?
                .text(),
        );

        count = selectors
            .search_index_count_value
            .captures(&count_text)
            .ok_or_else(|| anyhow!("Malformed count on page {page}"))?
            .get(1)
            .unwrap()
            .as_str()
            .replace('.', "")
            .parse()?;

        articles = document
            .select(&selectors.search_index_articles)
            .map(|element| {
                let href = element
                    .select(&selectors.links)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link for article"))?
                    .attr("href")
                    .unwrap()
                    .to_owned();

                let title = select_first_text(element, &selectors.title, "title for article")?;

                let modified = element
                    .select(&selectors.date)
                    .next()
                    .map(|date| {
                        let text = collect_text(date.text());
                        text.parse::<GermanDate>()
                    })
                    .transpose()?
                    .map(Into::into);

                Ok(Article {
                    href,
                    title,
                    modified,
                })
            })
            .collect::<Vec<Result<_>>>();

        publications = document
            .select(&selectors.search_index_publications)
            .map(|element| {
                let href = element
                    .select(&selectors.links)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link for publication"))?
                    .attr("href")
                    .unwrap()
                    .to_owned();

                let title = select_first_text(element, &selectors.title, "title for publication")?;

                let modified = element
                    .select(&selectors.date)
                    .next()
                    .map(|date| {
                        let text = collect_text(date.text());
                        Date::parse(&text, format_description!("[day].[month].[year]"))
                    })
                    .transpose()?;

                Ok(Publication {
                    href,
                    title,
                    modified,
                })
            })
            .collect::<Vec<Result<_>>>();
    }

    let (results, errors) = fetch_many(0, 0, articles, |article| async move {
        let article = article?;

        let url = source.url.join(&article.href)?;

        let key = make_key(url.path()).into_owned();

        let dataset = {
            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            // TODO: use breadcrumb bar for better titles

            let content = match document.select(&selectors.articles_content).next() {
                Some(content) => content,
                None => return Ok((1, 0, 0)),
            };

            let description = collect_text(content.text());

            let resources = content
                .select(&selectors.links)
                .map(|link| {
                    let href = link.attr("href").unwrap();
                    let description = collect_text(link.text());

                    Ok(Resource {
                        r#type: ResourceType::WebPage,
                        description: Some(description),
                        url: source.url.join(href)?.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                })
                .collect::<Result<SmallVec<_>>>()?;

            let language = if url.as_str().contains("/DE/Service/LeichteSprache/") {
                Language::GermanEasy
            } else {
                Language::German
            };

            Dataset {
                title: article.title,
                types: smallvec![Type::Text {
                    text_type: TextType::Editorial
                }],
                description: Some(description),
                modified: article.modified,
                resources,
                source_url: url.into(),
                origins: source.origins.clone(),
                license: License::OtherOpen, // https://www.elwis.de/DE/Service/Haftungsausschluss-und-Nutzungsbedingungen/Haftungsausschluss-und-Nutzungsbedingungen-node.html states that the content is openData.
                language,
                ..Default::default()
            }
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    let (results, errors) = fetch_many(results, errors, publications, |publication| async move {
        let publication = publication?;

        let url = source.url.join(&publication.href)?;

        let key = make_key(url.path()).into_owned();

        let description = if url.path().ends_with(".pdf") {
            let bytes = client
                .fetch_pdf(source, format!("{key}.isol"), &url)
                .await?;

            let document = Document::from_bytes(&bytes, None)?;

            Some(collect_pages(&document, 0..3))
        } else {
            None
        };

        let dataset = Dataset {
            title: publication.title,
            description,
            modified: publication.modified,
            source_url: url.into(),
            origins: source.origins.clone(),
            license: License::OtherOpen, // https://www.elwis.de/DE/Service/Haftungsausschluss-und-Nutzungsbedingungen/Haftungsausschluss-und-Nutzungsbedingungen-node.html states that the content is openData.
            language: Language::German,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

struct Article {
    href: String,
    title: String,
    modified: Option<Date>,
}

struct Publication {
    href: String,
    title: String,
    modified: Option<Date>,
}

async fn fetch_downloadable_documents(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = &Url::parse("https://vorhersage.bafg.de/14-Tage-Vorhersage/")?;

    let mut stations = HashMap::<String, Vec<(String, String)>>::new();

    {
        let text = client
            .fetch_text(source, "14-tage-rheinvorhersagen".to_owned(), url)
            .await?;

        let document = Html::parse_document(&text);

        for link in document.select(&selectors.links).skip(1) {
            let href = link.attr("href").unwrap().to_owned();
            let text = collect_text(link.text());

            let name = text
                .split_once('_')
                .map_or(text.as_str(), |(prefix, _suffix)| prefix);

            stations.entry_ref(name).or_default().push((href, text));
        }
    }

    let count = stations.len();

    let (results, errors) = fetch_many(0, 0, stations, |(name, hrefs)| async move {
        let mut resources = smallvec![Resource {
            r#type: ResourceType::WebPage,
            description: Some("Weiterführende Informationen".to_owned()),
            url: "https://www.elwis.de/DE/Service/Wasserstaende/14-Tage-Vorhersage-Rhein/14-Tage-Vorhersage-Rhein-node.html".to_owned(),
            ..Default::default()
        }];

        for (href, text) in hrefs {
            resources.push(
                Resource {
                    r#type: ResourceType::Document,
                    description: Some(text),
                    url: url.join(&href)?.into(),
                    primary_content: true,
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        let key = format!("14-tage-rheinvorhersagen-{name}");
        let title = format!("14-Tage-Rheinvorhersagen, Pegel {name}");

        let dataset = Dataset {
            title,
            types: smallvec![Type::Text {
                text_type: TextType::Editorial
            }],
            origins: source.origins.clone(),
            license: License::OtherOpen, // https://www.elwis.de/DE/Service/Haftungsausschluss-und-Nutzungsbedingungen/Haftungsausschluss-und-Nutzungsbedingungen-node.html states that the content is openData.
            language: Language::German,
            source_url: url.clone().into(),
            regions: smallvec![Region::Other(name.into())],
            resources,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_shipping_levels(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = source
        .url
        .join("DE/dynamisch/gewaesserkunde/wasserstaende/")?;

    let levels = {
        let text = client
            .fetch_text(source, "wasserstaende".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.level_links)
            .map(|link| {
                let href = link.attr("href").unwrap().to_owned();
                let text = collect_text(link.text());

                (href, text)
            })
            .collect::<Vec<_>>()
    };

    let count = levels.len();

    let (results, errors) = fetch_many(0, 0, levels, |(href, text)| async move {
        let key = format!("wasserstaende-{}", make_key(&text));
        let url = source.url.join(&href)?;

        let name = remove_pom_suffix(&text);

        let mut description = "Verfügbare Messwerte:".to_owned();
        let mut time_ranges = SmallVec::new();
        let mut bounding_boxes = SmallVec::new();
        let resources;

        {
            let text = client.fetch_text(source, key.clone(), &url).await?;

            let document = Html::parse_document(&text);

            for table in document.select(&selectors.level_tables) {
                if table.select(&selectors.level_data_table).next().is_some() {
                    let dates = table
                        .select(&selectors.level_date_cells)
                        .map(|cell| -> Result<Date> {
                            let text = collect_text(cell.text());

                            let (_prefix, suffix) = text
                                .split_once(' ')
                                .ok_or_else(|| anyhow!("Missing weekday: {text}"))?;

                            let date = parse_short_year(2000, suffix)?;

                            Ok(date)
                        })
                        .collect::<Result<Vec<_>>>()?;

                    if !dates.is_empty() {
                        let min_date = *dates.iter().min().unwrap();
                        let max_date = *dates.iter().max().unwrap();

                        time_ranges.push((min_date, max_date).into());
                    }
                } else {
                    for row in table.select(&selectors.level_rows).skip(1) {
                        let mut cells = row.select(&selectors.level_cells);

                        let mut next_cell_text = move |field: &str| -> Result<String> {
                            let cell = cells.next().ok_or_else(|| anyhow!("Missing {field}"))?;
                            let text = collect_text(cell.text());
                            Ok(text)
                        };

                        let short_name = next_cell_text("short name")?;
                        let long_name = next_cell_text("long name")?;

                        write!(&mut description, " {long_name} ({short_name})").unwrap();

                        if short_name == "Lage" {
                            let coord = next_cell_text("coordinates")?;

                            let coord = parse_german_coord(&coord)?;

                            bounding_boxes.push(Rect::new(coord, coord));
                        }
                    }
                }
            }

            resources = document
                .select(&selectors.level_plots)
                .filter(|link| collect_text(link.text()).contains("Grafische Darstellung..."))
                .map(|link| {
                    Ok(Resource {
                        r#type: ResourceType::WebPage,
                        description: Some("Grafische Darstellung".to_owned()),
                        url: source.url.join(link.attr("href").unwrap())?.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                })
                .collect::<Result<SmallVec<_>>>()?;
        }

        let title = format!("Wasserstände & Vorhersagen am schifffahrtsrelevanten Pegel {name}");

        let dataset = Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::News
            }],
            origins: source.origins.clone(),
            license: License::OtherOpen, // https://www.elwis.de/DE/Service/Haftungsausschluss-und-Nutzungsbedingungen/Haftungsausschluss-und-Nutzungsbedingungen-node.html states that the content is openData.
            language: Language::German,
            source_url: url.clone().into(),
            regions: smallvec![Region::Other(name.into())],
            time_ranges,
            bounding_boxes,
            resources,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_ice_reports(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let from = Date::from_ordinal_date(2005, 1)?;
    let until = yesterday();

    let dates = successors(Some(from), |date| Some(date.next_day().unwrap()))
        .take_while(|date| date != &until);

    let mut base_url = source
        .url
        .join("DE/dynamisch/gewaesserkunde/eislage/index.php")?;

    base_url
        .query_pairs_mut()
        .append_pair("mod_listing", "1")
        .append_pair("list_settings_submitted", "true")
        .append_pair("cmd_search", "Suche+starten...")
        .append_pair("cmd_display", "Anzeige+anpassen")
        .append_pair("AIR_TEMP", "true")
        .append_pair("STATUS", "true")
        .append_pair("WATER_TEMP", "true")
        .append_pair("PROGRESS", "true")
        .append_pair("ICE_TYPE", "true")
        .append_pair("NOTE", "true")
        .append_pair("ICE_THICKNESS", "true")
        .append_pair("WSD_INFORMATIONS", "true")
        .append_pair("COORDINATES", "true")
        .append_pair("PROGNOSIS", "true");

    let base_url = &base_url;

    let (results, errors) = fetch_many(0, 0, dates, |date| async move {
        // FIXME: The padding is required to stay compatible with the Python code.
        let day = format!("{:02}", date.day());
        let month = u8::from(date.month()).to_string();
        let year = date.year().to_string();

        let key = format!("ice-report-{day}-{month}-{year}");

        // TODO: We need to add the query parameters `cmd_all=Alles+wählen` and
        // `ws_id[<id>]=1` where `<id>` is the waterway identifier.
        let mut url = base_url.clone();

        url.query_pairs_mut()
            .append_pair("datum_tag", &day)
            .append_pair("datum_monat", &month)
            .append_pair("datum_jahr", &year);

        let datasets = {
            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            if document
                .select(&selectors.ice_report_error)
                .next()
                .is_some()
            {
                return Ok((0, 0, 0));
            }

            let mut stations = Vec::new();
            let mut curr_station = Vec::new();

            for row in document.select(&selectors.ice_report_rows) {
                if row
                    .value()
                    .has_class("ws_header", CaseSensitivity::AsciiCaseInsensitive)
                    && !curr_station.is_empty()
                {
                    stations.push(curr_station);
                    curr_station = Vec::new()
                }
                curr_station.push(row);
            }

            if !curr_station.is_empty() {
                stations.push(curr_station);
            }

            stations
                .into_iter()
                .map(|station| create_ice_report(source, selectors, date, &key, &url, station))
                .collect::<Vec<_>>()
        };

        let count = datasets.len();

        let (results, errors) = fetch_many(0, 0, datasets, |dataset| async {
            let dataset = dataset?;

            write_dataset(dir, client, source, dataset.id, dataset.value).await
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

fn create_ice_report(
    source: &Source,
    selectors: &Selectors,
    date: Date,
    key: &str,
    url: &Url,
    station: Vec<ElementRef>,
) -> Result<IdentifiedDataset> {
    let name = collect_text(station[0].text());

    let title = format!(
        "Eislage-Meldungen für {name} am {}",
        date.format(format_description!("[day].[month].[year]"))?,
    );

    let description = station
        .iter()
        .find(|row| {
            row.value()
                .has_class("eisl_header", CaseSensitivity::AsciiCaseInsensitive)
        })
        .map(|row| {
            let values = row
                .select(&selectors.ice_report_cells)
                .map(|cell| collect_text(cell.text()))
                .join(", ");

            format!("Verfügbare Werte: {values}")
        });

    let region = Region::Other(
        name.split_once('(')
            .map_or(&*name, |(prefix, _suffix)| prefix)
            .into(),
    );

    let mut bounding_boxes = SmallVec::new();
    let mut modified = Date::MIN;

    for row in station {
        // This CSS class happens to select the rows with measurement instead of the subheadings.
        if !row
            .value()
            .has_class("hellblau", CaseSensitivity::AsciiCaseInsensitive)
        {
            continue;
        }

        let mut coords = SmallVec::<[_; 2]>::new();

        for cell in row.select(&selectors.ice_report_cells) {
            let text = collect_text(cell.text());

            if let Some(match_) = selectors.ice_report_coords.find(&text) {
                let coord = parse_german_coord(match_.as_str())?;

                coords.push(coord);
            } else if let Some(match_) = selectors.ice_report_dates.find(&text) {
                let date =
                    Date::parse(match_.as_str(), format_description!("[day].[month].[year]"))?;

                modified = modified.max(date);
            }
        }

        match &coords[..] {
            [] => (),
            [coord] => bounding_boxes.push(Rect::new(*coord, *coord)),
            [min, max] => bounding_boxes.push(Rect::new(*min, *max)),
            coords => tracing::error!(
                "Unexpected number of coordinates {} in ice report of {}",
                coords.len(),
                date
            ),
        }
    }

    let issued = Some(date);
    let modified = (modified != Date::MIN).then_some(modified);

    let id = format!("{key}-{}", make_key(&name));

    let value = Dataset {
        title,
        description,
        types: smallvec![Type::Text {
            text_type: TextType::Editorial
        }],
        regions: smallvec![region],
        bounding_boxes,
        issued,
        modified,
        source_url: url.clone().into(),
        origins: source.origins.clone(),
        license: License::OtherOpen, // https://www.elwis.de/DE/Service/Haftungsausschluss-und-Nutzungsbedingungen/Haftungsausschluss-und-Nutzungsbedingungen-node.html states that the content is openData.
        language: Language::German,
        ..Default::default()
    };

    Ok(IdentifiedDataset { id, value })
}

selectors! {
    links: "a[href]",
    title: "h3",
    date: "span.date",
    search_index_count: "p.solrSortResults",
    search_index_count_value: r"von\s+insgesamt\s+([\d\.]+)\s+für\sSuchbegriff" as Regex,
    search_index_articles: "#content div.teaser.type-1.Basepage.row",
    search_index_publications: "#content div.teaser.type-1.Publication.row",
    articles_content: "#content",
    level_links: "tr td.b3.PegelId a[href]:not([href*='Grafik'])",
    level_tables: "#main table",
    level_data_table: "td > b.pegelTitle",
    level_date_cells: "td[colspan='2'] > b",
    level_rows: "tr",
    level_cells: "td",
    level_plots: "#main a[href]",
    ice_report_error: "p.error",
    ice_report_rows: "table.eislagen tr",
    ice_report_cells: "td",
    ice_report_coords: r"\d+°\s*N\s*[\d\.]+'\s*E\s*" as Regex,
    ice_report_dates: r"\d{2}\.\d{2}\.\d{4}" as Regex,
}
