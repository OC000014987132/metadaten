use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use cow_utils::CowUtils;
use miniproj::{get_projection, Projection};
use regex::Regex;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_str as from_json_str;
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date, Duration};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_text, contains_or, make_key, make_suffix_key, point_like_bounding_box, select_text,
        yesterday,
    },
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, Language, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();
    let proj = get_projection(25832).unwrap();

    let (count1, results1, errors1) = fetch_nid(dir, client, source, &selectors, proj).await?;

    let (count2, results2, errors2) = fetch_api(dir, client, source, proj).await?;

    Ok((count1 + count2, results1 + results2, errors1 + errors2))
}

async fn fetch_nid(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: &dyn Projection,
) -> Result<(usize, usize, usize)> {
    let mut all_links = fetch_mainlinks(client, source, selectors).await?;
    let l2_links = fetch_sublinks(client, source, &all_links, selectors).await?;
    let l3_links = fetch_sublinks(client, source, &l2_links, selectors).await?;
    all_links.extend(l2_links);
    all_links.extend(l3_links);
    all_links.retain(|link| link.contains("/tabellen"));
    all_links.sort_unstable();
    all_links.dedup();

    let mut sites = Vec::<Site>::new();
    let mut alarm_maps = Vec::new();

    for link in all_links {
        let parameter = if link.contains("/abfluss") {
            Parameter::Abfluss
        } else if link.contains("/niederschlag") {
            Parameter::Niederschlag
        } else if link.contains("/grundwasser") {
            Parameter::Grundwasser
        } else if link.contains("/speicher") {
            Parameter::Speicher
        } else if link.contains("/sauerstoff") {
            Parameter::Sauerstoffkonzentration
        } else if link.contains("wassertemperatur") {
            Parameter::Wassertemperatur
        } else if selectors.is_alarmplan_regex.is_match(&link) {
            alarm_maps.push(link);
            continue;
        } else {
            tracing::error!("No parameter known for {link}");
            continue;
        };

        let url = Url::parse(&link)?;
        let key = make_key(&link).into_owned();
        let text = client.fetch_text(source, key, &url).await?;
        let document = Html::parse_document(&text);

        for row in document.select(&selectors.rows) {
            let mut cells = row.select(&selectors.cells);

            let site = cells.next().ok_or_else(|| anyhow!("Missing name column"))?;
            let name = collect_text(site.text());

            let link1 = site
                .select(&selectors.link)
                .next()
                .ok_or_else(|| anyhow!("Missing link"))?;
            let href = link1.attr("href").unwrap().to_owned();

            // the second variable is either Lkr, Gewässer or Höhe
            let second_variable = cells
                .next()
                .ok_or_else(|| anyhow!("Missing second_variable column"))?
                .text()
                .collect::<CompactString>();

            // the third variable is either Lkr or Grundwasserleiter
            let third_variable = cells
                .next()
                .ok_or_else(|| anyhow!("Missing third_variable column"))?
                .text()
                .collect::<CompactString>();

            let site = Site {
                name,
                href,
                second_variable,
                third_variable,
                parameter,
            };

            sites.push(site);
        }
    }

    sites.sort_unstable();
    sites.dedup();

    let count = sites.len();

    let (results, errors) = fetch_many(0, 0, sites, |site| {
        write_datasets_nid(dir, client, source, site, selectors, proj)
    })
    .await;

    let (results, errors) = fetch_many(results, errors, alarm_maps, |link| {
        add_alarm_map(dir, client, source, selectors, link)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_mainlinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let text = client
        .fetch_text(source, "mainlinks".to_owned(), &source.url)
        .await?;

    let document = Html::parse_document(&text);

    let links = document
        .select(&selectors.tablelinks)
        .map(|element| element.attr("href").unwrap())
        .filter(|href| !href.contains("lage"))
        .filter(|href| !href.contains("ereignisse"))
        .filter(|href| !href.contains("/hilfe"))
        .filter(|href| !href.contains("/links"))
        .filter(|href| !href.contains("/wir"))
        .map(|href| href.to_owned())
        .collect();

    Ok(links)
}

async fn fetch_sublinks(
    client: &Client,
    source: &Source,
    main_links: &Vec<String>,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let mut links = Vec::new();

    for link in main_links {
        let key = make_key(link).into_owned();
        let url = source.url.join(link)?;

        let text = client.fetch_text(source, key, &url).await?;
        let document = Html::parse_document(&text);

        links.extend(
            document
                .select(&selectors.sublink_selector)
                .map(|element| element.attr("href").unwrap())
                .map(|s| s.to_owned())
                .collect::<Vec<String>>(),
        );
    }

    Ok(links)
}

async fn write_datasets_nid(
    dir: &Dir,
    client: &Client,
    source: &Source,
    site: Site,
    selectors: &Selectors,
    proj: &dyn Projection,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&site.href).into_owned();

    let dataset = {
        let url = fetch_stammdaten(client, source, &site.href, selectors).await?;
        let key = make_key(url.path()).into_owned();

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;
        let document = Html::parse_document(&text);

        let stammdaten = select_text(&document, &selectors.stammdaten_text);

        let site_id = selectors
            .messstellen_id_regex
            .captures(&stammdaten)
            .map(|captures| captures.get(1).unwrap().as_str());

        let municipality = selectors
            .municipality_regex
            .captures(&stammdaten)
            .map(|captures| captures.get(1).unwrap().as_str());

        let region = municipality.map_or(Region::BY, |municipality| {
            contains_or(municipality, Region::BY)
        });

        let ostwert = selectors
            .ostwert_regex
            .captures(&stammdaten)
            .map(|captures| captures[1].parse::<f64>())
            .transpose()?;

        let nordwert = selectors
            .nordwert_regex
            .captures(&stammdaten)
            .map(|captures| captures[1].parse::<f64>())
            .transpose()?;

        let organisations = selectors
            .betreiber_regex
            .captures(&stammdaten)
            .map(|captures| Organisation::Other {
                name: captures[1].to_owned(),
                role: OrganisationRole::Operator,
                websites: Default::default(),
            })
            .into_iter()
            .collect();

        let name = site.name;

        let second_variable = if !site.second_variable.is_empty() {
            site.second_variable
        } else {
            "unbekannt".into()
        };

        let third_variable = if !site.third_variable.is_empty() {
            site.third_variable
        } else {
            "unbekannt".into()
        };

        let title = if site.parameter == Parameter::Niederschlag {
            format!("{} ({})", site.parameter.title(), name)
        } else {
            format!("{} {} ({})", site.parameter.title(), second_variable, name)
        };

        let description = if let Some(municipality) = municipality {
            if let Some(site_id) = site_id {
                if site.parameter != Parameter::Grundwasser {
                    format!("Die Messstelle {name} (Messstellen-Nr: {site_id}) befindet sich im Gewässer {second_variable}, im Landkreis {municipality}.")
                } else {
                    format!(
                        "Die Messstelle {name} befindet sich im Grundwasserleiter {third_variable}, im Landkreis {municipality}.")
                }
            } else {
                format!("Die Messstelle {name} befindet sich im Gewässer {second_variable}, im Landkreis {municipality}.")
            }
        } else if let Some(site_id) = site_id {
            if site.parameter != Parameter::Grundwasser {
                format!(
                    "Die Messstelle {name} (Messstellen-Nr: {site_id}) befindet sich im Gewässer {second_variable}.")
            } else {
                format!(
                    "Die Messstelle {name} befindet sich im Grundwasserleiter {third_variable}."
                )
            }
        } else {
            format!("Die Messstelle {name} befindet sich im Gewässer {second_variable}.")
        };

        let (domain, measured_variable) = match site.parameter {
            Parameter::Abfluss => (Domain::Rivers, "Abfluss"),
            Parameter::Wassertemperatur => (Domain::Rivers, "Temperatur"),
            Parameter::Speicher => (Domain::Surfacewater, "Speicher"),
            Parameter::Grundwasser => (Domain::Groundwater, "Grundwasserstand"),
            Parameter::Niederschlag => (Domain::Air, "Niederschlag"),
            Parameter::Sauerstoffkonzentration => (Domain::Rivers, "Sauerstoffkonzentration"),
        };

        let station_id = match site_id {
            Some(site_id) => site_id.into(),
            None => name.into(),
        };

        let types = smallvec![Type::Measurements {
            domain,
            station: Some(Station {
                id: Some(station_id),
                ..Default::default()
            }),
            measured_variables: smallvec![measured_variable.to_owned()],
            methods: Default::default(),
        }];

        let mut regions = smallvec![region];

        let bounding_boxes = if let (Some(lon), Some(lat)) = (ostwert, nordwert) {
            let (lon, lat) = proj.projected_to_deg(lon, lat);

            if matches!(domain, Domain::Rivers | Domain::Surfacewater) {
                regions.extend(WISE.match_shape(lon, lat).map(Region::Watershed));
            }

            smallvec![point_like_bounding_box(lat, lon)]
        } else {
            Default::default()
        };

        Dataset {
            title,
            description: Some(description),
            types,
            regions,
            bounding_boxes,
            organisations,
            language: Language::German,
            license: License::CcBy40,
            origins: source.origins.clone(),
            source_url: site.href,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_stammdaten(
    client: &Client,
    source: &Source,
    entry_link: &str,
    selectors: &Selectors,
) -> Result<Url> {
    let url = source.url.join(entry_link)?;
    let key = format!("{}.isol", make_key(url.path()));

    let text = client.fetch_text(source, key, &url).await?;
    let document = Html::parse_document(&text);

    let url = document
        .select(&selectors.stammdaten)
        .map(|element| element.attr("href").unwrap())
        .find(|element| element.contains("stammdaten"))
        .ok_or_else(|| anyhow!("missing link to Stammdaten at `{entry_link}`"))?
        .parse()?;

    Ok(url)
}

async fn fetch_api(
    dir: &Dir,
    client: &Client,
    source: &Source,
    proj: &dyn Projection,
) -> Result<(usize, usize, usize)> {
    let items = {
        let mut url = source.url.join("/rest/wassertemperatur")?;

        url.query_pairs_mut()
            .append_pair("aufruf", "owertEingehalten")
            .append_pair("beginn", "2024-04-15");

        let text = client
            .fetch_text(source, "wassertemp".to_owned(), &url)
            .await?;

        from_json_str::<Vec<WaterTemperature>>(&text)?
    };

    let count = items.len();

    let (results, errors) = fetch_many(0, 0, items, |item| {
        write_datasets_wassertemp(dir, client, source, proj, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn write_datasets_wassertemp(
    dir: &Dir,
    client: &Client,
    source: &Source,
    proj: &dyn Projection,
    item: WaterTemperature,
) -> Result<(usize, usize, usize)> {
    let WaterTemperature {
        id,
        site_name,
        water_body,
        nordwert,
        ostwert,
        betreiber,
        fish,
        name,
        modified,
    } = item;

    let title = format!("Messstelle der Wassertemperatur des Niedrigwasser-Informationsdienst Bayern (Ort: {name}, Messstellen-Nr: {id})");

    let description = if let Some(fish) = fish {
        format!("Die Messstelle \"{site_name}, {name}\" am Gewässer {water_body}. Die Bezeichnung der dortigen Fischgemeinschaft lautet {fish}.")
    } else {
        format!("Die Messstelle \"{site_name}, {name}\" am Gewässer {water_body}.")
    };

    let modified = modified
        .map(|text| Date::parse(&text, format_description!("[year]-[month]-[day]")))
        .transpose()?;

    let site_name = site_name.cow_to_lowercase();
    let site_name = site_name.cow_replace(' ', "-");
    let site_name = site_name.cow_replace('ä', "ae");
    let site_name = site_name.cow_replace('ö', "oe");
    let site_name = site_name.cow_replace('ü', "ue");
    let site_name = site_name.cow_replace(&['.', '(', ')'][..], "");

    let source_url = source
        .url
        .join(&format!("/wassertemperatur/bayern/{site_name}-{id}"))?;

    let organisations = smallvec![Organisation::Other {
        name: betreiber.to_owned(),
        role: OrganisationRole::Operator,
        websites: Default::default(),
    }];

    let types = smallvec![Type::Measurements {
        domain: Domain::Surfacewater,
        station: Some(Station {
            id: Some(id.to_string().into()),
            ..Default::default()
        }),
        measured_variables: smallvec!["Temperatur".to_owned()],
        methods: Default::default(),
    }];

    let mut regions = smallvec![contains_or(name, Region::BY)];

    let bounding_boxes = {
        let lon = ostwert.parse::<f64>()?;
        let lat = nordwert.parse::<f64>()?;

        let (lon, lat) = proj.projected_to_deg(lon, lat);

        regions.extend(WISE.match_shape(lon, lat).map(Region::Watershed));

        smallvec![point_like_bounding_box(lat, lon)]
    };

    let dataset = Dataset {
        title,
        description: Some(description),
        types,
        regions,
        bounding_boxes,
        organisations,
        modified,
        language: Language::German,
        license: License::CcBy40,
        origins: source.origins.clone(),
        source_url: source_url.into(),
        ..Default::default()
    };

    write_dataset(
        dir,
        client,
        source,
        format!("wassertemperatur-{id}"),
        dataset,
    )
    .await
}

async fn add_alarm_map(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: String,
) -> Result<(usize, usize, usize)> {
    let key = make_suffix_key(&link, &source.url).into_owned();

    let title;
    let description;
    let url = source.url.join(&link)?;
    let resources;

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;
        let document = Html::parse_document(&text);

        title = select_text(&document, &selectors.alarmplan_title);
        description = Some(select_text(&document, &selectors.alarmplan_text));

        resources = document
            .select(&selectors.alarmplan_text)
            .flat_map(|textelem| {
                textelem.select(&selectors.hrefs).map(|elem| {
                    let href = elem.attr("href").unwrap();
                    let url = source.url.join(href)?;
                    let description = collect_text(elem.text());
                    Ok(Resource {
                        url: url.into(),
                        r#type: ResourceType::WebPage,
                        description: Some(description),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                })
            })
            .collect::<Result<SmallVec<_>>>()?;
    }
    let end = yesterday();

    let start = end - 6 * Duration::DAY;

    let dataset = Dataset {
        title,
        description,
        types: smallvec![Type::MapService],
        source_url: url.into(),
        language: Language::German,
        license: License::CcBy40,
        origins: source.origins.clone(),
        time_ranges: smallvec![(start, end).into()],
        regions: smallvec![Region::BY],
        resources,
        ..Default::default()
    };

    write_dataset(dir, client, source, key.to_string(), dataset).await
}

#[derive(Debug, PartialEq, Ord, PartialOrd, Eq, Clone)]
struct Site {
    name: String,
    href: String,
    second_variable: CompactString,
    third_variable: CompactString,
    parameter: Parameter,
}

#[derive(Debug, PartialEq, Ord, PartialOrd, Eq, Clone, Copy)]
enum Parameter {
    Abfluss,
    Niederschlag,
    Grundwasser,
    Speicher,
    Sauerstoffkonzentration,
    Wassertemperatur,
}

impl Parameter {
    fn title(&self) -> &'static str {
        match self {
            Self::Abfluss => "Abfluss",
            Self::Niederschlag => "Niederschlag",
            Self::Grundwasser => "Grundwasser",
            Self::Speicher => "Speicher",
            Self::Sauerstoffkonzentration => "Sauerstoffkonzentration",
            Self::Wassertemperatur => "Wassertemperatur",
        }
    }
}

#[derive(Debug, Deserialize)]
struct WaterTemperature {
    #[serde(rename = "Messstellennummer")]
    id: String,
    #[serde(rename = "Pegelname")]
    site_name: String,
    #[serde(rename = "Gewaesser")]
    water_body: String,
    #[serde(rename = "Nordwert")]
    nordwert: String,
    #[serde(rename = "Ostwert")]
    ostwert: String,
    #[serde(rename = "Betreiber")]
    betreiber: String,
    #[serde(rename = "Fischgemeinschaft")]
    fish: Option<String>,
    #[serde(rename = "Name")]
    name: CompactString,
    #[serde(rename = "Aktualisiert")]
    modified: Option<String>,
}

selectors! {
    tablelinks: "div#navi_horizontal a[href]",

    rows: "tbody > tr",
    cells: "td",
    link: "a[href]",

    sublink_selector: "#navi_horizontal_sub a[href], h4+ ul li+ li a[href]",

    stammdaten: "#navi_links_3c a[href]",
    stammdaten_text: "div.col > p",

    messstellen_id_regex :r"Messstellen-Nr.:\s+(\w+)" as Regex,
    municipality_regex: r"Landkreis:\s+(.+?)\s+(?:\w+:|Zuständiges)" as Regex,

    ostwert_regex :r"Ostwert:\s+(\w+)" as Regex,
    nordwert_regex :r"Nordwert:\s+(\w+)" as Regex,
    betreiber_regex: r"(?:Betreiber:|Zuständiges\sAmt:)\s+(.+?)\s+\w+:" as Regex,

    is_alarmplan_regex: r"/(a[md]o)/" as Regex,
    alarmplan_title: "h1",
    alarmplan_text: "p",
    hrefs: "a[href]",
}
