use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use itertools::Itertools;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::smallvec;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_first_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, Resource, ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_projects(dir, client, source, selectors).await?;

    let (count1, results1, errors1) = fetch_plans(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_impacts(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_habitats(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_species(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_bird_species(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    count += 1;
    match fetch_homepage(dir, client, source, selectors).await {
        Ok(()) => results += 1,
        Err(err) => {
            tracing::error!("{err:#}");
            errors += 1;
        }
    }

    count += 1;
    match fetch_downloads(dir, client, source, selectors).await {
        Ok(()) => results += 1,
        Err(err) => {
            tracing::error!("{err:#}");
            errors += 1;
        }
    }

    Ok((count, results, errors))
}

async fn fetch_projects(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let links = {
        let url = source.url.join("FFHVP/Projekt.jsp?start")?;

        let text = client
            .fetch_text(source, "project".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        let sublinks = {
            let url = source.url.join(&link)?;

            let key = format!("project-{}", make_key(&link));

            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            let content_div = document
                .select(&selectors.content_div)
                .nth(1)
                .ok_or_else(|| anyhow!("Missing content div at {link}"))?;

            content_div
                .select(&selectors.direct_links)
                .map(|element| element.attr("href").unwrap().to_owned())
                .collect::<Vec<_>>()
        };

        let count = sublinks.len();

        let (results, errors) = fetch_many(0, 0, sublinks, |sublink| async move {
            let url = source.url.join(&format!("FFHVP/{sublink}"))?;

            let key = format!("project-{}", make_key(&sublink));

            let title;
            let description;

            {
                let text = client
                    .fetch_text(source, format!("{key}.isol"), &url)
                    .await?;

                let document = Html::parse_document(&text);

                let content_div = document
                    .select(&selectors.content_div)
                    .nth(1)
                    .ok_or_else(|| anyhow!("Missing content div at {url}"))?;

                title = select_first_text(content_div, &selectors.title, "title")?;

                let content_row = content_div
                    .select(&selectors.project_content_row)
                    .next()
                    .ok_or_else(|| anyhow!("Missing content row"))?;

                description =
                    select_first_text(content_row, &selectors.project_description, "description")?;
            }

            let dataset = Dataset {
                title,
                description: Some(description),
                types: smallvec![Type::Text {
                    text_type: TextType::Editorial
                }],
                tags: vec![Tag::FFHRL, Tag::Other("Projekt".into())],
                language: Language::German,
                origins: source.origins.clone(),
                source_url: url.into(),
                ..Default::default()
            };

            write_dataset(dir, client, source, key, dataset).await
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_plans(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let links = {
        // The pages "Vorhabensbezogene Pläne" and "Raumordnungsverfahren"
        // only contain links to already fetched projects.
        let url = source
            .url
            .join("FFHVP/Plan.jsp?q.t=pla&q.p=%&button_suche=true")?;

        let text = client.fetch_text(source, "plan".to_owned(), &url).await?;

        let document = Html::parse_document(&text);

        let content_div = document
            .select(&selectors.content_div)
            .nth(1)
            .ok_or_else(|| anyhow!("Missing content div at {url}"))?;

        content_div
            .select(&selectors.direct_links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };

    let count = links.len();

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        let url = source.url.join(&format!("FFHVP/{link}"))?;

        let key = format!("plan-{}", make_key(&link));

        let title;
        let description;
        let comment;

        {
            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            let content_div = document
                .select(&selectors.content_div)
                .nth(1)
                .ok_or_else(|| anyhow!("Missing content div at {url}"))?;

            let element = content_div
                .select(&selectors.title)
                .next()
                .ok_or_else(|| anyhow!("Missing title"))?;

            title = collect_text(element.text());

            let mut texts = element
                .next_siblings()
                .filter_map(|node| node.value().as_text())
                .map(|text| text.text.as_ref().to_owned());

            description = texts.by_ref().take(2).join(" ");
            comment = texts.nth(2);
        }

        let dataset = Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial
            }],
            comment,
            tags: vec![Tag::FFHRL, Tag::Other("Plan".into())],
            language: Language::German,
            origins: source.origins.clone(),
            source_url: url.into(),
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_impacts(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let links = {
        let url = source.url.join("FFHVP/Wirkfaktor.jsp")?;

        let text = client.fetch_text(source, "impact".to_owned(), &url).await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        let sublinks = {
            let url = source.url.join(&link)?;

            let key = format!("impact-{}", make_key(&link));

            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            let content_div = document
                .select(&selectors.content_div)
                .nth(1)
                .ok_or_else(|| anyhow!("Missing content div at {link}"))?;

            content_div
                .select(&selectors.direct_links)
                .map(|element| element.attr("href").unwrap().to_owned())
                .collect::<Vec<_>>()
        };

        let count = sublinks.len();

        let (results, errors) = fetch_many(0, 0, sublinks, |sublink| async move {
            let url = source.url.join(&format!("FFHVP/{sublink}"))?;

            let key = format!("impact-{}", make_key(&sublink));

            let title;
            let description;
            let comment;

            {
                let text = client
                    .fetch_text(source, format!("{key}.isol"), &url)
                    .await?;

                let mut document = Html::parse_document(&text);

                let content_div = document
                    .select(&selectors.content_div)
                    .nth(1)
                    .ok_or_else(|| anyhow!("Missing Definition"))?;

                let element = content_div
                    .select(&selectors.title)
                    .next()
                    .ok_or_else(|| anyhow!("Missing title"))?;

                title = collect_text(element.text());

                // Detach the header so we can select the remaining text.
                document.tree.get_mut(element.id()).unwrap().detach();

                let content_div = document
                    .select(&selectors.content_div)
                    .nth(1)
                    .ok_or_else(|| anyhow!("Missing Definition"))?;

                description = collect_text(content_div.text());

                let content_div = document
                    .select(&selectors.content_div)
                    .nth(2)
                    .ok_or_else(|| anyhow!("Missing Vertiefung"))?;

                let element = content_div
                    .select(&selectors.title)
                    .next()
                    .ok_or_else(|| anyhow!("Missing title"))?;

                // Detach the header so we can select the remaining text.
                document.tree.get_mut(element.id()).unwrap().detach();

                let content_div = document
                    .select(&selectors.content_div)
                    .nth(2)
                    .ok_or_else(|| anyhow!("Missing Vertiefung"))?;

                comment = collect_text(content_div.text());
            }

            let dataset = Dataset {
                title,
                description: Some(description),
                types: smallvec![Type::Text {
                    text_type: TextType::Editorial
                }],
                comment: Some(comment),
                tags: vec![Tag::FFHRL, Tag::Other("Wirkfaktor".into())],
                language: Language::German,
                origins: source.origins.clone(),
                source_url: url.into(),
                ..Default::default()
            };

            write_dataset(dir, client, source, key, dataset).await
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_habitats(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    fetch_entities(
        dir,
        client,
        source,
        selectors,
        "habitat",
        "Lrt.jsp",
        &Tag::FFHLRT,
    )
    .await
}

async fn fetch_species(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    fetch_entities(
        dir,
        client,
        source,
        selectors,
        "species",
        "Art.jsp",
        &Tag::SPECIES,
    )
    .await
}

async fn fetch_bird_species(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    fetch_entities(
        dir,
        client,
        source,
        selectors,
        "bird-species",
        "Vog.jsp",
        &Tag::BIRD,
    )
    .await
}

async fn fetch_entities(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    entity: &str,
    href: &str,
    tag: &Tag,
) -> Result<(usize, usize, usize)> {
    let links = {
        let url = source.url.join(&format!("FFHVP/{href}"))?;

        let text = client.fetch_text(source, entity.to_owned(), &url).await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        let sublinks = {
            let url = source.url.join(&link)?;

            let key = format!("{entity}-{}", make_key(&link));

            let text = client
                .fetch_text(source, format!("{key}.isol"), &url)
                .await?;

            let document = Html::parse_document(&text);

            document
                .select(&selectors.sublinks)
                .map(|element| element.attr("href").unwrap().to_owned())
                .collect::<Vec<_>>()
        };

        let count = sublinks.len();

        let (results, errors) = fetch_many(0, 0, sublinks, |sublink| async move {
            let url = source.url.join(&sublink)?;

            let key = format!("{entity}-{}", make_key(&sublink));

            let title;
            let comment;
            let mut description = String::new();

            {
                let text = client
                    .fetch_text(source, format!("{key}.isol"), &url)
                    .await?;

                let document = Html::parse_document(&text);

                let content_div = document
                    .select(&selectors.content_div)
                    .next()
                    .ok_or_else(|| anyhow!("Missing content div at {url}"))?;

                let element = content_div
                    .select(&selectors.title)
                    .next()
                    .ok_or_else(|| anyhow!("Missing title"))?;

                title = collect_text(element.text());

                comment = element
                    .next_sibling()
                    .and_then(|node| node.value().as_text())
                    .map(|text| text.text.as_ref().to_owned());

                for row in document.select(&selectors.wirkfaktoren) {
                    let Some(link) = row.select(&selectors.wirkfaktor_link).next() else {
                        continue;
                    };

                    let relevanz = row
                        .select(&selectors.wirkfaktor_relevanz)
                        .next()
                        .ok_or_else(|| anyhow!("Missing Relevanz"))?;

                    let relevanz = collect_text(relevanz.text());
                    match relevanz.parse::<u8>() {
                        Ok(1..=3) => (),
                        _ => continue,
                    }

                    let link = collect_text(link.text());
                    let link = selectors.wirkfaktor_prefix.replace_all(&link, "");

                    if !description.is_empty() {
                        description.push(' ');
                    }
                    description.push_str(&link);
                }
            }

            let dataset = Dataset {
                title,
                description: Some(description),
                types: smallvec![Type::Text {
                    text_type: TextType::Editorial
                }],
                comment,
                tags: vec![Tag::FFHRL, tag.clone()],
                language: Language::German,
                origins: source.origins.clone(),
                source_url: url.into(),
                ..Default::default()
            };

            write_dataset(dir, client, source, key, dataset).await
        })
        .await;

        Ok((count, results, errors))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_homepage(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<()> {
    let url = source.url.join("FFHVP/Page.jsp?name=intro")?;

    let key = "homepage".to_owned();

    let description;

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        let content_div = document
            .select(&selectors.content_div)
            .next()
            .ok_or_else(|| anyhow!("Missing content div at {url}"))?;

        description = collect_text(content_div.text());
    }

    let dataset = Dataset {
        title: "Fachinformationssystem des Bundesamtes für Naturschutz zur Flora-Fauna-Habitat-Richtlinien-Verträglichkeitsprüfung".to_owned(),
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::Editorial
        }],
        tags: vec![Tag::FFHRL],
        language: Language::German,
        origins: source.origins.clone(),
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await?;

    Ok(())
}

async fn fetch_downloads(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<()> {
    let url = source.url.join("FFHVP/Page.jsp?name=raumbedarf")?;

    let key = "downloads".to_owned();

    let title;
    let description;
    let resources;

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        title = select_first_text(&document, &selectors.main_title, "title")?;

        let content_div = document
            .select(&selectors.content_div)
            .next()
            .ok_or_else(|| anyhow!("Missing content div at {url}"))?;

        description = collect_text(content_div.text());

        resources = content_div
            .select(&selectors.downloads_resources)
            .map(|element| {
                let href = element.attr("href").unwrap();

                let url = source.url.join(&format!("FFHVP/{href}"))?;

                let description = collect_text(element.text());

                Ok(Resource {
                    r#type: ResourceType::Document,
                    description: Some(description),
                    url: url.into(),
                    primary_content: true,
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<_>>()?;
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::Editorial
        }],
        resources,
        tags: vec![Tag::FFHRL],
        language: Language::German,
        origins: source.origins.clone(),
        source_url: url.into(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await?;

    Ok(())
}

selectors! {
    links: "div.menupunkt_ebene2 a[href]",
    sublinks: "div.menupunkt_ebene3 a[href]",
    direct_links: "ul li a[href]",
    content_div: "div.inhalt_text",
    title: "h2",
    main_title: "h1",
    wirkfaktoren: "table.uebersicht_tabelle tr",
    wirkfaktor_link: "td:nth-of-type(1) a[href]",
    wirkfaktor_prefix: r"\d+\-\d+\s*" as Regex,
    wirkfaktor_relevanz: "td:nth-of-type(2)",
    project_content_row: "table tr",
    project_description: "td:nth-of-type(2)",
    downloads_resources: "a[href]",
}
