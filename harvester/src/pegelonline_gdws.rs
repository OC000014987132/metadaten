use std::borrow::Cow;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use compact_str::ToCompactString;
use hashbrown::{HashMap, HashSet};
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::{smallvec, SmallVec};
use time::{format_description::well_known::Iso8601, Date};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, point_like_bounding_box, remove_pom_suffix, select_first_text},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, TextType, Type},
        Dataset, Language, License, Region, Resource, ResourceType,
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    // There are inconsistencies in naming in the JSON data available through the API and the file system.
    let timeseries_name = [
        ("FLIESSGESCHWINDIGKEIT", "Fließgeschwindigkeit"),
        (
            "FLIESSGESCHWINDIGKEIT_ROHDATEN",
            "Fließgeschwindigkeit_Rohdaten",
        ),
        ("GRUNDWASSER ROHDATEN", "GRUNDWASSER+ROHDATEN"),
        ("LUFTFEUCHTE", "Luftfeuchte"),
        ("LUFTTEMPERATUR", "Lufttemperatur"),
        ("LUFTTEMPERATUR ROHDATEN", "Lufttemperatur+Rohdaten"),
        ("PH-WERT", "PH-Wert"),
        ("SAUERSTOFFGEHALT ROHDATEN", "SAUERSTOFFGEHALT+ROHDATEN"),
        ("SIGNIFIKANTEWELLENHÖHE", "SignifikanteWellenhöhe"),
        ("TRÜBUNG", "Trübung"),
        ("TRÜBUNG_ROHDATEN", "Trübung_Rohdaten"),
        ("WASSERSTAND ROHDATEN", "Wasserstand+Rohdaten"),
        ("WASSERTEMPERATUR", "Wassertemperatur"),
        ("WASSERTEMPERATUR ROHDATEN", "Wassertemperatur+Rohdaten"),
        ("WELLENPERIODE", "Wellenperiode"),
    ]
    .into_iter()
    .collect::<HashMap<_, _>>();

    let water_names = [
        ("BÜTZFLETHER SÜDERELBE", "Bützflether+Süderelbe"),
        ("DATTELN-HAMM-KANAL", "Datteln-Hamm-Kanal"),
        ("DORTMUND-EMS-KANAL", "Dortmund-Ems-Kanal"),
        ("FINOWKANAL", "Finowkanal"),
        ("FREIBURGER HAFENPRIEL", "Freiburger+Hafenpriel"),
        ("KLEINES HAFF", "Kleines+Haff"),
        ("LYCHENER GEWÄSSER", "Lychener+Gewässer"),
        ("MALZER KANAL", "MALZER+KANAL"),
        ("NORD-OSTSEE-KANAL", "Nord-Ostsee-Kanal"),
        ("ORANIENBURGER HAVEL", "Oranienburger+Havel"),
        ("ORANIENBURGER KANAL", "Oranienburger+Kanal"),
        ("PLOUCNICE", "Ploucnice"),
        ("SCHWINGE", "Schwinge"),
        ("RHEIN-HERNE-KANAL", "Rhein-Herne-Kanal"),
        ("RUTHENSTROM", "Ruthenstrom"),
        ("ROTHENSEER-VERBINDUNGSKANAL", "Rothenseer-Verbindungskanal"),
        ("TEMPLINER GEWÄSSER", "Templiner+Gewässer"),
        ("JIZERA", "Jizera"),
        ("WENTOW-GEWÄSSER", "Wentow-Gewässer"),
        ("WERBELLINER GEWÄSSER", "Werbelliner+Gewässer"),
        ("WESEL-DATTELN-KANAL", "Wesel-Datteln-Kanal"),
        ("WISCHHAFENER SÜDERELBE", "Wischhafener+Süderelbe"),
    ]
    .into_iter()
    .collect::<HashMap<_, _>>();

    let (mut count, mut results, mut errors) =
        fetch_stations(dir, client, source, &timeseries_name, &water_names).await?;

    let (count1, results1, errors1) = fetch_apis(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_stations(
    dir: &Dir,
    client: &Client,
    source: &Source,
    timeseries_name: &HashMap<&str, &str>,
    water_names: &HashMap<&str, &str>,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join("webservices/rest-api/v2/stations.json")?;

    url.query_pairs_mut()
        .append_pair("includeTimeseries", "true")
        .append_pair("includeCurrentMeasurement", "true")
        .append_pair("includeCharacteristicValues", "true");

    let bytes = client
        .fetch_bytes(source, "stations".to_owned(), &url)
        .await?;

    let stations = from_slice::<Vec<GaugingStation>>(&bytes)?;

    let count = stations.len();

    let (results, errors) = fetch_many(0, 0, stations, |station| async move {
        let key = station.uuid.to_string();

        let source_url = source
            .url
            .join(&format!("gast/stammdaten?pegelnr={}", station.number))?
            .into();

        let title = format!(
            "Messstelle {}, {}",
            station.longname, station.water.longname,
        );

        let mut regions = smallvec![Region::Other(remove_pom_suffix(&station.longname).into())];

        let mut bounding_boxes = SmallVec::new();

        if let (Some(longitude), Some(latitude)) = (station.longitude, station.latitude) {
            bounding_boxes.push(point_like_bounding_box(latitude, longitude));

            regions.extend(WISE.match_shape(longitude, latitude).map(Region::Watershed));
        }

        let mut values = HashSet::new();

        let mut modified = Date::MIN;

        let mut resources = smallvec![
            Resource {
                r#type: ResourceType::JsonLd,
                description: Some("API-Endpunkt für die JSON-Metadaten der Station".to_owned()),
                url: {
                    let mut url = source.url.join(&format!(
                        "webservices/rest-api/v2/stations/{}.json",
                        station.uuid
                    ))?;

                    url.query_pairs_mut()
                        .append_pair("includeTimeseries", "true")
                        .append_pair("includeCurrentMeasurement", "true")
                        .append_pair("includeCharacteristicValues", "true");

                    url.into()
                },
                ..Default::default()
            },
            Resource {
                r#type: ResourceType::WebPage,
                description: Some("Karte aller Statione".to_owned()),
                url: source.url.join("gast/karte/standard")?.into(),
                ..Default::default()
            },
        ];

        let water_name = water_names.get(&*station.water.longname).map_or_else(
            || Cow::Owned(station.water.longname.replace(' ', "+")),
            |name| Cow::Borrowed(*name),
        );

        for timeseries in station.timeseries {
            values.extend(timeseries.values);

            let timestamp = Date::parse(timeseries.measurement.timestamp, &Iso8601::DEFAULT)?;
            modified = modified.max(timestamp);

            let name = timeseries_name
                .get(&*timeseries.longname)
                .copied()
                .unwrap_or(&*timeseries.longname);

            resources.push(Resource {
                r#type: ResourceType::WebPage,
                description: Some(format!(
                    "Alle {name} Messungen an Station {} für den letzten Monat",
                    station.longname
                )),
                url: source
                    .url
                    .join(&format!(
                        "webservices/files/{name}/{water_name}/{}",
                        station.uuid
                    ))?
                    .into(),
                ..Default::default()
            });

            resources.push(Resource {
                r#type: ResourceType::WebPage,
                description: Some(format!(
                    "Visualisierung für {name} Messungen an Station {}",
                    station.longname
                )),
                url: source
                    .url
                    .join(&format!(
                        "webservices/zeitreihe/visualisierung?parameter={name}&pegeluuid={}",
                        station.uuid
                    ))?
                    .into(),
                ..Default::default()
            });

            resources.push(Resource {
                r#type: ResourceType::JsonLd,
                description: Some("API-Endpunkt für die JSON-Rohdaten der Station".to_owned()),
                url: source
                    .url
                    .join(&format!(
                        "webservices/rest-api/v2/stations/{}/{}/measurements.json",
                        station.uuid, timeseries.shortname
                    ))?
                    .into(),
                primary_content: true,
                ..Default::default()
            });
        }

        let description = format!("Messstelle betrieben von {}.", station.agency);

        let modified = (modified != Date::MIN).then_some(modified);

        let types = smallvec![Type::Measurements {
            domain: Domain::Rivers,
            station: Some(Station {
                id: Some(station.number.to_compact_string()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Pegelstand".to_owned()],
            methods: Default::default(),
        }];

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            regions,
            bounding_boxes,
            modified,
            resources,
            source_url,
            license: License::OtherClosed,
            language: Language::German,
            origins: source.origins.clone(),
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_apis(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    const APIS: &[&str] = &[
        "dokuRestapi",
        "aktuell",
        "wmsAktuell",
        "wfsAktuell",
        "guide-sos-api",
    ];

    let count = APIS.len();

    let (results, errors) = fetch_many(0, 0, APIS, |api| async move {
        let url = source.url.join(&format!("webservice/{api}"))?;

        let key = format!("api-{api}");

        let dataset = {
            let text = client.fetch_text(source, key.clone(), &url).await?;

            let document = Html::parse_document(&text);

            let content = document
                .select(&selectors.content)
                .next()
                .ok_or_else(|| anyhow!("Missing content"))?;

            let header = select_first_text(&document, &selectors.header, "header")?;

            let resources = content
                .select(&selectors.links)
                .map(|link| {
                    let href = link.attr("href").unwrap();

                    let description =
                        Some(collect_text(link.text())).filter(|description| description != href);

                    Ok(Resource {
                        r#type: ResourceType::WebPage,
                        description,
                        url: source.url.join(href)?.into(),
                        ..Default::default()
                    }
                    .guess_or_keep_type())
                })
                .collect::<Result<_>>()?;

            let content = collect_text(content.text());

            Dataset {
                title: format!("PegelOnline API: {header}"),
                description: Some(content),
                resources,
                license: License::OtherClosed,
                language: Language::German,
                source_url: url.into(),
                types: smallvec![Type::Text {
                    text_type: TextType::Manual
                }],
                origins: source.origins.clone(),
                ..Default::default()
            }
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Deserialize)]
struct GaugingStation<'a> {
    uuid: &'a str,
    number: &'a str,
    #[serde(borrow)]
    longname: Cow<'a, str>,
    #[serde(borrow)]
    agency: Cow<'a, str>,
    #[serde(borrow)]
    water: Water<'a>,
    longitude: Option<f64>,
    latitude: Option<f64>,
    #[serde(borrow, default)]
    timeseries: Vec<Timeseries<'a>>,
}

#[derive(Deserialize)]
struct Water<'a> {
    #[serde(borrow)]
    longname: Cow<'a, str>,
}

#[derive(Deserialize)]
struct Timeseries<'a> {
    #[serde(borrow)]
    shortname: Cow<'a, str>,
    #[serde(borrow)]
    longname: Cow<'a, str>,
    #[serde(rename = "currentMeasurement", borrow)]
    measurement: ApiMeasurement<'a>,
    #[serde(rename = "characteristicValues", borrow)]
    values: Vec<Value<'a>>,
}

#[derive(Deserialize)]
struct ApiMeasurement<'a> {
    timestamp: &'a str,
}

#[derive(Deserialize, PartialEq, Eq, Hash)]
struct Value<'a> {
    #[serde(borrow)]
    shortname: Cow<'a, str>,
    #[serde(borrow)]
    longname: Cow<'a, str>,
}

selectors! {
    content: "#content",
    header: "h1",
    links: "tr.tablerow1 a[href]",
}
