use std::sync::Arc;

use anyhow::{anyhow, ensure, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{Coord, LineString, Polygon};
use miniproj::get_projection;
use serde::Deserialize;
use serde_json::from_slice;

use harvester::{client::Client, fetch_many, Source};
use metadaten::wise::{Database, Entry};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let proj = get_projection(3857).unwrap();

    let object_ids = {
        let mut url = source.url.join("1/query")?;
        url.query_pairs_mut()
            .append_pair("f", "json")
            .append_pair("returnIdsOnly", "true")
            .append_pair("where", "countryCode = 'DE'");

        let bytes = client
            .fetch_bytes(source, "object-ids".to_owned(), &url)
            .await?;
        from_slice::<ObjectIds>(&bytes)?
    };

    let database = &AtomicRefCell::new(Database::default());

    let count = object_ids.object_ids.len();

    let (results, errors) = fetch_many(0, 0, object_ids.object_ids, |object_id| async move {
        let mut url = source.url.join(&format!("1/{object_id}"))?;
        url.query_pairs_mut().append_pair("f", "json");

        let body = client
            .fetch_bytes(source, format!("object-{object_id}.isol"), &url)
            .await?;
        let response = from_slice::<Response>(&body)?;

        let name = response.feature.attributes.name;
        let id = response.feature.attributes.id;

        ensure!(response.feature.attributes.id_scheme == "euSubUnitCode");

        let mut rings = response.feature.geometry.rings.into_iter().map(|ring| {
            LineString::new(
                ring.into_iter()
                    .map(|(x, y)| {
                        let (x, y) = proj.projected_to_deg(x, y);

                        Coord { x, y }
                    })
                    .collect(),
            )
        });

        let exterior = rings.next().ok_or_else(|| anyhow!("Missing exterior"))?;
        let interiors = rings.collect();
        let shape = Polygon::new(exterior, interiors);

        let entry = Arc::new(Entry { name, id, shape });

        database.borrow_mut().entries.insert(object_id, entry);

        Ok((1, 1, 0))
    })
    .await;

    database.borrow().write(dir)?;

    Ok((count, results, errors))
}

#[derive(Debug, Deserialize)]
struct ObjectIds {
    #[serde(rename = "objectIds")]
    object_ids: Vec<u64>,
}

#[derive(Debug, Deserialize)]
struct Response<'a> {
    #[serde(borrow)]
    feature: Feature<'a>,
}

#[derive(Debug, Deserialize)]
struct Feature<'a> {
    #[serde(borrow)]
    attributes: Attributes<'a>,
    geometry: Geometry,
}

#[derive(Debug, Deserialize)]
struct Attributes<'a> {
    #[serde(rename = "nameText")]
    name: String,
    #[serde(rename = "thematicIdIdentifier")]
    id: CompactString,
    #[serde(rename = "thematicIdIdentifierScheme")]
    id_scheme: &'a str,
}

#[derive(Debug, Deserialize)]
struct Geometry {
    rings: Vec<Vec<(f64, f64)>>,
}
