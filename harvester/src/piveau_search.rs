use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use geo::algorithm::BoundingRect;
use geozero::{geojson::GeoJson, ToGeo};
use serde::Deserialize;
use serde_json::{from_slice, value::RawValue};
use smallvec::SmallVec;
use time::{macros::format_description, Date};

use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::dataset::{
    Dataset, GlobalIdentifier, Organisation, OrganisationRole, Resource, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let limit = source.batch_size;

    let (count, results, errors) = fetch_datasets(dir, client, source, 0, limit).await?;
    tracing::info!("Harvesting {} datasets", count);

    let pages = count.div_ceil(limit);
    let (results, errors) = fetch_many(results, errors, 1..pages, |page| {
        fetch_datasets(dir, client, source, page, limit)
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip(dir, client, source))]
async fn fetch_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    page: usize,
    limit: usize,
) -> Result<(usize, usize, usize)> {
    let page = page.to_string();

    let mut url = source.url.clone();

    url.query_pairs_mut()
        .append_pair("filters", "dataset")
        .append_pair("page", &page)
        .append_pair("limit", &limit.to_string());

    if let Some(filter) = &source.filter {
        url.query_pairs_mut().append_pair("facets", filter);
    }

    let bytes = client.fetch_bytes(source, page, &url).await?;

    let response = from_slice::<Response>(&bytes)?;

    let count = response.result.count;
    let (results, errors) = fetch_many(0, 0, response.result.results, |dataset| {
        translate_dataset(dir, client, source, dataset)
    })
    .await;

    Ok((count, results, errors))
}

#[tracing::instrument(skip_all, fields(key = dataset.id))]
async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    dataset: PiveauDataset<'_>,
) -> Result<(usize, usize, usize)> {
    let key = dataset.id;

    let (_, name) = dataset.resource.rsplit_once('/').ok_or_else(|| {
        anyhow!(
            "Failed to determine name from resource: {}",
            dataset.resource
        )
    })?;

    let source_url = source.source_url().replace("{{identifier}}", name);

    let global_identifier = dataset
        .identifier
        .first()
        .and_then(|identifier| GlobalIdentifier::parse(*identifier));

    let tags = dataset
        .keywords
        .into_iter()
        .map(|keyword| Tag::Other(keyword.label.into()))
        .collect();

    let bounding_boxes = dataset
        .spatial
        .into_iter()
        .filter_map(|spatial| {
            let geometry = GeoJson(spatial.get()).to_geo().ok()?;
            geometry.bounding_rect()
        })
        .collect();

    let organisations = dataset
        .publisher
        .into_iter()
        .map(|publisher| Organisation::Other {
            name: publisher.name,
            role: OrganisationRole::Publisher,
            websites: publisher.homepage.into_iter().collect(),
        })
        .chain(
            dataset
                .contact_point
                .into_iter()
                .filter_map(|contact_point| {
                    Some(Organisation::Other {
                        name: contact_point.name?,
                        role: OrganisationRole::Unknown,
                        websites: contact_point.url,
                    })
                }),
        )
        .collect();

    // FIXME: Consider all licenses as we do for CKAN.
    let license = dataset
        .distributions
        .iter()
        .find_map(|distribution| distribution.license.as_ref())
        .map(|license| license.resource.into())
        .unwrap_or_default();

    let resources = dataset
        .distributions
        .into_iter()
        .flat_map(|distribution| {
            let r#type = distribution
                .format
                .and_then(|format| format.resource)
                .map(|format| {
                    format
                        .trim_start_matches(
                            "http://publications.europa.eu/resource/authority/file-type/",
                        )
                        .into()
                })
                .unwrap_or_default();

            let description = distribution
                .description
                .or(distribution.title)
                .and_then(|translated_string| translated_string.de);

            distribution
                .access_url
                .into_iter()
                .map(move |url| Resource {
                    r#type,
                    description: description.clone(),
                    url,
                    ..Default::default()
                })
        })
        .collect();

    let extract_date = |value: Option<&str>| {
        value
            .map(|value| value.split_once('T').map_or(value, |(date, _time)| date))
            .map(|value| Date::parse(value, format_description!("[year]-[month]-[day]")))
            .transpose()
    };

    let issued = extract_date(dataset.issued)?;
    let modified = extract_date(dataset.modified)?;

    let language = dataset
        .language
        .into_iter()
        .next()
        .map(|language| {
            language
                .resource
                .trim_start_matches("http://publications.europa.eu/resource/authority/language/")
                .into()
        })
        .unwrap_or_default();

    let dataset = Dataset {
        title: dataset
            .title
            .value()
            .ok_or_else(|| anyhow!("Missing title for dataset {key}"))?,
        description: dataset
            .description
            .and_then(|translated_string| translated_string.value()),
        global_identifier,
        tags,
        bounding_boxes,
        organisations,
        license,
        resources,
        issued,
        modified,
        language,
        origins: source.origins.clone(),
        source_url,
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Deserialize)]
struct Response<'a> {
    #[serde(borrow)]
    result: PiveauResult<'a>,
}

#[derive(Deserialize)]
struct PiveauResult<'a> {
    count: usize,
    #[serde(borrow)]
    results: Vec<PiveauDataset<'a>>,
}

#[derive(Deserialize)]
struct PiveauDataset<'a> {
    id: String,
    resource: &'a str,
    #[serde(default)]
    identifier: Vec<&'a str>,
    title: TranslatedString,
    description: Option<TranslatedString>,
    #[serde(borrow, default)]
    keywords: Vec<Keyword<'a>>,
    issued: Option<&'a str>,
    modified: Option<&'a str>,
    #[serde(borrow, default)]
    language: SmallVec<[PiveauLanguage<'a>; 1]>,
    #[serde(borrow, default)]
    spatial: SmallVec<[&'a RawValue; 1]>,
    publisher: Option<Publisher>,
    #[serde(default)]
    contact_point: SmallVec<[ContactPoint; 1]>,
    #[serde(borrow, default)]
    distributions: SmallVec<[Distribution<'a>; 1]>,
}

#[derive(Deserialize)]
struct Keyword<'a> {
    label: &'a str,
}

#[derive(Deserialize)]
struct PiveauLanguage<'a> {
    resource: &'a str,
}

#[derive(Deserialize)]
struct Publisher {
    name: String,
    homepage: Option<String>,
}

#[derive(Deserialize)]
struct ContactPoint {
    name: Option<String>,
    #[serde(default)]
    url: SmallVec<[String; 1]>,
}

#[derive(Deserialize)]
struct Distribution<'a> {
    title: Option<TranslatedString>,
    description: Option<TranslatedString>,
    #[serde(borrow)]
    format: Option<Format<'a>>,
    #[serde(borrow)]
    license: Option<PiveauLicense<'a>>,
    access_url: SmallVec<[String; 1]>,
}

#[derive(Deserialize)]
struct Format<'a> {
    resource: Option<&'a str>,
}

#[derive(Deserialize)]
struct PiveauLicense<'a> {
    resource: &'a str,
}

#[derive(Deserialize)]
struct TranslatedString {
    de: Option<String>,
    en: Option<String>,
}

impl TranslatedString {
    fn value(self) -> Option<String> {
        self.de.or(self.en)
    }
}
