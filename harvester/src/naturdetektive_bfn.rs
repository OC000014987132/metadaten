use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::smallvec;

use harvester::{
    client::Client, fetch_many, selectors, utilities::collect_text, write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationKey, OrganisationRole, Person,
    PersonRole, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();

    let pages = fetch_links(client, source, &selectors).await?;

    let count = pages.len();
    tracing::info!("Scraping {} pages", count);

    let (results, errors) = fetch_many(0, 0, pages, |page| {
        fetch_page(dir, client, source, &selectors, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_links(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let url = source.url.join("/uebersicht-natur-fuer-kinder.html")?;
    let text = client
        .fetch_text(source, "uebersicht".to_owned(), &url)
        .await?;
    let document = Html::parse_document(&text);

    let mut links = document
        .select(&selectors.links)
        .map(|elem| elem.attr("href").unwrap().to_owned())
        .collect::<Vec<_>>();

    links.sort_unstable();
    links.dedup();

    Ok(links)
}

#[tracing::instrument(skip(dir, client, source, selectors))]
async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: String,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&page)?;
    let key = page.replace('/', "-");

    let dataset = {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        let article = document
            .select(&selectors.article)
            .next()
            .ok_or_else(|| anyhow!("Missing article on {page}"))?;

        let title = match article.select(&selectors.title).next() {
            Some(title) => collect_text(title.text()),
            None => {
                tracing::debug!("Ignoring {page} due to missing title");
                return Ok((0, 0, 0));
            }
        };

        if selectors.birds.is_match(&title)
            | title.starts_with("Teilnahme:")
            | title.starts_with("Frag Konstantin!")
            | title.starts_with("Lösungen")
        {
            return Ok((0, 0, 0));
        }

        let description = article
            .select(&selectors.description)
            .next()
            .map(|description| {
                let text = collect_text(
                    description
                        .text()
                        .filter(|val| !val.starts_with("(Foto:") && !val.starts_with("(Grafik:")),
                );
                format!("Ein Lernangebot für Kinder. {text}")
            });

        let organisations = smallvec![Organisation::WikiData {
            // according to https://naturdetektive.bfn.de/impressum.html
            identifier: OrganisationKey::BFN,
            role: OrganisationRole::Publisher,
        }];

        let persons = vec![Person {
            // according to https://naturdetektive.bfn.de/impressum.html
            name: "Frank Wörner".to_string(),
            role: PersonRole::Contact,
            emails: smallvec!["frank.woerner@foodmedia.de".to_string()],
            ..Default::default()
        }];

        let mut resources = document
            .select(&selectors.resource_link)
            .next()
            .map(|element| element.attr("href").unwrap())
            .filter(|resource_link| !selectors.html_link.is_match(resource_link))
            .map(|resource_link| -> Result<_> {
                let resource = Resource {
                    r#type: ResourceType::WebPage,
                    url: source.url.join(resource_link)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type();
                Ok(smallvec![resource])
            })
            .transpose()?
            .unwrap_or_default();

        for pdf_figure in document.select(&selectors.pdf_figure) {
            if let Some(pdf_link) = pdf_figure
                .select(&selectors.pdf_link)
                .next()
                .map(|element| element.attr("href").unwrap())
            {
                if let Some(pdf_title) = pdf_figure
                    .select(&selectors.pdf_title)
                    .next()
                    .map(|element| element.attr("title").unwrap().to_owned())
                {
                    resources.push(Resource {
                        r#type: ResourceType::Pdf,
                        url: source.url.join(pdf_link)?.into(),
                        description: pdf_title.into(),
                        ..Default::default()
                    })
                }
            }
        }

        Dataset {
            title,
            description,
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            source_url: url.into(),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial
            }],
            resources,
            language: Language::German,
            organisations,
            persons,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    links: "div.content_text li > a[href]",
    article: "div.content",
    title: "h1",
    description: "div",
    resource_link: "div.content_text div.ce-bodytext a[href]",
    pdf_figure: "div.content_text div.ce-gallery figure",
    pdf_link: "figcaption a[href]",
    pdf_title: "img[title]",
    birds: r"^Vogel\s+\d+$" as Regex,
    html_link: r"\.html$" as Regex,
}
