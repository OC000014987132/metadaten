use std::fmt::Write;

use anyhow::Result;
use cap_std::fs::Dir;
use compact_str::CompactString;
use csv::Reader;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::{Map, Value};
use smallvec::smallvec;
use time::Date;

use harvester::{
    client::Client,
    fetch_many,
    utilities::{collect_text, point_like_bounding_box},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let title;
    let description;

    {
        let key = "homepage".to_owned();

        let text = client.fetch_text(source, key.clone(), &source.url).await?;
        let document = Html::parse_document(&text);

        title = collect_text(
            document
                .select(&Selector::parse("div#heading > h1.title").unwrap())
                .flat_map(|element| element.text()),
        );

        description = collect_text(
            document
                .select(&Selector::parse("aside.description > p:not(:last-child)").unwrap())
                .flat_map(|element| element.text()),
        );

        let dataset = Dataset {
            title: title.clone(),
            description: Some(description.clone()),
            language: Language::German,
            origins: source.origins.clone(),
            source_url: source.url.clone().into(),
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await?;
    }

    let title = &title;
    let description = &description;

    let url = &source.url.join("/downloads/2008-2022_eKommu.csv")?;

    let text = client.fetch_text(source, "csv".to_owned(), url).await?;

    let mut reader = Reader::from_reader(text.as_bytes());

    let count = text.lines().count();

    let (results, errors) = fetch_many(
        1,
        0,
        reader.deserialize::<Record>().enumerate(),
        |(index, record)| async move {
            let record = record?;

            let title = format!(
                "{}: {} ({}), {}",
                title, record.name, record.state, record.year
            );

            let mut description = description.to_owned();
            description.push_str("\n\n");
            for (key, val) in record.other {
                write!(&mut description, "{key}: {val} ").unwrap();
            }

            let issued = Date::from_ordinal_date(record.year, 1)?;

            let region = Region::Other(record.state);

            let bounding_boxes = if !record.lat.is_empty() && !record.lon.is_empty() {
                smallvec![point_like_bounding_box(
                    record.lat.parse()?,
                    record.lon.parse()?
                )]
            } else {
                Default::default()
            };

            let resources = smallvec![
                Resource {
                    r#type: ResourceType::Csv,
                    description: None,
                    url: url.clone().into(),
                    primary_content: true,
                    ..Default::default()
                },
                Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(
                        "Hauptseite Europäische Kommunalabwasser-Richtlinie in Deutschland"
                            .to_owned(),
                    ),
                    url: source.url.clone().into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            ];

            let dataset = Dataset {
                title,
                description: Some(description),
                types: smallvec![Type::Text {
                    text_type: TextType::OfficialFile,
                }],
                issued: Some(issued),
                regions: smallvec![region],
                bounding_boxes,
                resources,
                language: Language::German,
                origins: source.origins.clone(),
                source_url: source.url.clone().into(),
                ..Default::default()
            };

            write_dataset(dir, client, source, index.to_string(), dataset).await
        },
    )
    .await;

    Ok((count, results, errors))
}

#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "Kläranlagenname")]
    name: String,
    #[serde(rename = "Länderkürzel")]
    state: CompactString,
    #[serde(rename = "Berichtsjahr")]
    year: i32,
    #[serde(rename = "geo_wgs84_lat")]
    lat: String,
    #[serde(rename = "geo_wgs84_long")]
    lon: String,
    #[serde(flatten)]
    other: Map<String, Value>,
}
