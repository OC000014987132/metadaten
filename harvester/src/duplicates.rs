use std::cmp::Ordering;
use std::hash::Hash;
use std::mem::swap;
use std::str::from_utf8;
use std::sync::Arc;

use anyhow::Result;
use bincode::serialize;
use cap_std::fs::Dir;
use hashbrown::{
    hash_map::{Entry, EntryRef, HashMap},
    Equivalent,
};
use memchr::memmem::find;
use once_cell::sync::Lazy;
use openssl::sha::Sha256;
use parking_lot::Mutex;
use regex::Regex;
use rustc_hash::{FxHashMap, FxHashSet};
use smallvec::SmallVec;
use string_cache::DefaultAtom;
use tokio::task::JoinSet;

use crate::{postprocess_dataset, Source};
use metadaten::{
    dataset::{Alternative, Dataset, GlobalIdentifier, Language, License},
    TrimExt,
};

#[derive(Debug)]
pub struct Duplicates {
    inner: Arc<Mutex<DuplicatesInner>>,
    source_classif: Arc<HashMap<DefaultAtom, SourceClassif>>,
}

#[derive(Debug)]
struct DuplicatesInner {
    fingerprints: HashMap<Fingerprint, Refs>,
    landing_pages: HashMap<[u8; 32], usize>,
}

#[derive(Debug)]
struct SourceClassif {
    tier: u8,
    primary: bool,
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum Fingerprint<I = GlobalIdentifier> {
    TitleAndDescription([u8; 32]),
    GlobalIdentifier(I),
}

impl<'a> Fingerprint<&'a GlobalIdentifier> {
    fn new(dataset: &'a Dataset) -> Self {
        if let Some(identifier) = &dataset.global_identifier {
            if !identifier.is_short() {
                return Self::GlobalIdentifier(identifier);
            }
        }

        let mut ctx = Sha256::new();

        let title = dataset.title.as_str();
        ctx.update(&title.len().to_ne_bytes());
        ctx.update(title.as_bytes());

        let description = dataset.description.as_deref().unwrap_or_default();
        ctx.update(&description.len().to_ne_bytes());
        ctx.update(description.as_bytes());

        let hash = ctx.finish();

        Self::TitleAndDescription(hash)
    }
}

impl Equivalent<Fingerprint> for Fingerprint<&'_ GlobalIdentifier> {
    fn equivalent(&self, key: &Fingerprint) -> bool {
        match (self, key) {
            (Self::TitleAndDescription(lhs), Fingerprint::TitleAndDescription(rhs)) => lhs == rhs,
            (Self::GlobalIdentifier(lhs), Fingerprint::GlobalIdentifier(rhs)) => lhs == &rhs,
            _ => false,
        }
    }
}

impl From<&'_ Fingerprint<&'_ GlobalIdentifier>> for Fingerprint {
    fn from(val: &Fingerprint<&GlobalIdentifier>) -> Self {
        match *val {
            Fingerprint::TitleAndDescription(hash) => Self::TitleAndDescription(hash),
            Fingerprint::GlobalIdentifier(identifier) => Self::GlobalIdentifier(identifier.clone()),
        }
    }
}

fn fingerprint_landing_page(dataset: &Dataset) -> [u8; 32] {
    let mut ctx = Sha256::new();
    ctx.update(dataset.source_url.as_bytes());
    ctx.finish()
}

impl DuplicatesInner {
    fn add_landing_page(&mut self, landing_page: [u8; 32]) {
        match self.landing_pages.entry(landing_page) {
            Entry::Vacant(entry) => {
                entry.insert(0);
            }
            Entry::Occupied(entry) => {
                let duplicates = entry.into_mut();

                *duplicates += 1;
            }
        }
    }

    fn remove_landing_page(&mut self, landing_page: [u8; 32]) {
        if let EntryRef::Occupied(mut entry) = self.landing_pages.entry_ref(&landing_page) {
            let duplicates = entry.get_mut();

            if *duplicates != 0 {
                *duplicates -= 1;
            } else {
                entry.remove();
            }
        }
    }
}

type Refs = SmallVec<[(DefaultAtom, String); 1]>;

impl Duplicates {
    pub fn new(sources: &[Source]) -> Self {
        let source_classif = sources
            .iter()
            .map(|source| {
                (
                    source.name.clone(),
                    SourceClassif {
                        tier: source.tier,
                        primary: source.primary,
                    },
                )
            })
            .collect();

        Self {
            inner: Arc::new(Mutex::new(DuplicatesInner {
                fingerprints: HashMap::new(),
                landing_pages: HashMap::new(),
            })),
            source_classif: Arc::new(source_classif),
        }
    }

    pub fn add_dataset(&self, source: DefaultAtom, id: String, dataset: &Dataset) {
        let fingerprint = Fingerprint::new(dataset);

        let landing_page = fingerprint_landing_page(dataset);

        let mut inner = self.inner.lock();

        inner
            .fingerprints
            .entry_ref(&fingerprint)
            .or_default()
            .push((source, id));

        inner.add_landing_page(landing_page);
    }

    pub fn remove_dataset(&self, source: &DefaultAtom, id: &str, dataset: &Dataset) {
        let fingerprint = Fingerprint::new(dataset);

        let landing_page = fingerprint_landing_page(dataset);

        let mut inner = self.inner.lock();

        if let EntryRef::Occupied(mut entry) = inner.fingerprints.entry_ref(&fingerprint) {
            let refs = entry.get_mut();

            refs.retain(|(source1, id1)| source1 != source || id1 != id);

            if refs.is_empty() {
                entry.remove();
            }
        }

        inner.remove_landing_page(landing_page);
    }

    pub fn remove_source(&self, source: &DefaultAtom) {
        self.inner.lock().fingerprints.retain(|_, refs| {
            refs.retain(|(source1, _)| source1 != source);

            !refs.is_empty()
        });
    }

    pub async fn merge_duplicates(&self, datasets_dir: &Arc<Dir>) {
        tracing::info!("Merging duplicates");

        let mut tasks = JoinSet::new();

        for (_fingerprint, refs) in self.inner.lock().fingerprints.drain() {
            if refs.len() > 1 {
                let inner = self.inner.clone();
                let source_classif = self.source_classif.clone();
                let datasets_dir = datasets_dir.clone();

                tasks.spawn_blocking(move || {
                    merge_duplicates(&inner, &source_classif, &datasets_dir, refs)
                });
            }
        }

        while let Some(res) = tasks.join_next().await {
            if let Err(err) = res.unwrap() {
                tracing::error!("Failed to deduplicate datasets: {:#}", err);
            }
        }
    }

    pub fn write_duplicate_landing_pages(&self, dir: &Dir) -> Result<()> {
        let duplicate_landing_pages = self
            .inner
            .lock()
            .landing_pages
            .drain()
            .filter(|(_, duplicates)| *duplicates != 0)
            .map(|(landing_page, _)| landing_page)
            .collect::<Vec<_>>();

        let buf = serialize(&duplicate_landing_pages)?;

        dir.write("duplicate_landing_pages.new", &buf)?;
        dir.rename(
            "duplicate_landing_pages.new",
            dir,
            "duplicate_landing_pages",
        )?;

        Ok(())
    }
}

#[tracing::instrument(skip_all)]
fn merge_duplicates(
    inner: &Mutex<DuplicatesInner>,
    source_classif: &HashMap<DefaultAtom, SourceClassif>,
    datasets_dir: &Dir,
    mut refs: Refs,
) -> Result<()> {
    // Sort by source and identifier to ensure a deterministic choice.
    refs.sort_unstable();

    let mut datasets = {
        let mut buf = Vec::new();

        refs.into_iter()
            .map(|(source, id)| {
                let file = datasets_dir.open_dir(&*source)?.open(&id)?;

                let dataset = Dataset::read_with(file, &mut buf)?;

                Ok((source, id, dataset))
            })
            .collect::<Result<Vec<_>>>()?
    };

    // Find the most frequent license, as we want to exclude all datasets that do not have this license.
    // If all are `License::Unknown`, `most_freq_license` is `None` and no dataset is removed from the set.
    let most_freq_license = most_freq_value(
        datasets
            .iter()
            .map(|(_, _, dataset)| dataset.license)
            .filter(|&license| license != License::Unknown),
    );

    if let Some(most_freq_license) = most_freq_license {
        let len_before = datasets.len();
        datasets.retain(|(_, _, dataset)| {
            dataset.license == most_freq_license || dataset.license == License::Unknown
        });
        let len_after = datasets.len();
        if len_before != len_after {
            tracing::trace!(
                "Removed {} datasets because of wrong license. Most frequent license is {:?}",
                len_before - len_after,
                most_freq_license
            );
        }
        if len_after < 2 {
            tracing::debug!("Not enough datasets remaining to merge due to license mismatch");
            return Ok(());
        }
    }

    // Preprocess the titles to remove common series indicators like volume numbers
    // and use the longest common title for the base dataset.
    let title_processed = datasets
        .iter()
        .map(|(_, _, dataset)| preprocess_title(&dataset.title))
        .collect::<Vec<_>>();

    let most_freq_title_processed = most_freq_value(title_processed.iter()).unwrap();

    {
        let mut idx = 0;
        datasets.retain(|(_, _, _)| {
            let title_processed = &title_processed[idx];
            let keep = title_processed == most_freq_title_processed;
            if !keep {
                tracing::trace!(
                    title_processed,
                    most_freq_title_processed,
                    "Removing from series as preprocessed title does not fit",
                )
            };
            idx += 1;
            keep
        });
        if datasets.len() < 2 {
            tracing::debug!(
                "Not enough datasets remaining to merge due to title mismatch: {}",
                datasets
                    .first()
                    .map_or("No remaining title", |(_, _, dataset)| &dataset.title)
            );
            return Ok(());
        }
    }

    let longest_common_title = datasets
        .iter()
        .map(|(_, _, dataset)| dataset.title.as_str())
        .reduce(longest_common_substr)
        .unwrap()
        .trim()
        .to_owned();

    if longest_common_title.len() < 20 {
        tracing::debug!("Longest common title too short: {longest_common_title}");
    }

    let base_idx = {
        let mut keys = datasets
            .iter()
            .map(|(source, _id, _dataset)| source_classif[source].tier)
            .enumerate();

        // Prefer to keep newer versions and then versions from lower tiers,
        // i.e. prefer primary sources over aggregators.
        let (mut base_idx, mut min_tier) = keys.next().unwrap();

        for (idx, tier) in keys {
            match min_tier.cmp(&tier) {
                Ordering::Greater => {
                    base_idx = idx;
                    min_tier = tier;
                }
                Ordering::Equal => {
                    let base_source = &datasets[base_idx].0;
                    let source = &datasets[idx].0;

                    if base_source != source {
                        tracing::warn!(
                            "Multiple current duplicates of tier {} from {} and {}",
                            tier,
                            base_source,
                            source,
                        );
                    }
                }
                Ordering::Less => (),
            }
        }

        base_idx
    };

    let (base_source, base_id, mut base_dataset) = datasets.swap_remove(base_idx);

    tracing::debug!(
        "Deleting {} duplicate(s) of dataset {}/{}",
        datasets.len(),
        base_source,
        base_id,
    );

    base_dataset.title = longest_common_title;

    let mut descriptions = Vec::with_capacity(datasets.len());
    let mut comments = Vec::with_capacity(datasets.len());

    if base_dataset.modified.is_none() {
        base_dataset.modified = datasets
            .iter()
            .filter_map(|(_, _, dataset)| dataset.modified)
            .max();
    };
    if base_dataset.issued.is_none() {
        base_dataset.issued = datasets
            .iter()
            .filter_map(|(_, _, dataset)| dataset.issued)
            .min();
    };

    for (source, id, dataset) in datasets {
        tracing::trace!("Deleting duplicate dataset {}/{}", source, id);
        datasets_dir.open_dir(&*source)?.remove_file(&id)?;

        let landing_page = fingerprint_landing_page(&dataset);

        inner.lock().remove_landing_page(landing_page);

        if base_dataset.source_url != dataset.source_url {
            base_dataset.alternatives.push(Alternative::Source {
                source,
                title: dataset.title,
                url: dataset.source_url.clone(),
            });
        }
        if base_dataset.language != dataset.language && dataset.language != Language::Unknown {
            base_dataset.alternatives.push(Alternative::Language {
                language: dataset.language,
                url: dataset.source_url,
            });
        }

        // Avoid merging potentially stale metadata from third parties
        // if we are the primary source of the metadata.
        if source_classif[&base_source].primary {
            continue;
        }

        base_dataset.types.extend(dataset.types);

        descriptions.extend(dataset.description);
        comments.extend(dataset.comment);

        base_dataset.tags.extend(dataset.tags);
        base_dataset.origins.extend(dataset.origins);

        base_dataset.organisations.extend(dataset.organisations);
        base_dataset.persons.extend(dataset.persons);

        base_dataset.resources.extend(dataset.resources);
        base_dataset.alternatives.extend(dataset.alternatives);

        base_dataset.regions.extend(dataset.regions);
        base_dataset.bounding_boxes.extend(dataset.bounding_boxes);

        base_dataset.time_ranges.extend(dataset.time_ranges);
    }

    merge_texts(&mut base_dataset.description, descriptions);
    merge_texts(&mut base_dataset.comment, comments);

    // Avoid sorting the origins to keep the primary origin first.
    {
        let mut origins = FxHashSet::default();
        base_dataset
            .origins
            .retain(|origin| origins.insert(origin.clone()));
    }

    postprocess_dataset(&mut base_dataset);

    {
        let base_file = datasets_dir.open_dir(&*base_source)?.create(base_id)?;
        base_dataset.write(base_file)?;
    }

    Ok(())
}

fn preprocess_title(title: &str) -> String {
    struct Patterns {
        bfn_schriften: Regex,
        gk_or_utm: Regex,
        schwerpunkträume: Regex,
        numbered_series: Regex,
        timerange: Regex,
        timeseries: Regex,
        harmonize_whitespace: Regex,
    }

    impl Default for Patterns {
        fn default() -> Self {
            Self {
                bfn_schriften: Regex::new(r"BfN Schriften \d+ - ").unwrap(),
                gk_or_utm: Regex::new(r",?\s*\(?(?:GK|UTM)\)?,?").unwrap(),
                schwerpunkträume: Regex::new(
                    r".*(?P<keep_text>Schwerpunkträume für die Maßnahmenumsetzung) -.*",
                )
                .unwrap(),
                numbered_series: Regex::new(r"^\d+\.").unwrap(),
                timerange: Regex::new(r"(?:ab\s*)?(?:19|20)\d{2}[\s-]*(?:bis\s*)?(?:19|20)\d{2}")
                    .unwrap(),
                timeseries: Regex::new(r"(?:,\s*)?(?:in\s*)?(?:19|20)\d{2}(\-\d{2})?(\-\d{2})?")
                    .unwrap(),
                harmonize_whitespace: Regex::new(r"\s{2,}").unwrap(),
            }
        }
    }

    static PATTERNS: Lazy<Patterns> = Lazy::new(Patterns::default);

    let patterns = &*PATTERNS;

    let title = patterns.bfn_schriften.replace(title, "");
    let title = patterns.gk_or_utm.replace_all(title.as_ref(), "");
    let title = patterns
        .schwerpunkträume
        .replace(title.as_ref(), "${keep_text}");
    let title = patterns.numbered_series.replace_all(title.as_ref(), "");
    let title = patterns.timerange.replace_all(title.as_ref(), "");
    let title = patterns.timeseries.replace_all(title.as_ref(), "");
    let title = patterns
        .harmonize_whitespace
        .replace_all(title.as_ref(), " ");

    title.trim().into_owned()
}

fn merge_texts(base_text: &mut Option<String>, mut texts: Vec<String>) {
    texts.sort_unstable();
    texts.dedup();

    if texts.is_empty() {
        return;
    }

    match base_text {
        Some(base_text) => {
            texts.retain(|text| text != base_text);

            for description in &texts {
                base_text.push_str("\n\n");
                base_text.push_str(description);
            }
        }
        base_text @ None => {
            *base_text = Some(texts.join("\n\n"));
        }
    }
}

fn most_freq_value<I, T>(iter: I) -> Option<T>
where
    I: Iterator<Item = T>,
    T: Eq + Hash,
{
    // This must be an unkeyed hash to ensure deterministic results when there are ties.
    let mut counts = FxHashMap::<T, usize>::default();

    for val in iter {
        *counts.entry(val).or_default() += 1;
    }

    counts
        .into_iter()
        .max_by_key(|(_, count)| *count)
        .map(|(val, _)| val)
}

// FIXME: Brute-force implementation, should use suffix trees or dynamic programming.
fn longest_common_substr<'a>(mut lhs: &'a str, mut rhs: &'a str) -> &'a str {
    if lhs == rhs {
        return lhs;
    }

    if lhs.len() > rhs.len() {
        swap(&mut lhs, &mut rhs);
    }

    if lhs.len() * rhs.len() > 1_000_000 {
        tracing::warn!(
            "Expecting a lot of possible iterations to determine longest common substring"
        );
    }

    let lhs = lhs.as_bytes();
    let rhs = rhs.as_bytes();

    let mut best = &[][..];

    for begin in 0..lhs.len() {
        let lhs = &lhs[begin..];
        if best.len() >= lhs.len() {
            break;
        }

        for end in (1..=lhs.len()).rev() {
            let lhs = &lhs[..end];
            if best.len() >= lhs.len() {
                break;
            }

            if find(rhs, lhs).is_some() {
                best = lhs;
            }
        }
    }

    from_utf8(best).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn most_freq_value_works() {
        assert_eq!(
            most_freq_value(
                [
                    License::OtherClosed,
                    License::OtherOpen,
                    License::MixedClosed,
                    License::OtherOpen,
                    License::OtherClosed,
                    License::OtherOpen
                ]
                .into_iter()
            ),
            Some(License::OtherOpen),
        )
    }

    #[test]
    fn longest_common_substr_works() {
        assert_eq!(longest_common_substr("", ""), "");
        assert_eq!(longest_common_substr("foo", ""), "");
        assert_eq!(longest_common_substr("foo", "foobar"), "foo");
        assert_eq!(longest_common_substr("bar", "foobar"), "bar");
        assert_eq!(longest_common_substr("foobar", "qux"), "");
        assert_eq!(longest_common_substr("foobar", "ooba"), "ooba");
        assert_eq!(longest_common_substr("foobar", "zoobaz"), "ooba");

        assert_eq!(
            longest_common_substr(
                "INSPIRE-WFS SL Geplante Bodennutzung Bebauungspläne Wadgassen - Räumlicher Plan - OGC API Features",
                "INSPIRE-WFS SL Geplante Bodennutzung Bebauungspläne Quierschied - OGC WFS Interface",
            ),
            "INSPIRE-WFS SL Geplante Bodennutzung Bebauungspläne ",
        );

        assert_eq!(
            longest_common_substr(
                "BfN Schriften 450 - Gefährdung und Schutz der Haie und Rochen in den deutschen Meeresgebieten der Nord- und Ostsee und der Südsee",
                "Gefährdung und Schutz der Haie und Rochen in den deutschen Meeresgebieten der Nord- und Ostsee",
            ),
            "Gefährdung und Schutz der Haie und Rochen in den deutschen Meeresgebieten der Nord- und Ostsee",
        );
    }

    #[test]
    fn preprocess_title_works() {
        let string1 = "Ruheplätze (gepuffert, 250m) von Seehunden im Schleswig-Holsteinischen Wattenmeer, GK, 1994#locale-eng:Resting areas (buffered, 250m) of Seals in the Schleswig-Holstein Wadden Sea in 1994";
        let string2 = "Ruheplätze (gepuffert, 250m) von Seehunden im Schleswig-Holsteinischen Wattenmeer, GK, 1994#locale-eng:Resting areas (buffered, 250m) of Seals in the Schleswig-Holstein Wadden Sea, GK, in 1994";

        assert_eq!(preprocess_title(string1), preprocess_title(string2));

        let string1 = "Makrophytische Grünalgen im Schleswig-Holsteinischen Wattenmeer 2001-07-18 (GK)#locale-eng:Macrophytic green algea in the Schleswig-Holstein Wadden Sea 2001-08-18";
        let string2 = "Makrophytische Grünalgen im Schleswig-Holsteinischen Wattenmeer 2001-07-18 (UTM)#locale-eng:Macrophytic green algea in the Schleswig-Holstein Wadden Sea 2001-08-18";
        assert_eq!(preprocess_title(string1), "Makrophytische Grünalgen im Schleswig-Holsteinischen Wattenmeer #locale-eng:Macrophytic green algea in the Schleswig-Holstein Wadden Sea");
        assert_eq!(preprocess_title(string1), preprocess_title(string2));

        assert_eq!(preprocess_title("Schwerpunkträume für die Maßnahmenumsetzung - Arten mit internationaler Verantwortung Brandenburgs"),
    "Schwerpunkträume für die Maßnahmenumsetzung");

        assert_eq!(preprocess_title("foo  bar foo     bar"), "foo bar foo bar");
    }
}
