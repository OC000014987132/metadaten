use std::cell::Cell;
use std::mem::take;

use anyhow::{anyhow, ensure, Context, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use hashbrown::HashSet;
use regex::Regex;
use scraper::{Element, ElementRef, Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{format_description::well_known::Iso8601, Date};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_text, make_key, parse_attributes, select_first_text, select_text, GermanDate,
        GermanMonth,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Person, PersonRole, Region,
    Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_projects(dir, client, source, selectors).await?;
    ensure!(count != 0, "No projects found");

    let (count1, results1, errors1) = fetch_events(dir, client, source, selectors).await?;
    ensure!(count1 != 0, "No events found");
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_news(dir, client, source, selectors).await?;
    ensure!(count1 != 0, "No news found");
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_publications(dir, client, source, selectors).await?;
    ensure!(count1 != 0, "No publications found");
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_nofm(dir, client, source, selectors).await?;
    ensure!(count1 != 0, "No number of the month found");
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_media_rec(dir, client, source, selectors).await?;
    ensure!(count1 != 0, "No media recommendations");
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_projects(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let projects;

    {
        let url = source.url.join("projekte")?;

        let text = client
            .fetch_text(source, "projects".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        projects = document
            .select(&selectors.projects)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<HashSet<_>>();
    }

    let count = projects.len();

    let (results, errors) = fetch_many(0, 0, projects, |project| async move {
        fetch_project_details(dir, client, source, selectors, &project)
            .await
            .with_context(|| format!("Failed to fetch project details for {project}"))
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_project_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    project: &str,
) -> Result<(usize, usize, usize)> {
    let key = format!("project-{}", make_key(project));

    let url = source.url.join(project)?;

    let mut title = String::new();
    let mut resources = SmallVec::new();
    let mut time_ranges = SmallVec::new();
    let mut comment = None;

    let mut organisations = SmallVec::new();
    let mut persons = Vec::new();

    let description;

    {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        for element in document.select(&selectors.project_metadata) {
            let text = collect_text(element.text());

            if let Some(text) = text.strip_prefix("Projekttitel") {
                text.trim().clone_into(&mut title);
            } else if text.starts_with("Internet") {
                if let Some(link) = element.select(&selectors.project_link).next() {
                    let url = source.url.join(link.attr("href").unwrap())?;

                    resources.push(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description: Some("Website".to_owned()),
                            url: url.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            } else if text.starts_with("Projektsteckbrief") {
                if let Some(link) = element.select(&selectors.project_link).next() {
                    let url = source.url.join(link.attr("href").unwrap())?;

                    resources.push(
                        Resource {
                            r#type: ResourceType::Pdf,
                            description: Some("Projektsteckbrief".to_owned()),
                            url: url.into(),
                            primary_content: true,
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            } else if text.starts_with("Projekt-Cluster") {
                if let Some(link) = element.select(&selectors.project_link).next() {
                    let url = source.url.join(link.attr("href").unwrap())?;

                    resources.push(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description: Some("Projekt-Cluster".to_owned()),
                            url: url.into(),
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            } else if let Some(captures) = selectors.project_runtime.captures(&text) {
                let from = captures["from"].parse::<GermanDate>()?;
                let until = captures["until"].parse::<GermanDate>()?;

                time_ranges.push((from, until).into());
            } else if text.starts_with("Förderkennzeichen:") {
                comment = Some(text);
            }
        }

        let contact = document
            .select(&selectors.project_contact)
            .find(|element| {
                element
                    .prev_sibling_element()
                    .map(|element| {
                        element.value().name() == "h2"
                            && collect_text(element.text()).contains("Kontakt")
                    })
                    .unwrap_or(false)
            })
            .ok_or_else(|| anyhow!("Missing contact"))?;

        let mut contact = contact
            .text()
            .map(|text| text.trim())
            .filter(|text| !text.is_empty());

        let person = contact
            .next()
            .ok_or_else(|| anyhow!("Missing person"))?
            .to_owned();

        persons.push(Person {
            name: person,
            role: PersonRole::Manager,
            ..Default::default()
        });

        let organisation = contact
            .next()
            .ok_or_else(|| anyhow!("Missing organisation"))?
            .to_owned();

        organisations.push(Organisation::Other {
            name: organisation,
            role: OrganisationRole::Management,
            websites: Default::default(),
        });

        for element in document.select(&selectors.project_details) {
            let key = select_first_text(element, &selectors.project_details_key, "key")?;

            if key.contains("Partner") {
                for element in element.select(&selectors.project_details_partners) {
                    organisations.push(Organisation::Other {
                        name: collect_text(element.text()),
                        role: OrganisationRole::Unknown,
                        websites: Default::default(),
                    });
                }
            }
        }

        description = select_text(&document, &selectors.project_description);
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        comment,
        resources,
        time_ranges,
        organisations,
        persons,
        language: Language::German,
        license: License::AllRightsReserved,
        source_url: url.into(),
        origins: source.origins.clone(),
        types: smallvec![Type::Text {
            text_type: TextType::Editorial,
        }],
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_events(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    const KINDS: &[EventKind] = &[
        EventKind {
            href: "events-news/interne-vergangene-events",
            key: "events-internal-past",
            param: "page_e156",
        },
        EventKind {
            href: "events-news/externe-vergangene-events",
            key: "events-external-past",
            param: "page_e157",
        },
        EventKind {
            href: "events-news/interne-anstehende-events",
            key: "events-internal-current",
            param: "only_one_page",
        },
        EventKind {
            href: "events-news/externe-anstehende-events",
            key: "events-external-current",
            param: "only_one_page",
        },
    ];

    let mut results = 0;
    let mut errors = 0;

    for kind in KINDS {
        let (pages, results1, errors1) =
            fetch_events_page(dir, client, source, selectors, kind, 1).await?;

        let (results1, errors1) = fetch_many(results1, errors1, 2..=pages, |page| async move {
            fetch_events_page(dir, client, source, selectors, kind, page)
                .await
                .with_context(|| format!("Failed to fetch events on page {page}"))
        })
        .await;

        results += results1;
        errors += errors1;
    }

    Ok((results, results, errors))
}

async fn fetch_events_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    kind: &EventKind<'_>,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join(kind.href)?;

    url.query_pairs_mut()
        .append_pair(kind.param, &page.to_string());

    let pages;
    let mut events = Vec::new();

    {
        let text = client
            .fetch_text(source, format!("{}-{}", kind.key, page), &url)
            .await?;

        let document = Html::parse_document(&text);

        pages = parse_attributes(
            &document,
            &selectors.last_events_page,
            &selectors.last_events_page_value,
            "href",
            "last page",
        )
        .next()
        .unwrap_or(Ok(1))?;

        for element in document.select(&selectors.events) {
            events.push(Event::parse(selectors, element));
        }
    }

    let (results, errors) = fetch_many(0, 0, events, |event| async {
        fetch_event(dir, client, source, selectors, event?).await
    })
    .await;

    Ok((pages, results, errors))
}

async fn fetch_event(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    event: Event,
) -> Result<(usize, usize, usize)> {
    if !event.href.starts_with("event/") && !event.href.starts_with("Termin-Detail/") {
        return Ok((1, 0, 0));
    }

    let key = make_key(&event.href).into_owned();

    let url = source.url.join(&event.href)?;

    let dataset = {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let description = select_text(&document, &selectors.event_description);

        let mut resources = SmallVec::new();

        for attachment in document.select(&selectors.event_attachments) {
            let url = source.url.join(attachment.attr("href").unwrap())?;
            let description = collect_text(attachment.text());

            resources.push(
                Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        let time_ranges = smallvec![event.time.into()];

        let region = event.location.map(Region::Other);

        Dataset {
            title: event.title,
            description: Some(description),
            time_ranges,
            regions: region.into_iter().collect(),
            resources,
            // TODO: Some events are described in English but lack a way of detecting that.
            language: Language::German,
            license: License::AllRightsReserved,
            source_url: url.into(),
            origins: source.origins.clone(),
            types: smallvec![Type::Event],
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug)]
struct EventKind<'a> {
    href: &'a str,
    key: &'a str,
    param: &'a str,
}

#[derive(Debug)]
struct Event {
    title: String,
    href: String,
    time: Date,
    location: Option<CompactString>,
}

impl Event {
    fn parse(selectors: &Selectors, element: ElementRef<'_>) -> Result<Self> {
        let title = select_first_text(element, &selectors.event_title, "title")?;

        let href = element
            .select(&selectors.event_href)
            .next()
            .map(|element| element.attr("href").unwrap())
            .or_else(|| {
                element
                    .parent_element()
                    .filter(|element| element.value().name() == "a")
                    .and_then(|element| element.attr("href"))
            })
            .ok_or_else(|| anyhow!("Missing link"))?
            .to_owned();

        let time = element
            .select(&selectors.event_time)
            .next()
            .ok_or_else(|| anyhow!("Missing time"))?;

        let time = Date::parse(time.attr("datetime").unwrap(), &Iso8601::DEFAULT)?;

        let location = element
            .select(&selectors.event_location)
            .next()
            .map(|element| {
                let text = collect_text(element.text());

                text.strip_prefix("Ort: ").unwrap_or(&text).into()
            });

        Ok(Self {
            title,
            href,
            time,
            location,
        })
    }
}

async fn fetch_news(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let (pages, results, errors) = fetch_news_page(dir, client, source, selectors, 1).await?;

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| async move {
        fetch_news_page(dir, client, source, selectors, page)
            .await
            .with_context(|| format!("Failed to fetch news on page {page}"))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_news_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join("news")?;

    url.query_pairs_mut()
        .append_pair("page_n13", &page.to_string());

    let pages;
    let mut items = Vec::new();

    {
        let text = client
            .fetch_text(source, format!("news-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        pages = parse_attributes(
            &document,
            &selectors.last_news_page,
            &selectors.last_news_page_value,
            "href",
            "last page",
        )
        .next()
        .unwrap_or(Ok(1))?;

        for element in document.select(&selectors.news_items) {
            items.push(NewsItem::parse(selectors, element));
        }
    }

    let (results, errors) = fetch_many(0, 0, items, |item| async {
        fetch_news_item(dir, client, source, selectors, item?).await
    })
    .await;

    Ok((pages, results, errors))
}

async fn fetch_news_item(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: NewsItem,
) -> Result<(usize, usize, usize)> {
    if !item.href.starts_with("news-details/") {
        return Ok((1, 0, 0));
    }

    let key = make_key(&item.href).into_owned();

    let url = source.url.join(&item.href)?;

    let description;
    let mut resources = SmallVec::new();

    {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        // TODO: Some descriptions contain miscellanous links and
        // we should include them when we support HTML fragments as descriptions.
        description = select_text(&document, &selectors.news_item_description);

        for image in document.select(&selectors.news_item_images) {
            let url = source.url.join(image.attr("src").unwrap())?;
            let description = image.attr("alt").map(|alt| alt.to_owned());

            resources.push(
                Resource {
                    r#type: ResourceType::Image,
                    description,
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }
    }

    let time_ranges = smallvec![item.time.into()];

    let dataset = Dataset {
        title: item.title,
        description: Some(description),
        time_ranges,
        resources,
        language: Language::German,
        license: License::AllRightsReserved,
        source_url: url.into(),
        origins: source.origins.clone(),
        types: smallvec![Type::Text {
            text_type: TextType::News,
        }],
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug)]
struct NewsItem {
    title: String,
    href: String,
    time: Date,
}

impl NewsItem {
    fn parse(selectors: &Selectors, element: ElementRef<'_>) -> Result<Self> {
        let title = select_first_text(element, &selectors.news_item_title, "title")?;

        let href = element
            .select(&selectors.news_item_href)
            .next()
            .map(|element| element.attr("href").unwrap())
            .or_else(|| {
                element
                    .parent_element()
                    .filter(|element| element.value().name() == "a")
                    .and_then(|element| element.attr("href"))
            })
            .ok_or_else(|| anyhow!("Missing link"))?
            .to_owned();

        let time = element
            .select(&selectors.news_item_time)
            .next()
            .ok_or_else(|| anyhow!("Missing time"))?;

        let time = Date::parse(time.attr("datetime").unwrap(), &Iso8601::DEFAULT)?;

        Ok(Self { title, href, time })
    }
}

async fn fetch_publications(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let mut datasets = Vec::<Result<Dataset>>::new();

    for page in ["publikationen", "presse"] {
        let url = source.url.join(page)?;

        {
            let text = client
                .fetch_text(source, format!("publications-{page}"), &url)
                .await?;

            let document = Html::parse_document(&text);

            for group in document.select(&selectors.publications_groups) {
                let group_title =
                    select_first_text(group, &selectors.publications_group_title, "group title")?;

                let year_header = Cell::new(None);

                datasets.extend(
                    group
                        .select(&selectors.publications_details)
                        .filter_map(|element| {
                            let text = collect_text(element.text());

                            if !text.is_empty() {
                                if let Some(captures) =
                                    selectors.publications_year_header.captures(&text)
                                {
                                    year_header.set(captures[1].parse::<i32>().ok());

                                    None
                                } else {
                                    Some((element, text))
                                }
                            } else {
                                None
                            }
                        })
                        .map(|(element, title)| {
                            let link = element
                                .select(&selectors.publications_details_link)
                                .next()
                                .map(|element| element.attr("href").unwrap())
                                .map(|href| source.url.join(href))
                                .transpose()?;

                            let resources = link
                                .into_iter()
                                .map(|link| {
                                    Resource {
                                        r#type: ResourceType::WebPage,
                                        url: link.clone().into(),
                                        description: Some(link.to_string()),
                                        primary_content: true,
                                        ..Default::default()
                                    }
                                    .guess_or_keep_type()
                                })
                                .collect();

                            let issued = if let Some(captures) =
                                selectors.publications_details_date.captures(&title)
                            {
                                let day = captures["day"].parse()?;
                                let month = captures["month"].parse::<GermanMonth>()?.into();
                                let year = captures["year"].parse()?;

                                Some(Date::from_calendar_date(year, month, day)?)
                            } else if let Some(captures) =
                                selectors.publications_details_year.captures(&title)
                            {
                                let year = captures["year"].parse()?;

                                Some(Date::from_ordinal_date(year, 1)?)
                            } else if let Some(year) = year_header.get() {
                                Some(Date::from_ordinal_date(year, 1)?)
                            } else {
                                None
                            };

                            Ok(Dataset {
                                title: format!("{group_title}: {title}"),
                                issued,
                                resources,
                                language: Language::German,
                                license: License::AllRightsReserved,
                                source_url: url.clone().into(),
                                origins: source.origins.clone(),
                                types: smallvec![Type::Text {
                                    text_type: TextType::Publication,
                                }],
                                ..Default::default()
                            })
                        }),
                );
            }
        }
    }

    write_datasets(dir, client, source, datasets, |index| {
        format!("publication-{index}")
    })
    .await
}

async fn fetch_nofm(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("zahl-des-monats")?;

    let mut datasets = Vec::new();

    let make_dataset = |title, date: String, description| {
        let title = format!("{title} {date}");

        let mut date = date.split_whitespace();

        let month = date
            .next()
            .ok_or_else(|| anyhow!("Missing month"))?
            .parse::<GermanMonth>()?
            .into();
        let year = date
            .next()
            .ok_or_else(|| anyhow!("Missing year"))?
            .parse()?;

        let issued = Some(Date::from_calendar_date(year, month, 1)?);

        Ok(Dataset {
            title,
            description: Some(description),
            issued,
            language: Language::German,
            license: License::AllRightsReserved,
            source_url: url.clone().into(),
            origins: source.origins.clone(),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial,
            }],
            ..Default::default()
        })
    };

    {
        let text = client.fetch_text(source, "nofm".to_owned(), &url).await?;

        let document = Html::parse_document(&text);

        let title = select_first_text(&document, &selectors.nofm_title, "title")?;

        let mut date = String::new();
        let mut description = String::new();

        for element in document.select(&selectors.nofm_content) {
            let text = collect_text(element.text());

            match element.value().name() {
                "h2" => {
                    if !date.is_empty() {
                        datasets.push(make_dataset(
                            &title,
                            take(&mut date),
                            take(&mut description),
                        ));
                    }

                    date = text;
                }
                "p" if !date.is_empty() => {
                    description.push_str(&text);
                }
                _ => (),
            }
        }

        if !date.is_empty() {
            datasets.push(make_dataset(&title, date, description));
        }
    }

    write_datasets(dir, client, source, datasets, |index| {
        format!("nofm-{index}")
    })
    .await
}

async fn fetch_media_rec(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join("medientipps")?;

    let title;
    let description;
    let resources;

    {
        let text = client
            .fetch_text(source, "media-rec".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        title = select_first_text(&document, &selectors.media_rec_title, "title")?;

        description = select_text(&document, &selectors.media_rec_description);

        resources = document
            .select(&selectors.media_rec_link)
            .map(|element| {
                let url = source.url.join(element.attr("href").unwrap())?;

                let description = collect_text(element.text());

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: url.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;
    }

    let dataset = Dataset {
        title,
        description: Some(description),
        resources,
        language: Language::German,
        license: License::AllRightsReserved,
        source_url: url.clone().into(),
        origins: source.origins.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, "media-rec".to_owned(), dataset).await
}

async fn write_datasets(
    dir: &Dir,
    client: &Client,
    source: &Source,
    datasets: Vec<Result<Dataset>>,
    key: impl Fn(usize) -> String,
) -> Result<(usize, usize, usize)> {
    let count = datasets.len();

    let datasets = datasets
        .into_iter()
        .rev()
        .enumerate()
        .map(|(index, dataset)| (key(index), dataset));

    let (results, errors) = fetch_many(0, 0, datasets, |(key, dataset)| async {
        write_dataset(dir, client, source, key, dataset?).await
    })
    .await;

    Ok((count, results, errors))
}

selectors! {
    projects: "nav#main-navigation li.active ul > li > a[href]",
    project_description: "div.rs-column.-large-first.-large-col-4-3 > div.ce_text p",
    project_metadata: "div.rs-column.-large-last div.ce_text p",
    project_link: "a[href]",
    project_runtime: r"(?<from>\d{2}\.\d{2}\.\d{4})\s*\-\s*(?<until>\d{2}\.\d{2}\.\d{4})" as Regex,
    project_contact: "div.rs-column.-large-first.-large-col-4-3 > div.ce_text > p",
    project_details: "section.ce_accordion",
    project_details_key: "div.toggler",
    project_details_partners: "div.accordion ul li",
    last_events_page: "li.last a.last[href*='events?page_e15']",
    last_events_page_value: r"events\?page_e15\d=(\d+)" as Regex,
    events: "div.event.cal_4, div.event.cal_1",
    event_title: "h4",
    event_href: "h2 a[href]",
    event_time: "p.time time[datetime]",
    event_location: "p.location",
    event_description: "div.event div.ce_text",
    event_attachments: "a[href]",
    last_news_page: "li.last a.last[href^='news?page_n13=']",
    last_news_page_value: r"news\?page_n13=(\d+)" as Regex,
    news_items: "article.news-image-teaser",
    news_item_title: "h4",
    news_item_href: "a[href]",
    news_item_time: "time[datetime]",
    news_item_description: "div.mod_newsreader div.ce_text",
    news_item_images: "div.ce_gallery figure img[src]",
    publications_groups: "section.ce_accordion",
    publications_group_title: "div.toggler",
    publications_year_header: r"^(\d{4}):$" as Regex,
    publications_details: "div.accordion p",
    publications_details_link: "a[href]",
    publications_details_year: r"\((?<year>\d{4})\):" as Regex,
    publications_details_date: r"\((?<day>\d{1,2})\.\s+(?<month>[A-Z][a-z]+)\s+(?<year>\d{4})\)" as Regex,
    nofm_title: "div.ce_text h1:not(.ce_headline)",
    nofm_content: "div.ce_text > *",
    media_rec_title: "div.ce_text > h1",
    media_rec_description: "div.ce_text > p",
    media_rec_link: "div.accordion p > a[href]",
}
