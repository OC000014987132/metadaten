use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::Date;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_text, make_key, parse_attribute, select_first_text, select_text, GermanMonth,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Person, PersonRole, Region,
    Resource, Tag, TimeRange,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (max_page, results, errors) = fetch_page(dir, client, source, selectors, 0).await?;

    let (results, errors) = fetch_many(results, errors, 1..=max_page, |page| {
        fetch_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((results, results, errors))
}

#[derive(Debug)]
struct Item {
    link: String,
    title: String,
    location: Option<String>,
    time_range: Option<TimeRange>,
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let max_page;
    let items;

    {
        let mut url = source.url.join("suche")?;
        url.query_pairs_mut().append_pair("page", &page.to_string());

        let text = client
            .fetch_text(source, format!("page-{}", page), &url)
            .await?;

        let document = Html::parse_document(&text);

        max_page = parse_attribute::<_, usize>(
            &document,
            &selectors.last_page_link,
            &selectors.last_page_number,
            "href",
            "maximum page number",
        )
        .unwrap_or(0);

        items = document
            .select(&selectors.articles)
            .map(|element| {
                let link = element
                    .select(&selectors.article_link)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link"))?;

                let title = collect_text(link.text());

                let link = link.attr("href").unwrap().to_owned();

                let location =
                    select_first_text(element, &selectors.article_region, "location").ok();

                let time_range = select_text(element, &selectors.article_time_range);

                let time_range = if let Some(captures) = selectors.date_select.captures(&time_range)
                {
                    let date = Date::from_calendar_date(
                        captures["year"].parse()?,
                        captures["month"].parse::<GermanMonth>()?.into(),
                        captures["day"].parse()?,
                    )?;

                    Some(date.into())
                } else {
                    None
                };

                Ok(Item {
                    link,
                    title,
                    location,
                    time_range,
                })
            })
            .collect::<Result<Vec<_>>>()?;
    }

    let (results, errors) = fetch_many(0, 0, items, |item| async move {
        fetch_details(dir, client, source, selectors, item).await
    })
    .await;

    Ok((max_page, results, errors))
}

async fn fetch_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&item.link).into_owned();
    let dataset;

    {
        let url = source.url.join(&item.link)?;

        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        let description = select_text(&document, &selectors.page_description);

        let tags = document
            .select(&selectors.page_tags)
            .map(|element| Tag::Other(collect_text(element.text()).into()))
            .collect();

        let issued = select_text(&document, &selectors.page_issued);

        let issued = if let Some(captures) = selectors.date_select.captures(&issued) {
            Some(Date::from_calendar_date(
                captures["year"].parse()?,
                captures["month"].parse::<GermanMonth>()?.into(),
                captures["day"].parse()?,
            )?)
        } else {
            None
        };

        let persons = document
            .select(&selectors.page_author)
            .map(|element| {
                let name = collect_text(element.text());

                Person {
                    name,
                    role: PersonRole::Creator,
                    ..Default::default()
                }
            })
            .collect();

        let organisations = document
            .select(&selectors.page_institution)
            .map(|element| {
                let name = collect_text(element.text());

                Organisation::Other {
                    name,
                    role: OrganisationRole::Unknown,
                    websites: Default::default(),
                }
            })
            .collect();

        let resources = document
            .select(&selectors.page_resource)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = element.attr("title").map(|title| title.to_owned());

                Ok(Resource {
                    description,
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                })
            })
            .collect::<Result<SmallVec<_>>>()?;

        let region = item.location.map(|location| Region::Other(location.into()));

        let time_ranges = item.time_range.into_iter().collect();

        dataset = Dataset {
            title: item.title,
            types: smallvec![Type::Text {
                text_type: TextType::Editorial,
            }],
            regions: region.into_iter().collect(),
            time_ranges,
            description: Some(description),
            tags,
            persons,
            organisations,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            resources,
            issued,
            source_url: url.into(),
            ..Default::default()
        };
    }

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    last_page_link:"li.pager-last.last a[href]",
    last_page_number: r"/suche\?page=(\d+)" as Regex,
    page_description: "div.field-name-field-difu-teaser div.field-item, div.field-name-field-difu-fliesstext div.field-item",
    page_tags: "div.field-name-field-difu-schlagworte div.field-item",
    page_issued: "div.field-name-field-difu-datum div.field-item",
    page_author: "div.field-name-field-difu-autor div.field-item",
    page_institution: "div.field_difu_kontakt_institution",
    page_resource: "span.region_left_bottom li a[href]",
    date_select: r"(?<day>\d+)\.\s+(?<month>\w+)\s+(?<year>\d{4})" as Regex,
    articles: "div.node-difu_wrapper.search-result",
    article_link: "div.wrapper h2.title a[href]",
    article_region: "div.difu-veranstaltungsort",
    article_time_range: "div.difu-start-ende",
}
