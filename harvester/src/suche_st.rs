use std::path::Path;

use anyhow::{anyhow, Context, Result};
use cap_std::fs::Dir;
use glib::Bytes as GBytes;
use poppler::Document;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{format_description::well_known::Iso8601, macros::format_description, Date};
use url::{Position, Url};

use harvester::{
    client::{CachedDocument, Client},
    fetch_many, selectors,
    utilities::{
        collect_pages, collect_text, make_key, parse_text, select_first_text, select_text,
        GermanDate, GermanMonth,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Person, PersonRole, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();

    let (count, results, errors) = fetch_page(dir, client, source, &selectors, 0).await?;

    let pages = count.div_ceil(BATCH_SIZE);

    // Do multiple requests for paginated search at the same time and fetch respective search result pages
    let (results, errors) = fetch_many(results, errors, 1..pages, |page| {
        fetch_page(dir, client, source, &selectors, page * BATCH_SIZE)
    })
    .await;

    Ok((count, results, errors))
}

const BATCH_SIZE: usize = 10;

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    start: usize,
) -> Result<(usize, usize, usize)> {
    let start = start.to_string();
    tracing::trace!(
        "Fetching {} datasets starting from result number {}",
        BATCH_SIZE,
        start
    );

    let (portal, query) = source
        .filter
        .as_ref()
        .ok_or_else(|| anyhow!("Missing filter"))?
        .split_once(':')
        .ok_or_else(|| anyhow!("Invalid filter, use `portal:query`"))?;

    let count;
    let links;

    {
        let mut url = source.url.clone();

        url.query_pairs_mut()
            .append_pair("q", query)
            .append_pair("fq", &format!("appKey:\"{portal}\""))
            .append_pair("start", &start);

        let text = client.fetch_text(source, start, &url).await?;

        let document = Html::parse_document(&text);

        count = parse_text(
            &document,
            &selectors.results,
            &selectors.results_value,
            "number of results",
        )?;

        links = fetch_links(selectors, &document);
    }

    let (results, errors) = fetch_many(0, 0, links, |link| async move {
        let doc = client
            .fetch_doc(source, format!("art-{}.isol", link.key), &link.url)
            .await?;

        match &*doc {
            CachedDocument::Html(text) => {
                if link.agency == "LVERMGEO" {
                    fetch_geo_article(dir, client, source, selectors, link, text).await
                } else if link.agency == "PDB" {
                    fetch_presse_article(dir, client, source, selectors, link, text).await
                } else {
                    fetch_article(dir, client, source, selectors, link, text).await
                }
            }
            CachedDocument::Pdf(bytes) => {
                fetch_document(dir, client, source, selectors, link, bytes).await
            }
        }
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Debug)]
struct Link {
    url: Url,
    key: String,
    agency: &'static str,
    modified: Option<Date>,
    title: String,
}

fn fetch_links(selectors: &Selectors, document: &Html) -> Vec<Link> {
    document
        .select(&selectors.result_row)
        .filter_map(|elem| {
            let href = elem
                .select(&selectors.result_link)
                .next()?
                .attr("href")
                .unwrap();

            let url = Url::parse(href).ok()?;

            let agency = match url.host_str() {
                Some("mwl.sachsen-anhalt.de") => "MWL",
                Some("mwu.sachsen-anhalt.de" | "wrrl.sachsen-anhalt.de") => "MWU",
                Some("lau.sachsen-anhalt.de") => "LAU",
                Some("lhw.sachsen-anhalt.de") => "LHW",
                Some("lena.sachsen-anhalt.de") => "LENA",
                Some("www.lvermgeo.sachsen-anhalt.de" | "lvermgeo.sachsen-anhalt.de") => "LVERMGEO",
                Some("presse.sachsen-anhalt.de") => "PDB",
                _ => {
                    tracing::trace!("Unknown agency in link {:?}", url.host_str());
                    return None;
                }
            };

            if selectors.unsupported_extensions.is_match(url.path()) {
                return None;
            }

            let key = make_key(&format!("{}{}", agency, &url[Position::BeforePath..])).into_owned();

            let modified = elem
                .select(&selectors.result_date)
                .filter_map(|elem| {
                    let text = collect_text(elem.text());

                    if text.starts_with("Aktualisiert:") {
                        Some(text)
                    } else {
                        None
                    }
                })
                .next()
                .and_then(|text| {
                    let date = text.split_ascii_whitespace().nth(2)?;
                    Date::parse(date, format_description!("[day].[month].[year]")).ok()
                });

            let title = collect_text(
                elem.select(&selectors.result_link_title)
                    .flat_map(|element| element.text()),
            );

            Some(Link {
                url,
                key,
                agency,
                modified,
                title,
            })
        })
        .collect()
}

async fn fetch_geo_article(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Link,
    text: &str,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", link.url);

    let dataset;

    {
        let document = Html::parse_document(text);

        let title = select_text(&document, &selectors.geo_title);
        let description = select_text(&document, &selectors.geo_description);

        dataset = Dataset {
            title,
            description: Some(description),
            origins: source
                .origins
                .iter()
                .map(|origin| origin.replace("<agency>", link.agency).into())
                .collect(),
            license: License::MixedClosed, // two differing documents describing the usage exist: https://www.lvermgeo.sachsen-anhalt.de/de/datei/anzeigen/id/3567,501/nutzungsbedingungenv5.0_b.pdf and https://www.lvermgeo.sachsen-anhalt.de/de/impressum.html
            source_url: link.url.into(),
            language: Language::German,
            ..Default::default()
        };
    }

    write_dataset(dir, client, source, link.key, dataset).await
}

async fn fetch_presse_article(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Link,
    text: &str,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", link.url);

    let dataset = {
        let document = Html::parse_document(text);

        let title = select_first_text(&document, &selectors.presse_title, "title")?;
        let description = select_text(&document, &selectors.presse_description);
        let place_and_date = select_first_text(
            &document,
            &selectors.presse_place_and_date,
            "place_and_date",
        )?;

        let region;
        let issued;

        if let Some(captures) = selectors
            .presse_place_and_date_value
            .captures(&place_and_date)
        {
            region = Some(Region::Other(
                captures.name("place").unwrap().as_str().into(),
            ));

            issued = Some(
                captures
                    .name("date")
                    .unwrap()
                    .as_str()
                    .parse::<GermanDate>()?
                    .into(),
            );
        } else {
            tracing::error!("Failed to extract place and date from `{place_and_date}`.");

            region = None;
            issued = None;
        }

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::PressRelease,
            }],
            regions: align_region(region),
            issued,
            origins: source
                .origins
                .iter()
                .map(|origin| origin.replace("<agency>", link.agency).into())
                .collect(),
            license: License::AllRightsReserved,
            source_url: link.url.into(),
            language: Language::German,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, link.key, dataset).await
}

async fn fetch_article(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Link,
    text: &str,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", link.url);

    let dataset = {
        let document = Html::parse_document(text);

        let modified = document
            .select(&selectors.last_modified)
            .next()
            .map(|element| -> Result<Date, _> {
                let date = element.attr("content").unwrap();
                Date::parse(date, &Iso8601::DEFAULT)
            })
            .transpose()?;

        let article = document
            .select(&selectors.article)
            .next()
            .ok_or_else(|| anyhow!("Missing article on {}", link.url))?;

        let mut title = collect_text(
            article
                .select(&selectors.title)
                .flat_map(|element| element.text()),
        );

        if title.is_empty() {
            title = collect_text(
                document
                    .select(&selectors.title_alternative1)
                    .flat_map(|element| element.text()),
            );
        }

        if title.is_empty() {
            title = collect_text(
                document
                    .select(&selectors.title_alternative2)
                    .flat_map(|element| element.text()),
            );
        }

        if title.is_empty() {
            tracing::debug!("Empty or missing title in article {}", link.url);
            return Ok((1, 0, 0));
        }

        let mut description = collect_text(
            article
                .select(&selectors.description)
                .flat_map(|element| element.text()),
        );

        if description.is_empty() {
            description = collect_text(
                article
                    .select(&selectors.description_alternative)
                    .flat_map(|element| element.text()),
            );
        }

        let resources = document
            .select(&selectors.resource_link)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let title = element.attr("title").unwrap();

                Ok(Resource {
                    r#type: ResourceType::Unknown,
                    description: Some(title.to_owned()),
                    url: link.url.join(href)?.into(),
                    ..Default::default()
                })
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title,
            description: Some(description),
            origins: source
                .origins
                .iter()
                .map(|origin| origin.replace("<agency>", link.agency).into())
                .collect(),
            license: License::AllRightsReserved,
            source_url: link.url.into(),
            resources,
            language: Language::German,
            modified: modified.or(link.modified),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, link.key, dataset).await
}

async fn fetch_document(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Link,
    bytes: &GBytes,
) -> Result<(usize, usize, usize)> {
    tracing::trace!("Fetching dataset at {}", link.url);

    let dataset = {
        let document = Document::from_bytes(bytes, None)
            .with_context(|| format!("Failed to parse PDF at `{}`", link.url))?;

        let description = collect_pages(&document, 0..3);

        let mut title = if link.title != "Folie 1" {
            link.title
        } else {
            Path::new(link.url.path())
                .file_name()
                .and_then(|file_name| file_name.to_str())
                .map_or(link.title, |file_name| file_name.to_owned())
        };

        let (region, issued_header, title_header) =
            match extract_place_and_date(selectors, link.url.path(), &description) {
                Ok(val) => val,
                Err(err) => {
                    tracing::debug!(
                        "Failed to extract place and date of {}: {:#}",
                        link.key,
                        err,
                    );
                    (None, None, None)
                }
            };

        if let Some(title_header) = title_header {
            title = title_header;
        }

        let persons = document
            .author()
            .filter(|author| !author.is_empty())
            .map(|author| Person {
                name: author.into(),
                role: PersonRole::Creator,
                ..Default::default()
            })
            .into_iter()
            .collect();

        let issued_metadata = document
            .creation_datetime()
            .map(|datetime| -> Result<Date> {
                let day = datetime.day_of_year();
                let year = datetime.year();

                let date = Date::from_ordinal_date(year, day.try_into()?)?;

                Ok(date)
            })
            .transpose()?;

        let modified_metadata = document
            .creation_datetime()
            .map(|datetime| -> Result<Date> {
                let day = datetime.day_of_year();
                let year = datetime.year();

                let date = Date::from_ordinal_date(year, day.try_into()?)?;
                Ok(date)
            })
            .transpose()?
            // documents cannot be modified before they were created
            .filter(|&date| Some(date) > issued_header);

        Dataset {
            title,
            description: Some(description),
            origins: source
                .origins
                .iter()
                .map(|origin| origin.replace("<agency>", link.agency).into())
                .collect(),
            license: License::AllRightsReserved,
            source_url: link.url.into(),
            language: Language::German,
            modified: modified_metadata.or(link.modified),
            issued: issued_header.or(issued_metadata),
            persons,
            regions: align_region(region),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, link.key, dataset).await
}

fn extract_place_and_date(
    selectors: &Selectors,
    path: &str,
    description: &str,
) -> Result<(Option<Region>, Option<Date>, Option<String>)> {
    // The code below was tested only for press releases.
    if !path.contains("Pressemitteilung") {
        return Ok((None, None, None));
    }

    let mut region = None;
    let mut issued_header = None;
    let mut title_header = None;

    if let Some(captures) = selectors.place_and_date.captures(description) {
        region = captures
            .name("place")
            .map(|place| place.as_str().trim().into())
            .map(Region::Other);

        if let (Some(day), Some(month), Some(year)) = (
            captures.name("day"),
            captures.name("month"),
            captures.name("year"),
        ) {
            let mut year = year.as_str().parse()?;
            let month = month.as_str().parse::<u8>()?.try_into()?;
            let day = day.as_str().parse()?;

            if year < 100 {
                year += 2000;
            }

            issued_header = Some(Date::from_calendar_date(year, month, day)?);
        } else if let (Some(day), Some(month), Some(year)) = (
            captures.name("day1"),
            captures.name("month1"),
            captures.name("year1"),
        ) {
            let year = year.as_str().parse()?;
            let month = month.as_str().parse::<GermanMonth>()?.into();
            let day = day.as_str().parse()?;

            issued_header = Some(Date::from_calendar_date(year, month, day)?);
        }

        if let Some(rest) = description.get(captures.get(0).unwrap().range().end + 1..) {
            let mut lines = rest.lines();

            if let Some(first) = lines.next() {
                if !first.is_empty()
                    && first != "Der Präsident"
                    && first != "Die Präsidentin"
                    && !first.eq_ignore_ascii_case("Presseinformation")
                    && !first.eq_ignore_ascii_case("Pressemitteilung")
                {
                    title_header = Some(first.to_owned());
                } else if let Some(second) = lines.next() {
                    if !second.is_empty() {
                        title_header = Some(second.to_owned());
                    }
                }
            }
        }
    }

    Ok((region, issued_header, title_header))
}

fn align_region(region: Option<Region>) -> SmallVec<[Region; 1]> {
    region
        .into_iter()
        .filter_map(|region| region.align_in(Some(&Region::ST)))
        .collect()
}

selectors! {
    results: "div.pagination.top",
    results_value: r"(\d+)\s+Treffer" as Regex,
    result_row: "div.result-document.row",
    result_link: "div.result-title > a[href]",
    result_link_title: "div.result-title > a > h3",
    result_date: "div.meta > div",
    unsupported_extensions: r"(?i)\.(?:(?:docx)|(?:xlsx)|(?:epub)|(?:mp4))$" as Regex,
    article: "div.row div.main_content",
    title: "h1,h2,h3[itemprop='headline']",
    title_alternative1: "div.page_header div.image_meta h5",
    title_alternative2: "ul#breadcrumb li[itemprop='itemListElement']",
    description: "div.ce > div.ce-textpic > div.ce-bodytext",
    description_alternative: "div.ce div.article div.news-text-wrap",
    resource_link: "div.row div.main_content div.ce-bodytext a[href][title],div.row div.col.col-title a[href][title]",
    last_modified: "head meta[name='last-modified'][content]",
    place_and_date: r"(?<place>[^,|\n]+)[,|]\s(?:den\s)?(?:(?<day>\d{1,2})\.(?<month>\d{1,2}).(?<year>\d{2,4})|(?<day1>\d{1,2}).\s(?<month1>[A-Za-z]+)\s(?<year1>\d{4}))" as Regex,
    geo_title: "title",
    geo_description: "div.singleview-content",
    presse_title: "h1",
    presse_description: "div.content p",
    presse_place_and_date: "div.header",
    presse_place_and_date_value: r"(?:.*\d+\s*)?(?<place>[^,]+),\s+den\s+(?<date>\d{2}\.\d{2}\.\d{4})" as Regex,
}
