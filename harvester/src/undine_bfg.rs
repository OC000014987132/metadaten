use anyhow::{anyhow, ensure, Result};
use cap_std::fs::Dir;
use compact_str::CompactString;
use geo::{Coord, Rect};
use miniproj::{get_projection, Projection};
use regex::Regex;
use scraper::{ElementRef, Html, Selector};
use smallvec::{smallvec, SmallVec};
use url::Url;

use harvester::{
    client::Client, fetch_many, selectors, utilities::collect_text, write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Dataset, License, Organisation, OrganisationRole, Region, Resource, ResourceType,
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();
    let proj = [
        get_projection(31466).unwrap(),
        get_projection(31467).unwrap(),
        get_projection(31468).unwrap(),
        get_projection(31469).unwrap(),
    ];

    let url = source.url.join("index.html")?;
    let text = client.fetch_text(source, "index".to_owned(), &url).await?;

    let mut rivers = Vec::new();

    {
        let document = Html::parse_document(&text);

        for element in document.select(&selectors.river) {
            let href = element.attr("href").unwrap();

            if href == "index.html" || href.starts_with("basisinfo") {
                continue;
            }

            rivers.push(href.to_owned());
        }
    }

    ensure!(!rivers.is_empty(), "Failed to extract any rivers");

    let (results, errors) = fetch_many(0, 0, rivers, |href| {
        fetch_river(dir, client, source, &selectors, proj, &url, href)
    })
    .await;

    Ok((results, results, errors))
}

#[tracing::instrument(skip(dir, client, source, selectors, proj, url))]
async fn fetch_river(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: [&dyn Projection; 4],
    url: &Url,
    href: String,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&href);
    let url = url.join(&href)?;

    let mut levels = Vec::new();
    let mut qualities = Vec::new();
    let mut floods = Vec::new();

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        for element in document.select(&selectors.level) {
            let href = element.attr("href").unwrap();
            levels.push(href.to_owned());
        }

        for element in document.select(&selectors.quality) {
            let href = element.attr("href").unwrap();
            qualities.push(href.to_owned());
        }

        for element in document.select(&selectors.flood) {
            let href = element.attr("href").unwrap();
            floods.push(href.to_owned());
        }
    }

    ensure!(!levels.is_empty(), "Failed to extract any levels");

    ensure!(
        !qualities.is_empty(),
        "Failed to extract any quality points of measurement"
    );

    ensure!(!floods.is_empty(), "Failed to extract any floods");

    let (results, errors) = fetch_many(0, 0, levels, |href| {
        fetch_level(dir, client, source, selectors, &proj, &url, href)
    })
    .await;

    let (results, errors) = fetch_many(results, errors, qualities, |href| {
        fetch_quality(dir, client, source, selectors, &proj, &url, href)
    })
    .await;

    let (results, errors) = fetch_many(results, errors, floods, |href| {
        fetch_flood(dir, client, source, selectors, &url, href)
    })
    .await;

    Ok((results, results, errors))
}

#[tracing::instrument(skip(dir, client, source, selectors, proj, url))]
async fn fetch_level(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: &[&dyn Projection; 4],
    url: &Url,
    href: String,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&href);
    let url = url.join(&href)?;

    let title;
    let description;
    let station_id;
    let pos;
    let mut organisations = SmallVec::new();

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        title = collect_text(
            document
                .select(&selectors.title)
                .next()
                .ok_or_else(|| anyhow!("Missing title"))?
                .text(),
        );

        description = document
            .select(&selectors.intro)
            .next()
            .map(|elem| collect_text(elem.text()));

        for element in document.select(&selectors.field) {
            let text = collect_text(element.text());

            if text.contains("Betreiber:") {
                for element in element.select(&selectors.link) {
                    let name = collect_text(element.text());
                    let href = element.attr("href").unwrap();

                    organisations.push(Organisation::Other {
                        name,
                        role: OrganisationRole::Operator,
                        websites: smallvec![href.to_owned()],
                    });
                }
            }
        }

        (station_id, pos) = extract_station_id_and_pos(selectors, proj, &document)?;
    }

    let region = extract_place_name(&selectors.place_name, &title);

    let types = smallvec![Type::Measurements {
        domain: Domain::Rivers,
        station: Some(Station {
            id: station_id,
            ..Default::default()
        }),
        measured_variables: smallvec!["Pegelstand".to_owned()],
        methods: Default::default(),
    }];

    let mut regions = region.into_iter().collect::<SmallVec<_>>();
    regions.extend(
        pos.and_then(|pos| WISE.match_shape(pos.x, pos.y))
            .map(Region::Watershed),
    );

    let dataset = Dataset {
        title,
        types,
        origins: source.origins.clone(),
        source_url: url.into(),
        description,
        organisations,
        license: License::AllRightsReserved,
        regions,
        bounding_boxes: pos.into_iter().map(|pos| Rect::new(pos, pos)).collect(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[tracing::instrument(skip(dir, client, source, selectors, proj, url))]
async fn fetch_quality(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: &[&dyn Projection; 4],
    url: &Url,
    href: String,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&href);
    let url = url.join(&href)?;

    let title;
    let station_id;
    let pos;
    let mut organisations = SmallVec::new();
    let mut resources = SmallVec::new();

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        title = document
            .select(&selectors.title)
            .next()
            .ok_or_else(|| anyhow!("Missing title"))?
            .text()
            .collect::<String>();

        for element in document.select(&selectors.field) {
            let text = collect_text(element.text());

            if text.contains("Betreiber:") {
                for element in element.select(&selectors.link) {
                    let name = collect_text(element.text());
                    let href = element.attr("href").unwrap();

                    organisations.push(Organisation::Other {
                        name,
                        role: OrganisationRole::Operator,
                        websites: smallvec![href.to_owned()],
                    });
                }
            } else if text.contains("Messdaten direkt beim Betreiber") {
                if let Some(element) = element.select(&selectors.link).next() {
                    let href = element.attr("href").unwrap();

                    resources.push(Resource {
                        r#type: ResourceType::WebPage,
                        url: href.to_owned(),
                        description: Some("Messdaten direkt beim Betreiber".to_owned()),
                        primary_content: true,
                        direct_link: false,
                    })
                }
            }
        }

        (station_id, pos) = extract_station_id_and_pos(selectors, proj, &document)?;
    }

    let region = extract_place_name(&selectors.place_name, &title);

    let types = smallvec![
        Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: station_id.clone(),
                ..Default::default()
            }),
            measured_variables: smallvec!["Wassergüte".to_owned()],
            methods: Default::default(),
        },
        Type::Measurements {
            domain: Domain::Rivers,
            station: Some(Station {
                id: station_id,
                ..Default::default()
            }),
            measured_variables: smallvec!["Wassergüte".to_owned()],
            methods: Default::default(),
        },
    ];

    // there are some quality gauging stations with wrong coordiates
    // namely, they have 8 digit numbers which is not allowed for Gauss Krueger coordinates
    // using these incorrect GK coordinates results in negative x coodinates when projecting them onto WGS 84
    // there is no known way to recover the correct coordinates so they are currently simply not used
    let bounding_boxes = pos
        .filter(|pos| pos.x > 0.0)
        .map(|pos| Rect::new(pos, pos))
        .into_iter()
        .collect();

    let dataset = Dataset {
        title,
        types,
        origins: source.origins.clone(),
        source_url: url.into(),
        organisations,
        resources,
        license: License::AllRightsReserved,
        regions: region.into_iter().collect(),
        bounding_boxes,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[tracing::instrument(skip(dir, client, source, selectors, url))]
async fn fetch_flood(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    url: &Url,
    href: String,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&href);
    let url = url.join(&href)?;

    let title;
    let mut description = None;

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        title = document
            .select(&selectors.title)
            .next()
            .ok_or_else(|| anyhow!("Missing title"))?
            .text()
            .collect::<String>();

        for elem in document.select(&selectors.intro) {
            let heading_elem = elem
                .prev_siblings()
                .filter_map(ElementRef::wrap)
                .find(|elem| elem.value().name() == "h3")
                .unwrap();

            let heading = collect_text(heading_elem.text());

            if heading.contains("Ablauf") {
                description = Some(collect_text(elem.text()));
            }
        }
    }

    let dataset = Dataset {
        title,
        description,
        origins: source.origins.clone(),
        source_url: url.into(),
        license: License::AllRightsReserved,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

fn make_key(href: &str) -> String {
    href.strip_suffix(".html").unwrap_or(href).replace('/', "-")
}

fn extract_place_name(place_name: &Regex, title: &str) -> Option<Region> {
    place_name
        .captures(title)
        .map(|captures| captures[1].into())
        .map(Region::Other)
}

fn extract_station_id_and_pos(
    selectors: &Selectors,
    proj: &[&dyn Projection; 4],
    document: &Html,
) -> Result<(Option<CompactString>, Option<Coord>)> {
    let mut station_id = None;
    let mut pos = None;

    for elem in document.select(&selectors.info_box) {
        let text = collect_text(elem.text());

        if let Some(captures) = selectors.station_id.captures(&text) {
            station_id = Some(captures[1].into());
        } else if let Some(captures) = selectors.pos.captures(&text) {
            let lon = captures[1].parse()?;
            let lat = captures[2].parse()?;

            // Gauss Krueger coordinates are supposed to have 7 digits but some have only 6
            // no fix is currently known, so these coordinates are discarded
            const BOUNDARY: f64 = 1_000_000.0;
            if lat < BOUNDARY || lon < BOUNDARY {
                continue;
            }

            // The zone of the employed Gauss Krueger projection is not specified.
            // We assume GK 3, however this is wrong for coordiates
            // in the West (which have GK 2) and the East (which have GK 4).
            let (x, y) = proj[1].projected_to_deg(lon, lat);

            let (x, y) = match x {
                // GK 2 should be used instead of GK 3
                -7.0..3.0 => proj[0].projected_to_deg(lon, lat),
                // GK 4 should be used instead of GK 3
                17.0..27.0 => proj[2].projected_to_deg(lon, lat),
                // GK 5 should be used instead of GK 3
                27.0..37.0 => proj[3].projected_to_deg(lon, lat),
                _ => (x, y),
            };

            pos = Some(Coord { x, y });
        }
    }

    Ok((station_id, pos))
}

selectors! {
    river: "nav.navgebiet > ul > li > a[href]",
    level: "nav.navfluss > ul > li > ul[id$=\"pegelnav\"] > li > a[href]",
    quality: "nav.navfluss > ul > li > ul[id$=\"messnav\"] > li > a[href]",
    flood: "nav.navfluss > ul > li > ul[id$=\"hwnwnav\"] > li > a[href]",
    title: "article > h2",
    intro: "section.intro_kasten > h3 ~ p",
    field: "section.intro > ul.square > li",
    link: "a[href]",
    place_name: r":\s*([^,]+)" as Regex,
    info_box: "ul.square > li",
    station_id: r"Messstellennummer:\s*([^\s]+)" as Regex,
    pos: r"Rechtswert\s+/\s+Hochwert:\s*(\d+)\s*/\s*(\d+)" as Regex,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_place_name_from_level() {
        assert_eq!(
            extract_place_name(
                &Selectors::default().place_name,
                "Pegel im Wesergebiet: Porta, Weser"
            ),
            Some(Region::Other("Porta".into()))
        );
    }

    #[test]
    fn extract_place_name_from_quality() {
        assert_eq!(
            extract_place_name(
                &Selectors::default().place_name,
                "Gütemessstellen im Elbegebiet: Dessau, Mulde"
            ),
            Some(Region::Other("Dessau".into()))
        );
    }
}
