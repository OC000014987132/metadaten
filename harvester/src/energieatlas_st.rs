use anyhow::Result;
use cap_std::fs::Dir;
use scraper::{Html, Selector};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_first_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::Type, Dataset, Language, License, Person, PersonRole, Resource, ResourceType,
};
use smallvec::SmallVec;

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let categories = {
        let text = client
            .fetch_text(source, "homepage".to_owned(), &source.url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.categories)
            .filter_map(|element| {
                let category = collect_text(element.text());

                if category.contains("Service") {
                    return None;
                }

                let href = element.attr("href").unwrap().to_owned();

                Some(href)
            })
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(0, 0, categories, |href| {
        fetch_category(dir, client, source, selectors, href)
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_category(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    href: String,
) -> Result<(usize, usize, usize)> {
    let panels = {
        let url = source.url.join(&href)?;

        let key = make_key(&href).into_owned();

        let text = client.fetch_text(source, key, &url).await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.panels)
            .filter_map(|element| {
                if element.attr("target") == Some("_blank") {
                    return None;
                }

                let title = collect_text(element.text());
                let href = element.attr("href").unwrap().to_owned();

                Some((title, href))
            })
            .collect::<Vec<_>>()
    };

    let (results, errors) = fetch_many(0, 0, panels, |(title, href)| {
        fetch_panel(dir, client, source, selectors, title, href)
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_panel(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    title: String,
    href: String,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&href)?;

    let key = make_key(&href).into_owned();

    let description;
    let resources;
    let persons;
    let mut types = SmallVec::new();

    {
        let text = client.fetch_text(source, key.clone(), &url).await?;

        let document = Html::parse_document(&text);

        description =
            select_first_text(&document, &selectors.panel_description, "description").ok();

        resources = document
            .select(&selectors.panel_resources)
            .map(|element| {
                let mut description = element.attr("title").map(|title| title.to_owned());

                if description.is_none() {
                    let text = collect_text(element.text());

                    if !text.is_empty() {
                        description = Some(text);
                    }
                }

                description = description.map(|text| {
                    text.split_once("(Foto:")
                        .map(|(prefix, _suffix)| prefix.to_owned())
                        .unwrap_or(text)
                });

                let href = element.attr("href").unwrap();

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description,
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<_>>()?;

        persons = document
            .select(&selectors.panel_contacts)
            .filter_map(|element| {
                element.select(&selectors.contact_name).next().map(|name| {
                    let name = collect_text(name.text());

                    let emails = element
                        .select(&selectors.contact_email)
                        .map(|email| collect_text(email.text()))
                        .collect();

                    Person {
                        name,
                        role: PersonRole::Contact,
                        emails,
                        ..Default::default()
                    }
                })
            })
            .collect();

        if description
            .as_deref()
            .is_some_and(|description| description.contains("Karte"))
        {
            types.push(Type::MapService);
        }
    }

    let dataset = Dataset {
        title,
        description,
        types,
        resources,
        persons,
        source_url: url.into(),
        license: License::OtherClosed,
        language: Language::German,
        origins: source.origins.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

selectors! {
    categories: "nav.gctopmenu-nav > ul > li > a[href^='/de/']",
    panels: "div#content div.card div.card-header a.card-link[href^='//www.']",
    panel_description: "div.cfgtext_pre.module-text.module-text-pre, div.pagecontent-content, div.card-body h1, div.gc_map-pretext, h1.pagecontent-title",
    panel_resources: "div#content div.card div.card-header a.card-link[href], div.card.border-primary a[href^='//www.'], a.list-group-item.list-group-item-action.internal-link[href], div.gc_map_copyright a[href^='https://'], div.pagecontent-content a[href]",
    panel_contacts: "section.gccontact-listview div.gccontact-list-row",
    contact_name: "div.fn",
    contact_email: "a.contact-block-link",
}
