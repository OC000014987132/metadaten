use anyhow::{anyhow, Context, Result};
use cap_std::fs::Dir;
use regex::{Regex, RegexSet};
use scraper::{ElementRef, Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, parse_text, strip_jsessionid},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Resource, ResourceType, TimeRange,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, results, errors) = fetch_search_page(dir, client, source, selectors, 1).await?;

    let pages = count.div_ceil(source.batch_size);
    tracing::info!("Fetching {count} datasets from {pages} pages");

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| {
        fetch_search_page(dir, client, source, selectors, page)
    })
    .await;

    let (mut results, mut errors) = fetch_many(
        results,
        errors,
        ["/DE/DAS/das_node.html", "/DE/Home/home_node.html"],
        |href| fetch_news_article(dir, client, source, selectors, href),
    )
    .await;

    match fetch_news_page(dir, client, source, selectors).await {
        Ok((count1, results1, errors1)) => {
            count += count1;
            results += results1;
            errors += errors1;
        }
        Err(err) => {
            tracing::error!("Error fetching news articles: {err:#}");

            count += 1;
            errors += 1;
        }
    }

    Ok((count, results, errors))
}

async fn fetch_search_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let count: usize;
    let items;

    {
        let mut url = source
            .url
            .join("/SiteGlobals/Forms/Suche/Expertensuche_Formular.html")?;

        url.query_pairs_mut()
            .append_pair("gtp", &format!("1933328_list%3D{page}"))
            .append_pair("resultsPerPage", &source.batch_size.to_string());

        let text = client
            .fetch_text(source, format!("serp-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        count = parse_text(
            &document,
            &selectors.results,
            &selectors.results_count,
            "Number of search results",
        )?;

        items = document
            .select(&selectors.items)
            .map(|item| {
                parse_contentbox(&item, source, selectors)
                    .with_context(|| format!("Failed to parse contentbox on page {page}"))
            })
            .collect::<Vec<Result<_>>>();
    }

    let (results, errors) = fetch_many(0, 0, items, |item| async move {
        let mut item = match item? {
            Some(item) => item,
            None => return Ok((0, 0, 0)),
        };

        if item.url.path() == "/DE/Aktuelles/aktuelles_node.html" {
            return Ok((0, 0, 0));
        }

        let resources = add_resources_and_description(&mut item, client, source, selectors).await?;

        let time_ranges = item.time_range.into_iter().collect();

        let language = determine_language_by_link(item.url.path());

        let dataset = Dataset {
            title: item.title,
            description: item.description,
            types: smallvec![Type::Text {
                text_type: TextType::Editorial,
            }],
            resources,
            time_ranges,
            license: License::AllRightsReserved,
            language,
            source_url: item.url.into(),
            origins: source.origins.clone(),
            ..Default::default()
        };

        write_dataset(dir, client, source, item.key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

struct Item {
    key: String,
    title: String,
    description: Option<String>,
    time_range: Option<TimeRange>,
    url: Url,
}

fn parse_contentbox(
    contentbox: &ElementRef,
    source: &Source,
    selectors: &Selectors,
) -> Result<Option<Item>> {
    let link = contentbox
        .select(&selectors.item_link)
        .next()
        .ok_or_else(|| anyhow!("Missing item link"))?;

    let title = collect_text(
        link.descendants()
            .filter_map(ElementRef::wrap)
            .filter(|element| {
                !(element.value().name() == "span"
                    && element
                        .value()
                        .classes()
                        .any(|class| matches!(class, "aural" | "category")))
            })
            .flat_map(|element| {
                element
                    .children()
                    .filter_map(|node| node.value().as_text())
                    .map(|text| text.as_ref())
            }),
    );

    if selectors.title_exclusion_list.is_match(&title) {
        return Ok(None);
    }

    let mut url = source.url.join(link.attr("href").unwrap())?;
    strip_jsessionid(&mut url);

    let description = contentbox
        .select(&selectors.item_description)
        .next()
        .map_or_else(
            || {
                tracing::debug!("Missing description for '{title}'");

                None
            },
            |elem| Some(collect_text(elem.text())),
        );

    let item_category = collect_text(
        contentbox
            .select(&selectors.item_category)
            .next()
            .ok_or_else(|| anyhow!("Missed category"))?
            .text(),
    );

    let time_range = selectors
        .item_date
        .captures(&item_category)
        .map(|captures| -> Result<TimeRange> {
            let time_range = if let Some(match_) = captures.name("date") {
                Date::parse(
                    match_.as_str().trim(),
                    format_description!("[day].[month].[year]"),
                )?
                .into()
            } else if let Some(match_) = captures.name("year") {
                TimeRange::whole_year(match_.as_str().parse()?)?
            } else {
                unreachable!("At least one of the capture groups must match")
            };

            Ok(time_range)
        })
        .transpose()?;

    Ok(Some(Item {
        key: make_key(&title).into_owned(),
        title,
        description,
        time_range,
        url,
    }))
}

async fn add_resources_and_description(
    item: &mut Item,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<SmallVec<[Resource; 4]>> {
    let text = client
        .fetch_text(source, format!("{}.isol", item.key), &item.url)
        .await?;

    let document = Html::parse_document(&text);

    if let Some(elem) = document.select(&selectors.article_description).next() {
        let text = collect_text(elem.text());

        if let Some(captures) = selectors.article_description_content.captures(&text) {
            item.description = Some(captures["description"].to_owned());
        }
    }

    let resources = document
        .select(&selectors.item_downloadlink)
        .map(|download_item| {
            let link = download_item.attr("href").unwrap();
            let mut url = source.url.join(link).unwrap();
            strip_jsessionid(&mut url);
            Resource {
                r#type: ResourceType::WebPage,
                url: url.into(),
                ..Default::default()
            }
            .guess_or_keep_type()
        })
        .collect();

    Ok(resources)
}

async fn fetch_news_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let hrefs: Vec<String>;

    {
        let url = source.url.join("/DE/Aktuelles/aktuelles_node.html")?;

        let text = client.fetch_text(source, "news".to_owned(), &url).await?;

        let document = Html::parse_document(&text);

        hrefs = document
            .select(&selectors.news_item)
            .map(|item| item.attr("href").unwrap().to_owned())
            .collect();
    }

    let count = hrefs.len();

    let (results, errors) = fetch_many(0, 0, &hrefs, |href| {
        fetch_news_article(dir, client, source, selectors, href)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_news_article(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    href: &str,
) -> Result<(usize, usize, usize)> {
    let url = &mut source.url.join(href)?;
    strip_jsessionid(url);
    let href = url.path();

    let id = match href.rsplit_once('/') {
        Some((_prefix, suffix)) => format!("news-{}", make_key(suffix)),
        None => return Err(anyhow!("Strange formed href '{href}'")),
    };

    let title;
    let time_range;
    let description;
    let resources;

    {
        let text = client.fetch_text(source, id.to_owned(), url).await?;

        let document = Html::parse_document(&text);

        let content = document
            .select(&selectors.news_article_content)
            .next()
            .ok_or_else(|| anyhow!("Missing content in news article {url}"))?;

        resources = content
            .select(&selectors.news_article_resource_link)
            .map(|elem| {
                let link = elem.attr("href").unwrap();

                let mut url = source.url.join(link).unwrap();
                strip_jsessionid(&mut url);

                let description = elem.attr("title").map(|text| {
                    selectors
                        .news_article_clean_link
                        .replace_all(text, "")
                        .into_owned()
                });

                Resource {
                    r#type: ResourceType::WebPage,
                    url: url.into(),
                    description,
                    ..Default::default()
                }
                .guess_or_keep_type()
            })
            .collect();

        let headline_text = collect_text(
            document
                .select(&selectors.news_article_headline)
                .next()
                .ok_or_else(|| anyhow!("Missing title in news article {url}"))?
                .text(),
        );

        (title, time_range) = if let Some(captures) = selectors
            .news_article_seperate_date
            .captures(&headline_text)
        {
            let title = captures["title"].trim().to_owned();
            let time_range: TimeRange = Date::parse(
                &captures["date"],
                format_description!("[day].[month].[year]"),
            )?
            .into();

            (title, Some(time_range))
        } else {
            (headline_text.to_owned(), None)
        };

        description = match document.select(&selectors.news_article_description).next() {
            Some(elem) => elem
                .attr("content")
                .ok_or_else(|| anyhow!("Missing 'content' in description for news article {url}"))?
                .trim()
                .to_owned(),
            None => collect_text(
                document
                    .select(&selectors.news_article_content)
                    .next()
                    .ok_or_else(|| anyhow!("Malformed page {url}"))?
                    .text(),
            ),
        };
    }

    let time_ranges = time_range.into_iter().collect();

    let language = determine_language_by_link(url.path());

    let dataset = Dataset {
        title,
        description: Some(description),
        types: smallvec![Type::Text {
            text_type: TextType::News,
        }],
        time_ranges,
        resources,
        license: License::AllRightsReserved,
        language,
        source_url: url.to_owned().into(),
        origins: source.origins.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, id, dataset).await
}

fn determine_language_by_link(href: &str) -> Language {
    if href.contains("/DE/") {
        return Language::German;
    }
    if href.contains("/EN/") {
        return Language::English;
    }
    tracing::debug!("Failed to determine language in '{href}'");
    Language::Unknown
}

selectors! {
    results: ".solrSortResults",
    results_count: r".*insgesamt\s*(\d+)\s*" as Regex,
    items: ".searchresult>li.column",
    item_link: "a[href]",
    item_description: "div div p",
    item_category: "span.category",
    item_date: r"Datum:\s*(?:(?<date>\d{2}\.\d{2}\.\d{4})|(?<year>\d{4}))" as Regex,
    item_downloadlink: ".downloadLink[href]",
    article_description: ".c-steckbrief__beschreibung",
    article_description_content: r"Beschreibung\s*(?<description>.*)" as Regex,
    news_item: "a.c-news__a[href]",
    news_article_content: ".l-content",
    news_article_resource_link: ".RichTextExtLink[href], .RichTextIntLink[href]",
    news_article_clean_link: r"(\(Öffnet neues Fenster\))" as Regex,
    news_article_description: "meta[name='description']",
    news_article_headline: ".c-module__headline",
    news_article_seperate_date: r"(?<date>\d{2}\.\d{2}\.\d{4})\s*(?<title>.*)" as Regex,
    title_exclusion_list:
        r"^Abschlussbericht$",
        r"^Anlage 1: Basic Manual$",
        r"^Anlage 2: Hinweise Arbeitssicherheit, Gefährdung$",
        r"^Anlage 3: FAQ Arbeitsrecht$",
        r"^Bestätigung$",
        r"^Bild \- Bildtitel$",
        r"^Datenschutzerklärung$",
        r"^Der Bildtitel$",
        r"^Download \- Imperium Romanum$",
        r"^Englische Kurzfassung$",
        r"^FAQ$",
        r"^Grundlagen$",
        r"^Grundlagenbericht$",
        r"^Hilfe$",
        r"^Hinweise und Erläuterungen$",
        r"^Ihr Gästebuch\-Eintrag wurde erstellt.$",
        r"^Impressum$",
        r"^Logo$",
        r"^Neues Passwort \- Bestätigung$",
        r"^Passwort vergessen \- Sendebestätigung$",
        r"^Projektübersicht$",
        r"^Registrierungs\-Bestätigung$",
        r"^Titel$",
        r"^Titelasasasasas$",
        r"^Titelunter$",
        r"^Vorgehenskonzept$",
        r"^We are looking forward to hearing from you!$",
        r"^Wir freuen uns auf Ihre Nachricht$"
     as RegexSet,
}
