use std::borrow::Cow;

use anyhow::{ensure, Result};
use cap_std::fs::Dir;
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};

use harvester::{
    client::Client, fetch_many, utilities::point_like_bounding_box, write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, Type},
        Alternative, Dataset, Language, License, Organisation, OrganisationRole, Region,
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let mut url = source.url.clone();
    url.query_pairs_mut().append_pair("page[limit]", "1000");

    let bytes = client
        .fetch_bytes(source, "stations".to_owned(), &url)
        .await?;

    let stations = from_slice::<Stations>(&bytes)?;

    ensure!(stations.count < 1000);

    let count = stations.count;

    let (results, errors) = fetch_many(0, 0, stations.data, |station| async move {
        let key = format!("messstation-{}", station.id);

        let title = format!(
            "Hochwasservorhersagezentrale Baden-Württemberg: Messstation {} ({})",
            station.attributes.name, station.attributes.water,
        );

        let description = format!(
            "Die Messstation {} befindet sich am Fluss {} und wird betrieben vom {}.",
            station.attributes.name,
            station.attributes.water,
            station.attributes.operator.clone(),
        );

        let mut types = SmallVec::new();
        if !station.attributes.measurement.water_level.not_measured {
            types.push(Type::Measurements {
                domain: Domain::Rivers,
                station: Some(Station {
                    id: Some(station.id.into()),
                    ..Default::default()
                }),
                measured_variables: smallvec!["Pegel".to_owned()],
                methods: Default::default(),
            })
        };
        if !station.attributes.measurement.water_drain.not_measured {
            types.push(Type::Measurements {
                domain: Domain::Rivers,
                station: Some(Station {
                    id: Some(station.id.into()),
                    ..Default::default()
                }),
                measured_variables: smallvec!["Abfluss".to_owned()],
                methods: Default::default(),
            })
        };

        ensure!(
            station.attributes.active,
            "Station is marked inactive but the schema does not model this status yet."
        );

        let organisations = smallvec![Organisation::Other {
            name: station.attributes.operator.into_owned(),
            role: OrganisationRole::Operator,
            websites: Default::default(),
        }];

        let bounding_boxes = smallvec![point_like_bounding_box(
            station.attributes.location.lat,
            station.attributes.location.lon
        )];

        let mut regions = smallvec![Region::BW];
        regions.extend(
            WISE.match_shape(
                station.attributes.location.lon,
                station.attributes.location.lat,
            )
            .map(Region::Watershed),
        );

        let alternatives = vec![Alternative::Source {
            source: "niz-bw".into(),
            title: format!(
                "Niedrigwasser-Informationszentrum Baden-Württemberg: Messstation {} ({})",
                station.attributes.name, station.attributes.water,
            ),
            url: format!(
                "https://niz.baden-wuerttemberg.de/oberflaechengewaesser/abfluss-wasserstand?id={}",
                station.id
            ),
        }];

        let modified = station
            .attributes
            .measurement
            .water_level
            .wakttime
            .or(station.attributes.measurement.water_drain.qakttime)
            .map(|date| {
                Date::parse(
                    date,
                    format_description!("[day].[month].[year] [hour]:[minute]"),
                )
            })
            .transpose()?;

        let dataset = Dataset {
            title,
            description: Some(description),
            types,
            organisations,
            bounding_boxes,
            regions,
            alternatives,
            modified,
            language: Language::German,
            license: License::OtherClosed,
            source_url: source.source_url().replace("{id}", station.id),
            origins: source.origins.clone(),
            machine_readable_source: true,
            ..Default::default()
        };

        write_dataset(dir, client, source, key, dataset).await
    })
    .await;

    Ok((count, results, errors))
}

#[derive(Deserialize)]
struct Stations<'a> {
    count: usize,
    #[serde(borrow)]
    data: Vec<PegelStation<'a>>,
}

#[derive(Deserialize)]
struct PegelStation<'a> {
    id: &'a str,
    attributes: Attributes<'a>,
}

#[derive(Deserialize)]
struct Attributes<'a> {
    #[serde(rename = "locationName", borrow)]
    name: Cow<'a, str>,
    active: bool,
    #[serde(borrow)]
    water: Cow<'a, str>,
    #[serde(borrow)]
    operator: Cow<'a, str>,
    location: Location,
    #[serde(borrow)]
    measurement: Measurement<'a>,
}

#[derive(Deserialize)]
struct Measurement<'a> {
    #[serde(rename = "waterLevel", borrow)]
    water_level: WaterLevel<'a>,
    #[serde(rename = "waterDrain", borrow)]
    water_drain: WaterDrain<'a>,
}

#[derive(Deserialize)]
struct WaterLevel<'a> {
    #[serde(rename = "notMeasured")]
    not_measured: bool,
    #[serde(rename = "waktTime")]
    wakttime: Option<&'a str>,
}

#[derive(Deserialize)]
struct WaterDrain<'a> {
    #[serde(rename = "notMeasured")]
    not_measured: bool,
    #[serde(rename = "qaktTime")]
    qakttime: Option<&'a str>,
}

#[derive(Deserialize)]
struct Location {
    lon: f64,
    lat: f64,
}
