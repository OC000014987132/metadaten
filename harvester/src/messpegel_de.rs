use anyhow::Result;
use cap_std::fs::Dir;
use compact_str::ToCompactString;
use regex::Regex;
use scraper::{Html, Selector};
use serde::Deserialize;
use serde_json::from_str;
use smallvec::{smallvec, SmallVec};
use time::{serde::rfc3339, Date, OffsetDateTime};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{make_key, parse_attribute, point_like_bounding_box, select_text, GermanMonth},
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, Station, TextType, Type},
        Dataset, Language, License, Region, Resource, ResourceType,
    },
    parse_str,
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let mut links = {
        let text = client
            .fetch_text(source, "start".to_owned(), &source.url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.mainlink_selector)
            .filter_map(|element| {
                let link = element.attr("href").unwrap();

                if !link.starts_with("/pegel")
                    && !link.starts_with("/support")
                    && !link.starts_with("/?view=article")
                {
                    return None;
                }

                Some(link.to_owned())
            })
            .collect::<Vec<_>>()
    };

    links.sort_unstable();
    links.dedup();

    let count = links.len();

    let (results, errors) = fetch_many(0, 0, links, |link| {
        fetch_dataset(dir, client, source, selectors, link)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: String,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&link).into_owned();

    let dataset = {
        let url = source.url.join(&link)?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let title = if link.contains("blog") {
            "Blog - Tagebuch".to_owned()
        } else {
            select_text(&document, &selectors.title)
        };

        let description = if title == "Erklärvideo" {
            "Video des DRK-Kreisverbands Vulkaneifel e.V. zur Hochwasservorsorge und dem informationsangebot messpegel.de.".to_owned()
        } else {
            select_text(&document, &selectors.description)
        };

        let mut resources = document
            .select(&selectors.video)
            .map(|element| {
                let path = element.attr("src").unwrap();

                Ok(Resource {
                    r#type: ResourceType::Video,
                    url: source.url.join(path)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        let mut issued = if let Some(captures) = selectors
            .date_select
            .captures(&select_text(&document, &selectors.date))
        {
            Some(Date::from_calendar_date(
                captures["year"].parse()?,
                captures["month"].parse::<GermanMonth>()?.into(),
                captures["day"].parse()?,
            )?)
        } else {
            None
        };

        let mut modified = None;

        let station = {
            if let Ok(id) = parse_attribute::<_, String>(
                &document,
                &selectors.id,
                &selectors.id_select,
                "src",
                "channel id",
            ) {
                let channel_url =
                    format!("https://thingspeak.mathworks.com/channels/{id}/feeds.json").parse()?;

                let text = client
                    .fetch_text(source, format!("station-info-{id}.isol"), &channel_url)
                    .await?;

                match from_str::<Channel>(&text) {
                    Ok(channel) => Some(channel),
                    Err(err) => {
                        tracing::error!("Failed to parse channel data for station {id}: {err:#}");

                        None
                    }
                }
            } else {
                None
            }
        };

        let mut regions = smallvec![Region::GeoName(3247914)];
        let mut bounding_boxes = SmallVec::new();

        if let Some(channel) = &station {
            let station = &channel.channel;

            issued = Some(station.created_at.date());
            modified = Some(station.updated_at.date());

            regions.extend(
                WISE.match_shape(station.longitude, station.latitude)
                    .map(Region::Watershed),
            );

            bounding_boxes.push(point_like_bounding_box(station.latitude, station.longitude));

            for (ext, r#type) in [
                (".xml", ResourceType::Xml),
                (".json", ResourceType::Json),
                (".csv", ResourceType::Csv),
                ("", ResourceType::WebPage),
            ] {
                resources.push(Resource {
                    r#type,
                    description: match r#type {
                        ResourceType::WebPage => {
                            Some("Dashboard tagesaktueller Daten der Messstelle".into())
                        }
                        _ => Some("Tagesaktuelle Daten der Messstelle".into()),
                    },
                    url: match r#type {
                        ResourceType::WebPage => {
                            format!("https://thingspeak.mathworks.com/channels/{}", station.id)
                        }
                        _ => format!(
                            "https://thingspeak.mathworks.com/channels/{}/feed{}",
                            station.id, ext
                        ),
                    },
                    primary_content: true,
                    ..Default::default()
                });
            }
        };

        let r#type = if url.path().contains("/pegel") {
            if title == "Blog - Tagebuch" {
                Type::Text {
                    text_type: TextType::BlogPost,
                }
            } else {
                Type::Measurements {
                    domain: Domain::Rivers,
                    station: Some(Station {
                        id: station.map(|channel| channel.channel.id.to_compact_string()),
                        measurement_frequency: Some("5 Minutes".into()),
                        ..Default::default()
                    }),
                    measured_variables: smallvec!["Pegelstand".to_owned()],
                    methods: Default::default(),
                }
            }
        } else if title == "Erklärvideo" {
            Type::Video
        } else {
            Type::Text {
                text_type: TextType::Editorial,
            }
        };

        Dataset {
            title,
            description: Some(description),
            types: smallvec![r#type],
            resources,
            issued,
            modified,
            regions,
            bounding_boxes,
            language: Language::German,
            license: License::AllRightsReserved,
            origins: source.origins.clone(),
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Deserialize)]
struct Channel {
    channel: StationInfo,
}

#[derive(Debug, Deserialize)]
struct StationInfo {
    id: usize,
    #[serde(deserialize_with = "parse_str")]
    latitude: f64,
    #[serde(deserialize_with = "parse_str")]
    longitude: f64,
    #[serde(with = "rfc3339")]
    created_at: OffsetDateTime,
    #[serde(with = "rfc3339")]
    updated_at: OffsetDateTime,
}

selectors! {
    mainlink_selector: ".sp-has-child a[href], .articleBody > .article-header > h2 > a[href]",
    title: "div.article-header h1, div.article-header h2",
    description: "div.article-details div p, div.article-body",
    video: "div.article-details div p video",
    date: "div.article-info time",
    date_select: r"(?<day>\d+)\.\s+(?<month>\w+)\s+(?<year>\d{4})" as Regex,
    id: "div[itemprop='articleBody'] iframe[src*='widgets']",
    id_select: r"channels/(?<id>\d+)/widgets/" as Regex,
}
