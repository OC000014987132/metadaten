use std::borrow::Cow;

use anyhow::{Context, Result};
use cap_std::fs::Dir;
use geo::{Coord, Rect};
use regex::Regex;
use serde::Deserialize;
use serde_roxmltree::{from_doc, roxmltree::Document};
use smallvec::{smallvec, SmallVec};
use time::{format_description::well_known::Iso8601, Date};

use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::dataset::{
    r#type::Type, Dataset, Language, License, Organisation, OrganisationRole, Region, Resource,
    ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let spatial_format = &Regex::new(
        r"(?x)
        (?<name>.*?) :\s+
        (?: (?<min_x>[\-\d\.]+) | null) ,\s+
        (?: (?<min_y>[\-\d\.]+) | null) ,\s+
        (?: (?<max_x>[\-\d\.]+) | null) ,\s+
        (?: (?<max_y>[\-\d\.]+) | null)",
    )
    .unwrap();

    let (count, results, errors) = fetch_page(dir, client, source, spatial_format, 1).await?;

    let pages = count.div_ceil(source.batch_size);
    tracing::info!("Fetching {count} datasets on {pages} pages");

    let (results, errors) = fetch_many(results, errors, 2..=pages, |page| async move {
        fetch_page(dir, client, source, spatial_format, page)
            .await
            .with_context(|| format!("Failed to fetch page {page}"))
    })
    .await;

    Ok((results, results, errors))
}

async fn fetch_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    spatial_format: &Regex,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let mut url = source.url.join("/interface-opensearch/query")?;

    url.query_pairs_mut()
        .append_pair("detail", "1")
        .append_pair("ingrid", "1")
        .append_pair("h", &source.batch_size.to_string())
        .append_pair("p", &page.to_string());

    if let Some(filter) = &source.filter {
        url.query_pairs_mut().append_pair("q", filter);
    }

    let text = client.fetch_text(source, page.to_string(), &url).await?;

    let document = Document::parse(&text)?;

    let page = from_doc::<Page>(&document)?;

    let count = page.channel.total_results;

    let (results, errors) = fetch_many(0, 0, page.channel.items, |item| {
        translate_item(dir, client, source, spatial_format, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_item(
    dir: &Dir,
    client: &Client,
    source: &Source,
    spatial_format: &Regex,
    item: Item<'_>,
) -> Result<(usize, usize, usize)> {
    let mut resources = SmallVec::new();
    let mut organisations = SmallVec::new();
    let mut description = item.description;
    let mut issued = None;
    let mut regions = SmallVec::new();
    let mut bounding_boxes = SmallVec::new();

    // these are usually datasets describing persons or organisations instead of projects
    if description
        .as_ref()
        .is_some_and(|description| description.is_empty())
        && item.details.is_none()
    {
        return Ok((0, 0, 0));
    }

    if let Some(name) = item.provider_name {
        organisations.push(Organisation::Other {
            role: OrganisationRole::Provider,
            websites: Default::default(),
            name,
        });
    }

    if let Some(details) = item.details {
        if let Some(html) = details.html {
            let metadata = html.body.metadata;

            issued = metadata
                .date
                .map(|text| Date::parse(text, &Iso8601::DEFAULT))
                .transpose()?;

            for docs in metadata.docs {
                for doc in docs.docs {
                    resources.push(
                        Resource {
                            r#type: ResourceType::WebPage,
                            description: Some(doc.label),
                            url: doc.link,
                            ..Default::default()
                        }
                        .guess_or_keep_type(),
                    );
                }
            }

            if let Some(steps) = metadata.steps {
                for step in steps.steps {
                    for docs in step.docs {
                        for doc in docs.docs {
                            resources.push(
                                Resource {
                                    r#type: ResourceType::WebPage,
                                    description: Some(doc.label),
                                    url: doc.link,
                                    ..Default::default()
                                }
                                .guess_or_keep_type(),
                            );
                        }
                    }
                }
            }

            if let Some(addresses) = metadata.addresses {
                for address in addresses.addresses {
                    let mut name = address.name;

                    for parent in address.parents {
                        name = format!("{name} ({})", parent.name);
                    }

                    organisations.push(Organisation::Other {
                        role: OrganisationRole::Unknown,
                        websites: address.url.into_iter().collect(),
                        name,
                    });
                }
            }

            if let Some(spatial_value) = metadata.spatial_value {
                if !spatial_value.is_empty() {
                    if let Some(captures) = spatial_format.captures(&spatial_value) {
                        let name = captures.name("name").unwrap().as_str();
                        let mut name = name.replace(", undefined", "").replace("undefined, ", "");

                        let parts = name.split(", ").collect::<Vec<_>>();
                        if !parts.is_empty() {
                            let first = *parts.first().unwrap();
                            let last = *parts.last().unwrap();

                            if (first == "Deutschland" || first == "Germany")
                                && last.chars().any(|char_| !char_.is_ascii_digit())
                            {
                                name = last.to_owned();
                            } else if (last.contains("Deutschland") || last.contains("Germany"))
                                && first.chars().any(|char_| !char_.is_ascii_digit())
                            {
                                name = first.to_owned();
                            }
                        }

                        regions.push(Region::Other(name.into()));

                        let min_x = captures.name("min_x");
                        let min_y = captures.name("min_y");
                        let max_x = captures.name("max_x");
                        let max_y = captures.name("max_y");

                        if let (Some(min_x), Some(min_y), Some(max_x), Some(max_y)) =
                            (min_x, min_y, max_x, max_y)
                        {
                            let min_x = min_x.as_str().parse::<f64>()?;
                            let min_y = min_y.as_str().parse::<f64>()?;
                            let max_x = max_x.as_str().parse::<f64>()?;
                            let max_y = max_y.as_str().parse::<f64>()?;

                            bounding_boxes.push(Rect::new(
                                Coord { x: min_x, y: min_y },
                                Coord { x: max_x, y: max_y },
                            ));
                        }
                    } else {
                        tracing::error!("Failed to parse spatial value: {spatial_value}");
                    }
                }
            }
        }

        for detail in details.details {
            if detail.key == "scraped_content" {
                description = Some(detail.value);
            } else if detail.key == "organisation" {
                organisations.push(Organisation::Other {
                    role: OrganisationRole::Unknown,
                    websites: Default::default(),
                    name: detail.value,
                });
            } else if detail.key.starts_with("blp_url") {
                resources.push(
                    Resource {
                        r#type: ResourceType::WebPage,
                        url: detail.value,
                        ..Default::default()
                    }
                    .guess_or_keep_type(),
                );
            }
        }
    }

    let dataset = Dataset {
        title: item.title,
        description,
        types: smallvec![Type::Uvp],
        source_url: item.link,
        issued,
        modified: item
            .last_modified
            .map(|text| Date::parse(text, &Iso8601::DEFAULT))
            .transpose()?,
        resources,
        organisations,
        regions,
        bounding_boxes,
        language: Language::German,
        license: License::AllRightsReserved,
        origins: source.origins.clone(),
        machine_readable_source: true,
        ..Default::default()
    };

    write_dataset(dir, client, source, item.docid, dataset).await
}

#[derive(Debug, Deserialize)]
struct Page<'a> {
    #[serde(borrow)]
    channel: Channel<'a>,
}

#[derive(Debug, Deserialize)]
struct Channel<'a> {
    #[serde(rename = "totalResults")]
    total_results: usize,
    #[serde(rename = "item", borrow)]
    items: Vec<Item<'a>>,
}

#[derive(Debug, Deserialize)]
struct Item<'a> {
    docid: String,
    title: String,
    link: String,
    description: Option<String>,
    #[serde(rename = "provider-name")]
    provider_name: Option<String>,
    #[serde(rename = "last-modified", borrow)]
    last_modified: Option<&'a str>,
    #[serde(borrow)]
    details: Option<Details<'a>>,
}

#[derive(Debug, Deserialize)]
struct Details<'a> {
    #[serde(borrow)]
    html: Option<Html<'a>>,
    #[serde(rename = "detail", default, borrow)]
    details: Vec<Detail<'a>>,
}

#[derive(Debug, Deserialize)]
struct Html<'a> {
    #[serde(borrow)]
    body: Body<'a>,
}

#[derive(Debug, Deserialize)]
struct Body<'a> {
    #[serde(rename = "idfMdMetadata", borrow)]
    metadata: Metadata<'a>,
}

#[derive(Debug, Deserialize)]
struct Metadata<'a> {
    #[serde(borrow)]
    date: Option<&'a str>,
    #[serde(default)]
    docs: SmallVec<[Docs; 1]>,
    steps: Option<Steps>,
    #[serde(rename = "spatialValue", borrow)]
    spatial_value: Option<Cow<'a, str>>,
    #[serde(borrow)]
    addresses: Option<Addresses<'a>>,
}

#[derive(Debug, Deserialize)]
struct Docs {
    #[serde(rename = "doc", default)]
    docs: SmallVec<[Doc; 1]>,
}

#[derive(Debug, Deserialize)]
struct Doc {
    link: String,
    label: String,
}

#[derive(Debug, Deserialize)]
struct Steps {
    #[serde(rename = "step", default)]
    steps: Vec<Step>,
}

#[derive(Debug, Deserialize)]
struct Step {
    #[serde(default)]
    docs: SmallVec<[Docs; 1]>,
}

#[derive(Debug, Deserialize)]
struct Addresses<'a> {
    #[serde(rename = "address", default, borrow)]
    addresses: SmallVec<[Address<'a>; 1]>,
}

#[derive(Debug, Deserialize)]
struct Address<'a> {
    name: String,
    url: Option<String>,
    #[serde(rename = "parent", default, borrow)]
    parents: Vec<ParentAddress<'a>>,
}

#[derive(Debug, Deserialize)]
struct ParentAddress<'a> {
    #[serde(borrow)]
    name: Cow<'a, str>,
}

#[derive(Debug, Deserialize)]
struct Detail<'a> {
    #[serde(rename = "detail-key")]
    key: &'a str,
    #[serde(rename = "detail-value")]
    value: String,
}
