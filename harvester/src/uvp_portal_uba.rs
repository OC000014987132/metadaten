use std::borrow::Cow;
use std::slice::from_ref;

use anyhow::{anyhow, ensure, Result};
use cap_std::fs::Dir;
use cow_utils::CowUtils;
use geo::{algorithm::BoundingRect, Coord, Geometry, GeometryCollection, Point, Rect};
use regex::Regex;
use scraper::{Html, Selector};
use serde_json::{from_str, Value};
use smallvec::{smallvec, SmallVec};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_text, make_key, parse_text, point_like_bounding_box, select_first_text,
        select_text, GermanDate,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Organisation, OrganisationRole, Resource, ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_vorhaben(dir, client, source, selectors).await?;

    let (count1, results1, errors1) = fetch_wissenswertes(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_vorhaben(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let (count, results, errors) = fetch_vorhaben_page(dir, client, source, selectors, 0).await?;

    let pages = count.div_ceil(50);

    let (results, errors) = fetch_many(results, errors, 1..pages, |page| {
        fetch_vorhaben_page(dir, client, source, selectors, page)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_vorhaben_page(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let count;

    let project_links = {
        let mut url = source.url.join("de/vorhaben")?;

        url.query_pairs_mut().append_pair("page", &page.to_string());

        let text = client
            .fetch_text(source, format!("vorhaben-{page}"), &url)
            .await?;

        let document = Html::parse_document(&text);

        count = parse_text(&document, &selectors.count, &selectors.count_value, "count")?;

        document
            .select(&selectors.project_link)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };

    ensure!(!project_links.is_empty(), "No project links extracted");

    let (results, errors) = fetch_many(0, 0, project_links, |project_link| {
        fetch_vorhaben_details(dir, client, source, selectors, project_link)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_vorhaben_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    project_link: String,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&project_link)?;

    let key = make_key(&project_link).into_owned();

    let dataset = {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let content = document
            .select(&selectors.content)
            .next()
            .ok_or_else(|| anyhow!("Missing content for project {project_link}"))?;

        let title = select_first_text(content, &selectors.title_vorhaben, "title")?;

        let description = select_text(content, &selectors.description_vorhaben);

        let description = match description.cow_replace('\u{2}', "") {
            Cow::Borrowed(_) => description,
            Cow::Owned(description) => description,
        };

        let mut resources = content
            .select(&selectors.documents_vorhaben)
            .enumerate()
            .map(|(index, element)| {
                let primary_content = index == 0;
                let href = element.attr("href").unwrap();
                let description = collect_text(element.text());

                Ok(Resource {
                    r#type: ResourceType::Pdf,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    primary_content,
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        for element in content.select(&selectors.links_vorhaben) {
            let href = element
                .select(&selectors.links_field_vorhaben)
                .next()
                .ok_or_else(|| anyhow!("Missing link anchor"))?
                .attr("href")
                .unwrap();

            let description =
                select_first_text(element, &selectors.links_label_vorhaben, "link description")?;

            resources.push(
                Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            )
        }

        let mut organisations = SmallVec::new();

        for element in content.select(&selectors.organisation_institution) {
            let name = select_first_text(
                element,
                &selectors.organisation_institution_name,
                "institution name",
            )?;

            let websites = element
                .select(&selectors.organisation_institution_website)
                .map(|anchor| {
                    let href = anchor.attr("href").unwrap();
                    Ok(source.url.join(href)?.into())
                })
                .collect::<Result<_>>()?;

            organisations.push(Organisation::Other {
                name,
                role: OrganisationRole::Owner,
                websites,
            });
        }

        for element in content.select(&selectors.organisation_company) {
            let name = select_first_text(
                element,
                &selectors.organisation_company_name,
                "institution name",
            )?;

            let websites = element
                .select(&selectors.organisation_company_website)
                .map(|anchor| {
                    let href = anchor.attr("href").unwrap();
                    Ok(source.url.join(href)?.into())
                })
                .collect::<Result<_>>()?;

            organisations.push(Organisation::Other {
                name,
                role: OrganisationRole::Operator,
                websites,
            });
        }

        let dates = content
            .select(&selectors.date_vorhaben)
            .map(|element| collect_text(element.text()))
            .map(|text| text.parse::<GermanDate>())
            .collect::<Result<Vec<_>>>()?;

        let time_ranges = if let (Some(min), Some(max)) = (dates.iter().min(), dates.iter().max()) {
            smallvec![(*min, *max).into()]
        } else {
            Default::default()
        };

        let tags = content
            .select(&selectors.category_vorhaben)
            .map(|element| collect_text(element.text()))
            .map(|text| Tag::Other(text.into()))
            .collect();

        let drupal_settings = document
            .select(&selectors.drupal_settings)
            .next()
            .ok_or_else(|| anyhow!("Missing Drupal settings"))?;

        let drupal_settings = from_str::<Value>(&collect_text(drupal_settings.text()))?;

        let bounding_boxes = extract_bounding_boxes(&drupal_settings)?;

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Uvp],
            bounding_boxes,
            source_url: url.into(),
            language: Language::German,
            origins: source.origins.clone(),
            resources,
            organisations,
            time_ranges,
            tags,
            license: License::OtherClosed, // the Vorhaben contain content from external providers which can be copyright-protected (see https://www.uvp-portal.de/de/impressum)
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_wissenswertes(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    // fetch main page
    let links;

    let (mut count, mut results, mut errors) = {
        let url = source.url.join("de/wissenswertes")?;

        let text = client
            .fetch_text(source, "wissenswertes".to_owned(), &url)
            .await?;

        let document = Html::parse_document(&text);

        let title = select_text(&document, &selectors.title_wissenswertes_main);
        let description = select_text(&document, &selectors.description_wissenswertes_main);

        let resources = document
            .select(&selectors.resources_wissenswertes)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = element
                    .text()
                    .next()
                    .ok_or_else(|| anyhow!("Missing description"))?;

                Ok(Resource {
                    description: Some(description.to_owned()),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                })
            })
            .collect::<Result<SmallVec<_>>>()?;

        links = document
            .select(&selectors.weiterlesen)
            .map(|elem| elem.attr("href").unwrap().to_owned())
            .collect::<Vec<String>>();

        let dataset = Dataset {
            title,
            description: Some(description),
            source_url: url.clone().into(),
            language: Language::German,
            origins: source.origins.clone(),
            resources,
            types: smallvec![Type::Text {
                text_type: TextType::Editorial,
            }],
            license: License::CcByNcNd40, // https://www.uvp-portal.de/de/impressum states that content provided by the UBA are licensed according to Creative Commons Namensnennung – Nicht kommerziell – keine Bearbeitungen 4.0 International Lizenz
            ..Default::default()
        };

        write_dataset(dir, client, source, "wissenswertes".to_owned(), dataset).await?
    };

    count += links.len();

    (results, errors) = fetch_many(results, errors, links, |link| {
        fetch_wissenswertes_details(dir, client, source, selectors, link)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_wissenswertes_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: String,
) -> Result<(usize, usize, usize)> {
    let url = source.url.join(&link)?;

    let key = format!("wissenwertes-{}", make_key(&link));

    let dataset = {
        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let content = document
            .select(&selectors.content)
            .next()
            .ok_or_else(|| anyhow!("Missing content for project {link}"))?;

        let title = select_first_text(content, &selectors.title_wissenswertes_sub, "title")?;

        let description = select_text(&document, &selectors.description_wissenswertes_sub);

        let mut resources = document
            .select(&selectors.resources_wissenswertes)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = element
                    .text()
                    .next()
                    .ok_or_else(|| anyhow!("Missing description"))?;

                Ok(Resource {
                    description: Some(description.to_owned()),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                })
            })
            .collect::<Result<SmallVec<_>>>()?;

        for element in document.select(&selectors.video) {
            let src = element.attr("src").unwrap();
            let description = "Erklärvideo zur Umweltprüfung".to_string();

            resources.push(
                Resource {
                    r#type: ResourceType::Video,
                    description: Some(description),
                    url: source.url.join(src)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type(),
            );
        }

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial,
            }],
            source_url: url.into(),
            language: Language::German,
            origins: source.origins.clone(),
            resources,
            license: License::CcByNcNd40, // https://www.uvp-portal.de/de/impressum states that content provided by the UBA are licensed according to Creative Commons Namensnennung – Nicht kommerziell – keine Bearbeitungen 4.0 International Lizenz
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

fn extract_bounding_boxes(drupal_settings: &Value) -> Result<SmallVec<[Rect; 1]>> {
    let mut bounding_boxes = SmallVec::new();

    if let Some(leaflet) = drupal_settings
        .pointer("/leaflet")
        .and_then(|value| value.as_object())
    {
        for geojson in leaflet.values() {
            if let Some(features) = geojson
                .pointer("/features")
                .and_then(|value| value.as_array())
            {
                for feature in features {
                    let components = if let Some(components) = feature
                        .pointer("/component")
                        .and_then(|value| value.as_array())
                    {
                        components.as_slice()
                    } else {
                        from_ref(feature)
                    };

                    for component in components {
                        if let Some(points) = component
                            .pointer("/points")
                            .and_then(|value| value.as_array())
                        {
                            let points = points
                                .iter()
                                .flat_map(|point| match point.as_array() {
                                    Some(point) => point.as_slice(),
                                    None => from_ref(point),
                                })
                                .flat_map(|point| match point.as_array() {
                                    Some(point) => point.as_slice(),
                                    None => from_ref(point),
                                });

                            let mut geometry = Vec::new();

                            for point in points {
                                let lat = point
                                    .pointer("/lat")
                                    .and_then(|value| value.as_f64())
                                    .ok_or_else(|| anyhow!("Missing point lat"))?;

                                let lon = point
                                    .pointer("/lon")
                                    .and_then(|value| value.as_f64())
                                    .ok_or_else(|| anyhow!("Missing point lon"))?;

                                geometry.push(Geometry::Point(Point(Coord { x: lon, y: lat })));
                            }

                            bounding_boxes
                                .extend(GeometryCollection::new_from(geometry).bounding_rect());
                        } else {
                            let lat = component
                                .pointer("/lat")
                                .and_then(|value| value.as_f64())
                                .ok_or_else(|| anyhow!("Missing component lat"))?;

                            let lon = component
                                .pointer("/lon")
                                .and_then(|value| value.as_f64())
                                .ok_or_else(|| anyhow!("Missing component lon"))?;

                            bounding_boxes.push(point_like_bounding_box(lat, lon));
                        }
                    }
                }
            }
        }
    }

    Ok(bounding_boxes)
}

selectors! {
    count: "div.project-search footer",
    count_value: r"Ergebnis\s+\d+\s+\-\s+\d+\s+von\s+(\d+)" as Regex,
    project_link: "div.views-row a[href]",
    content: "div.page-content",
    title_vorhaben: "span.field-title",
    description_vorhaben: "div.clearfix, .content-container",
    documents_vorhaben: "div.field-field_nuvp_document a[href]",
    links_vorhaben: "div.field-field_uvpp_docs_global_link",
    links_label_vorhaben: "div.content-label",
    links_field_vorhaben: "div.content-field a[href]",
    organisation_institution: "article.node-uvp_institution",
    organisation_institution_name: "div.content-field > span.field-title",
    organisation_institution_website: "div.field-field_uvpi_homepage a[href]",
    organisation_company: "article.node-company",
    organisation_company_name: "div.content-field > span.field-title",
    organisation_company_website: "div.field-field_cpy_homepage a[href]",
    date_vorhaben: "div.field-field_uvpp_approval_doc_date div.content-field, div.field-field_uvpp_decision_date div.content-field",
    category_vorhaben: "div.field-field_uvpp_annex_number div.content-field",
    drupal_settings: "script[data-drupal-selector='drupal-settings-json']",
    weiterlesen: ".node-readmore a[href]",
    title_wissenswertes_main: ".align-c h1",
    title_wissenswertes_sub: ".field-title",
    description_wissenswertes_main: "#block-views-block-faqs-block-1",
    description_wissenswertes_sub: "p",
    resources_wissenswertes: ".content-field a[href]",
    video: "#video-upload-9 > source[src], #video-upload-10 > source[src], #video-upload-11 > source[src]",
}
