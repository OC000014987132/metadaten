use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use time::{macros::format_description, Date};

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{collect_text, make_key, select_text},
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{TextType, Type},
    Dataset, Language, License, Person, PersonRole, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) = fetch_links(dir, client, source, selectors).await?;

    let (max_page, results1, errors1) =
        fetch_publications(dir, client, source, selectors, 1).await?;

    let (results1, errors1) = fetch_many(results1, errors1, 2..=max_page, |page| {
        fetch_publications(dir, client, source, selectors, page)
    })
    .await;

    count += results1;
    results += results1;
    errors += errors1;

    let (max_page, results1, errors1) = fetch_news(dir, client, source, selectors, 1).await?;

    let (results1, errors1) = fetch_many(results1, errors1, 2..=max_page, |page| {
        fetch_news(dir, client, source, selectors, page)
    })
    .await;

    count += results1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_links(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let mut links = {
        let text = client
            .fetch_text(source, "topic".to_owned(), &source.url)
            .await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.navbar_links)
            .map(|element| element.attr("href").unwrap().to_owned())
            .collect::<Vec<_>>()
    };

    links.retain(|link| {
        link.contains("themen")
            || link.contains("werkzeuge")
            || link.contains("service/normen-und-richtlinien")
            || link.contains("leitfaden")
    });

    let mut sublinks = Vec::new();
    for link in &links {
        let url = source.url.join(link)?;
        let key = make_key(link).into_owned();
        let text = client.fetch_text(source, key, &url).await?;
        let document = Html::parse_document(&text);

        sublinks.extend(
            document
                .select(&selectors.sub_links)
                .map(|element| element.attr("href").unwrap())
                .filter(|href| href.starts_with('/'))
                .map(|href| href.to_owned()),
        );
    }
    links.append(&mut sublinks);

    links.sort_unstable();
    links.dedup();

    let count = links.len();

    let (results, errors) = fetch_many(0, 0, links, |item| {
        fetch_links_details(dir, client, source, selectors, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_links_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: String,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&link).into_owned();

    let dataset = {
        let url = source.url.join(&link)?;
        let text = client.fetch_text(source, key.clone(), &url).await?;
        let document = Html::parse_document(&text);

        let title = select_text(&document, &selectors.link_title);

        let description = select_text(&document, &selectors.link_description);

        let resources = document
            .select(&selectors.links_resource)
            .filter_map(|element| {
                let description = collect_text(element.text());
                if description.is_empty() {
                    return None;
                }

                let href = element.attr("href").unwrap();

                Some((description, href))
            })
            .chain(
                document
                    .select(&selectors.links_resource_content)
                    .filter_map(|element| {
                        let description = select_text(element, &selectors.links_resource_title);

                        let href = element
                            .select(&selectors.links_resource_href)
                            .next()?
                            .attr("href")
                            .unwrap();

                        Some((description, href))
                    }),
            )
            .map(|(description, href)| {
                let mut resource = Resource {
                    r#type: ResourceType::WebPage,
                    description: Some(description),
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type();

                resource.primary_content =
                    matches!(resource.r#type, ResourceType::Pdf | ResourceType::Zip);

                Ok(resource)
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Editorial
            }],
            resources,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::AllRightsReserved,
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_publications(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let max_page;
    let items;

    {
        let url = &source
            .url
            .join(&format!("/service/publikationen/seite/{page}/"))?;

        let text = client
            .fetch_text(source, format!("publikationen-{page}"), url)
            .await?;

        let document = Html::parse_document(&text);

        max_page = document
            .select(&selectors.max_pages)
            .next_back()
            .map(|elem| collect_text(elem.text()).parse())
            .unwrap_or(Ok(0))?;

        items = document
            .select(&selectors.publications_items)
            .map(|element| {
                let link = element
                    .select(&selectors.publications_href)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link"))?;

                let title = collect_text(link.text());
                let link = link.attr("href").unwrap().to_owned();

                let date = element
                    .select(&selectors.publications_date)
                    .next()
                    .map(|element| {
                        let text = collect_text(element.text());
                        Date::parse(&text, format_description!("[day].[month].[year]"))
                    })
                    .transpose()?;

                Ok(Item { link, title, date })
            })
            .collect::<Result<Vec<_>>>()?;
    }

    let (results, errors) = fetch_many(0, 0, items, |item| {
        fetch_publications_details(dir, client, source, selectors, item)
    })
    .await;

    Ok((max_page, results, errors))
}

async fn fetch_publications_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&link.link).into_owned();

    let dataset = {
        let url = source.url.join(&link.link)?;
        let text = client.fetch_text(source, key.clone(), &url).await?;
        let document = Html::parse_document(&text);

        let description = select_text(&document, &selectors.publications_description_detail);

        let resources = document
            .select(&selectors.publications_resource)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = Some(collect_text(element.text()));

                let mut resource = Resource {
                    r#type: ResourceType::WebPage,
                    description,
                    url: source.url.join(href)?.into(),
                    ..Default::default()
                }
                .guess_or_keep_type();

                resource.primary_content =
                    matches!(resource.r#type, ResourceType::Pdf | ResourceType::Zip);

                Ok(resource)
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title: link.title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::Publication
            }],
            resources,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::OtherClosed,
            issued: link.date,
            source_url: url.into(),
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn fetch_news(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    page: usize,
) -> Result<(usize, usize, usize)> {
    let max_page;
    let items;

    {
        let url = &source.url.join(&format!("/aktuelles/seite/{page}/"))?;

        let text = client
            .fetch_text(source, format!("aktuelles-{page}"), url)
            .await?;

        let document = Html::parse_document(&text);

        max_page = document
            .select(&selectors.max_pages)
            .next_back()
            .map(|elem| collect_text(elem.text()).parse())
            .unwrap_or(Ok(0))?;

        items = document
            .select(&selectors.news_items)
            .map(|element| {
                let link = element
                    .select(&selectors.news_href)
                    .next()
                    .ok_or_else(|| anyhow!("Missing link"))?;

                let title = collect_text(link.text());
                let link = link.attr("href").unwrap().to_owned();

                let date = element
                    .select(&selectors.publications_date)
                    .next()
                    .map(|element| {
                        let text = collect_text(element.text());
                        Date::parse(&text, format_description!("[day].[month].[year]"))
                    })
                    .transpose()?;

                Ok(Item { link, title, date })
            })
            .collect::<Result<Vec<_>>>()?;
    }

    let (results, errors) = fetch_many(0, 0, items, |item| {
        fetch_news_details(dir, client, source, selectors, item)
    })
    .await;

    Ok((max_page, results, errors))
}

async fn fetch_news_details(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    link: Item,
) -> Result<(usize, usize, usize)> {
    let key = make_key(&link.link).into_owned();

    let dataset = {
        let url = source.url.join(&link.link)?;
        let text = client.fetch_text(source, key.clone(), &url).await?;
        let document = Html::parse_document(&text);

        let description = select_text(&document, &selectors.news_description_detail);

        let emails = select_text(&document, &selectors.news_email);

        let persons = vec![Person {
            name: select_text(&document, &selectors.news_person),
            role: PersonRole::Contact,
            emails: smallvec![emails],
            ..Default::default()
        }];

        let resources = document
            .select(&selectors.news_resource)
            .map(|element| {
                let href = element.attr("href").unwrap();
                let description = Some(collect_text(element.text()));
                let low_href = &href.to_lowercase();
                let primary_content = low_href.contains("pdf") || low_href.contains("zip");

                Ok(Resource {
                    r#type: ResourceType::WebPage,
                    description,
                    url: source.url.join(href)?.into(),
                    primary_content,
                    ..Default::default()
                }
                .guess_or_keep_type())
            })
            .collect::<Result<SmallVec<_>>>()?;

        Dataset {
            title: link.title,
            description: Some(description),
            types: smallvec![Type::Text {
                text_type: TextType::News
            }],
            resources,
            language: Language::German,
            origins: source.origins.clone(),
            license: License::OtherClosed,
            issued: link.date,
            source_url: url.into(),
            persons,
            ..Default::default()
        }
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug)]
struct Item {
    title: String,
    link: String,
    date: Option<Date>,
}

selectors! {
    navbar_links: "a[class='m-mainnav__link--level1'], a[class='m-mainnav__link--level2'], a[class='m-mainnav__link--level3']",
    sub_links: "div[class='m-teaser__text-container m-teaser__text-container--two_columns_image_text'] a[href], div.m-innovations__list a[href]",
    max_pages: "div.m-newspagination__pages a[href]",
    link_title: "h1[class='m-headline'], h1.m-headline.m-headline--vdihandlungsempfehlung, h1[class='m-news__title'], h1.m-mtdd__title, h1[class='m-publication__title'], h1.m-innovations-detail__header",
    link_description: "div[class='l-page__abstract'], div.m-text__inner > p, div.m-page-tabs__introtext, div.m-publication__text, p.m-mtdd__teaser, div.l-page__abstract.l-page__abstract--vdihandlungsempfehlung, div.m-news__text > p",
    links_resource: "a.m-footnotes-sources__link,
                     a.m-teaser__link,
                     li.m-linklist__item a[href],
                     div.m-video-slider__slider-wrapper a[href],
                     div.m-teaser-topics__content a[href],
                     div.l-grid--6040__left h3.m-publicationlistitem__title a[href],
                     div.l-grid--6040__right h3.m-shortanalyselistitem__title a[href],
                     div.m-teaser-further-contents__link-container a[href]",
    links_resource_content: "li.m-tools__tool--desktop",
    links_resource_href: "li.m-tools__tool--desktop a[href]",
    links_resource_title: "div.m-tools__tool-name--desktop.accordion-tab",
    news_items: "div.m-newslist__listitem",
    news_href: "h3.m-newslistitem__title a[href]",
    news_date: "div.m-newslistitem__date",
    news_description: "div.m-newslistitem p",
    news_description_detail: "div.m-news__text",
    news_resource: "div.m-news__text a[href], a.m-footnotes-sources__link a[href]",
    news_person: "div.m-contact__info p.m-contact__info-position",
    news_email: "div.m-contact__info p.m-contact__info-email",
    publications_items: "div.m-publicationlist__listitem",
    publications_href: "h3.m-publicationlistitem__title a[href]",
    publications_date: "div.m-publicationlistitem__date",
    publications_description: "div[class='m-publicationlistitem__content'] p",
    publications_description_detail: "div.m-publication__text",
    publications_resource: "div.m-publication__text a[href], div.m-publication__download a[href]",
}
