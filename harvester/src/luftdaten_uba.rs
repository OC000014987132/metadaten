use std::fmt::Write;

use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use geo::{Coord, Rect};
use hashbrown::{HashMap, HashSet};
use serde::Deserialize;
use serde_json::{from_str, Map, Value};
use smallvec::smallvec;
use time::{format_description::FormatItem, macros::format_description, Date};

use harvester::{client::Client, fetch_many, utilities::yesterday, write_dataset, Source};
use metadaten::dataset::{
    r#type::{Domain, Station, Type},
    Dataset, Language, License, Region, Resource, ResourceType,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let yesterday = yesterday().format(DATE_FORMAT)?;

    let scopes_str = fetch_scopes_str(client, source).await?;
    let components_map = fetch_components_map(client, source, &yesterday).await?;

    #[derive(Deserialize)]
    struct Stations {
        count: usize,
        data: Map<String, Value>,
    }

    let stations = {
        let url = source.url.join("/api/air_data/v3/stations/json?lang=de")?;

        let text = client
            .fetch_text(source, String::from("stations-de"), &url)
            .await?;

        from_str::<Stations>(&text)?
    };

    let count = stations.count;
    let (results, errors) = fetch_many(0, 0, stations.data, |(_, data)| {
        create_station_dataset(
            dir,
            client,
            source,
            &yesterday,
            &scopes_str,
            &components_map,
            data,
        )
    })
    .await;

    Ok((count, results, errors))
}

async fn create_station_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    yesterday: &str,
    scopes_str: &str,
    components_map: &HashMap<String, HashSet<usize>>,
    data: Value,
) -> Result<(usize, usize, usize)> {
    let id = data[0]
        .as_str()
        .ok_or_else(|| anyhow!("Missing ID"))?
        .parse::<usize>()?;

    let code = data[1]
        .as_str()
        .ok_or_else(|| anyhow!("Missing code for ID {id}"))?;

    let name = data[2]
        .as_str()
        .ok_or_else(|| anyhow!("Missing name for ID {id}"))?;

    // cleanup of some ideosyncratic suffixes used in the database for some names
    let name = name.strip_prefix("zDDR_").unwrap_or(name);
    let name = name.strip_prefix("zDep_").unwrap_or(name);
    let name = name.strip_prefix("Dep_").unwrap_or(name);

    let city = data[3]
        .as_str()
        .ok_or_else(|| anyhow!("Missing city for ID {id}"))?;

    let active_from = data[5]
        .as_str()
        .ok_or_else(|| anyhow!("Missing active-from for ID {id}"))?;
    let active_to = data[6].as_str();

    let longitude = data[7]
        .as_str()
        .ok_or_else(|| anyhow!("Missing longitude for ID {id}"))?
        .parse::<f64>()?;
    let latitude = data[8]
        .as_str()
        .ok_or_else(|| anyhow!("Missing latitude for ID {id}"))?
        .parse::<f64>()?;

    let components_str = create_components_str(components_map, id);

    let mut title = format!("Luftdaten der Station {name} ({code})");
    if !city.is_empty() {
        write!(&mut title, " in {city}").unwrap();
    }

    let description = Some(format!(
        r"Dieser Datensatz enthält Information zu gas- und partikelförmigen Schadstoffen.

{components_str}

{scopes_str}

Diese werden mehrmals täglich von Fachleuten an Messstationen der Bundesländer und des Umweltbundesamtes ermittelt.
Schon kurz nach der Messung können Sie sich hier mit Hilfe von deutschlandweiten Karten und Verlaufsgrafiken
über aktuelle Messwerte und Vorhersagen informieren und Stationswerte der letzten Jahre einsehen.
Neben der Information über die aktuelle Luftqualität umfasst das Luftdatenportal auch zeitliche Verläufe der
Schadstoffkonzentrationen, tabellarische Auflistungen der Belastungssituation an den deutschen Messstationen,
einen Index zur Luftqualität sowie Jahresbilanzen für die einzelnen Schadstoffe."
    ));

    let source_url =
        format!("https://www.env-it.de/stationen/public/station.do?selectedStationcode={code}");

    let region = if !city.is_empty() {
        Some(Region::Other(city.into()))
    } else {
        None
    };

    let position = Coord {
        x: longitude,
        y: latitude,
    };
    let bounding_boxes = smallvec![Rect::new(position, position)];

    let active_from = Date::parse(active_from, DATE_FORMAT)?;
    let active_to = Date::parse(active_to.unwrap_or(yesterday), DATE_FORMAT)?;
    let time_ranges = smallvec![(active_from, active_to).into()];

    let resources = smallvec![
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("Interaktive Grafiken, Karten und Auswertung der Luftdaten am UBA".to_owned()),
            url: source.url.clone().into(),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            description: Some("Dokumentation der API für die Luftdaten am UBA".to_owned()),
            url: source.url.join("/daten/luft/luftdaten/doc")?.into(),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::Json,
            description: Some("Aktuelle Messwerte der Station".to_owned()),
            url: source.url.join(&format!("/api/air_data/v3/measures/json?lang=de&date_from={yesterday}&time_from=1&date_to={yesterday}&time_to=24&station={id}"))?.into(),
            primary_content: true,
            ..Default::default()
        },
    ];

    let types = smallvec![
        Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(id.to_string().into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Schadstoffe".to_owned()],
            methods: Default::default(),
        },
        Type::Measurements {
            domain: Domain::Air,
            station: Some(Station {
                id: Some(id.to_string().into()),
                ..Default::default()
            }),
            measured_variables: smallvec!["Schadstoffe".to_owned()],
            methods: Default::default(),
        },
    ];

    let dataset = Dataset {
        title,
        description,
        types,
        language: Language::German,
        license: License::DlDeBy20,
        source_url,
        origins: source.origins.clone(),
        resources,
        regions: region.into_iter().collect(),
        bounding_boxes,
        time_ranges,
        ..Default::default()
    };

    write_dataset(dir, client, source, format!("station-{id}"), dataset).await
}

async fn fetch_scopes_str(client: &Client, source: &Source) -> Result<String> {
    let url = source.url.join("/api/air_data/v3/scopes/json?lang=de")?;

    let text = client
        .fetch_text(source, "scopes-de".to_owned(), &url)
        .await?;

    let scopes = from_str::<Map<String, Value>>(&text)?;

    let names = scopes
        .iter()
        .filter(|(key, _)| key.parse::<usize>().is_ok())
        .map(|(index, scope)| {
            let name = scope[5]
                .as_str()
                .ok_or_else(|| anyhow!("Missing scope name at index {index}"))?;

            Ok(name)
        })
        .collect::<Result<Vec<_>>>()?;

    Ok(format!(
        "Verfügbare Auswertungen der Schadstoffe sind: {}.",
        names.join(", ")
    ))
}

async fn fetch_components_map(
    client: &Client,
    source: &Source,
    yesterday: &str,
) -> Result<HashMap<String, HashSet<usize>>> {
    let url = source
        .url
        .join("/api/air_data/v3/components/json?lang=de")?;

    let text = client
        .fetch_text(source, "components-de".to_owned(), &url)
        .await?;

    let components = from_str::<Map<String, Value>>(&text)?;

    let names = components
        .iter()
        .filter(|(key, _)| key.parse::<usize>().is_ok())
        .map(|(index, component)| {
            let symbol = component[2]
                .as_str()
                .ok_or_else(|| anyhow!("Missing component symbol at index {index}"))?;
            let name = component[4]
                .as_str()
                .ok_or_else(|| anyhow!("Missing component name at index {index}"))?;

            Ok(format!("{name} ({symbol})"))
        })
        .collect::<Result<Vec<_>>>()?;

    let mut map = HashMap::new();

    for (index, name) in names.into_iter().enumerate() {
        let index = index + 1;

        let mut url = source.url.join("/api/air_data/v3/measures/json?lang=de")?;

        url.query_pairs_mut()
            .append_pair("date_from", yesterday)
            .append_pair("time_from", "1")
            .append_pair("date_to", yesterday)
            .append_pair("time_to", "24")
            .append_pair("component", &index.to_string());

        let text = client
            .fetch_text(source, format!("measures-de-{index}"), &url)
            .await?;

        #[derive(Deserialize)]
        struct Measures {
            data: Map<String, Value>,
        }

        let measures = from_str::<Measures>(&text)?;

        let stations = measures
            .data
            .keys()
            .map(|id| id.parse())
            .collect::<Result<HashSet<_>, _>>()?;

        map.insert(name, stations);
    }

    Ok(map)
}

fn create_components_str(map: &HashMap<String, HashSet<usize>>, id: usize) -> String {
    let names = map
        .iter()
        .filter(|(_, stations)| stations.contains(&id))
        .map(|(name, _)| name.as_str())
        .collect::<Vec<_>>();

    if !names.is_empty() {
        format!(
            "Aktuelle Messwerte sind verfügbar für die Schadstoffe: {}.",
            names.join(", ")
        )
    } else {
        String::new()
    }
}

const DATE_FORMAT: &[FormatItem] = format_description!("[year]-[month]-[day]");
