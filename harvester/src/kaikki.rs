use std::borrow::Cow;

use anyhow::Result;
use bincode::serialize;
use cap_std::fs::Dir;
use hashbrown::HashSet;
use serde::Deserialize;
use serde_json::from_str;

use harvester::{client::Client, Source};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let text = client
        .fetch_text(source, "dictionary".to_owned(), &source.url)
        .await?;

    let mut nouns = HashSet::<String>::new();

    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    for line in text.lines() {
        count += 1;

        match from_str::<Entry>(line) {
            Ok(entry) if entry.pos == "noun" && entry.word.len() > 3 => {
                nouns.insert(entry.word.to_lowercase());

                results += 1;
            }
            Ok(_) => (),
            Err(err) => {
                tracing::error!("Failed to parse entry: {err:#}");

                errors += 1;
            }
        }
    }

    const MISSING_NOUNS: &[&str] = &["messstelle", "messstellen"];

    for &noun in MISSING_NOUNS {
        if !nouns.insert(noun.to_owned()) {
            tracing::error!("Noun `{noun}` is not actually missing in Kaikki dictionary.");
        }
    }

    let buf = serialize(&nouns)?;
    dir.write("nouns.bin", &buf)?;

    Ok((count, results, errors))
}

#[derive(Deserialize)]
struct Entry<'a> {
    #[serde(borrow)]
    word: Cow<'a, str>,
    #[serde(borrow)]
    pos: &'a str,
}
