use std::fmt::Write;

use anyhow::Result;
use cap_std::fs::Dir;
use serde::Deserialize;
use serde_json::from_slice;
use smallvec::smallvec;
use time::{serde::format_description, PrimitiveDateTime};

use harvester::{
    client::Client, fetch_many, utilities::point_like_bounding_box, write_dataset, Source,
};
use metadaten::dataset::{
    r#type::{Domain, Station, Type},
    Dataset, Language, License, Resource, ResourceType, Tag,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let body = client
        .fetch_bytes(source, "responses".to_owned(), &source.url)
        .await?;

    let responses = from_slice::<Vec<Response>>(&body)?;

    let count = responses.len();
    tracing::info!("Retrieved {count} responses");

    let (results, errors) = fetch_many(0, 0, responses, |response| {
        translate_dataset(dir, client, source, response)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_dataset(
    dir: &Dir,
    client: &Client,
    source: &Source,
    response: Response<'_>,
) -> Result<(usize, usize, usize)> {
    if response.location.country != "DE" {
        return Ok((1, 0, 0));
    }

    let latitude = response.location.latitude.parse()?;
    let longitude = response.location.longitude.parse()?;
    let bounding_box = point_like_bounding_box(latitude, longitude);

    let id = response.sensor.id.to_string();

    let title = format!("Sensor.Community Sensor {id}");

    let description = format!(
        "Die Messwerte wurden mittels des Sensor-Modells \"{manufacturer} {name}\" erfasst.",
        manufacturer = response.sensor.sensor_type.manufacturer,
        name = response.sensor.sensor_type.name
    );

    let modified = response.timestamp.date();

    let measured_variables = response
        .values
        .iter()
        .filter_map(|value| match value.value_type {
            "N05" => Some("Feinstaub PM0.5"),
            "P0" | "N1" => Some("Feinstaub PM1"),
            "P1" | "N25" => Some("Feinstaub PM2.5"),
            "P4" | "N4" => Some("Feinstaub PM4"),
            "P2" | "N10" => Some("Feinstaub PM10"),
            "TS" => Some("Feinstaub"),
            "temperature" => Some("Temperatur"),
            "humidity" => Some("Luftfeuchtigkeit"),
            "pressure" | "pressure_sealevel" | "pressure_at_sealevel" => Some("Luftdruck"),
            "noise_LAeq" | "noise_LA_max" | "noise_LA_min" => Some("Lärm"),
            "lat" | "lon" | "height" | "timestamp" => None,
            "durP1" | "durP2" | "ratioP1" | "ratioP2" => None,
            "counts" | "counts_per_minute" | "hv_pulses" => None,
            "max_micro" | "min_micro" | "samples" | "sample_time_ms" => None,
            value_type => {
                tracing::warn!("Unexpected value type {value_type} measured by sensor {id}");

                None
            }
        })
        .map(ToOwned::to_owned)
        .collect();

    let r#type = Type::Measurements {
        domain: Domain::Air,
        station: Some(Station {
            id: Some(id.as_str().into()),
            ..Default::default()
        }),
        measured_variables,
        methods: Default::default(),
    };

    let resources = smallvec![
        Resource {
            r#type: ResourceType::Json,
            url: format!("https://data.sensor.community/airrohr/v1/sensor/{id}/"),
            description: Some("Messwerte der letzten 5 Minuten".to_owned()),
            primary_content: true,
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::Csv,
            url: "https://archive.sensor.community/".to_owned(),
            description: Some("Archiv aller Messwerte".to_owned()),
            primary_content: true,
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            url: source.source_url().replace("maps.", ""),
            description: Some("Projekt-Hauptseite".to_owned()),
            ..Default::default()
        },
        Resource {
            r#type: ResourceType::WebPage,
            url: "https://github.com/opendata-stuttgart/meta/wiki/APIs".to_owned(),
            description: Some("API-Dokumentation".to_owned()),
            ..Default::default()
        },
    ];

    let mut source_url = source.source_url().to_owned();
    write!(&mut source_url, "#16/{latitude}/{longitude}").unwrap();

    let dataset = Dataset {
        title,
        description: Some(description),
        modified: Some(modified),
        types: smallvec![r#type],
        resources,
        bounding_boxes: smallvec![bounding_box],
        tags: vec![Tag::CITIZEN_SCIENCE],
        license: License::DbCl10,
        language: Language::German,
        source_url,
        origins: source.origins.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, id, dataset).await
}

#[derive(Deserialize)]
struct Response<'a> {
    #[serde(with = "timestamp_format")]
    timestamp: PrimitiveDateTime,
    #[serde(borrow)]
    location: Location<'a>,
    #[serde(borrow)]
    sensor: Sensor<'a>,
    #[serde(rename = "sensordatavalues", borrow)]
    values: Vec<Value<'a>>,
}

#[derive(Deserialize)]
struct Location<'a> {
    country: &'a str,
    latitude: &'a str,
    longitude: &'a str,
}

#[derive(Deserialize)]
struct Sensor<'a> {
    id: u64,
    #[serde(borrow)]
    sensor_type: SensorType<'a>,
}

#[derive(Deserialize)]
struct SensorType<'a> {
    manufacturer: &'a str,
    name: &'a str,
}

#[derive(Deserialize)]
struct Value<'a> {
    value_type: &'a str,
}

format_description!(
    timestamp_format,
    PrimitiveDateTime,
    "[year]-[month]-[day] [hour]:[minute]:[second]"
);
