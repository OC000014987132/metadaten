use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use serde::Deserialize;
use tokio::fs::{read_dir, read_to_string};
use toml::from_str;

use harvester::{client::Client, fetch_many, write_dataset, Source};
use metadaten::dataset::IdentifiedDataset;

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let path = source
        .url
        .to_file_path()
        .map_err(|()| anyhow!("{} is not a valid path", source.url))?;

    let mut count = 0;
    let mut results = 0;
    let mut errors = 0;

    let mut entries = read_dir(path).await?;

    while let Some(entry) = entries.next_entry().await? {
        let contents = from_str::<FileContents>(&read_to_string(entry.path()).await?)?;

        count += contents.datasets.len();
        tracing::info!("Harvesting {} datasets from {:?}", count, entry.file_name());

        (results, errors) = fetch_many(results, errors, contents.datasets, |dataset| {
            write_dataset(dir, client, source, dataset.id, dataset.value)
        })
        .await;
    }

    Ok((count, results, errors))
}

#[derive(Deserialize)]
struct FileContents {
    datasets: Vec<IdentifiedDataset>,
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::fs::{read_dir, read_to_string};

    use anyhow::{ensure, Context};
    use tantivy::schema::Facet;

    #[test]
    fn validate_manual_datasets() -> Result<()> {
        for entry in read_dir("../deployment/manual_datasets")? {
            let entry = entry?;
            eprintln!("Validating {:?}...", entry.file_name());

            let contents = from_str::<FileContents>(&read_to_string(entry.path())?)?;

            for dataset in &contents.datasets {
                let id = &dataset.id;
                let dataset = &dataset.value;

                ensure!(!id.is_empty(), "Dataset has no identifier");

                ensure!(!dataset.title.is_empty(), "Dataset {id} has no title");
                ensure!(
                    dataset
                        .description
                        .as_ref()
                        .is_some_and(|description| !description.is_empty()),
                    "Dataset {id} has no description"
                );

                ensure!(!dataset.origins.is_empty(), "Dataset {id} has no origins");

                for origin in &dataset.origins {
                    Facet::from_text(origin)
                        .with_context(|| format!("Dataset {id} has invalid origin {origin}"))?;
                }

                ensure!(
                    !dataset.source_url.is_empty(),
                    "Dataset {id} has no source URL"
                );
            }
        }

        Ok(())
    }
}
