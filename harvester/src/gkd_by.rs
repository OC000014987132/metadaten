use std::fmt::Write;

use anyhow::{anyhow, Result};
use atomic_refcell::AtomicRefCell;
use cap_std::fs::Dir;
use compact_str::CompactString;
use cow_utils::CowUtils;
use geo::Rect;
use hashbrown::HashMap;
use miniproj::{get_projection, Projection};
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, remove_datasets, selectors,
    utilities::{
        collect_text, contains_or, make_suffix_key, point_like_bounding_box, select_text,
        GermanDate,
    },
    write_dataset, Source,
};
use metadaten::{
    dataset::{
        r#type::{Domain, ReportingObligation, Station, Type},
        Alternative, Dataset, GlobalIdentifier, Language, License, Organisation, OrganisationRole,
        Region, Resource, ResourceType, TimeRange,
    },
    wise::WISE,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = Selectors::default();
    let proj = get_projection(25832).unwrap();

    let clusters = Default::default();

    let (count, results, errors) =
        fetch_tabledata(dir, client, source, &selectors, proj, &clusters).await?;

    for (_key, mut site_keys) in clusters.into_inner() {
        site_keys.sort_unstable();
        site_keys.dedup();

        if site_keys.len() > 1 {
            merge_cluster(dir, client, source, &selectors, site_keys).await?;
        }
    }

    Ok((count, results, errors))
}

async fn fetch_tablelinks(
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    let url = &source.url.join("de/")?;

    let text = client
        .fetch_text(source, "mainlink".to_owned(), url)
        .await?;

    let document = Html::parse_document(&text);

    let mut links = document
        .select(&selectors.tablelinks)
        .filter_map(|element| {
            let link = element.attr("href").unwrap();
            if link.contains("downloadcenter")
                || link.ends_with("/fluess")
                || link.ends_with("/seen")
                || link.ends_with("/meteo")
                || link.ends_with("/grundwasser")
            {
                return None;
            }

            let href = format!("{}{}", &link, "/tabellen");
            Some(href)
        })
        .collect::<Vec<_>>();

    links.sort_unstable();
    links.dedup();

    Ok(links)
}

async fn fetch_tabledata(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    proj: &dyn Projection,
    clusters: &Cluster,
) -> Result<(usize, usize, usize)> {
    let tablelinks = fetch_tablelinks(client, source, selectors).await?;

    let mut sites = Vec::<Site>::new();

    for site_href in tablelinks {
        let url = Url::parse(&site_href)?;

        let key = make_suffix_key(&site_href, &source.url);

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        for row in document.select(&selectors.rows) {
            let mut cells = row.select(&selectors.cells);

            let site = cells.next().ok_or_else(|| anyhow!("Missing name column"))?;

            let link = site
                .select(&selectors.link)
                .next()
                .ok_or_else(|| anyhow!("Missing link"))?;

            let href = link.attr("href").unwrap();

            let href = href.cow_replace("/gesamtzeitraum?method=tabellen", "");
            let href = href.cow_replace("/befunde?method=tabellen", "");
            let href = href.cow_replace("/gesamtzeitraum?", "");
            let href = href.cow_replace('?', "").into_owned();

            let (category, parameter, parameter_description) = create_type_parameter(&href);

            let name = collect_text(site.text());

            let waterbody: CompactString;
            let municipality: CompactString;

            if category == "Meteorologie" || category == "Grundwasser" {
                waterbody = Default::default();

                municipality = cells
                    .next()
                    .ok_or_else(|| anyhow!("Missing municipality column"))?
                    .text()
                    .collect();
            } else {
                waterbody = cells
                    .next()
                    .ok_or_else(|| anyhow!("Missing body column"))?
                    .text()
                    .collect();

                municipality = cells
                    .next()
                    .ok_or_else(|| anyhow!("Missing municipality column"))?
                    .text()
                    .collect();
            }

            let measured_variable = match category {
                "Grundwasser" => "Grundwasserstand".to_owned(),
                "Fließgewässer" | "See" => "Pegelstand".to_owned(),
                "Meteorologie" => "Niederschlagshöhe".to_owned(),
                _ => "Unbekannt".to_owned(),
            };

            let title = format!("Messstelle {}, {} {} ", name, category, waterbody);

            let site = Site {
                href,
                category,
                parameter,
                parameter_description,
                name,
                waterbody,
                municipality,
                title,
                measured_variable,
            };

            sites.push(site);
        }
    }

    sites.sort_unstable_by(|lhs, rhs| lhs.href.cmp(&rhs.href));
    sites.dedup_by(|lhs, rhs| lhs.href == rhs.href);

    let count = sites.len();

    let (results, errors) = fetch_many(0, 0, sites, |site| {
        write_datasets_tables(dir, client, source, site, selectors, proj, clusters)
    })
    .await;

    Ok((count, results, errors))
}

async fn write_datasets_tables(
    dir: &Dir,
    client: &Client,
    source: &Source,
    site: Site,
    selectors: &Selectors,
    proj: &dyn Projection,
    clusters: &Cluster,
) -> Result<(usize, usize, usize)> {
    let key = make_suffix_key(&site.href, &source.url).into_owned();

    let site_details = fetch_site_details(client, source, &key, &site, selectors, proj).await?;

    // HACK: Reproduce the global identifier created by open.bydata and used by GovData to manage deduplication.
    let global_identifier = Some(GlobalIdentifier::Other(key.clone()));

    let (time_ranges, chemical_parameters_vec) =
        fetch_time_range_chemical_parameters(client, source, &site, selectors).await?;

    let description = site_details.description;

    let biological_parameters_vec =
        fetch_biological_parameters(client, source, &site, selectors).await?;

    let domain = if site.category.contains("Grundwasser") {
        Domain::Groundwater
    } else if site.category.contains("Meteorologie") {
        Domain::Air
    } else if site.category.contains("Fließgewässer") {
        Domain::Rivers
    } else if site.category.contains("See") {
        Domain::Surfacewater
    } else {
        Domain::Unspecified
    };

    let reporting_obligations = site
        .title
        .contains("WRRL")
        .then_some(ReportingObligation::Wrrl)
        .into_iter()
        .collect::<SmallVec<_>>();

    let mut types = smallvec![Type::Measurements {
        domain,
        station: Some(Station {
            id: Some(site_details.site_id.clone()),
            reporting_obligations: reporting_obligations.clone(),
            ..Default::default()
        }),
        measured_variables: smallvec![site.measured_variable],
        methods: Default::default(),
    }];

    if matches!(
        site.parameter,
        "Wassertemperatur" | "Abfluss" | "Schwebstoffe"
    ) {
        types.push(Type::Measurements {
            domain: Domain::Rivers,
            station: Some(Station {
                id: Some(site_details.site_id.clone()),
                reporting_obligations: reporting_obligations.clone(),
                ..Default::default()
            }),
            measured_variables: smallvec![site.parameter.to_owned()],
            methods: Default::default(),
        });
    }
    if !chemical_parameters_vec.is_empty() {
        types.push(Type::Measurements {
            domain: Domain::Chemistry,
            station: Some(Station {
                id: Some(site_details.site_id.clone()),
                reporting_obligations: reporting_obligations.clone(),
                ..Default::default()
            }),
            measured_variables: chemical_parameters_vec,
            methods: Default::default(),
        });
    }
    if !biological_parameters_vec.is_empty() {
        types.push(Type::Measurements {
            domain: Domain::Biology,
            station: Some(Station {
                id: Some(site_details.site_id.clone()),
                reporting_obligations,
                ..Default::default()
            }),
            measured_variables: biological_parameters_vec,
            methods: Default::default(),
        });
    }

    let dataset = Dataset {
        global_identifier,
        title: site.title,
        description: Some(description),
        types,
        time_ranges,
        regions: site_details.regions,
        bounding_boxes: site_details.bounding_boxes,
        language: Language::German,
        license: License::CcBy40,
        origins: source.origins.clone(),
        organisations: site_details.organisations,
        resources: site_details.resources,
        source_url: site.href,
        ..Default::default()
    };

    clusters
        .borrow_mut()
        .entry(site_details.site_id)
        .or_default()
        .push(key.clone());

    write_dataset(dir, client, source, key, dataset).await
}

fn create_type_parameter(href: &str) -> (&'static str, &'static str, &'static str) {
    let category;
    let parameter;
    let parameter_description;

    if href.contains("fluesse") && href.contains("wasserstand") {
        category = "Fließgewässer";
        parameter = "Wasserstand";
        parameter_description = "des Wasserstands";
    } else if href.contains("fluesse") && href.contains("abfluss") {
        category = "Fließgewässer";
        parameter = "Abfluss";
        parameter_description = "des Abflusses";
    } else if href.contains("fluesse") && href.contains("wassertemperatur") {
        category = "Fließgewässer";
        parameter = "Wassertemperatur";
        parameter_description = "der Wassertemperatur";
    } else if href.contains("fluesse") && href.contains("schwebstoff") {
        category = "Fließgewässer";
        parameter = "Schwebstoffe";
        parameter_description = "von Schwebstoffen";
    } else if href.contains("fluesse") && href.contains("chemie") {
        category = "Fließgewässer";
        parameter = "Chemie";
        parameter_description = "des chemischen Zustands";
    } else if href.contains("fluesse") && href.contains("biologie") {
        category = "Fließgewässer";
        parameter = "Biologie";
        parameter_description = "des biologischen Zustands";
    } else if href.contains("seen") && href.contains("wasserstand") {
        category = "See";
        parameter = "Wasserstand";
        parameter_description = "des Wasserstands";
    } else if href.contains("seen") && href.contains("wassertemperatur") {
        category = "See";
        parameter = "Wassertemperatur";
        parameter_description = "der Wassertemperatur";
    } else if href.contains("seen") && href.contains("chemie") {
        category = "See";
        parameter = "Chemie";
        parameter_description = "des chemischen Zustands";
    } else if href.contains("seen") && href.contains("biologie") {
        category = "See";
        parameter = "Biologie";
        parameter_description = "des biologischen Zustands";
    } else if href.contains("meteo") && href.contains("niederschlag") {
        category = "Meteorologie";
        parameter = "Niederschlag";
        parameter_description = "des Niederschlags";
    } else if href.contains("meteo") && href.contains("schnee") {
        category = "Meteorologie";
        parameter = "Schnee";
        parameter_description = "der Schneehöhe";
    } else if href.contains("meteo") && href.contains("lufttemperatur") {
        category = "Meteorologie";
        parameter = "Lufttemperatur";
        parameter_description = "der Lufttemperatur";
    } else if href.contains("meteo") && href.contains("luftfeuchte") {
        category = "Meteorologie";
        parameter = "Luftfeuchte";
        parameter_description = "der relativen Luftfeuchte";
    } else if href.contains("meteo") && href.contains("wind") {
        category = "Meteorologie";
        parameter = "Wind";
        parameter_description = "der Windgeschwindigkeit";
    } else if href.contains("meteo") && href.contains("globalstrahlung") {
        category = "Meteorologie";
        parameter = "Globalstrahlung";
        parameter_description = "der Globalstrahlung";
    } else if href.contains("meteo") && href.contains("luftdruck") {
        category = "Meteorologie";
        parameter = "Luftdruck";
        parameter_description = "des Luftdrucks";
    } else if href.contains("grundwasser") && href.contains("oberesstockwerk") {
        category = "Grundwasser";
        parameter = "Oberes Grundwasserstockwerk";
        parameter_description = "des Grundwasserstands im oberen Grundwasserstockwerk"
    } else if href.contains("grundwasser") && href.contains("tieferestockwerke") {
        category = "Grundwasser";
        parameter = "Tiefere Grundwasserstockwerke";
        parameter_description = "des Grundwasserstands in tieferen Grundwasserstockwerken"
    } else if href.contains("grundwasser") && href.contains("quellschuettung") {
        category = "Grundwasser";
        parameter = "Quellschüttung";
        parameter_description = "der Quellschüttung";
    } else if href.contains("grundwasser") && href.contains("quelltemperatur") {
        category = "Grundwasser";
        parameter = "Quelltemperatur";
        parameter_description = "der Quelltemperatur";
    } else if href.contains("grundwasser") && href.contains("chemie") {
        category = "Grundwasser";
        parameter = "Chemie";
        parameter_description = "des chemischen Zustands";
    } else {
        category = Default::default();
        parameter = Default::default();
        parameter_description = Default::default();
    }

    (category, parameter, parameter_description)
}

async fn fetch_site_details(
    client: &Client,
    source: &Source,
    key: &str,
    site: &Site,
    selectors: &Selectors,
    proj: &dyn Projection,
) -> Result<SiteDetails> {
    let mut bounding_boxes = SmallVec::new();
    let mut description;
    let mut regions = SmallVec::new();
    let mut organisations = SmallVec::new();
    let site_id;
    let mut resources = SmallVec::new();

    {
        let url = site.href.parse()?;

        let text = client
            .fetch_text(source, format!("{key}.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        let basic_data = select_text(&document, &selectors.basic_data);

        regions.extend(
            document
                .select(&selectors.basic_data)
                .flat_map(|elem| elem.text())
                .filter_map(|text| {
                    text.find("Landkreis: ")
                        .map(|pos| text[pos + 11..].trim())
                        .filter(|municipality| !municipality.is_empty())
                })
                .next()
                .map(|municipality| contains_or(municipality, Region::BY)),
        );

        if let Some(capture) = selectors.position_regex.captures(&basic_data) {
            let lon = capture["east"].parse::<f64>();
            let lat = capture["north"].parse::<f64>();

            match (lon, lat) {
                (Ok(lon), Ok(lat)) => {
                    let (lon, lat) = proj.projected_to_deg(lon, lat);

                    if !site.category.contains("Grundwasser")
                        && !site.category.contains("Meteorologie")
                    {
                        regions.extend(WISE.match_shape(lon, lat).map(Region::Watershed));
                    }

                    bounding_boxes.push(point_like_bounding_box(lat, lon))
                }
                _ => tracing::error!("Error parsing coordinates from {basic_data}"),
            }
        }

        if let Some(capture) = selectors.betreiber_regex.captures(&basic_data) {
            let name = capture[2].to_owned();

            organisations.push(Organisation::Other {
                name,
                role: OrganisationRole::Operator,
                websites: Default::default(),
            })
        } else {
            tracing::warn!("No operator found in {basic_data}");
        };

        site_id = if let Some(capture) = selectors.site_id_regex.captures(&basic_data) {
            capture[1].into()
        } else {
            tracing::error!("Failed to extract site_id from {basic_data}");

            "Unbekannt".into()
        };

        if !site.waterbody.is_empty() {
            description = format!("Die Messstelle {} (Messstellen-Nr: {}) befindet sich im Gewässer {}. Die Messstelle dient der Überwachung {}.", site.name, site_id, site.waterbody, site.parameter_description)
        } else {
            description = format!(
                "Die Messstelle {} (Messstellen-Nr: {}) dient der Überwachung {}.",
                site.name, site_id, site.parameter_description
            )
        };
    }

    {
        let url = format!("{}/download", &site.href).parse()?;

        let text = client
            .fetch_text(source, format!("{key}-download.isol"), &url)
            .await?;

        let document = Html::parse_document(&text);

        if document.select(&selectors.downloadbutton).next().is_some() {
            resources.push(Resource {
                r#type: ResourceType::Csv,
                url: url.into(),
                description: Some("Link zum Datendownload".to_owned()),
                direct_link: false,
                primary_content: true,
            });
        } else {
            write!(
                &mut description,
                "\n\nFür diese Station ist kein Daten-Download möglich."
            )?;
        }
    }

    Ok(SiteDetails {
        bounding_boxes,
        description,
        regions,
        organisations,
        site_id,
        resources,
    })
}

async fn fetch_time_range_chemical_parameters(
    client: &Client,
    source: &Source,
    site: &Site,
    selectors: &Selectors,
) -> Result<(SmallVec<[TimeRange; 1]>, SmallVec<[String; 1]>)> {
    let url = format!("{}/download", &site.href);
    let key = make_suffix_key(&url, &source.url);

    let text = client
        .fetch_text(source, format!("{key}.isol"), &Url::parse(&url)?)
        .await?;

    let document = Html::parse_document(&text);

    let datadownload = select_text(&document, &selectors.datadownload);

    let mut time_ranges = SmallVec::new();

    if let Some(capture) = selectors.time_ranges.captures(&datadownload) {
        let from = capture["from"].parse::<GermanDate>()?;
        let until = capture["until"].parse::<GermanDate>()?;
        time_ranges.push((from, until).into());
    }

    let chemical_analyses_parameters_vec = if site.parameter == "Chemie" {
        // The selector does not return all possible parameters,
        // pesticides are not fetched.
        document
            .select(&selectors.chemical_analyses)
            .map(|element| collect_text(element.text()))
            .filter(|text| text != "- alle -")
            .collect::<SmallVec<_>>()
    } else {
        Default::default()
    };

    Ok((time_ranges, chemical_analyses_parameters_vec))
}

async fn fetch_bio_parameter_combinations(
    client: &Client,
    source: &Source,
    site: &Site,
    selectors: &Selectors,
) -> Result<Vec<String>> {
    if site.parameter != "Biologie" {
        return Ok(Vec::new());
    }

    let url = format!("{}/befunde", &site.href);
    let key = make_suffix_key(&url, &source.url);

    let text = client
        .fetch_text(source, format!("{key}.isol"), &Url::parse(&url)?)
        .await?;

    let document = Html::parse_document(&text);

    let components = document
        .select(&selectors.components)
        .next()
        .ok_or_else(|| anyhow!("Missing component"))?;

    let bio_component = components
        .select(&selectors.bio_component)
        .map(|element| element.attr("name").unwrap());

    let bio_component_value = components
        .select(&selectors.bio_component_value)
        .map(|element| element.attr("value").unwrap());

    let mut combinations = Vec::new();

    for value in bio_component_value {
        for component in bio_component.clone() {
            combinations.push(format!("{}={}", component, value));
        }
    }

    Ok(combinations)
}

async fn fetch_biological_parameters(
    client: &Client,
    source: &Source,
    site: &Site,
    selectors: &Selectors,
) -> Result<SmallVec<[String; 1]>> {
    let combinations = fetch_bio_parameter_combinations(client, source, site, selectors).await?;

    let mut biological_parametes_vec = SmallVec::new();

    let url = format!("{}/befunde", &site.href);

    for combination in combinations {
        let url = format!("{}?{}", &url, combination);
        let key = make_suffix_key(&url, &source.url);

        let text = client
            .fetch_text(source, format!("{key}.isol"), &Url::parse(&url)?)
            .await?;

        let document = Html::parse_document(&text);

        for row in document.select(&selectors.rows) {
            let mut cells = row.select(&selectors.cells);

            let species = cells
                .nth(2)
                .ok_or_else(|| anyhow!("Missing DV Nr column"))?;

            let species_names = species.text().collect::<String>();

            biological_parametes_vec.push(species_names);
        }
    }

    biological_parametes_vec.sort_unstable();
    biological_parametes_vec.dedup();

    Ok(biological_parametes_vec)
}

type Cluster = AtomicRefCell<HashMap<CompactString, SmallVec<[String; 5]>>>;

async fn merge_cluster(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    site_keys: SmallVec<[String; 5]>,
) -> Result<()> {
    let datasets = remove_datasets(dir, client, source, &site_keys).await?;

    let latest_key = site_keys.into_iter().next().unwrap();

    let mut datasets = datasets.into_iter();
    let mut latest_dataset = datasets.next().unwrap();

    fn split_description(
        haystack: &str,
        selectors: &Selectors,
    ) -> Result<(String, String, String)> {
        if let Some(capture) = selectors.parameters_collect_regex.captures(haystack) {
            Ok((
                capture["start"].to_owned(),
                capture["topic"].to_owned(),
                capture["details"].to_owned(),
            ))
        } else {
            Err(anyhow!("Failed to split description"))
        }
    }

    let mut desc_topics = Vec::new();
    let mut desc_details = Vec::new();

    let (desc_start, desc_topic, desc_detail) =
        split_description(&latest_dataset.description.unwrap(), selectors)?;

    desc_topics.push(desc_topic);
    if !desc_detail.is_empty() {
        desc_details.push(desc_detail);
    }

    for older_dataset in datasets {
        latest_dataset.global_identifier = latest_dataset
            .global_identifier
            .or(older_dataset.global_identifier);

        let (_desc_start, desc_topic, desc_detail) =
            split_description(&older_dataset.description.unwrap(), selectors)?;

        desc_topics.push(desc_topic);
        if !desc_detail.is_empty() {
            desc_details.push(desc_detail);
        }

        latest_dataset.alternatives.push(Alternative::Source {
            source: source.name.clone(),
            title: older_dataset.title,
            url: older_dataset.source_url,
        });

        latest_dataset.types.extend(older_dataset.types);

        latest_dataset.resources.extend(older_dataset.resources);

        latest_dataset
            .organisations
            .extend(older_dataset.organisations);

        latest_dataset
            .bounding_boxes
            .extend(older_dataset.bounding_boxes);

        latest_dataset.time_ranges.extend(older_dataset.time_ranges);
    }

    latest_dataset.description = Some(format!(
        "{} {}.\n{}",
        desc_start,
        desc_topics.join(", "),
        desc_details.join("\n\n")
    ));

    write_dataset(dir, client, source, latest_key, latest_dataset).await?;

    Ok(())
}

#[derive(Debug, PartialEq, Ord, PartialOrd, Eq, Clone)]
struct Site {
    href: String,
    category: &'static str,
    parameter: &'static str,
    parameter_description: &'static str,
    name: String,
    waterbody: CompactString,
    municipality: CompactString,
    title: String,
    measured_variable: String,
}

struct SiteDetails {
    bounding_boxes: SmallVec<[Rect; 1]>,
    description: String,
    regions: SmallVec<[Region; 1]>,
    organisations: SmallVec<[Organisation; 2]>,
    site_id: CompactString,
    resources: SmallVec<[Resource; 4]>,
}

selectors! {
    tablelinks: "div#navi_horizontal a[href]",

    rows: "tbody > tr",
    cells: "td",
    link: "a[href]",

    basic_data: "div.col > p",

    datadownload: "div.col",

    time_ranges: r"(?<from>\d{2}\.\d{2}.\d{4})\sbis\szum\s*(?<until>\d{2}\.\d{2}.\d{4})" as Regex,

    site_id_regex :r"Messstellen-Nr.:\s(\w+)" as Regex,

    municipality_regex: r"Landkreis:\s(.*)" as Regex,
    position_regex: r"Ostwert:\s+(?<east>\d+).+Nordwert:\s+(?<north>\d+)" as Regex,
    betreiber_regex: r"(Betreiber:|Zuständiges\sAmt:|Messdaten bereitgestellt durch:)\s(.*?)\s+\w*:" as Regex,
    chemical_analyses:
    "div#messgroesse_fluesse_chemie select option,
    div#messgroesse_seen_chemie select option, 
    div#messgroesse_grundwasser_chemie select option",
    components: "div.col > form",
    bio_component: "div > select",
    bio_component_value: "div > select > option",
    parameters_collect_regex: r"(?<start>.*dient der Überwachung )(?<topic>.*?)\.\s*(?<details>.*)?" as Regex,
    downloadbutton: "#dlbutton",
}
