use anyhow::{anyhow, Result};
use cap_std::fs::Dir;
use regex::Regex;
use scraper::{Html, Selector};
use smallvec::{smallvec, SmallVec};
use url::Url;

use harvester::{
    client::Client,
    fetch_many, selectors,
    utilities::{
        collect_text, make_key, make_suffix_key, parse_text, parse_texts, select_first_text,
        select_text, yesterday, GermanDate,
    },
    write_dataset, Source,
};
use metadaten::dataset::{
    r#type::Type, Dataset, Language, License, Resource, ResourceType, TimeRange,
};

pub async fn harvest(dir: &Dir, client: &Client, source: &Source) -> Result<(usize, usize, usize)> {
    let selectors = &Selectors::default();

    let (mut count, mut results, mut errors) =
        fetch_bundesrecht(dir, client, source, selectors).await?;

    let (count1, results1, errors1) = fetch_verordnungen(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    let (count1, results1, errors1) = fetch_weiteres(dir, client, source, selectors).await?;
    count += count1;
    results += results1;
    errors += errors1;

    Ok((count, results, errors))
}

async fn fetch_bundesrecht(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let source_url = source.url.join("bundesrecht")?;

    let text = client
        .fetch_text(source, "bundesrecht".to_owned(), &source_url)
        .await?;

    let document = Html::parse_document(&text);

    let items = document
        .select(&selectors.br_legal_links)
        .map(|element| {
            let act_href = element.attr("href").unwrap().into();

            let act_title = collect_text(element.text());

            Pagedata {
                act_title,
                act_href,
            }
        })
        .collect::<Vec<_>>();

    let count = items.len();

    let (results, errors) = fetch_many(0, 0, items, |item| async move {
        if item.act_href.contains("gesetze-im-internet") {
            translate_pagedata_gesetze_im_internet(dir, client, source, selectors, item).await
        } else {
            translate_pagedata_bundesrecht(dir, client, source, item).await
        }
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_verordnungen(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let source_url = source.url.join("verordnungs-und-gesetzgebungsverfahren")?;

    let text = client
        .fetch_text(
            source,
            "verordnungs-und-gesetzgebungsverfahren".to_owned(),
            &source_url,
        )
        .await?;

    let document = Html::parse_document(&text);

    let mut entries = Vec::new();

    let mut curr_entry = PageEntry::default();

    for top_element in document.select(&selectors.vo_container) {
        let element = top_element
            .select(&selectors.vo_paragraph)
            .next()
            .ok_or_else(|| anyhow!("Could not find paragraph in container"))?;

        let anchor = top_element
            .select(&selectors.vo_anchor)
            .next()
            .map(|elem| elem.attr("id").unwrap().to_owned());

        let title = select_first_text(element, &selectors.vo_container_title, "title")
            .ok()
            .filter(|title| !title.starts_with("Weiterführende"));

        if let Some(title) = title {
            if !curr_entry.title.is_empty() {
                entries.push(curr_entry);
            }

            curr_entry = PageEntry::default();

            curr_entry.title = title;
            curr_entry.anchor = anchor;

            curr_entry.description =
                select_first_text(element, &selectors.vo_container_description, "description")?;
        } else {
            for element in element.select(&selectors.vo_container_resource_title_href) {
                let description = element.attr("title").unwrap().to_owned();
                let href = element.attr("href").unwrap().to_owned();

                curr_entry.links.push((description, href));
            }

            for element in element.select(&selectors.vo_container_download_title_href) {
                let description = select_text(element, &selectors.vo_container_download_title);
                let href = element.attr("href").unwrap().to_owned();

                curr_entry.downloads.push((description, href));
            }
        }
    }

    if !curr_entry.title.is_empty() {
        entries.push(curr_entry);
    }

    let count = entries.len();

    let (results, errors) = fetch_many(0, 0, entries, |entry| {
        translate_pagedata_verordnungen(dir, client, source, entry)
    })
    .await;

    Ok((count, results, errors))
}

async fn fetch_weiteres(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
) -> Result<(usize, usize, usize)> {
    let source_url = source.url.join("weiteres-umweltrecht")?;

    let text = client
        .fetch_text(source, "weiteres-umweltrecht".to_owned(), &source_url)
        .await?;

    let document = Html::parse_document(&text);

    let items = document
        .select(&selectors.br_legal_links)
        .map(|element| {
            let act_href = element.attr("href").unwrap().into();

            let act_title = collect_text(element.text());

            Pagedata {
                act_title,
                act_href,
            }
        })
        .collect::<Vec<_>>();

    let count = items.len();

    let (results, errors) = fetch_many(0, 0, items, |item| {
        translate_pagedata_gesetze_im_internet(dir, client, source, selectors, item)
    })
    .await;

    Ok((count, results, errors))
}

async fn translate_pagedata_bundesrecht(
    dir: &Dir,
    client: &Client,
    source: &Source,
    item: Pagedata,
) -> Result<(usize, usize, usize)> {
    let key = make_suffix_key(&item.act_href, &source.url).into_owned();

    let dataset = Dataset {
        title: item.act_title,
        types: smallvec![Type::Legal],
        license: License::OfficialWork,
        language: Language::German,
        source_url: item.act_href,
        origins: source.origins.clone(),
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn translate_pagedata_verordnungen(
    dir: &Dir,
    client: &Client,
    source: &Source,
    entry: PageEntry,
) -> Result<(usize, usize, usize)> {
    let href = if let Some(anchor) = entry.anchor {
        format!("verordnungs-und-gesetzgebungsverfahren#{}", anchor)
    } else {
        "verordnungs-und-gesetzgebungsverfahren".to_owned()
    };

    let source_url = source.url.join(&href)?;

    let key = make_key(&entry.title).into_owned();

    let resources = entry
        .links
        .into_iter()
        .map(|(description, href)| {
            let description = description.replace("(externe PDF)", "").trim().to_owned();

            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: Some(description),
                url: source.url.join(&href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        })
        .chain(entry.downloads.into_iter().map(|(description, href)| {
            Ok(Resource {
                r#type: ResourceType::WebPage,
                description: Some(description),
                url: source.url.join(&href)?.into(),
                ..Default::default()
            }
            .guess_or_keep_type())
        }))
        .collect::<Result<SmallVec<_>>>()?;

    let dataset = Dataset {
        title: entry.title,
        description: Some(entry.description),
        types: smallvec![Type::Legal],
        resources,
        origins: source.origins.clone(),
        source_url: source_url.into(),
        license: License::OfficialWork,
        language: Language::German,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

async fn translate_pagedata_gesetze_im_internet(
    dir: &Dir,
    client: &Client,
    source: &Source,
    selectors: &Selectors,
    item: Pagedata,
) -> Result<(usize, usize, usize)> {
    let href = {
        let source_url = item.act_href.parse()?;

        let key = make_key(&item.act_href).into_owned();

        let text = client.fetch_text(source, key, &source_url).await?;

        let document = Html::parse_document(&text);

        document
            .select(&selectors.gesetz_href)
            .next()
            .map(|element| element.attr("href").unwrap().to_owned())
            .unwrap_or_else(|| source_url.into())
    };

    let source_url = item.act_href.parse::<Url>()?.join(&href)?;

    let key = make_key(source_url.as_str()).into_owned();

    let text = client.fetch_text(source, key.clone(), &source_url).await?;

    let document = Html::parse_document(&text);

    let description = select_first_text(&document, &selectors.gesetz_norms, "First Norm")
        .unwrap_or_else(|_| select_text(&document, &selectors.gesetz_fulltext));

    let issued = parse_text::<_, GermanDate>(
        &document,
        &selectors.gesetz_header,
        &selectors.gesetz_date,
        "No issued present",
    )
    .ok()
    .map(Into::into);

    let modified = parse_texts::<_, GermanDate>(
        &document,
        &selectors.gesetz_modified,
        &selectors.gesetz_modified_date,
        "No modified present",
    )
    .last()
    .transpose()?
    .map(Into::into);

    let time_ranges = issued
        .iter()
        .map(|from| TimeRange {
            from: *from,
            until: yesterday(),
        })
        .collect();

    let dataset = Dataset {
        title: item.act_title,
        description: Some(description),
        types: smallvec![Type::Legal],
        issued,
        modified,
        time_ranges,
        origins: source.origins.clone(),
        source_url: source_url.into(),
        license: License::OfficialWork,
        language: Language::German,
        ..Default::default()
    };

    write_dataset(dir, client, source, key, dataset).await
}

#[derive(Debug, Default)]
struct PageEntry {
    title: String,
    anchor: Option<String>,
    description: String,
    links: Vec<(String, String)>,
    downloads: Vec<(String, String)>,
}

#[derive(Debug)]
struct Pagedata {
    act_title: String,
    act_href: String,
}

selectors! {
    br_legal_links: "a[href*='gesetze-im-internet']",
    vo_container: "article.s-node section.s-paragraph",
    vo_paragraph: "div.s-paragraph__container",
    vo_container_title: "h2.field.field--name-field-headline",
    vo_container_description: "div.field.field--name-field-text",
    vo_container_resource_title_href: "div.field.field--name-field-links a[title][href]",
    vo_container_download_title_href: "div.field.field--name-field-downloads a[title][href]",
    vo_container_download_title: "span.s-file__name",
    vo_anchor: "div.s-wrapper--anchor a[id]",
    gesetz_href: ".headline a[href*='html']",
    gesetz_fulltext: "div#content_2022",
    gesetz_header: ".jnheader",
    gesetz_title: "h1",
    gesetz_date: r"Ausfertigungsdatum:\s+(\d{2}\.\d{2}\.\d{4})" as Regex,
    gesetz_norms: ".jnnorm[title*='Einzelnorm']",
    gesetz_modified: ".standangaben tbody",
    gesetz_modified_date: r"(\d{1,2}\.\d{1,2}\.\d{4})" as Regex,
}
