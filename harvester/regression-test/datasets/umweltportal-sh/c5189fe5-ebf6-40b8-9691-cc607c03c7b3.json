{
  "title": "Bodenbewertung - natürliche Ertragsfähigkeit (BGZ), landesweit bewertet",
  "description": "Die natürliche Ertragsfähigkeit ist ein Kennwert zur Bewertung des Bodens als Standort für die landwirtschaftliche Nutzung und wird über die Boden- und Grünlandgrundzahl bewertet. Boden- und Grünlandgrundzahlen werden in Abhängigkeit von der Bodenart, der Zustandsstufe, der Entstehung sowie dem Klima geschätzt. Besonders die Bodenart beeinflusst viele ertragsbildende Prozesse. So können Böden aus Sand wenig Wasser mit den darin gelösten Nährstoffen bei Trockenheit bereitstellen, Böden aus Lehm mehr. Böden aus Lehm können austauschbar gebundene Nährstoffe besser speichern als Böden aus Sand. Böden gleicher Bodenart besitzen bei unterschiedlichen Zustandsstufen (Entwicklungs-/ Alterungsstufen) auch in unterschiedlichem Maße die Fähigkeit, Wasser und Nährstoffe zu speichern und den Kulturpflanzen bereitzustellen. Solche und andere für die Ertragsfähigkeit wichtigen Unterschiede in den Standortverhältnissen schlagen sich in den Boden- und Grünlandgrundzahlen nieder. Mit Boden- und Grünlandgrundzahlen wird eine Nutzungsfunktion des Bodens nach § 2 Abs. 2 BBodSchG bewertet und zwar nach Punkt 3.c) die Nutzungsfunktionen als Standort für die land- und forstwirtschaftliche Nutzung. Das hierfür gewählte Kriterium ist die natürliche Ertragsfähigkeit mit dem Kennwert Boden- und Grünlandgrundzahl. Die Karten liegen für die folgenden Maßstabsebenen vor: - 1 : 1.000 - 10.000 für hochaufgelöste oder parzellenscharfe Planung, - 1 : 10.001 - 35.000 für Planungen auf Gemeindeebene, - 1 : 35.001 - 100.000 für Planungen in größeren Regionen, - 1 : 100.001 - 350.000 für landesweit differenzierte Planung, - 1 : 350.001 - 1000.000 für landesweite bis bundesweite Planung. In dieser Darstellung wird die natürliche Ertragsfähigkeit landesweit einheitlich klassifiziert. Unter dem Titel \"Bodenbewertung - natürliche Ertragsfähigkeit (BGZ), regionalspezifisch bewertet\" gibt es noch eine naturraumbezogene Klassifikation des Bodenwasseraustausches, die den Bodenwasseraustausch regional differenzierter darstellt.",
  "origins": [
    "/Land/Schleswig-Holstein/Umweltportal"
  ],
  "license": {
    "path": "/offen/dl-de/by/2.0",
    "label": "dl-by-de/2.0",
    "url": "https://www.govdata.de/dl-de/by-2-0"
  },
  "mandatory_registration": false,
  "organisations": [
    {
      "Other": {
        "name": "Landesamt für Umwelt des Landes Schleswig-Holstein (LfU)",
        "role": "Unknown",
        "websites": [
          "https://www.schleswig-holstein.de/DE/landesregierung/ministerien-behoerden/LFU/organisation/abteilungen/abteilung6_geologie.html"
        ]
      }
    }
  ],
  "persons": [
    {
      "name": "Willer, Jan, Herr",
      "role": "Contact",
      "emails": [
        "jan.willer@lfu.landsh.de"
      ]
    }
  ],
  "tags": [
    {
      "Other": "BBodSchG"
    },
    {
      "Other": "Boden"
    },
    {
      "Other": "Boden- und Grünlandgrundzahl"
    },
    {
      "Other": "Bodenart"
    },
    {
      "Other": "Bodenbewertung"
    },
    {
      "Other": "Bodenfruchtbarkeit"
    },
    {
      "Other": "Bodenfunktion"
    },
    {
      "Other": "Bodenpunkte"
    },
    {
      "Other": "Bodenschätzung"
    },
    {
      "Other": "Bodenzahl"
    },
    {
      "Other": "Bundes-Bodenschutzgesetz"
    },
    {
      "Other": "ENVI"
    },
    {
      "Other": "Ertragsfähigkeit"
    },
    {
      "Other": "Grünlandgrundzahl"
    },
    {
      "Other": "Grünlangrundzahl"
    },
    {
      "Other": "Karten"
    },
    {
      "Other": "Klassenzeichen"
    },
    {
      "Other": "Landschaftsplan"
    },
    {
      "Other": "Landschaftsplanung"
    },
    {
      "Other": "Landschaftsprogramm"
    },
    {
      "Other": "Landschaftsrahmenplan"
    },
    {
      "Other": "Nutzungsfunktion"
    },
    {
      "Other": "Schätzflächen"
    },
    {
      "Other": "Schätzungsrahmen"
    },
    {
      "Other": "Standort für land- und forstwirtschaftliche Nutzung"
    },
    {
      "Other": "Standort für landwirtschaftliche Nutzung"
    },
    {
      "Other": "Zustandsstufe"
    },
    {
      "Other": "gdi-sh"
    },
    {
      "Other": "natürliche Ertragsfähigkeit"
    },
    {
      "Other": "opendata"
    }
  ],
  "modified": "2023-08-29",
  "source_url": "https://umweltportal.schleswig-holstein.de/trefferanzeige?docuuid=c5189fe5-ebf6-40b8-9691-cc607c03c7b3",
  "machine_readable_source": true,
  "resources": [
    {
      "type": {
        "path": "/Archiv/ZIP",
        "label": "ZIP"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/bgz/bgz_reg_sh_002.zip",
      "direct_link": true,
      "primary_content": true
    },
    {
      "type": {
        "path": "/Archiv/ZIP",
        "label": "ZIP"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/bgz/bgz_sh_025.zip",
      "direct_link": true,
      "primary_content": true
    },
    {
      "type": {
        "path": "/Archiv/ZIP",
        "label": "ZIP"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/bgz/bgz_sh_100.zip",
      "direct_link": true,
      "primary_content": true
    },
    {
      "type": {
        "path": "/Dokument/PDF",
        "label": "PDF"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/dok/erlaeuterungen_bodenbewertung.pdf",
      "direct_link": true,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Dokument/PDF",
        "label": "PDF"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/dok/steckbrief_bgz.pdf",
      "direct_link": true,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Dokument/PDF",
        "label": "PDF"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/dok/zuordnung_bgz.pdf",
      "direct_link": true,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Webseite",
        "label": "Webseite"
      },
      "url": "https://umweltgeodienste.schleswig-holstein.de/WFS_Bodenbewertung?",
      "direct_link": false,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Webseite",
        "label": "Webseite"
      },
      "url": "https://umweltgeodienste.schleswig-holstein.de/WMS_Bodenbewertung?",
      "direct_link": false,
      "primary_content": false
    }
  ],
  "language": {
    "id": "German",
    "path": "/Deutsch",
    "label": "Deutsch"
  },
  "bounding_boxes": [
    {
      "min": {
        "x": 7.87,
        "y": 53.35
      },
      "max": {
        "x": 11.32,
        "y": 55.06
      }
    }
  ],
  "time_ranges": [
    {
      "from": "2023-08-29",
      "until": "2023-08-29"
    }
  ],
  "global_identifier": {
    "Url": "http://portalu.de/igc/aee17477-6027-4451-8cd6-435beaa3c17f"
  },
  "quality": {
    "findability": {
      "title": 0.0,
      "description": 0.34353352,
      "spatial": "BoundingBox",
      "spatial_score": 0.5,
      "temporal": true,
      "keywords": 0.0,
      "identifier": true,
      "score": 0.47392225
    },
    "accessibility": {
      "landing_page": "Specific",
      "landing_page_score": 1.0,
      "direct_access": true,
      "publicly_accessible": true,
      "score": 1.0
    },
    "interoperability": {
      "open_file_format": true,
      "media_type": true,
      "machine_readable_data": false,
      "machine_readable_metadata": true,
      "score": 0.75
    },
    "reusability": {
      "license": "ClearlySpecifiedAndFree",
      "license_score": 1.0,
      "contact_info": true,
      "publisher_info": true,
      "score": 1.0
    },
    "score": 0.80598056
  },
  "status": "Active"
}
