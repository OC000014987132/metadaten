{
  "title": "Bodenbewertung - Nitratauswaschungsgefährdung/Bodenwasseraustausch (NAG), landesweit bewertet",
  "description": "Der Bodenwasseraustausch ist ein Kennwert zur Bewertung des Bodens als Filter für nicht sorbierbare Stoffe und kennzeichnet das Verlagerungsrisiko für nicht oder kaum sorbierbare Stoffe wie Nitrat (Nitratauswaschungsgefährdung). Die Nährstoffe verbleiben fast vollständig in gelöster Form im Bodenwasser und werden bei Versickerung mit diesem verlagert (Bodenwasseraustausch). Das Verlagerungsrisiko ist hoch bei Böden mit geringem Wasserrückhaltevermögen, bei hohen Niederschlägen und bei geringer Evapotranspiration. Das Verlagerungsrisiko ist umso höher, je höher der Bodenwasseraustausch ist, weil das ausgetauschte Bodenwasser mit den darin gelösten Nitraten versickert. Mit dem Bodenwasseraustausch wird eine natürliche Bodenfunktionen nach § 2 Abs. 2 BBodSchG bewertet und zwar nach Punkt 1.c) als Abbau-, Ausgleichs- und Aufbaumedium für stoffliche Einwirkungen auf Grund der Filter-, Puffer- und Stoffumwandlungseigenschaften, insbesondere auch zum Schutz des Grundwassers. Das hierfür gewählte Kriterium ist das Rückhaltevermögen des Bodens für nicht sorbierbare Stoffe mit dem Kennwert Bodenwasseraustausch. Die Karten liegen für die folgenden Maßstabsebenen vor: - 1 : 1.000 - 10.000 für hochaufgelöste oder parzellenscharfe Planung, - 1 : 10.001 - 35.000 für Planungen auf Gemeindeebene, - 1 : 35.001 - 100.000 für Planungen in größeren Regionen, - 1 : 100.001 - 350.000 für landesweit differenzierte Planung, - 1 : 350.001 - 1000.000 für landesweite bis bundesweite Planung. In dieser Darstellung wird der Bodenwasseraustausch landesweit einheitlich klassifiziert. Unter dem Titel \"Bodenbewertung - Nitratauswaschungsgefährdung/Bodenwasseraustausch (NAG), regionalspezifisch bewertet\" gibt es noch eine naturraumbezogene Klassifikation des Bodenwasseraustausches, die den Bodenwasseraustausch regional differenzierter darstellt.",
  "origins": [
    "/Land/Schleswig-Holstein/Umweltportal"
  ],
  "license": {
    "path": "/offen/dl-de/by/2.0",
    "label": "dl-by-de/2.0",
    "url": "https://www.govdata.de/dl-de/by-2-0"
  },
  "mandatory_registration": false,
  "organisations": [
    {
      "Other": {
        "name": "Landesamt für Umwelt des Landes Schleswig-Holstein (LfU)",
        "role": "Unknown",
        "websites": [
          "https://www.schleswig-holstein.de/DE/landesregierung/ministerien-behoerden/LFU/organisation/abteilungen/abteilung6_geologie.html"
        ]
      }
    }
  ],
  "persons": [
    {
      "name": "Willer, Jan, Herr",
      "role": "Contact",
      "emails": [
        "jan.willer@lfu.landsh.de"
      ]
    }
  ],
  "tags": [
    {
      "Other": "Abbau"
    },
    {
      "Other": "Aufbau"
    },
    {
      "Other": "Ausgleich"
    },
    {
      "Other": "Austauschhäufigkeit"
    },
    {
      "Other": "BBodSchG"
    },
    {
      "Other": "Boden"
    },
    {
      "Other": "Bodenbewertung"
    },
    {
      "Other": "Bodenfunktion"
    },
    {
      "Other": "Bodenwasseraustausch"
    },
    {
      "Other": "Bundes-Bodenschutzgesetz"
    },
    {
      "Other": "ENVI"
    },
    {
      "Other": "Filter"
    },
    {
      "Other": "Grundwasser"
    },
    {
      "Other": "Grundwasserschutz"
    },
    {
      "Other": "Karten"
    },
    {
      "Other": "Landschaftsplan"
    },
    {
      "Other": "Landschaftsprogramm"
    },
    {
      "Other": "Landschaftsrahmenplan"
    },
    {
      "Other": "NAG"
    },
    {
      "Other": "Nitrat"
    },
    {
      "Other": "Nitratauswaschungsgefährdung"
    },
    {
      "Other": "Nitrate"
    },
    {
      "Other": "Puffer"
    },
    {
      "Other": "Stoffumwandlung"
    },
    {
      "Other": "gdi-sh"
    },
    {
      "Other": "nicht sorbierbare Stoffe"
    },
    {
      "Other": "opendata"
    }
  ],
  "modified": "2023-08-29",
  "source_url": "https://umweltportal.schleswig-holstein.de/trefferanzeige?docuuid=75dc70d3-2aba-4c7a-b4a7-e42fdc218e8c",
  "machine_readable_source": true,
  "resources": [
    {
      "type": {
        "path": "/Dokument/PDF",
        "label": "PDF"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/dok/erlaeuterungen_bodenbewertung.pdf",
      "direct_link": true,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Dokument/PDF",
        "label": "PDF"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/dok/steckbrief_swr.pdf",
      "direct_link": true,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Dokument/PDF",
        "label": "PDF"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/dok/zuordnung_nag.pdf",
      "direct_link": true,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Archiv/ZIP",
        "label": "ZIP"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/nag/nag_fach_025.zip",
      "direct_link": true,
      "primary_content": true
    },
    {
      "type": {
        "path": "/Archiv/ZIP",
        "label": "ZIP"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/nag/nag_fach_100.zip",
      "direct_link": true,
      "primary_content": true
    },
    {
      "type": {
        "path": "/Archiv/ZIP",
        "label": "ZIP"
      },
      "url": "https://umweltanwendungen.schleswig-holstein.de/data/meta/boden/bodenbewertung/nag/nag_reg_fach_002.zip",
      "direct_link": true,
      "primary_content": true
    },
    {
      "type": {
        "path": "/Webseite",
        "label": "Webseite"
      },
      "url": "https://umweltgeodienste.schleswig-holstein.de/WFS_Bodenbewertung?",
      "direct_link": false,
      "primary_content": false
    },
    {
      "type": {
        "path": "/Webseite",
        "label": "Webseite"
      },
      "url": "https://umweltgeodienste.schleswig-holstein.de/WMS_Bodenbewertung?",
      "direct_link": false,
      "primary_content": false
    }
  ],
  "language": {
    "id": "German",
    "path": "/Deutsch",
    "label": "Deutsch"
  },
  "bounding_boxes": [
    {
      "min": {
        "x": 7.87,
        "y": 53.35
      },
      "max": {
        "x": 11.32,
        "y": 55.06
      }
    }
  ],
  "time_ranges": [
    {
      "from": "2023-08-29",
      "until": "2023-08-29"
    }
  ],
  "global_identifier": {
    "Url": "http://portalu.de/igc/a30c4c9a-a339-475a-aa7b-78dae94c162d"
  },
  "quality": {
    "findability": {
      "title": 0.0,
      "description": 0.2457974,
      "spatial": "BoundingBox",
      "spatial_score": 0.5,
      "temporal": true,
      "keywords": 0.0,
      "identifier": true,
      "score": 0.4576329
    },
    "accessibility": {
      "landing_page": "Specific",
      "landing_page_score": 1.0,
      "direct_access": true,
      "publicly_accessible": true,
      "score": 1.0
    },
    "interoperability": {
      "open_file_format": true,
      "media_type": true,
      "machine_readable_data": false,
      "machine_readable_metadata": true,
      "score": 0.75
    },
    "reusability": {
      "license": "ClearlySpecifiedAndFree",
      "license_score": 1.0,
      "contact_info": true,
      "publisher_info": true,
      "score": 1.0
    },
    "score": 0.80190825
  },
  "status": "Active"
}
