{
  "title": "ZKI Fire Monitoring System",
  "description": "Das \"ZKI Fire Monitoring System\" des DLR liefert aktuelle Waldbrandinformationen für Europa der letzten 31 Tage. Mit dem Ziel, verbrannte Flächen und deren räumliche Entwicklung nahezu in Echtzeit zu überwachen, wurde ein vollautomatisierter Ansatz entwickelt (Nolde et al. 2020, Link zur Publikation https://doi.org/10.3390/rs12132162) und in einer operativen Verarbeitungskette umgesetzt. Um mehrere Aktualisierungen pro Tag zu erreichen, werden Satellitendaten mittlerer Auflösung verwendet. Das Informationssystem ermöglicht eine interaktive Analyse aktueller Waldbrände in jedem von einem Benutzer definierten Interessengebiet.\n\nDie täglichen Produkte zeigen die Grenzen der verbrannten Gebiete, Minuten nachdem Satellitenbilder verfügbar sind. Für MODIS erfolgt der Datenempfang über die DLR-Bodenstationen in Neustrelitz und Oberpfaffenhofen (Deutschland) mittels Direktübertragung bei jeder Satellitenüberführung. Für Copernicus Sentinel-3-Daten stützen wir uns auf den Datenverteilungsmechanismus des Sentinel Open Access Hub der Europäischen Weltraumorganisation und das Copernicus Data Integration and Analysis System (DIAS).\n\nDie Produkte der verbrannten Fläche werden iterativ und automatisch über einen Zeitraum von 10 Tagen aktualisiert, sobald neue Satellitendaten verfügbar sind. Dies ermöglicht eine kontinuierliche Verbesserung der abgeleiteten Brandperimeter, indem der Einfluss von Störfaktoren wie Wolkenbedeckung, Rauchfahnen und Schatten minimiert wird.\n\nNeben dem Brandflächenumfang fließen weitere Attribute wie die Brandschwere, die betroffene Fläche sowie die genauen Erkennungszeiten in die Daten ein. Es ist daher möglich, die Entwicklung jedes verbrannten Bereichs in Raum und Zeit zu verfolgen.",
  "origins": [
    "/Land/Niedersachsen/MetaVer"
  ],
  "license": {
    "path": "/offen/andere",
    "label": "other-open",
    "url": "https://www.dcat-ap.de/def/licenses/other-open"
  },
  "mandatory_registration": false,
  "organisations": [
    {
      "Other": {
        "name": "Koordinierungsstelle GDI-NI beim Landesamt für Geoinformation und Landesvermessung Niedersachsen (LGLN)",
        "role": "Unknown",
        "websites": [
          "https://www.geodaten.niedersachsen.de/"
        ]
      }
    }
  ],
  "persons": [
    {
      "name": "Mitarbeiter/in",
      "role": "Contact",
      "emails": [
        "gdi@lgln.niedersachsen.de"
      ]
    }
  ],
  "tags": [
    {
      "Other": "GDIMRH"
    },
    {
      "Other": "Satellitenbild"
    },
    {
      "Other": "Waldbrand"
    },
    {
      "Other": "opendata"
    }
  ],
  "regions": [
    {
      "Other": "030000000000"
    }
  ],
  "modified": "2024-01-29",
  "source_url": "https://metaver.de/trefferanzeige?docuuid=1b690674-ecf7-4ff9-bc0a-730b05c89519",
  "machine_readable_source": true,
  "resources": [
    {
      "type": {
        "path": "/Webseite",
        "label": "Webseite"
      },
      "url": "https://services.zki.dlr.de/fire/#/map",
      "direct_link": false,
      "primary_content": false
    }
  ],
  "language": {
    "id": "German",
    "path": "/Deutsch",
    "label": "Deutsch"
  },
  "bounding_boxes": [
    {
      "min": {
        "x": -11.0,
        "y": 36.0
      },
      "max": {
        "x": 19.0,
        "y": 55.0
      }
    }
  ],
  "time_ranges": [
    {
      "from": "2024-01-29",
      "until": "2024-01-29"
    }
  ],
  "global_identifier": {
    "Uuid": "1b690674-ecf7-4ff9-bc0a-730b05c89519"
  },
  "quality": {
    "findability": {
      "title": 0.59000003,
      "description": 0.23003519,
      "spatial": "BoundingBox",
      "spatial_score": 0.5,
      "temporal": true,
      "keywords": 0.0,
      "identifier": true,
      "score": 0.5533392
    },
    "accessibility": {
      "landing_page": "Specific",
      "landing_page_score": 1.0,
      "direct_access": false,
      "publicly_accessible": true,
      "score": 0.6666667
    },
    "interoperability": {
      "open_file_format": false,
      "media_type": false,
      "machine_readable_data": false,
      "machine_readable_metadata": true,
      "score": 0.25
    },
    "reusability": {
      "license": "UnclearInformation",
      "license_score": 0.33333334,
      "contact_info": true,
      "publisher_info": true,
      "score": 0.77777785
    },
    "score": 0.5619459
  },
  "status": "Active"
}
