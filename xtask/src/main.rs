use std::env::{args, set_current_dir, var_os};
use std::fs::{create_dir, remove_dir_all, File, OpenOptions as FsOpenOptions};
use std::io::Write;
use std::path::Path;
use std::process::Command;

use anyhow::{anyhow, ensure, Context, Result};

fn main() -> Result<()> {
    set_workspace_dir()?;

    let args = args().skip(1).collect::<Vec<_>>();

    let (cmd, args) = args
        .split_first()
        .map(|(cmd, args)| (Some(&**cmd), args))
        .unwrap_or_default();

    match cmd {
        None => default(),
        Some("harvester") => harvester(),
        Some("indexer") => indexer(),
        Some("server") => server(),
        Some("test") => test(args),
        Some("bench") => bench(args),
        Some("regression-test") => regression_test(args),
        Some("pretty-print-dataset") => pretty_print_dataset(args),
        Some("explain-query") => explain_query(args),
        Some("playground") => playground(),
        Some(name) => Err(anyhow!("Unknown task {name}")),
    }
}

fn default() -> Result<()> {
    cargo("Rustfmt", &["fmt", "--all"], &[], &[])?;

    cargo(
        "Clippy",
        &["clippy", "--workspace", "--all-targets"],
        &[],
        &[],
    )?;

    Ok(())
}

fn harvester() -> Result<()> {
    let _ = create_dir("data");

    if let Ok(mut file) = FsOpenOptions::new()
        .write(true)
        .create_new(true)
        .open("data/harvester.toml")
    {
        file.write_all(b"keep_failed_sources = false\nmerge_duplicates = true\n\n[auto_classify]\ndisabled = true\n\n")?;

        println!("An empty configuration was created at 'data/harvester.toml'. Please add at least one source and retry.");
        return Ok(());
    }

    cargo(
        "Harvester",
        &["run", "--package=harvester", "--bin=harvester"],
        &[],
        &[
            ("DATA_PATH", "data"),
            ("CONFIG_PATH", "data/harvester.toml"),
            ("EXTERNAL_PATH", "deployment/external_harvesters"),
            (
                "BROWSER_DOWNLOAD_DIR",
                "/home/ubuntu/snap/chromium/current/Downloads",
            ),
            ("RESPONSES_IN_FLIGHT", "256"),
            ("KEEP_STALE_RESPONSES", ""),
            ("CHECK_SOURCE_URL", ""),
            ("RETRY_ATTEMPTS", "0"),
            (
                "RUST_LOG",
                "info,metadaten=debug,harvester=debug,Poppler=warn",
            ),
            ("PYTHON_LOG", "DEBUG"),
        ],
    )?;

    indexer()?;

    Ok(())
}

fn indexer() -> Result<()> {
    let _ = remove_dir_all("data/index");

    cargo(
        "Indexer",
        &["run", "--bin=indexer"],
        &[],
        &[
            ("DATA_PATH", "data"),
            ("MEMORY_BUDGET", "1024"),
            ("RUST_LOG", "info,metadaten=debug,indexer=debug"),
        ],
    )?;

    Ok(())
}

fn server() -> Result<()> {
    cargo(
        "Server",
        &["run", "--package=server", "--bin=server"],
        &[],
        &[
            ("DATA_PATH", "data"),
            ("ASSETS_PATH", "server/assets"),
            ("ORIGINS_PATH", "deployment/origins.toml"),
            ("SEARCH_CACHE_SIZE", "128"),
            ("COMPLETE_CACHE_SIZE", "1024"),
            ("BIND_ADDR", "127.0.0.1:8081"),
            ("RUST_LOG", "info,metadaten=debug,server=debug"),
        ],
    )?;

    Ok(())
}

fn test(args: &[String]) -> Result<()> {
    cargo("Tests", &["test", "--workspace"], args, &[])?;

    Ok(())
}

fn bench(args: &[String]) -> Result<()> {
    cargo(
        "Benchmarks",
        &["bench", "--workspace"],
        args,
        &[
            ("DATA_PATH", "data"),
            ("SEARCH_CACHE_SIZE", "0"),
            ("COMPLETE_CACHE_SIZE", "0"),
        ],
    )?;

    Ok(())
}

fn regression_test(args: &[String]) -> Result<()> {
    if args == ["accept"] {
        let status = Command::new("git")
            .args([
                "add",
                "harvester/regression-test/harvester.toml",
                "harvester/regression-test/responses",
                "harvester/regression-test/datasets",
            ])
            .status()?;

        ensure!(
            status.success(),
            "Failed to add changes to regression tests"
        );

        let status = Command::new("git").arg("commit").status()?;

        ensure!(
            status.success(),
            "Failed to commit changes to regression tests"
        );
    }

    cargo(
        "Harvester",
        &[
            "run",
            "--features=regression-test",
            "--package=harvester",
            "--bin=harvester",
        ],
        &[],
        &[
            ("DATA_PATH", "harvester/regression-test"),
            ("CONFIG_PATH", "harvester/regression-test/harvester.toml"),
            ("RESPONSES_IN_FLIGHT", "256"),
            ("KEEP_STALE_RESPONSES", ""),
            ("RUST_LOG", "info,html5ever::tree_builder=off,Poppler=off"),
        ],
    )?;

    cargo(
        "Indexer",
        &["run", "--features=regression-test", "--bin=indexer"],
        &[],
        &[
            ("DATA_PATH", "harvester/regression-test"),
            ("MEMORY_BUDGET", "1024"),
            ("RUST_LOG", "info"),
        ],
    )?;

    let status = Command::new("git")
        .args([
            "diff",
            "--stat",
            "--exit-code",
            "harvester/regression-test/datasets",
        ])
        .status()?;

    ensure!(status.success(), "Changes detected in datasets");

    let status = Command::new("git")
        .args(["clean", "--force", "--quiet", "harvester/regression-test"])
        .status()?;

    ensure!(
        status.success(),
        "Failed to clean regression test directory"
    );

    Ok(())
}

fn pretty_print_dataset(args: &[String]) -> Result<()> {
    for arg in args {
        let stdin = File::open(arg).with_context(|| format!("Failed to open dataset {}", arg))?;

        let status = Command::new("cargo")
            .args(["run", "--bin=pretty_print_dataset", "--quiet", "--"])
            .envs([("DATA_PATH", "data"), ("RUST_LOG", "info")])
            .stdin(stdin)
            .status()?;

        ensure!(
            status.success(),
            "Failed to pretty-print dataset with status {status:?}"
        );
    }

    Ok(())
}

fn explain_query(args: &[String]) -> Result<()> {
    let status = Command::new("cargo")
        .args(["run", "--bin=explain_query", "--quiet", "--"])
        .args(args)
        .envs([
            ("DATA_PATH", "data"),
            ("SEARCH_CACHE_SIZE", "0"),
            ("COMPLETE_CACHE_SIZE", "0"),
            ("RUST_LOG", "info"),
        ])
        .status()?;

    ensure!(
        status.success(),
        "Failed to explain query with status {status:?}"
    );

    Ok(())
}

fn playground() -> Result<()> {
    let status = Command::new("cargo")
        .args(["run", "--bin=playground"])
        .envs([("DATA_PATH", "data"), ("RUST_LOG", "info")])
        .status()?;

    ensure!(
        status.success(),
        "Failed run playground with status {status:?}"
    );

    Ok(())
}

fn cargo(name: &str, args: &[&str], forward_args: &[String], envs: &[(&str, &str)]) -> Result<()> {
    let status = Command::new("cargo")
        .args(args.iter().copied())
        .args(forward_args)
        .envs(envs.iter().copied())
        .status()?;

    ensure!(status.success(), "{name} failed with status {status:?}");

    Ok(())
}

fn set_workspace_dir() -> Result<()> {
    let manifest_dir = var_os("CARGO_MANIFEST_DIR").expect("Must be invoked via `cargo xtask`");

    let workspace_dir = Path::new(&manifest_dir).parent().unwrap();

    set_current_dir(workspace_dir)?;

    Ok(())
}
